//########################################################
//
// FILE:   HC415U111_main.h
//
// TITLE:   HA415U111_main variables definitions.
//
//########################################################
// Running on TMS320LF280xA   DCDC part   HA415U111                
// External clock is 20MHz, PLL * 6/2 , CPU-Clock 60 MHz	      
// Date: from March 16, 2007 to Nov 30, 2007  , (C) czk
// Version:1.00     Change Date: April 4, 2007 , (C) czk					  	
//########################################################

#ifndef App_MAIN_H
#define App_MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/*******************************************************************************
*variables and functions for main and time treament 														  
*******************************************************************************/
extern void vSingleBrdTst(void); 
extern void vLEDTst(void); 
extern ubitintc	g_u16RunFlag;		//run flag of all the functions

////the functions of interrupt
extern interrupt void Epwm8_Isr(void);    //PWM7作为计算中断ISR的频率
extern interrupt void Epwm1_Isr(void);    //PWM1捕捉周期
extern interrupt void Epwm3_Isr(void);    //PWM3捕捉相位
extern void vCanProcess(void);

//asm Pointer
//kick watch dog
extern void WatchDogKickPointer(void);

extern void vIntOscSet(UINT16  u16set);


//cmd
extern UINT16 g_u16ActionReady;     //rectifier soft start state

extern UINT16 g_u16DcRippSWCtrl;
extern ubitfloat g_lq10OpenTime;    //interval time of module working orderly(s)
extern ubitfloat g_lq10WalkInTime;  // rectifier start time for current walk in
/*******************************************************************************
*variables and functions declare for CAN communication
*******************************************************************************/
extern CANFRAME  g_sBootCanTxData;
extern CANFRAME  g_sBootCanRxData;
extern ubitfloat g_lq10RunTime;     // the whole time of module working (hour)
extern ubitfloat g_lq10MaintainTimes;// the Maintain times of module(times)
extern ubitinta  g_u16MdlCtrl;      //module control flag

extern ubitintaextend  g_u16MdlCtrlExtend;

extern  volatile    UINT16 g_u16MdlAddr;
//variables for module`s node id
extern UINT16 g_u16NodeId0H;
extern UINT16 g_u16NodeId0L;
extern UINT16 g_u16NodeId1H;
extern UINT16 g_u16NodeId1L;
extern UINT16 g_u16NodeId1LTmp;
extern UINT16 g_u16NodeId0H_TO_CONTROLLER;
extern UINT16 g_u16NodeId1L_TO_CONTROLLER;

extern UINT16 g_u16WriteNumber;
// variables for module`s  firstly maintain time and contain
extern UINT16 g_u16MaintainData0H;
extern UINT16 g_u16MaintainData0L;
//variables for module`s character data
extern UINT16 g_u16CharactData0H;
extern UINT16 g_u16CharactData0L;
// variables for module`s  secondly maintain time and contain
extern UINT16 g_u16MaintainData1H;
extern UINT16 g_u16MaintainData1L;
//variables for module`s hardware and software version
extern UINT16 g_u16VersionNoHw;
extern UINT16 g_u16VersionNoSw;
// variables for module`s  bar code
extern UINT16 g_u16BarCode0H;
extern UINT16 g_u16BarCode0L;
extern UINT16 g_u16BarCode1H;
extern UINT16 g_u16BarCode1L;
extern UINT16 g_u16BarCode2H;
extern UINT16 g_u16BarCode2L;
extern UINT16 g_u16BarCode3H;
extern UINT16 g_u16BarCode3L;

extern struct ECAN_REGS ECana32Temp;

extern ubitfloat g_lq10AcCurrFt;        // AC input current limit default value
extern ubitfloat g_lq12VoltConSysa;     //DC volt refer adjust coefficient a
extern ubitfloat g_lq12VoltConSysb;     //DC volt refer adjust coefficient b
extern ubitfloat g_lq10CurrConSysa;     //dc curr limit adjust coefficient a
extern ubitfloat g_lq10CurrConSysb;     //dc curr limit coefficient b
extern ubitfloat g_lq10PowerConSysa;    //power  adjust coefficient a
extern ubitfloat g_lq10PowerConSysb;    //power adjust coefficient b
extern ubitfloat g_lq10VpfcConSys;      //pfc volt refer adjust coefficient a

extern ubitfloat g_lq10PfcVoltChangeValue;//PFC VOLTAGE VARIFY
extern UINT16 g_u16PfcVoltDebug;
extern UINT16 g_u16UnPlugFlag ;
extern UINT16 g_u16RealyTestFlag;

extern UINT16 g_u16BrdTestFlag;
extern UINT16 g_u16ForceDriveFlag;
extern UINT16  g_u16LEDFlg;
extern INT16 g_i16FANADJFLAG;
extern INT16 g_i16FANADJVAL;
extern UINT16 g_u16calflag;   //查看是哪一个变量没校准的标志位
//extern UINT16 g_u16ADTRIGVAL;
extern UINT16 g_u16INTTRIGVAL;
extern UINT16 g_u16PFCphaseADJFLAG;
extern UINT16 g_u16newPFCCORECOEF;
extern UINT16 g_u16downloadflag;
//extern UINT16 g_i16FANADSTEP;
extern UINT16 g_u16HVSDDebug;
extern UINT16 g_u16ACMAXPolarity;
extern UINT16  g_u16PfcvoltChgFlag;
extern ubitfloat g_lq10VacOffsetVolt;
extern ubitfloat g_lq10PfcVolt;         //pfc volt for protect
extern ubitfloat g_lq10VpfcSampSys;     //pfc volt adjust coefficient a

extern ubitfloat g_lq12VoltSampSysa;    //dcdc volt adjust coefficient a
extern ubitfloat g_lq12VoltSampSysb;    // dcdc volt adjust coefficient b
extern ubitfloat g_lq10CurrSampSysa;    //dcdc curr adjust coefficient a
extern ubitfloat g_lq10CurrSampSysb;    //dcdc curr adjust coefficient b
extern ubitfloat g_lq10MdlPow;          // rectifier power of cal
extern ubitfloat g_lq10AcVrmsSampSysa;  //AC volt RMS adjust coefficient a
extern ubitfloat g_lq10AcVrmsSampSysb;  //AC volt RMS adjust coefficient b
extern ubitfloat g_lq10AcIrmsSampSysa;//AC Cur RMS adjust coefficient a
extern ubitfloat g_lq10AcIrmsSampSysb;//AC Cur RMS adjust coefficient b
extern UINT16 g_u16RepairEnable;

extern UINT16 g_u16FanEnable;         // Fan run enable flag


/*******************************************************************************
*variables and functions declare for eeprom
*******************************************************************************/

extern ubitfloat g_lq10MdlPowerFt;  //power limit default value
extern ubitfloat g_lq10MdlCurrFt;   //current limit default value
extern ubitfloat g_lq10MdlVoltFt;   //dcvolt default value
extern ubitfloat g_lq10ReonTime;    // rectifier restart time after HVSD

extern ubitfloat g_lq10MdlVolt;        // Display and control rectifir dcvolt
extern ubitfloat g_lq10MdlVoltUp;   // dcvolt upper limit set by SCU+
extern ubitfloat g_lq10MdlTempUp;   // rectifier upper limit set by SCU+
extern UINT16 g_u16IntOscCalibrateFlag;
extern INT16 g_i16FreDeviation;

///////////////////////////////////////////////////
//main
extern UINT16 g_u16CpuStartTimer;
extern UINT16 g_u16IntOscCalibrateStatus;
extern ubitinti g_u16SelfDxTimer;
extern UINT16 g_u16FuseBroken;
extern INT16  g_i16SelfDxDelta;
extern UINT32 g_u32MasterDxTimer;






/*******************************************************************************
*variables and functions declare for alarm log  treatment														  
*******************************************************************************/


extern 	UINT16 	  g_u16I2CErrorType;
extern  ubitfloat g_fRdTemp;
extern  UINT16 	  g_u16WriteNumber;



/*******************************************************************************
*functions declare:these functions only can use in this file                                    
*******************************************************************************/



//模块关PFC,DC的动作。


/*******************************************************************************
*variables  declare for password
*******************************************************************************/
//extern Uint16 PRG_key0;        //   CSM Key values
//extern Uint16 PRG_key1;
//extern Uint16 PRG_key2;
//extern Uint16 PRG_key3;
//extern Uint16 PRG_key4;
//extern Uint16 PRG_key5;
//extern Uint16 PRG_key6;
//extern Uint16 PRG_key7;



 
#ifdef __cplusplus
}
#endif 


#endif 


