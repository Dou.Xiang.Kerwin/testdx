//###########################################################################
//
// FILE:   F28004x_device.h
//
// TITLE:  F28004x Device Definitions.
//
//###########################################################################
// $TI Release: F28004x Support Library v1.05.00.00 $
// $Release Date: Thu Oct 18 15:43:57 CDT 2018 $
// $Copyright:
// Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Texas Instruments Incorporated nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// $
//###########################################################################

#ifndef F28004X_DEVICE_H
#define F28004X_DEVICE_H

#ifdef __cplusplus
extern "C" {
#endif

//
// Common CPU Definitions:
//
#ifdef __TMS320C28XX_CLA__
//
// There are only two assembly instructions that can access the MSTF register
// - MMOV32 mem32, MSTF
// - MMOV32 mem32, MSTF
// The CLA C compiler allows 'C' access to this control register through the __cregister
// keyword. In order to access the register's contents, the user must copy it to the
// shadow object defined below
// Note that _MSTF is the only __cregister recognized by the CLA C compiler; IER and IFR
// are not accessible (therefore not recognized), therefore __cregister must be redefined to
// null to prevent a cla C compiler error
//
struct MSTF_SHADOW_BITS {     // bits description
    unsigned short LVF:1;     // 0    Latched Overflow Flag
    unsigned short LUF:1;     // 1    Latched Underflow Flag
    unsigned short NF:1;      // 2    Negative Float Flag
    unsigned short ZF:1;      // 3    Zero Float Flag
    unsigned short rsvd1:2;   // 5:4  Reserved
    unsigned short TF:1;      // 6    Test Flag
    unsigned short rsvd2:2;   // 8:7  Reserved
    unsigned short RNDF32:1;  // 9    Rounding Mode
    unsigned short rsvd3:1;   // 10   Reserved
    unsigned short MEALLOW:1; // 11   MEALLOW Status
    unsigned short RPCL:4;    // 15:12    Return PC: Low Portion
    unsigned short RPCH:12;   // 27:16    Return PC: High Portion
    unsigned short rsvd4:4;   // 31:28    Reserved
};
extern __cregister volatile unsigned int MSTF;
#endif //__TMS320C28XX_CLA__

#ifndef __TMS320C28XX__
#define __cregister
#endif  //__TMS320C28xx__

extern __cregister volatile unsigned int IFR;
extern __cregister volatile unsigned int IER;



#define  EINT   __asm(" clrc INTM")
#define  DINT   __asm(" setc INTM")
#define  ERTM   __asm(" clrc DBGM")
#define  DRTM   __asm(" setc DBGM")
//#define  EALLOW __eallow()
//#define  EDIS   __edis()
#define    EALLOW __asm(" EALLOW")
#define    EDIS   __asm(" EDIS")
#define  ESTOP0 __asm(" ESTOP0")

#define M_INT1  0x0001
#define M_INT2  0x0002
#define M_INT3  0x0004
#define M_INT4  0x0008
#define M_INT5  0x0010
#define M_INT6  0x0020
#define M_INT7  0x0040
#define M_INT8  0x0080
#define M_INT9  0x0100
#define M_INT10 0x0200
#define M_INT11 0x0400
#define M_INT12 0x0800
#define M_INT13 0x1000
#define M_INT14 0x2000
#define M_DLOG  0x4000
#define M_RTOS  0x8000

#define BIT0    0x00000001
#define BIT1    0x00000002
#define BIT2    0x00000004
#define BIT3    0x00000008
#define BIT4    0x00000010
#define BIT5    0x00000020
#define BIT6    0x00000040
#define BIT7    0x00000080
#define BIT8    0x00000100
#define BIT9    0x00000200
#define BIT10   0x00000400
#define BIT11   0x00000800
#define BIT12   0x00001000
#define BIT13   0x00002000
#define BIT14   0x00004000
#define BIT15   0x00008000
#define BIT16   0x00010000
#define BIT17   0x00020000
#define BIT18   0x00040000
#define BIT19   0x00080000
#define BIT20   0x00100000
#define BIT21   0x00200000
#define BIT22   0x00400000
#define BIT23   0x00800000
#define BIT24   0x01000000
#define BIT25   0x02000000
#define BIT26   0x04000000
#define BIT27   0x08000000
#define BIT28   0x10000000
#define BIT29   0x20000000
#define BIT30   0x40000000
#define BIT31   0x80000000

//
// For Portability, User Is Recommended To Use the C99 Standard integer types
//
#if !defined(__TMS320C28XX_CLA__)
#include <assert.h>
#include <stdarg.h>
#endif //__TMS320C28XX_CLA__
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

//
// C99 defines boolean type to be _Bool, but this doesn't match the format of
// the other standard integer types.  bool_t has been defined to fill this gap.
//
typedef _Bool bool_t;

//
// Used for a bool function return status
//
typedef _Bool status_t;

#ifndef SUCCESS
#define SUCCESS     true
#endif

#ifndef FAIL
#define FAIL        false
#endif

//
// The following data types are included for compatibility with legacy code,
// they are not recommended for use in new software.  Please use the C99
// types included above
//
#if (!defined(DSP28_DATA_TYPES) && !defined(F28_DATA_TYPES))
#define DSP28_DATA_TYPES
#define F28_DATA_TYPES

#ifdef __TMS320C28XX_CLA__
typedef short                                   int16;
typedef long                                    int32;
typedef unsigned char                           Uint8;
typedef unsigned short                          Uint16;
typedef unsigned long                           Uint32;
typedef float                                   float32;
typedef long double                             float64;
typedef struct { Uint32 low32; Uint32 high32; } Uint64;
typedef struct { int32  low32; int32  high32; } int64;

typedef int                                     INT16;
typedef long                                    INT32;
typedef unsigned int                            UINT16;
typedef unsigned long                           UINT32;
typedef float                                   FLOAT32;
typedef long double                             FLOAT64;

#else // __TMS320C28XX__
typedef int                                     int16;
typedef long                                    int32;
typedef long long                               int64;
typedef unsigned int                            Uint16;
typedef unsigned long                           Uint32;
typedef unsigned long long                      Uint64;
typedef float                                   float32;
typedef long double                             float64;

typedef int                                     INT16;
typedef long                                    INT32;
typedef unsigned int                            UINT16;
typedef unsigned long                           UINT32;
typedef float                                   FLOAT32;
typedef long double                             FLOAT64;

#endif //__TMS320C28XX_CLA__

#endif //(!defined(DSP28_DATA_TYPES) && !defined(F28_DATA_TYPES))

//
// The following data types are for use with byte addressable peripherals.
// See compiler documentation on the byte_peripheral type attribute.
//
#ifndef __TMS320C28XX_CLA__
typedef unsigned int bp_16 __attribute__((byte_peripheral));
typedef unsigned long bp_32 __attribute__((byte_peripheral));
#endif

//transplant from R48-3000e3 v104
struct MDLCTRL_BITS {       // bits  description
    Uint16  rsvd:1;     //paul-20220228
    Uint16  AntiTheftCanIntOff:1; //1 CAN通信故障是否关机。1-允许关机  0-禁止关机
    Uint16  AntiTheftFlag:1;  //2  Antitheft function  1:enbale   0:disable
    Uint16  AntiTheftUnlockFlag:1; //3 AntiTheftUnlock 1: enable   0:disable

    Uint16  EStopFlag:1;    // 5  Emergent stop Rect    1:off       0:on
    Uint16  NoDeratingFlag:1; // 6 Mix system Derating or not       1:not       0:derating
////////////////////////////////FUSEB 要对应写入EPROM,不要轻易动位置//////////
    Uint16  FUSEB:1;        // 6 fuse broken            1:broken    0:normal
////////////////////////////////ONCELOCK 要对应写入EPROM,不要轻易动位置//////////
    Uint16  ONCELOCK:1;     // 7  DCOV times for lock   1:once      0:twice
////////////////高8位和通讯协议的位控制相对应,不能调整位置,除非改协议////////////
    Uint16  ALLOWACOV:1;    // 8  ACOV allow start      1:enable    0:disable
    Uint16  INITCAN:1;       // 9  CAN INIT                 1:INIT CAN 0:NO ACTION
    Uint16  SHORTRELAY:1;   // 10 turn off PFC&DC and main relay 1:turn off 0:not turn off
    Uint16  IDENTIFY:1;     // 11 set rect identify     1:identify  0:no identify
    Uint16  FANFULL:1;      // 12  set fan full speed   1:full      0:adjust
    Uint16  WALKIN:1;       // 13  set Walkin           1:enable    0:disable
    Uint16  HVSDRESET:1;    // 14 HVSD RESET            1:RESET    0: No action
    Uint16  OFFCTRL:1;      // 15  set rectifier on/off  1:off          0:on
};
typedef union {
   Uint16               all;
   struct MDLCTRL_BITS   bit;
}ubitinta;

struct MDLCTRLEXTEND_BITS{      //bits description
    Uint16  ACLIMMODE:1;    //0 AC lim mode 1:mode1(B) 90~132(constant)132~176(linear derating)     0:mode0(A):176 linear below(default)
    Uint16  DCINPUTALARM:1; //1 DC input alarm  1:when DC input no alarm(default)   0:when DC input no alarm
    Uint16  rsd:14;         //15~2
};
typedef union{
    Uint16                      all;
    struct  MDLCTRLEXTEND_BITS bit;
}ubitintaextend;


/*
#define MDLSTAT_OFFSTAT            0x0001
#define MDLSTAT_DCOV               0x0002
#define MDLSTAT_AMBIOT             0x0004
#define MDLSTAT_RECTOT             0x0008
#define MDLSTAT_FANFAIL            0x0010
#define MDLSTAT_ACUV               0x0020
#define MDLSTAT_ACOV               0x0040
#define MDLSTAT_PFCUV              0x0080
#define MDLSTAT_PFCOV              0x0100
#define MDLSTAT_CALIBRFAIL         0x0200
#define MDLSTAT_NOCONTROLLER       0x0400
#define MDLSTAT_IUNBALMAJOR        0x0800
#define MDLSTAT_IUNBALMINOR        0x1000
#define MDLSTAT_SHORT              0x2000
#define MDLSTAT_IDERR              0x4000
#define MDLSTAT_HVSDLOCK           0x8000
*/





struct RUNFLAG_BITS {         // bits  description
    Uint16  CAL:1;              // 0    circuit calculation     1:set   0:clear
    Uint16  WARN:1;             // 1    warn and control        1:set   0:clear
    Uint16  LOADSHARE:1;        // 2    load sharing flag       1:set   0:clear
    Uint16  SHARESYN:1;         // 3    load share syn send     1:set   0:clear
    Uint16  SHAREDATA:1;        // 4    load share data send    1:set   0:clearl
    Uint16  ADDRCAL:1;          // 5    address calculation     1:set   0:clear
    Uint16  ADC:1;              // 6    ADC filter calculation  1:set   0:clear
    Uint16  LIGHT:1;            // 7    light blink flag        1:set   0:clear
    Uint16  RECORDTIME:1;       // 8    record time             1:set   0:clear
    Uint16  TIMEEEPROM:1;       // 9    record time to eeprom flag  1:set   0:clear
//  Uint16  QUICKLIGHT:1;           //8 quick light blink flag  1:set   0:clear
//    Uint16  PFCUVCHECKTIMER:1;    // 8    PFC UV Check flag       1:set   0:clear
//    Uint16  DCFAULTCHECKTIMER:1;// 9  DC FAULT Check flag     1:set   0:clear
//    Uint16  FUSEFAULBACK:1;     //9
    Uint16  HWHVSDDisp:1;      //10
    Uint16  HWHVSDDispRst:1;   //11
    Uint16  ANTITHIEFTIME:1;   // 12

    Uint16  rsd:3;              // 15:13
};



typedef union {
   Uint16               all;
   struct RUNFLAG_BITS   bit;
}ubitintc;



//20111130
struct SELFDXVALUE_BITS {       // bits  description
    Uint16  ADDRESS:8;      // address
    Uint16  ATTR:8;         // attribute
};

typedef union {
   Uint16               all;
   struct SELFDXVALUE_BITS   bit;
}ubitintg;

struct SELFDXCTRL_BITS {        // bits  description
    Uint16  INGADDR:7;      // address(which is diagnosing) 0~100
    Uint16  MASTERMDL:1;    // 1: master module, 0: not
    Uint16  START:1;        // 1: start self diagnosis, 0: disable
    Uint16  TXEN:1;         // 1: send can data enable, 0: disable
    Uint16  NEXTDELAY:3;    // time for master mdl not receive data
    Uint16  STATE:3;        // self diagnosis state
};

typedef union {
   Uint16               all;
   struct SELFDXCTRL_BITS   bit;
}ubitinth;

struct SELFDXTIMER_BITS {       // bits  description
    Uint16  SLAVETIMER:16;   // slave mdl timer
    Uint16  CHKDELAY:16;    // delay time for judge whether is fuse broken or not
};

typedef union {
    Uint32               all;
   struct SELFDXTIMER_BITS   bit;
}ubitinti;
//////////////////////////////////////////////////////////////
struct int16Data
{
    int iLD;
    int iHD;
};
typedef struct int16Data intStructData;
/////////////////////////////////////////////////////////////

typedef union
{
    int32 lData;
    intStructData iData;
}longunion;


typedef union
{
    unsigned int id ;
    struct   packed_data
    {
         unsigned highchar : 8;
         unsigned lowchar  : 8;
    }bitdata;
}ubitint ;


typedef union
{
    float fd ;
    ubitint uintdata[2];
    long    lData;  //changed by lvyg
    intStructData iData; //changed by lvyg
}ubitfloat ;


typedef union
{
    unsigned int msgaddid[3];
    struct
    {
        unsigned ADDIDL :16;        /* reserved */
        unsigned ADDIDH :16;        /* reserved */
        unsigned ADDADD :8;     /* com attribute */
        unsigned ADDOK  :8;     /* 1:continuous frame */
    }msgaddbit;
}frameaddid;


/*----------for boot---------------*/

typedef struct
{
    Uint16  *uiLoadAddress;
    Uint16  uiLength;
} APPLICATIONHEADER;

typedef struct
{
    Uint16      uiAddress;
    Uint16      uiAlarm;
    ubitfloat   ubitfData;
}EepromData;

extern void StartTimerPointer(Uint16 uiDelayTime);
extern Uint16 TimeIsUpPointer(void);
extern void ProcessInquiryPointer(void);
//extern void SendBlockRequestPointer(Uint16  uiStatus, Uint32  ulAddr, Uint16  uiLength);
extern Uint16 GetHeaderPointer(APPLICATIONHEADER *Header, Uint16  uiImageAddr, Uint16 uiHeaderSize);
extern Uint16 GetDataBlockPointer(Uint16  *uiDestination, Uint16  uiLength, Uint32  ulImageAddr);


//----------------------------------------------------------------------
#define TIME_200mS                  10000  //20us time base  ls 20111128
#define TIME_1000mS                 50000

#define SIZE_OF_BOOTLOADERHEADER    4
#define FIRSTTRIES                  255 // We try 255 times on the first header
#define TRIES                       5       // We try five times on subsequent data
#define BLOCKACKNOWLEDGE            0   // Block Acknowledge
#define MISSINGDATAPACKET           1   // Missing Data Packet in Block
#define LASTBLOCKACKNOWLEDGE        2   // Last Block Acknowledge
#define INVALIDAPPLICATION          3   //  Invalid Application Image
#define DEVICEMISPROGRAMMED         4   //  Device Misprogrammed

#define FLASHROWSIZE                128// 128 words = 256 bytes, the maximum size
#define RAMBLOCKSIZE                128

#define MSG_SIZE_8      0x08        /*  the address arbitration message has 6 bytes of data */

extern volatile UINT32   ulENP_Lo_SN;
extern volatile ubitfloat   ulENP_Hi_SN;
extern volatile Uint16  u16WaitLoopCount;

extern Uint16 BootRamfuncsLoadStart;
extern Uint16 BootRamfuncsLoadEnd;
 extern Uint16 BootRamfuncsRunStart;

extern Uint16 IsrRamfuncsLoadStart;
extern Uint16 IsrRamfuncsLoadEnd;
extern Uint16 IsrRamfuncsRunStart;

// These are defined by the linker file
extern Uint16 Cla1funcsLoadStart;
extern Uint16 Cla1funcsLoadEnd;
extern Uint16 Cla1funcsRunStart;

extern volatile Uint16 uiFromApplication;
//extern volatile   Uint16 g_u16MdlAddr;
/*-----------------------------------------------------------------*/

extern void Load_Code_And_Execute(void);
extern void ApplicationValidPointer(void);
extern void B1AppEntryPointer(void);
extern void B1RstEntryPointer(void);
extern void InitFlashPointer(void);
extern void MemCopyPointer(Uint16* SourceAddr, Uint16* SourceEndAddr, Uint16* DestAddr);
extern void InitSysCtrlPointer(void);
extern void InitGpioPointer(void);
extern void InitCanPointer(void);
extern void InitCanaIDPointer(void);
//extern void InitCanaPointer(void); //ls 20111125
extern void InitCanaloadPointer(void);
extern void InitCanaPointer(void);
extern void InitI2CPointer(void);
extern void InitSciPointer(void);
extern void InitECapturePointer(void);

//extern void InitECapAPWMPointer(void);

extern void InitCpuTimersPointer(void);
extern void DisableDogPointer(void);
extern void EnableDogPointer(void);

//extern void   InitAdcPointer(void);
//extern void   InitEPwmPointer(void);
extern void InitPieCtrlPointer(void);
extern void InitPieVectTablePointer(void);
extern void InitPeripheralsPointer(void);



extern void InitRAMPointer(void);
extern void InitRAM(void);
extern interrupt void USER_ISR_Pointer(void);
extern void Application_Entry_Point(void);

extern void CRC16_CheckWord(void);
extern void Boot_Version(void);
extern void Software_Version(void);
extern void Hardware_Version(void);
extern void Baseline_Version(void);

//
// Include All Peripheral Header Files:
//
#include "f28004x_adc.h"
#include "f28004x_analogsubsys.h"
#include "f28004x_can.h"
#include "f28004x_cla.h"
#include "f28004x_cla_prom_crc32.h"
#include "f28004x_cmpss.h"
#include "f28004x_cputimer.h"
#include "f28004x_dac.h"
#include "f28004x_dcsm.h"
#include "f28004x_dma.h"
#include "f28004x_ecap.h"
#include "f28004x_epwm.h"
#include "f28004x_epwm_xbar.h"
#include "f28004x_eqep.h"
#include "f28004x_erad.h"
#include "f28004x_flash.h"
#include "f28004x_fsi.h"
#include "f28004x_gpio.h"
#include "f28004x_i2c.h"
#include "f28004x_input_xbar.h"
#include "f28004x_memconfig.h"
#include "f28004x_nmiintrupt.h"
#include "f28004x_output_xbar.h"
#include "f28004x_pga.h"
#include "f28004x_piectrl.h"
#include "f28004x_pievect.h"
#include "f28004x_pmbus.h"
#include "f28004x_sci.h"
#include "f28004x_sdfm.h"
#include "f28004x_spi.h"
#include "f28004x_sysctrl.h"
#include "f28004x_xbar.h"
#include "f28004x_xint.h"

#include "f28004x_cla_typedefs.h"   // f28004x CLA Type definitions
#include "f28004x_examples.h"       // f28004x Examples Include File

#include "hw_can.h"
//
// Include Example Header Files:
//

#include "f28004x_globalprototypes.h"  //Prototypes for global functions
                                       //within the .c files.
#include "f28004x_adc_defines.h"
#include "f28004x_I2c_defines.h"
#include "f28004x_cputimervars.h"
#include "f28004x_epwm_defines.h"
#include "f28004x_ecap_defines.h"
#include "f28004x_gpio_defines.h"       // Macros used for GPIO support code
#include "f28004x_pie_defines.h"        // Macros used for PIE examples
#include "f28004x_sysctrl_defines.h"    // Macros used for LPM support code
#include "f28004x_dma_defines.h"        // Macros used for DMA support code
#include "f28004x_cla_defines.h"        //Macros used for CLA support code

//
// Include files not used with F/BIOS
//
#ifndef F28_BIOS
#include "f28004x_defaultisr.h"
#endif

//transplant from R48-3000e3 v104
//#include "DSP2803x_Comp.h"
//#include "DSP2803x_Cla.h"
//#include "DSP2803x_Cla_defines.h"
//#include "Flash2803x_API.h"
//
// byte_peripheral attribute is only supported on the C28
//
#ifndef __TMS320C28XX_CLA__
#include "f28004x_can.h"
#include "f28004x_dcc.h"
#include "f28004x_lin.h"
#endif

#ifdef __cplusplus
}
#endif                                  /* extern "C" */

#endif                                  // end of F28004X_DEVICE_H definition

//===========================================================================
// End of file.
//===========================================================================
