
MEMORY
{
PAGE 0 : //program space
   /* BEGIN is used for the "boot to SARAM" bootloader mode   */
 	BEGIN            : origin = 0x080000, length = 0x000002
 	//BEGIN            : origin = 0x08EFF0, length = 0x000002
   	RAMM0            : origin = 0x0000F5, length = 0x00030B


   	RAMLS3           : origin = 0x009800, length = 0x000800
   	RAM_BOOT 		 : origin = 0x00A000, length = 0x000058
   	RAMLS4           : origin = 0x00A058, length = 0x0007A8

   	//for cla program and data
   	RAM_CLARUN         : origin = 0x00A800, length = 0x001000
   	RAM_ISRRUN         : origin = 0x008400, length = 0x001400
  // RAMLS6           : origin = 0x00B000, length = 0x000800
   	RAMLS7           : origin = 0x00B800, length = 0x000800

   	RESET            : origin = 0x3FFFC0, length = 0x000002

	/* Flash sectors */
   	/* BANK 0 */
   	APPVEC 	   	     : origin = 0x082000, length = 0x000010
	FLASH_BANK0_SEC0 : origin = 0x082010, length = 0x00DFEE	/* on-chip Flash */
	/*FLASH_BANK0_SEC1 : origin = 0x08E000, length = 0x001FEF*/



	APPCRC	   		 : origin = 0x08FFFF, length = 0x000001

	BOOTVEC     : origin = 0x080002, length = 0x000052
	RAMFLASH    : origin = 0x080054, length = 0x000058

	LIB_FLASH   : origin = 0x0800AC, length = 0x000200	 //ZBW for memcpy.obj 20111213
	FLASH_BOOT  : origin = 0x0802AC, length = 0x001D53    /* on-chip FLASH */


   /* BANK 1 */
   FLASH_BANK1_SEC0  : origin = 0x090000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC1  : origin = 0x091000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC2  : origin = 0x092000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC3  : origin = 0x093000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC4  : origin = 0x094000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC5  : origin = 0x095000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC6  : origin = 0x096000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC7  : origin = 0x097000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC8  : origin = 0x098000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC9  : origin = 0x099000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC10 : origin = 0x09A000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC11 : origin = 0x09B000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC12 : origin = 0x09C000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC13 : origin = 0x09D000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC14 : origin = 0x09E000, length = 0x001000	/* on-chip Flash */
   FLASH_BANK1_SEC15 : origin = 0x09F000, length = 0x001000	/* on-chip Flash */

   //RAMRead
   RAMGS0        	 : origin = 0x00CA00, length =  0x0015F0
  // RAMGS0_ReadRam   : origin = 0x00D740, length =  0x000880


PAGE 1 : //dataspace

   BOOT_RSVD        : origin = 0x000002, length = 0x0000F3     /* Part of M0, BOOT rom will use this for stack */
   RAMM1            : origin = 0x000400, length = 0x000360     /* on-chip RAM block M1 */
   FLASH_API_ROM_RSVD : origin = 0x000760, length = 0x000020     /* Required by Flash API from ROM */

   CPUVariable      : origin = 0x008000, length = 0x00400


   //RAMLS1           : origin = 0x008800, length = 0x001000
  // RAMLS2           : origin = 0x009000, length = 0x000800

   IsrVariable      : origin = 0x00C000, length = 0x000400		/* ISR variable define */
   CanComVariable   : origin = 0x00C400, length = 0x000400     	/* CANCOM variable define */
   CanComVolVariable :origin = 0x00C800, length = 0x000200     	/* CANCOM Vol variable define */

   //RAMGS1			: origin = 0x00E200, length = 0x005e00
   //RAMGS1			: origin = 0x00E200, length = 0x003D00
   //RAMGS1Variable	: origin = 0x0012000, length = 0x001FF0
   RAMGS1			: origin = 0x00E200,  length = 0x001DF0
   RAMGS1Variable	: origin = 0x0010000, length = 0x003FF0

   CLAtoCPUMSGRAM   : origin = 0x001480, length = 0x000080
   CPUtoCLAMSGRAM   : origin = 0x001500, length = 0x000080
}


SECTIONS
{
   /* Allocate program areas: */
   .text            : > FLASH_BANK0_SEC0,     PAGE = 0, ALIGN(4)
   .cinit           : > FLASH_BANK0_SEC0,     PAGE = 0, ALIGN(4)
   /*.pinit           : > FLASH_BANK0_SEC0,     PAGE = 0, ALIGN(4)*/
   codestart        : > BEGIN       PAGE = 0, ALIGN(4)
   
    /* Allocate uninitialized data sections: */
   .cio             : > RAMGS1,      PAGE = 1
   .stack           : > RAMM1        PAGE = 1
   .ebss            : > RAMGS1       PAGE = 1
   .esysmem         : > RAMGS1       PAGE = 1

    /* Initialized sections go in Flash */
   .econst          : > FLASH_BANK0_SEC0,      PAGE = 0, ALIGN(4)
   .switch          : > FLASH_BANK0_SEC0,      PAGE = 0, ALIGN(4)
   .const           : > FLASH_BANK0_SEC0,      PAGE = 0, ALIGN(4)


   .reset           : > RESET,     PAGE = 0, TYPE = DSECT /* not used, */
   
    /*** Code Security Module Register Structures ***/
   IsrVariableFile	 : > IsrVariable,        PAGE = 1

   CanComVariableFile :> CanComVariable,        PAGE = 1
   CanComVolVariableFile :> CanComVolVariable,  PAGE = 1

	.init			   : LOAD = FLASH_BOOT      PAGE = 0
	{
		//.\SOURCE\Boot\f28004x_piectrl.obj (.text)
		//.\SOURCE\Boot\f28004x_pievect.obj (.text)
		//.\SOURCE\Boot\f28004x_cputimers.obj (.text)
		//.\SOURCE\Boot\f28004x_defaultisr.obj (.text)
		//.\SOURCE\Boot\f28004x_i2c.obj (.text)
		//.\SOURCE\Boot\f28004x_can.obj (.text)
		//.\SOURCE\Boot\f28004x_ecap.obj (.text)
		//.\SOURCE\Boot\f28004x_gpio.obj (.text)
		//.\SOURCE\Boot\f28004x_MemCopy.obj (.text)   ___ok
		//.\SOURCE\Boot\f28004x_sysctrl.obj (.text)
	}

   .cal  : > LIB_FLASH      PAGE = 0
   {
		rts2800_fpu32.lib <
		fd_mpy28.asm.obj
		exit.c.obj
		l_div28.asm.obj
		cpy_tbl.c.obj
		fd_tol28.asm.obj
		memcpy.c.obj
		fs_tofdfpu32.asm.obj
		args_main.c.obj
		_lock.c.obj
		u_div28.asm.obj
		pre_init.c.obj
		boot28.asm.obj
		startup.c.obj> (.text)

  }


  bootramfuncs        : LOAD = RAMFLASH,
                         RUN = RAM_BOOT,
                         LOAD_START(_BootRamfuncsLoadStart),
                         LOAD_END(_BootRamfuncsLoadEnd),
                         RUN_START(_BootRamfuncsRunStart),
                       	 PAGE = 0, ALIGN(4)

   IsrRamfuncs     : LOAD = FLASH_BANK0_SEC0,
                         RUN = RAM_ISRRUN,
                         LOAD_START(_IsrRamfuncsLoadStart),
                         LOAD_END(_IsrRamfuncsLoadEnd),
                         RUN_START(_IsrRamfuncsRunStart),
                        PAGE = 0, ALIGN(4)

    /* CLA specific sections */
    Cla1Prog        : LOAD = FLASH_BANK0_SEC0,
                      RUN = RAM_CLARUN,
                      LOAD_START(_Cla1funcsLoadStart),
                      LOAD_END(_Cla1funcsLoadEnd),
                      RUN_START(_Cla1funcsRunStart),

                      PAGE = 0, ALIGN(4)

    ClatoCpuVarFile   : > CLAtoCPUMSGRAM,     PAGE = 1
    CputoClaVarFile   : > CPUtoCLAMSGRAM,     PAGE = 1

    /*.TI.ramfunc      : LOAD = FLASH_BANK0_SEC0,
                      RUN = RAMLS4,
                      LOAD_START(_RamfuncsLoadStart),
                      LOAD_SIZE(_RamfuncsLoadSize),
                      LOAD_END(_RamfuncsLoadEnd),
                      RUN_START(_RamfuncsRunStart),
                      RUN_SIZE(_RamfuncsRunSize),
                      RUN_END(_RamfuncsRunEnd),
                      PAGE = 0, ALIGN(4)*/


   .scratchpad      : > RAMLS7,           PAGE = 0
   .bss_cla         : > RAMLS7,           PAGE = 0
   	Cla1DataRam     : > RAMLS7,           PAGE = 0

  /* csmpasswds          : > CSM_PWL_P0  PAGE = 0*/
   .appvec             : > APPVEC   PAGE = 0
   appcrc             : > APPCRC   PAGE = 0
   .bootvec            : > BOOTVEC  PAGE = 0
   FlashBoot           : > FLASH_BOOT   PAGE = 0, ALIGN(4)

   /*.const_cla       :  LOAD = FLASH_BANK0_SEC0,
                       RUN = RAMLS3,
                       RUN_START(_Cla1ConstRunStart),
                       LOAD_START(_Cla1ConstLoadStart),
                       LOAD_SIZE(_Cla1ConstLoadSize),
                       PAGE = 0*/

   /* Allocate IQ math areas: */
 //  IQmath			: > FLASH_BANK0_SEC0, PAGE = 0, ALIGN(4)            /* Math Code */
 //  IQmathTables		: > FLASH_BANK0_SEC0, PAGE = 0, ALIGN(4)
  /* The following section definition are for IQMATH */
   IQmath           : > FLASH_BANK0_SEC0,      PAGE = 0, ALIGN(4)
   IQmathTables     : > FLASH_BOOT,     PAGE = 0, ALIGN(4)


    /* ESNA protocal variants define*/
	/*{_Hardware_Version  = 0x07E0;}*//* 4-byte 0x07E0 and 0x07E1 */
	/*common variants*/

	{_ulENP_Lo_SN		= 0xE000;}/*Hi & Lo Extended Emerson Serial Number */
	{_ulENP_Lo_SN_H		= 0xE001;}/* Note7(A23-A16),Hi_SN(A15-A00),Lo_SN(A23-A00)*/
	{_ulENP_Hi_SN		= 0xE002;}
	{_ulENP_Hi_SN_H		= 0xE003;}/* all 48 bits */
	{_g_u16WriteNumber	= 0xE004;}
	{_g_u16I2CErrorType	= 0xE005;}
	{_g_u16MdlStatus    = 0xE006;}
	{_g_u16ActionReady	= 0xE007;} //ls 20111128



	{_u16WaitLoopCount	= 0xE008;} //空闲
	{_g_u16ReadEepromOk	= 0xE009;}
	{_RatedCurrent 		= 0xE00A;}
	{_u16AddIdentify	= 0xE00B;}
	{_g_u16MdlAddr 		= 0xE00C;}
	{_uiFromApplication  = 0xE00D;}



	{_g_fRdTemp				 = 0xE00E;}
	{_ECana32Temp			 = 0xE010;}
	{_g_lq10AcVrmsSampSysa   = 0xE012;}
	{_g_lq10AcVrmsSampSysb	 = 0xE014;}
	{_g_lq10VpfcSampSys      = 0xE016;}
	{_g_lq10VpfcConSys		 = 0xE018;}
	{_g_lq12VoltSampSysa	 = 0xE01A;}
	{_g_lq12VoltSampSysb	 = 0xE01C;}
	{_g_lq12VoltConSysa	 	 = 0xE01E;}
	{_g_lq12VoltConSysb	 	 = 0xE020;}
	{_g_lq10CurrSampSysa	 = 0xE022;}
	{_g_lq10CurrSampSysb	 = 0xE024;}
	{_g_lq10CurrConSysa	 	 = 0xE026;}
	{_g_lq10CurrConSysb	 	 = 0xE028;}
	{_g_lq10PowerConSysa	 = 0xE02A;}
	{_g_lq10PowerConSysb	 = 0xE02C;}
	{_g_lq10MdlVoltUp		 = 0xE02E;}
	{_g_lq10MdlTempUp		 = 0xE030;}
	{_g_lq10ReonTime         = 0xE032;}
	{_g_lq10OpenTime	     = 0xE034;}
	{_g_lq10RunTime			 = 0xE036;}
	{_g_lq10MaintainTimes    = 0xE038;}
	{_g_lq10AcCurrFt         = 0xE03A;}
	{_g_lq10WalkInTime       = 0xE03C;}
	{_g_lq10MdlPowerFt       = 0xE03E;}
	{_g_lq10SetPower         = 0xE040;}
	{_g_lq10MdlCurrFt        = 0xE042;}
	{_g_lq10SetLimit         = 0xE044;}
	{_g_lq10MdlVoltFt        = 0xE046;}
	{_g_lq10SetVolt          = 0xE048;}
	{_g_u16MdlCtrl	         = 0xE04A;}
	{_g_u16EpromWr			 = 0xE04B;}
	{_g_u16BarCode0H         = 0xE04C;}
	{_g_u16BarCode0L         = 0xE04D;}
	{_g_u16BarCode1H         = 0xE04E;}
	{_g_u16BarCode1L         = 0xE04F;}
	{_g_u16BarCode2H         = 0xE050;}
	{_g_u16BarCode2L         = 0xE051;}
	{_g_u16BarCode3H         = 0xE052;}
	{_g_u16BarCode3L         = 0xE053;}
	{_g_u16MaintainData0H    = 0xE054;}
	{_g_u16MaintainData0L    = 0xE055;}
	{_g_u16MaintainData1H    = 0xE056;}
	{_g_u16MaintainData1L    = 0xE057;}
	{_g_u16CharactData0H     = 0xE058;}
	{_g_u16CharactData0L     = 0xE059;}
	{_g_u16NodeId0H          = 0xE05A;}
	{_g_u16NodeId0L          = 0xE05B;}
	{_g_u16NodeId1H          = 0xE05C;}
	{_g_u16NodeId1L          = 0xE05D;}
	{_g_u16VersionNoHw       = 0xE05E;}
	{_g_u16TestPoint         = 0xE05F;}
	{_g_lq10MdlVolt          = 0xE060;}
    {_g_u16MdlCtrlExtend     = 0xE062;}
//-----------------------------------------------
    {_CpuTimer0		           = 0x8000;}

//------------------------------------------------------------
    {_g_u16LEDFlg            = 0xE063;}
	{_g_lq10PfcVoltChangeValue = 0xE064;}
	{_g_u16AlarmLogEnable    = 0xE066;}
    {_g_u16BrdTestFlag       = 0xE067;}
	{_g_u16ForceDriveFlag    = 0xE068;}

    {_g_u16WaitLoopCount     = 0xE069;}
	{_g_u16DcRippSWCtrl      = 0xE06A;}
    {_g_u16PfcStatus         = 0xE06B;}
    {_g_u16PfcCtrl           = 0xE06C;}

	{_g_u16PfcCtrlEx         = 0xE06D;} //空闲

	{_g_u16PFCphaseADJFLAG   = 0xE06E;} //空闲
    {_g_u16newPFCCORECOEF    = 0xE06F;} //空闲
	{_g_u16downloadflag      = 0xE070;}
	//{_g_i16FANADSTEP = 0x8fA2;}
	{_g_u16HVSDDebug         = 0xE071;}
//	{_g_u16VolSamCaliSuccess = 0x8fA4;}
    //PFC 频率调整
	{_g_u16PfcvoltChgFlag    = 0xE072;}
	{_g_u16DCDUBUGFlg        = 0xE073;}

   	{_g_lq10AcIrmsSampSysa   = 0xE074;}
	{_g_lq10AcIrmsSampSysb	 = 0xE076;}
    {_g_u16calflag	         = 0xE078;}
	{_g_u16MdlStatusEx       = 0xE079;}

	//从0x8FAC~0x8FFF之间的数据不会在InitRam中被初始化为0
	{_g_u16RepairEnable      = 0xE07A;} //空闲
	{_g_u16FlagBootLoader    = 0xE07B;}
	{_g_u16FlagPFCFd		 = 0xE07C;}//PFC侧强制下载标志位	0x8FA1

	{_g_u16INTTRIGVAL           = 0xE07D;}
	{_g_u16MdlStatusExtend      = 0xE07E;}
	{_g_i16FANADJVAL            = 0xE07F;}
	{_g_u16nousesss             = 0xE080;}
	{_g_u16PfcVoltDebug         = 0xE081;}
	{_g_u16ACMAXPolarity        = 0xE082;}
	{_g_i16FANADJVAL1           = 0xE083;}
	{_g_i16FANADJFLAG           = 0xE084;}
	{_g_u16FromApplication      = 0xE085;}
	{_g_u16PFCFreqSetVal        = 0xE086;}
	{_g_u16FanEnable	 		= 0xE087;}

	// for unrepairable function
	{_g_lq10VacOffsetVolt   	= 0xE088;}
	{_g_u16RealyTestFlag 		= 0xE08A;}
	{_g_u16UnPlugFlag 		    = 0xE08B;}


	{_g_sBootCanTxData 		    = 0xE08C;}
	{_g_sBootCanRxData		    = 0xE094;}
	{_g_u16IntOscCalibrateFlag    = 0xE09C;}
	{_g_i16FreDeviation			= 0xE09D;}
	{_g_u16EpromWrExtend			= 0xE09E;}


	{_Application_Entry_Point   = 0x082000;}
	{_ApplicationValidPointer	= 0x80008;}
	{_SendBlockRequestPointer	= 0x80038;}
	{_GetHeaderPointer			= 0x8003A;}
	{_GetDataBlockPointer		= 0x8003C;}
	{_StartTimerPointer			= 0x8003E;}
	{_TimeIsUpPointer			= 0x80040;}
	{_ProcessInquiryPointer		= 0x80042;}



	/***************************************************************************/

}

/*
//===========================================================================
// End of file.
//===========================================================================
*/
