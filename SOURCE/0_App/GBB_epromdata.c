

/*=============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : gbb_epromdata.c
 *  PURPOSE  : read and write data from/into eeprom	      
 *  
 *   HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A000           DSP               Created.   Pre-research 
 *	
 *============================================================================*/
#include "f28004x_device.h"		// DSP280x Headerfile Include File
#include  "App_cal.h"
#include  "App_vEventLog.h"
#include  "App_warnctrl.h"
#include  "GBB_epromdata.h"
#include <App_isr.h>
#include "CLAShared.h"
#include "GBB_constant.h"
#include <App_constant.h>
#include <Prj_macro.h>
#include <App_main.h>
#include "IQmathLib.h"
#include  "Prj_config.h"
#include "GBB_eprom_Interface.h"

/*******************************************************************************
*variables definition:these variables only can use in this file 														  
*******************************************************************************/

//extern  UINT16 g_u16WriteNumber;

UINT16  g_u16NodeId1LTmp;
UINT16  g_u16NodeId0H_TO_CONTROLLER;
UINT16  g_u16NodeId1L_TO_CONTROLLER;
UINT32	g_u32EpromWrState;


//ubitfloat g_lq10AcFrequencySampSysa;//AC frequency adjust coefficient a
//ubitfloat g_lq10AcFrequencySampSysb;//AC frequency adjust coefficient b
/*******************************************************************************
*globle variables definition:these variables  can use in all of the files 														  
*******************************************************************************/

/*******************************************************************************
*functions declare:these functions only can use in this file                                    
*******************************************************************************/
// write float in some one address
void vWriteFloatDataSig(unsigned int u16Address, ubitfloat fTemp);



//read float in three address and verify,this function is defined in 'FlashBoot'
ubitfloat fReadFloatDataThree(unsigned int u16Address);

void  ReadE2prom(void);

/*******************************************************************************
 *Function name: vLimitInit
 *Description:   adjust parameters, rectifier operation parameters and 
 *               information initialization
 *input:         void 
 *global vars:    
 *output:        void 
 *CALLED BY:    main() 
 ******************************************************************************/
void vLimitInit(void)
{
	ubitfloat	ftemp1;
	ubitfloat	ubitfTmp;
	UINT16		u16CodeTmp;

	ubitfTmp.lData = (INT32)0;

//		// for PFC and DCDC fail record, mhp 20120905
//	if (g_u32FailFlagPointer != 0xFFFFFFFF)
//	{
//		g_u16DisableOn = 1;
//	}

	//enable eeprom write
	mWrEnable();

	g_u16WriteNumber = 0;
	g_u16MdlStatus.all = 0x0001;  // initial status is OFF
    
	g_u16MdlStatusExtend.all = 0;
	g_u16MdlStatusLast.all 		 = 	g_u16MdlStatus.all;  //last 
	g_u16MdlStatusExtendLast.all =  g_u16MdlStatusExtend.all ;

	g_u16EpromWr.all  = 0;
	ftemp1.lData = 0;
    g_u16calflag = CAL_ALL_OK;//初始化校准OK，哪项不OK，在或一下标志位
	if (uiI2caReadDataPointer(EEPROM_CHK_ADDR) == I2C_SUCCESS)
	{
		ftemp1.lData = g_fRdTemp.lData;
	}

	// avoid watchdog reset, feed dog
	WatchDogKickPointer();
	
	if (ftemp1.lData == EPROM_READY_LONG)
	{
		//ReadE2promPointer();
		ReadE2prom();
	}
	else
	{
		ubitfTmp.uintdata[0].id = EPROM_READY_UINT;
		ubitfTmp.uintdata[1].id = EPROM_READY_UINT;


		u16CodeTmp = ucWriteFloatDataThree(EEPROM_CHK_ADDR,ubitfTmp);
		if (u16CodeTmp == NOVCOM)
		{
			u16CodeTmp = ucWriteFloatDataThree(EEPROM_CHK_ADDR,ubitfTmp);
		}
		//输入电量值初始值为0

		g_lPowerAccum.lData = 0; 
		ubitfTmp.lData = g_lPowerAccum.lData;
		u16CodeTmp = ucWriteFloatDataThree(INPUT_WATT_ACCUMULATE_ADDR,ubitfTmp);
		if (u16CodeTmp == NOVCOM)
		{
			u16CodeTmp = ucWriteFloatDataThree(INPUT_WATT_ACCUMULATE_ADDR,ubitfTmp);
		}

		g_lq10PowerAccumLow.lData=0;

    	//输入电量累计时间初始值为0
        g_lPowerAccumTime.lData = 0;
		ubitfTmp.lData = g_lPowerAccumTime.lData;
		u16CodeTmp = ucWriteFloatDataThree(INPUT_WATT_ACCUMULATE_TIME_ADDR,ubitfTmp);
		if (u16CodeTmp == NOVCOM)
		{
			u16CodeTmp = ucWriteFloatDataThree(INPUT_WATT_ACCUMULATE_TIME_ADDR,ubitfTmp);
		}
		WatchDogKickPointer(); 

		g_u16MdlStatus.bit.CALIBRFAIL = CALIBR_FAIL;
        g_u16calflag = g_u16calflag | EPROM_READ_FAIL;
        
		
		g_u16MdlCtrl.all = 0;
        g_u16MdlCtrlExtend.all = 0x2;//when DC input no alarm(default)；//国内G3 50A DC输入不关机

		g_lq10MdlVoltFt.lData = VDC_DEF;
		g_lq10SetVolt.lData = VDC_DEF;
		g_lq10MdlVoltUp.lData = VDCUP_SET_UP;  
		g_lq10MdlTempUp.lData = TEMP_UP_DEF; 

		g_lq10MdlPowerFt.lData = IQ10_POW_DEF_LIM_VALUE;
//		g_lq10MdlCurrFt.lData = IQ10_IDC_DEF_LIM_VALUE;
        g_lq10MdlCurrFt.lData = IQ10_IDC_DEF_LIM_VALUE;
		g_lq10AcCurrFt.lData = IQ10_IAC_DEF_VALUE; 

	   	//new mdl set a=1,b=0 csm 090925
        //default  calibrate coefficient
		g_lq10AcVrmsSampSysa.lData = VAC_RMS_SYSA_DF_VALUE; 
		g_lq10AcVrmsSampSysb.lData = VAC_RMS_SYSB_DF_VALUE; 


        g_lq10AcIrmsSampSysa.lData = IAC_RMS_SYSA_DF_VALUE; 
        g_lq10AcIrmsSampSysb.lData = IAC_RMS_SYSB_DF_VALUE; 

		g_lq10VpfcSampSys.lData = VPFC_SAM_SYS_DF_VALUE; 
		g_lq10VpfcConSys.lData = VPFC_CON_SYS_DF_VALUE; 

		g_lq12VoltSampSysa.lData = VDC_SAM_SYSA_DF_VALUE;
		g_lq12VoltSampSysb.lData = VDC_SAM_SYSB_DF_VALUE;

		IsrVars._u16VolSamCaliSuccess = 0; 
        IsrVars._i16VdcShortCoefB = SHORT_COEF_B_DEF; 

		g_lq12VoltConSysa.lData = VDC_CON_SYSA_DF_VALUE;
		g_lq12VoltConSysb.lData = VDC_CON_SYSB_DF_VALUE;
		g_lq10CurrSampSysa.lData = IDC_SAM_SYSA_DF_VALUE;
		g_lq10CurrSampSysb.lData = IDC_SAM_SYSB_DF_VALUE;

		g_lq10CurrConSysa.lData = IDC_CON_SYSA_DF_VALUE;
		g_lq10CurrConSysb.lData = IDC_CON_SYSB_DF_VALUE;
		g_lq10PowerConSysa.lData = POW_CON_SYSA_DF_VALUE;		
		g_lq10PowerConSysb.lData = POW_CON_SYSB_DF_VALUE;

//		g_lq10AcFrequencySampSysa.lData = AC_FRE_SYSA_DF_VALUE;
//		g_lq10AcFrequencySampSysb.lData = AC_FRE_SYSB_DF_VALUE;
		g_lq10WalkInTime.lData = WALKTIME10S;

		g_lq10ReonTime.lData = IQ10TIME300S;

		//g_lq10RunTime.lData = 0;
		

	//	g_u16NodeId0H = 0;
		g_u16NodeId0L = 1;	

    	g_u16NodeId0H = (g_u16NodeId0H & CLEAR_TYPE0_1) | SET_TYPE0_1_G3_4320W;
        g_u16NodeId0H_TO_CONTROLLER = (g_u16NodeId0H & CLEAR_TYPE0_1) | SET_TYPE0_1_G3_4320W_TO_CONTROLLER;

      	g_u16NodeId1L =(g_u16NodeId1LTmp & CLEAR_TYPE2)|SET_TYPE2_G3_4320W;
    	g_u16NodeId1L_TO_CONTROLLER =(g_u16NodeId1LTmp & CLEAR_TYPE2)|SET_TYPE2_G3_4320W_TO_CONTROLLER;


//把模块特征数据改到Datainit中写死
//		g_u16CharactData0H = MDL_CHARCT_H;   //need change csm090915
//		g_u16CharactData0L = MDL_CHARCT_L;

		//----------------------------for alarm log---------------------
		g_lq10RunTime.lData = 0;			
		g_lq10CurrentRunTime.lData = TIME_0S_1STIMER; 	// rectifier run time recording by self
		//第一次上电时间
		g_lq10BaseRunTime.lData = g_lq10CurrentRunTime.lData;
		g_u16AddressPointer = ALARM_START_ADDR;
		//----------------------------alarm end ------------------------

        //paul 20220228
        //Eileen 20210812
        g_u16CustomerNumberRx = 0xFF;
        g_u32SitNumberRx = 0x00FFFFFF;

        g_u16CustomerNumberTx = 0xFF;
        g_u32SitNumberTx = 0x00FFFFFF;


        g_u16MdlCtrl.bit.AntiTheftCanIntOff =0;
        g_u16MdlCtrl.bit.AntiTheftFlag =0;

        g_lq10AntiThiefDelayTime.lData = IQ10_AntiThiefDelay_DFT;
        g_u16AntiThiefDelaySet = (UINT16)(_IQ10mpy(g_lq10AntiThiefDelayTime.lData,720) ) ;//Time*60*12 base on 5s_base



	}

	//WP=1,disable write
	mWrDisable();			
}


/*******************************************************************************
 *Function name: ReadE2prom
 *Description:   Read default Data from EEPROM
 *input:         void
 *global vars:   void 
 *output:        void
 *CALLED BY:    vLimitInit() 
 ******************************************************************************/
//#pragma CODE_SECTION(ReadE2prom,"FlashBoot");
void  ReadE2prom(void)
{
	
	ubitfloat	lq10temp1,lq10temp2;
	UINT16		u16CodeTmp;

	UINT16  u16BlockAddr; //block address 
	UINT16  u16WordAddr;  //word address
	
	UINT16	u16AddressPointerTemp;

//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(VDC_SAM_SYSA_ADDR);
	u16CodeTmp = g_u16ReadEepromOk;
	lq10temp2 = fReadFloatDataThreePointer(VDC_SAM_SYSB_ADDR);

	if (((lq10temp1.lData >= IQ12_VDC_SAM_SYSA_LIMDN) 
		&& (lq10temp1.lData <= IQ12_VDC_SAM_SYSA_LIMUP)
		&& (lq10temp2.lData >= IQ12_VDC_SAM_SYSB_LIMDN)
		&& (lq10temp2.lData <= IQ12_VDC_SAM_SYSB_LIMUP))
		&& (u16CodeTmp == 0) && (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))

	{
		g_lq12VoltSampSysa.lData = lq10temp1.lData; 
		g_lq12VoltSampSysb.lData = lq10temp2.lData; 
        IsrVars._u16VolSamCaliSuccess = 1;

	}
	else
	{

		g_lq12VoltSampSysa.lData = VDC_SAM_SYSA_DF_VALUE;
		g_lq12VoltSampSysb.lData = VDC_SAM_SYSB_DF_VALUE;

        IsrVars._u16VolSamCaliSuccess = 0; 
		g_u16MdlStatus.bit.CALIBRFAIL = CALIBR_FAIL;
		g_u16calflag = g_u16calflag | VDC_SAM_CAL_FAIL;
	}
	
	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(VDC_CON_SYSA_ADDR);
	u16CodeTmp = g_u16ReadEepromOk;
	lq10temp2 = fReadFloatDataThreePointer(VDC_CON_SYSB_ADDR);


	if (((lq10temp1.lData >= IQ12_VDC_CON_SYSA_LIMDN) 
		&& (lq10temp1.lData <= IQ12_VDC_CON_SYSA_LIMUP)
		&& (lq10temp2.lData >= IQ12_VDC_CON_SYSB_LIMDN)
		&& (lq10temp2.lData <= IQ12_VDC_CON_SYSB_LIMUP))
		&& (u16CodeTmp == 0) && (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))

	{
		g_lq12VoltConSysa.lData = lq10temp1.lData; 
		g_lq12VoltConSysb.lData = lq10temp2.lData; 
	}
	else
	{ 
		g_lq12VoltConSysa.lData = VDC_CON_SYSA_DF_VALUE;
		g_lq12VoltConSysb.lData = VDC_CON_SYSB_DF_VALUE; 

		g_u16MdlStatus.bit.CALIBRFAIL = CALIBR_FAIL;
        g_u16calflag = g_u16calflag | VDC_CON_CAL_FAIL;
		
	}

	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(IDC_SAM_SYSA_ADDR);
	u16CodeTmp = g_u16ReadEepromOk;
	lq10temp2 = fReadFloatDataThreePointer(IDC_SAM_SYSB_ADDR);

	if (((lq10temp1.lData >= IQ10_IDC_SAM_SYSA_LIMDN) 
		&& (lq10temp1.lData <= IQ10_IDC_SAM_SYSA_LIMUP)
		&& (lq10temp2.lData >= IQ10_IDC_SAM_SYSB_LIMDN)
		&& (lq10temp2.lData <= IQ10_IDC_SAM_SYSB_LIMUP))
		&& (u16CodeTmp == 0) && (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		g_lq10CurrSampSysa.lData = lq10temp1.lData; 
		g_lq10CurrSampSysb.lData = lq10temp2.lData; 
	}
	else
	{

		g_lq10CurrSampSysa.lData = IDC_SAM_SYSA_DF_VALUE; 
		g_lq10CurrSampSysb.lData = IDC_SAM_SYSB_DF_VALUE; 
		 

		g_u16MdlStatus.bit.CALIBRFAIL = CALIBR_FAIL;
        g_u16calflag = g_u16calflag | IDC_SAM_CAL_FAIL;

	}
	
	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(IDC_CON_SYSA_ADDR);
	u16CodeTmp = g_u16ReadEepromOk;
	lq10temp2 = fReadFloatDataThreePointer(IDC_CON_SYSB_ADDR);

	if (((lq10temp1.lData >= IQ10_IDC_CON_SYSA_LIMDN) 
		&& (lq10temp1.lData <= IQ10_IDC_CON_SYSA_LIMUP)
		&& (lq10temp2.lData >= IQ10_IDC_CON_SYSB_LIMDN)
		&& (lq10temp2.lData <= IQ10_IDC_CON_SYSB_LIMUP))
		&& (u16CodeTmp == 0) && (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		g_lq10CurrConSysa.lData = lq10temp1.lData; 
		g_lq10CurrConSysb.lData = lq10temp2.lData; 
	}
	else
	{

		g_lq10CurrConSysa.lData = IDC_CON_SYSA_DF_VALUE; 
		g_lq10CurrConSysb.lData = IDC_CON_SYSB_DF_VALUE;

		g_u16MdlStatus.bit.CALIBRFAIL = CALIBR_FAIL;
		g_u16calflag = g_u16calflag | IDC_CON_CAL_FAIL;
	}
	
	//--------------------AC 电压有效值----------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(VAC_SAM_SYSA_ADDR);
	u16CodeTmp = g_u16ReadEepromOk;
	lq10temp2 = fReadFloatDataThreePointer(VAC_SAM_SYSB_ADDR);

	if (((lq10temp1.lData >= IQ10_VAC_RMS_SYSA_LIMDN) 
		&& (lq10temp1.lData <= IQ10_VAC_RMS_SYSA_LIMUP)
		&& (lq10temp2.lData >= IQ10_VAC_RMS_SYSB_LIMDN)
		&& (lq10temp2.lData <= IQ10_VAC_RMS_SYSB_LIMUP))
		&& (u16CodeTmp == 0) && (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))

	{
		g_lq10AcVrmsSampSysa.lData = lq10temp1.lData; 
		g_lq10AcVrmsSampSysb.lData = lq10temp2.lData; 
	}
	else
	{
		
		g_lq10AcVrmsSampSysa.lData = VAC_RMS_SYSA_DF_VALUE; 
		g_lq10AcVrmsSampSysb.lData = VAC_RMS_SYSB_DF_VALUE; 
		 
		g_u16MdlStatus.bit.CALIBRFAIL = CALIBR_FAIL;
		g_u16calflag = g_u16calflag | VAC_SAM_CAL_FAIL;
	}


		//--------------------AC 电流有效值----------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(IAC_SAM_SYSA_ADDR);
	u16CodeTmp = g_u16ReadEepromOk;
	lq10temp2 = fReadFloatDataThreePointer(IAC_SAM_SYSB_ADDR);

	if (((lq10temp1.lData >= IQ10_IAC_RMS_SYSA_LIMDN) 
		&& (lq10temp1.lData <= IQ10_IAC_RMS_SYSA_LIMUP)
		&& (lq10temp2.lData >= IQ10_IAC_RMS_SYSB_LIMDN)
		&& (lq10temp2.lData <= IQ10_IAC_RMS_SYSB_LIMUP))
		&& (u16CodeTmp == 0) && (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))

	{
		g_lq10AcIrmsSampSysa.lData = lq10temp1.lData; 
		g_lq10AcIrmsSampSysb.lData = lq10temp2.lData; 
	}
	else
	{
		
		g_lq10AcIrmsSampSysa.lData = IAC_RMS_SYSA_DF_VALUE; 
		g_lq10AcIrmsSampSysb.lData = IAC_RMS_SYSB_DF_VALUE; 
		 
	//	g_u16MdlStatus.bit.CALIBRFAIL = CALIBR_FAIL;

	}

	
	//--------------------------pfc sample---------------------------------
	lq10temp1 = fReadFloatDataThreePointer(VPFC_SAM_SYS_ADDR);

	if ((lq10temp1.lData >= IQ10_VPFC_SAM_SYS_LIMDN) 
		&& (lq10temp1.lData <= IQ10_VPFC_SAM_SYS_LIMUP)
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))

	{
		g_lq10VpfcSampSys.lData = lq10temp1.lData; 
	}
	else
	{
		g_lq10VpfcSampSys.lData = VPFC_SAM_SYS_DF_VALUE; 

		g_u16MdlStatus.bit.CALIBRFAIL = CALIBR_FAIL;

        g_u16calflag = g_u16calflag | VPFC_SAM_CAL_FAIL;
	}

	lq10temp1 = fReadFloatDataThreePointer(VPFC_CON_SYS_ADDR);

	if ((lq10temp1.lData >= IQ10_VPFC_CON_SYS_LIMDN) 
		&& (lq10temp1.lData <= IQ10_VPFC_CON_SYS_LIMUP)
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))

	{
		g_lq10VpfcConSys.lData = lq10temp1.lData; 
	}
	else
	{
		g_lq10VpfcConSys.lData = VPFC_CON_SYS_DF_VALUE; 

		g_u16MdlStatus.bit.CALIBRFAIL = CALIBR_FAIL;

		g_u16calflag = g_u16calflag | VPFC_CON_CAL_FAIL;
	}
	
	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(POW_CON_SYSA_ADDR);//64
	u16CodeTmp = g_u16ReadEepromOk;
	lq10temp2 = fReadFloatDataThreePointer(POW_CON_SYSB_ADDR);//68

	if (((lq10temp1.lData >= IQ10_POW_CON_SYSA_LIMDN)
		&& (lq10temp1.lData <= IQ10_POW_CON_SYSA_LIMUP)
		&& (lq10temp2.lData >= IQ10_POW_CON_SYSB_LIMDN)
		&& (lq10temp2.lData <= IQ10_POW_CON_SYSB_LIMUP))
		&& (u16CodeTmp == 0) && (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		g_lq10PowerConSysa.lData = lq10temp1.lData; 
		g_lq10PowerConSysb.lData = lq10temp2.lData; 
	}
	else
	{
		  
		 g_lq10PowerConSysa.lData = POW_CON_SYSA_DF_VALUE;
		 g_lq10PowerConSysb.lData = POW_CON_SYSB_DF_VALUE;

		g_u16MdlStatus.bit.CALIBRFAIL = CALIBR_FAIL;

		g_u16calflag = g_u16calflag | POW_CON_CAL_FAIL;
	}

	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(POW_DFT_ADDR);

	if ((lq10temp1.lData >= IQ10_POW_LIM_DN) 
		&& (lq10temp1.lData <= IQ10_POW_LIM_UP) 
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		g_lq10MdlPowerFt.lData = lq10temp1.lData; 
	}
	else
	{

		g_lq10MdlPowerFt.lData = IQ10_POW_DEF_LIM_VALUE;
	}
	g_lq10SetPower.lData = g_lq10MdlPowerFt.lData;

	//----------DCDC current offset  saved in e2prom---------------------------
	

	lq10temp1 = fReadFloatDataThreePointer(IDC_OFF_SET_ADDR);

	if((lq10temp1.lData >=  IQ10_IDC_OffSET_LIMDN)  
		&& (lq10temp1.lData <= IQ10_IDC_OffSET_LIMUP)
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		CputoClaVar._i16DCCurOffset = (int16)(lq10temp1.lData>>10); 

        CputoClaVar._i16DCCurOffsetTrue = (int16)(lq10temp1.lData>>10);
	}
	else
	{
		CputoClaVar._i16DCCurOffset = IDC_OFFSET_DF_VALUE; 

		CputoClaVar._i16DCCurOffsetTrue = IDC_OFFSET_DF_VALUE;

		g_u16MdlStatus.bit.CALIBRFAIL = CALIBR_FAIL;

		g_u16calflag = g_u16calflag | IDC_OFFSET_CAL_FAIL;
	}

	//----------DCDC current offset  saved in e2prom---------------------------
	lq10temp1 = fReadFloatDataThreePointer(SHORTB_ADDR);

	if((lq10temp1.lData >=  IQ10_SHORTB_LIMDN)  
		&& (lq10temp1.lData <= IQ10_SHORTB_LIMUP)
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
	  IsrVars._i16VdcShortCoefB	 = (int16)(lq10temp1.lData>>10); 
	}
	else
	{
	  IsrVars._i16VdcShortCoefB = SHORT_COEF_B_DEF; 

	   //	g_u16MdlStatus.bit.CALIBRFAIL = CALIBR_FAIL;

	   //	g_u16calflag = g_u16calflag | SHORTB_CAL_FAIL;
	}



	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(CURR_DFT_ADDR);

	if ((lq10temp1.lData >= IQ10_IDC_LIM_DN) 
		&& (lq10temp1.lData <= IQ10_IDC_LIM_UP) 
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))	
	{
		g_lq10MdlCurrFt.lData = lq10temp1.lData; 
	}
	else
	{

		g_lq10MdlCurrFt.lData = IQ10_IDC_DEF_LIM_VALUE; 


	}
	g_lq10SetLimit.lData = g_lq10MdlCurrFt.lData;

	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(ACCURR_DFT_ADDR);

	if ((lq10temp1.lData > IQ10_IAC_DN)
		&& (lq10temp1.lData < IQ10_IAC_UP) 
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))	
	{
		g_lq10AcCurrFt.lData = lq10temp1.lData; 
	}
	else
	{
		g_lq10AcCurrFt.lData =IQ10_IAC_DEF_VALUE;
		 
	}
	
	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(VDC_DFT_ADDR);	// note:range

	if ((lq10temp1.lData >= VDC_SET_DN)
		&& (lq10temp1.lData <= VDC_SET_UP) 
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS) )
	{
		g_lq10MdlVoltFt.lData = lq10temp1.lData; 
	}
	else
	{
		// default value:53.5V
		g_lq10MdlVoltFt.lData = VDC_DEF;
	}
	
	g_lq10SetVolt.lData = g_lq10MdlVoltFt.lData;

	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(VDC_HIGH_ADDR);

	if ((lq10temp1.lData >= VDCUP_SET_DN)
		&& (lq10temp1.lData <= VDCUP_SET_UP) 
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS) )
	{
		g_lq10MdlVoltUp.lData = lq10temp1.lData; 
	}
	else
	{
		g_lq10MdlVoltUp.lData = VDCUP_SET_UP; 
	}

	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(TEMP_HIGH_ADDR);

	if ((lq10temp1.lData >= TEMPUP_SET_DN)
		&& (lq10temp1.lData <= TEMPUP_SET_UP) 
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS) )
	{
		g_lq10MdlTempUp.lData = lq10temp1.lData; 
	}
	else
	{

		g_lq10MdlTempUp.lData = TEMP_UP_DEF;
	}
	

	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(RECT_ACT_ADDR);

	if (((lq10temp1.uintdata[0].id & MDLCTRL_NOT_EPROM_BIT ) == 0)    //0XCE3F
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS)) 
	{
		g_u16MdlCtrl.all = lq10temp1.uintdata[0].id; 
	}
	else
	{
		g_u16MdlCtrl.all = 0;
	}

	if (g_u16MdlCtrl.bit.ALLOWACOV)
	{
		IsrVars._u16PermitOverAc = PERMIT_ACOVER;
	}

	/*if(g_u16MdlCtrl.bit.FUSEB == 1)//20111130
	{
		g_u16MdlStatusExtend.bit.DCFUSEBROKEN = 1;
	}
       */

   //---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(AC_MDLCTRLEXTEND_ADDR);

	if (((lq10temp1.uintdata[0].id & 0xFFFC) == 0)    //ls 2013.8.9
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS)) 
	{
		g_u16MdlCtrlExtend.all = lq10temp1.uintdata[0].id;
	}
	else
	{
		g_u16MdlCtrlExtend.all = 0x2;//when DC input no alarm(default)；//国内G3 50A DC输入不关机
	}
	

	//---------------------------------------------------------------------
	
	lq10temp1 = fReadFloatDataThreePointer(REON_TIME_ADDR);

	if ((lq10temp1.lData >= IQ10TIME50S) 
		&& (lq10temp1.lData <= IQ10TIME300S) 
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		g_lq10ReonTime.lData = lq10temp1.lData; 
	}
	else
	{
		 g_lq10ReonTime.lData = IQ10TIME300S;
	}

	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(WALKIN_TIME_ADDR);

	if ((lq10temp1.lData >= WALKTIME8S)
		&& (lq10temp1.lData <= WALKTIME200S) 
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		g_lq10WalkInTime.lData = lq10temp1.lData; 
	}
	else
	{
		g_lq10WalkInTime.lData = WALKTIME10S; 
	}
	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(ORDER_TIME_ADDR);


	if ((lq10temp1.lData >= 0) && (lq10temp1.lData <= IQ10TIME10S)
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		g_lq10OpenTime.lData = lq10temp1.lData; 
	}
	else
	{
		g_lq10OpenTime.lData = 0; 
	}
	
	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(RUN_TIME_ADDR);


	if (( lq10temp1.lData >= 0)	&& (lq10temp1.lData <= IQ10TIME200000H)
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		g_lq10RunTime.lData = lq10temp1.lData;
	}
	else
	{
		g_lq10RunTime.lData = 0; 	// rectifier total run time
	}
	//---------------------------------------------------------------------	
	
	//---------------------------------------------------------------------	
	//DCOCP & PFCOVP counter
	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(DCOCPPFCOVPCNT_ADDR);//216
	//if read no value, clear ocp
	if(( lq10temp1.lData != 0xffffffff)&&(g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		IsrVars._u16DcdcOCPPfcOVP = lq10temp1.uintdata[0].id;
	}
	else
	{
		IsrVars._u16DcdcOCPPfcOVP = 0;
	}

	//---------------------------------------------------------------------	
	//   for alarm log   模块自己记录的运行时间
	//---------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(RUNTIME_RECORD_BYSELF);

	if (( lq10temp1.lData >= 0)	&& (lq10temp1.lData <= RUNTIME_UP_LIMIT)//
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		g_lq10CurrentRunTime.lData = lq10temp1.lData;
	}
	else
	{	
		g_lq10CurrentRunTime.lData = TIME_0S_1STIMER; 	// rectifier run time recording by self		
	}
	//上电时间初始时间
	g_lq10BaseRunTime.lData = g_lq10CurrentRunTime.lData;


	//最后一次故障记录到的对应的EEPROM 地址
	//--------------------------------------------------------------------
	lq10temp1 = fReadFloatDataThreePointer(RECORD_EEPROM_ADDR);

	if (( lq10temp1.lData >= ALARM_START_ADDR)	&& (lq10temp1.lData <= ALARM_END_ADDR)
		&& (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		//g_u16AddressPointer = (UINT16)(lq10temp1.lData + 6);

		u16AddressPointerTemp = lq10temp1.lData ;

		u16BlockAddr = (UINT16)((u16AddressPointerTemp >> 8) & 0x0007);

		u16WordAddr = (UINT16)(u16AddressPointerTemp & 0x00ff);

		if(u16WordAddr == BLOCK_END_WORD_ADDR )//246
		{
			if (u16BlockAddr == BLOCK_ADDR) //7
			{
				u16AddressPointerTemp =  ALARM_START_ADDR ; 
			}
			else 
			{			
				//如果一个故障记录压入堆栈后，EEPROM的地址增加10
				u16AddressPointerTemp = u16AddressPointerTemp + 10;			
			}
				
		}
		else if(u16WordAddr < BLOCK_END_WORD_ADDR)
		{
			//如果一个故障记录压入堆栈后，EEPROM的地址增加6
			u16AddressPointerTemp = u16AddressPointerTemp + 6;
				
		}
		else  //u16WordAddr > 246
		{
			if (u16BlockAddr == BLOCK_ADDR) //7
			{
				u16AddressPointerTemp =  ALARM_START_ADDR ; 
			}
			else 
			{			
				
				u16AddressPointerTemp = (UINT16)((u16BlockAddr +1) << 8);		
			}
		
		}	

		//<1024 or >2038 ,g_u16AddressPointer =1024
		g_u16AddressPointer = u16AddressPointerTemp;

	}
	else
	{	
		g_u16AddressPointer = ALARM_START_ADDR; //1024		
	}
    //-----------------------------------alarm log end --------------------

	lq10temp1 = fReadFloatDataThreePointer (ENP_LO_SN_ADDR);
	g_u16NodeId0H = lq10temp1.uintdata[1].id;
	g_u16NodeId0L = lq10temp1.uintdata[0].id;

	if ((g_u16NodeId0H == 0) && (g_u16NodeId0L == 0))
	{
		g_u16NodeId0L = 1;
	}
	
	g_u16NodeId0H = (g_u16NodeId0H & CLEAR_TYPE0_1) | SET_TYPE0_1_G3_4320W;
    g_u16NodeId0H_TO_CONTROLLER = (g_u16NodeId0H & CLEAR_TYPE0_1) | SET_TYPE0_1_G3_4320W_TO_CONTROLLER;
	
	lq10temp1 = fReadFloatDataThreePointer(ENP_HI_SN_ADDR);
	g_u16NodeId1H = lq10temp1.uintdata[1].id;
	g_u16NodeId1LTmp = lq10temp1.uintdata[0].id ;
	g_u16NodeId1L =(g_u16NodeId1LTmp & CLEAR_TYPE2)|SET_TYPE2_G3_4320W;
	g_u16NodeId1L_TO_CONTROLLER =(g_u16NodeId1LTmp & CLEAR_TYPE2)|SET_TYPE2_G3_4320W_TO_CONTROLLER;
	//---------------------------------------------------------------------

	lq10temp1 = fReadFloatDataThreePointer(VERSION_NO_ADDR);
	g_u16VersionNoHw = lq10temp1.uintdata[1].id;
	//---------------------------------------------------------------------	

	//模块特征类型数据直接写死，不再从EPROM里写入、读出
	//把模块特征数据改到Datainit中写死
	//	g_u16CharactData0H = MDL_CHARCT_H;  
	//	g_u16CharactData0L = MDL_CHARCT_L;
	
	//---------------------------------------------------------------------	

	lq10temp1 = fReadFloatDataThreePointer(BAR_CODE0_ADDR);
	g_u16BarCode0H = lq10temp1.uintdata[1].id;
	g_u16BarCode0L = lq10temp1.uintdata[0].id;

	lq10temp1 = fReadFloatDataThreePointer(BAR_CODE1_ADDR);
	g_u16BarCode1H = lq10temp1.uintdata[1].id;
	g_u16BarCode1L = lq10temp1.uintdata[0].id;

	lq10temp1 = fReadFloatDataThreePointer(BAR_CODE2_ADDR);
	g_u16BarCode2H = lq10temp1.uintdata[1].id;
	g_u16BarCode2L = lq10temp1.uintdata[0].id;

	lq10temp1 = fReadFloatDataThreePointer(BAR_CODE3_ADDR);
	g_u16BarCode3H = lq10temp1.uintdata[1].id;
	g_u16BarCode3L = lq10temp1.uintdata[0].id;
	//---------------------------------------------------------------------
	
	lq10temp1 = fReadFloatDataThreePointer(MEND_DATA0_ADDR);
	g_u16MaintainData0H = lq10temp1.uintdata[1].id;
	g_u16MaintainData0L = lq10temp1.uintdata[0].id;

	lq10temp1 = fReadFloatDataThreePointer(MEND_DATA1_ADDR);
	g_u16MaintainData1H = lq10temp1.uintdata[1].id;
	g_u16MaintainData1L = lq10temp1.uintdata[0].id;
	//---------------------------------------------------------------------	

    //AC 频率校准
	//---------------------------------------------------------------------
/*	lq10temp1 = fReadFloatDataThreePointer(AC_FRE_SYSA_ADDR);
	u16CodeTmp = g_u16ReadEepromOk;
	lq10temp2 = fReadFloatDataThreePointer(AC_FRE_SYSB_ADDR);

	if (((lq10temp1.lData >= IQ10_AC_FRE_SYSA_LIMDN) 
		&& (lq10temp1.lData <= IQ10_AC_FRE_SYSA_LIMUP)
		&& (lq10temp2.lData >= IQ10_AC_FRE_SYSB_LIMDN)
		&& (lq10temp2.lData <= IQ10_AC_FRE_SYSB_LIMUP))
		&& (u16CodeTmp == 0) && (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		g_lq10AcFrequencySampSysa.lData = lq10temp1.lData; 
		g_lq10AcFrequencySampSysb.lData = lq10temp2.lData; 
	}
	else
	{

		g_lq10AcFrequencySampSysa.lData = AC_FRE_SYSA_DF_VALUE; 
		g_lq10AcFrequencySampSysb.lData = AC_FRE_SYSB_DF_VALUE;

//		g_u16MdlStatus.bit.CALIBRFAIL = CALIBR_FAIL;
//		g_u16calflag = g_u16calflag | AC_FRE_CAL_FAIL;
	}*/

	 //输入电量计量
	//---------------------------------------------------------------------
	//输入电量值
	//计量满了，或则第一次BIN下载功率计量，电量初始值为0
	lq10temp1 = fReadFloatDataThreePointer(INPUT_WATT_ACCUMULATE_ADDR);
	if(( lq10temp1.lData != 0xffffffff)&&(g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{
		g_lPowerAccum.lData = lq10temp1.lData; 
		g_lq10PowerAccumLow.lData=((INT32)g_lPowerAccum.uintdata[0].id<<10);
	}
	else
	{
		g_lPowerAccum.lData = (INT32)0; 
		g_lq10PowerAccumLow.lData=((INT32)0);
	
	}

	//输入电量累计时间
	//计量满了，或则第一次BIN下载功率计量，电量累计初始值为0
	lq10temp2 = fReadFloatDataThreePointer(INPUT_WATT_ACCUMULATE_TIME_ADDR);
	if(( lq10temp2.lData != 0xffffffff)&&(g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
	{

     	 g_lPowerAccumTime.lData = lq10temp2.lData;
	}
	else
	{
		g_lPowerAccumTime.lData = (INT32)0; 

	
	}

    //paul 20220228
    //--------------for anti thief function----------------------------------
    lq10temp1 = fReadFloatDataThreePointer(MDL_CUSTOMER_NUM_ADDR);

    g_u16CustomerNumberTx = (lq10temp1.uintdata[1].id>>8);

    g_u32SitNumberTx= (((Uint32)(lq10temp1.uintdata[1].id&0x00FF))<<16)
                    |((Uint32)(lq10temp1.uintdata[0].id));


    lq10temp1 = fReadFloatDataThreePointer(ANTI_TITHIEF_DELAY);


    if ( (lq10temp1.lData <= IQ10_AntiThiefDelay_UP)
                && (lq10temp1.lData >= IQ10_AntiThiefDelay_DN)
                && (g_u16ReadEepromOk == READ_E2PROM_SUCCESS))
    {

        g_lq10AntiThiefDelayTime.lData = lq10temp1.lData + 1;
        g_u16AntiThiefDelaySet = (UINT16)(_IQ10mpy(g_lq10AntiThiefDelayTime.lData,720) ) ;//Time*60*12 base on 5s_base
    }
    else
    {

//      g_lq10AntiThiefDelayTime.lData = IQ10_AntiThiefDelay_DFT;
        g_u16AntiThiefDelaySet = (UINT16)(_IQ10mpy(g_lq10AntiThiefDelayTime.lData,720) ) ;//Time*60*12 base on 5s_base
    }


}



/*******************************************************************************
 *Function name: vWriteE2PROMSIG
 *Description:   the treatment of float write needed in the process of 
 *input:         communication u16Address:eeprom address
 *global vars:   void 
 *output:        void
 *CALLED BY:    vMtimerTreat()
 ******************************************************************************/
void	vWriteE2PROMSIG(void)
{
	ubitfloat	iq10wrtmp;
	
	iq10wrtmp.lData = (INT32)0;
	if (g_u16EpromWr.all)
	{
		if (g_u16EpromWr.bit.RUNTIME
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_RUNTIME))
		{
		
			g_u32EpromWrState = WR_RUNTIME;
			vWriteFloatDataSig(RUN_TIME_ADDR,g_lq10RunTime);

			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.RUNTIME = 0;
				g_u32EpromWrState = 0;
			}
		}
		else if (g_u16EpromWr.bit.MDLCTRL
			  //把MDLCTRL中的控制位写入EPROM中
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_MDLCTRL))
		{
			g_u32EpromWrState = WR_MDLCTRL;
			iq10wrtmp.uintdata[0].id = g_u16MdlCtrl.all & MDLCTRL_EPROM_BIT;  //0x31C0 //ls for fusebroken 20111130
			iq10wrtmp.uintdata[1].id = 0;
			vWriteFloatDataSig(RECT_ACT_ADDR,iq10wrtmp);			

			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.MDLCTRL = 0;
				g_u32EpromWrState = 0;
			}
		}
		else if (g_u16EpromWr.bit.INITCAN)
		{
			//InitECan();

			InitCanPointer();  //ls 20120525

 			g_u16EpromWr.bit.INITCAN = 0;
			g_u16MdlCtrl.bit.INITCAN = 0;
       	}
		else if (g_u16EpromWr.bit.VOLTUP
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_VOLTUP))
		{
			g_u32EpromWrState = WR_VOLTUP;
			vWriteFloatDataSig(VDC_HIGH_ADDR,g_lq10MdlVoltUp);

			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.VOLTUP = 0;
				g_u32EpromWrState = 0;
			}
		}
		else if (g_u16EpromWr.bit.VDCDFT
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_VDCDFT))
		{
			g_u32EpromWrState = WR_VDCDFT;
			vWriteFloatDataSig(VDC_DFT_ADDR,g_lq10MdlVoltFt);

			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.VDCDFT = 0;
				g_u32EpromWrState = 0;
			}
		}
		else if (g_u16EpromWr.bit.TEMPUP
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_TEMPUP))
		{
			g_u32EpromWrState = WR_TEMPUP;
			vWriteFloatDataSig(TEMP_HIGH_ADDR,g_lq10MdlTempUp);

			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.TEMPUP = 0;
				g_u32EpromWrState = 0;
			}
		}
		else if (g_u16EpromWr.bit.REONTIME
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_REONTIME))
		{
			g_u32EpromWrState = WR_REONTIME;
			vWriteFloatDataSig(REON_TIME_ADDR,g_lq10ReonTime);

			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.REONTIME = 0;
				g_u32EpromWrState = 0;
			}
		}
		else if (g_u16EpromWr.bit.WALKTIME
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_WALKTIME))
		{
			g_u32EpromWrState = WR_WALKTIME;
			vWriteFloatDataSig(WALKIN_TIME_ADDR,g_lq10WalkInTime);

			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.WALKTIME = 0;
				g_u32EpromWrState = 0;
			}
		}
		else if (g_u16EpromWr.bit.ORDERTIME
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_ORDERTIME))
		{
			g_u32EpromWrState = WR_ORDERTIME;
			vWriteFloatDataSig(ORDER_TIME_ADDR,g_lq10OpenTime);

			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.ORDERTIME = 0;
				g_u32EpromWrState = 0;
			}
		}
		else if (g_u16EpromWr.bit.POWERDFT
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_POWERDFT))
		{
			g_u32EpromWrState = WR_POWERDFT;
			vWriteFloatDataSig(POW_DFT_ADDR,g_lq10MdlPowerFt);

			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.POWERDFT = 0;
				g_u32EpromWrState = 0;
			}
		}
		else if (g_u16EpromWr.bit.CURRRDFT
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_CURRRDFT))
		{
			g_u32EpromWrState = WR_CURRRDFT;
			vWriteFloatDataSig(CURR_DFT_ADDR,g_lq10MdlCurrFt);

			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.CURRRDFT = 0;
				g_u32EpromWrState = 0;
			}
		}
		else if (g_u16EpromWr.bit.ACCURRRDFT
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_ACCURRRDFT))
		{
			g_u32EpromWrState = WR_ACCURRRDFT;
			vWriteFloatDataSig(ACCURR_DFT_ADDR,g_lq10AcCurrFt);

			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.ACCURRRDFT = 0;
				g_u32EpromWrState = 0;
			}
		}

		else if (g_u16EpromWr.bit.MDLCTRLEXTEND  
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_MDLCTRLEXTEND))
		{
			g_u32EpromWrState = WR_MDLCTRLEXTEND;
            iq10wrtmp.uintdata[0].id = g_u16MdlCtrlExtend.all&0x0003;   //ls 2013.8.9
			iq10wrtmp.uintdata[1].id = 0;
			vWriteFloatDataSig(AC_MDLCTRLEXTEND_ADDR,iq10wrtmp);			

			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.MDLCTRLEXTEND = 0;
				g_u32EpromWrState = 0;
			}
		}
		else if (g_u16EpromWr.bit.WATTACCUMULATE
	        //输入电量值
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_WATTACCUMULATE))
		{
			g_u32EpromWrState = WR_WATTACCUMULATE;
			vWriteFloatDataSig(INPUT_WATT_ACCUMULATE_ADDR,g_lPowerAccum);
			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.WATTACCUMULATE = 0;
				g_u32EpromWrState = 0;
			}
		} 
		else if (g_u16EpromWr.bit.WATTACCUMULATETIME
	        //输入电量累计时间
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_WATTACCUMULATETIME))
		{
			g_u32EpromWrState = WR_WATTACCUMULATETIME;
			vWriteFloatDataSig(INPUT_WATT_ACCUMULATE_TIME_ADDR,g_lPowerAccumTime);
			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWr.bit.WATTACCUMULATETIME = 0;
				g_u32EpromWrState = 0;
			}
		}
        //paul 20220228
        else if (g_u16EpromWr.bit.ANTITHIEFDELAY
            && ((g_u32EpromWrState == WR_FREE) || (g_u32EpromWrState == WR_WATTANTITHIEFNUM)))
        {

            g_u32EpromWrState = WR_WATTANTITHIEFNUM;
            vWriteFloatDataSig(ANTI_TITHIEF_DELAY,g_lq10AntiThiefDelayTime);
            if (g_u16WriteNumber == 0)
            {
                g_u16EpromWr.bit.ANTITHIEFDELAY = 0;
                g_u32EpromWrState = WR_FREE;
            }
        }


	} //if (g_u16EpromWr.all) ended

	if(g_u16EpromWrExtend.all)
	{
	    // save E2prom address pointer
		if(g_u16EpromWrExtend.bit.EEPROMROINTER   //eeprom pointer 
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_EEPROMROINTER))
		{
			g_u32EpromWrState = WR_EEPROMROINTER;
			iq10wrtmp.lData = (INT32)ALARM_START_ADDR;
			
			vWriteFloatDataSig(RECORD_EEPROM_ADDR,iq10wrtmp);							
			
			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWrExtend.bit.EEPROMROINTER = 0;
				g_u16ClearEepromOk = 2;
				g_u32EpromWrState = 0;
			}	
		}		
        
		//每隔半个小时（或AC切断）写三次EEPROM时间,每次的时间间隔为115ms
		else if(g_u16EpromWrExtend.bit.RECORDTIMESELF		
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_RECORDTIMESELF))
		{
			g_u32EpromWrState = WR_RECORDTIMESELF;
			vWriteFloatDataSig(RUNTIME_RECORD_BYSELF,g_lq10CurrentRunTime);//写三次


			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWrExtend.bit.RECORDTIMESELF = 0;

				g_lq10BaseRunTime.lData = g_lq10CurrentRunTime.lData;
				g_u32EpromWrState = 0;
			}
		}

		//DCOCP & PFCOVP 发生次数计数写入e2prom
		else if(g_u16EpromWrExtend.bit.DCOCP_PFCOVP	
			&& (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_DCOCP_PFCOVP))
		{
			g_u32EpromWrState = WR_DCOCP_PFCOVP;	
			iq10wrtmp.uintdata[0].id = IsrVars._u16DcdcOCPPfcOVP;
			iq10wrtmp.uintdata[1].id = 0;
			vWriteFloatDataSig(DCOCPPFCOVPCNT_ADDR,iq10wrtmp);//写三次

			if (g_u16WriteNumber == 0)
			{
				g_u16EpromWrExtend.bit.DCOCP_PFCOVP = 0;
				g_u32EpromWrState = 0;
			}
		}
        else if(g_u16EpromWrExtend.bit.INTOSC
            && (g_u32EpromWrState == 0 || g_u32EpromWrState == WR_INTOSC_CALIBRATE))
        {
            g_u32EpromWrState = WR_INTOSC_CALIBRATE;
            iq10wrtmp.uintdata[0].id = g_u16IntOscCalibrateStatus;
            iq10wrtmp.uintdata[1].id = 0;
            vWriteFloatDataSig(INTOSC_SET_ADDR,iq10wrtmp);//写三次
            if (g_u16WriteNumber == 0)
            {
                g_u16EpromWrExtend.bit.INTOSC = 0;
                g_u32EpromWrState = 0;
            }
        }
		
	}	
}


void vIntOscfromE2(void)
{
    ubitfloat   lq10temp;
    //内部晶振校准
    lq10temp = fReadFloatDataThreePointer(INTOSC_SET_ADDR);
    if(( lq10temp.lData != 0xffffffff)&&(g_u16ReadEepromOk == READ_E2PROM_SUCCESS)&&(lq10temp.lData>=3)&&(lq10temp.lData<=7))
    {

        g_u16IntOscCalibrateStatus = lq10temp.uintdata[0].id;
    }
    else
    {
        g_u16IntOscCalibrateStatus = 5;//lq10temp2.uintdata[0];
    }
    vIntOscSet(g_u16IntOscCalibrateStatus);
}

//===========================================================================
// No more.
//===========================================================================


