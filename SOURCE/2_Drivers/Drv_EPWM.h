/*=============================================================================*
 *         Copyright(c) 2004-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : HD415CZ
 *
 *  FILENAME : gbb_CanDriver.h
 *  PURPOSE  : rectifier communicate with controller and other rectifiers	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *     2008-11-25      A000           zoulx             Created.   Pre-research
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *    detail information refer to gbb_main.h      
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                               		     DESCRIPTION 
 *============================================================================*/

#ifndef Drv_EPWM_H
#define Drv_EPWM_H
 //
// PWM functions
//
extern void InitEPwm(void);

#endif
