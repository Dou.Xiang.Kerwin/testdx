//###########################################################################
//
// FILE:   f28004x_I2C.c
//
// TITLE:  f28004x I2C Initialization & Support Functions.
//
//###########################################################################
// $TI Release: f28004x Support Library v3.05.00.00 $
// $Release Date: Thu Oct 18 15:50:26 CDT 2018 $
// $Copyright:
// Copyright (C) 2014-2018 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Texas Instruments Incorporated nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// $
//###########################################################################

//
// Included Files
//
#include "f28004x_device.h"
#include "App_main.h"
void InitI2CGpio(void);
UINT16 uiI2caReadData2 (UINT16 u16Address);
UINT16 uiI2caWriteData2(UINT16 u16Address, ubitfloat fTemp);
// I2CA_Init - Initialize I2CA settings
//
void InitI2C(void)
{
    InitI2CGpio();

    I2caRegs.I2CMDR.bit.IRS = 0;
    I2caRegs.I2CSAR.all = 0x0050;     // Slave address - EEPROM control code

    I2caRegs.I2CPSC.all = 5;         // Prescaler - need 7-12 Mhz on module clk     10M
    I2caRegs.I2CCLKL = 65;            // NOTE: must be non zero
    I2caRegs.I2CCLKH = 65;             // NOTE: must be non zero
    I2caRegs.I2CIER.all = 0x00;       // Enable SCD & ARDY __interrupts

    //I2caRegs.I2CMDR.all = 0x0020;     // Take I2C out of reset
    I2caRegs.I2CMDR.bit.IRS = 1;                                 // Stop I2C when suspended

    I2caRegs.I2CFFTX.all = 0x6000;    // Enable FIFO mode and TXFIFO
    I2caRegs.I2CFFRX.all = 0x2040;    // Enable RXFIFO, clear RXFFINT,

    //return;
    I2caRegs.I2CCNT=0x0000;
    I2caRegs.I2CSTR.all=0xFFFF;
}


void InitI2CGpio()
{

   EALLOW;
/* Enable internal pull-up for the selected pins */
// Pull-ups can be enabled or disabled disabled by the user.
// This will enable the pullups for the specified pins.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAPUD.bit.GPIO26 = 0;    // Enable pull-up for GPIO27 (SDAA)
    GpioCtrlRegs.GPAPUD.bit.GPIO27 = 0;    // Enable pull-up for GPIO26 (SCLA)

/* Set qualification for selected pins to asynch only */
// This will select asynch (no qualification) for the selected pins.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAQSEL2.bit.GPIO27 = 3;  // Asynch input GPIO27 (SDAA)
    GpioCtrlRegs.GPAQSEL2.bit.GPIO26 = 3;  // Asynch input GPIO26 (SCLA)

/* Configure SCI pins using GPIO regs*/
// This specifies which of the possible GPIO pins will be I2C functional pins.
// Comment out other unwanted lines.

    GpioCtrlRegs.GPAGMUX2.bit.GPIO27 = 2;
    GpioCtrlRegs.GPAMUX2.bit.GPIO27 = 3;   // Configure GPIO27 for SDAA operation
    GpioCtrlRegs.GPAGMUX2.bit.GPIO26 = 2;
    GpioCtrlRegs.GPAMUX2.bit.GPIO26 = 3;   // Configure GPIO26 for SCLA operation


    EDIS;
    GpioDataRegs.GPASET.bit.GPIO25 = 1; //WP=1,disable write
}
/*******************************************************************************
 *Function name: uiI2caWriteData
 *Description:   Write Data to EEPROM
 *input:         u16Address:eeprom address
 *               fTemp: the data that will be write to the eeprom address
 *global vars:   void
 *output:        I2C_STP_NOT_READY_ERROR, I2C_BUS_BUSY_ERROR,I2C_SUCCESS
 *CALLED BY:    ucWriteFloatDataThree();fReadFloatDataThree();
 *              vWriteFloatDataSig();
 ******************************************************************************/
#pragma CODE_SECTION(uiI2caWriteData,"FlashBoot");
UINT16 uiI2caWriteData(UINT16 u16Address, ubitfloat fTemp)
{

    UINT16 u16CodeMsg1;

    //WatchDogKickPointer();  //_uiI2caWriteData1Pointer

    u16CodeMsg1 = uiI2caWriteData2(u16Address, fTemp);
    //u16CodeMsg1 = uiI2caWriteData1Pointer(u16Address, fTemp);

    //DELAY_US(TIME5MS_CPUCLK);


    //WatchDogKickPointer();

    //u16CodeMsg1 = u16CodeMsg1 | uiI2caWriteData2(u16Address, fTemp);
    //u16CodeMsg1 = u16CodeMsg1 | uiI2caWriteData2Pointer(u16Address, fTemp);

      return u16CodeMsg1;
}
/*******************************************************************************
 *Function name: uiI2caWriteData1
 *Description:   Write Data to EEPROM
 *input:         u16Address:eeprom address
 *               fTemp: the data that will be write to the eeprom address
 *global vars:   void
 *output:        I2C_STP_NOT_READY_ERROR, I2C_BUS_BUSY_ERROR,I2C_SUCCESS
 *CALLED BY:    ucWriteFloatDataThree();fReadFloatDataThree();
 *              vWriteFloatDataSig();
 ******************************************************************************/

#pragma CODE_SECTION(uiI2caWriteData1,"FlashBoot");
UINT16 uiI2caWriteData1(UINT16 u16Address, ubitfloat fTemp)
{
    UINT16 u16Count;

    // Wait until the STP bit is cleared from any previous master communication.
    // Clearing of this bit by the module is delayed after the SCD bit is set.
    // If this bit is not checked prior to initiating a new message, the I2C
    // could get confused.
    u16Count = 0;

    while (I2caRegs.I2CMDR.bit.STP == 1)
    {
        u16Count ++ ;
        if (u16Count > 6000)
        {
            I2caRegs.I2CMDR.bit.STP = 0;
            g_u16I2CErrorType = I2C_ERROR;
            return I2C_STP_NOT_READY_ERROR;
        }
    }

    // Setup slave address
    I2caRegs.I2CSAR.all = (UINT16)(((u16Address >> 8) & 0x0007) | 0x0050);

    // Check if bus busy
    /*
    if (I2caRegs.I2CSTR.bit.BB == 1)
    {
        return I2C_BUS_BUSY_ERROR;

    }
    */
    // Check if bus busy
    u16Count = 0;
    while(I2caRegs.I2CSTR.bit.BB == 1)
    {
            WatchDogKickPointer();
            u16Count ++ ;
            if(u16Count > 6000)
            {
                I2caRegs.I2CMDR.bit.IRS = 0;
                //I2caRegs.I2CSTR.bit.BB=1;
                asm("    NOP    " );
                asm("    NOP    " );
                asm("    NOP    " );
                asm("    NOP    " );
                asm("    NOP    " );
                I2caRegs.I2CMDR.bit.IRS = 1;
                return I2C_BUS_BUSY_ERROR;
            }
    }

    // Setup number of bytes to send: MsgBuffer + Address
    //I2caRegs.I2CCNT = 5;
     I2caRegs.I2CCNT = 3;

    // Setup data to send
    I2caRegs.I2CDXR.all = (UINT16)(u16Address & 0x00ff);

    I2caRegs.I2CDXR.all = fTemp.uintdata[0].bitdata.highchar;
    I2caRegs.I2CDXR.all = fTemp.uintdata[0].bitdata.lowchar;

    //I2caRegs.I2CDXR.all = fTemp.uintdata[1].bitdata.highchar;
    //I2caRegs.I2CDXR.all = fTemp.uintdata[1].bitdata.lowchar;

    // Send start as master transmitter
    I2caRegs.I2CMDR.all = 0x6E20;

    return I2C_SUCCESS;
}

/*******************************************************************************
 *Function name: uiI2caWriteData2
 *Description:   Write Data to EEPROM
 *input:         u16Address:eeprom address
 *               fTemp: the data that will be write to the eeprom address
 *global vars:   void
 *output:        I2C_STP_NOT_READY_ERROR, I2C_BUS_BUSY_ERROR,I2C_SUCCESS
 *CALLED BY:    ucWriteFloatDataThree();fReadFloatDataThree();
 *              vWriteFloatDataSig();
 ******************************************************************************/
#pragma CODE_SECTION(uiI2caWriteData2,"FlashBoot");

UINT16 uiI2caWriteData2(UINT16 u16Address, ubitfloat fTemp)
{
    UINT16 u16Count;

    // Wait until the STP bit is cleared from any previous master communication.
    // Clearing of this bit by the module is delayed after the SCD bit is set.
    // If this bit is not checked prior to initiating a new message, the I2C
    // could get confused.
    u16Count = 0;

    while (I2caRegs.I2CMDR.bit.STP == 1)
    {
        u16Count ++ ;
        if (u16Count > 6000)
        {
            I2caRegs.I2CMDR.bit.STP = 0;
            g_u16I2CErrorType = I2C_ERROR;
            return I2C_STP_NOT_READY_ERROR;
        }
    }

    // Setup slave address
    I2caRegs.I2CSAR.all = (UINT16)(((u16Address >> 8) & 0x0007) | 0x0050);

    // Check if bus busy
    /*
    if (I2caRegs.I2CSTR.bit.BB == 1)
    {
        return I2C_BUS_BUSY_ERROR;

    }
    */
    // Check if bus busy
    u16Count = 0;
    while(I2caRegs.I2CSTR.bit.BB == 1)
    {
            WatchDogKickPointer();
            u16Count ++ ;
            if(u16Count > 6000)
            {
                I2caRegs.I2CMDR.bit.IRS = 0;
                //I2caRegs.I2CSTR.bit.BB=1;
                asm("    NOP    " );
                asm("    NOP    " );
                asm("    NOP    " );
                asm("    NOP    " );
                asm("    NOP    " );

                I2caRegs.I2CMDR.bit.IRS = 1;
                return I2C_BUS_BUSY_ERROR;
            }
     }

    // Setup number of bytes to send: MsgBuffer + Address
    I2caRegs.I2CCNT = 5;
     //I2caRegs.I2CCNT = 3;

    // Setup data to send
    I2caRegs.I2CDXR.all = (UINT16)(u16Address& 0x00ff);

    I2caRegs.I2CDXR.all = fTemp.uintdata[0].bitdata.highchar;
    I2caRegs.I2CDXR.all = fTemp.uintdata[0].bitdata.lowchar;

    I2caRegs.I2CDXR.all = fTemp.uintdata[1].bitdata.highchar;
    I2caRegs.I2CDXR.all = fTemp.uintdata[1].bitdata.lowchar;

    // Send start as master transmitter
    I2caRegs.I2CMDR.all = 0x6E20;

    return I2C_SUCCESS;
}


/*******************************************************************************
 *Function name: uiI2caReadData1
 *Description:   Read Data from EEPROM
 *input:         u16Address:eeprom address
 *global vars:   void
 *output:        I2C_STP_NOT_READY_ERROR, I2C_BUS_BUSY_ERROR,I2C_ERROR
 *CALLED BY:    ucWriteFloatDataThree();fReadFloatDataThree();
 *              vWriteFloatDataSig();
 ******************************************************************************/
#pragma CODE_SECTION(uiI2caReadData,"FlashBoot");
UINT16 uiI2caReadData(UINT16 u16Address)
{

    UINT16  u16CodeMsg2;

     //WatchDogKickPointer(); //uiI2caReadData1Pointer

     u16CodeMsg2 =uiI2caReadData2(u16Address);
    //u16CodeMsg2 =uiI2caReadData1Pointer(u16Address);

     //DELAY_US(TIME5MS_CPUCLK);


     //WatchDogKickPointer();

     //u16CodeMsg2 = u16CodeMsg2 | uiI2caReadData2(u16Address);
     //u16CodeMsg2 = u16CodeMsg2 | uiI2caReadData2Pointer(u16Address);

      return u16CodeMsg2;

}

/*******************************************************************************
 *Function name: uiI2caReadData1
 *Description:   Read Data from EEPROM
 *input:         u16Address:eeprom address
 *global vars:   void
 *output:        I2C_STP_NOT_READY_ERROR, I2C_BUS_BUSY_ERROR,I2C_ERROR
 *CALLED BY:    ucWriteFloatDataThree();fReadFloatDataThree();
 *              vWriteFloatDataSig();
 ******************************************************************************/

#pragma CODE_SECTION(uiI2caReadData1,"FlashBoot");

UINT16 uiI2caReadData1(UINT16 u16Address)
{
    UINT16  i,u16Count,au16RdMsgBuffer[4];  //8bits EEPROM data

    u16Count = 0;

    while (I2caRegs.I2CMDR.bit.STP == 1)
    {
        u16Count ++ ;
        if(u16Count > 6000)
        {
            I2caRegs.I2CMDR.bit.STP = 0;
            g_u16I2CErrorType = I2C_ERROR;
            return I2C_STP_NOT_READY_ERROR;
        }
    }

    I2caRegs.I2CSAR.all = (UINT16)(((u16Address >> 8) & 0x0007)| 0x0050);

    // Check if bus busy
    /*
    if (I2caRegs.I2CSTR.bit.BB == 1)
    {
        return I2C_BUS_BUSY_ERROR;

    }
    */
    // Check if bus busy
    u16Count = 0;
    while(I2caRegs.I2CSTR.bit.BB == 1)
    {
            WatchDogKickPointer();
            u16Count ++ ;
            if(u16Count > 6000)
            {
                I2caRegs.I2CMDR.bit.IRS = 0;
                //I2caRegs.I2CSTR.bit.BB=1;
                asm("    NOP    " );
                asm("    NOP    " );
                asm("    NOP    " );
                asm("    NOP    " );
                asm("    NOP    " );

                I2caRegs.I2CMDR.bit.IRS = 1;
                return I2C_BUS_BUSY_ERROR;
            }
    }

    I2caRegs.I2CCNT = 1;
    I2caRegs.I2CDXR.all = (UINT16)(u16Address & 0x00ff);
    I2caRegs.I2CMDR.all = 0x6620;       // Send data to setup EEPROM address
    I2caRegs.I2CSTR.bit.SCD=1;

    u16Count = 0;

    while (I2caRegs.I2CSTR.bit.ARDY!=1)
    {
        u16Count ++ ;

        if(u16Count > 4000)             // on GZ test, is 173, about 0.95ms
        {
            g_u16I2CErrorType = I2C_ERROR;
            return I2C_ERROR;
        }

    }

    u16Count = 0;
    I2caRegs.I2CCNT = 2;
    I2caRegs.I2CMDR.all = 0x6C20;       // Send data to setup EEPROM address

    while (I2caRegs.I2CSTR.bit.SCD != 1)
    {
        u16Count ++ ;
        if(u16Count > 6000)             //on GZ test, is 298,  about 1.4ms
        {
            g_u16I2CErrorType = I2C_ERROR;
            return I2C_ERROR;
        }
    }

    for (i = 0; i < 2; i++)
    {
        au16RdMsgBuffer[i] = I2caRegs.I2CDRR.all;
    }

    g_fRdTemp.uintdata[0].bitdata.highchar = au16RdMsgBuffer[0]&0x00ff;
    g_fRdTemp.uintdata[0].bitdata.lowchar = au16RdMsgBuffer[1]&0x00ff;
    //g_fRdTemp.uintdata[1].bitdata.highchar = au16RdMsgBuffer[2]&0x00ff;
    //g_fRdTemp.uintdata[1].bitdata.lowchar = au16RdMsgBuffer[3]&0x00ff;

    return I2C_SUCCESS;
}

/*******************************************************************************
 *Function name: uiI2caReadData2
 *Description:   Read Data from EEPROM
 *input:         u16Address:eeprom address
 *global vars:   void
 *output:        I2C_STP_NOT_READY_ERROR, I2C_BUS_BUSY_ERROR,I2C_ERROR
 *CALLED BY:    ucWriteFloatDataThree();fReadFloatDataThree();
 *              vWriteFloatDataSig();
 ******************************************************************************/
#pragma CODE_SECTION(uiI2caReadData2,"FlashBoot");

UINT16 uiI2caReadData2 (UINT16 u16Address)
{
    UINT16  i,u16Count,au16RdMsgBuffer[4];  //8bits EEPROM data

    u16Count = 0;

    while (I2caRegs.I2CMDR.bit.STP == 1)
    {
        u16Count ++ ;
        if(u16Count > 6000)
        {
            I2caRegs.I2CMDR.bit.STP = 0;
            g_u16I2CErrorType = I2C_ERROR;
            return I2C_STP_NOT_READY_ERROR;
        }
    }

    I2caRegs.I2CSAR.all = (UINT16)(((u16Address >> 8) & 0x0007)| 0x0050);

    // Check if bus busy

    /*
    if (I2caRegs.I2CSTR.bit.BB == 1)
    {
        return I2C_BUS_BUSY_ERROR;

    }
    */
    // Check if bus busy
    u16Count = 0;
    while(I2caRegs.I2CSTR.bit.BB == 1)
    {
            WatchDogKickPointer();
            u16Count ++ ;
            if(u16Count > 6000)
            {
                I2caRegs.I2CMDR.bit.IRS = 0;
                //I2caRegs.I2CSTR.bit.BB=1;
                asm("    NOP    " );
                asm("    NOP    " );
                asm("    NOP    " );
                asm("    NOP    " );
                asm("    NOP    " );

                I2caRegs.I2CMDR.bit.IRS = 1;
                return I2C_BUS_BUSY_ERROR;
            }
    }


    I2caRegs.I2CCNT = 1;
    I2caRegs.I2CDXR.all = (UINT16)(u16Address & 0x00ff);
    I2caRegs.I2CMDR.all = 0x6620;       // Send data to setup EEPROM address
    I2caRegs.I2CSTR.bit.SCD=1;

    u16Count = 0;

    while (I2caRegs.I2CSTR.bit.ARDY!=1)
    {
        u16Count ++ ;

        if(u16Count > 4000)             // on GZ test, is 173, about 0.95ms
        {
            g_u16I2CErrorType = I2C_ERROR;
            return I2C_ERROR;
        }

    }

    u16Count = 0;
    I2caRegs.I2CCNT = 4;
    I2caRegs.I2CMDR.all = 0x6C20;       // Send data to setup EEPROM address

    while (I2caRegs.I2CSTR.bit.SCD != 1)
    {
        u16Count ++ ;
        if(u16Count > 6000)             //on GZ test, is 298,  about 1.4ms
        {
            g_u16I2CErrorType = I2C_ERROR;
            return I2C_ERROR;
        }
    }

    for (i = 0; i < 4; i++)
    {
        au16RdMsgBuffer[i] = I2caRegs.I2CDRR.all;
    }

    g_fRdTemp.uintdata[0].bitdata.highchar = au16RdMsgBuffer[0]&0x00ff;
    g_fRdTemp.uintdata[0].bitdata.lowchar = au16RdMsgBuffer[1]&0x00ff;
    g_fRdTemp.uintdata[1].bitdata.highchar = au16RdMsgBuffer[2]&0x00ff;
    g_fRdTemp.uintdata[1].bitdata.lowchar = au16RdMsgBuffer[3]&0x00ff;

    return I2C_SUCCESS;
}

//
// End of file
//
