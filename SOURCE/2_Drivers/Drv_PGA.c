/*
 * f28004x_pga.c
 *
 *  Created on: 2019��11��28��
 *      Author: Lzy
 */
//
// initPGA - Configure PGA3&5 gain
//
#include "f28004x_device.h"


void InitPGA(void)
{
    EALLOW;

    //
    // Set a gain of 3 to PGA3
    //
    Pga3Regs.PGACTL.bit.GAIN = 3;//gain=24

    //
    // No filter resistor for output
    //
    Pga3Regs.PGACTL.bit.FILTRESSEL = 0;

    //
    // Enable PGA3
    //
    Pga3Regs.PGACTL.bit.PGAEN = 1;

    //
    Pga5Regs.PGACTL.bit.GAIN = 3;

    //
    // No filter resistor for output
    //
    Pga5Regs.PGACTL.bit.FILTRESSEL = 0;

    //
    // Enable PGA5
    //
    Pga5Regs.PGACTL.bit.PGAEN = 1;

    EDIS;
}
