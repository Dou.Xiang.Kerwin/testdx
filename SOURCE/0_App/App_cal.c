/*=============================================================================*
 *         Copyright(c) 2008-20012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : gbb_cal.c
 *  PURPOSE  : power calculation              
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A000           DSP               Created.   Pre-research 
 *      
 *============================================================================*/

#include "f28004x_Device.h"             // DSP280x Headerfile Include File
#include "IQmathLib.h"
#include  "App_vAdcTreatment.h"
#include  "App_cal.h"
#include  "App_warnctrl.h"
#include <App_cancom.h>
#include <App_isr.h>
#include "GBB_constant.h"
#include <App_constant.h>
#include <App_main.h>
#include "GBB_epromdata.h"
#include  "App_vEventLog.h"
#include  "Prj_config.h"
/*******************************************************************************
*variables definition:these variables only can use in this file                                                                                                                   
*******************************************************************************/
static ubitfloat s_lq10TempLimitPower;      //temperature limit power coefficient
static ubitfloat s_lq10M1TempLimitPower;    //M1 temperature limit power coefficient
static ubitfloat s_lq10ACLimitPower;        //AC limit power coefficient
static ubitfloat s_lq10AcCurrLimitPower;    //AC current limit power coefficient
static ubitfloat s_lq10AcOldVolt;           //record the last used AC volt
static ubitfloat s_lq10PowAccumPerMin;      //(power accumulate /115ms)
static ubitfloat s_lq10ShortLimt;           //short limit current coefficient
static ubitfloat s_lq10DspTempLimitPower;    //dsp temperature limit power coefficient
static UINT16    s_u16DspTempHandleTimer;
static UINT16    s_u16AcHvHoldOnTimer;
static UINT16    s_u16OpShortHandleTimer;
static UINT16    s_u16KWHCnt;	            // time interval 115ms
//static UINT16    s_u16LvLowLimit;
//static UINT16 s_u16CurrlimitFlag;
static ubitfloat g_lq10LoadPercent;        //负载百分比,计算THD补偿使用
static UINT16 s_u16AcCurrLimFlag;
static int16 s_i16CurrLimitCnt;//paul

/*******************************************************************************
*globle variables definition:these variables  can use in all of the files                                                                                                                 
*******************************************************************************/
ubitfloat g_lq10SetVoltTrue;      // actual dcvolt set by SCU+
ubitfloat g_lq10SetLimitTrue;     // actual curr limit set by SCU+
ubitfloat g_lq10SetPowerTrue;     // actual power limit set by SCU+
ubitfloat g_lq10SetPowerWant;     // actual power limit coefficient of module
ubitfloat g_lq10SetPowerWant2;    // actual power limit coefficient of module，用于限流计算
ubitfloat g_lq10MdlLimit;         // actual rectifir current limit for diaplay
ubitfloat g_lq10MdlLimitDisp;	  // actual rectifir current limit for diaplay
ubitfloat g_lq10MdlLimitCurrent;  // actual rectifir current limit Current
ubitfloat g_lq10SetPfcVolt;       // wanted pfcvolt set by SCU+
ubitfloat g_lq10SetPfcVoltTrue;   // actual pfcvolt set by SCU+
ubitfloat g_lq10PfcVoltRegulate;  // the regulation value of pfc volt
ubitfloat g_lq10VpfcVdcRatio;     // 母线电压和输出电压的比值

//ubitfloat g_lq10PfcVoltUpValue;   // 母线电压调压策略的上限值，轻载重载切换
ubitfloat g_lq10TempUse;          // 计算环温回滞的变量，用于环温限功率等。
ubitfloat g_lq10MdlVoltUse;       // 计算输出电压回滞的变量，用于输入高压，输出低压限功率等。
ubitfloat g_lq10AcVoltUse;        // 计算输入电压回滞的变量，用于输入电压限功率等。
ubitfloat g_lq20SourceAdjSysa;
ubitfloat g_lq10SourceAdjSysb;

INT32     g_i32PowerRate;
INT32     g_i32CurrRate;
INT32     g_i32CurrDisMax;
INT32     g_i32CurrLimtMax;
INT32     g_i32CurrLimtMaxExtra;
UINT16    g_u16LimitStatus;       // rectifier current/power limit status
//INT16     g_i16VPFCCALFlag;

ubitfloat g_lPowerAccum;  //input Power accumulate
ubitfloat g_lq10PowerAccumLow; //input Power accumulate low 16 (Q10)
ubitfloat g_lSCUGetWattAccum; // scu get powe
ubitfloat g_lPowerAccumTime;//input Power accumulate timer
UINT16    g_u16KWHCalState;// input Power accumulate state
UINT16    g_u16MdlPowerAcuumClearFlg;// input Power accumulate clear flg
UINT16    g_u16Restflag;
UINT16    g_u16CurrCompenEnable;
static INT32     i32SourceAdjDelta;
static UINT16    s_u16PfcMdlVolt;
UINT16    g_u16PfcvoltChgFlag_high;

UINT16 g_u16CurrLimitStatusFlag; //paul 20210610
ubitfloat    g_lq10MdlCurrLast;

/*******************************************************************************
*functions declare:these functions only can use in this file                                    
*******************************************************************************/
//dcdc voltage step regulate
static  void    vDcdcVoltStepRegu(void);

//dcdc voltage reference cal
static  void    vDcdcVrefCal(void);

//DCdc current limit set  step in step
static  void    vDcdcCurrStepRegu(void);

//output power step regulate
static  void    vPowerStepRegu(void);

//Temperature limit power  coefficient calculate
static  void    vTempLimPowerControl(void);

//M1 Board Temperature limit power  coefficient calculate
static  void  vM1TempLimPowerControl(void);

//dsp Board Temperature limit power  coefficient calculate
static  void  vMDSPTempLimPowerControl(void);

//AC limit power coefficient calculate
static  void    vAcLimPowerControl(void);

//AC limit power coefficient calculate
static  void    vAc_Lim_Power_Cal(ubitfloat lq10Acin_Volt);

//DC limit power coefficient calculate
static  void    vDc_Lim_Power_Cal(ubitfloat lq10Dcin_Volt);

//AC curr limit power coefficient calculate
static  void    vAcCurrLimPowerControl(void);

//power limit value calculate
static  void    vPowerLimCal(void);

//power limit warning handle
static  void    vPowerLimWarningHandle(void);

//output short handle 
static  void    vOpShortHandle(void);
static  void    vOpShortHandle_2900(void);
static  void    vOpShortHandle_3000e3(void);
static  void    vOpShortHandle_3500e3(void);
static  void    vOpShortHandle_4320e3(void);

//Current limit cal
static  void    vCurrLimCal(void);

//PFC voltage step regulate
static  void    vPfcVoltStepRegu(void);

//pfc voltage referrence cal
static  void    vPfcVoltVrefCal(void);

//pfc volt regulate Algorithm
static  void    vPfcVoltReguAlgorithm(void); 

//输入电压的回滞计算，用于高低压限功率等
static  void vInVolHysCal(void);  
//环境温度的回滞计算，用于环境温度限功率等。         
static  void vAmbTempHysCal(void);
//输出电压的回滞计算，用于高压输入，低压输出限功率及短路回缩等 
static  void  vOutVolHysCal(void); 

//Based DC Vol cal DC minum frequency 
static  void  vDcVolCalFreqLowLim(void);

//2000e temperature limit power curve

static void   vTempLimPower_2900(ubitfloat i32UseTmp);
//static void vTempLimPower_3200(ubitfloat i32UseTmp);
//static void vTempLimPower_3500(ubitfloat i32UseTmp);
//static void vTempLimPower_4000e3(ubitfloat i32UseTmp);
static void   vTempLimPower_Default_4320(ubitfloat i32UseTmp);
//high voltage input and low voltage output limit power
//static void vVACHighVDCLow(void); 

//计算中断里的 PFC校准后的保护值。
static	void  vPFCProtectPointCal(void);

static void   vVpfcVdcRatioCal(void);
// power accumulate calc
static void   vKWHCal(void);

static  void  vPfcPhaseComplement(void);
/*******************************************************************************
 *Function name: vCircuitCal()  
 *Description :  Vref, Iref, power limit calculation, draw back if short etc
 *input:         void                                
 *global vars:   g_u16RunFlag.bit.CAL: run flag of circuit cal,115ms once 
 *output:        void 
 *CALLED BY:    main() 
 *******************************************************************************/
void  vCircuitCal(void)
{       
    //according to G250 v102  csm  091221
    if(g_u16ActionReady == NORMAL_RUN_STATE)
    {
    	if(IsrVars._u16NormalRunTimer  >= TIME150MS_ISRTIMER)//658*16*14.28us=150ms
		{
			IsrVars._u16NormalRunTimer  = 0;
			IsrVars._u16DcOpened = 1;
		}
	}
    else
	{
		IsrVars._u16NormalRunTimer  = 0;
		IsrVars._u16DcOpened = 0;	
	}
    
    //according to G250 v102  csm  091221
    if((g_u16ActionReady >= NORMAL_RUN_STATE) && (IsrVars._u16MinTimer >= TIME20MS_ISRTIMER)) 
    { //20ms
        IsrVars._u16MinTimer = 0 ;
       
        //pfc voltage referrence step regulate
        vPfcVoltStepRegu();     
       
        //Pfc voltage reference cal
        vPfcVoltVrefCal();


        	
        //dcdc voltage step regulate
        vDcdcVoltStepRegu();
       
        //dcdc voltage reference cal
        vDcdcVrefCal();

        //paul ---20210610
        if((IsrVars._u16ChoiseCon)&&(s_u16AcCurrLimFlag == 1))
        {
            s_i16CurrLimitCnt++;
            if(s_i16CurrLimitCnt>=50)
            {
                g_u16CurrLimitStatusFlag = 1;
                s_i16CurrLimitCnt = 50;
            }
        }
        else
        {
            s_i16CurrLimitCnt=0;
            g_u16CurrLimitStatusFlag = 0;
        }


    }
     //用于在中断里改善均流的算法
    if((!g_u16MdlStatus.bit.OFFSTAT)&&(g_u16MdlStatusExtend.bit.PARAFLAG)&&(g_u16ActionReady>=NORMAL_RUN_STATE))
	{
		IsrVars._u16LoadDeltaFlag = 1;
	}
	else
	{
		IsrVars._u16LoadDeltaFlag = 0;
	}        
	
	
    if(g_u16ActionReady && g_u16RunFlag.bit.CAL)
    {//115ms
        g_u16RunFlag.bit.CAL = 0;
        
        //pfc volt regulate Algorithm  1
        vPfcVoltReguAlgorithm();     
        
        //dcdc current step regulate                            
        vDcdcCurrStepRegu();
        
        //PFC CUR/VOL PHASE delay complement
        vPfcPhaseComplement();

        //output power step regulate
        vPowerStepRegu();       

        vInVolHysCal();   //输入电压的回滞计算，用于高低压限β实取?
         
        vAmbTempHysCal();//环境温度的回滞计算，用于环境温度限功率等。

        vOutVolHysCal(); //输出电压的回滞计算，用于高压输入，低压输出限功率及短路回缩等   
        
        //****************power  limit**************************************
        //tempeture power limit control  2
        vTempLimPowerControl();

        //M1 board tempeture power limit control  2
        vM1TempLimPowerControl();
        
        //AC Volt power limit control   3
        vAcLimPowerControl();
        
        vMDSPTempLimPowerControl();//115

        //AC input current power limit control  4
        vAcCurrLimPowerControl();

		//AC high DC low output limit powr
    	//vVACHighVDCLow(); //暂时屏蔽高压输入、低压输出
        
        //power limit cal   5
        vPowerLimCal();
        
        // power limit warning handle 6
        vPowerLimWarningHandle();               
        	
        //****************current limit*************************************
        // draw back if short
        vOpShortHandle();
        
        //Current limit cal
        vCurrLimCal();

        //Based Real  output DC Vol value cal DC minum frequency
		vDcVolCalFreqLowLim();

       //计算中断里的 PFC校准后的保护值
        vPFCProtectPointCal();

       //计算PFC母线和DC输出电压比值；
		vVpfcVdcRatioCal();

        //power accumulate calc；
		vKWHCal();
    }
}

/*******************************************************************************
 *Function name: vDcdcVoltStepRegu()  
 *Description:   dcdc voltage step regulate
 *input:         void
 *global vars:   g_iq10SetVolt: wanted dcvolt set by SCU+                                 
 *               g_iq10SetVoltTrue :actual dcvolt set by SCU+
 *output:        void
 *CALLED BY:     vCircuitCal() 
 ******************************************************************************/
void vDcdcVoltStepRegu(void)
{
    if(g_lq10SetVoltTrue.lData < g_lq10SetVolt.lData - DCVOLT_REGU_STEP)//0.1V
    {
    	g_lq10SetVoltTrue.lData += DCVOLT_REGU_STEP;
    }
    else if(g_lq10SetVoltTrue.lData > g_lq10SetVolt.lData + DCVOLT_REGU_STEP)
    {
    	g_lq10SetVoltTrue.lData -= DCVOLT_REGU_STEP;
    }
    else
    {
    	g_lq10SetVoltTrue.lData = g_lq10SetVolt.lData;
    }       
}

/*******************************************************************************
 *Function name: vDcdcVrefCal()  
 *Description :  DCDC voltage vref calculate
 *input:         void 
 *global vars:   g_iq10SetVoltTrue :actual dcvolt set by SCU+
 *               g_i16CurrDelta:volt adjust delta cal by current adjust 
 *               g_u16VoltAdjDelta: volt adjust delta by scu                
 *               IsrVars._iVdcSet:Dcdc control referrence voltage
 *output:        void
 *CALLED BY:    vCircuitCal() 
 ******************************************************************************/
void vDcdcVrefCal(void)
{
    longunion lLimitTmp;
    INT16     i16DeltaTmp;
//	INT16     i16LoadRegDelta;
   
//      for balance current
/*
    if ((g_u16MdlStatusExtend.bit.PARAFLAG == 1) 
       && (g_u16MdlStatus.bit.OFFSTAT == 0))  
	{		   
		  
		 //采用延时更小的未经平均值滤波的模块输出电流做负载调整率可以降低模块之间的电流振荡
		 //见XiangBo文档
		i16LoadRegDelta = (INT16)(_IQ10mpy((IDC_RATE_50PCT - g_lq10MdlCurrNoFilter.lData),
		                                            CURR_SHARE_DELTAK)>>10); 
		                                    

		if ( i16LoadRegDelta  > LOAD_REG_DELTA_UP)				
		{
			i16LoadRegDelta  = LOAD_REG_DELTA_UP;
		}
		else if ( i16LoadRegDelta  < LOAD_REG_DELTA_DN)		
		{
			i16LoadRegDelta  = LOAD_REG_DELTA_DN;
		}
	
	}
	else
	{
		i16LoadRegDelta  = 0;
	}
  //end
 
    i16DeltaTmp = i16LoadRegDelta + g_i16CurrDelta - g_i16SelfDxDelta     //ls 20111130
        + (INT16)g_u16VoltAdjDelta - (INT16)VOLT_ADJUST_DEF;
*/
    /*源调整率不足,根据输入电压进行补偿*/

   if ((g_u16MdlStatusExtend.bit.PARAFLAG == 0) && (g_u16MdlStatus.bit.OFFSTAT == 0))
    {
       if((g_lq10AcVolt.lData > VAC_220V) && (g_lq10MdlVolt.lData < VDC_45V) && (g_lq10MdlCurr.lData > IDC_70A) )
        {
//   i32SourceAdjDelta = (0.2761 * vac - 56.807)/1000
           i32SourceAdjDelta = _IQ10mpy(g_lq20SourceAdjSysa.lData, g_lq10AcVolt.lData);
           i32SourceAdjDelta = (i32SourceAdjDelta>>10) - g_lq10SourceAdjSysb.lData;
        }
       else if((g_lq10AcVolt.lData < VAC_218V) || (g_lq10MdlVolt.lData > VDC_46V) || (g_lq10MdlCurr.lData < IDC_68A))
       {
           i32SourceAdjDelta = 0;
       }

    }
   else
   {
       i32SourceAdjDelta = 0;
   }

   if(i32SourceAdjDelta < 0)
   {
       i32SourceAdjDelta = 0;
   }
   if(i32SourceAdjDelta > VDC_0V05 )
   {
       i32SourceAdjDelta = VDC_0V05;
   }


   i16DeltaTmp =  g_i16CurrDelta - g_i16SelfDxDelta
           + (INT16)g_u16VoltAdjDelta - (INT16)VOLT_ADJUST_DEF + (INT16)i32SourceAdjDelta;  //xBO


    //cal set volt after adjust
    lLimitTmp.lData = (g_lq12VoltConSysb.lData >> 2) 
            + (_IQ10mpy(g_lq10SetVoltTrue.lData, g_lq12VoltConSysa.lData) >> 2); 
    	
    //set volt transform to control referrence volt 
    lLimitTmp.lData = _IQ10mpy(lLimitTmp.lData, IQ10_DEF_VDCFACTOR);
    lLimitTmp.lData = (lLimitTmp.lData >> 10) + (INT32)i16DeltaTmp;
    
    //control referrence volt up limit
    if(lLimitTmp.lData > VDC_AD_MAX)
    {
        lLimitTmp.lData = VDC_AD_MAX;
    }
    
    IsrVars._i16VdcSetTmp = lLimitTmp.iData.iLD; 
    
}

/*******************************************************************************
 *Function name:  vDcdcCurrStepRegu()  
 *Description :   DCdc current limit set  step in step
 *input:          void 
 *global vars:    g_iq10SetLimit:wanted curr limit set by SCU+
 *                g_iq10SetLimitTrue:actual curr limit set by SCU+
 *output:         void
 *CALLED BY:      vCircuitCal() 
 ******************************************************************************/
 void vDcdcCurrStepRegu(void)
{
    if(g_lq10SetLimitTrue.lData < g_lq10SetLimit.lData - DCCURR_REGU_STEP)
    {
    	g_lq10SetLimitTrue.lData += DCCURR_REGU_STEP;
    }
    else if(g_lq10SetLimitTrue.lData > g_lq10SetLimit.lData + DCCURR_REGU_STEP)
    {
    	g_lq10SetLimitTrue.lData -= DCCURR_REGU_STEP;
    }
    else
    {
    	g_lq10SetLimitTrue.lData = g_lq10SetLimit.lData;
    }
}

/*=============================================================================*
 *Function name:  vPowerStepRegu()  
 *Description :   Power limit  set step in step 
 *input:          void 
 *global vars:    g_iq10SetPower :wanted power limit set by SCU+                              
 *                    g_iq10SetPowerTrue:actual power limit set by SCU+
 *output:         void
 *CALLED BY:      vCircuitCal() 
 *============================================================================*/
void vPowerStepRegu(void)
{
    //0.1
    if(g_lq10SetPowerTrue.lData < g_lq10SetPower.lData - POW_REGU_STEP)
    {
        g_lq10SetPowerTrue.lData += POW_REGU_STEP;
    }
    else if(g_lq10SetPowerTrue.lData > g_lq10SetPower.lData + POW_REGU_STEP)
    {
        g_lq10SetPowerTrue.lData -= POW_REGU_STEP;
    }
    else
    {
        g_lq10SetPowerTrue.lData = g_lq10SetPower.lData;
    }
}
/*******************************************************************************
 *Function name: vMDSPTempLimPowerControl()
 *Description : DSP Temperature limit power  calculate
 *input:         void
 *global vars:
 *
 *output:
 *CALLED BY:     vCircuitCal()
 ******************************************************************************/
static  void  vMDSPTempLimPowerControl(void)
{
   // ubitfloat g_lq10Tempdelta;
    //s_lq10DspTempLimitPower
    //g_lq10TempUse.lData >= TEMP_50C
    //g_lq10TempSensor.lData
    //ubitfloat g_lq10Tempdelta;


    //g_lq10Tempdelta.lData = g_lq10TempSensor.lData - g_lq10TempAmbiDisp.lData;

    //paul-20220210
    //    if ((g_lq10AcVolt.lData <= VAC_195V)&&(g_lq10TempAmbiDisp.lData >= TEMP_30C)&&(g_lq10Tempdelta.lData>=TEMP_53C))
    if ((g_lq10AcVolt.lData <= VAC_195V)&&(g_lq10TempSensor.lData>=TEMP_110C))
    {
         s_u16DspTempHandleTimer++;
         if( s_u16DspTempHandleTimer >= TIME10S_CAL)
        {
            s_u16DspTempHandleTimer = TIME10S_CAL;

            //if(g_lq10Tempdelta.lData>=TEMP_56C)
            // {
                 //s_lq10DspTempLimitPower.lData=  IQ10_DSP_LIM_POW_86PCT;//3700w
                 s_lq10DspTempLimitPower.lData=  IQ10_DSP_LIM_POW_93PCT;//4000w--paul20220210

            //}
             /* else if((g_lq10Tempdelta.lData>=TEMP_50C)&&(g_lq10Tempdelta.lData<TEMP_56C))
             {
                //(10-0.14t)/3
               s_lq10DspTempLimitPower.lData = 10240- (_IQ10mpy(g_lq10Tempdelta.lData, ((INT32)286))>>1);
               s_lq10DspTempLimitPower.lData=  _IQ10div(s_lq10DspTempLimitPower.lData, (((INT32)3)<<10));
             }
             else
             {
                s_lq10DspTempLimitPower.lData=  IQ10_TEMP_LIM_POW_MAX;
             }
             */
         }
         else
         {
            s_lq10DspTempLimitPower.lData=  IQ10_TEMP_LIM_POW_MAX;
         }

    }
   // else if ((g_lq10AcVolt.lData >= VAC_205V)|| (g_lq10TempAmbiDisp.lData < TEMP_25C)||(g_lq10Tempdelta.lData<TEMP_50C))
    else if ((g_lq10AcVolt.lData >= VAC_205V)|| (g_lq10TempSensor.lData < TEMP_100C))
    {
        // step by step out, 115s timebase step=13w/4320=3cnt(Q10)

        s_lq10DspTempLimitPower.lData+=3;
        if(s_lq10DspTempLimitPower.lData>=IQ10_TEMP_LIM_POW_MAX)
        {
            s_lq10DspTempLimitPower.lData=  IQ10_TEMP_LIM_POW_MAX;
        }

        s_u16DspTempHandleTimer = 0;
    }

    //zhang0130
    if(IsrVars._u16SoftStartEndTimer<=300)//(g_u16ActionReady < NORMAL_RUN_STATE)
    {
        s_lq10DspTempLimitPower.lData = IQ10_TEMP_LIM_POW_MAX;
    }
    //zhang0130

    //paul20220301
    if(g_lq10TempSensor.lData >= TEMP_120C)
    {
        s_lq10DspTempLimitPower.lData = IQ10_DSP_LIM_POW_86PCT;
    }
    //paul20220301

    if(s_lq10DspTempLimitPower.lData > IQ10_TEMP_LIM_POW_MAX)
    {
        s_lq10DspTempLimitPower.lData = IQ10_TEMP_LIM_POW_MAX;
    }

    if(s_lq10DspTempLimitPower.lData < IQ10_DSP_LIM_POW_86PCT)
    {
        s_lq10DspTempLimitPower.lData = IQ10_DSP_LIM_POW_86PCT;
    }

    //g_lq10DspTempLimitPowerTest.lData= s_lq10DspTempLimitPower.lData ;
}



/*******************************************************************************
 *Function name: vTempLimPowerControl()  
 *Description :  Temperature limit power  calculate 
 *input:         void
 *global vars:   g_iq10TempAmbi:rectifier intake temperature                                 
 *               
 *output:        s_iq10TempLimitPower:temperature limit power coefficient 
 *CALLED BY:     vCircuitCal() 
 ******************************************************************************/
void vTempLimPowerControl(void)
{    

      //G2 2900w :45C->2900W  55C->2320W 60C -->1450W 65C -->0
     if(((g_u16PowerMixedSystemType & MIX_TYPE_2900W)==MIX_TYPE_2900W)
		  && (g_u16MdlCtrl.bit.NoDeratingFlag == 0))
		{
		 vTempLimPower_2900(g_lq10TempUse);
		}
		/*
	//G2 3200w :45C->3200W  55C->2900W 65C -->2320W 70C -->1450W 75C ->0
	else if(((g_u16PowerMixedSystemType & MIX_TYPE_3200W)==MIX_TYPE_3200W)
	  		&& (g_u16MdlCtrl.bit.NoDeratingFlag == 0))
		{
		 vTempLimPower_3200(g_lq10TempUse);
		 }
		 */

/*
	//G2 3500w :45C->3500W  55C->3100W 65C -->2700W 70C -->1450W 75C ->0	
	else if(((g_u16PowerMixedSystemType & MIX_TYPE_3500W)==MIX_TYPE_3500W)
	  		&& (g_u16MdlCtrl.bit.NoDeratingFlag == 0))
    	{
		  vTempLimPower_3500(g_lq10TempUse);
		  }

    //G2 4000w :45C->4000W  55C->3500W 65C -->3000W 70C -->1500W 75C ->0	
    else
	    {
	      vTempLimPower_4000e3(g_lq10TempUse);
        }
		*/
		 else
	    {
	      vTempLimPower_Default_4320(g_lq10TempUse);
        }

	    //混插还要根据是否DERATING，区分
}


/*******************************************************************************
 *Function name: vM1TempLimPowerControl()  
 *Description : M1 Board Temperature limit power  calculate 
 *input:         void
 *global vars:   g_lq10TempPfcDispSave:rectifier PFC temperature                                 
 *               g_lq10TempDcDispSave:rectifier DC temperature
 *output:         s_lq10M1TempLimitPower: M1 board temperature limit power coefficient 
 *CALLED BY:     vCircuitCal() 
 ******************************************************************************/
static  void  vM1TempLimPowerControl(void)
{
   

       ubitfloat lM1TempLimit,lM1TempLimitCoef;
	   UINT16 u16M1TempLimCurveFlag;

		lM1TempLimit.lData = g_lq10TempPfcDispSave.lData;

		if(lM1TempLimit.lData < g_lq10TempDcDispSave.lData)
		{
		 lM1TempLimit.lData = g_lq10TempDcDispSave.lData;
		}
        //根据母线电压和输出电压比值决定M1板温度的限功率曲线
		if(IsrVars._u16SoftStartEndTimer >= DCSTARTEND_5S)
	    {   
	          //根据母线和DC电压在正常工作时的变比决定板温保护曲线，如果大于（376/40）×1024=9625
	          if(g_lq10VpfcVdcRatio.lData > LQ10_VPFC_VDC_RATIO_9P4 )
	          {
	           u16M1TempLimCurveFlag = 1;
	          }
	          else
	          {
	           u16M1TempLimCurveFlag = 0;
	          }
    	}
	    else //在非正常工作态，过温点不变
	    {
	      u16M1TempLimCurveFlag = 0;
	    }

	    if( u16M1TempLimCurveFlag == 0)
	    {
           //板温75度以下不限功率,75度~85度限到一半功率,85~90限到0
	    	if (lM1TempLimit.lData < TEMP_75C)
        	{
         	 lM1TempLimitCoef.lData = IQ10_TEMP_LIM_POW_MAX;
        	}
		 
             //75C~85C  1.0~0.5 RATING POWER
            //功率 = -0.05*x + 4.75
	     	else if (lM1TempLimit.lData < TEMP_85C)   
         	{
		    	lM1TempLimitCoef.lData = (((INT32)475)<<10)
		    			                   - _IQ10mpy(lM1TempLimit.lData, (((INT32)5)<<10));
    	    	lM1TempLimitCoef.lData = 
    	    			      _IQ10div(lM1TempLimitCoef.lData, (((INT32)100)<<10));
        	}

            //75C~85C  1.0~0.5 RATING POWER
            //功率 = = -0.1*x + 9
       	    else if (lM1TempLimit.lData < TEMP_90C)   
        	{
	    	  lM1TempLimitCoef.lData = (((INT32)90)<<10)
		    			                   - _IQ10mpy(lM1TempLimit.lData, (((INT32)1)<<10));
    	      lM1TempLimitCoef.lData = 
    	    			      _IQ10div(lM1TempLimitCoef.lData, (((INT32)10)<<10));
        	}
	    	else 
        	{
		    	lM1TempLimitCoef.lData = 0; 
    
     	    }

        }
	    else
	    {
	       //板温75度以下不限功率,75度~85度限到0
	    	if (lM1TempLimit.lData < TEMP_75C)
        	{
         	 lM1TempLimitCoef.lData = IQ10_TEMP_LIM_POW_MAX;
        	}		 
             //75C~85C  1.0~0 RATING POWER
            //功率 = -0.1x + 8.5
	     	else if (lM1TempLimit.lData < TEMP_85C)   
         	{
		    	lM1TempLimitCoef.lData = (((INT32)85)<<10)
		    			                   - _IQ10mpy(lM1TempLimit.lData, (((INT32)1)<<10));
    	    	lM1TempLimitCoef.lData = 
    	    			      _IQ10div(lM1TempLimitCoef.lData, (((INT32)10)<<10));
        	}
	    	else 
        	{
		    	lM1TempLimitCoef.lData = 0; 
    
     	    }
	   
	   
	    }
       s_lq10M1TempLimitPower.lData= lM1TempLimitCoef.lData ;

}

/*******************************************************************************
 *Function name: vAcLimPowerControl()  
 *Description :  AC limit power  calculate
 *input:         void 
 *global vars:   g_lq10AcVolt:input ac voltage value 
 *               s_iq10AcOldVolt:record the last used AC volt                               
 *               s_iq10ACLimitPower:AC limit power coefficient
 *output:        void 
 *CALLED BY:     vCircuitCal() 
 ******************************************************************************/
void vAcLimPowerControl(void)
{       

      
    //set AC limit power flag
    g_u16LimitStatus |= POW_LIM_AC; 
    
    //if AC over,clr AC limit power flag  
    if(g_u16MdlStatus.bit.ACOV)
    {
        g_u16LimitStatus &= ~POW_LIM_AC; 
    }


  if(g_u16MdlStatusExtend.bit.DCINPUTSTATUS == 0) //假如是AC电压输入
  {
	vAc_Lim_Power_Cal(g_lq10AcVoltUse);
  }

  else//如果是DC电压输入
  {
   vDc_Lim_Power_Cal(g_lq10AcVoltUse);
  }

}

/*******************************************************************************
 *Function name: vAc_Lim_Power_Cal()  
 *Description :  AC limit power  calculate
 *input:         void 
 * vars:        g_lq10AcVoltUse:input ac voltage value 
 *                                             
 *               
 *output:        void 
 *CALLED BY:     vAcLimPowerControl() 
 ******************************************************************************/
void vAc_Lim_Power_Cal(ubitfloat lq10Acin_Volt)
{
  //	ubitfloat lq10AcHvTempUse;
 //高压输入,低压输出限功率//y=100%-0.7*(54V-Vo)*(Vac-264V)/(12*26)
    if(  (lq10Acin_Volt.lData >= VAC_264V)
		  &&(g_lq10MdlVoltUse.lData <= VDC_48V) 
		  && (g_lq10MdlVoltUse.lData >= VDC_10V) 
		  && (g_lq10TempUse.lData >= TEMP_45C)
		  &&( g_u16ActionReady == NORMAL_RUN_STATE))	//高压WLK IN 满载要看能不能热抗住
    {      
             s_u16AcHvHoldOnTimer++;            //需抗10S后再限功率

			if( s_u16AcHvHoldOnTimer >= TIME10S_CAL)
			{
                s_u16AcHvHoldOnTimer = TIME10S_CAL;

		        s_lq10ACLimitPower.lData  = 
		        _IQ10mpy((VDC_54V - g_lq10MdlVoltUse.lData),(lq10Acin_Volt.lData - VAC_264V ));
                //2352=(0.7/(12*26))*1024*1024
		        s_lq10ACLimitPower.lData = ((INT32)1024) - (_IQ10mpy(s_lq10ACLimitPower.lData,(INT32)2352)>>10);  
		        //高压输入,低压输出限功率最小不低于0.03		
		        if(s_lq10ACLimitPower.lData < 31)
		        {
		          s_lq10ACLimitPower.lData = 31;
		         }	
		 
		        if (s_lq10ACLimitPower.lData > IQ10_AC_LIM_POW_MAX) 
		        {
		           s_lq10ACLimitPower.lData = IQ10_AC_LIM_POW_MAX;
			    	 //clr ac limit power flag
                   g_u16LimitStatus &= ~POW_LIM_AC; 
		        }                             		
		   } 
           else //高压输入，低压输出如果没到10S则输出满功率
		   {
		         s_lq10ACLimitPower.lData = IQ10_AC_LIM_POW_MAX;
			     //clr ac limit power flag
                 g_u16LimitStatus &= ~POW_LIM_AC; 
		   
		   }

     }
    // 非高压输入低压输出的限功率
    else
	{
            s_u16AcHvHoldOnTimer = 0;
        // 176~264VAC full power,264V以上非低压输出也是满功率
        if(lq10Acin_Volt.lData >= VAC_176V)
        {  
		  s_lq10ACLimitPower.lData = IQ10_AC_LIM_POW_MAX;//1024,100%
    	  //clr ac limit power flag
          g_u16LimitStatus &= ~POW_LIM_AC; 
        }   
        //85V~176V 分两种模式  
        else if(lq10Acin_Volt.lData >= VAC_85V)
        {
			//Mode B:
			if(g_u16MdlCtrlExtend.bit.ACLIMMODE)  //20120613  
			{    //154~176Vac:989.87(96.7%)->1024(100%)
			     //132~154Vac:512(50%)->989.87(96.7%)
			     //90~132Vac:512(50%)
			    //85~90Vac:409.6(40%)->512(50%)  // //85~90Vac:0.4->0.5 y = 0.02x - 1.3

				//154~176Vac:989.92(96.7%)->1024(100%)
			     //132~154Vac:410(40%)->989.92(96.7%)
			     //90~132Vac:410(40%)
			    //85~90Vac:358.4(35%)->410(40%)  // //85~90Vac:0.35->0.4 y = 0.01x - 0.5

				if(lq10Acin_Volt.lData <= VAC_90V)
				{
					s_lq10ACLimitPower.lData = _IQ10mpy(lq10Acin_Volt.lData, (((INT32)1)<<10))
					                           - (((INT32)50)<<10);
    				s_lq10ACLimitPower.lData = 
    				      _IQ10div(s_lq10ACLimitPower.lData, (((INT32)100)<<10));
				}				
				else if(lq10Acin_Volt.lData <= VAC_132V)////90~132Vac,410(40%)   //512(50%)
				{
					s_lq10ACLimitPower.lData = 410;    //IQ10_AC_LIM_POW_50PCT;
				}
                //132~154Vac:512(50%)->989.87(96.7%)
				//y(定标后)=21.72*x - 2355.18

				//132~154Vac:410(40%)->989.92(96.7%)
				//y(定标后)=26.36*x - 3069.52

				else if(lq10Acin_Volt.lData <= VAC_154V) 
				{
    			  s_lq10ACLimitPower.lData = _IQ10mpy(lq10Acin_Volt.lData, (((INT32)2636)<<10))
					                           - (((INT32)306952)<<10);

    				s_lq10ACLimitPower.lData = 
    				      _IQ10div(s_lq10ACLimitPower.lData, (((INT32)100)<<10))>>10;
				}
				 //154~176Vac:989.87(96.7%)->1024(100%)
				//y(未定标)= 0.0015*x + 0.736

				 //154~176Vac:989.92(96.7%)->1024(100%)
				//y(未定标)= 0.0015*x + 0.736
				else
				{
				    s_lq10ACLimitPower.lData = _IQ10mpy(lq10Acin_Volt.lData, (((INT32)15)<<10))
					                           + (((INT32)7360)<<10);
    			   s_lq10ACLimitPower.lData = 
    				      _IQ10div(s_lq10ACLimitPower.lData, (((INT32)10000)<<10));
				
				}						
			}			
			//Mode A:
			else 
			{    
			     //90~174Vac:512(50%)->1024(100%)
			    //85~90Vac:409.6(40%)->512(50%)  // //85~90Vac:0.4->0.5 y = 0.02x - 1.3

				//85~90:0.35~0.4    y = 0.01x - 0.5
				if(lq10Acin_Volt.lData <= VAC_90V)
				{
					s_lq10ACLimitPower.lData = _IQ10mpy(lq10Acin_Volt.lData, (((INT32)1)<<10))
					                           - (((INT32)50)<<10);

    				s_lq10ACLimitPower.lData = 
    				      _IQ10div(s_lq10ACLimitPower.lData, (((INT32)100)<<10));
				}				
         
				 //90~174Vac:512(50%)->1024(100%)
				//y(未定标)= 5953*x/1000 -24

				//90~174:40~100   y= 6.549*x -129
				//y=7.14*x -233

				else
				{
				    s_lq10ACLimitPower.lData = _IQ10mpy(lq10Acin_Volt.lData, ((INT32)7140));
					                           
    			   s_lq10ACLimitPower.lData = 
    				      _IQ10div(s_lq10ACLimitPower.lData, (((INT32)1000)<<10))+ ((INT32)-233);
				
				}								
			}  		
         }
		 //<85v以下40%,但80V应该关机了。

		 ////<85v以下35%,但80V应该关机了。
         else
        {  
		  s_lq10ACLimitPower.lData = IQ10_AC_LIM_POW_MIN;//35%  //40%
        }
   }

}

/*******************************************************************************
 *Function name: vDc_Lim_Power_Cal()  
 *Description :  HVDC INPUT limit power  calculate
 *input:         void 
 *global vars:   g_lq10AcVoltUse:input ac voltage value
 *                                             
 *               
 *output:        void 
 *CALLED BY:     vAcLimPowerControl() 
 ******************************************************************************/
void    vDc_Lim_Power_Cal(ubitfloat lq10Dcin_Volt)
{
  	//ubitfloat lq10AcHvTempUse;
  if(lq10Dcin_Volt.lData >= VHVDC_250V)//250V以上满功率 3000W
   {
        //高压输入,低压输出限功率
        //对应AC输入高压输入低压输出限功率的//y=100%-0.7*(54V-Vo)*(Vac-264V)/(12*26)
		//HVDC输入的高压输入低压输出限功率公式为//y=100%-0.7*(54V-Vo)*【(HVdc-374V)/1.414】/(12*26)
        if(  (lq10Dcin_Volt.lData >= VHVDC_374V)
		      &&(g_lq10MdlVoltUse.lData <= VDC_48V) 
		      && (g_lq10MdlVoltUse.lData >= VDC_10V) 
			  && (g_lq10TempUse.lData >= TEMP_45C)
		      &&( g_u16ActionReady == NORMAL_RUN_STATE))	//高压WLK IN 满载要看能不能热抗住
          {   
               s_u16AcHvHoldOnTimer++;            //需抗10S后再限功率
			   if( s_u16AcHvHoldOnTimer >= TIME10S_CAL)
			   {
                   s_u16AcHvHoldOnTimer = TIME10S_CAL;

		           s_lq10ACLimitPower.lData  = 
		             _IQ10mpy((VDC_54V - g_lq10MdlVoltUse.lData),(lq10Dcin_Volt.lData - VHVDC_374V ));

                    s_lq10ACLimitPower.lData =  _IQ10div(s_lq10ACLimitPower.lData,SQRT2);
 
		            s_lq10ACLimitPower.lData = ((INT32)1024) - (_IQ10mpy(s_lq10ACLimitPower.lData,(INT32)2352)>>10);  
		          //高压输入,低压输出限功率最小不低于0.03		
		           if(s_lq10ACLimitPower.lData < 31)
		           {
		             s_lq10ACLimitPower.lData = 31;
		            }	
		 
		           if (s_lq10ACLimitPower.lData > IQ10_AC_LIM_POW_MAX) 
		           {
		             s_lq10ACLimitPower.lData = IQ10_AC_LIM_POW_MAX;
				      //clr ac limit power flag
                     g_u16LimitStatus &= ~POW_LIM_AC; 
		           }                             		
		       } 
			   else //高压输入，低压输出如果没到10S则输出满功率
		       {
		         s_lq10ACLimitPower.lData = IQ10_AC_LIM_POW_MAX;
			     //clr ac limit power flag
                 g_u16LimitStatus &= ~POW_LIM_AC; 
		   
		       }

          }
        else
		{
		  s_lq10ACLimitPower.lData = IQ10_AC_LIM_POW_MAX;//1024,100%
    	  //clr ac limit power flag
          g_u16LimitStatus &= ~POW_LIM_AC; 

		  s_u16AcHvHoldOnTimer = 0;
		
		}
      
   }
   else
   {
       s_u16AcHvHoldOnTimer = 0;
       //200V 1500W, 250V 3000W
       // y= 0.01*x-1.5
        if(lq10Dcin_Volt.lData >= VAC_200V)
        {
            s_lq10ACLimitPower.lData = _IQ10mpy(lq10Dcin_Volt.lData, (((INT32)1)<<10))
					                   - (((INT32)150)<<10);

        	s_lq10ACLimitPower.lData = _IQ10div(s_lq10ACLimitPower.lData, (((INT32)100)<<10));
   
       }
       //85V 600W=20% 200V 1500W=50%
       // y = 0.0026x - 0.0217
       else if(lq10Dcin_Volt.lData >= VAC_85V)
       {
            s_lq10ACLimitPower.lData = _IQ10mpy(lq10Dcin_Volt.lData, (((INT32)26)<<10))
					                 - (((INT32)217)<<10);
         	s_lq10ACLimitPower.lData = _IQ10div(s_lq10ACLimitPower.lData, (((INT32)10000)<<10));
       }
        //<85v以下20%,但80V应该关机了。
       else
       {  
		      s_lq10ACLimitPower.lData = (IQ10_AC_LIM_POW_MIN/((INT32)2));//40%/2=20%
       }
   }
}
/*******************************************************************************
 *Function name: vAcCurrLimPowerControl()  
 *Description :  input current  limit power  calculate 
 *input:         void
 *global vars:   s_iq10AcOldVolt:record the last used AC volt
 *               g_iq10AcCurrFt:AC input current limit default value                                 
 *               s_iq10AcCurrLimitPower:AC current limit power coefficient 
 *output:        void
 *CALLED BY:     vCircuitCal() 
 ******************************************************************************/
void vAcCurrLimPowerControl(void)
{
    // AC input current limit
    // s_iq10AcOldVolt*g_iq10AcCurrFt*efficiency ;  -18: to meet error (-1A,0A) 
    s_lq10AcCurrLimitPower.lData = 
    			_IQ10mpy(s_lq10AcOldVolt.lData, g_lq10AcCurrFt.lData);

    s_lq10AcCurrLimitPower.lData = 
    			_IQ10mpy(s_lq10AcCurrLimitPower.lData, MAX_EFFICIENCY); 
    			              
	s_lq10AcCurrLimitPower.lData = 
    			_IQ10div(s_lq10AcCurrLimitPower.lData, IQ10POW_RATE) - ACCRRENT_LIM_DELTA;

}


/*******************************************************************************
 *Function name: vPowerLimCal()  
 *Description :  power limit  calculate 
 *input:         void
 *global vars:   s_iq10TempLimitPower:temperature limit power coefficient
 *               s_iq10ACLimitPower:AC limit power coefficient
 *               s_iq10AcCurrLimitPower:AC current limit power coefficient
 *               g_iq10SetPowerWant: actual power limit coefficient
 *               IsrVars._iPowerMax:actual power limit value                                 
 *output:        void 
 *CALLED BY:     vCircuitCal() 
 *******************************************************************************/
void vPowerLimCal(void)
{
    ubitfloat i32LimitData,lq10TempLimit;
    ubitfloat lq10Temp_compen;         // power compensation
    ubitfloat  lq10Temp_PfcMdlVolt;   // PFCVolt/MdlVolt

	if(  ( s_lq10TempLimitPower.lData == IQ10_TEMP_LIM_POW_MAX )
	   &&( s_lq10M1TempLimitPower.lData == IQ10_TEMP_LIM_POW_MAX ) )
	   {
	    g_u16LimitStatus &= ~POW_LIM_TEMP;
	   }
	   else
	   {
	     g_u16LimitStatus |=  POW_LIM_TEMP;
	   }

    	
    //power lim coefficient equ power limit set
    i32LimitData.lData = g_lq10SetPowerTrue.lData;
    
    //compare with temp limit mult AC limit,power lim coefficient equ the smaller
    lq10TempLimit.lData = 
    		_IQ10mpy(s_lq10TempLimitPower.lData, s_lq10ACLimitPower.lData); 
		
    lq10TempLimit.lData = 
    		_IQ10mpy(s_lq10M1TempLimitPower.lData, lq10TempLimit.lData);
    		                                                            
    lq10TempLimit.lData =
            _IQ10mpy(s_lq10DspTempLimitPower.lData, lq10TempLimit.lData);
    
    if(i32LimitData.lData > lq10TempLimit.lData)
    {
        i32LimitData.lData = lq10TempLimit.lData;
    }
    
    //用于限流计算  20220726 wwj
    g_lq10SetPowerWant2.lData = lq10TempLimit.lData;

    //compare with ac current limit,power lim coefficient equ the smaller
    s_u16AcCurrLimFlag = 0; //paul 20220302
    if(i32LimitData.lData > s_lq10AcCurrLimitPower.lData)
    {
    	i32LimitData.lData = s_lq10AcCurrLimitPower.lData;
        s_u16AcCurrLimFlag = 1; //paul 20220302
    }
    
	//compare with vVACHighVDCLow  power limit,power lim coefficient equ the smaller
	/*
	if(i32LimitData.lData > s_lq10HacLdctLimt.lData)
    {
    	i32LimitData.lData = s_lq10HacLdctLimt.lData;
    }
	*/

	 if(i32LimitData.lData < IQ10_POW_LIM_MIN)
    {    
        i32LimitData.lData = IQ10_POW_LIM_MIN; 
    }

    //get power lim coefficient
    g_lq10SetPowerWant.lData = i32LimitData.lData;    
    
    //cal power limit value
  /*  lq10TempLimit.lData = _IQ10mpy(g_lq10SetPowerWant.lData, g_i32PowerRate) 
						  + (INT32)g_u16PermitAdjust * POW_120W;//g_i32CurrRate;
  */
  //校准后根据Vpfc/Vo补偿额外功率 chenli 20200421
  //Vpfc/Vo<8,校准后补一个4%的额外功率  
  //Vpfc/Vo>8, Vpfc/Vo=8.7,compen 9.5% 1024*0.095=97, linear compensation, compen=40+57*(Vpfc/Vo-8)/0.7=40+81*(Vpfc/Vo-8)
    lq10Temp_PfcMdlVolt.lData = _IQ10div(g_lq10SetPfcVoltTrue.lData, g_lq10MdlVolt.lData);
      if(lq10Temp_PfcMdlVolt.lData < 8252)   //Vpfc/Vo<8.06
      {
      //58.19(PfcMdlVolt/7655)  compen1.4%      52.8(8252)  5.5%
           lq10Temp_compen.lData =8252-lq10Temp_PfcMdlVolt.lData;
           lq10Temp_compen.lData=_IQ10div(lq10Temp_compen.lData,597);
         lq10Temp_compen.lData=48-_IQ10mpy(lq10Temp_compen.lData,40);
        if(lq10Temp_compen.lData < 0)
        {
            lq10Temp_compen.lData = 0;
        }
      }
      //41.24V(9655)   0%
       else
       {
         lq10Temp_compen.lData=48;
       }
      //zhang0130
      if(IsrVars._u16SoftStartEndTimer<=300)
      {
          lq10Temp_compen.lData = 0;
      }
      else
      {
          lq10Temp_compen.lData = lq10Temp_compen.lData;
      }
      //zhang0130end
    lq10TempLimit.lData = _IQ10mpy(g_lq10SetPowerWant.lData, g_i32PowerRate) 
						  + (((INT32)g_u16PermitAdjust * g_i32PowerRate * lq10Temp_compen.lData)>>10);//g_i32PowerRate * 4% //g_i32CurrRate;


    lq10TempLimit.lData = _IQ10mpy(lq10TempLimit.lData, g_lq10PowerConSysa.lData) 
    		              + g_lq10PowerConSysb.lData;

    lq10TempLimit.lData = _IQ10mpy(IQ10_DEF_POWERFACTOR, lq10TempLimit.lData);
    
    lq10TempLimit.lData = lq10TempLimit.lData >> 10;


    
    if(lq10TempLimit.lData > (INT32)POWER_LIM_UPPER)
    {
        IsrVars._i16PowerLimMax = POWER_LIM_UPPER;
    }

	else if(lq10TempLimit.lData < (INT32)POW_MAX_DN) 
    {
    	IsrVars._i16PowerLimMax = POW_MAX_DN;
    }
    

    else
    {
     if(( IsrVars._i16PowerLimMax < ((INT16)(lq10TempLimit.lData - POW_LIM_ADJ_STEP)) )     	
    	 && (IsrVars._u16PfcOpened))
    	{
    	    IsrVars._i16PowerLimMax += ( (INT16)POW_LIM_ADJ_STEP);
    	}
    	else
    	{
    	    IsrVars._i16PowerLimMax = lq10TempLimit.iData.iLD;
    	}
    }
    /*//40V以下回缩要保持一定时间再回缩
	if(IsrVars._u16LimDcPowSetFlag == 1)
	{
	    IsrVars._u16LimDcPowSetTimer ++ ;
		if(IsrVars._u16LimDcPowSetTimer < 5)  //保持500ms DC 限功率
		{
			if(IsrVars._i16PowerLimMax > 3000)
			{
				IsrVars._i16PowerLimMax = 3000;
			}
			
		}
		else
		{
			IsrVars._u16LimDcPowSetFlag = 0;
			IsrVars._u16LimDcPowSetTimer = 0;
		}
	} 
	*/
	// csm091202
	IsrVars._i16PowerMaxExtra = IsrVars._i16PowerLimMax + POW_MAX_DELTA;
}

/*******************************************************************************
 *Function name: vPowerLimWarningHandle()  
 *Description :  power limit warning handle  
 *input:         void                                
 *global vars:   g_iq10MdlCurr:actual rectifir current
 *               g_iq10MdlVolt:actual rectifir dcvolt
 *               g_iq10SetPowerWant: actual power limit coefficient
 *               IsrVars._uiChoiseCon:voltage/limit power/limit current flag                           
 *output:        void      
 *CALLED BY:    vCircuitCal() 
 ******************************************************************************/
void vPowerLimWarningHandle(void)
{
    ubitfloat lTmp;
    							
    if((g_u16LimitStatus & POW_LIM_AC) || (g_u16LimitStatus & POW_LIM_TEMP))            
    {
        lTmp.lData = _IQ10mpy(g_lq10MdlCurr.lData, g_lq10MdlVolt.lData);
        
        //output power lager than maxpower(allowable) - 2.5%*DEFAULT_POWER ,set power limit flag
        if((lTmp.lData + POW_LIM_WARN_UP 
			>=_IQ10mpy(g_lq10SetPowerWant.lData, g_i32PowerRate))
			&& (IsrVars._u16ChoiseCon)) 
        {
            g_u16LimitStatus |= POW_LIM;
        }
        //output power less than maxpower(allowable)-3.5%*DEFAULT_POWER ,clear power limit flag
        else if((lTmp.lData + POW_LIM_WARN_BACK 
				<_IQ10mpy(g_lq10SetPowerWant.lData, g_i32PowerRate))
        		&& (IsrVars._u16ChoiseCon != 1))
        {               
            g_u16LimitStatus &= ~POW_LIM; //down
        }
    }
    else
    {
    	g_u16LimitStatus &= ~POW_LIM;
    }       
    	
    if(g_u16MdlStatus.bit.OFFSTAT)
    {
    	g_u16LimitStatus = 0;
    }       
}

/********************************************************************************
 *Function name: vOpShortHandle()  
 *Description :  output short handle 
 *input:         void
 *global vars:   g_iq10MdlVolt:actual rectifir dcvolt
 *               s_iq10ShortLimt:short limit current coefficient                
 *output:        void
 *CALLED BY:     vCircuitCal() 
 *******************************************************************************/
void vOpShortHandle_2900(void)
{

    if(g_lq10MdlVolt.lData <= VDC_20V)
    {
        s_lq10ShortLimt.lData = 1024;
    }
    else if(g_lq10MdlVolt.lData <= VDC_40V)
    {
        //20V-->1.0 40V-->1.2   limit=0.01*volt+0.8
        s_lq10ShortLimt.lData = ((_IQ10mpy(g_lq10MdlVolt.lData, 41))>>2) + 820;
    }
    else
    {
        s_lq10ShortLimt.lData = g_i32CurrLimtMax;
    }

}
/********************************************************************************
 *Function name: vOpShortHandle()
 *Description :  output short handle
 *input:         void
 *global vars:   g_iq10MdlVolt:actual rectifir dcvolt
 *               s_iq10ShortLimt:short limit current coefficient
 *output:        void
 *CALLED BY:     vCircuitCal()
 *******************************************************************************/
void vOpShortHandle_3000e3(void)
{
    ubitfloat temp;

    if(g_lq10MdlVolt.lData < VDC_40V)
    {
        s_u16OpShortHandleTimer++;

        if( s_u16OpShortHandleTimer >= TIME10S_CAL)
        {
            s_u16OpShortHandleTimer = TIME10S_CAL;

            if(g_lq10MdlVolt.lData <= VDC_20V)
            {
                //20V-->50A/51.7A*1024=990
                s_lq10ShortLimt.lData = 792;
            }
            else
            {
                //40V-->1250 IQ10_CURRLlMlTMAX
                //20---792  40---1127IQ10_CURRLlMlTMAX  limit= y =(2290x + 33400)/100
                temp.lData = (_IQ10mpy(g_lq10MdlVolt.lData, (INT32)2290)) + ((INT32)33400);
                s_lq10ShortLimt.lData = _IQ10div(temp.lData, (INT32)100<<10);
            }
        }
        else
        {
            s_lq10ShortLimt.lData = g_i32CurrLimtMax;
        }
    }
    else
    {
        s_lq10ShortLimt.lData = g_i32CurrLimtMax;
        s_u16OpShortHandleTimer=0;
    }
}
/********************************************************************************
 *Function name: vOpShortHandle()
 *Description :  output short handle
 *input:         void
 *global vars:   g_iq10MdlVolt:actual rectifir dcvolt
 *               s_iq10ShortLimt:short limit current coefficient
 *output:        void
 *CALLED BY:     vCircuitCal()
 *******************************************************************************/
void vOpShortHandle_3500e3(void)
{
    if(g_lq10MdlVolt.lData < VDC_40V)
    {
        s_u16OpShortHandleTimer++;

        if( s_u16OpShortHandleTimer >= TIME10S_CAL)
        {
            s_u16OpShortHandleTimer = TIME10S_CAL;

            if(g_lq10MdlVolt.lData <= VDC_20V)
            {
                        //20V-->50A/60.35A*1024=848
                s_lq10ShortLimt.lData = 848;
            }
            else
            {
                //40V-->1250 IQ10_CURRLlMlTMAX
                //20---848  40---1250 IQ10_CURRLlMlTMAX  limit= y = 20.1x + 446
                s_lq10ShortLimt.lData = (_IQ10mpy(g_lq10MdlVolt.lData, (INT32)201)) + ((INT32)4460);
                s_lq10ShortLimt.lData = (_IQ10div(s_lq10ShortLimt.lData<<10, ((INT32)10) << 10 ) )>>10;

            }
        }
        else
        {
            s_lq10ShortLimt.lData = g_i32CurrLimtMax;
        }
    }
    else
    {
        s_lq10ShortLimt.lData = g_i32CurrLimtMax;
        s_u16OpShortHandleTimer=0;
    }

}

/********************************************************************************
 *Function name: vOpShortHandle()
 *Description :  output short handle
 *input:         void
 *global vars:   g_iq10MdlVolt:actual rectifir dcvolt
 *               s_iq10ShortLimt:short limit current coefficient
 *output:        void
 *CALLED BY:     vCircuitCal()
 *******************************************************************************/

void vOpShortHandle_4320e3(void)
{
    ubitfloat temp;

    if(g_lq10MdlVolt.lData < VDC_40V)
    {
        s_u16OpShortHandleTimer++;

        if( s_u16OpShortHandleTimer >= TIME10S_CAL)
        {
            s_u16OpShortHandleTimer = TIME10S_CAL;

            if(g_lq10MdlVolt.lData <= VDC_20V)
            {
                //20V-->50A/51.7A*1024=990
                s_lq10ShortLimt.lData = 683;
            }
            else
            {
                //40V-->1130 IQ10_CURRLlMlTMAX
                //20---683  40---1127IQ10_CURRLlMlTMAX
                //limit= y =447v/20+236=(447v+4720)/20
                temp.lData = _IQ10mpy(g_lq10MdlVolt.lData, ((INT32)447)) + ((INT32)4720);
                s_lq10ShortLimt.lData = _IQ10div(temp.lData, ((INT32)20<<10));
            }
        }
        else
        {
            s_lq10ShortLimt.lData = g_i32CurrLimtMax;
        }
    }
    else
    {
        s_lq10ShortLimt.lData = g_i32CurrLimtMax;
        s_u16OpShortHandleTimer=0;
    }
}
/********************************************************************************
 *Function name: vOpShortHandle()
 *Description :  output short handle
 *input:         void
 *global vars:   g_iq10MdlVolt:actual rectifir dcvolt
 *               s_iq10ShortLimt:short limit current coefficient
 *output:        void
 *CALLED BY:     vCircuitCal()
 *******************************************************************************/
void vOpShortHandle(void)
{       
    //ubitfloat temp;
   	if(IsrVars._u16SoftStartEndTimer <= DCSTARTEND_500MS)
  	 {
		s_lq10ShortLimt.lData = g_i32CurrLimtMax;
		return;
	}
   
//2900w Derating 当混插且Derating时，默认额定功率，额定电流按2900W计算
	if(((g_u16PowerMixedSystemType & MIX_TYPE_2900W)==MIX_TYPE_2900W)
	   && (g_u16MdlCtrl.bit.NoDeratingFlag == 0))
	{//20V 50A

	    vOpShortHandle_2900();
		s_u16OpShortHandleTimer=0;//与老模块混插,不要求撑10S才短路回缩
	}
	else if(((g_u16PowerMixedSystemType & MIX_TYPE_3000e3)==MIX_TYPE_3000e3)
	         && (g_u16MdlCtrl.bit.NoDeratingFlag == 0))
	{

	    vOpShortHandle_3000e3();
	}
	else if(((g_u16PowerMixedSystemType & MIX_TYPE_3500e3)==MIX_TYPE_3500e3)
	         && (g_u16MdlCtrl.bit.NoDeratingFlag == 0))
	{

	    vOpShortHandle_3500e3();
	}
	else   //3000w //需要挺10S不能回缩
	{

         vOpShortHandle_4320e3();
	
	}
}

/*******************************************************************************
 *Function name: vCurrLimCal()  
 *Description :  DCDC current limit value calculate 
 *input:         void
 *global vars:   g_iq10SetLimitTrue:actual curr limit set by SCU+
 *               g_iq10SetPowerWant:actual power limit coefficient of module
 *               g_iq10MdlLimit:actual rectifir current limit for diaplay
 *               IsrVars._iIdcdcSys:dcdc limit current coefficient             
 *output:        void
 *CALLED BY:    vCircuitCal() 
 ******************************************************************************/
void vCurrLimCal(void)
{
  
    ubitfloat lq10LimitData,lq10TempLimit,lq10TempLimit2;
    ubitfloat lq10Temp_compen;
    ubitfloat  lq10Temp_PfcMdlVolt;   // PFCVolt/MdlVolt

    lq10LimitData.lData = g_i32CurrLimtMaxExtra;//1290
    //lq10LimitData.lData = 1280;//
    if(g_lq10MdlVolt.lData <= VDC_41V)
    {
      if(lq10LimitData.lData > s_lq10ShortLimt.lData)
      {
         lq10LimitData.lData = s_lq10ShortLimt.lData;
      }
    }

    //get actual curr limit coefficient set by SCU+                                                 
    lq10TempLimit.lData = g_lq10SetLimitTrue.lData;
    
    //compare with max current limit,curr lim coefficient equ the smaller           
    if(lq10LimitData.lData > lq10TempLimit.lData)
    {
        lq10LimitData.lData = lq10TempLimit.lData;
    }
    
    // current limit coefficient setting decrease with power limit coefficient
    /*
    lq10TempLimit.lData = 
                _IQ10mpy(g_lq10SetPowerWant.lData, IQ10_CURRLlMlTMAX);
    */
    lq10TempLimit.lData =
                _IQ10mpy(g_lq10SetPowerWant2.lData, g_i32CurrLimtMaxExtra); //保证48V输出功率正偏，跟温度限功率相关限流

    //curr lim coefficient equ the smaller
    if(lq10LimitData.lData > lq10TempLimit.lData)  
    {
        lq10LimitData.lData = lq10TempLimit.lData;
    }

    //compensation all voltage range
    lq10Temp_PfcMdlVolt.lData = _IQ10div(g_lq10SetPfcVoltTrue.lData, g_lq10MdlVolt.lData);  //K
    if(lq10LimitData.lData<(INT32)205)
	{
	    if(lq10Temp_PfcMdlVolt.lData<(INT32)7800)  // 135/1024=0.13   7885
        {
	        s_u16PfcMdlVolt=1;
        }
        else if(lq10Temp_PfcMdlVolt.lData>=(INT32)7950)
        {
            s_u16PfcMdlVolt=2;
        }
	    if( s_u16PfcMdlVolt==1)   //7.7 Q10 8038  K<8.2    391.5V /47.71V= 8.2
        {
            lq10Temp_compen.lData=-(INT32)15;
        }
	    else if( s_u16PfcMdlVolt==2)    //Io=0.278*Vpfc/Vo+0.2263   Io=0.361*Vpfc/Vo-2.0459
	    {
	        lq10Temp_compen.lData=_IQ10mpy((INT32)256,lq10Temp_PfcMdlVolt.lData);
            lq10Temp_compen.lData=lq10Temp_compen.lData-(INT32)2095;
            lq10Temp_compen.lData=_IQ10div(lq10Temp_compen.lData,CURR_RATE);
	    }
	}
	if(( lq10LimitData.lData>=(INT32)205)&&(lq10LimitData.lData<(INT32)410))   //    0.2<LimitData<0.4 Q10 205  410   1.05  Q10 1075
    {
	    if(lq10Temp_PfcMdlVolt.lData<(INT32)8806)  // 135/1024=0.13  8397
        {
            s_u16PfcMdlVolt=1;
        }
        else if(lq10Temp_PfcMdlVolt.lData>=(INT32)8908)
        {
            s_u16PfcMdlVolt=2;
        }
	    if(s_u16PfcMdlVolt==1)   //7.85 Q10 8038  K<8.2  Io=0.3971*Vpfc/Vo-5.0733    391.5V /47.71V= 8.2
        {
	        lq10Temp_compen.lData=_IQ10mpy((INT32)2990,lq10Temp_PfcMdlVolt.lData);
            lq10Temp_compen.lData=lq10Temp_compen.lData-(INT32)25383;
            lq10Temp_compen.lData=_IQ10div(lq10Temp_compen.lData,CURR_RATE);
            if(lq10Temp_compen.lData<(-(INT32)4096))
            {
                lq10Temp_compen.lData=-(INT32)4096;
        }
        }
        else if(s_u16PfcMdlVolt==2)        //K>=9.5   Io=0.76*Vpfc/Vo+0.0848  Io=0.7788*Vpfc/Vo+0.0848
        {
            lq10Temp_compen.lData=_IQ10mpy((INT32)100,lq10Temp_PfcMdlVolt.lData);
            lq10Temp_compen.lData=lq10Temp_compen.lData+(INT32)87;
            lq10Temp_compen.lData=_IQ10div(lq10Temp_compen.lData,CURR_RATE);
            //lq10Temp_compen_read0=lq10Temp_compen.lData;
        }
    }

	if(( lq10LimitData.lData>=(INT32)410)&&(lq10LimitData.lData<(INT32)614))   //    0.4<LimitData<0.62 Q10 666  922   1.05  Q10 1075
    {
	    if(lq10Temp_PfcMdlVolt.lData<(INT32)8400)  // 135/1024=0.13  8397
        {
            s_u16PfcMdlVolt=1;
        }
        else if(lq10Temp_PfcMdlVolt.lData>=(INT32)8500)
        {
            s_u16PfcMdlVolt=2;
        }
        if(s_u16PfcMdlVolt==1)   //8.2 Q10 8396  K<8.2  Io=0.3971*Vpfc/Vo-5.0733    391.5V /47.71V= 8.2
        {
            lq10Temp_compen.lData=-44;
        }
        else if(s_u16PfcMdlVolt==2) //8.7<K<12    Io=0.0383*Vpfc/Vo-0.3385
        {
            lq10Temp_compen.lData=_IQ10mpy((INT32)2330,lq10Temp_PfcMdlVolt.lData);
            lq10Temp_compen.lData=lq10Temp_compen.lData-(INT32)22128;
            lq10Temp_compen.lData=_IQ10div(lq10Temp_compen.lData,CURR_RATE);
        }
     }
    if(( lq10LimitData.lData>=(INT32)614)&&(lq10LimitData.lData<(INT32)921))   //    0.65<LimitData<0.9 Q10 666  922   1.05  Q10 1075
    {
        if(lq10Temp_PfcMdlVolt.lData<(INT32)8806)  // 135/1024=0.13  8397
        {
            s_u16PfcMdlVolt=1;
        }
        else if(lq10Temp_PfcMdlVolt.lData>=(INT32)8908)
        {
            s_u16PfcMdlVolt=2;
        }
        if(s_u16PfcMdlVolt==1)   //8.2 Q10 8396  K<8.1  Io=0.3971*Vpfc/Vo-5.0733
        {
            if(lq10Temp_PfcMdlVolt.lData<(INT32)8243)
            {
                lq10Temp_compen.lData=-(INT32)48;
            }
            else
            {
                lq10Temp_compen.lData=_IQ10mpy((INT32)345,lq10Temp_PfcMdlVolt.lData);
                lq10Temp_compen.lData=lq10Temp_compen.lData-(INT32)5195;
                lq10Temp_compen.lData=_IQ10div(lq10Temp_compen.lData,CURR_RATE);
            }
        }
        else if(s_u16PfcMdlVolt==2)  //8.2<K<11   Io=1.656*Vpfc/Vo-3.0298
        {
            lq10Temp_compen.lData=_IQ10mpy((INT32)3495,lq10Temp_PfcMdlVolt.lData);
            lq10Temp_compen.lData=lq10Temp_compen.lData-(INT32)30587;
            lq10Temp_compen.lData=_IQ10div(lq10Temp_compen.lData,CURR_RATE);
        }
    }
    if((lq10LimitData.lData>=(INT32)921)&&( lq10LimitData.lData<(INT32)1075))   //0.9 Q10  922   1.05  Q10 1075
    {
        if(lq10Temp_PfcMdlVolt.lData<(INT32)8880)  // 135/1024=0.13  8397
        {
            s_u16PfcMdlVolt=1;
        }
        else if(lq10Temp_PfcMdlVolt.lData>=(INT32)8930)
        {
            s_u16PfcMdlVolt=2;
        }
        if( s_u16PfcMdlVolt==1)   //8.2 Q10 8396  K<8.1  Io=-1.5661*Vpfc/Vo+8.9816
        {
            lq10Temp_compen.lData=_IQ10mpy((INT32)3480,lq10Temp_PfcMdlVolt.lData);
            lq10Temp_compen.lData=lq10Temp_compen.lData-(INT32)30780;
            lq10Temp_compen.lData=_IQ10div(lq10Temp_compen.lData,CURR_RATE);
        }
        else if(s_u16PfcMdlVolt==2)  //Io=2.1395*Vpfc/Vo-17.393
        {
            lq10Temp_compen.lData=_IQ10mpy((INT32)2250,lq10Temp_PfcMdlVolt.lData);
            lq10Temp_compen.lData=lq10Temp_compen.lData-(INT32)17810;
            lq10Temp_compen.lData=_IQ10div(lq10Temp_compen.lData,CURR_RATE);
        }
    }
    if( lq10LimitData.lData>=(INT32)1075)       //1.05  Q10 1075
    {
        if(lq10Temp_PfcMdlVolt.lData<(INT32)9216)
        {
            s_u16PfcMdlVolt=1;
        }
        else if(lq10Temp_PfcMdlVolt.lData>=(INT32)9318)
        {
            s_u16PfcMdlVolt=2;
        }
        if(s_u16PfcMdlVolt==1)
        {
            lq10Temp_compen.lData=_IQ10mpy((INT32)3260,lq10Temp_PfcMdlVolt.lData);
            lq10Temp_compen.lData=lq10Temp_compen.lData-(INT32)28535;
            lq10Temp_compen.lData=_IQ10div(lq10Temp_compen.lData,CURR_RATE);
        }
        else if(s_u16PfcMdlVolt==2)
        {
            lq10Temp_compen.lData=_IQ10mpy((INT32)1450,lq10Temp_PfcMdlVolt.lData);
            lq10Temp_compen.lData=lq10Temp_compen.lData-(INT32)10416;
            lq10Temp_compen.lData=_IQ10div(lq10Temp_compen.lData,CURR_RATE);
        }
    }
    lq10LimitData.lData += lq10Temp_compen.lData * (INT32)g_u16PermitAdjust;
        //lq10LimitData_test.lData = lq10LimitData.lData;
    //limit power change into limit current
    //g_iq10MdlLimit = fLimitData*58.0/tagMdlVolt.fd;
    g_lq10MdlLimit.lData = 
    _IQ10div(_IQ10mpy(g_lq10SetPowerWant.lData, VDC_POW_LIM), g_lq10MdlVolt.lData);

    if(lq10LimitData.lData < g_lq10MdlLimit.lData)
    {
        //显示限流系数时把补偿量减回去
        g_lq10MdlLimitDisp.lData = lq10LimitData.lData - lq10Temp_compen.lData * (INT32)g_u16PermitAdjust;
    }
    else
    {
        g_lq10MdlLimitDisp.lData = g_lq10MdlLimit.lData;
    }
    lq10TempLimit2.lData = _IQ10mpy(g_lq10MdlLimit.lData,g_i32CurrRate);
    lq10TempLimit2.lData =  _IQ10mpy(lq10TempLimit2.lData,g_lq10PowerConSysa.lData)
                         + g_lq10PowerConSysb.lData;


    //for DC Fault Check add by ls 20111129
    //g_lq10MdlLimitCurrent.lData =  _IQ10mpy(g_lq10MdlLimit.lData,g_i32CurrRate);
    
    //current limit value cal                       
    lq10TempLimit.lData = _IQ10mpy(lq10LimitData.lData,g_i32CurrRate); 
    lq10TempLimit.lData = _IQ10mpy(lq10TempLimit.lData,g_lq10CurrConSysa.lData)
                         + g_lq10CurrConSysb.lData;

    //改为限功率，限流校准后取小作为DC FAULT的判据
    if(lq10TempLimit2.lData > lq10TempLimit.lData)
    {
      lq10TempLimit2.lData = lq10TempLimit.lData;
     }
        g_lq10MdlLimitCurrent.lData = lq10TempLimit2.lData;


    lq10TempLimit.lData = _IQ10mpy(lq10TempLimit.lData,IQ10_DEF_IDCSYSFACTOR);            
    
    if(g_u16ActionReady == NORMAL_RUN_STATE)                    
    {               
        lq10LimitData.lData = lq10TempLimit.lData >> 10;
        

        if(lq10LimitData.lData > IDC_SYS_MAX)
        {
            IsrVars._i16IdcdcSysTrue = IDC_SYS_MAX;
        }

        else if(lq10LimitData.lData < IDC_SYS_MIN)
        {
            IsrVars._i16IdcdcSysTrue = IDC_SYS_MIN;
        }
        else
        {
             if(lq10LimitData.lData - IDC_SYS_REGU_STEP > (INT32)IsrVars._i16IdcdcSysTrue)
             {
                 IsrVars._i16IdcdcSysTrue += IDC_SYS_REGU_STEP;
             }
             else
             {
                 IsrVars._i16IdcdcSysTrue = (INT16)lq10LimitData.lData;
             }
        }
        IsrVars._i16IdcdcSys = IsrVars._i16IdcdcSysTrue;

    }

    if(IsrVars._i16IdcdcSys == IsrVars._i16IdcdcSysTrue)
    {
        IsrVars._i16CurrLimFloor -= IDC_POW_REGU_STEP ;
        if(IsrVars._i16CurrLimFloor < IDC_POW_LIM_DN)
        {
            IsrVars._i16CurrLimFloor = IDC_POW_LIM_DN;
        }
    }
}

/*******************************************************************************
 *Function name: vTempLimPower_2900()  
 *Description :  Temperature limit power  calculate 
 *input:         void
 *global vars:   g_iq10TempAmbi:rectifier intake temperature                                 
 *               s_iq10TempLimitPower:temperature limit power coefficient 
 *output:        void
 *CALLED BY:     vTempLimPowerControl() 
 ******************************************************************************/
//G2 2900w :45C->2900W  55C->2320W 60C -->1450W 65C -->0
static void vTempLimPower_2900 (ubitfloat i32UseTmp)
{
	ubitfloat lq10TempLimit,ubitfDataTemp;
	ubitfDataTemp.lData = i32UseTmp.lData;

		if (ubitfDataTemp.lData > TEMP_65C)
    		{
    			s_lq10TempLimitPower.lData = 0;
    		}    
   	 	else if (ubitfDataTemp.lData > TEMP_60C)
    		{
    		//fLimitTmp = (65 - fCtrlTempOld)/10;
    			lq10TempLimit.lData = ((INT32)65<<10) - ubitfDataTemp.lData;

    			s_lq10TempLimitPower.lData = 
    			     _IQ10div(lq10TempLimit.lData, ((INT32)10<<10));
    		}
    	else if (ubitfDataTemp.lData > TEMP_55C)
    		{
    		//fLimitTmp = (410-6*fCtrlTempOld)/100;
    			lq10TempLimit.lData = ((INT32)410<<10) 
    	                      - _IQ10mpy(((INT32)6<<10), ubitfDataTemp.lData);
    	                                        
    			s_lq10TempLimitPower.lData = 
    			            _IQ10div(lq10TempLimit.lData, ((INT32)100<<10));
    		}
    	else if (ubitfDataTemp.lData > TEMP_45C)
    		{
    		//fLimitTmp = (190-2*fCtrlTempOld)/100; 
    			lq10TempLimit.lData = ((INT32)190<<10) 
    	                      - _IQ10mpy(((INT32)2<<10), ubitfDataTemp.lData);
    	                                          
    			s_lq10TempLimitPower.lData = 
    	                      _IQ10div(lq10TempLimit.lData, ((INT32)100<<10));
    		}
    	else 
    		{
    			s_lq10TempLimitPower.lData = IQ10_TEMP_LIM_POW_MAX; 
    
    		//clr temp limit power flag  //和板温薰β首酆掀蓝?
    		//	g_u16LimitStatus &= ~POW_LIM_TEMP;
    		}  

}   

/*******************************************************************************
 *Function name: vTempLimPower_3200()  
 *Description :  Temperature limit power  calculate 
 *input:         void
 *global vars:   g_iq10TempAmbi:rectifier intake temperature                                 
 *               s_iq10TempLimitPower:temperature limit power coefficient 
 *output:        void
 *CALLED BY:     vTempLimPowerControl() 
 ******************************************************************************/
//G2 3200w :45C->3200W  55C->2900W 65C -->2320W 70C -->1450W 75C ->0
/*
static void vTempLimPower_3200 (ubitfloat i32UseTmp)
{
	ubitfloat lq10TempLimit,ubitfDataTemp;

	ubitfDataTemp.lData = i32UseTmp.lData;

	    	if (ubitfDataTemp.lData > TEMP_75C)
    		{
    			s_lq10TempLimitPower.lData = 0;
    		}
    		else if (ubitfDataTemp.lData > TEMP_70C)
    		{       
    		//fLimitTmp = (2175 - 29t)/320;  
    			lq10TempLimit.lData =((INT32)2175<<10) 
    			             - _IQ10mpy((INT32)29<<10, ubitfDataTemp.lData);

    			s_lq10TempLimitPower.lData = 
    			              _IQ10div(lq10TempLimit.lData, ((INT32)320<<10));
    		}
   	    	else if (ubitfDataTemp.lData > TEMP_65C)
    		{
    		//fLimitTmp = (6815 - 87*fCtrlTempOld)/1600;
    			lq10TempLimit.lData = ((INT32)6815<<10) 
    	                      -  _IQ10mpy(((INT32)87<<10),ubitfDataTemp.lData);

    			s_lq10TempLimitPower.lData = 
    			     _IQ10div(lq10TempLimit.lData, ((INT32)1600<<10));
    		}
    		else if (ubitfDataTemp.lData > TEMP_55C)
    		{
    		//fLimitTmp = (3045-29*fCtrlTempOld)/1600;
    			lq10TempLimit.lData = ((INT32)3045<<10) 
    	                      - _IQ10mpy(((INT32)29<<10), ubitfDataTemp.lData);
    	                                        
    			s_lq10TempLimitPower.lData = 
    			            _IQ10div(lq10TempLimit.lData, ((INT32)1600<<10));
    		}
    		else if (ubitfDataTemp.lData > TEMP_45C)
    		{
    		//fLimitTmp = (455-3*fCtrlTempOld)/320; 
    			lq10TempLimit.lData = ((INT32)455<<10) 
    	                      - _IQ10mpy(((INT32)3<<10), ubitfDataTemp.lData);
    	                                          
    			s_lq10TempLimitPower.lData = 
    	                      _IQ10div(lq10TempLimit.lData, ((INT32)320<<10));
    		}
    		else 
    		{
    			s_lq10TempLimitPower.lData = IQ10_TEMP_LIM_POW_MAX; 
    
    		//clr temp limit power flag //和板温限功率综合评定
    		//	g_u16LimitStatus &= ~POW_LIM_TEMP;
    		}  
	
}  
*/
/*******************************************************************************
 *Function name: vTempLimPower_3500()  
 *Description :  Temperature limit power  calculate 
 *input:         void
 *global vars:   g_iq10TempAmbi:rectifier intake temperature                                 
 *               s_iq10TempLimitPower:temperature limit power coefficient 
 *output:        void
 *CALLED BY:     vTempLimPowerControl() 
 ******************************************************************************/
//G2 3500w :45C->3500W  55C->3100W 65C -->2700W 70C -->1450W 75C ->0
/*
static void vTempLimPower_3500 (ubitfloat i32UseTmp)
{
	ubitfloat lq10TempLimit,ubitfDataTemp;
	ubitfDataTemp.lData = i32UseTmp.lData;

           if (ubitfDataTemp.lData > TEMP_75C)
    		{
    			s_lq10TempLimitPower.lData = 0;
    		}
    	    else if (ubitfDataTemp.lData > TEMP_70C)
    		{       
    		//fLimitTmp = 290*(75-t)/3500*1024=(2175-29t)<<10/350;  
    			lq10TempLimit.lData =((INT32)2175<<10) 
    			             - _IQ10mpy((INT32)29<<10, ubitfDataTemp.lData);

    			s_lq10TempLimitPower.lData = 
    			              _IQ10div(lq10TempLimit.lData, ((INT32)350<<10));
    		}
   	    	else if (ubitfDataTemp.lData > TEMP_65C)
    		{
    		//fLimitTmp = (13630 - 174*fCtrlTempOld)/3500*1024;
    			lq10TempLimit.lData = ((INT32)277212) 						//by lvbl 20110908
    	                      -  _IQ10mpy(((INT32)3657), ubitfDataTemp.lData);

    			s_lq10TempLimitPower.lData = 
    			     _IQ10div(lq10TempLimit.lData, ((INT32)50<<10));
    		}
    		else if (ubitfDataTemp.lData > TEMP_55C)
    		{
    		//fLimitTmp = (3045-29*fCtrlTempOld)/1600;G2 3200w
			//fLimitTmp = (3045-29*fCtrlTempOld)/1750;G2 3500w
    			lq10TempLimit.lData = ((INT32)15507) 						//by lvbl 20110908
    	                      - _IQ10mpy(((INT32)117), ubitfDataTemp.lData);
    	                                        
    			s_lq10TempLimitPower.lData = 
    			            _IQ10div(lq10TempLimit.lData, ((INT32)10<<10));
    		}
    		else if (ubitfDataTemp.lData > TEMP_45C)
    		{
    		//fLimitTmp = (455-3*fCtrlTempOld)/320; G2 3200w
			//fLimitTmp = (620-6*fCtrlTempOld)/350;G2 3500w		
    			lq10TempLimit.lData = ((INT32)15507) 					//by lvbl 20110908
    	                      - _IQ10mpy(((INT32)117), ubitfDataTemp.lData);
    	                                          
    			s_lq10TempLimitPower.lData = 
    	                      _IQ10div(lq10TempLimit.lData, ((INT32)10<<10));
    		}
    		else 
    		{
    			s_lq10TempLimitPower.lData = IQ10_TEMP_LIM_POW_MAX; 
    
    		//clr temp limit power flag //和板温限功率综合评定
    		//	g_u16LimitStatus &= ~POW_LIM_TEMP;
    		}  
      
}   
*/ 

/*******************************************************************************
 *Function name: vTempLimPower_4000e3()  
 *Description :  Temperature limit power  calculate 
 *input:         void
 *global vars:   g_iq10TempAmbi:rectifier intake temperature                                 
 *               s_iq10TempLimitPower:temperature limit power coefficient 
 *output:        void
 *CALLED BY:     vTempLimPowerControl() 
 ******************************************************************************/
/*
static void vTempLimPower_4000e3 (ubitfloat i32UseTmp)
{
	ubitfloat lq10TempLimit,ubitfDataTemp;

	ubitfDataTemp.lData = i32UseTmp.lData;
	
	//G3 4000w : -20~45度，4000W（100%）
    //            55度，3500W（87.5%）
    //            65度，3000W(75%)
    //            75度， 0W
         //75C  输出功率为0
		if (ubitfDataTemp.lData > TEMP_75C)
    	{
    		s_lq10TempLimitPower.lData = 0;
    	}
		 //65C~75C  3000w->0w;75%->0%;768->0;y = （-384x + 28800）/5(x为温度；y为功率百分比的定标值)
		else if (ubitfDataTemp.lData > TEMP_65C)   
    	{
			lq10TempLimit.lData = ((INT32)28800<<10) - _IQ10mpy((INT32)384<<10, ubitfDataTemp.lData);

    		s_lq10TempLimitPower.lData = 
    			              _IQ10div(lq10TempLimit.lData, ((INT32)5<<10))>>10;
    	}

        //45C~65C  4000w->3000w;100%->75%;1024->768;y = (-128x + 16000)/10 (x为温度；y为功率百分比的定标值)
		else if (ubitfDataTemp.lData > TEMP_45C)   
    	{
			lq10TempLimit.lData =  ((INT32)16000<<10) 
    			             - _IQ10mpy((INT32)128<<10, ubitfDataTemp.lData);

    		s_lq10TempLimitPower.lData = 
    			              _IQ10div(lq10TempLimit.lData, ((INT32)10<<10))>>10;
    	}
        
        
		else //-40C~45C full power  1024
    	{
			s_lq10TempLimitPower.lData = IQ10_TEMP_LIM_POW_MAX; 
    
    		//clr temp limit power flag //和板温限功率综合评定
    		//g_u16LimitStatus &= ~POW_LIM_TEMP;
    	}

}   
*/

/*******************************************************************************
 *Function name: vTempLimPower_Default_3000(ubitfloat i32UseTmp)  
 *Description :  Temperature limit power  calculate 
 *input:         void
 *global vars:   g_iq10TempAmbi:rectifier intake temperature                                 
 *               s_iq10TempLimitPower:temperature limit power coefficient 
 *output:        void
 *CALLED BY:     vTempLimPowerControl() 
 ******************************************************************************/
static void vTempLimPower_Default_4320(ubitfloat i32UseTmp)
{
    ubitfloat lq10TempLimit,ubitfDataTemp;

    ubitfDataTemp.lData = i32UseTmp.lData;


    //G3 3000w :     -40~45度，3000W（100%）
                      //55度，2900W（96.67%)
                     //65度，2250W（75%）
                    // 70度， 0W

    //paul2021-11-06
    //
   if (ubitfDataTemp.lData > TEMP_75C)
    {
        s_lq10TempLimitPower.lData = 0;
    }
    //70c--2150w---->0.498 *1024 = 509.6
    //75c--0w--->0
    //a=-101.92
    //b=7642
    else if (ubitfDataTemp.lData > TEMP_70C) //
    {

        lq10TempLimit.lData = ((INT32)68778<<10)
                                  - _IQ10mpy((INT32)917<<10, ubitfDataTemp.lData);

        s_lq10TempLimitPower.lData =
                                   _IQ10div(lq10TempLimit.lData, ((INT32)9<<10))>>10;
    }

    //60c-->3010w---->713.48
    //70C-->2150w---->509.6
    //a=20.4
    //b=1936.2 + 24
    else if (ubitfDataTemp.lData > TEMP_60C) //65度～70度   ( 4200-60t)/435
    {

        lq10TempLimit.lData = ((INT32)19602<<10)
                                  - _IQ10mpy((INT32)204<<10, ubitfDataTemp.lData);

        s_lq10TempLimitPower.lData =
                                   _IQ10div(lq10TempLimit.lData, ((INT32)10<<10))>>10;
    }
    //60c-->3010----713.48
    //55c-->3500----829.62
    //a=23.2
    //b=2105.6+24
    else if (ubitfDataTemp.lData > TEMP_55C) //55度～65度   ( 625-5t)/435
    {
        lq10TempLimit.lData = ((INT32)21296<<10)
                                       - _IQ10mpy((INT32)232<<10, ubitfDataTemp.lData);

       s_lq10TempLimitPower.lData =
                                   _IQ10div(lq10TempLimit.lData, ((INT32)10<<10))>>10;
    }
   //50C~55C 4000W to 3500W
    else if (ubitfDataTemp.lData > TEMP_50C)    //50度～55度( 900-10t)/432
    {
     lq10TempLimit.lData = ((INT32)900<<10)
                           - _IQ10mpy((INT32)10<<10, ubitfDataTemp.lData);

    s_lq10TempLimitPower.lData =
                                  _IQ10div(lq10TempLimit.lData, ((INT32)432<<10));
    }

   //45C~55C
    else if (ubitfDataTemp.lData > TEMP_45C) //45度～50度4320 to 4000W  (-6.4t+720)/432=(-32t+3600)/2160
    {
        lq10TempLimit.lData = ((INT32)3600<<10)
                                        - _IQ10mpy((INT32)32<<10, ubitfDataTemp.lData);

       s_lq10TempLimitPower.lData =
                                   _IQ10div(lq10TempLimit.lData, ((INT32)2160<<10));
    }
    else //-40C~45C full power  1024
    {
        s_lq10TempLimitPower.lData = IQ10_TEMP_LIM_POW_MAX;
    }


} 
//当输入305Vac时，母线电压要比305*1.414=431.27V高，否则靠工频管整流，输入电流畸变严重。
/*******************************************************************************
 *Function name: vPfcVoltVrefCal()  
 *Description :  PFC voltage vref calculate 
 *input:         void
 *global vars:   IsrVars._iVpfcSet:Vpfc setting voltage 
 *               g_iq10SetPfcVoltTrue :actual pfcvolt set by SCU+             
 *output:        void
 *CALLED BY:    vCircuitCal() 
 ******************************************************************************/
void vPfcVoltVrefCal(void)
{
    longunion   iTmp;   
    
    //pfc vref cal for isr
    iTmp.lData = _IQ10mpy(g_lq10VpfcConSys.lData, g_lq10SetPfcVoltTrue.lData);
    iTmp.lData = (_IQ10mpy(IQ10_DEF_VPFCFACTOR, iTmp.lData)) >> 10;    
    IsrVars._i16VpfcSet = iTmp.iData.iLD;
}

/*******************************************************************************
 *Function name: vPfcVoltStepRegu()  
 *Description:   pfc voltage step regulate
 *input:         void
 *global vars:   g_iq10PfcVoltRegulate: the regulation value of pfc volt                                  
 *               g_iq10SetPfcVoltTrue :actual pfcvolt set by SCU+
 *output:        void
 *CALLED BY:     vPfcVoltVrefCal() 
 ******************************************************************************/
void vPfcVoltStepRegu(void)
{
    if( g_lq10SetPfcVoltTrue.lData < (g_lq10PfcVoltRegulate.lData 
        - PFCVOLT_REGU_STEP) )
    {
        g_lq10SetPfcVoltTrue.lData += PFCVOLT_REGU_STEP;
    }

    else if( g_lq10SetPfcVoltTrue.lData > (g_lq10PfcVoltRegulate.lData 
             + PFCVOLT_REGU_STEP) )
    {
        g_lq10SetPfcVoltTrue.lData -= PFCVOLT_REGU_STEP;
    }
    else
    {
    	g_lq10SetPfcVoltTrue.lData = g_lq10PfcVoltRegulate.lData; 
    }       
}

/*******************************************************************************
 *Function name: vPfcVoltReguAlgorithm()  
 *Description :  pfc volt regulate Algorithm 
 *input:         void
 *global vars:   g_iq10SetPfcVolt: wanted pfcvolt set by SCU+
 *               g_iq10PfcVoltRegulate: the regulation value of pfc volt
 *               g_lq10AcVolt:input ac voltage value 
 *               g_iq10SetVoltTrue:actual dcvolt set by SCU+             
 *output:        void
 *CALLED BY:    vPfcVoltVrefCal() 
 ******************************************************************************/
void vPfcVoltReguAlgorithm(void)
{
    //pfc volt regulate Algorithm:
    //dcdc softstart end 600ms later begin pfc voltage regulate 
	longunion   lTmpVpfc_A, lTmpVpfc_B,lTmpVpfc_C, lTmpCal;
 	
	if(IsrVars._u16SoftStartEndTimer >= DCSTARTEND_600MS)
 	{
		//Vpfc_setA=factork*Vo + 0.5*(Io-10A)	(10A<Io<50A)
	    if(g_lq10MdlCurrDisp.lData < IDC_10A)
	    {
	        lTmpCal.lData = IDC_10A;
	    }
	    else if(g_lq10MdlCurrDisp.lData > IDC_50A)
	    {
	        lTmpCal.lData = IDC_50A;
	    }
	    else
	    {
	        lTmpCal.lData = g_lq10MdlCurrDisp.lData;
	    }

	    //lTmpVpfc_A.lData = _IQ10mpy(g_lq10MdlVolt.lData, VPFC_VDC_K) + ((lTmpCal.lData - IDC_10A) >> 1);
        //Vpfc_A=Vo*K+(Io-10A)*0.625   母线电压计算
	    lTmpVpfc_A.lData = _IQ10mpy(g_lq10MdlVolt.lData, VPFC_VDC_K) + _IQ10mpy((lTmpCal.lData - IDC_10A), (INT32)640);

         //改为峰值电压＋10V，防止输入波形不是正弦波
        lTmpVpfc_B.lData = g_lq10ACPeak_CalVpfc.lData + VPFC_10V ;

		//输入电压有效值计算母线调压值
		if(g_lq10AcVolt.lData < VAC_200V)
		{
		    lTmpCal.lData = VAC_200V;
		}
		else if(g_lq10AcVolt.lData > VAC_300V)
		{
		    lTmpCal.lData = VAC_300V;
		}
		else
		{
		    lTmpCal.lData = g_lq10AcVolt.lData;
		}

		if(lTmpCal.lData <= VAC_250V)
		{
		    //Vpfc=Vrms + 170V
		    lTmpVpfc_C.lData = lTmpCal.lData + VPFC_170V;
		}
		else
		{
		    //Vpfc=0.3*Vrms + 345V
		    lTmpVpfc_C.lData = _IQ10mpy(lTmpCal.lData, ((INT32)307)) + VPFC_345V;
		}

		if(lTmpVpfc_A.lData < lTmpVpfc_B.lData)
		{
		    lTmpVpfc_A.lData =  lTmpVpfc_B.lData;
		}
		if(lTmpVpfc_A.lData < lTmpVpfc_C.lData)
		{
		    lTmpVpfc_A.lData =  lTmpVpfc_C.lData;
		}

        //     杂音优化补丁 wwj
        if((g_u16MdlStatusExtend.bit.PARAFLAG == 0)
                && (g_lq10AcVolt.lData < VAC_230V)
                && (g_lq10AcVolt.lData > VAC_210V)
                && (g_lq10MdlVolt.lData < VDC_55V)
                && (g_lq10MdlVolt.lData > VDC_52V)
                && (g_lq10MdlCurr.lData < IDC_77A)
                && (g_lq10MdlCurr.lData > IDC_72A))
        {
            g_lq10PfcVoltRegulate.lData = VPFC_ADJUST_UP;
            g_u16PfcvoltChgFlag_high = 1;
        }
        else if((g_u16PfcvoltChgFlag_high==1)
                && (g_u16MdlStatusExtend.bit.PARAFLAG == 0)
                && ((g_lq10AcVolt.lData > VAC_205V) && (g_lq10AcVolt.lData < VAC_235V))
                && ((g_lq10MdlVolt.lData > VDC_51V) && (g_lq10MdlVolt.lData < VDC_56V))
                && (g_lq10MdlCurr.lData > IDC_70A) && (g_lq10MdlCurr.lData < IDC_79A))
        {
            g_lq10PfcVoltRegulate.lData = VPFC_ADJUST_UP;
        }
        else
        {

            g_lq10PfcVoltRegulate.lData =  lTmpVpfc_A.lData;
            g_u16PfcvoltChgFlag_high = 0;
        }
        //Vpfc debug code
		/*lTmpCal.lData = (INT32)g_u16PfcVoltDebug << 10;
		if(lTmpCal.lData > lTmpVpfc_B.lData)
		{
		    g_lq10PfcVoltRegulate.lData = lTmpCal.lData;
		}*/

		if(g_u16PfcvoltChgFlag==1)
		{
		    g_lq10PfcVoltRegulate.lData = g_lq10SetPfcVolt.lData;
		}

		//Limit
		if( g_lq10PfcVoltRegulate.lData <= VPFC_ADJUST_DN )
	    {
	   	 	g_lq10PfcVoltRegulate.lData = VPFC_ADJUST_DN;
	    }
		else if(g_lq10PfcVoltRegulate.lData >= VPFC_ADJUST_UP)
		{
		    g_lq10PfcVoltRegulate.lData = VPFC_ADJUST_UP;
		}

	}	 
   
	else //在软启动没完成时,PFC母线下限为405V，但如果AC电压峰值大于母线电压，把母线电压抬高到AC峰值，防止不控整流
    {
        lTmpVpfc_C.lData = g_lq10ACPeak_CalVpfc.lData+ VPFC_10V ;

		if( lTmpVpfc_C.lData > VPFC_ADJUST_UP)
		{
		   lTmpVpfc_C.lData = VPFC_ADJUST_UP;
		}

		if(lTmpVpfc_C.lData < g_lq10SetPfcVolt.lData)
    	{
    	    g_lq10PfcVoltRegulate.lData = g_lq10SetPfcVolt.lData;
		}
		else
		{
		    g_lq10PfcVoltRegulate.lData = lTmpVpfc_C.lData;
		}
    }
}

/*******************************************************************************
 *Function name: vPFCProtectPointCal()  
 *Description :  pfc volt protect value calculation with calibrate,will used in ISR 
 *input:         void
 *global vars:  _i16VpfcProtectPoint 
 *              
 *               
 *                           
 *output:        void
 *CALLED BY:    none
 ******************************************************************************/

void  vPFCProtectPointCal(void)
{

    ubitfloat	lq10Temp;
/*
    lq10Temp.lData = VPFC_460V;
	lq10Temp.lData = _IQ10mpy(lq10Temp.lData ,IQ10_DEF_VPFCFACTOR);
	lq10Temp.lData = _IQ10div(lq10Temp.lData , g_lq10VpfcSampSys.lData);//PFC显示电压校准系数   
    lq10Temp.lData = lq10Temp.lData >>10;
	IsrVars._i16Vpfc460VPoint = lq10Temp.iData.iLD;
*/
    lq10Temp.lData = VPFC_490V;
	lq10Temp.lData = _IQ10mpy(lq10Temp.lData ,IQ10_DEF_VPFCFACTOR);
	lq10Temp.lData = _IQ10div(lq10Temp.lData , g_lq10VpfcSampSys.lData);//PFC显示电压校准系数   
    lq10Temp.lData = lq10Temp.lData >>10;
	IsrVars._i16Vpfc490VPoint = lq10Temp.iData.iLD;

	lq10Temp.lData = VPFC_300V;
	lq10Temp.lData = _IQ10mpy(lq10Temp.lData ,IQ10_DEF_VPFCFACTOR);
	lq10Temp.lData = _IQ10div(lq10Temp.lData , g_lq10VpfcSampSys.lData);//PFC显示电压校准系数   
    lq10Temp.lData = lq10Temp.lData >>10;
	IsrVars._i16Vpfc300VPoint = lq10Temp.iData.iLD;
/*
	lq10Temp.lData = VPFC_70V;
	lq10Temp.lData = _IQ10mpy(lq10Temp.lData ,IQ10_DEF_VPFCFACTOR);
	lq10Temp.lData = _IQ10div(lq10Temp.lData , g_lq10VpfcSampSys.lData);//PFC显示电压校准系数   
    lq10Temp.lData = lq10Temp.lData >>10;
	IsrVars._i16Vpfc70VPoint = lq10Temp.iData.iLD;

*/


} 

/*******************************************************************************
 *Function name: vDcVolCalFreqLowLim()  
 *Description :  Based Real  output DC Vol value cal DC minum frequency
 *input:         void
 *global vars:   
           
 *output:        void
 *CALLED BY:    vPfcVoltVrefCal() 
 ******************************************************************************/
static  void  vDcVolCalFreqLowLim(void)
 {

       longunion   i32TmpA,i32TmpB;

       i32TmpA.lData = _IQ16mpy((INT32)g_lq12VoltSampSysa.lData<<4, (INT32)1036124);
       i32TmpA.lData = _IQ16div(i32TmpA.lData,(INT32)30683824);

       i32TmpB.lData = _IQ16mpy((INT32)g_lq12VoltSampSysb.lData<<4,(INT32)1036124);
       i32TmpB.lData = (i32TmpB.lData + (INT32)75641651)>>16;

       IsrVars._iq16DcVolCalDcFreqCoefA = i32TmpA.iData.iLD;
       IsrVars._i16DcVolCalDcFreqCoefB = i32TmpB.iData.iLD; 


}



/*******************************************************************************
 *Function name: vInVolHysCal(void)  
 *Description :  //输入电压的回滞计算，用于高低压限功率等
 *input:         void
 *global vars:   
           
 *output:        void
 *CALLED BY:     
 ******************************************************************************/
static void vInVolHysCal(void)
{
    // calculate power limit of input voltage,hysteresis:2V
    if (( g_lq10AcVolt.lData <=  s_lq10AcOldVolt.lData )
        ||( g_lq10AcVolt.lData > ( s_lq10AcOldVolt.lData + VAC_2V )))
    {
    	s_lq10AcOldVolt.lData = g_lq10AcVolt.lData;
    }


     ////-------------------------------------------------------------------------------//
    /////////////////////以255V为分界点,区分264V以上和176V以下不同的电压余量策略/////////
	/////////////////////同时考虑到高压直流从250V开始限功率/////////////////////////////
	////-------------------------------------------------------------------------------//
    // to ensure AC > 264V enough power, SUB 1.5 volt,make sure 1.5v~3.5v  MARGIN
    //当AC电压下行,用于计算限功率的AC电压比当前的AC电压小1.5V,作为余量(保证出足够功率
	//当AC电压上行,用于计算限功率的AC电压比当前的AC电压小1.5~3.5V,作为余量(保证出足够功率
	if(g_lq10AcVolt.lData >= VAC_255V)
	{
	  g_lq10AcVoltUse.lData = s_lq10AcOldVolt.lData - VAC_1V5;
	}
    
    // to ensure AC < 176V enough power, ADD 3.5 volt,make sure 1.5v~3.5v MARGIN
	//当AC电压下行,用于计算限功率的AC电压比当前的AC电压大3.5V,作为余量(保证出足够功率
	//当AC电压上行,用于计算限功率的AC电压比当前的AC电压大1.5~3.5V,作为余量(保证出足够功率
  else
  {
      g_lq10AcVoltUse.lData = s_lq10AcOldVolt.lData + VAC_3V5;
  }


      if(g_u16MdlStatusExtend.bit.DCINPUTSTATUS == 0) //假如是AC电压输?
	{
	     if( g_lq10AcVoltUse.lData < VAC_DIS_DN )
	     {
	 	     g_lq10AcVoltUse.lData = VAC_DIS_DN;
	 
	     }
	 }
    else//假如是DC电压输入
	{
	     if( g_lq10AcVoltUse.lData < VDCIN_DIS_DN)
	     {
	    	 g_lq10AcVoltUse.lData = VDCIN_DIS_DN;
	 
	      }
	}




}

/*******************************************************************************
 *Function name:  vAmbTempHysCal(void) 
 *Description :  //环境温度的回滞计算，用于环境温度限功率等。
 *               //用于计算的g_lq10TempUse.lData比显示环温小0～2C
 *input:         void
 *global vars:   
           
 *output:        void
 *CALLED BY:     
 ******************************************************************************/         
static void  vAmbTempHysCal(void)
{    
    //record the last used U1 bord temperature
    static ubitfloat s_iq10TempCOld; 

	/*
    //U1 bord temp limit power ,hysteresis: 2℃
    // calculate power limit of temperature
    if((g_lq10TempAmbiDisp.lData >= s_iq10TempCOld.lData)
    	||(g_lq10TempAmbiDisp.lData < (s_iq10TempCOld.lData - TEMP_2C)))
   */
	 if((g_lq10TempAmbiDisp.lData >= s_iq10TempCOld.lData)
    	||(g_lq10TempAmbiDisp.lData < (s_iq10TempCOld.lData - TEMP_0C7)))
    {
        s_iq10TempCOld.lData = g_lq10TempAmbiDisp.lData;
    }
    
    // to ensure enough power, over 2 degree
    g_lq10TempUse.lData = s_iq10TempCOld.lData - TEMP_3C;

}

/*******************************************************************************
 *Function name: vOutVolHysCal(void); 
 *Description :  //输出电压的回滞计算，用于高压输入，低压输出限功率及短路回缩等 。
 *               //用于计算的g_lq10MdlVoltUse.lData比显示电压大0～0.5V
 *input:         void
 *global vars:   
           
 *output:        void
 *CALLED BY:     
 ******************************************************************************/ 
static void vOutVolHysCal(void)
{
    //record the last used output voltage,
    static ubitfloat s_lq10MdlVoltOld; 

    //Output Voltage ,hysteresis: 0.5V
    if((g_lq10MdlVolt.lData <= s_lq10MdlVoltOld.lData)
    	||(g_lq10MdlVolt.lData > (s_lq10MdlVoltOld.lData + VDC_0V5)))
    {
        s_lq10MdlVoltOld.lData = g_lq10MdlVolt.lData;
    }
    
    // to ensure enough power, over 0.5V
    g_lq10MdlVoltUse.lData = s_lq10MdlVoltOld.lData + VDC_0V5;


}

/*******************************************************************************
 *Function name: vVpfcVdcRatioCal(void) 
 *Description :  //计算PFC母线和DC输出电压比值；
 *               
 *input:         void
 *global vars:   
           
 *output:        void
 *CALLED BY:     
 ******************************************************************************/ 
static void vVpfcVdcRatioCal(void)
{
    ubitfloat lTemp;

	lTemp.lData = _IQ10div(g_lq10PfcVoltDisp.lData,g_lq10MdlVolt.lData);

	g_lq10VpfcVdcRatio.lData = lUpdownlimit(lTemp.lData , LQ10_VPFC_VDC_RATIO_MAX, LQ10_VPFC_VDC_RATIO_MIN);

}

/*******************************************************************************
 *Function name: vKWHCal(void) 
 *Description :  //电量计算；
 *               
 *input:         void
 *global vars:   
           
 *output:        void
 *CALLED BY:     
 ******************************************************************************/ 
void vKWHCal(void)
{
    if (g_u16MdlPowerAcuumClearFlg == 1)
	{
		g_u16MdlPowerAcuumClearFlg = 0;

		//STEP1:power accumulate time clear
		g_lPowerAccumTime.lData = 0;

		//STEP2:Time Counter cleared
		s_u16KWHCnt = 0;

		//STEP3:EEPROM保存的电量清0
		g_lPowerAccum.lData = 0;
		//读取电量低位用于累加功率计算
		g_lq10PowerAccumLow.lData=0;
		// 功率累加器每分钟清0，累加时间间隔115MS
		s_lq10PowAccumPerMin.lData = 0;
		//  Monitor get power clear
		g_lSCUGetWattAccum.lData= 0;

		//STEP4:使能EEPROM写入
		g_u16WriteNumber = 0;
		g_u16EpromWr.bit.WATTACCUMULATE = 1;
        g_u16EpromWr.bit.WATTACCUMULATETIME = 1;

	}

     //When the input volt Undervoltage ，save power and cumulative time  
    if (((g_u16MdlStatusExtend.bit.DCINPUTSTATUS == 0)&&(g_lq10AcVolt.lData <= VAC_COM_HALT))
	  ||((g_u16MdlStatusExtend.bit.DCINPUTSTATUS == 1)&&(g_lq10AcVolt.lData <= VDCIN_COM_HALT))
	  ||(g_u16Restflag == 1))
	{
	    //when input volt undervoltage,enable only once
	    if (g_u16KWHCalState == CALCULATION_STATE)
		{
	   		g_u16KWHCalState = STOP_CALCULATION_STATE;
	   		//STEP1:power accumulate time Update，unit: minute
	    	g_lPowerAccumTime.lData += ((((INT32)s_u16KWHCnt) * TIME115MS)/TIME1MIN_MS);

			//STEP2:Time Counter cleared
			s_u16KWHCnt = 0;

	    	//STEP3:存储一次电量:UNIT :w*h;定标Q0
	    	// unit: w*h  , Calibration Q10
        	g_lq10PowerAccumLow.lData = (s_lq10PowAccumPerMin.lData /TIME1H_CAL) + g_lq10PowerAccumLow.lData;
	   		// 功率累加器每分钟清0，累加时间间隔115MS
	    	s_lq10PowAccumPerMin.lData = 0;
	    	//  Monitor get power ; unit: w*h 
	    	g_lSCUGetWattAccum.lData= ((INT32)g_lPowerAccum.uintdata[1].id<<16) + (g_lq10PowerAccumLow.lData>>10);

			//上报监控电量限幅
			g_lSCUGetWattAccum.lData = lUpdownlimit(g_lSCUGetWattAccum.lData , POWER_MAX, POWER_MIN);

	   		g_lPowerAccum.lData =  ((INT32)g_lPowerAccum.uintdata[1].id<<16) +(g_lq10PowerAccumLow.lData>>10);
            //模块保存电量限幅
			g_lPowerAccum.lData = lUpdownlimit(g_lPowerAccum.lData , POWER_MAX, POWER_MIN);
		
			//STEP4:Write EEPROM 
			g_u16WriteNumber = 0;
	       	g_u16EpromWr.bit.WATTACCUMULATE = 1;
           	g_u16EpromWr.bit.WATTACCUMULATETIME = 1;
			//STEP5：读取电量低位用于累加功率计算
			g_lq10PowerAccumLow.lData=((INT32)g_lPowerAccum.uintdata[0].id<<10);
		}

			
	}
	else
	{
		//accumulate state
		g_u16KWHCalState = CALCULATION_STATE;

		//timer counter (per115ms)
		s_u16KWHCnt++;

		//increment the accumulator and count the sample
		s_lq10PowAccumPerMin.lData += g_lq10AcPow.lData;	
      
		//Calculated WATT once per minute 
		if( s_u16KWHCnt% TIME64S_CAL == 0)
		{
		   // unit: w*h  , Calibration Q10
	       g_lq10PowerAccumLow.lData = (s_lq10PowAccumPerMin.lData /TIME1H_CAL) + g_lq10PowerAccumLow.lData;
		   // 功率累加器每分钟清0，累加时间间隔115MS
		   s_lq10PowAccumPerMin.lData = 0;
		   //  Monitor get power ; unit: w*h 
		   g_lSCUGetWattAccum.lData= ((INT32)g_lPowerAccum.uintdata[1].id<<16) + (g_lq10PowerAccumLow.lData>>10);
		    //上报监控电量限幅
			g_lSCUGetWattAccum.lData = lUpdownlimit(g_lSCUGetWattAccum.lData , POWER_MAX, POWER_MIN);
		}
		//每小时存储一次电量
		if ( s_u16KWHCnt >= TIME1H_CAL )
		{		
			//STEP1:power accumulate time Update，unit: minute
		    g_lPowerAccumTime.lData += TIME1H_MIN;
			//STEP2:Time Counter cleared
			s_u16KWHCnt = 0;
	        //STEP3:每小时存储一次电量:UNIT :w*h;定标Q0
			g_lPowerAccum.lData =  ((INT32)g_lPowerAccum.uintdata[1].id<<16) + (g_lq10PowerAccumLow.lData>>10);
			//模块保存电量限幅
			g_lPowerAccum.lData = lUpdownlimit(g_lPowerAccum.lData , POWER_MAX, POWER_MIN);
			//STEP4:使能EEPROM写入
			g_u16WriteNumber = 0;
			g_u16EpromWr.bit.WATTACCUMULATE = 1;
            g_u16EpromWr.bit.WATTACCUMULATETIME = 1;
			//STEP5：读取电量低位用于累加功率计算
			g_lq10PowerAccumLow.lData=((INT32)g_lPowerAccum.uintdata[0].id<<10);
		}

	}
}
/*=============================================================================*
 *Function name: vPfcPhaseComplement()
 *Description :  PFC CUR/VOL PHASE delay complement
 *input:         g_lq10MdlPow.lData,g_lq10AcVolt.lData etc
 *global vars:   void
 *output:        IsrVars._u16AcVoltPhaseshift
 *CALLED BY:     void vWarnCtrl(void)
 *modify data:   20081209
 *============================================================================*/
INT16     g_i16VpfcCalCnt;
static  void  vPfcPhaseComplement(void)
{
    Uint16 u16PhaseshiftTemp;
    Uint16 u16TonShiftLimitTemp;
    //UINT16 TonShiftLimitTemp;
    ubitfloat s_lq10ShiftLimitTemp;
    ubitfloat lq10PowerLimit;

//    lq10PowerLimit.iData.iLD = IsrVars._i16PowerLimMax;
//    lq10PowerLimit.lData = lq10PowerLimit.lData << 10;
//    lq10PowerLimit.lData = _IQ10div(lq10PowerLimit.lData, IQ10_DEF_POWERFACTOR);

    if (abs(g_lq10MdlCurrNoFilter.lData - g_lq10MdlCurrLast.lData) > IDC_20A)
    //if (g_lq10PfcVolt.lData <= VPFC_375V)
    {
        g_i16VpfcCalCnt = 20;
    }
    else
    {
        g_i16VpfcCalCnt --;
    }

    g_lq10MdlCurrLast.lData = g_lq10MdlCurrNoFilter.lData;

    if (g_i16VpfcCalCnt <= 1)
    {
        g_i16VpfcCalCnt = 1;
    }

    lq10PowerLimit.lData = _IQ10mpy(g_lq10SetPowerWant.lData, g_i32PowerRate);
    g_lq10LoadPercent.lData =  _IQ10div(g_lq10MdlPow.lData,lq10PowerLimit.lData);

    //if(IsrVars._i16PfcDynFlg <= PFC_DYN_FLG_LOW_LIMIT)//如果没有处于PFC动态且延时一段时间
    if ((IsrVars._i16AcDropFlag == 1) && (IsrVars._i16AcUpjumpFlag == 1))
    {
        g_u16PFCphaseADJFLAG =1;//使能PFC电流超前相位补丁
    }
    else //如果处于PFC动态
    {
        g_u16PFCphaseADJFLAG =0;//不使能PFC电流超前相位补丁
    }

    if((g_u16ActionReady == NORMAL_RUN_STATE)&&(g_u16PFCphaseADJFLAG ==1)
            &&(g_i16VpfcCalCnt == 1))
    {
        if ((g_lq10AcVolt.lData > VAC_200V)&&(g_lq10AcVolt.lData <= VAC_245V))
        {
            if ((g_lq10LoadPercent.lData >= LOAD_15_PERCENT)
                    &&(g_lq10LoadPercent.lData < LOAD_25_PERCENT))
            {
                //Output=55.2V,20%
                if ((g_lq10AcFre.lData < FREQUENCY_55Hz)&&(g_lq10AcVolt.lData > VAC_200V)
                        &&(g_lq10AcVolt.lData <= VAC_225V))
                {
                    u16PhaseshiftTemp = (UINT16)23;//25
                }
                else if ((g_lq10AcFre.lData > FREQUENCY_55Hz)&&(g_lq10AcVolt.lData > VAC_235V))
                {
                    u16PhaseshiftTemp = (UINT16)14;
                }
                else
                {
                    u16PhaseshiftTemp = (UINT16)16;
                }
            }
            else if ((g_lq10LoadPercent.lData >= LOAD_25_PERCENT)
                    &&(g_lq10LoadPercent.lData < LOAD_45_PERCENT))
            {
                //Output=55.2V,30-40%

                if (g_lq10AcFre.lData > FREQUENCY_55Hz)
                      {
                        if (g_lq10AcVolt.lData >= VAC_235V)
                                {
                                    u16PhaseshiftTemp = (UINT16)20;//12
                                }
                        else
                            {
                                    u16PhaseshiftTemp = (UINT16)24;//12
                            }


                      }
                else
                      {
                           u16PhaseshiftTemp = (UINT16)24;

                      }
             }


            else if((g_lq10LoadPercent.lData >= LOAD_45_PERCENT)
                     &&(g_lq10LoadPercent.lData < LOAD_65_PERCENT))
                        {
                            if (g_lq10AcFre.lData < FREQUENCY_55Hz)
                            {
                                //Output=55.2V,50%~100%
                                    if ((g_lq10AcVolt.lData >= VAC_225V)&&(g_lq10AcVolt.lData < VAC_235V))
                                                {
                                                    u16PhaseshiftTemp = (UINT16)9;//12
                                                }
                                    else if (g_lq10AcVolt.lData >=VAC_235V)
                                                {
                                                    u16PhaseshiftTemp = (UINT16)8;//10
                                                }
                                    else
                                                {
                                                    u16PhaseshiftTemp = (UINT16)10;//10
                                                }
                            }

                            else
                            {
                                //Output=55.2V,50%~100%
                                    if (g_lq10AcVolt.lData > VAC_235V)
                                                {
                                                    u16PhaseshiftTemp = (UINT16)7;//12
                                                }
                                     else
                                                {
                                                    u16PhaseshiftTemp = (UINT16)6;//10
                                                }
                            }
                        }
            else if((g_lq10LoadPercent.lData >= LOAD_65_PERCENT)
                     &&(g_lq10LoadPercent.lData < LOAD_90_PERCENT))
                        {
                            if (g_lq10AcFre.lData < FREQUENCY_55Hz)
                            {
                                //Output=55.2V,50%~100%
                                    if (g_lq10AcVolt.lData >= VAC_235V)
                                                {
                                                    u16PhaseshiftTemp = (UINT16)8;//12
                                                }

                                    else
                                                {
                                                    u16PhaseshiftTemp = (UINT16)10;//10
                                                }
                            }

                            else
                            {
                                //Output=55.2V,50%~100%
                                    if (g_lq10AcVolt.lData > VAC_235V)
                                                {
                                                    u16PhaseshiftTemp = (UINT16)7;//12
                                                }
                                     else
                                                {
                                                    u16PhaseshiftTemp = (UINT16)6;//10
                                                }
                            }
                        }
            else
            {
                if (g_lq10AcVolt.lData > VAC_235V)
                       {
                         u16PhaseshiftTemp = (UINT16)10;//12
                       }
                else
                        {
                          u16PhaseshiftTemp = (UINT16)9;//10
                        }
            }

        }
        else
        {
            u16PhaseshiftTemp = 0;
        }

       //超前相位补偿值的上下限幅
       if(u16PhaseshiftTemp > PFCPHASESHIFTUPVAL)
       {
           u16PhaseshiftTemp = PFCPHASESHIFTUPVAL;
       }
       else if(u16PhaseshiftTemp < PFCPHASESHIFTCLRVAL)
       {
           u16PhaseshiftTemp = (UINT16)0;
       }
    }
    else //如果没有处于PFC动态且延时一段时间且处于NORMALRUN状态，相位补偿为0
    {
        u16PhaseshiftTemp = 0;
    }

    IsrVars._u16AcVoltPhaseshift = u16PhaseshiftTemp;

/*--------------------------Phase Interleaving Limit---------------------------*/
    if (g_lq10AcVolt.lData >= VAC_278V)
    {
        u16TonShiftLimitTemp = TONSHIFTUPPERLIM;
    }
    else if ((g_lq10AcVolt.lData >= VAC_200V)&&(g_lq10AcVolt.lData <= VAC_276V))
    {
        u16TonShiftLimitTemp = TONSHIFTLOWERLIM;
    }
    else if ((g_lq10AcVolt.lData >= VAC_176V) && (g_lq10AcVolt.lData < VAC_200V))
    {
        //y=540-2.5x
        //TonShiftLimitTemp = (((UINT16)((g_lq10AcVolt.lData) >> 10)))*((UINT16)25);
        //TonShiftLimitTemp = ((UINT16)540) + TonShiftLimitTemp/10;
        //u16TonShiftLimitTemp = (UINT16) (TonShiftLimitTemp);

        s_lq10ShiftLimitTemp.lData = _IQ10mpy(g_lq10AcVolt.lData, (((INT32)25)<<10));
        s_lq10ShiftLimitTemp.lData = _IQ10div(s_lq10ShiftLimitTemp.lData, (((INT32)10)<<10));
        s_lq10ShiftLimitTemp.lData = (((INT32)540)<<10) - s_lq10ShiftLimitTemp.lData;
        u16TonShiftLimitTemp = (UINT16)(s_lq10ShiftLimitTemp.lData >> 10);
    }
    else if ((g_lq10AcVolt.lData >= VAC_115V) && (g_lq10AcVolt.lData < VAC_176V))
    {
         //y=389-1.64x
         //TonShiftLimitTemp = (((UINT16)((g_lq10AcVolt.lData) >> 10)))*((UINT16)164);
         //TonShiftLimitTemp = ((UINT16)389) + TonShiftLimitTemp/100;
         //u16TonShiftLimitTemp = (UINT16) (TonShiftLimitTemp);

        s_lq10ShiftLimitTemp.lData = _IQ10mpy(g_lq10AcVolt.lData, (((INT32)164)<<10));
        s_lq10ShiftLimitTemp.lData = _IQ10div(s_lq10ShiftLimitTemp.lData, (((INT32)100)<<10));
        s_lq10ShiftLimitTemp.lData = (((INT32)389)<<10) - s_lq10ShiftLimitTemp.lData;
        u16TonShiftLimitTemp = (UINT16)(s_lq10ShiftLimitTemp.lData >> 10);

    }
    else
    {
        u16TonShiftLimitTemp = TONSHIFTUPPERLIM;
    }

    if (u16TonShiftLimitTemp > TONSHIFTUPPERLIM)
    {
        u16TonShiftLimitTemp = TONSHIFTUPPERLIM;
    }

    if (u16TonShiftLimitTemp < TONSHIFTLOWERLIM)
    {
        u16TonShiftLimitTemp = TONSHIFTLOWERLIM;
    }

    IsrVars._u16TonShiftLimit = (UINT16)u16TonShiftLimitTemp;  //tangyin debug

//-------------------------------THD COMPENSATE-------------------------------//

//    //THD Ctrl Hysteresis
//    if ((g_lq10MdlVolt.lData >= VDC_53V5) && (g_lq10MdlVolt.lData <= VDC_57V))
//    {
//        g_lq10MdlVoltTHD.lData = g_lq10MdlVolt.lData;
//    }
//    if ((g_lq10MdlVolt.lData <= VDC_51V5) || (g_lq10MdlVolt.lData >= VDC_58V))
//    {
//        g_lq10MdlVoltTHD.lData = g_lq10MdlVolt.lData;
//    }
//
//    //Ac Volt Hysteresis
//    if (!(((g_lq10AcVolt.lData > VAC_90V) && (g_lq10AcVolt.lData < VAC_95V))
//        || ((g_lq10AcVolt.lData > VAC_130V) && (g_lq10AcVolt.lData < VAC_135V))
//        || ((g_lq10AcVolt.lData > VAC_185V) && (g_lq10AcVolt.lData < VAC_190V))
//        || ((g_lq10AcVolt.lData > VAC_245V) && (g_lq10AcVolt.lData < VAC_250V))))
//    {
//        g_lq10AcVoltTHD.lData = g_lq10AcVolt.lData;
//    }
//
//    //Load Percent Hysteresis
//    if ((g_lq10LoadPercent.lData >= LOAD_15_PERCENT)
//            ||(g_lq10LoadPercent.lData <= LOAD_12_PERCENT))
//    {
//        g_lq10LoadPercTHD.lData = g_lq10LoadPercent.lData;
//    }
//
//
//    if (g_u16TestPoint == 0)
//    {
//        if((g_u16ActionReady == NORMAL_RUN_STATE) && (IsrVars._i16AcDropFlag == 1)
//                && (IsrVars._i16AcUpjumpFlag == 1) && (g_lq10MdlVoltTHD.lData >= VDC_53V5)
//                && (g_lq10MdlVoltTHD.lData <= VDC_57V) && (IsrVars._u16SoftStartEndTimer >= DCSTARTEND_5S))
//        {
//
//                if ((g_lq10LoadPercTHD.lData >= LOAD_15_PERCENT) && (g_i16VpfcCalCnt == 1)
//                        && (g_lq10AcVoltTHD.lData >= VAC_190V) && (g_lq10AcVoltTHD.lData <= VAC_245V))
//                {
//                    IsrVars._i16AdIpfcTrue = (INT16)256 + (g_lq10AcIrmsSampSysb.lData >> 1);
//                    IsrVars._i16AdIpfcTrue2 = (INT16)256 - (g_lq10AcIrmsSampSysb.lData >> 1);
//
//                    if(IsrVars._i16AdIpfcTrue<10)
//                      {
//                          IsrVars._i16AdIpfcTrue=10;
//                      }
//                      if(IsrVars._i16AdIpfcTrue2<10)
//                      {
//                          IsrVars._i16AdIpfcTrue2=10;
//                      }
//                      if(IsrVars._i16AdIpfcTrue>512)
//                      {
//                          IsrVars._i16AdIpfcTrue=512;
//                      }
//                      if(IsrVars._i16AdIpfcTrue2>512)
//                      {
//                          IsrVars._i16AdIpfcTrue2=512;
//                      }
//                    IsrVars._i16IQ10TrvIpfcCLA = (INT16)1952;
//                    s_i16TrCompenCoefSet = (INT16)12;
//
//                    if ((g_lq10LoadPercTHD.lData >= LOAD_15_PERCENT) && (g_lq10LoadPercTHD.lData <= LOAD_25_PERCENT))
//                            {
//                                if(g_lq10AcVoltTHD.lData <= VAC_225V)
//                                {
//                                    IsrVars._i16IQ10TrvIpfcCLA = (INT16)1975;
//                                    s_i16TrCompenCoefSet = (INT16)14;
//
//                                }
//                                else
//                                {
//                                    IsrVars._i16IQ10TrvIpfcCLA = (INT16)2050;
//                                    s_i16TrCompenCoefSet = (INT16)13;
//                                }
//
//
//                            }
//                    if ((g_lq10LoadPercTHD.lData >= LOAD_45_PERCENT) && (g_lq10LoadPercTHD.lData <= LOAD_55_PERCENT))
//                            {
//
//
//
//                                   s_i16TrCompenCoefSet = (INT16)16;
//
//
//
//                            }
//                    if ((g_lq10LoadPercTHD.lData > LOAD_55_PERCENT) && (g_lq10LoadPercTHD.lData <= LOAD_80_PERCENT))
//                            {
//
//                                   s_i16TrCompenCoefSet = (INT16)14;
//
//                            }
//
//                }
//                else if ((g_lq10LoadPercTHD.lData >= LOAD_15_PERCENT) && (g_i16VpfcCalCnt == 1)
//                        && (g_lq10AcVoltTHD.lData >= VAC_95V) && (g_lq10AcVoltTHD.lData <= VAC_130V))
//                {
//                    s_i16TrCompenCoefSet = (INT16)12;
//                    IsrVars._i16AdIpfcTrue = (INT16)256 + (g_lq10AcIrmsSampSysb.lData >> 1);
//                    IsrVars._i16AdIpfcTrue2 = (INT16)256 - (g_lq10AcIrmsSampSysb.lData >> 1);
//                    IsrVars._i16IQ10TrvIpfcCLA = (INT16)1952;
//                }
//                else
//                {
//                    //TIF 176V
//                    s_i16TrCompenCoefSet = (INT16)4;
//                    IsrVars._i16AdIpfcTrue = (INT16)300;
//                    IsrVars._i16AdIpfcTrue2 = (INT16)300;
//                    IsrVars._i16IQ10TrvIpfcCLA = (INT16)1152;
//                }
//        }
//        else
//        {
//            s_i16TrCompenCoefSet = (INT16)4;
//            IsrVars._i16AdIpfcTrue = (INT16)0;
//            IsrVars._i16AdIpfcTrue2 = (INT16)0;
//            IsrVars._i16IQ10TrvIpfcCLA = (INT16)1638;
//        }
//    }

}

//===========================================================================
// No more.
//===========================================================================

