//###########################################################################
//
// FILE:    f28004x_adc.c
//
// TITLE:   F28004x ADC Support Functions.
//
//###########################################################################
// $TI Release: F28004x Support Library v1.05.00.00 $
// $Release Date: Thu Oct 18 15:43:57 CDT 2018 $
// $Copyright:
// Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Texas Instruments Incorporated nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// $
//###########################################################################

//
// Included Files
//
#include "f28004x_device.h"      // Header File Include File
#include "Drv_ADC.h"
#include "Prj_macro.h"
#include "Prj_Adc.h"

void InitAdc(void)
{

    EALLOW;

    // for internal reference
    /*AnalogSubsysRegs.ANAREFCTL.bit.ANAREFASEL = 0;
    AnalogSubsysRegs.ANAREFCTL.bit.ANAREFBSEL = 0;
    AnalogSubsysRegs.ANAREFCTL.bit.ANAREFCSEL = 0;
    AnalogSubsysRegs.ANAREFCTL.bit.ANAREFA2P5SEL = 0;
    AnalogSubsysRegs.ANAREFCTL.bit.ANAREFB2P5SEL = 0;
    AnalogSubsysRegs.ANAREFCTL.bit.ANAREFC2P5SEL = 0;*/


    //for External reference
    AnalogSubsysRegs.ANAREFCTL.bit.ANAREFASEL = 1;
    AnalogSubsysRegs.ANAREFCTL.bit.ANAREFBSEL = 1;
    AnalogSubsysRegs.ANAREFCTL.bit.ANAREFCSEL = 1;
    AnalogSubsysRegs.ANAREFCTL.bit.ANAREFA2P5SEL = 0;
    AnalogSubsysRegs.ANAREFCTL.bit.ANAREFB2P5SEL = 0;
    AnalogSubsysRegs.ANAREFCTL.bit.ANAREFC2P5SEL = 0;

    //Set ADCCLK divider to /2
    AdcaRegs.ADCCTL2.bit.PRESCALE=0x02;
    AdcbRegs.ADCCTL2.bit.PRESCALE=0x02;
    AdccRegs.ADCCTL2.bit.PRESCALE=0x02;


    AdcaRegs.ADCCTL1.bit.ADCPWDNZ   = 1;      // Power ADC
    AdcbRegs.ADCCTL1.bit.ADCPWDNZ   = 1;      // Power ADC
    AdccRegs.ADCCTL1.bit.ADCPWDNZ   = 1;      // Power ADC
    DELAY_US(ADC_usDELAY);
    AnalogSubsysRegs.TSNSCTL.bit.ENABLE=1;//$

    //为防止第一个转换结果不准确，第一个转换结果全部采样次要模拟信号
    //EPWM4,ADCSOCA , smaple window 9 cycles
    AdcaRegs.ADCSOC0CTL.bit.TRIGSEL = EPWM4_ADCSOCA;
    AdcaRegs.ADCSOC0CTL.bit.ACQPS = ADC_SHCLK_CH0;
    AdcaRegs.ADCSOC0CTL.bit.CHSEL = DCDCTEMP;//AMBTEMP;

    AdcaRegs.ADCSOC1CTL.bit.TRIGSEL = EPWM4_ADCSOCA;
    AdcaRegs.ADCSOC1CTL.bit.ACQPS = ADC_SHCLK_CH0;
    AdcaRegs.ADCSOC1CTL.bit.CHSEL = DCCURR;//PFCOVP;

    //EPWM4,ADCSOCA , smaple window 9 cycles
   /* AdcaRegs.ADCSOC2CTL.bit.TRIGSEL = EPWM8_ADCSOCA;
    AdcaRegs.ADCSOC2CTL.bit.ACQPS = ADC_SHCLK_CH0;
    AdcaRegs.ADCSOC2CTL.bit.CHSEL = PFCCURR1;//DCCURR;*/

    //AdcaRegs.ADCSOC2CTL.bit.TRIGSEL = EPWM6_ADCSOCA;
    AdcaRegs.ADCSOC2CTL.bit.TRIGSEL = EPWM4_ADCSOCB;
    AdcaRegs.ADCSOC2CTL.bit.ACQPS = ADC_SHCLK_CH0;
    AdcaRegs.ADCSOC2CTL.bit.CHSEL = DCVOLT;

    //EPWM4,ADCSOCA , smaple window 9 cycles
    //AdcaRegs.ADCSOC3CTL.bit.TRIGSEL = EPWM6_ADCSOCA;//
    AdcaRegs.ADCSOC3CTL.bit.TRIGSEL = EPWM4_ADCSOCB;
    AdcaRegs.ADCSOC3CTL.bit.ACQPS = ADC_SHCLK_CH0;
    AdcaRegs.ADCSOC3CTL.bit.CHSEL = FANBAD;//DCDCTEMP;

    //round ,prity same
    AdcaRegs.ADCSOCPRICTL.bit.SOCPRIORITY = 0x02;

    //EPWM8,ADCSOCA , smaple window 9 cycles
    AdcbRegs.ADCSOC0CTL.bit.TRIGSEL = EPWM4_ADCSOCA;
    AdcbRegs.ADCSOC0CTL.bit.ACQPS = ADC_SHCLK_CH0;
    AdcbRegs.ADCSOC0CTL.bit.CHSEL = PFCOVP;//GND_B;

    /*AdcbRegs.ADCSOC1CTL.bit.TRIGSEL = EPWM4_ADCSOCA;
    AdcbRegs.ADCSOC1CTL.bit.ACQPS = ADC_SHCLK_CH0;
    AdcbRegs.ADCSOC1CTL.bit.CHSEL = DCVOLT;//VACSAMP1_L;*/

    AdcbRegs.ADCSOC1CTL.bit.TRIGSEL = EPWM8_ADCSOCA;
    AdcbRegs.ADCSOC1CTL.bit.ACQPS = ADC_SHCLK_CH0;
    AdcbRegs.ADCSOC1CTL.bit.CHSEL = PFCCURR1;

    //EPWM8,ADCSOCA , smaple window 9 cycles
    AdcbRegs.ADCSOC2CTL.bit.TRIGSEL = EPWM8_ADCSOCA;
    AdcbRegs.ADCSOC2CTL.bit.ACQPS = ADC_SHCLK_CH0;
    AdcbRegs.ADCSOC2CTL.bit.CHSEL = VACSAMP2_N;

    //EPWM8,ADCSOCA , smaple window 9 cycles
    AdcbRegs.ADCSOC3CTL.bit.TRIGSEL = EPWM8_ADCSOCA;
    AdcbRegs.ADCSOC3CTL.bit.ACQPS = ADC_SHCLK_CH0;
    AdcbRegs.ADCSOC3CTL.bit.CHSEL = PFCCURR2;

    //EPWM8,ADCSOCA , smaple window 9 cycles
    AdcbRegs.ADCSOC4CTL.bit.TRIGSEL = EPWM8_ADCSOCA;
    AdcbRegs.ADCSOC4CTL.bit.ACQPS = ADC_SHCLK_CH1;
    AdcbRegs.ADCSOC4CTL.bit.CHSEL = DSPTMP;

    //round ,prity same
    AdcbRegs.ADCSOCPRICTL.bit.SOCPRIORITY = 0x01;

    //EPWM8,ADCSOCA , smaple window 9 cycles
    AdccRegs.ADCSOC0CTL.bit.TRIGSEL = EPWM8_ADCSOCA;
    AdccRegs.ADCSOC0CTL.bit.ACQPS = ADC_SHCLK_CH0;
    AdccRegs.ADCSOC0CTL.bit.CHSEL = AMBTEMP;//PFCVSAMP;

    AdccRegs.ADCSOC1CTL.bit.TRIGSEL = EPWM8_ADCSOCA;
    AdccRegs.ADCSOC1CTL.bit.ACQPS = ADC_SHCLK_CH0;
    AdccRegs.ADCSOC1CTL.bit.CHSEL = PFCVSAMP;

    AdccRegs.ADCSOC2CTL.bit.TRIGSEL = EPWM8_ADCSOCA;
    AdccRegs.ADCSOC2CTL.bit.ACQPS = ADC_SHCLK_CH0;
    AdccRegs.ADCSOC2CTL.bit.CHSEL = VACSAMP1_L;

    EDIS;

}
//
// End of File
//
