/*=============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : HD415CU111_warnctrl.c
 *  PURPOSE  : warn judge and on/off control, panel led, relay, fan control
 *
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A000           DSP               Created.   Pre-research
 *
 *============================================================================*/

#ifndef App_cal_H
#define App_cal_H

extern ubitfloat g_lq10SetPowerWant;// actual power limit coefficient of module
extern ubitfloat g_lq10MdlLimit;        // actual rectifir current limit
extern ubitfloat g_lq10MdlLimitDisp;    // current limit for dispaly
extern ubitfloat g_lq10SetPfcVolt;      // wanted pfcvolt set by SCU+
extern INT16     g_i16VpfcCalCnt;

extern ubitfloat g_lq10SetPfcVoltTrue;  // actual pfcvolt set by SCU+
extern ubitfloat g_lq20SourceAdjSysa;   //DC volt refer adjust coefficient a (AC adjust)
extern ubitfloat g_lq10SourceAdjSysb;
extern UINT16 g_u16LimitStatus;         // rectifier current/power limit status
//extern INT16  g_i16VPFCCALFlag;



extern ubitfloat g_lq10PfcVoltRegulate;  //the regulation value of pfc volt

/*******************************************************************************
*variables and functions declare for Circuit calculate
*******************************************************************************/
extern ubitfloat g_lq10MdlLimitCurrent; // actual rectifir current limit Current     by ls 20111128
extern ubitfloat g_lq10SetVoltTrue;     // actual dcvolt set by SCU+
extern ubitfloat g_lq10SetVolt;         // wanted dcvolt set by SCU+
extern ubitfloat g_lq10SetLimit;        // wanted curr limit set by SCU+
extern ubitfloat g_lq10SetLimitTrue;    // actual curr limit set by SCU+
extern ubitfloat g_lq10SetPower;        // wanted power limit set by SCU+
extern ubitfloat g_lq10SetPowerTrue;    // actual power limit set by SCU+
extern UINT16 g_u16CurrLimitStatusFlag; //paul 20210610
extern UINT16  g_u16PfcvoltChgFlag_high;
extern ubitfloat g_lq10AcVoltUse; //计算输入电压回滞的变量，用于输入电压限功率等。
extern ubitfloat g_lq10VpfcVdcRatio; //母线电压和输出电压的比值
extern INT32     g_i32PowerRate;
extern INT32     g_i32CurrRate;
extern INT32     g_i32CurrDisMax;
extern INT32     g_i32CurrLimtMax;
extern INT32     g_i32CurrLimtMaxExtra;
extern ubitfloat g_lPowerAccumTime;//input Power accumulate timer
extern ubitfloat g_lPowerAccum;  //input Power accumulate
extern ubitfloat g_lq10PowerAccumLow; //input Power accumulate low 16 (Q10)
extern ubitfloat g_lSCUGetWattAccum; // scu get power
extern UINT16    g_u16MdlPowerAcuumClearFlg;// input Power accumulate clear flg
extern UINT16    g_u16CurrCompenEnable;
extern void vCircuitCal(void);

#endif

//===========================================================================
// No more.
//===========================================================================
