/*============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : gbb_start.c
 *  PURPOSE  : relay control pfc, dcdc soft start	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A000           DSP               Created.   Pre-research 
 *===========================================================================*/

#include "f28004x_device.h"	    // DSP280x Headerfile Include File
#include  "App_vAdcTreatment.h"
#include  "App_cal.h"
#include  "App_start.h"
#include  "App_warnctrl.h"
#include <App_isr.h>
#include "CLAShared.h"
#include "GBB_constant.h"
#include <App_constant.h>
#include <Prj_macro.h>
#include <App_main.h>
#include "IQmathLib.h"
#include "GBB_epromdata.h"
#include  "App_vEventLog.h"
#include  "Prj_config.h"
/******************************************************************************
*variables definition:these variables only can use in this file 														  
******************************************************************************/
static longunion s_lSSVsetMax;      //max dcdc volt ref 
static longunion s_lSSIsetMax;	    //max dcdc curr ref 
static longunion s_lq10VoltWalkIn;  //volt ref for walk in control 
static longunion s_lq10WalkIn;       //curr ref for walk in control
static INT16 s_i16SoftStartPwm;		//temp variable for PWM soft start    
static INT16 s_i16SoftStartSet;		//temp variable for PFC volt soft start 
static ubitfloat lq10WalkInTmp;     //for walk in
static ubitfloat s_lq10WalkInTimeUse;//walk in time for really use based on 3500W update xgh
INT16 i16WalkinFlag;	
/******************************************************************************
*globle variables definition:these variables  can use in all of the files 														  
******************************************************************************/
//UINT16 g_u16ActionReady;		     //rectifier soft start state 
UINT16 g_u16MainRlyOpenFlag;       //main rly open flag  
UINT16 g_u16MainRlyCloseFlag;      //main rly close flag 
UINT16 g_u16SequeOnTimer;	         //count for rectifer open sequencely 
UINT16 g_u16OverRlyOpenFlag;
//ubitfloat g_iq10OpenTime;	 	     //the interval time of module working orderly(s) 
//ubitfloat g_iq10WalkInTime;	     // rectifier start time for current walk in 

UINT16 g_u16DisableOn;              // for fault record, disable soft start
longunion g_lFailFlag;			//PFC OR DC FAIL FLAG
/******************************************************************************
*functions declare:these functions only can use in this file                                    
******************************************************************************/
//pfc software soft start initialization
void vPfcSwSoftStartInitial(void);

//calculate max dcdc volt ref 
void vCalMaxDcVoltRef(void);

//calculate max dcdc curr ref 
void vCalMaxDcCurrRef(void);

//reset DC ocp trip flag
void vResetDcOcpTripFlag(void);
	
//initial dc piout and pwm duty
void vInitialDcPwmVar(void);

//DCDC ????????????????
static void vDcSoftstartInit(void);

/******************************************************************************
 *Function name: vSstartCtrl()  
 *Description :  Rectifier soft start treatment (relay, pfc start, dcdc start )                                                    
 *input:         void                                  
 *global vars:   void               
 *output:        void 
 *CALLED BY:    main()
 *****************************************************************************/
void   vSstartCtrl(void)
{
    ubitfloat lTmp;

    UINT16 u16PreCurlimDelay;


    if (g_u16ActionReady == POWON_STATE)
    {
        //open main relay,close over relay,
        //begin soft start PFC by hardware

        mOffHmosDriv();//lzy 20200312

        mMainRelayOpen();

        mOverRelayClose();
        //g_u16MainRlyCloseFlag = 0;
        g_u16MainRlyCloseFlag = MAINRLY_ZVS_CTL_OPEN;
        //disable DCDC PWM
        mOffDcdcPwm();
        //disable pfc pwm
        mOffPfcPwm();

        //set PFC closed flag
        IsrVars._u16PfcOpened = 0;
        IsrVars._u16PwmEnableFlag = 0;

        //set FAN closed flag
        //g_u16FanEnable = FAN_RUN_DISABLE;
        //set rectifier off flag
        g_u16MdlStatus.bit.OFFSTAT = 1;
        g_u16PfcvoltChgFlag_high = 0;
        //to next step
        // for PFC and DCDC fail record, mhp 20120905
        if (g_u16DisableOn == 0)
        {
            // permit on if no fault record
            g_u16ActionReady = PFC_HWSOFT_START;
        }
        //clr timer cnt for 2s delay begin
        IsrVars._u16MinTimer = 0;
         g_u16FanEnable = FAN_RUN_DISABLE;

    }

    else if(g_u16ActionReady == PFC_HWSOFT_START)
    {
           //soft start delay 2s,then to next step
           //????????????????????????????????????10HZ?????
       if(IsrVars._u16MinTimer >= TIME_2S_ISRTIMER)
        {
                //clr timer cnt
               // IsrVars._u16MinTimer = TIME_2S_ISRTIMER;

           //???��?????,?????????WALKIN?????????????????????WALKIN???????????????
           //???????????????????????????4S??��?????????????PFC????????????4S
           if(   (IsrVars._lVdcDisUse.iData.iHD < VDC_AD_WITH_BATT)
              || (g_u16MdlCtrl.bit.WALKIN)
              || (g_lq10TempAmbi.lData <= N_TEMP_27C))
            {
                 u16PreCurlimDelay = TIME_2S_ISRTIMER;
            }
            else
            {
                 u16PreCurlimDelay = TIME_6S_ISRTIMER;
            }
            if(IsrVars._u16MinTimer >= u16PreCurlimDelay)
            {
                  //to next step
                  g_u16ActionReady = PFC_SWSOFT_START_INIT;
                  IsrVars._u16MinTimer = 0;
                  //reset DC ocp trip flag ???PFCOVP ??DCOCP?????????��??
                 vResetDcOcpTripFlag();//???2S??????TZ?��????????PFC 500V??????TZ?��????????????????��?
            }
        }

    }

    else if (g_u16ActionReady == PFC_SWSOFT_START_INIT)
    {
        // AC voltage is normal and pfc pwm enable
        if((g_lq10AcVolt.lData >= VPFC_SSTART_VAC_LIM)
            && (g_lq10PfcVolt.lData >= VPFC_SSTART_VPFC_LIM)
            && (g_lq10PfcVolt.lData <= VPFC_450V)
            && (g_u16UnPlugFlag == 0)//lzy 20200312
            && (((IsrVars._u16OverAcVolt == 0)
            &&(g_u16MdlStatus.bit.ACOV == 0))
            ||(IsrVars._u16PermitOverAc == PERMIT_ACOVER)))

        {
            //control main relay zvs on
            vMainRlyOnOffCtrl();
 
            if(g_u16MainRlyCloseFlag == MAINRLY_ZVS_CTL_CLOSE)
            {
                //clr main rly congtrol flag
                g_u16MainRlyCloseFlag = MAINRLY_ZVS_CTL_OPEN;
                //???????????��???10ms?????????????10ms???PFC????

                //pfc softstart initial
                vPfcSwSoftStartInitial();

                //disable pfc regulate voltage flag
                //enable pfc pwm
                mOnPfcPwm();
                mOnHmosDriv();//lzy 20200312
                IsrVars._u16PwmEnableFlag = 1;

                //to next step
                g_u16ActionReady = PFC_SWSOFT_START;

                //clear the sourcedynamic flag
                IsrVars._i16AcUpjumpFlag = 1;
                IsrVars._i16AcDropFlag = 1;
                //clr time cnt for next step
                IsrVars._u16MinTimer = 0;

                //set FAN enable flag
                g_u16FanEnable = FAN_RUN_ENABLE;
           }
       }
    }

    else if (g_u16ActionReady == PFC_SWSOFT_START)
    {
        //  1ms one times, 100 times
        if(IsrVars._u16MinTimer >= TIME1MS_ISRTIMER)
        {
            //clr time cut by 1ms
            IsrVars._u16MinTimer = 0;
            IsrVars._i16DCDCPowerSetDyn =  POWER_LIM_UPPER;
            //************calculate Ton permit*******************
            s_i16SoftStartPwm += SSTART_PFCPWM_STEP;

            if(s_i16SoftStartPwm >= PFC_TON_PERMIT)//PFC_DUTY_MAX
            {
                s_i16SoftStartPwm = PFC_TON_PERMIT;
            }

            IsrVars._i16PfcDutyPermit = s_i16SoftStartPwm ;
            IsrVars._i16PfcMaxPower += SSTART_PFCMAXPOW_STEP; //STEP=10
            if(IsrVars._i16PfcMaxPower >= PFC_MAXPOW) // MAX POW =2500
            {
                IsrVars._i16PfcMaxPower = PFC_MAXPOW;
            }
            //************calculate pfc ref********************

            lTmp.lData = _IQ10mpy(g_lq10VpfcConSys.lData,g_lq10PfcVoltRegulate.lData);
            lTmp.lData = _IQ10mpy(IQ10_DEF_VPFCFACTOR,lTmp.lData)>>10;

            //1Vpfc -->8,400Vpfc -->3200  90v*1.414=127*8~=1000
            s_i16SoftStartSet += SSTART_PFCVOL_STEP;

            if(s_i16SoftStartSet >= lTmp.iData.iLD)
            {
               s_i16SoftStartSet = lTmp.iData.iLD;
            }

            //if(s_i16SoftStartSet >= lTmp.iData.iLD)
            if((s_i16SoftStartSet >= lTmp.iData.iLD)
              && (IsrVars._i16PfcMaxPower >= PFC_MAXPOW))
            {
                //Pfc soft start end
                //s_i16SoftStartSet = lTmp.iData.iLD;

                IsrVars._i16PfcDutyPermit = PFC_TON_PERMIT;//PFC_DUTY_MAX;

                g_lq10SetPfcVoltTrue.lData = g_lq10PfcVoltRegulate.lData;

                if(IsrVars._lVpfcUse.iData.iHD >= VPFC_AD_320V)
                {
                   //??PFCOPEN FLAG????????????????320V????1.
                   //????320?????��??PFVUV????
                     IsrVars._u16PfcOpened = 1;

                    if(IsrVars._lVpfcUse.iData.iHD >= VPFC_AD_DC_START)
                     {

                        //enable pfc regulate voltage
                        //IsrVars._u16PfcLoopCount = 1 ;
                       //?????????-25V???DC???????????????,25V????
//                      g_u16VpfcAdDcStart =   \
//                            (INT16)((((g_lq10PfcVoltRegulate.lData >> 10)-25)*IQ10_DEF_VPFCFACTOR)>>10);
                       //to next step
                         g_u16ActionReady = DC_SOFT_START_INIT;

                           //clr time cnt for next step
                         IsrVars._u16MinTimer = 0;

                         //??PFCOPEN FLAG??????????????????20V????1
                         //set PFC open flag for  pfc protect
                         //IsrVars._u16PfcOpened = 1;

                            //clr seque on timer cnt
                           g_u16SequeOnTimer = 0;
                           lq10WalkInTmp.lData = 0;
                       }
                  }
             }
            IsrVars._i16VpfcSet = s_i16SoftStartSet;
        }
    }

    else if (g_u16ActionReady == DC_SOFT_START_INIT)
    {

        if( (IsrVars._lVpfcUse.iData.iHD >= VPFC_AD_DC_START)
            &&(g_u16MdlStatus.bit.DCOV == 0)
            &&(g_u16MdlStatusExtend.bit.HWHVSD==0)
            &&(g_u16MdlCtrl.bit.OFFCTRL == 0)
            &&(g_u16MdlStatus.bit.HVSDLOCK == 0) )
        {
            vDcSoftstartInit();
        }
        else
        {
            g_u16SequeOnTimer = 0;
        }

    }


    else if (g_u16ActionReady == DC_SOFT_START)
    {
        //???????????WALKIN?????????????????��?????????
        //????��??????DC?څ???????????????څ?????????????????????
        //???????????????��?????????????
        // s_lq10VoltWalkIn.lData????????vCalMaxDcVoltRef()?????s_lSSVsetMax.lData;
        //?????????????vCalMaxDcVoltRef();
        vCalMaxDcVoltRef();

        //calculate max dcdc curr ref 
        vCalMaxDcCurrRef();
        // for walk-in enable
        if((g_u16MdlCtrl.bit.WALKIN) || (g_lq10TempAmbi.lData <= N_TEMP_27C))//xgh walkin test
    //  if(lq10WalkInTmp.lData)
        {
            if(IsrVars._u16MinTimer >= TIME5MS_ISRTIMER)//22  5ms one times
            {
                IsrVars._u16MinTimer = 0;

                //PWM limit soft open

                s_i16SoftStartPwm += SSTART_DCDCPWM_STEP ;
                //??????????Idcpiout?????????????????????????????��??????��??????
                //???????Idcpiout?????????????????????????��?????????????????????????��?��?????��?
                //??PWM?????????????????????????????????????????????
            //  if(s_i16SoftStartPwm >= IsrVars._i16DcdcPWMTsMax + SSTART_DCTS_DELTA)
                if(s_i16SoftStartPwm >= DC_TS_MAX_40K + SSTART_DCTS_DELTA)
                {
                    //s_i16SoftStartPwm = IsrVars._i16DcdcPWMTsMax + SSTART_DCTS_DELTA;
                    s_i16SoftStartPwm = DC_TS_MAX_40K + SSTART_DCTS_DELTA;
                }

                IsrVars._i16IdcPioutPermit = s_i16SoftStartPwm ;
                CputoClaVar._i16IdcPioutPermitCLA = s_i16SoftStartPwm ;
                //*********current  limit walk in **********//
                s_lq10WalkIn.lData += lq10WalkInTmp.lData;


                if(s_lq10WalkIn.lData > s_lSSIsetMax.lData)
                {
                    s_lq10WalkIn.lData = s_lSSIsetMax.lData;
                }

                //***********note:below programme need test  20081204 cxm****//
                //*********voltage walk in**********//
             /*
             if (g_lq10WalkInTime.lData > g_lq10LowTempWalkInTime.lData )//xgh for wlkin test
               {
                 s_lq10WalkInTimeUse.lData = g_lq10WalkInTime.lData;//xgh for wlkin test
                }
             */

             if(g_lq10LowTempWalkInTime.lData >= g_lq10WalkInTime.lData)
              {
                 s_lq10WalkInTimeUse.lData = g_lq10LowTempWalkInTime.lData;
              }
             else
              {
                  s_lq10WalkInTimeUse.lData = g_lq10WalkInTime.lData;
              }


                s_lq10VoltWalkIn.lData
                  +=  s_lSSVsetMax.lData / (s_lq10WalkInTimeUse.lData >> 9);//xgh based on 3500W update
                
                if(s_lq10VoltWalkIn.lData >= s_lSSVsetMax.lData)
                {
                    s_lq10VoltWalkIn.lData = s_lSSVsetMax.lData;
                }

            if((s_lq10WalkIn.lData >= s_lSSIsetMax.lData)
                &&(s_lq10VoltWalkIn.lData >= s_lSSVsetMax.lData))
                {
                    //dcdc soft over

                    IsrVars._i16CurrLimFloor = POWER_LIM_FLOOR;
                    g_u16ActionReady = NORMAL_RUN_STATE;
					
 		    g_u16MdlStatusExtend.bit.DCOCP_PFCOVP = 0;//??????????????????
					
                    g_u16MdlStatus.bit.OFFSTAT = 0;
                    g_lq10SetVoltTrue.lData = g_lq10SetVolt.lData;  //must be add
                }
                    //*********sent to isr loop*******
                IsrVars._i16IdcdcSysTrue = (INT16)(s_lq10WalkIn.lData >> 10);
                IsrVars._i16IdcdcSys = (INT16)(s_lq10WalkIn.lData >> 10);
                IsrVars._i16VdcSet  = (INT16)(s_lq10VoltWalkIn.lData >> 10);
            }
        }
        else                        // for walk-in disable
        {
            //685us one times, 170 times ,1160ms soft start time
            if(IsrVars._u16MinTimer >= TIME685US_ISRTIMER)
            {
                IsrVars._u16MinTimer = 0;

                //PWM limit soft open  //80K=750 250K=240 (750-240)/3=170*685us=116ms
                s_i16SoftStartPwm += SSTART_DCDCPWM_STEP;//3

            //  if(s_i16SoftStartPwm >= IsrVars._i16DcdcPWMTsMax + SSTART_DCTS_DELTA)
                if(s_i16SoftStartPwm >= DC_TS_MAX_40K + SSTART_DCTS_DELTA)
                {
                    //s_i16SoftStartPwm = IsrVars._i16DcdcPWMTsMax + SSTART_DCTS_DELTA; //????PERMIT????????70K???
                    s_i16SoftStartPwm = DC_TS_MAX_40K + SSTART_DCTS_DELTA;
                }
                IsrVars._i16IdcPioutPermit = s_i16SoftStartPwm;
                CputoClaVar._i16IdcPioutPermitCLA = s_i16SoftStartPwm;

                //dcdc soft start over

                //dcdc soft start over WALKIN_STEP_LOWTMP_CNT
              
                   if( s_lq10VoltWalkIn.lData < (VDC_AD_42V<<10))
                    {
                         s_lq10VoltWalkIn.lData +=  (s_lSSVsetMax.lData/WALKIN_STEP_CNT);
                    }
                   else
                   {
                       s_lq10VoltWalkIn.lData +=  (s_lSSVsetMax.lData/WALKIN_STEP_CNT2);
                   }
                if(s_lq10VoltWalkIn.lData >= s_lSSVsetMax.lData)
                {
                    s_lq10VoltWalkIn.lData = s_lSSVsetMax.lData ;
                    g_u16ActionReady = NORMAL_RUN_STATE;

		    g_u16MdlStatusExtend.bit.DCOCP_PFCOVP = 0;//??????????????????
			
//                  g_u16RunFlag.bit.PFCUVCHECKTIMER = 0;      //ls add 20120113
                    g_u16MdlStatus.bit.OFFSTAT = 0;
                    IsrVars._i16CurrLimFloor = POWER_LIM_FLOOR;
                    //for calculate use
                    g_lq10SetVoltTrue.lData = g_lq10SetVolt.lData;  //must be add
                }
                //initial dcdc curr limit
                //IsrVars._i16IdcdcSysTrue = s_lSSIsetMax.lData >> 10;
                IsrVars._i16IdcdcSysTrue = ((INT16)(s_lSSIsetMax.lData >> 10));
                IsrVars._i16VdcSet = (INT16)(s_lq10VoltWalkIn.lData >> 10);
                IsrVars._i16IdcdcSys = IsrVars._i16IdcdcSysTrue;

            }
        }
    }

     //else if (g_u16ActionReady >= NORMAL_RUN_STATE)
    //if (g_u16ActionReady >= DC_SOFT_START_INIT) //???DC????????��???DCOCP

    //R48-3000E3 TZ????PFCOV??DCOCP???
    //???DCOCP??PFCOV????TZ??????PFC????????
    //if (g_u16ActionReady >= PFC_HWSOFT_START)
    if (g_u16ActionReady >= PFC_SWSOFT_START)//20200327 for PFCOVPTZ
    {
        if(mDC_TZ_FLG())
        {
          //close pfc and dcdc PWM
        //  mOffDcdcPwm();
        //    mOffPfcPwm();
            IsrVars._u16DcdcOCPPfcOVP ++ ;
            if(IsrVars._u16DcdcOCPPfcOVP > 30000)
            {
                IsrVars._u16DcdcOCPPfcOVP = 30000;
            }
            else
            {
                g_u16EpromWrExtend.bit.DCOCP_PFCOVP = 1;
            }
            vModelTurnOff_PfcDc();//  ????PFCDC????
 
            EALLOW;                                 //DC ocp TZ4
            EPwm4Regs.TZCLR.bit.OST = 1;            //clear ost flag
            EPwm5Regs.TZCLR.bit.OST = 1;            //
            EPwm6Regs.TZCLR.bit.OST = 1;
            EPwm7Regs.TZCLR.bit.OST = 1;
            //20200327 for PFCOVPTZ
            EPwm1Regs.TZCLR.bit.OST = 1;
            EPwm3Regs.TZCLR.bit.OST = 1;
            EDIS;

            g_u16MdlStatusExtend.bit.DCOCP_PFCOVP = 1;
        //  g_u16ActionReady = PFC_HWSOFT_START;
        //  IsrVars._u16PfcOpened = 0;              //set PFC closed flag
        //  g_u16MdlStatus.bit.OFFSTAT = 1;         // set Close Mdl flag


        }
    }


}

/*******************************************************************
 *Function name: vPfcSwSoftStartInitial()  
 *Description :  pfc software soft start initialization                                                    
 *input:         void                                  
 *global vars:   void               
 *output:        void 
 *CALLED BY:     vSstartCtrl()
 *********************************************************************/
void vPfcSwSoftStartInitial(void)
{
    IsrVars._lVpfcPiOut.lData = 0;
    IsrVars._i16VpfcPiTmp = 400;
    IsrVars._i16VpfcErr0 = 0;
    IsrVars._i16VpfcPiOutMax = 11600;
    s_i16SoftStartPwm = PFC_TON_MIN;//12
    IsrVars._i16PfcMaxPower = 400;//5*100=500W
    IsrVars._i16PfcDutyPermit = PFC_TON_MIN;//12

    IsrVars._i16VpfcSet = (INT16)IsrVars._lVpfcUse.iData.iHD;
    s_i16SoftStartSet = IsrVars._i16VpfcSet;
    //??????EPWM1,3???????2??4?????
    EPwm1Regs.TBPRD = PFC_PRD_DEF;
    //EPwm1Regs.CMPA.all =  PFC_PRD_DEF + 1;
    EPwm1Regs.CMPA.bit.CMPA =  100;

    EPwm3Regs.TBPRD = PFC_PRD_DEF;
    //EPwm3Regs.CMPA.all =  PFC_PRD_DEF + 1;
     EPwm3Regs.CMPA.bit.CMPA =  100;
    Cla1ForceTask1();

}


/******************************************************************************
 *Function name: vCalMaxDcVoltRef()  
 *Description :  calculate max dcdc volt ref                                                     
 *input:         void                                  
 *global vars:   g_iq10SetVolt:wanted dcvolt set by SCU+
                 lSSVsetMax: max dcdc volt ref              
 *output:        void 
 *CALLED BY:     vSstartCtrl()
 *****************************************************************************/
void vCalMaxDcVoltRef(void)
{
    s_lSSVsetMax.lData
    = (_IQ10mpy(g_lq10SetVolt.lData,g_lq12VoltConSysa.lData) >> 2)
    + (g_lq12VoltConSysb.lData >> 2);
    s_lSSVsetMax.lData
    =  _IQ10mpy(s_lSSVsetMax.lData,IQ10_DEF_VDCFACTOR) >> 10;

    if(s_lSSVsetMax.lData > VDC_AD_MAX)
    {
        s_lSSVsetMax.lData = VDC_AD_MAX;
    }
    else if(s_lSSVsetMax.lData <= 0)
    {
        s_lSSVsetMax.lData = 0;
    }

    s_lSSVsetMax.lData = s_lSSVsetMax.lData << 10;
}

/******************************************************************************
 *Function name: vCalMaxDcCurrRef()  
 *Description :  calculate max dcdc curr ref                                                     
 *input:         void                                  
 *global vars:   iq10SetLimit:wanted curr limit set by SCU+
                 lSSIsetMax: max dcdc curr ref              
 *output:        void 
 *CALLED BY:     vSstartCtrl()
 *****************************************************************************/
void vCalMaxDcCurrRef(void)
{
    s_lSSIsetMax.lData = _IQ10mpy(g_lq10SetLimit.lData,CURR_RATE);
    s_lSSIsetMax.lData = _IQ10mpy(s_lSSIsetMax.lData,g_lq10CurrConSysa.lData)
    + g_lq10CurrConSysb.lData;
    s_lSSIsetMax.lData
    = _IQ10mpy(s_lSSIsetMax.lData,IQ10_DEF_IDCSYSFACTOR) >> 10;


    if(s_lSSIsetMax.lData > IDC_SYS_MAX)
    {
       s_lSSIsetMax.lData = IDC_SYS_MAX;
    }
    else if(s_lSSIsetMax.lData <= 0)
    {
        s_lSSIsetMax.lData = 0;
    }

    s_lSSIsetMax.lData = s_lSSIsetMax.lData << 10;
}

/******************************************************************************
 *Function name: vResetDcOcpTripFlag()  
 *Description :  reset DC ocp trip flag                                                     
 *input:         void                                  
 *global vars:   void
 *output:        void 
 *CALLED BY:     vSstartCtrl()
 *****************************************************************************/
void vResetDcOcpTripFlag(void)
{


    //DC ocp TZ,clr trip flag
    EALLOW;
    EPwm4Regs.TZCLR.bit.OST = 1;//Latched Status Flag for A One-Shot Trip Event
    EPwm5Regs.TZCLR.bit.OST = 1;
    EPwm6Regs.TZCLR.bit.OST = 1;
    EPwm7Regs.TZCLR.bit.OST = 1;
    //20200327 for PFCOVPTZ
    EPwm1Regs.TZCLR.bit.OST = 1;
    EPwm3Regs.TZCLR.bit.OST = 1;
    //EPwm1Regs.TZCLR.bit.INT = 1;//Latched Trip Interrupt Status Flag
    EDIS;

    //enable //EPWM1_TZINT (EPWM1)
    PieCtrlRegs.PIEIER2.bit.INTx1 = 1;
}

/******************************************************************************
 *Function name: vInitialDcPwmVar()  
 *Description :  initial dc piout and pwm duty                                                     
 *input:         void                                  
 *global vars:   void
 *output:        void 
 *CALLED BY:     vSstartCtrl()
 *****************************************************************************/
void vInitialDcPwmVar(void)
{
    //initial piout & pwm duty
    IsrVars._i16IdcPioutPermit = 200;//DC_MIN_CMPA;
    IsrVars._lDcdcPWMTsShadow.iData.iHD= DC_TIMER_TBPRD*2;
    IsrVars._lDcdcDutyShadow.iData.iHD = 0;
    CputoClaVar._i16IdcPioutPermitCLA = 200;//DC_MIN_CMPA;
    IsrVars._i16VdcSet  = 0;
    IsrVars._lVdcPiOut.lData = 0;
    Cla1ForceTask1();

    //enable epwm5 cla int
    EPwm5Regs.ETSEL.bit.INTEN = 1;
}

/*===========================================================================*
 *Function name: vMainRlyCtrl()  
 *Description :  control main relay to zvs on                                                    
 *input:         void                                  
 *global vars:   void               
 *output:        void 
 *CALLED BY:    vSstartCtrl() and vOnOffCtrl()
 *modify data:  20081222               
 *===========================================================================*/
void vMainRlyOnOffCtrl(void)
{
     if(g_u16MainRlyCloseFlag == MAINRLY_ZVS_CTL_OPEN)
    {
        //g_u16MainRlyCloseFlag = 1;
        g_u16MainRlyCloseFlag = MAINRLY_ZVS_CTL_START;
        IsrVars._u16MainRly_Timer = 0;
    }

    else if((g_u16MainRlyCloseFlag == MAINRLY_ZVS_CTL_START)
            &&((IsrVars._i16AdAcVoltTrue <= VAC_AD_20V)
            ||(IsrVars._u16MainRly_Timer >= DEFWAVEPROIDMAX)))   // 30ms
    {
        g_u16MainRlyCloseFlag=MAINRLY_ZVS_CTL_CHECK;
        IsrVars._u16MainRly_Timer = 0;
    }

     //30ms-3.22ms(Rlydelay)=16.78ms /0.16=104.875
     else if((g_u16MainRlyCloseFlag == MAINRLY_ZVS_CTL_CHECK)
            &&(IsrVars._u16MainRly_Timer >= DeFRLYDELAYACT))
    {
        if((g_u16MdlStatus.bit.ACUV) ||
        (g_u16MdlStatus.bit.ACOV)||(g_u16MdlCtrl.bit.SHORTRELAY))
        {
            mMainRelayOpen();      //main relay zvs close

        }
        else
        {
            mMainRelayClose();     //main relay zvs close

        }

        IsrVars._u16MainRly_Timer = 0;
        g_u16MainRlyCloseFlag = MAINRLY_ZVS_CTL_CLOSE;
    }
}

/*===========================================================================*
 *Function name: vDcSoftstartInit(void) 
 *Description :  //DCDC ????????????????                                                    
 *input:         void                                  
 *global vars:   void               
 *output:        void 
 *CALLED BY:    vSstartCtrl() 
 *modify data:  20140417              
 *===========================================================================*/
static void vDcSoftstartInit(void)
{
    ubitfloat lq10Tmp;
    //calculate max dcdc volt ref 
    vCalMaxDcVoltRef();
    //calculate max dcdc curr ref 
    vCalMaxDcCurrRef();

    //initial dcdc voltage and curr
    s_lq10VoltWalkIn.lData = 0;
    s_lq10WalkIn.lData = 0;
    // if not start inturn or uiSequeOnTimer is arrived ,dcdc on
    if( (g_lq10OpenTime.lData == 0)
        ||(g_u16SequeOnTimer >= ( (UINT16)(g_lq10OpenTime.lData >> 10) * (1 + g_u16MdlAddr) ) ) )
    {
        // for walk-in enable
        if( (g_u16MdlCtrl.bit.WALKIN)
          ||(g_lq10TempAmbi.lData <= N_TEMP_27C) )
        {
            //initial dcdc curr limit
            IsrVars._i16IdcdcSysTrue = 0 ;
            IsrVars._i16CurrLimFloor = 10 ;

            /////judge if there is a  battery //without battery
            if(IsrVars._lVdcDisUse.iData.iHD < VDC_AD_WITH_BATT)
            {
                s_lq10WalkIn.lData = s_lSSIsetMax.lData;
            }
            else                             //with battery
            {
                s_lq10VoltWalkIn.lData = s_lSSVsetMax.lData;
            }

            if(g_lq10LowTempWalkInTime.lData >= g_lq10WalkInTime.lData)
            {
                lq10WalkInTmp.lData = g_lq10LowTempWalkInTime.lData;
            }
            else
            {
                lq10WalkInTmp.lData = g_lq10WalkInTime.lData;
            }

            s_lq10WalkInTimeUse.lData = lq10WalkInTmp.lData;//xgh based on 3500W update
            //**********calculate current limit walk in setp ********//
            //GR947 :90%  Load Tmin=8s;   Design T=8.5
            //       100% Load Tmax=10s   Design T=8.5/0.9=9.5S
            //By default:Tset=10s Unit=10ms So: WalkInTime=10s*100=1000
            //Time= WalkInTime*0.95*10mS/5mS=WalkInTime*1.9
            //By test Set K=1.44 ;
            //So 1.9*1.44=2.736  Q10=2800
            ///////////////////////////////////////////////////////////

            lq10Tmp.lData = _IQ10mpy(lq10WalkInTmp.lData,WALKIN_TIME_K);
            lq10WalkInTmp.lData = _IQ10div(s_lSSIsetMax.lData,lq10Tmp.lData);

        }
        else                            // for walk-in disable
        {
            //initial dcdc curr limit
            IsrVars._i16IdcdcSysTrue = ((INT16)(s_lSSIsetMax.lData >> 10));
            IsrVars._i16CurrLimFloor = POWER_LIM_FLOOR; //10% rate power
            lq10WalkInTmp.lData = 0;

        }

        IsrVars._i16IdcdcSys = IsrVars._i16IdcdcSysTrue;

        //initial dc piout and pwm duty
        vInitialDcPwmVar();

        //delay 20us
        DELAY_US(20);

        //reset DC ocp trip flag ???PFCOVP ??DCOCP?????????��??
        //vResetDcOcpTripFlag();

        //set module on flag
        g_u16MdlStatus.bit.OFFSTAT = 0;     // set to 'on' state

        //to next step
        g_u16ActionReady = DC_SOFT_START;
        IsrVars._u16MinTimer = 0;
        s_i16SoftStartPwm = DCPWM_TS_MIN;

        //enable pfc regulate voltage
        // IsrVars._u16PfcLoopCount = 1 ;
        //enable dcdc pwm
        if((EPwm1Regs.AQCSFRC.all == 0x00) && (EPwm3Regs.AQCSFRC.all == 0x00))//?��?PFC??????????????
        {
            mOnDcdcPwm();
        }
        else  //???PFC??????????��PFC??????????????????????????????
        {
            //g_u16ActionReady = DC_SOFT_START_INIT;
            //g_u16MdlStatus.bit.OFFSTAT = 1;
            //  IsrVars._u16PfcLoopCount = 0;

            vModelTurnOff_PfcDc();//???PFC???????????????????????????????
        }

     }
}
//=============================================================================
// No more.
//=============================================================================

