;#########################################################
; Date: from 2019.8  , fuhuaxing
; Version:1.00     Change Date:
; FileName: HD415GU111_IsrVariableDefs.asm
;#########################################################

	;************ DP = #__AdcaResultRegs =#0B00H **************
;   _ADC_RESULT_MIRROR_REGS offset defines
_TEMPA 		    .set 	0000H	;_AdccResultRegs Result0
_PFCOVP  		.set 	0000H	;_AdcbResultRegs Result0
_DCCURR 		.set 	0001H	;_AdcaResultRegs Result1
_DCTEMP  		.set 	0000H	;_AdcaResultRegs Result0
;_DCVOLT 	   	.set 	0001H	;_AdcbResultRegs Result1
_DCVOLT 	    .set 	0002H	;_AdcbResultRegs Result2
_FANBAD    	    .set 	0003H	;_AdcaResultRegs Result3
_VACIN1 	   	.set 	0002H	;_AdccResultRegs Result2
_VACIN2 	   	.set 	0002H	;_AdcbResultRegs Result2
_PFCVSAMP  		.set 	0001H	;_AdccResultRegs Result1

_VACSAMP  		.set 	0003H	;_AdccResultRegs Result3  ;todo check it
;	.endif
		
