

/*=============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : HD415CU111
 *
 *  FILENAME : HD415CU111_vEventLog.c
 *  PURPOSE  : read and write eventlog data from/into eeprom	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *   2015-04-13        A000           zlm        Created.   Pre-research 
 *	
 *============================================================================*/
#ifndef    App_vEventLog_H
#define    App_vEventLog_H
//zlm
extern  ubitfloat g_lq10CurrentRunTime;         //record run time byself
extern  ubitfloat g_lq10BaseRunTime;            //record run time byself from eeprom
extern  UINT16    g_u16AddressPointer;          //eeprom address pointer in alarm record zone


extern  UINT16  *g_pReadEepromData;
extern  UINT16  g_u16ReadEepromStartAddr;

extern  UINT16  *g_pReadEepromClibData;
extern  UINT16  g_u16ReadEepromClibStartAddr;

extern  UINT16  g_u16ClearEepromStartAddr;
extern  UINT16  g_u16ClearNumber;
extern  UINT16  g_u16ClearEepromOk;
extern  UINT16  g_u16AlarmLogEnable;
struct MDLSTAT_BITS {         // bits  description
    Uint16  OFFSTAT:1;      // 0    on/off state        1:off   0:on
    Uint16  DCOV:1;             // 1    dc over voltage     1:OV    0:normal
    Uint16  AMBIOT:1;           // 2    ambi over temp      1:OT    0:normal
    Uint16  RECTOT:1;           // 3    rect over temp      1:OT    0:normal
    Uint16  FANFAIL:1;          // 4    fan fail            1:fail  0:normal
    Uint16  ACUV:1;             // 5    AC under voltage    1:UV    0:normal
    Uint16  ACOV:1;             // 6    AC over voltage     1:OV    0:normal
    Uint16  PFCUV:1;            // 7    PFC under voltage   1:UV    0:normal
    Uint16  PFCOV:1;            // 8    PFC over voltage    1:OV    0:normal
    Uint16  CALIBRFAIL:1;       // 9    calibrat data fail  1:fail  0:normal
    Uint16  NOCONTROLLER:1;     // 10   can't com with controller   1:
    Uint16  IUNBALMAJOR:1;      // 11   load seriously unbalance    1:
    Uint16  IUNBALMINOR:1;      // 12   load slightly unbalance     1:
    Uint16  SHORT:1;            // 13   output short        1:fail  0:normal
    Uint16  IDERR:1;            // 14   ID error            1:fail  0:normal
    Uint16  HVSDLOCK:1;         // 15   hvsd lock           1:lock  0:normal
};
typedef union {
   Uint16               all;
   struct MDLSTAT_BITS   bit;
}ubitintb;

extern  ubitintb        g_u16MdlStatusLast;
extern ubitintb g_u16MdlStatus;       // rectifier status

struct MDLSTATEXTEND_BITS {         // bits  description
        Uint16  PFCOT:1;            // 0    PFC over temp       1:OT    0:normal
        Uint16  ACCOMHALT:1;        // 1     AC>80V,COM fail    1:fail  0:normal
        Uint16  HWHVSD:1;           // 2    HWHVSD flag         1:fail  0:normal
        Uint16  DCFAULT:1;          // 3 DC Fault Flag          1: DC Fault 0: Normal
        Uint16  DCFUSEBROKEN:1;     // 4 DC Fuse broken Flag    1: broken 0: Normal
        Uint16  PFCFAIL:1;          // 5    PFC Fail            1:Fail  0:normal
        Uint16  PARAFLAG:1;         // 6  Rectifier single or more 1: more  0:single
        Uint16  DCINPUTSTATUS:1;       // 7 DC input status         1: DC input 0: AC input
        Uint16  PARAFLAGMASTER:1;   // 8 for
        Uint16  DIAGNOSISTEST:1;    // 9//begin test SCU set volt or not:set:1// ELSE :0
        Uint16  DIAGNOSISBEGIN:1;   //10//SCU set volt start self diagnosis��1;ELSE:0
        Uint16  SSHORTSD:1;         //11        Safety Short Shunt Down 1,safety short; 0,Normal//
        Uint16  DcInputAlarmStatus:1;       // 12 DC input alarm        1: DC input WARN 0: Normal NO DC input WARN
        Uint16  ACOVRMS:1;          // 13    Unplug flag 1:unplug 0:plug
        Uint16  ACOVPEAK:1;         //14     ac peak ov
        Uint16  DCOCP_PFCOVP:1;         // 15    DCOCP
};


typedef union {                                                    //XGH
   Uint16               all;
    struct MDLSTATEXTEND_BITS   bit;
}ubitintbextend;
extern ubitintbextend   g_u16MdlStatusExtend;         // rectifier status

extern  ubitintbextend  g_u16MdlStatusExtendLast;

extern  void  vEventLog(void);
extern  void  vWriteEepromEvent(void);
extern  void vEepromBufferInitial(void);
extern  void vReadEepromAlarmData(UINT16 u16Address);
//clear alarm data in eeprom
extern  void vClearEepromAlarmData(void);
extern void  vReadEepromcClibaData(UINT16 u16Address);

#endif

//===========================================================================
// No more.
//===========================================================================


