/*=============================================================================*
 *         Copyright(c) 2004-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : HD415CZ
 *
 *  FILENAME : gbb_CanDriver.h
 *  PURPOSE  : rectifier communicate with controller and other rectifiers	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *     2008-11-25      A000           zoulx             Created.   Pre-research
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *    detail information refer to gbb_main.h      
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                               		     DESCRIPTION 
 *============================================================================*/

#ifndef Drv_I2C_H
#define Drv_I2C_H
 
//
// I2C functions
//
extern void InitI2C(void);
extern UINT16 uiI2caWriteData1(UINT16 u16Address, ubitfloat fTemp);
extern UINT16 uiI2caWriteData2(UINT16 u16Address, ubitfloat fTemp);
extern UINT16 uiI2caReadData(UINT16 u16Address);
extern UINT16 uiI2caReadData1(UINT16 u16Address);
extern UINT16 uiI2caReadData2 (UINT16 u16Address);

#endif
