;#########################################################
; Date: from 2019.8  , fuhuaxing
; Version:1.00     Change Date:
; FileName: HD415GU111_IsrVariableDefs.asm
;#########################################################
;----------------------------------------
;variables define
;************ DP = #_IsrVars=0xC000(280049 #_IsrVars=0xC000)**************
;------------offset=0000H---_DCDC_DP----------------------------------------
_i16LowTempLoopflag	.set 	0000H   ;wl added
_i16VdcSet 			.set 	0001H	;Dcdc set voltage
_i16AdVdcSam0 		.set 	0002H 	;dcdc control voltage AD sample currently value
_i16AdVdcSam1 		.set 	0003H	
_lVdcFilterK1		.set 	0004H	;dcdc voltage filter coefficient
_lVdcFilterK1_H		.set 	0005H
_i16VdcFilterK2		.set 	0006H
_i16VdcFilterK3 	.set 	0007H
_lVdcUse 			.set 	0008H	;dcdc voltage after filter
_lVdcUse_H 			.set 	0009H
_i16VdcErrUse1		.set 	000AH	;dcdc voltage error currently value
_i16VdcErrUse2 		.set 	000BH
_i16VdcLoopK3		.set 	000CH   ;dcdc voltage loop coefficient
_i16VdcLoopK4 		.set 	000DH
_i16VdcRippleRangePos 	.set 	000EH
_i16VdcRippleRangeNeg 	.set 	000FH

;---------------offset=0010H-------------------------
_i16VdcRippleUsePos 	.set 	0010H
_i16VdcRippleUseNeg 	.set 	0011H
_lVdcRippleUse 	  	.set  	0012H
_lVdcRippleUse_H  	.set  	0013H
_i16IdcdcSys 	   	.set 	0014H	;dcdc limit current coefficient
_i16IdcLoopFilterK2	.set 	0015H	
_lCurrLim			.set 	0016H	;dcdc limit current value
_lCurrLim_H			.set 	0017H
_i16CurrLimFloor	.set 	0018H	;floor level value of limit current 
_i16PowerMaxExtra 	.set 	0019H
_lVdcPiOut 			.set 	001AH	;dcdc voltage loop output
_lVdcPiOut_H		.set 	001BH
_i16PowerLimMin 	.set 	001CH 	;可以更换成常数
_i16PowerLimMax 	.set 	001DH	;power upper limit
_i16DCDCPowerSet 	.set 	001EH	;dcdc power set
_i16DCDCPowerFeedback	.set 	001FH	;dcdc power feedback

;---------------offset=0020H-------------------------
_u32Vdc47v25Point		.set 	0020H
_u32Vdc47v25Point_H		.set    0021H

_u32Vdc47v7Point		.set 	0022H
_u32Vdc47v7Point_H		.set 	0023H


_i16AdIdc 			.set 	0024H	;dcdc current AD sample value
_i16IdcPioutPermit	.set 	0025H
_lIdcPiout 			.set 	0026H	;dcdc power loop output
_lIdcPiout_H 		.set 	0027H
_i16DcdcPWMTsMin 	.set 	0028H    ;可以更换成常数
_i16DcdcPWMTsMax 	.set 	0029H
_i16DcdcPWMDutyMax	.set 	002AH
_u16AcChangeFlag  	.set 	002BH   ;;;;;;;;;;DC prd 的保留
_lDcdcDutyShadow	.set 	002CH	;
_lDcdcDutyShadow_H	.set 	002DH	
_lDcdcPWMTsShadow	.set 	002EH
_lDcdcPWMTsShadow_H	.set 	002FH 

;---------------offset=0030H-------------------------
_lDcdcPWMDutyK         	.set 	0030H
_lDcdcPWMDutyK_H       	.set 	0031H
_lDcdcPWMDutyD         	.set 	0032H
_lDcdcPWMDutyD_H       	.set 	0033H
_u16LowPowerFlag2	.set 	0034H
_i16VdcOVPTimes		.set 	0035H       
_i16VdcHVSDTimes	.set	0036H
_i16LowACvoltageFlag  	.set    0037H     ; WL added_i16PowerPIcoeffK3  no useed
_i16LowACLPowerFlag	.set    0038H     ;_i16PowerPIcoeffK4  no used
_i16VRippleK1		.set	0039H
_i16VRippleK2		.set	003AH
_i16VRippleK3		.set	003BH
_i16PowerLimMaxUse 	.set 	003CH	;如果不改死区可以用常数替代
_u16ChoiseCon 		.set 	003DH	;constant voltage/limit power/limit current flag
_u16RippleDisable 	.set    003EH
_i16IdcLoopFilterK3   	.set 	003FH

;------------offset=0040H- _DCDC1_DP-------------------------------------
_lIdcDisUse			.set 	0040H   ;ac ov voltage permit
_lIdcDisUse_H		.set 	0041H
_i16AdIdcDis		.set 	0042H
_i16IdcdcLoopQn		.set 	0043H
_lVdcDisUse			.set 	0044H	;ac under voltage flag
_lVdcDisUse_H		.set 	0045H
_i16AdVdcDis		.set 	0046H
_u16DcdcILoopOpen	.set 	0047H
_i16DcdcVoltRipple0	.set 	0048H
_i16DcdcVoltRipple1	.set 	0049H   ;ac over voltage flag
_i16VdcShortCoefA	.set 	004AH
_i16VdcShortCoefB	.set 	004BH
_u16Pointer			.set	004CH
_u16PCtrl			.set 	004DH
_u16PAddress		.set 	004EH
_u16PAddress2		.set 	004FH

;---------------offset=0050H--------------------------------
_u16PAddress3		.set 	0050H
_u16PAddress4		.set 	0051H
_lU1Temp			.set 	0052H
_lU1Temp_H			.set 	0053H
_lDcTemp			.set 	0054H
_lDcTemp_H			.set 	0055H
_u16ADU1Temp		.set 	0056H
_u16ADDcTemp		.set 	0057H
;qtest
;_lDCDC_Nouse1A 		.set 	0058H
;_lDCDC_Nouse1A_H 	.set 	0059H
_u16RamReadEn 		.set 	0058H
_u16RamReadNoUse 	.set 	0059H
_u16SwitchFlag 		.set 	005AH
_i16IdcdcSysTrue	.set 	005BH
_lFanBad			.set 	005CH
_lFanBad_H 			.set 	005DH
_u16ADFanBad		.set 	005EH
_i16PfcDynFlg		.set    005FH

;---------------offset=0060H----------------------
;qtest0424
_i16PFCFastLoopForce .set 	0060H
_i16PFCFastLoopCnt	 .set 	0061H
;_lDCDC_Nouse01		.set 	0060H
;_lDCDC_Nouse01_H	.set 	0061H

;_lDCDC_Nouse02		.set 	0062H
;_lDCDC_Nouse02_H	.set 	0063H
_i16VpfcOnePhasePower   .set 	0062H;
_i16DCPFCPowerDetlaIsr   .set 	0063H;
;qtest0424 end
_lDCPFCPowerDetlaIsrUse  .set 	0064H
_lDCPFCPowerDetlaIsrUse_H    .set 	0065H
;_lDCDC_Nouse03		.set 	0064H
;_lDCDC_Nouse03_H	.set 	0065H
_i16DCPFCPowerDetlaIsrUseFinal  .set 	0066H
_i16QtestNoUse        .set 	0067H
;_lDCDC_Nouse04		.set 	0066H
;_lDCDC_Nouse04_H	.set 	0067H
;qtest0601
;_lDCDC_Nouse05		.set 	0068H
;_lDCDC_Nouse05_H	.set 	0069H
;_lDCDC_Nouse06		.set 	006AH
;_lDCDC_Nouse06_H	.set 	006BH
;_lDCDC_Nouse07		.set 	006CH
;_lDCDC_Nouse07_H	.set 	006DH
;_lDCDC_Nouse08		.set 	006EH
;_lDCDC_Nouse08_H	.set    006FH
_lIdcFilterOut              .set 	0068H
_lIdcFilterOut_H			.set 	0069H
_i16AdIdcSamp 				.set 	006AH
_i16CphaseOnCnt				.set 	006BH
_u16PhaseSheddingFlagTest  .set 	006CH
_i16AdIdc14A             	.set 	006DH
_i16SingleToDualQuick   	.set 	006EH
_i16PhaseShieldingTest   	.set 	006FH
;_i16DCDC_Nouse08    	.set    006FH
;qtest0601end

;---------------offset=0070H-------------------------
_u16DcOpened 		.set 	0070H
_u16nouse1111		.set 	0071H
_u16NormalRunTimer	.set 	0072H
_u16LowTempLVoutLLoadflag .set 	0073H
_i16AdIdc25A		.set 	0074H
_u16LoadDeltaFlag	.set 	0075H
_u16MainRly_Timer	.set	0076H
_u16DcdcOCPPfcOVP	.set    0077H
_u16IsrTimer		.set    0078H
_i16VFanSet			.set	0079H
_i16VdcSetTmp		.set	007AH
_i16LoadDelta		.set	007BH
_u16PFCOCP			.set 	007CH
_u16SoftStartEndTimer	.set 	007DH
_lDCDC_Nouse09		.set    007EH
_lDCDC_Nouse09_H	.set 	007FH


;---------------offset=0080H--_DCDC2_DP----------------------------------------
_iq16DcVolCalDcFreqCoefA .set 	0080H	
_i16DcVolCalDcFreqCoefB  .set 	0081H
_u16VolSamCaliSuccess	.set 	0082H
_u16DcdcPWMTsMaxHighLine_80K	.set 	0083H
_u16DcdcPWMTsMaxLowLine_103k	.set 	0084H	
_u16DcdcPWMTsLIM_95K		.set 	0085H
_u16PermitOverAcFlag		.set 	0086H	
_u16ADPFCTemp			.set    0087H
_lPFCTemp				.set 	0088H
_lPFCTemp_H				.set 	0089H
_i16ShortIdcOffset		.set    008AH  
_i16ShortVdcOffset		.set    008BH   
;_lDCDC_Nouse0A			.set 	008CH
;_lDCDC_Nouse0A_H		.set 	008DH
_i16DsptmpFlag			.set 	008CH
_i16AbmtmpFlag			.set 	008DH
_lDCDC_Nouse0B			.set 	008EH
_lDCDC_Nouse0B_H		.set 	008FH

;---------------offset=0090H-------------------------
_lVdcLoopFilterK1	.set  	0090H 
_lVdcLoopFilterK1_H	.set  	0091H 
_i16VdcLoopFilterK2	.set    0092H
_i16VdcLoopFilterK3	.set    0093H
_i16polartiyV		.set    0094H
_u16LowVolDcLoopChgFlag	.set    0095H
_u16LowPowerFlag1	.set    0096H
_u16DCCurrADtrigValue	.set    0097H
_u16DCCurrADtrigValue2	.set    0098H
_i16VacDynFlag			.set    0099H
_u1632BitConRamEn		.set    009AH
_u1632BitSingleRamEn    .set    009BH
;_u16DCDC_Nouse0D	.set    009AH
;_u16DCDC_Nouse0E	.set    009BH
_u16DCDC_Nouse0F	.set 	009CH   
_u16DCDC_Nouse0G	.set 	009DH   
_u16DCDC_Nouse0H	.set 	009EH   
_u16DCDC_Nouse0I	.set 	009FH  	

;---------------offset=00A0H------------------------- 

_u16DCDC_Nouse01	.set	00A0H
_u16DCDC_Nouse02	.set	00A1H
_u16DCDC_Nouse03	.set	00A2H
_u16DCDC_Nouse04	.set	00A3H
_u16DCDC_Nouse05	.set	00A4H
_u16DCDC_Nouse06	.set	00A5H

_lOpenPIoutStart    .set    00A6H
_lOpenPIoutStart_H  .set    00A7H
_lOpenLoopStep      .set    00A8H
_lOpenLoopStep_H    .set    00A9H
_u16OpenLoopCtrl    .set    00AAH
_u16OpenLoopLR		.set 	00ABH
_lOpenPRDDUN        .set 	00ACH
_lOpenPRDDUN_H      .set 	00ADH
_lOpenPRDUP			.set 	00AEH
_lOpenPRDUP_H		.set 	00AFH
;_u16DCDC_Nouse07	.set	00A6H
;_u16DCDC_Nouse08	.set	00A7H
;_u16DCDC_Nouse09	.set	00A8H
;_u16DCDC_Nouse010	.set	00A9H
;_u16DCDC_Nouse011	.set	00AAH
;_u16DCDC_Nouse012	.set	00ABH
;_u16DCDC_Nouse013	.set	00ACH
;_u16DCDC_Nouse014	.set	00ADH
;_u16DCDC_Nouse015	.set	00AEH
;_u16DCDC_Nouse016	.set	00AFH

;---------------offset=00B0H-------------------------
_u16DCDC_DynDelayCnt	.set	00B0H
_u16DCDC_OvpDelayCnt	.set	00B1H
_u16DCDC_OvpLimitFlag	.set	00B2H
_u16DCDC_OvpLimitCeiling	.set	00B3H
_u16DCDC_OvpLimitFloor		.set	00B4H
_u16DCDC_Nouse16	.set	00B5H
_u16DCDC_Nouse17	.set	00B6H
_u16DCDC_Nouse18	.set	00B7H
_u16DCDC_Nouse19	.set	00B8H  
_u16DCDC_Nouse110	.set	00B9H 
_u16DCDC_Nouse111	.set	00BAH	
_u16DCDC_Nouse112	.set	00BBH
_u16DCDC_Nouse113	.set	00BCH 
_u16DCDC_Nouse114	.set	00BDH 
_u16DCDC_Nouse115	.set	00BEH 
_u16DCDC_Nouse116	.set	00BFH 

;---------------offset=00C0H--_PFC_DP-----------------------------------------
_u16PermitOverAc 	.set 	00C0H   ;ac ov voltage permit
_i16AdVpfcProtect	.set 	00C1H	;ac under voltage flag
_lVpfcProtect		.set 	00C2H   
_lVpfcProtect_H		.set 	00C3H   
_u16MinTimer 		.set 	00C4H 
_u16PriOcp			.set 	00C5H
_u16OverAcVolt		.set 	00C6H   ;ac over voltage flag	
_u16AdAcVoltcount 		.set 	00C7H
_u32AdAcVoltAddress 	.set 	00C8H    
_u32AdAcVoltAddress_H  	.set 	00C9H
_u16AcVoltPhaseshift 	.set 	00CAH  	
_u16IpfcLoopGaincoef 	.set 	00CBH
_u16IpfcLoopGaincoefWant	.set	00CCH
_u16Reserved 			.set 	00CDH
_u16QuickProtCnt 		.set 	00CEH
_u16PfcZCD3Cnt 			.set 	00CFH

;---------------offset=00D0H--------------------------------------------
_i16IpfcErr0 		.set 	00D0H   ;Ipfc error
_i16IpfcErr1		.set 	00D1H
_lIpfcErrFilterK1	.set 	00D2H 	;Ipfc error filter coefficient
_lIpfcErrFilterK1_H	.set 	00D3H 	;
_i16IpfcErrFilterK2 	.set 	00D4H
_i16IpfcErrFilterK3 	.set 	00D5H
_lIpfcErrUse0 		.set 	00D6H   ;Ipfc error filter Tmp variable
_lIpfcErrUse0_H 	.set 	00D7H
_lIpfcErrUse1 		.set 	00D8H   ;Ipfc error after filter
_lIpfcErrUse1_H 	.set 	00D9H
_i16IpfcLoopK3 		.set 	00DAH
_i16IpfcLoopK4 		.set 	00DBH
_lIpfcPiOut 		.set 	00DCH   ;Ipfc pi output
_lIpfcPiOut_H 		.set 	00DDH
_i16PfcQn			.set 	00DEH   ;NO USE
_i16PfcMinDuty      	.set    00DFH  

;---------------offset=00E0H-------------------------
_i16IpfcErr20 		.set 	00E0H   ;Ipfc error
_i16IpfcErr21		.set 	00E1H
_lIpfcErrFilterK21	.set 	00E2H 	;Ipfc error filter coefficient
_lIpfcErrFilterK21_H	.set 	00E3H 	;
_i16IpfcErrFilterK22 	.set 	00E4H
_i16IpfcErrFilterK23 	.set 	00E5H
_lIpfcErrUse20 		.set 	00E6H   ;Ipfc error filter Tmp variable
_lIpfcErrUse20_H 	.set 	00E7H
_lIpfcErrUse21 		.set 	00E8H   ;Ipfc error after filter
_lIpfcErrUse21_H 	.set 	00E9H
_i16IpfcLoopK23 	.set 	00EAH
_i16IpfcLoopK24 	.set 	00EBH
_lIpfcPiOut2 		.set 	00ECH   ;Ipfc pi output
_lIpfcPiOut2_H 		.set 	00EDH
_i16PfcQn2			.set 	00EEH   ;pfc Qn
_i16PfcMinDuty2      	.set    00EFH 
 
;---------------offset=00F0H-------------------------
_lVacRmsSqrInvUse       .set 	00F0H ;//DC DISP current 10hz filter value
_lVacRmsSqrInvUse_H     .set 	00F1H
_lVacRmsSqrInvFast      .set 	00F2H
_lVacRmsSqrInvFast_H    .set 	00F3H
_u16PfcZCD1Cnt		.set 	00F4H
_i16PfcSWoffFlag	.set 	00F5H       
_i16VpfcPiTmp		.set	00F6H
;qtest0601
_i16DualDrvStratFlag		.set    00F7H
;_u16PFC_Nouse02		.set    00F7H     ; WL added_i16PowerPIcoeffK3  no useed
;qtest0601end
_u16PFC_Nouse03		.set    00F8H       ;_i16PowerPIcoeffK4  no used
_u16PFC_Nouse04		.set	00F9H
_u16PFC_Nouse05		.set	00FAH
_u16PFC_Nouse06		.set	00FBH
_u16PFC_Nouse07		.set 	00FCH	;如果不改死区可以用常数替代
_u16PFC_Nouse08		.set 	00FDH	;constant voltage/limit power/limit current flag
_u16PFC_Nouse09		.set    00FEH
_u16PFC_Nouse010	.set 	00FFH

;---------------offset=0100H----_PFC1_DP----------------------
_u16PfcOpened  		.set  	0100H ;pfc opened flag
_i16AdVpfc0			.set	0101H ;Vpfc protect
_i16AdVpfc1			.set	0102H ;Vpfc loop control
_i16VpfcFilterK2	.set	0103H
_lVpfcFilterK1		.set	0104H ;Vpfc filter coefficient
_lVpfcFilterK1_H	.set	0105H
_lVpfcUse			.set 	0106H  ;Vpfc after filter
_lVpfcUse_H			.set 	0107H
_i16VpfcSet			.set 	0108H  ;Vpfc setting voltage
_i16VpfcErr0		.set 	0109H  ;Vpfc error
_i16BurstAct10msDly	.set 	010AH
_i16VpfcLoopKp		.set 	010BH ;Vpfc loop control coefficient
_i16VpfcLoopKi		.set 	010CH
_i16VpfcPiOutMax 	.set 	010DH ;Vpfc pi output limit
_lVpfcPiOut			.set 	010EH ;Vpfc loop control output
_lVpfcPiOut_H		.set 	010FH


;---------------offset=0110H------------------------
_u16VpfcOverFlag	.set 	0110H   ;Vpfc pi output
_i16PFCPRDFLG       .set 	0111H
_u16VpfcPara		.set 	0112H   ;
_u16VpfcQn 			.set 	0113H
_i16AdAcVoltSamp	.set 	0114H
_i16PfcMaxPower		.set 	0115H
_i16PfcQuickLoop	.set 	0116H   ;NO USE
_i16LowTempPFCloopFlag	.set 	0117H
_i16AdAcMax 		.set 	0118H    
_u16PfcQuickPFCUVPFault .set 	0119H 
_u16VpfcOnePhasePower	.set 	011AH   
_u16IpfcTon2CMPA	.set 	011BH      
_u16PfcQuickPFCOVPFault .set 	011CH
_u16PfcPWM1TsCTR  	.set 	011DH

;_u16NouseA			.set 	011EH
_u16LowPfcVoltCnt	.set    011FH  


;---------------offset=0120H-----------------------
_u16vref1v55 		.set 	0120H
_i16AdAcVoltTrue 	.set 	0121H   
_i16AdVacThreshold   	.set 	0122H
_i16AdIpfcTrue		.set 	0123H 	;Ipfc AD sample value
_i16AdIpfcTrue2		.set 	0124H 	;Ipfc AD sample value
_u16IpfcTonTemp		.set 	0125H   ;ton (us)
_lVacRmsSqrInv  	.set 	0126H   ;1/VacRms^2
_lVacRmsSqrInv_H  	.set 	0127H
_u32Lpfc			.set 	0128H
_u32Lpfc_H			.set 	0129H
_i16Coss 			.set 	012AH
_i16TLC 			.set 	012BH
_i16PfcDutyPermit 	.set 	012CH   ;pfc duty permit
_u16IpfcTon2CMPB 	.set 	012DH	
_u16PfcSinglePhaseMode 	.set 	012EH   ;Ipfc ad (have offset)
_u16SlaveOffFlag	.set 	012FH   ;


;---------------offset=0130H-----------------------
_i16LLoadBurstOutTime   .set 	0130H   ; burst delay time
_i16LLoadBurstInTime	.set 	0131H   ;
_i16BurstOffDrvFlag 	.set    0132H   ; burst mode no drive
_i16LLoadBurstFlag      .set    0133H   ;TCM burst flag
_u16VacPolarity  		.set    0134H
_u16AdVacPosDelayTime   .set 	0135H   ;
_u16AdVacNegDelayTime  	.set 	0136H	;

_i16pfcompare1      .set 	0137H
_u16CapDischargeLim		    .set 	0138H 	;CTR3

;_u16CCMWorkTime		    .set 	0137H 	;CTR1
;_u16TCMWorkTime	 		.set 	0138H 	;CTR3

_i16PfcFaultFlag 		.set 	0139H
_i16BurstSumFlag		.set 	013AH   ;Tshift
_i16TonKpower			.set 	013BH
_u16VacZeroFlag  		.set 	013CH
_u16PwmEnableFlag  		.set 	013DH
_u16DelayTimeCnt		    .set 	013EH
_i16FirstPwmDrvFlag		.set    013FH

;---------------offset=0140H----_PFC2_DP----------------------
_lAcSamVoltAvg        		.set	0140H
_lAcSamVoltAvg_H        	.set	0141H
_lAcVoltAvgFilt_LW        	.set	0142H
_lAcVoltAvgFilt_LW_H        .set	0143H
_lAcVoltAvgFilt        		.set	0144H
_lAcVoltAvgFilt_H        	.set	0145H
_lAcVoltAvgFilt1        	.set	0146H
_lAcVoltAvgFilt1_H        	.set	0147H
_lAcVoltAvgOut_LW        	.set	0148H
_lAcVoltAvgOut_LW_H        	.set	0149H
_lAcVoltAvgOut        		.set	014AH
_lAcVoltAvgOut_H        	.set	014BH
_u16DC_NL_CNT				.set 	014CH	;RAM data test
_u16DC_LN_CNT 				.set 	014DH
_u16PFC_Nouse012			.set 	014EH
_u16PFC_Nouse013 			.set 	014FH

;---------------offset=0150H----_PFC2_DP----------------------
_i16VbusSysa 	    		.set  	0150H
_i16VbusSysb 	    		.set  	0151H
_u16RamReadCnt				.set    0152H
_u16PfcOcpCnt				.set    0153H
_i16AcDropFlag				.set    0154H
;_u16PFC_Nouse014				.set    0155H
_u16RamReadInterval          .set    0155H
_i16NoiseLoopflag			.set    0156H
_i16NoiseLoopflag2			.set    0157H
_u16PFC_Nouse017				.set    0158H
_u16PFC_Nouse018			.set    0159H
_u16DCDC_Dynflag				.set    015AH
_VdcUse_DEBUG            .set    015BH              ;_u16PFC_Nouse01A    	    .set    015BH  ;NO USE
_u16PFC_Nouse01B				.set 	015CH
_u16PFC_Nouse01C			.set 	015DH
_u16PFC_Nouse01D 	   		.set 	015EH
_u16PFC_Nouse01E			.set 	015FH

;---------------offset=0160H----_PFC2_DP----------------------
_lAcSamVoltSquare 	  	.set	0160H	;currently AC voltage^2
_lAcSamVoltSquare_H 	.set	0161H
_lAcVoltSquareFilt_LW   .set	0162H   ;adding 64 var low 16 of low 32  by zzh 4-26
_lAcVoltSquareFilt_LW_H .set	0163H   ;adding 64 var high 16 of low 32  by zzh 4-26
_lAcVoltSquareFilt 		.set	0164H	;after first filter of AC voltage^2
_lAcVoltSquareFilt_H 	.set	0165H
_lAcVoltSquareFilt1 	.set	0166H
_lAcVoltSquareFilt1_H 	.set	0167H
_lAcVoltSquareRms_LW    .set	0168H  ;adding 64 var high 16 of low 32  by zzh 4-26
_lAcVoltSquareRms_LW_H 	.set	0169H  ;adding 64 var high 16 of low 32  by zzh 4-26
_lAcVoltSquareRms 		.set	016AH	;after second filter of AC voltage^2
_lAcVoltSquareRms_H 	.set	016BH
_lVacAvgFltK            .set	016CH ; add by zzh 4-26
_lVacAvgFltK_H          .set	016DH ; add by zzh 4-26
_lVacRmsSquFltK         .set	016EH ; add by zzh 4-26
_lVacRmsSquFltK_H       .set	016FH ; add by zzh 4-26

;---------------offset=0170H----_PFC2_DP----------------------
_i16AcUpjumpFlag        .set    0170H
_u16InputLowVolCNT      .set    0171H
_i16Vpfc460VPoint	.set    0172H   ;By zzh 4-26
_i16Vpfc490VPoint	.set 	0173H   ;;WL 20140310
_i16Vpfc300VPoint	.set 	0174H
_i16Vpfc70VPoint	.set 	0175H
_u16PFC_Nouse020	.set 	0176H	;;#########Free for use########
_u16PFC_Nouse021	.set 	0177H	;;#########Free for use########
_u16PFC_Nouse022	.set    0178H   ;20081203 cxm
_u16PFC_Nouse023	.set    0179H   ;20081203 cxm
_u16PFC_Nouse024   	.set    017AH   ;20081204 cxm
_u16PFC_Nouse025	.set	017BH	;20081209 cxm
_u16PFC_Nouse026	.set 	017CH
_u16PFC_Nouse027	.set 	017DH
_u16PFC_Nouse028	.set	017EH	;PFC OCP COUNT
_u16PFC_Nouse029	.set	017FH	;NO USE

;---------------offset=0180H----_PFC3_DP----------------------
_u16PFC_Nouse030		.set    0180H
_u16PFC_Nouse031		.set    0181H
_u16PFC_Nouse032	.set    0182H
_u16PFC_Nouse033	.set    0183H
_u16PFC_Nouse034	.set    0184H
_u16PFC_Nouse035	.set    0185H
_u16PFC_Nouse036		.set 	0186H
_u16PFC_Nouse037		.set 	0187H
_u16PFC_Nouse038		.set 	0188H
_u16PFC_Nouse039		.set 	0189H
_u16PFC_Nouse03A	.set    018AH
_u16PFC_Nouse03B	.set    018BH
_u16PFC_Nouse03C	.set    018CH
_u16PFC_Nouse03D	.set    018DH
;_u16PFC_Nouse03E		.set   018EH
;_u16PFC_Nouse03F		.set    018FH
_i16PfcQuickLoopQnSetQtest	.set 	018EH
_i16VpfcPioutShiftSetQtest	.set 	018FH

;---------------offset=0190H----_PFC3_DP----------------------
_i16VpfcP04mp		.set 	0190H	;dcdc voltage filter coefficient
_u16LowPowerFlag	.set 	0191H
_i16VinHighPeakFlag	.set 	0192H
_i16PfcOvershootVal	.set 	0193H
_i16PfcQuickLoopQnSet	.set 	0194H
_i16VpfcPioutShiftSet	.set 	0195H
_i16PfcSlowLoopQnSet	.set 	0196H
_i16VpfcQnSet		.set 	0197H
;qtest0601
_f32SinglePhaseTonLimit    .set 	0198H
_f32SinglePhaseTonLimit_H    .set 	0199H
_i16SinglePhaseLargeCurrFlag   .set	019AH
;_u16PFC_Nouse041	.set 	0198H
;_u16PFC_Nouse042	.set 	0199H
;_u16PFC_Nouse043		.set	019AH
;qtest0601end
_u16PFC_Nouse044			.set	019BH
_u16PFC_Nouse045	.set	019CH
_u16PFC_Nouse046	.set	019DH
_u16PFC_Nouse047	.set	019EH  ;DQA 20121226
_u16PFC_Nouse048	.set	019FH

;---------------offset=01A0H----_PFC3_DP----------------------
_i16AdAcVoltDelay0        .set	01A0H
_i16AdAcVoltDelay1        .set	01A1H
_i16AdAcVoltDelay2        .set	01A2H
_i16AdAcVoltDelay3        .set	01A3H
_i16AdAcVoltDelay4        .set	01A4H
_i16AdAcVoltDelay5        .set	01A5H
_i16AdAcVoltDelay6        .set	01A6H
_i16AdAcVoltDelay7        .set	01A7H
_i16AdAcVoltDelay8        .set	01A8H
_i16AdAcVoltDelay9        .set	01A9H
_i16AdAcVoltDelay10        .set	01AAH
_i16AdAcVoltDelay11        .set	01ABH
_i16AdAcVoltDelay12        .set	01ACH
_i16AdAcVoltDelay13        .set	01ADH
_i16AdAcVoltDelay14        .set	01AEH
_i16AdAcVoltDelay15        .set	01AFH
;---------------offset=01B0H----_PFC3_DP----------------------
_i16AdAcVoltDelay16        .set	01B0H
_i16AdAcVoltDelay17        .set	01B1H
_i16AdAcVoltDelay18        .set	01B2H
_i16AdAcVoltDelay19        .set	01B3H
_i16AdAcVoltDelay20        .set	01B4H
_i16AdAcVoltDelay21        .set	01B5H
_i16AdAcVoltDelay22        .set	01B6H
_i16AdAcVoltDelay23        .set	01B7H
_i16AdAcVoltDelay24        .set	01B8H
_i16AdAcVoltDelay25        .set	01B9H
_i16AdAcVoltDelay26        .set	01BAH
_i16AdAcVoltDelay27        .set	01BBH
_i16AdAcVoltDelay28        .set	01BCH
_i16AdAcVoltDelay29        .set	01BDH
_i16AdAcVoltDelay30        .set	01BEH
_i16AdAcVoltDelay31        .set	01BFH

;---------------offset=01C0H----_PFC4_DP----------------------
_i16AdAcVoltDelay32        .set	01C0H
_i16AdAcVoltDelay33        .set	01C1H
_i16AdAcVoltDelay34        .set	01C2H
_i16AdAcVoltDelay35        .set	01C3H
_i16AdAcVoltDelay36        .set	01C4H
_i16AdAcVoltDelay37        .set	01C5H
_i16AdAcVoltDelay38        .set	01C6H
_i16AdAcVoltDelay39        .set	01C7H
_i16AdAcVoltDelay40        .set	01C8H
_i16AdAcVoltDelay41        .set	01C9H
_i16AdAcVoltDelay42        .set	01CAH
_i16AdAcVoltDelay43        .set	01CBH
_i16AdAcVoltDelay44        .set	01CCH
_i16AdAcVoltDelay45        .set	01CDH
_i16AdAcVoltDelay46        .set	01CEH
_i16AdAcVoltDelay47        .set	01CFH;;;;当前数组只用到了48个元素

;---------------offset=01D0H----_PFC4_DP----------------------
_i16AdAcVoltDelay48        .set	01D0H
_i16AdAcVoltDelay49        .set	01D1H
_i16AdAcVoltDelay50        .set	01D2H
_i16AdAcVoltDelay51        .set	01D3H
_i16AdAcVoltDelay52        .set	01D4H
_u16AdAcMaxPowLim	       .set	01D5H;;;715H
;_i16AdAcVoltDelay54       .set	01D6H
;_i16AdAcVoltDelay55       .set	01D7H
;_i16AdAcVoltDelay56       .set	01D8H
_lVacUse			    	.set 01D6H
_lVacUse_H					.set 01D7H
_i16AdAcVolt1	        	.set 01D8H

_u16PolarityFlgHz          .set	01D9H
_u16PolarityUse            .set	01DAH
_u16VinTimer1              .set	01DBH
_u16VinT                   .set	01DCH
_u16VinFrequency		   .set	01DDH
_u16VinF                   .set	01DEH
_u16VinTimer2			   .set	01DFH


;---------------offset=01E0H----_PFC4_DP----------------------
_lAcCurSamSquare 	  		.set	01E0H	;currently AC voltage^2
_lAcCurSamSquare_H 			.set	01E1H
_lAcCurSquareFilt_LW   	    .set	01E2H   ;adding 64 var low 16 of low 32  by zzh 4-26
_lAcCurSquareFilt_LW_H   	.set	01E3H   ;adding 64 var high 16 of low 32  by zzh 4-26
_lAcCurSquareFilt 			.set	01E4H		;after first filter of AC voltage^2
_lAcCurSquareFilt_H 		.set	01E5H
_lAcCurSquareFilt1 	    	.set	01E6H
_lAcCurSquareFilt1_H 		.set	01E7H
_lAcCurSquareRms_LW      	.set	01E8H  ;adding 64 var high 16 of low 32  by zzh 4-26
_lAcCurSquareRms_LW_H 		.set	01E9H  ;adding 64 var high 16 of low 32  by zzh 4-26
_lAcCurSquareRms 			.set	01EAH	;after second filter of AC voltage^2
_lAcCurSquareRms_H 		    .set	01EBH
_lAcCurAdcSquFltK          	.set	01ECH ; add by zzh 4-26
_lAcCurAdcSquFltK_H        	.set	01EDH ; add by zzh 4-26
_lAcCurRmsSquFltK          	.set	01EEH ; add by zzh 4-26
_lAcCurRmsSquFltK_H        	.set	01EFH ; add by zzh 4-26

;---------------offset=01F0H----_PFC4_DP----------------------
_lAcCurSamSquare2 	  		.set	01F0H	;currently AC voltage^2
_lAcCurSamSquare2_H 		.set	01F1H
_lAcCurSquareFilt2_LW   	.set	01F2H   ;adding 64 var low 16 of low 32  by zzh 4-26
_lAcCurSquareFilt2_LW_H   	.set	01F3H   ;adding 64 var high 16 of low 32  by zzh 4-26
_lAcCurSquareFilt2 			.set	01F4H	;after first filter of AC voltage^2
_lAcCurSquareFilt2_H 		.set	01F5H
_lAcCurSquareFilt21 	    .set	01F6H
_lAcCurSquareFilt21_H 		.set	01F7H
_lAcCurSquareRms2_LW      	.set	01F8H  ;adding 64 var high 16 of low 32  by zzh 4-26
_lAcCurSquareRms2_LW_H 		.set	01F9H  ;adding 64 var high 16 of low 32  by zzh 4-26
_lAcCurSquareRms2 			.set	01FAH	;after second filter of AC voltage^2
_lAcCurSquareRms2_H 		.set	01FBH
_lAcCurAdcSquFltK2          .set	01FCH ; add by zzh 4-26
_lAcCurAdcSquFltK2_H        .set	01FDH ; add by zzh 4-26
_lAcCurRmsSquFltK2          .set	01FEH ; add by zzh 4-26
_lAcCurRmsSquFltK2_H        .set	01FFH ; add by zzh 4-26

;------------offset=0200H-- _PFC5_DP---------------------
_f32VpfcVal        			.set	0200H
_f32VpfcVal_H        		.set	0201H
_f32InvVpfcVal        		.set	0202H
_f32InvVpfcVal_H        	.set	0203H
_f32VacAbsUse        		.set	0204H
_f32VacAbsUse_H        		.set	0205H
_f32INVVacUse        		.set	0206H
_f32INVVacUse_H        		.set	0207H
_f32DiffVpfcVacUse        	.set	0208H
_f32DiffVpfcVacUse_H        .set	0209H
_f32INVDiffVpfcVacUse		.set	020AH
_f32INVDiffVpfcVacUse_H		.set	020BH
_f32PFCDuty        			.set	020CH
_f32PFCDuty_H        		.set	020DH
_f32PwmTonshift        		.set	020EH
_f32PwmTonshift_H			.set	020FH


;------------offset=0210H-- _PFC5_DP---------------------
_f32TsTonRatio				.set	0210H
_f32TsTonRatio_H			.set	0211H
_f32PhaseShiftCoef			.set	0212H
_f32PhaseShiftCoef_H		.set	0213H
_f32OCPTonPermit			.set	0214H
_f32OCPTonPermit_H			.set	0215H
_f32TonMaxPrd				.set	0216H
_f32TonMaxPrd_H				.set	0217H
_f32TonMinPrd				.set	0218H
_f32TonMinPrd_H				.set	0219H
_f32TCMTonMaxCMPA			.set	021AH
_f32TCMTonMaxCMPA_H			.set	021BH
_f32TCMTonMinCMPA			.set	021CH
_f32TCMTonMinCMPA_H			.set	021DH
_i16AdAcVoltPhaseShift		.set	021EH
_i16LpfcPFU					.set	021FH


;------------offset=0220H-- _PFC5_DP---------------------
_u16TCMTonFPU				.set	0220H
_u16IpfcTon1CMPACLA			.set	0221H
_u16IpfcTon2CMPACLA			.set	0222H
_u16TCMHalfTsByCal			.set	0223H
_f32TCMTonCMPA				.set	0224H
_f32TCMTonCMPA_H			.set	0225H
_f32TCMTsByCal				.set	0226H
_f32TCMTsByCal_H			.set	0227H
_f32ExpLC					.set	0228H
_f32ExpLC_H					.set	0229H
_f32Trv						.set	022AH
_f32Trv_H					.set	022BH
_u16TonCompeK				.set	022CH
_i16Trv						.set	022DH
_f32Irv						.set	022EH
_f32Irv_H					.set	022FH


;------------offset=0230H-- _PFC5_DP---------------------
_f32TCMTonCMPA3				.set	0230H
_f32TCMTonCMPA3_H			.set	0231H
_u16TonHVpermit        		.set	0232H
_i16PwmTonshift				.set	0233H
_u16LoopTest                .set	0234H
_u16QVacAbsTest             .set	0235H
_u16DiffVpfcVac				.set	0236H
_u16ChangeFlag          	.set	0237H
_u16VacAbs                  .set	0238H

_u16BlankWindowTest			.set	0239H
_u16BlankWindowTest3		.set	023AH

_i16DCDCPowerSetDyn      .set	023BH
_i16DCDCPowerSetNew     .set	023CH
_i16DCDCPowerSetBus		.set	023DH
_i16DCDCPowerSetUse   	.set	023EH

_i16Qtest   			.set	023FH
;todo start here ,to confirm  the address

_f32HVTonPermitMaxPrd		.set	0240H
_f32HVTonPermitMaxPrd_H     .set	0241H

_f32INVVacUse        		.set	0206H
_f32INVVacUse_H        		.set	0207H

_f32IrvTrCLA      			.set	020AH
_f32IrvTrCLA_H           	.set	020BH
_f32IrvTrvCLA               .set	020CH
_f32IrvTrvCLA_H             .set	020DH


;------------offset=0210H-- _PFC5_DP---------------------

_f32PhaseShiftCoef      	.set	0212H
_f32PhaseShiftCoef_H      	.set	0213H

_f32TCMTonPermit   			.set	0218H
_f32TCMTonPermit_H       	.set	0219H

_f32TonPermitMinPrd    		.set	021CH
_f32TonPermitMinPrd_H  		.set	021DH
_f32TrIpfcCompenCLA    		.set	021EH
_f32TrIpfcCompenCLA_H      	.set	021FH

;------------offset=0220H-- _PFC5_DP---------------------
_f32TrIrv        			.set	0220H
_f32TrIrv_H        	    	.set	0221H
_f32TrvIrv       			.set	0222H
_f32TrvIrv_H        		.set	0223H

_f32VacVal           		.set	0228H
_f32VacVal_H        		.set	0229H

_i16IQ10TrIpfcOffsetCLA    	.set	022EH
_i16IQ10TrvIpfcCLA        	.set	022FH

;------------offset=0230H-- _PFC5_DP---------------------
_i16DiffVpfcVacuse   		.set	0230H
_i16DiffVpfcVacuse_H        .set	0231H
_f32PfcDutyLimit       		.set	0232H
_f32PfcDutyLimit_H     		.set	0233H
_i16IQ10TrIpfcCLA      		.set	0234H
_i16LpfcCLA                 .set	0235H
_i16VpfcGap              	.set	0236H
_u16TonShiftLimit       	.set	0237H
_i16TrCompenCoef           	.set	0238H
_u16LoadBurstFlagFPU        .set	0239H
_f32FilterCoef	        	.set	023AH
_f32FilterCoef_H       		.set	023BH

_u16Vref1V6CLA        		.set	023EH
_i16AdAcVoltNoFilter  	    .set 	011DH

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
_u16VacTCMEnable			.set    0155H
_u16PwmFirstOffCnt	        .set 	011EH


;------------offset=0240H-- _PFC6_DP---------------------
_f32DACVALLowerLimitCCM	    .set	0240H
_f32DACVALLowerLimitCCM_H  	.set	0241H
_u16AdresultDcOcp2			.set	0242H
_i16PfcPhaseError      		.set	0243H
_u16PfcOcpCounter      		.set	0244H
_i16PFCPRDMINCLA        	.set	0245H
_i16CMPAStep	       		.set	0246H


;************ DP = #_CputoClaVar=0x1500(length=0x80)**************
;------------offset=0000H----------------------------------
_i16DCDCPowerSetCLA       .set	0000H
_i16IdcPioutPermitCLA     .set	0001H
_i16DcdcPWMTsMaxCLA       .set	0002H
_i16DcdcPWMTsMinCLA       .set	0003H
_u16DCADCCOEF             .set	0004H
_u16DCCurloopCoef         .set	0005H
_i16DCCurOffset           .set	0006H
_i16DCCurOffsetTrue       .set	0007H
_u16ShortFlag             .set	0008H
_u16DCDriveCurveFLG .set	0009H
_u16InterimVal         .set	000AH
_i16OnePhasePowerToCLA  .set	000BH
;_f32FilterK1             .set	000CH
_u16DCVsamlePointFLG    .set	000CH
_u16DCVsamlePointFLG1   .set	000DH
_f32FilterK2            .set	000EH
_u16VDCADCOffSet			.set	0010H
_u16IDCADCOffSet			.set	0011H

;;;;;;;;;;;;;扫频sweep;;;;;;;;;;
_lOpenPIoutStartCLA      .set	0012H
_lOpenPIoutStartCLA_H      .set	0013H
_u16OpenLoopCtrlCLA        .set	0014H
_u16Ton_BurstOff		.set	0015H

_i16IdcPioutPermitCLA1  .set	0016H
;************ DP = #_ClatoCpuVar=0x1480(length=0x80)*******
;------------offset=0030H----------------------------------
_u16IdcPiout               .set 0034H;???????这个没用啊

_i16DCPFCPowerDetlaFilter     .set 0037H;//14B7
_u32DCDCPowerFeedbackCLA  .set 0038H
_u32DCDCPowerFeedbackCLA_H .set 0039H
_i16QtestCLAtoCPU     .set 003AH
_i16DCPFCPowerDetla     .set 003BH

_f32DCPFCPowerDetla .set 003CH;//14BC
_f32DCPFCPowerDetlaFilter .set 003EH;//14BE



;************ DP = #_AdcRegs =#7100H **************
;    _ADC_REGS offset  defines
;_ADCCTL1 		.set 	0000H
;_ADCCTL2 		.set 	0001H

;************ DP = #_PieCtrlRegs =#0CE0H **************
;   _PIE_CTRL_REGS  offset defines
_PIECTRL		.set 	0000H
_PIEACK 		.set 	0001H
_PIEIER1 		.set 	0002H
_PIEIFR1		.set 	0003H
_PIEIER2		.set 	0004H
_PIEIFR2		.set 	0005H

;************ DP =          **************
;   EPWM1        : origin = 0x004000,
;   EPWM2        : origin = 0x004100,
;   EPWM3        : origin = 0x004200,
;   EPWM4        : origin = 0x004300,
;   EPWM5        : origin = 0x004400,
;   EPWM6        : origin = 0x004500,
;   EPWM7        : origin = 0x004600,
;   EPWM8        : origin = 0x004700,
;    _EPWM_REGS  offset  defines
_TBCTL			.set 	0000H
_TBCTL2			.set 	0001H
_TBCTR   		.set 	0004H
_TBSTS			.set 	0005H
_CMPCTL			.set 	0008H
_CMPCTL2		.set 	0009H
_DBCTL  		.set 	000CH
_DBCTL2  		.set 	000DH
_AQCTL			.set 	0010H
_AQTSRCSEL		.set 	0011H
_AQCTLA			.set 	0040H
_AQCTLA2		.set 	0041H
_AQCTLB			.set 	0042H
_AQCTLB2		.set 	0043H
_AQSFRC			.set 	0047H
_AQCSFRC		.set 	0049H
_TBPHSHR		.set 	0060H
_TBPHS			.set 	0061H
_TBPRDHR		.set 	0062H
_TBPRD 			.set 	0063H
_CMPAHR 		.set 	006AH
_CMPA 			.set 	006BH
_CMPBHR 		.set 	006CH
_CMPB 			.set 	006DH
_CMPCHR 		.set 	006EH
_CMPC 			.set 	006FH
_CMPDHR 		.set 	0070H
_CMPD 			.set 	0071H
_TZSEL			.set 	0080H

_TZDCSEL		.set 	0082H
_TZCTL			.set 	0084H
_TZCTL2			.set 	0085H
_TZEINT			.set 	008DH
_TZFLG			.set 	0093H
_TZOSTFLG       .set	0095H
_TZCLR			.set 	0097H
_TZFRC			.set 	009BH
_ETSEL			.set 	00A4H
_ETPS 			.set 	00A6H
_ETFLG			.set 	00A8H
_ETCLR			.set 	00AAH
_ETFRC			.set 	00ACH
_DCFWINDOW		.set	00CBH
_DCBCTL                .set	00C4H

;************ DP = #_COMP2Regs =#6420H  **************
_COMPCTL        .set    0000H
_COMPSTS        .set    0002H
_DACVAL         .set 	0006H

;************ DP = #_ECap1Regs =#5200H  **************
;************ DP = #_ECap2Regs =#5240H  **************
_TSCTR        	.set    0000H
;;_TSCTR_H        	.set    0001H
_CTRPHS        	.set    0002H
_CAP1         	.set 	0004H
_CAP2         	.set 	0006H
_CAP3         	.set 	0008H
_CAP4         	.set 	000AH
_ECCTL0        .set 	0012H
_ECCTL1        .set 	0014H
_ECCTL2        .set 	0015H




;************ DP = #_GpioDataRegs =#6FC0H  **************
;   _GPIO_DATA_REGS  defines
_GPADAT			.set 	0000H
_GPADAT_H		.set 	0001H
_GPASET			.set 	0002H
_GPASET_H		.set 	0003H
_GPACLEAR		.set 	0004H
_GPACLEAR_H		.set 	0005H
_GPATOGGLE		.set 	0006H
_GPATOGGLE_H	.set 	0007H
_GPBDAT			.set 	0008H
_GPBDAT_H		.set    0009H
_GPBSET			.set 	000AH
_GPBCLEAR		.set 	000CH
_GPBTOGGLE		.set 	000EH

;_rsvd1	.set 
;************ DP = #_CpuSysRegs = #7010H  **************
;   _SYS_CTRL_REGS  defines
_WDKEY  	    .set 	0015H


;位测试指令的位代码(BIT)
; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BIT15			.set	0000h		;位代码15
BIT14			.set	0001h		;位代码14
BIT13			.set	0002h		;位代码13
BIT12			.set	0003h		;位代码12
BIT11			.set	0004h		;位代码11
BIT10			.set	0005h		;位代码10
BIT9			.set	0006h		;位代码9
BIT8			.set	0007h		;位代码8
BIT7			.set	0008h		;位代码7
BIT6			.set	0009h		;位代码6
BIT5			.set	000Ah		;位代码5
BIT4			.set	000Bh		;位代码4
BIT3			.set	000Ch		;位代码3
BIT2			.set	000Dh		;位代码2
BIT1			.set	000Eh		;位代码1
BIT0			.set	000Fh		;位代码 0
;用 SBIT0 和 SBIT1 宏屏蔽位
; ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
B15_MSK 		.set	8000h		;位屏蔽15
B14_MSK 		.set	4000h		;位屏蔽 14
B13_MSK 		.set	2000h		;位屏蔽 13
B12_MSK 		.set	1000h		;位屏蔽12
B11_MSK 		.set	0800h		;位屏蔽11
B10_MSK 		.set	0400h		;位屏蔽 10
B9_MSK			.set	0200h		;位屏蔽 9
B8_MSK			.set	0100h		;位屏蔽 8
B7_MSK			.set	0080h		;位屏蔽 7
B6_MSK			.set	0040h		;位屏蔽 6
B5_MSK			.set	0020h		;位屏蔽 5
B4_MSK			.set	0010h		;位屏蔽 4
B3_MSK			.set	0008h		;位屏蔽3
B2_MSK			.set	0004h		;位屏蔽 2
B1_MSK			.set	0002h		;位屏蔽 1
B0_MSK			.set	0001h		;位屏蔽 0
;数据页    	



;--------------------10Hz filter -------------------------------;
; fs = 70e3 Hz
;filter=tf([1],[1/6.28/10 1]);c2d(filter,Ts*16,'tustin')
_Filter10HzK1	 .equ   -934
_Filter10HzK2	 .equ 	467

_Filter1kHzK1	 .equ 	10754
_Filter1kHzK2	 .equ 	27391

;--------------------VPFC---------------------------------------;
_PFCVolt310V     .equ   2497   	;310*8.056   UVP voltage
_PFCVolt320V     .equ   2578   	;320*8.056   UVP voltage
_PFCVolt330V     .equ   2658   	;330*8.056   UVP voltage
_PFCVolt340V     .equ   2739   	;340*8.056   UVP voltage
_PFCVolt345V     .equ   2779   	;340*8.056   UVP voltage
_PFCVolt350V     .equ   2820   	;350*8.056   UVP voltage
_PFCVolt355V     .equ   2860   	;350*8.056   UVP voltage
_PFCVolt360V     .equ   2900   	;360*8.056   UVP voltage
_PFCVolt365V     .equ   2940   	;365*8.056   UVP voltage
_PFCVolt370V     .equ   2981   	;370*8.056   UVP voltage
_PFCVolt375V     .equ   3021   	;375*8.056   UVP voltage
_PFCVolt385V     .equ   3101   	;385*8.056   UVP voltage
_PFCVolt395V     .equ   3182   	;395*8.056   UVP voltage
_PFCVolt405V     .equ   3266   	;405*8.056   UVP voltage
_PFCVolt410V     .equ   3303   	;410*8.056   UVP voltage
_PFCVolt445V     .equ   3585   	;445*8.056
_PFCVolt450V     .equ   3625   	;450*8.056

_PFCVolt460V     .equ   3680 	;460*8.056=3680
_PFCVolt465V     .equ   3746 	;465*8.056=3680

_PFCVolt470V     .equ   3786 	;470*8.056=3786
_PFCVolt475V     .equ   3826 	;475*8.056=3826.6
_PFCVolt480V     .equ   3866 	;480*8.056=3866
_PFCVolt490V     .equ   3947 	;490*8.056=3947
_PFCVolt500V	 .equ	4028	;500*8.056=4028
_PFCVolt7V       .equ   60		;7.5*8.056
_N_PFCVolt7V     .equ   -60		;-7.5*8.056
_PFCVolt10V       .equ   80		;7.5*8.056
_N_PFCVolt10V     .equ   -80		;-7.5*8.056

_PFCVolt5V       .equ   40	;7.5*8.056
_PFCVolt20V       .equ   161	;7.5*8.056
_N_PFCVolt5V     .equ   -40	;-7.5*8.056
_N_PFCVolt25V    .equ   -201  	;-25*8.056
_N_PFCVolt20V    .equ   -161  	;-25*8.056
_N_PFCVolt15V    .equ   -121  	;-25*8.056
_PFCVolt5V       .equ   40   	;5*8.056
_N_PFCVolt5V     .equ   -40  	;-5*8.056
_PFCVolt4V       .equ   32   	;5*8.056
_N_PFCVolt4V     .equ   -32  	;-5*8.056
_PFCVolt40V     .equ   320 		;40*8.006=320
;_PFCVolt40V     .equ   322 		;40*8.056=322
_DACHVALS		.set   0006H

;--------------------VAC----------------------------------------;
;_VAC_AD_30V      .equ   115;30*3.77889 = 113
;_VAC_AD_25V      .equ   94 ;25*3.77889 = 94;
;_VAC_AD_20V      .equ   76 ;20*3.77889 = 76
;_VAC_AD_312V_RMS .equ   1667;312 *1.414* 3.77889
;_VAC_AD_275V_RMS .equ   1469;275 *1.414* 3.77889

_VAC_AD_30V      .equ   115;30*7.51181 = 225
_VAC_AD_25V      .equ   94 ;25*7.51181 = 188;
_VAC_AD_20V      .equ   76 ;20*7.51181 = 150
_VAC_AD_312V_RMS .equ   3314;312 *1.414* 7.51181
_VAC_AD_275V_RMS .equ   2921;275 *1.414* 7.51181
_VAC_AD_305V_RMS .equ   3230;305 *1.414* 7.51181
_POWER_LIM_UPPER  .equ  9509



;-------------------PAGE----------------------------------------;
_DCDC_DP	.equ	0x0000
_DCDC1_DP	.equ	0x0040
_DCDC2_DP	.equ	0x0080
_PFC_DP		.equ	0x00C0
_PFC1_DP   	.equ	0x0100
_PFC2_DP   	.equ	0x0140
_PFC3_DP   	.equ	0x0180
_PFC4_DP   	.equ	0x01C0
_PFC5_DP   	.equ	0x0200
;------------------variables definition-------------------------;
	
;	.endif
		
