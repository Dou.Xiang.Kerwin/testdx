											 /*=============================================================================*
 *         Copyright(c) 2007-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : GBB Rectifiers
 *
 *  FILENAME : GBB_constant.h
 *  PURPOSE  : Define the constant
 *				     
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *  2009-12-18         OOA                      Created.   Pre-research 
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *    
 *============================================================================*/

/*============================================================================*
 * constant definition  
 *============================================================================*/
#ifndef GBB_constant_H
#define GBB_constant_H
 //--------------------------------------------------------------------
//define constant status falg  for watt accumulate                                  
//--------------------------------------------------------------------

//-------------------------AC 偏置电压--------------------------------------//
#define VAC_OFFSET_DF_VALUE       (int16)2048


#define CALCULATION_STATE    1     /*watt accumulate*/
#define STOP_CALCULATION_STATE  0  /*stop watt accumulate*/
 //--------------------------------------------------------------------
//define constant status falg  for communication    csm 091217                                    
//--------------------------------------------------------------------
#define RECOK		0xf0		/* communication normal */ 		
#define CRCCOM		0xf1 		/* CRC check fail */
#define XORCOM		0xf1 		/* XOR check fail */
#define NOVCOM		0xf2		/* invalid command  */
#define NULLCOM		0xf3		/* other fault  */
 //--------------------------------------------------------------------
//define constant status falg  for load sharing     csm 091217                                    
//--------------------------------------------------------------------
#define SYN_REDUCE_VOLT 0x0300    
#define SYN_RAISE_VOLT  0x0200 
#define ATUO_ADJUST     0x0100
#define NON_ADJUST      0x0000

//add for new 0x11,0x12 protocol, ls 20120224
#define NEW_SYN_REDUCE_VOLT 0x03
#define NEW_SYN_RAISE_VOLT  0x02
#define NEW_ATUO_ADJUST     0x01
#define NEW_NON_ADJUST      0x00 
//------------------------------------------------------------------------------
//define constant for address auto-identification                                         
//------------------------------------------------------------------------------
#define NON_IDENTIFY_ID 0 
#define REQUEST_ID      1 
#define SEND_ID         2 

//------------------------------------------------------------------------------
//define e2prom  RW status                                         
//------------------------------------------------------------------------------
#define  CALIBR_FAIL       0x01 
#define  READ_E2PROM_SUCCESS  0
#define  READ_E2PROM_FAIL     1
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//define e2prom  write status                                         
//------------------------------------------------------------------------------

#define WR_FREE                 ((UINT32)0x00000000)
#define WR_RUNTIME   	  		((UINT32)0x00000001)
#define WR_MDLCTRL  			((UINT32)0x00000002)
#define WR_INITCAN  			((UINT32)0x00000004)
#define WR_VOLTUP  	  			((UINT32)0x00000008)

#define WR_VDCDFT  				((UINT32)0x00000010)
#define WR_TEMPUP  				((UINT32)0x00000020)
#define WR_REONTIME   	  		((UINT32)0x00000040)
#define WR_WALKTIME 			((UINT32)0x00000080)

#define WR_ORDERTIME  			((UINT32)0x00000100)
#define WR_POWERDFT 			((UINT32)0x00000200)
#define WR_CURRRDFT				((UINT32)0x00000400)
#define WR_ACCURRRDFT      		((UINT32)0x00000800)

#define WR_MDLCTRLEXTEND	    ((UINT32)0x00001000)
#define WR_WATTACCUMULATE	    ((UINT32)0x00002000)
#define WR_WATTACCUMULATETIME	((UINT32)0x00004000)
#define WR_WATTANTITHIEFNUM		((UINT32)0x00008000) //PAUL 20220304 ANTITHIEFDELAY

#define WR_RECORDTIMESELF	    ((UINT32)0x00010000)
#define WR_DCOCP_PFCOVP	    	((UINT32)0x00020000)
#define WR_EEPROMROINTER		((UINT32)0x00040000)
#define WR_INTOSC_CALIBRATE        ((UINT32)0x00080000)
//----------------------------------------------------------------------
//define constant for module soft start states                                         
//-----------------------------------------------------------------------
#define POWON_STATE   	  		((UINT16)0x0000)
#define PFC_HWSOFT_START  		((UINT16)0x0001)
#define PFC_SWSOFT_START_INIT  	((UINT16)0x0002)
#define PFC_SWSOFT_START       	((UINT16)0x0003)
#define DC_SOFT_START_INIT  	((UINT16)0x0004)
#define DC_SOFT_START        	((UINT16)0x0005)
#define NORMAL_RUN_STATE        ((UINT16)0x0006)

#define DCOFF_SOFT_STATE        ((UINT16)0x0010)
#define PFCOFF_SOFT_STATE       ((UINT16)0x0020)

//----------------------------------------------------------------------
//define g_u16calflag BIT to check which vars hasn't be calibrated.                                        
//-----------------------------------------------------------------------
#define CAL_ALL_OK   	  		((UINT16)0x0000)
#define EPROM_READ_FAIL  		((UINT16)0x0001)
#define VDC_SAM_CAL_FAIL  		((UINT16)0x0002)
#define VDC_CON_CAL_FAIL  	  	((UINT16)0x0004)
#define IDC_SAM_CAL_FAIL  		((UINT16)0x0008)
#define IDC_CON_CAL_FAIL  		((UINT16)0x0010)
#define VAC_SAM_CAL_FAIL   	  	((UINT16)0x0020)
#define VPFC_SAM_CAL_FAIL 		((UINT16)0x0040)
#define VPFC_CON_CAL_FAIL  		((UINT16)0x0080)
#define POW_CON_CAL_FAIL 		((UINT16)0x0100)
#define IDC_OFFSET_CAL_FAIL		((UINT16)0x0200)
//#define IAC_SAM_CAL_FAIL      ((UINT16)0x0400)
#define  AC_FRE_CAL_FAIL	    ((UINT16)0x0800)
//------------------------------------------------------------------------------
//define constant for power limit flag                                        
//------------------------------------------------------------------------------
#define POW_LIM         		((UINT16)0x0001)
#define POW_LIM_TEMP    		((UINT16)0x0002)
#define POW_LIM_AC      		((UINT16)0x0004)


//----------------------------------均流模式-----------------------------------//
#define   SHARE_MODE_LOAD_PCT    0  //// according to  percent share load
#define   SHARE_MODE_REAL_CURR   1 // according to  real current share load

//------------------------------------------------------------------------------
//define constant for main relay zvs close flag                                        
//------------------------------------------------------------------------------
#define MAINRLY_ZVS_CTL_OPEN  			((UINT16)0x0000)
#define MAINRLY_ZVS_CTL_START   		((UINT16)0x0001)
#define MAINRLY_ZVS_CTL_CHECK  			((UINT16)0x0002)
#define MAINRLY_ZVS_CTL_CLOSE  			((UINT16)0x0003)

//------------------------------------------------------------------------------
//define constant for dcdc over status  flag                                     
//------------------------------------------------------------------------------
#define DC_OV_STATE_NORMAL			((UINT16)0x0000)
#define DC_OV_STATE_OV				((UINT16)0x0001)
#define DC_OV_STATE_DELAY_5S		((UINT16)0x0002)
#define DC_OV_STATE_RESTART			((UINT16)0x0003)
#define DC_OV_STATE_DELAY_5MINS		((UINT16)0x0004)

//------------------------------------------------------------------------------
//安规短路的重启状态定义                                   
//------------------------------------------------------------------------------

#define DC_SAFE_SHORT_STATUS_NORMAL		((UINT16)0x0000)
#define DC_SAFE_SHORT_STATUS_DCOV		((UINT16)0x0001)
#define DC_SAFE_SHORT_STATUS_DELY5S		((UINT16)0x0002)
#define DC_SAFE_SHORT_STATUS_DELY5MIN	        ((UINT16)0x0003)

//--------------------------------------------------------------------
//define constant for voltage smaple up and down limt   csm 091217                                    
//--------------------------------------------------------------------
#define IQ12_VDC_SAM_SYSA_LIMDN	  			((INT32)3277)  
#define IQ12_VDC_SAM_SYSA_LIMUP  			((INT32)4915)
#define IQ12_VDC_SAM_SYSB_LIMDN	  			(((INT32)(-20)) << 12)
#define IQ12_VDC_SAM_SYSB_LIMUP  			((INT32)20 << 12)

//--------------------------------------------------------------------
//define constant for control  smaple up and down limt   csm 091217                                    
//--------------------------------------------------------------------
#define IQ12_VDC_CON_SYSA_LIMDN	  			((INT32)3277)
#define IQ12_VDC_CON_SYSA_LIMUP  			((INT32)4915)
#define IQ12_VDC_CON_SYSB_LIMDN	  			(((INT32)(-20)) << 12)
#define IQ12_VDC_CON_SYSB_LIMUP  			((INT32)20 << 12)
//--------------------------------------------------------------------
//define constant for sample current  up and down limt   csm 091217                                    
//--------------------------------------------------------------------

#define IQ10_IDC_SAM_SYSA_LIMDN	  			((INT32)512)
#define IQ10_IDC_SAM_SYSA_LIMUP  			((INT32)1536)
#define IQ10_IDC_SAM_SYSB_LIMDN	  			(((INT32)(-20)) << 10) 
#define IQ10_IDC_SAM_SYSB_LIMUP  			((INT32)20 << 10)
//--------------------------------------------------------------------
//define constant for control current  up and down limt   csm 091217                                    
//--------------------------------------------------------------------
#define IQ10_IDC_CON_SYSA_LIMDN	  			((INT32)512)
#define IQ10_IDC_CON_SYSA_LIMUP  			((INT32)1536)
#define IQ10_IDC_CON_SYSB_LIMDN	  			(((INT32)(-20)) << 10) 
#define IQ10_IDC_CON_SYSB_LIMUP  			((INT32)20 << 10)

//--------------------------------------------------------------------
//define constant for vac rms  up and down limt   csm 091217                                    
//--------------------------------------------------------------------
#define IQ10_VAC_RMS_SYSA_LIMDN	  			((INT32)512)
#define IQ10_VAC_RMS_SYSA_LIMUP  			((INT32)1536)
#define IQ10_VAC_RMS_SYSB_LIMDN	  			(((INT32)(-120)) << 10) 
#define IQ10_VAC_RMS_SYSB_LIMUP  			((INT32)120 << 10)


//--------------------------------------------------------------------
//define constant for Iac rms  up and down limt                                     
//--------------------------------------------------------------------
#define IQ10_IAC_RMS_SYSA_LIMDN	  			((INT32)512)
#define IQ10_IAC_RMS_SYSA_LIMUP  			((INT32)1536)
#define IQ10_IAC_RMS_SYSB_LIMDN	  			(((INT32)(-10)) << 10) 
#define IQ10_IAC_RMS_SYSB_LIMUP  			((INT32)10 << 10)
//--------------------------------------------------------------------
//define constant for pfc sample   up and down limt   csm 091217                                    
//--------------------------------------------------------------------

#define IQ10_VPFC_SAM_SYS_LIMDN	  			((INT32)819)
#define IQ10_VPFC_SAM_SYS_LIMUP  			((INT32)1250)
//--------------------------------------------------------------------
//define constant for pfc contorl  up and down limt   csm 091217                                    
//--------------------------------------------------------------------
#define IQ10_VPFC_CON_SYS_LIMDN	  			((INT32)819)
#define IQ10_VPFC_CON_SYS_LIMUP  			((INT32)1250)
//--------------------------------------------------------------------
//define constant for power control   up and down limt   csm 091217                                    
//--------------------------------------------------------------------
#define IQ10_POW_CON_SYSA_LIMDN	  			((INT32)512)
#define IQ10_POW_CON_SYSA_LIMUP  			((INT32)1536)
#define IQ10_POW_CON_SYSB_LIMDN	  			(((INT32)(-1000)) << 10) 
#define IQ10_POW_CON_SYSB_LIMUP  			((INT32)1000 << 10)
//----------------------------------------------------------------------------
//--------------------------------------------------------------------
//#define IQ10_IPFC_OffSET_LIMDN	  			((INT32)70 << 10)
//#define IQ10_IPFC_OffSET_LIMUP  			((INT32)295 << 10)
//--------------------------------------------------------------------
//--------------------------------------------------------------------
#define IQ10_IDC_OffSET_LIMDN	  			((INT32)1 << 10)
#define IQ10_IDC_OffSET_LIMUP  				((INT32)460 << 10)//((INT32)310 << 10)

//--------------------------------------------------------------------
//--------------------------------------------------------------------
#define IQ10_SHORTB_LIMDN	  			( ((INT32)(-400)) << 10 )
#define IQ10_SHORTB_LIMUP  				((INT32)400 << 10)

//--------------------------------------------------------------------
#define IQ10_VDC_REAL_LIMDN	  				(((INT32)47) << 10)
#define IQ10_VDC_REAL_LIMUP  				(((INT32)58) << 10)


//--------------------------------------------------------------------
//define constant AC frequency  up and down limt                                    
//--------------------------------------------------------------------
#define IQ10_AC_FRE_SYSA_LIMDN	  			((INT32)512)
#define IQ10_AC_FRE_SYSA_LIMUP  			((INT32)1536)
#define IQ10_AC_FRE_SYSB_LIMDN	  			(((INT32)(-20)) << 10) 
#define IQ10_AC_FRE_SYSB_LIMUP  			((INT32)20 << 10)
//==============================默认校准系数===============================//
#define VDC_SAM_SYSA_DF_VALUE	((INT32)4096)		//dcdc 电压采样校准系数A
#define VDC_SAM_SYSB_DF_VALUE	((INT32)(0))	//dcdc 电压采样校准系数B

#define VDC_CON_SYSA_DF_VALUE	((INT32)4096)		//dcdc 电压控制校准系数A
#define VDC_CON_SYSB_DF_VALUE	((INT32)(0))	//dcdc 电压控制校准系数B

#define IDC_SAM_SYSA_DF_VALUE	((INT32)1024)		//dcdc 电流采样校准系数A
#define IDC_SAM_SYSB_DF_VALUE	((INT32)0)			//dcdc 电流采样校准系数b

#define IDC_CON_SYSA_DF_VALUE	((INT32)1024)		//dcdc限流点校准系数a
#define IDC_CON_SYSB_DF_VALUE	((INT32)0)			//dcdc限流点校准系数b

#define VAC_RMS_SYSA_DF_VALUE	((INT32)1024)		//交流电压有效值校准系数a
#define VAC_RMS_SYSB_DF_VALUE	((INT32)0)			//交流电压有效值校准系数b

#define IAC_RMS_SYSA_DF_VALUE	((INT32)1024)		//交流电流有效值校准系数a
#define IAC_RMS_SYSB_DF_VALUE	((INT32)0)			//交流电流有效值校准系数b


#define VAC_PK_SYSA_DF_VALUE	((INT32)1024)		//交流电压峰值校准系数a
#define VAC_PK_SYSB_DF_VALUE	((INT32)0)			//交流电压峰值校准系数b	

#define VPFC_SAM_SYS_DF_VALUE	((INT32)1024)		//pfc 电压采样校准系?
#define VPFC_CON_SYS_DF_VALUE	((INT32)1024)		//pfc 电压样校准系数

#define POW_CON_SYSA_DF_VALUE	((INT32)1024)		//限功率校准系数
#define POW_CON_SYSB_DF_VALUE	((INT32)0)			//限功率校准系数b

#define IDC_OFFSET_DF_VALUE     ((INT16)100)        //DC 默认偏置电流

#define AC_FRE_SYSA_DF_VALUE	((INT32)1024)		//dcdc限流点校准系数a
#define AC_FRE_SYSB_DF_VALUE	((INT32)0)			//dcdc限流点校准系数b

#define AC_ADJUST_SYSA          ((INT32)289)       //源调整系数a
#define AC_ADJUST_SYSB          ((INT32)58)          //源调整系数b
//----------------------------------------------------------------------
//define constant for e2prom default data and up down limt  csm 091217                                       
//------------------------------------------------------------------------

//#define IQ10_H_AC_L_DC_LIM_VALUE	((INT32)1024) 

#define IQ10_POW_DEF_LIM_VALUE		((INT32)1024)
#define IQ10_POW_LIM_DN		       	((INT32)10)
#define IQ10_POW_LIM_UP		       	((INT32)1250)

#define IQ10_IDC_DEF_LIM_VALUE		((INT32)1130)//保证48V最大功率正偏//((INT32)1250)
#define IQ10_IDC_LIM_DN				((INT32)10)
#define IQ10_IDC_LIM_UP				((INT32)1130)//保证48V最大功率正偏//((INT32)1250)

#define IQ10_IAC_DEF_VALUE		    (((INT32)30) << 10) 
#define IQ10_IAC_DN		       		(((INT32)1) << 10) 
#define IQ10_IAC_UP		       		(((INT32)31) << 10)

#define IQ10_TEMP_LIM_POW_MAX		((INT32)1024)
#define IQ10_AC_LIM_POW_MAX			((INT32)1024)
#define IQ10_AC_LIM_POW_50PCT		((INT32)512)
#define IQ10_DSP_LIM_POW_93PCT       ((INT32)953)
#define IQ10_DSP_LIM_POW_89PCT       ((INT32)911)
#define IQ10_DSP_LIM_POW_86PCT       ((INT32)880)
#define IQ10_AC_LIM_POW_MIN			((INT32)358)    
#define IQ10_POW_LIM_MIN			((INT32)1 )
#define IQ10_IDC_LIM_121PCT			((INT32)1239) // 1.21*1024
#define IQ10_IDC_LIM_110PCT         ((INT32)1130) // 1.21*1024
#define IQ10_IDC_LIM_100PCT			((INT32)1024)  // 1.0*1024

//paul-20200301
#define IQ10_AntiThiefDelay_UP          (((INT32)24) << 10)  // 1.0*1024
#define IQ10_AntiThiefDelay_DN          ((INT32)0)  // 0*1024
#define IQ10_AntiThiefDelay_DFT         ((INT32)1024)  // 1.0*1024

//----------------------------默认限功率系数 -----------------------------//                               
#define IQ10_SET_POW_LlM_DEF			((INT32)1024)
#define IQ10_SET_POW_TRUE_LlM_DEF		((INT32)1024)
#define IQ10_POWERLlMlTMAX	        	((INT32)1250)
#define IQ10_CURRLlMlTMAX	        	((INT32)1130)
#define IQ10_CURRLlMlTMAXEXTRA	        	((INT32)1130)  //保证48V功率正偏

#define IQ10_CURRLlMlTMAX_2900W               ((INT32)1250)
#define IQ10_CURRLlMlTMAX_3000W               ((INT32)1250)
#define IQ10_CURRLlMlTMAX_3500W               ((INT32)1250)

#define IQ10_CURRLlMlTMAXEXTRA_2900W              ((INT32)1250)
#define IQ10_CURRLlMlTMAXEXTRA_3000W             ((INT32)1250)
#define IQ10_CURRLlMlTMAXEXTRA_3500W             ((INT32)1250)
//------------------------------------------------------------------------------
//define constant for EEPROM value                                        
//------------------------------------------------------------------------------
#define EPROM_READY_LONG  		((INT32)0x55AA55AA)
#define EPROM_READY_UINT  		((UINT16)0x55AA)

#define EPROM_CLEAR_FFFFFFFF  	((UINT32)0xFFFFFFFF)
//define constant for  permit AC over control                                          
//------------------------------------------------------------------------------
#define PERMIT_ACOVER    		((UINT16)0x0000) 
#define FORBID_ACOVER   	    ((UINT16)0x0001) 
#define PERMIT_ACOVER_HANDLED   ((UINT16)0x0002)

//------------------------------------------------------------------------------
//define constant for dc pfc on off control                                       
//------------------------------------------------------------------------------
#define DCDC_OFF        		((UINT16)0x01) 
#define PFC_OFF         		((UINT16)0x02) 

//------------------------------------------------------------------------------
//define constant for EEPROM address                                    
//----------------------------------------------------------------------------
#define EEPROM_CHK_ADDR			0
#define VDC_SAM_SYSA_ADDR		4		//dcdc 电压采样校准系数
#define VDC_SAM_SYSB_ADDR		8
#define VDC_CON_SYSA_ADDR		12		//dcdc 电压控制校准系数
#define VDC_CON_SYSB_ADDR		16
#define IDC_SAM_SYSA_ADDR		20		//输出电流采样校准系数
#define IDC_SAM_SYSB_ADDR		24
#define IDC_CON_SYSA_ADDR		28		//限流点校准系数
#define IDC_CON_SYSB_ADDR		32
#define VAC_SAM_SYSA_ADDR		40		//交流电压采样校准系数
#define VAC_SAM_SYSB_ADDR		44
#define VPFC_SAM_SYS_ADDR		56		//pfc 电压采样校准系数
#define VPFC_CON_SYS_ADDR		60
#define POW_CON_SYSA_ADDR		64		//限功率校准系数
#define POW_CON_SYSB_ADDR		68
#define POW_DFT_ADDR			72		//默认限功率点
#define CURR_DFT_ADDR			76		//默认限流点
#define ACCURR_DFT_ADDR 		80		//输出电压过压点
#define VDC_DFT_ADDR			84		//浮充电压
#define VDC_HIGH_ADDR			88		//输出电压上限
#define TEMP_HIGH_ADDR			92
		
#define RUN_TIME_ADDR			96		//模块总运行时间
#define RECT_ACT_ADDR			100	 	//模块风扇全速、walkin、允许过压起机、过压版本等功能使能
#define REON_TIME_ADDR			104		//HVSD 重启时间
#define WALKIN_TIME_ADDR		108		//walk in时间
#define ORDER_TIME_ADDR			112		//模块顺序起机时间

#define ENP_LO_SN_ADDR			116		//模块序列号
#define ENP_HI_SN_ADDR			120
#define VERSION_NO_ADDR			124		//模块版本信息
//模块特征类型数据直接写死，不再从EPROM里写入、读出
// #define CHARACT_DATA_ADDR		128		//模块特征数据


#define BAR_CODE0_ADDR			132		//模块条码信息
#define BAR_CODE1_ADDR			136
#define BAR_CODE2_ADDR			140
#define BAR_CODE3_ADDR			144

#define MEND_DATA0_ADDR			148		//模块维护信息
#define MEND_DATA1_ADDR			152
#define MEND_TIMES_ADDR			156
#define	AC_MDLCTRLEXTEND_ADDR	160	

#define IAC_SAM_SYSA_ADDR		164		//交流电流采样校准系数
#define IAC_SAM_SYSB_ADDR		168

#define IDC_OFF_SET_ADDR        172
#define SHORTB_ADDR             176

//#define AC_FRE_SYSA_ADDR		180		//交流频率采样校准系数
//#define AC_FRE_SYSB_ADDR		184

//three addr for alarm
#define RUNTIME_RECORD_BYSELF   200//232	//模块当前的运行时间，每隔30分钟存一次
#define RECORD_EEPROM_ADDR   	204//236	//模块当前的EEPROM地址

#define INPUT_WATT_ACCUMULATE_ADDR       208 //输入电量
#define INPUT_WATT_ACCUMULATE_TIME_ADDR  212 //输入电量累计时间
#define DCOCPPFCOVPCNT_ADDR			216          //DCOCP 次数

#define INTOSC_SET_ADDR         220

//paul-20200301
#define ANTI_TITHIEF_DELAY          228
#define MDL_CUSTOMER_NUM_ADDR       240    //Antitheft CustomerNumber and SitNumber address 20210806 Eileen



//----------------------故障日志相关-----------------------------------------//

//one addr 
#define ALARM_START_ADDR   		1024 
#define ALARM_END_ADDR   		2038 //2038+6-1=2043


#define BLOCK_END_WORD_ADDR   	246 
#define BLOCK_ADDR   			7 


#define ALARM_CLEAR_END_ADDR   	1276 //1276+4=1280  ,1279


//每条记录占用6个EEPROM地址，一共168条故障---4block* (1275-1024+1)/6= 168
//记录的EEPROM的首地址 1024~1270，记录的首地址最后一条为2038


/*EEPROM 地址

1024	1280	1536	1792  第一条记录
  ~		  ~		 ~       ~
1270	1526	1782	2038  块最后一条记录
1271	1527	1783	2039
1272	1528	1784	2040
1273	1529	1785	2041
1274	1530	1786	2042
1275	1531	1787	2043

*/

//------------------------------------------------------------------------------
//define for INTOSC calibrate
//------------------------------------------------------------------------------
#define  INTOSC_ERR 0x0800
#define  INTOSC_SUCCESS    0x8000
#define  OUTOSC_RETURN    0x0080
#define  Q10_PERCENT3D15 (INT16)3226
#define  Q10_PERCENT1D9  (INT16)1946
#define  Q10_PERCENT0D65 (INT16)666
#define  Q10_N_PERCENT0D65   (INT16)-666
#define  Q10_N_PERCENT1D9    (INT16)-1946
#define  Q10_N_PERCENT3D15   (INT16)-3226

//------------------------------------------------------------------------------
//define constant for normal/calibration state                                          
//------------------------------------------------------------------------------
#define NORMAL_RUN_MODE			0x0001
#define CALIBRA_RUN_MODE		0x0000  

#define PWL0         (volatile Uint16*)0x003F7FF8   /* Password 0 */
#define PWL1         (volatile Uint16*)0x003F7FF9   /* Password 1 */
#define PWL2         (volatile Uint16*)0x003F7FFA   /* Password 2 */
#define PWL3         (volatile Uint16*)0x003F7FFB   /* Password 3 */
#define PWL4         (volatile Uint16*)0x003F7FFC   /* Password 4 */
#define PWL5         (volatile Uint16*)0x003F7FFD   /* Password 5 */
#define PWL6         (volatile Uint16*)0x003F7FFE   /* Password 6 */
#define PWL7         (volatile Uint16*)0x003F7FFF   /* Password 7 */

#endif
