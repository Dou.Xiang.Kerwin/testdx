/*=============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : gbb_cancom.c
 *  PURPOSE  : rectifier communicate with controller and other rectifiers	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A000           DSP               Created.   Pre-research 
 *	
 *============================================================================*/
#include "f28004x_device.h"		// DSP280x Headerfile Include File
#include "App_cancom.h"
#include "App_vAdcTreatment.h"
#include  "App_warnctrl.h"
#include  "App_cal.h"
#include  "App_start.h"
#include  "GBB_epromdata.h"
#include  "Drv_CAN.h"
#include "GBB_Can_Interface.h"
#include  <App_isr.h>
#include  "CLAShared.h"
#include "GBB_constant.h"
#include <App_constant.h>
#include <Prj_macro.h>
#include  "Prj_config.h"
#include <App_main.h>
#include "IQmathLib.h"
#include  "App_vEventLog.h"


#define MDLCTRL_ALLOWACOV         0x0100// 2  ACOV allow start
#define MDLCTRL_INITCAN           0x0200 // CAN INIT
#define MDLCTRL_SHORTRELAY        0x0400 // 10 turn off PFC&DC and main relay
#define MDLCTRL_IDENTIFY          0x0800 // 11 set rect identify    1:identify  0:no identify
#define MDLCTRL_FANFULL           0x1000 // 12  set fan full speed  1:full      0:adjust
#define MDLCTRL_WALKIN            0x2000 // 13  set Walkin          1:enable    0:disable
#define MDLCTRL_HVSDRESET         0x4000// 14 HVSD RESET
#define MDLCTRL_OFFCTRL           0x8000 // // 15  set rectifier on/off  1:off          0:on
#define MDLCTRL_ANTITHEFTUNLOCK   0x0200 //paul-20220228

/*******************************************************************************
*variables definition:these variables only can use in this file 														  
*******************************************************************************/
//old rec in flag
static	UINT16	s_u16OldRecInFlag;


//for all modules` ID restore buffer 
static frameaddid s_afMdlAddID[MAX_NUM + 2];


static UINT16 s_u16SysAvgVolt;	  	// system average output volt, ls 20120224

static UINT16 s_u16SysRateCurr;		// system rating output current
static ubitfloat s_lq10SysAvgPct;
static UINT16 s_u16SysMixStatus;	// system mix system status, ls 20120224

//module`s adjust attribute for voltage regulation
static UINT16 s_u16CurrCtrlFlag;
//for new 0x11,0x12 protocol 20120224
static UINT16 s_u16NewCurrCtrlFlag;

//for AC limit mode select
//ubitfloat g_fAcLimModeFlag;
//ubitintaextend	g_u16MdlCtrlExtend;
 
static ubitfloat s_lq10SetSysTmp;	// setsys temp variable
static ubitfloat s_lq10AdjTmp;      //temp variable for adjust
static UINT16 s_u16MdlCalibra;		//MdlCalibra flag
static UINT16 s_u16ForbidConnect;	//set rectifier volt connect flag
static UINT16 s_u16SetEepromAdd;    //set eeprom address by can

//variables for read memery
static UINT16	s_u16RdMemNum;
static UINT16  *s_pu16MemScopeAdd0;
static UINT16  *s_pu16MemScopeAdd1;
UINT16	g_u16RdNumSet;
static UINT16	g_u16ComIntFlag;

static ubitfloat g_lq10MdlCalibVolt;
//paul-20220228
UINT16  g_u16CustNumDisMatch;          // 模块站点信息是否匹配状态位
//Anti-theft
Uint32  g_u32SitNumberRx;    //防盗功能，模块接收的站点信息；
UINT16  g_u16CustomerNumberRx; //防盗功能,模块接收的用户信息;
Uint32  g_u32SitNumberTx;   //防盗功能，模块自身存的站点信息；
UINT16  g_u16CustomerNumberTx; //防盗功能，模块自身存的用户信息；
UINT16  g_u16ReceiveDataStatus; //防盗功能，接收数据标志位 Eileen


/*******************************************************************************
*globle variables definition:these variables  can use in all of the files 														  
*******************************************************************************/
UINT16 g_u16AddIdentifyFlag; 	//address auto-identifiication flag
UINT16 g_u16AddIdentifyNum; 	//address auto-identifiication cnt
UINT16 g_u16MdlAddrBuff;		//module address
UINT16 g_u16AddressSmall;		//minnum  address on system
UINT16 g_u16VoltAdjDelta;		// volt adjust delta

INT16  g_i16CurrDelta;	        // volt adjust delta cal by current adjust
ubitfloat g_lq10MdlAvgCurr;		// system average current 

//variables for module`s hardware and software version	
UINT16 g_u16VersionNoSw;

// variables for nor derating mixed
INT32	g_i32CurrChgFactor;
INT32	g_i32AvgCurrChgFactor;

// rectifier mix system status, ls 20120224
UINT16 g_u16MdlMixStatus;		

//Power mixed flag 													  
UINT16	g_u16PowerMixedSystemType;
UINT16	g_u16PowerMixedFlag;

UINT16	g_u16SelfDxCtrlNextTimer;	// delay to next which no answer
UINT16	g_u16MdlNumber;//在位模块数量

static UINT16 s_u16FuseNextTime;	// delay to next fuse 
/*******************************************************************************
*functions declare:these functions only can use in this file                                    
*******************************************************************************/
//module handle command 00 from SCU
void vReplyDataForComm00(void);

//SCU set the data of rectifier by bit
static void vScuSetByBit(void);

//module handle command 20 from SCU
static void vReplyDataForComm20(void);

//module handle command 30 from SCU
static void vReplyDataForComm30(void);

//RRP protocol handle
static void vRrpCanDataHandle(void);

//for master cal average current
static void vMasterLoadSharingCal(void);

//RmP protocol handle
static void vRmpProtocol(void);

//get rectifier warn and state
static float fStatusTrans(void);

//address auto-identify				
static void vAddressCal(void);

// AC <80v,only trans one time 00 and not answser scu

static void vACComInterrupt(void);


/*******************************************************************************
*Function name:		vCanDataParsing()											  
*Description:		Parsing CAN Data 
*******************************************************************************/
void vCanDataParsing(void)
{	


	while(sCanRead(&g_stCanRxData) != CAN_RXBUF_EMPTY)
	{
		sDecodeCanRxFrame(g_stCanRxData);

		//RRP protocol handle
		if(g_u16PROTNO == RRP)
		{
         	vRrpCanDataHandle();
		}
		//RmP protocol handle
		else if((g_u16PROTNO == RMP) 
		       && ((g_u16DSTADDR == g_u16MdlAddr) 
		           || (g_u16DSTADDR == BROAD_CAST_ADDR)))
		{
			//receive scu`s information,communcation is normal,clr can fail cnt
			g_u16CanFailTime = 0;
			//clr can fail flag
			g_u16MdlStatus.bit.NOCONTROLLER = 0;
			
			/*when module receive command 0x00,frist refresh rectifier total run 
             time and load sharing voltage,set the data of rectifier by bit that 
             which scu set,then reply data:0x01,0x02,0x03,0x04,0x05,0x07,0x40,
             0x54,0x58*/
			if(g_u16MsgType == 0x00)		
			{			

			    //vReplyDataForComm00();
			    if(g_u16ComIntFlag==0)
			   {
			        vReplyDataForComm00();
			   		if(g_u16MdlStatusExtend.bit.ACCOMHALT)
					{
						g_u16ComIntFlag=1;
					
					}			   			
				}
			}
			/*when module receive command 0x20,reply data:0x51,0x55,0x56,0x5A,
			  0x5B,0x5C,0x5D*/
			else if(g_u16MsgType == 0x20)
			{
				vReplyDataForComm20();
			}
            /*when module receive command 0x30,reply data:0x40,0xD0,0xD3*/
			else if(g_u16MsgType == 0x30)		
			{			
				vReplyDataForComm30();
			}

			
			
			else if((g_u16MsgType == 0x01)
			       || (g_u16MsgType == 0x02 )
			       || (g_u16MsgType == 0x03) )	
			{				
				vRmpProtocol();
			}
			
			vACComInterrupt();					
		}
	}
}

/*******************************************************************************
*Function name:  vReplyDataForComm00()
*Description: when module receive command 0x00,frist refresh rectifier total run 
*            time and load sharing voltage,set the data of rectifier by bit that 
*            which scu set,then reply data:0x01,0x02,0x03,0x04,0x05,0x07,0x40,
*            0x54,0x58
*******************************************************************************/
void vReplyDataForComm00(void)
{
   ubitfloat ubitfData;
    
	ubitfData.lData = (INT32)0;
    // refresh rectifier total run time
	if((g_lq10CanData.lData >= g_lq10RunTime.lData + 1000)
	  && (g_lq10CanData.lData <= g_lq10RunTime.lData + 1050))
	{
		g_u16EpromWr.bit.RUNTIME = 1;
		g_lq10RunTime.lData = g_lq10CanData.lData;
	}
	
	//load sharing voltage setting
	g_u16VoltAdjDelta = g_u16ValueType & 0x00ff;

	//clr the address offset for read DSP memory of DCDC continually 
	s_u16RdMemNum = 0;

	//set error type
	g_u16ErrType = NORMAL;
	
	//scu set the data of rectifier by bit
	vScuSetByBit();

	/*reply data to scu:0x01,0x02,0x03,0x04,0x05,0x07,0x40,0x54,0x58*/
	//0x01:get rectifier Volt
	sEncodeCanTxID(RMP,PTP_MODE,CSU_ADDR,DATA_CNT);
	sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0x01,g_lq10MdlVolt);
	sCanWrite5(&g_stCanTxData);

	//set error type  
	g_u16ErrType = NORMAL;

	//0x02: get rectifier real Curr   
	sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0x02,g_lq10MdlCurr);
	sCanWrite5(&g_stCanTxData);

	//0x03:get rectifier Curr Limit

	if (g_lq10MdlLimitDisp.lData >= IQ10_IDC_LIM_121PCT)  
	{
		ubitfData.lData = IQ10_IDC_LIM_121PCT;
	}
	else
	{
		ubitfData.lData = g_lq10MdlLimitDisp.lData;
	}


	sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0x03,ubitfData);
	sCanWrite5(&g_stCanTxData); 

	//0x04:get rectifier U1 temperature     
//	sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0x04,g_lq10TempDcDisp);
    sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0x04,g_lq10TempAmbiDisp);
	sCanWrite5(&g_stCanTxData);

	//0x05:get rectifier aca volt     
	sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0x05,g_lq10AcVolt);
	sCanWrite5(&g_stCanTxData);

	

	//0x07:get rectifier curr for display    
	sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0x07,g_lq10MdlCurrDisp2);
	sCanWrite5(&g_stCanTxData);

	//0x40:get rectifier warm and status
	ubitfData.fd = fStatusTrans();	    
	sEncodeCanTxData(REPLY_BY_BIT,g_u16ErrType,0x40,ubitfData);
	sCanWrite5(&g_stCanTxData);

	//0x54:read rectifier SearialNo low
	ubitfData.uintdata[1].id = g_u16NodeId0H_TO_CONTROLLER;
	ubitfData.uintdata[0].id = g_u16NodeId0L;	    
	sEncodeCanTxData(REPLY_BY_BYTE,g_u16ErrType,0x54,ubitfData);
	sCanWrite5(&g_stCanTxData);

    //0x58:read the whole time of module running (hour)	
    sEncodeCanTxID(RMP,PTP_MODE,CSU_ADDR,DATA_END);	    
	sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0x58,g_lq10RunTime);
	sCanWrite5(&g_stCanTxData);  
}

/*******************************************************************************
*Function name:  vReplyDataForComm20()
*Description: when module receive command 0x20,reply data: 0x51,0x55,0x56,0x5A,
*             0x5B,0x5C,0x5D
*******************************************************************************/
static void vReplyDataForComm20(void)
{
  	ubitfloat ubitfData;
     ubitfData.lData = (INT32)0;
    /*reply data:0x51,0x55,0x56,0x5A,0x5B,0x5C,0x5D*/	
    //0x51:read rectifier charact
    sEncodeCanTxID(RMP,PTP_MODE,CSU_ADDR,DATA_CNT); 
	ubitfData.uintdata[1].id = g_u16CharactData0H;
	ubitfData.uintdata[0].id = g_u16CharactData0L;
	sEncodeCanTxData(REPLY_BY_BYTE,NORMAL,0x51,ubitfData);
	sCanWrite5(&g_stCanTxData);

    //0x55:read rectifier SerialNo hi 
	ubitfData.uintdata[1].id = g_u16NodeId1H;
	ubitfData.uintdata[0].id = g_u16NodeId1L_TO_CONTROLLER;
	sEncodeCanTxData(REPLY_BY_BYTE,NORMAL,0x55,ubitfData);
	sCanWrite5(&g_stCanTxData);

    //0x56:read rectifier hw/sw version number 
	ubitfData.uintdata[1].id = g_u16VersionNoHw;
	ubitfData.uintdata[0].id = g_u16VersionNoSw;
	sEncodeCanTxData(REPLY_BY_BYTE,NORMAL,0x56,ubitfData);
	sCanWrite5(&g_stCanTxData);

    //0x5A:read rectifier bar code0 contain 
	ubitfData.uintdata[1].id = g_u16BarCode0H;
	ubitfData.uintdata[0].id = g_u16BarCode0L;
	sEncodeCanTxData(REPLY_BY_BYTE,NORMAL,0x5A,ubitfData);
	sCanWrite5(&g_stCanTxData);

    //0x5B:read rectifier bar code1 contain 
	ubitfData.uintdata[1].id = g_u16BarCode1H;
	ubitfData.uintdata[0].id = g_u16BarCode1L;
	sEncodeCanTxData(REPLY_BY_BYTE,NORMAL,0x5B,ubitfData);
	sCanWrite5(&g_stCanTxData);

    //0x5C:read rectifier bar code2 contain 
	ubitfData.uintdata[1].id = g_u16BarCode2H;
	ubitfData.uintdata[0].id = g_u16BarCode2L;
	sEncodeCanTxData(REPLY_BY_BYTE,NORMAL,0x5C,ubitfData);
	sCanWrite5(&g_stCanTxData);

    //0x5D:read rectifier bar code3 contain
    sEncodeCanTxID(RMP,PTP_MODE,CSU_ADDR,DATA_END); 
	ubitfData.uintdata[1].id = g_u16BarCode3H;
	ubitfData.uintdata[0].id = g_u16BarCode3L;
	sEncodeCanTxData(REPLY_BY_BYTE,NORMAL,0x5D,ubitfData);
	sCanWrite5(&g_stCanTxData);

}

/*******************************************************************************
*Function name:  vReplyDataForComm30()
*Description: when module receive command 0x30,reply data:0x40,0xD0,0xD3
*******************************************************************************/
static void vReplyDataForComm30(void)
{
   ubitfloat ubitfData;

    //监控可以设置电量清零（0x30综合命令的byte2 bit0）
	if ((g_u16ValueType & 0x0100)>>8)
	{
	    //模块保存的电量信息清0
		g_u16MdlPowerAcuumClearFlg=1;
	
	}
	else
	{
		g_u16MdlPowerAcuumClearFlg=0;
	}


    //paul-20200228
    /*set Anti-theft unlock command,0:disable,1:enable*/
    if((g_u16ValueType & MDLCTRL_ANTITHEFTUNLOCK)>>9)
    {
        g_u16MdlCtrl.bit.AntiTheftUnlockFlag = 1;
    }
    else
    {
        g_u16MdlCtrl.bit.AntiTheftUnlockFlag = 0;
    }

	//reply data to scu:0x40,0xD0,0XD3,0XD4
	//0x40:get rectifier warm and status
	sEncodeCanTxID(RMP,PTP_MODE,CSU_ADDR,DATA_CNT);
	ubitfData.fd = fStatusTrans();	    
	sEncodeCanTxData(REPLY_BY_BIT,g_u16ErrType,0x40,ubitfData);
	sCanWrite5(&g_stCanTxData);

	//0xD0:get rectifier aca curr       
	sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0xD0,g_lq10AcCur);
	sCanWrite5(&g_stCanTxData);	


   //0xD3:get rectifier aca POWER    
    sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0xD3,g_lq10AcPow);
	sCanWrite5(&g_stCanTxData);	

	//0xD4:	 get rectifier power accumulate
	if(g_lSCUGetWattAccum.lData < POWER_Q21)
	{
	   //g_lSCUGetWattAccum单位为：W*H,定标为：Q:0
	   //上报监控转换为：KW*H ,g_lSCUGetWattAccum/1000
	   //上面计算的结果Q10定标后转换为浮点数，精度为1/1024=0.0009
	   //即上报误差为1(w*h).
	   ubitfData.fd = _IQ10toF((g_lSCUGetWattAccum.lData*128)/125);
	}
	else
	{
		//g_lSCUGetWattAccum单位为：W*H,定标为：Q:0
	   //上报监控转换为：KW*H ,g_lSCUGetWattAccum/1000
	   //上面计算的结果Q4定标后转换为浮点数，精度为1/16=0.0625
	   //即上报误差为60(w*h).
	   ubitfData.fd = _IQ4toF((g_lSCUGetWattAccum.lData*2)/125);
	} 	
    sEncodeCanTxData(REPLY_BY_BYTE,g_u16ErrType,0xD4,ubitfData);
	sCanWrite5(&g_stCanTxData);

	//0xD5:	 get rectifier aca FRE	
    sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0xD5,g_lq10AcFre);
	sCanWrite5(&g_stCanTxData);


   //0x01:	// get rectifier Volt 
    sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0x01,g_lq10MdlVolt);
	sCanWrite5(&g_stCanTxData); 

	  //0x01:	// get rectifier Volt 
    sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0x01,g_lq10MdlVolt);
	sCanWrite5(&g_stCanTxData);  

	 //0x01:	// get rectifier Volt 
    sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0x01,g_lq10MdlVolt);
	sCanWrite5(&g_stCanTxData); 

	  //0x01:	// get rectifier Volt 
	sEncodeCanTxID(RMP,PTP_MODE,CSU_ADDR,DATA_END);
    sEncodeCanTxIQ10Data(REPLY_BY_BYTE,g_u16ErrType,0x01,g_lq10MdlVolt);
	sCanWrite5(&g_stCanTxData);

}


/*******************************************************************************
*Function name:  vScuSetByBit()
*Description: SCU set the data of rectifier by bit
*******************************************************************************/
static void vScuSetByBit(void)
{
	/*set rectifier on/off,0:on,1:off*/
	if (g_u16ValueType & MDLCTRL_OFFCTRL)	//0X8000							
	{
		//off control
		g_u16MdlCtrl.bit.OFFCTRL = 1;
	}
	else if(!(mScuSetMdl_Off()))
	{
		//rectifier on if no fault 
		g_u16MdlCtrl.bit.OFFCTRL = 0;
	}

	else
	{
		g_u16ErrType = COMM_FAIL;
	}
	
	/*set rectifier HVSD reset,0:normal,1:reset */
	if ((g_u16ValueType & MDLCTRL_HVSDRESET) && (g_u16MdlStatus.bit.HVSDLOCK || g_u16SafetyShortStatus))	
	{
	  g_u16MdlCtrl.bit.HVSDRESET = 1; //0x4000

	}
	
	/*set rectifier walk in command,0:disable,1:enable*/
	if (((g_u16ValueType & MDLCTRL_WALKIN ) >> 13) != (g_u16MdlCtrl.bit.WALKIN))
    // 按照协议调整g_u16MdlCtrl的位,把WALKIN放到和协议里相同的0X2000位置
//    if (( g_u16ValueType & MDLCTRL_WALKIN ) != (g_u16MdlCtrl.bit.WALKIN))
	{
		//write eeprom flag set
		g_u16EpromWr.bit.MDLCTRL = 1;

		// walk-in enable 
		if (g_u16ValueType & MDLCTRL_WALKIN )		//0X2000					
		{
			g_u16MdlCtrl.bit.WALKIN = 1;
		}
		// normal, walk-in disable
		else			 
		{
			g_u16MdlCtrl.bit.WALKIN = 0;		
		}
	}

	// set FAN  full-speed command,0:normal,1:full-speed 
	if ( (( g_u16ValueType & MDLCTRL_FANFULL)>>12)  != (g_u16MdlCtrl.bit.FANFULL)  )
	{
		//write eeprom flag set
		g_u16EpromWr.bit.MDLCTRL = 1;

		// FAN full-speed
		if (g_u16ValueType & MDLCTRL_FANFULL)	//0X1000						 
		{
			g_u16MdlCtrl.bit.FANFULL = 1;
		}
		// fan control normal
		else 			 
		{
			g_u16MdlCtrl.bit.FANFULL = 0;	
		}
	}

	// set rectifier green-led control mode,0:normal,1:twinkle 
	if (g_u16ValueType & MDLCTRL_IDENTIFY)		//0X800						
	{
		// green-led twinkle 
		g_u16MdlCtrl.bit.IDENTIFY = 1;
	}
	else 							
	{
		//normal
		g_u16MdlCtrl.bit.IDENTIFY = 0;		
	}	

	// set rectifier relay action for over voltage,0:normal,1:break
	if (g_u16ValueType & MDLCTRL_SHORTRELAY)			//0X400		
	{
		// relay break away, AC off
		g_u16MdlCtrl.bit.SHORTRELAY = 1;
	}
	else 			
	{
    	// normal 
    	g_u16MdlCtrl.bit.SHORTRELAY = 0;
	}
	
	// set rectifier can initialization,0:normal,1:reset 
	if (g_u16ValueType & MDLCTRL_INITCAN )	//0X200	
	{
		g_u16EpromWr.bit.INITCAN = 1;
        g_u16MdlCtrl.bit.INITCAN = 1;
	}
	
	// set weather rectifier permit over voltage,0:permit1:forbid 
		if ((g_u16ValueType & MDLCTRL_ALLOWACOV) \
	     == ((IsrVars._u16PermitOverAc & FORBID_ACOVER) << 8))			
	{
		//write eeprom flag set
		g_u16EpromWr.bit.MDLCTRL = 1;

		if (g_u16ValueType & MDLCTRL_ALLOWACOV)    //0X100
		{
			//permit over voltage
			IsrVars._u16PermitOverAc = PERMIT_ACOVER;
			g_u16MdlCtrl.bit.ALLOWACOV = 1;
	 		g_u1660sTimer = 0;
		}
		else 
		{
			//forbid over voltage
			IsrVars._u16PermitOverAc = FORBID_ACOVER;
			g_u16MdlCtrl.bit.ALLOWACOV = 0;			
		}
	}
}

/*******************************************************************************
*Function name:  fStatusTrans()
*Description: get rectifier warn and state
*******************************************************************************/
static float fStatusTrans(void)
{
	UINT16	u16TmpH,u16TmpL;
	ubitfloat tagStatusSd;
	
	u16TmpH = 0;
	u16TmpL = 0;

	if(g_u16MdlStatus.bit.DCOV 
		||g_u16MdlStatusExtend.bit.HWHVSD
        ||g_u16RunFlag.bit.HWHVSDDisp
		|| g_u16MdlStatus.bit.HVSDLOCK
		|| g_u16SafetyShortStatus)
	{
		u16TmpH = u16TmpH | 0x8000;		// over volt
	}

	if(g_u16MdlStatus.bit.RECTOT || g_u16MdlStatus.bit.AMBIOT
		|| g_u16MdlStatusExtend.bit.PFCOT )
	{
		u16TmpH = u16TmpH | 0x4000;		// over temp
	}

	if( (g_u16MdlStatus.bit.DCOV
		|| g_u16MdlStatusExtend.bit.HWHVSD
        || g_u16RunFlag.bit.HWHVSDDisp
		|| g_u16MdlStatus.bit.HVSDLOCK
		|| g_u16SafetyShortStatus) 
		|| g_u16MdlStatusExtend.bit.DCFAULT	 //ls 20111129
	    || (g_u16CurrSerUnBal >= TIME20S) 
	    || g_u16MdlStatus.bit.IDERR
	//    || g_u16MdlStatusExtend.bit.DCFUSEBROKEN
	  )
	{
		u16TmpH = u16TmpH | 0x2000;		// rectifier fault 
	}

	// AC under voltage, turn off AC don't warn protect 
	//ls add pfc fail 20111129
	if((g_u16MdlStatus.bit.PFCOV) || (g_u16CurrUnBal >= TIME20S) || (g_u16MdlStatusExtend.bit.PFCFAIL)
	 ||g_u16UnPlugFlag
	 ||((g_u16MdlStatus.bit.ACOV) && (g_lq10AcVolt.lData >= VAC_HBACK)))
	{
		u16TmpH = u16TmpH | 0x1000;		// rectifier protect
	}

	if(g_u16MdlStatus.bit.FANFAIL)
	{
		u16TmpH = u16TmpH | 0x0800;		// fan fault
	}

	if(g_u16MdlStatus.bit.CALIBRFAIL)
	{
		u16TmpH = u16TmpH | 0x0400;		// E2prom fault
	}

	if(g_u16LimitStatus & POW_LIM_AC)
	{
		u16TmpH = u16TmpH | 0x0200;		// power limit status for AC
	}

	if(g_u16LimitStatus & POW_LIM_TEMP)
	{
		u16TmpH = u16TmpH | 0x0100;		// power limit status for temp
	}

	if(g_u16LimitStatus & POW_LIM)
	{
		u16TmpH = u16TmpH | 0x0080;		// power limit warm
	}

	if(g_u16MdlStatus.bit.OFFSTAT)
	{
		u16TmpH = u16TmpH | 0x0040;		// rectifier onoff state
	}

	if(g_u16MdlCtrl.bit.FANFULL)
	{
		u16TmpH = u16TmpH | 0x0020;		// fan speed
	}

	if(g_u16MdlCtrl.bit.WALKIN)
	{
		u16TmpH = u16TmpH | 0x0010;		// walk-in status
	}

	if(g_u16MdlStatus.bit.ACOV || g_u16MdlCtrl.bit.SHORTRELAY)
	{
		u16TmpH = u16TmpH | 0x0008;		//AC过压/脱离	
	}

	if (g_u16MdlCtrl.bit.IDENTIFY)
	{
		u16TmpH = u16TmpH | 0x0004;		// rectifier identify
	}

	if(g_u16MdlStatus.bit.IUNBALMAJOR || g_u16MdlStatus.bit.IUNBALMINOR)
	{
		u16TmpH = u16TmpH | 0x0002;		// curr unbalance
	}

	if((CanaRegs.CAN_ERRC.bit.REC > 10) || (CanaRegs.CAN_ERRC.bit.TEC > 10))
	{
		u16TmpH = u16TmpH | 0x0001;		// CAN error	
	}

	if(g_lq10OpenTime.lData > 0)
	{
		u16TmpL = u16TmpL | 0x8000;		// start by order 
	}

	if(g_u16CurrSerUnBal >= TIME20S)		// severe current unbalance
	{
		u16TmpL = u16TmpL | 0x0800;
	}

	if(g_u16MdlStatus.bit.IDERR)		// ID overlap
	{
		u16TmpL = u16TmpL | 0x0400;
	}

	// AC over volt
	if(g_u16MdlStatus.bit.ACOV)			
	{
		u16TmpL = u16TmpL | 0x0200;
	}

	if((g_u16MdlStatus.bit.PFCOV) || (g_u16MdlStatusExtend.bit.PFCFAIL))		// PFC fault ls add pfc fail 20111129
	{
		u16TmpL = u16TmpL | 0x0100;
	}


	// current unbalance 
	if(g_u16CurrUnBal >= TIME20S)
	{
		u16TmpL = u16TmpL | 0x0080;
	}

    if(g_u16UnPlugFlag)
    {
        u16TmpL = u16TmpL | 0x0040;
    }

	// fuse broken 20111130
	/*if(g_u16MdlStatusExtend.bit.DCFUSEBROKEN)
	{
		u16TmpL = u16TmpL | 0x0004;		// rectifier fuse is broken
	}
       */

	if(g_u16MdlStatusExtend.bit.DCINPUTSTATUS)
	{
		u16TmpL = u16TmpL | 0x0002; 	// dc input status
	}
	if(g_u16MdlStatusExtend.bit.DcInputAlarmStatus) 
	{
		u16TmpL = u16TmpL | 0x0001; 	// dc input alarm
	}



	tagStatusSd.uintdata[1].id = u16TmpH;
	tagStatusSd.uintdata[0].id = u16TmpL;

	return(tagStatusSd.fd);
}

/*******************************************************************************
*Function name:  vRrpCanDataHandle()
*Description: RRP can data handle
*******************************************************************************/
static void vRrpCanDataHandle(void)
{
	UINT16 i;
	ubitfloat ubitftemp;
	ubitintg g_u16SelfDxValueTmp;

	// reassign address if address overlap
	if( (g_u16SequeOnTimer >= SEQUE_ON_TIME_10S)  
		&& (g_u16MdlStatus.bit.IDERR == 0) )   
	{
		if (g_u16SRCADDR == g_u16MdlAddr)			
		{
			g_u16AddIdentifyFlag = REQUEST_ID;	
			g_u16AddIdentifyNum = 0;
		}        
	}	

	/*the data is other module`s load sharing data*/
	if(g_u16MsgType == 0x01)
	{	
		s_u16OldRecInFlag = SHARE_MODE_REAL_CURR;   //ls 20120202

		if(g_u16SRCADDR <= MAX_NUM)
		{
			//save the module`s  on/off status
			CanComVars._au16MdlOnoff[g_u16SRCADDR] = (g_u16ValueType & 0xF000) >> 12;
           
		    //cal the module`s regulation volt and save
            g_u16ValueType = g_u16ValueType & 0x0fff;
			if (g_u16ValueType >= 0x0800)
		    {
				//negative data handle
				g_u16ValueType = g_u16ValueType | 0xf000;
			}
			//经过前面的算法，通过把无符号16位的最高位写成1，再强制转换成有符号16位来实现负数的传递
		   CanComVars._ai16MdlCurrDelta[g_u16SRCADDR] = (INT16)g_u16ValueType;

			//save the module`s output current
			//if(g_u16ShareLoadMode)
			//#if SHARE_MODE_LOAD_PCT
			
			CanComVars._alq10MdlCurrent[g_u16SRCADDR].lData = g_lq10CanData.lData;
			
			//refresh the Smallest module address,to identify the master            
			if (g_u16SRCADDR <= g_u16AddressSmall)
			{
				g_u16AddressSmall = g_u16SRCADDR;
			}

			//receive other module`s current information,clr the cnt
			g_u16Box0FailTimer = 0;
			g_u16MdlStatusExtend.bit.PARAFLAG = 1;
		}
	}
	
	/*the master send the data that for synchronization between load sharing 
	  and volt regulation*/	
	else if (g_u16MsgType == 0x02)
	{
		//refresh the adjust attribute of load sharing 
		s_u16CurrCtrlFlag = g_u16ValueType;

		//refresh the system average current,for nor derating mixed
		
		//ls change for load PCT 20111128
		//#if SHARE_MODE_LOAD_PCT		
	
		g_lq10MdlAvgCurr.lData = _IQmpy(g_lq10CanData.lData,
		                                g_i32AvgCurrChgFactor);
		
	}	

	/*the data is that the other module`s ID*/
    else if (g_u16MsgType == 0x03)
	{		
		//set address cal flag	
		g_u16RunFlag.bit.ADDRCAL = 1;

		//save the module`s ID
		s_afMdlAddID[MAX_NUM+1].msgaddid[0] = g_ubitfCanData.uintdata[1].id;
		s_afMdlAddID[MAX_NUM+1].msgaddid[1] = g_ubitfCanData.uintdata[0].id;
		s_afMdlAddID[MAX_NUM+1].msgaddid[2] = g_u16ValueType;	            
		
		// address auto-identify
		vAddressCal();					
	}
	
	/*request rectifier ID*/	
	else if (g_u16MsgType == 0x04)
	{
		//clr the ID buffer
		for (i = 0; i < MAX_NUM; i++)			
	   	{
			s_afMdlAddID[i].msgaddid[2]=0;
			s_afMdlAddID[i].msgaddid[1]=0;
			s_afMdlAddID[i].msgaddid[0]=0;
		}

		//clr self ID to 0
		g_u16MdlAddrBuff = 0;
		//g_u16MdlAddr = 0;
		g_u16PowerMixedSystemType=0;  //ls 20120224
		g_u16PowerMixedFlag = 0;  //ls 20120224

		//ls 20120110
		g_u32MasterDxTimer = 0;			// clear		
		g_u16SelfDxCtrl.bit.STATE = 0;		
		g_u16SelfDxValue.bit.ADDRESS = g_u16MdlAddr;		
		g_u16SelfDxCtrl.bit.START = 0;

		s_u16FuseNextTime = 0;	//20130704 LQC

		g_u16MdlStatusExtend.bit.DIAGNOSISTEST = 0; //20121218 LQC
	    g_u16MdlStatusExtend.bit.DIAGNOSISBEGIN =0;

		/*when other module find out ID fault,send the command 0x04,all module 
		  begin to send self id for address atuo-identification */
		if(g_u16AddIdentifyFlag != REQUEST_ID)
		{
			//set the flag to send self id
			g_u16AddIdentifyFlag = SEND_ID;
			g_u16AddIdentifyNum = 1;
		}
	}
	
	//20111130
	// self diagnosis for fuse broken
	else if (g_u16MsgType == 0x05)					
	{	
		g_u16SelfDxValueTmp.bit.ATTR = (g_u16ValueType & 0xff00) >> 8;
		g_u16SelfDxValueTmp.bit.ADDRESS = g_u16ValueType & 0x00ff;

		// data form master mdl, and other mdl is diagnosing
		if (g_u16SelfDxValueTmp.bit.ATTR == 2) 
		{	
			if (g_u16SelfDxValueTmp.bit.ADDRESS < MAX_NUM)	
			{	
				g_u16SelfDxCtrl.bit.START = 1;	
				if (g_u16SelfDxValueTmp.bit.ADDRESS == g_u16MdlAddr)	
				{	
					g_u16SelfDxValue.bit.ATTR = 0x01;	
					g_u16SelfDxCtrl.bit.TXEN = 1;	
				}	
			}	
			
			else	
			{	
				g_u16SelfDxCtrl.bit.START = 0;	
				if (g_u16SelfDxValueTmp.bit.ADDRESS == MAX_NUM)	
				{	
					for (i = 0; i < MAX_NUM; i++)				
			       	{	
						CanComVolVars._au16MdlVoltSelfDiag[i] = 0;	
					}	
				}	
			}	
			g_u16SelfDxTimer.bit.SLAVETIMER = 0;	
		}	
		
		// data from mdl which isn't diagnosing (master or slave mdl)
		else if (g_u16SelfDxValueTmp.bit.ATTR == 0)	
		{	
			CanComVolVars._au16MdlVoltSelfDiag[g_u16SelfDxValueTmp.bit.ADDRESS] 
			= g_ubitfCanData.uintdata[0].id;	
	
			if ((g_u16SelfDxCtrl.bit.INGADDR == g_u16SelfDxValueTmp.bit.ADDRESS) 
				&& g_u16SelfDxCtrl.bit.MASTERMDL)	
			{	
				s_u16FuseNextTime++;			// 20130704 LQC
				if ( s_u16FuseNextTime >= 2)
                {  
                   s_u16FuseNextTime = 0;
				g_u16SelfDxValue.bit.ADDRESS = g_u16SelfDxCtrl.bit.INGADDR + (UINT16)1;	
				}
			}	
		}	

		// a mdl is diagnosing 
		if ((g_u16SelfDxValueTmp.bit.ATTR == 1) && g_u16SelfDxCtrl.bit.MASTERMDL)	
		{	
			//g_u16SelfDxCtrl.bit.NEXTDELAY = 0;	
			g_u16SelfDxCtrlNextTimer = 0;
		}
	}

	/*the data is other module`s load sharing data*/
	if(g_u16MsgType == 0x11)
	{	
		if(g_u16SRCADDR <= MAX_NUM)
		{
			//save the module`s  on/off status
			CanComVars._au16MdlOnoff[g_u16SRCADDR] = (g_u16ValueType & 0xF000) >> 12;
           
		    //cal the module`s regulation volt and save
            g_u16ValueType = g_u16ValueType & 0x0fff;
			if (g_u16ValueType >= 0x0800)
		    {
				//negative data handle
				g_u16ValueType = g_u16ValueType | 0xf000;
			}
			//经过前面的算法，通过把无符号16位的最高位写成1，再强制转换成有符号16位来实现负数的传递
			CanComVars._ai16MdlCurrDelta[g_u16SRCADDR] = (INT16)g_u16ValueType;

			//save the module`s output voltage
			//for new RRP protocol, use g_u16ErrType as output-volt information, 20120224
			CanComVolVars._au16MdlVolt[g_u16SRCADDR] = g_u16ErrType;

			//save the module`s output current
			//#if SHARE_MODE_LOAD_PCT
			
			CanComVars._au16MdlRateCurr[g_u16SRCADDR] = g_ubitfCanData.uintdata[1].id;
			CanComVars._alq10MdlCurrPct[g_u16SRCADDR].lData 
										= (INT32)g_ubitfCanData.uintdata[0].id << 2;
			
			//ls add for new protocol 20120224
			if (CanComVars._au16MdlRateCurr[g_u16SRCADDR] != IDC_RATE_Q7)
			{
				s_u16SysMixStatus = s_u16SysMixStatus | 0x01;
			}

			//refresh the Smallest module address,to identify the master            
			if (g_u16SRCADDR <= g_u16AddressSmall)
			{
				g_u16AddressSmall = g_u16SRCADDR;
			}

			//receive other module`s current information,clr the cnt
			g_u16Box0FailTimer = 0;
			g_u16MdlStatusExtend.bit.PARAFLAG = 1;
			//在位模块计算
			if(g_u16MdlNumber < g_u16SRCADDR)
			{
			    g_u16MdlNumber = g_u16SRCADDR;
			}
			if(g_u16MdlNumber < g_u16MdlAddr)
			{
			    g_u16MdlNumber = g_u16MdlAddr;		
			}
		}
	}
	
	/*the master send the data that for synchronization between load sharing 
	  and volt regulation*/	
	else if (g_u16MsgType == 0x12)
	{
		//ls delete for new protocol 20120224
		//refresh the adjust attribute of load sharing 
		//s_u16NewCurrCtrlFlag = g_u16ValueType;

		//refresh the system average current,for nor derating mixed
		//ls add for new protocol 20120224
		g_u16MdlMixStatus = g_u16MdlMixStatus & 0x0004;	// save trait flag
		s_u16NewCurrCtrlFlag = (g_u16ValueType & 0xF000) >>12;
		g_u16MdlMixStatus = (g_u16ValueType & 0x0F00) | g_u16MdlMixStatus;

		//ls change for load PCT 20111128
		//#if SHARE_MODE_LOAD_PCT		
		
		s_u16SysRateCurr = g_ubitfCanData.uintdata[1].id;
		s_lq10SysAvgPct.lData = (INT32)g_ubitfCanData.uintdata[0].id << 2;
		// transfer to old  add div 100, 20101021
		//ubitftemp.lData = _IQ10mpy(CURR_RATE, s_lq10SysAvgPct.lData);
	    ubitftemp.lData = _IQ10mpy(g_i32CurrRate, s_lq10SysAvgPct.lData);
		g_lq10MdlAvgCurr.lData = _IQ10div(ubitftemp.lData,((INT32)100 << 10));				

	}
			
}

/*******************************************************************************
*Function name:  vRrpProtocol()
*Description: Rrp protocol treatment, address identify, load sharing information
*             etc.
*******************************************************************************/
void vRrpProtocol(void)
{	
	UINT16 i,u16NodeId1LTemp;
	ubitfloat ubitfData;
	ubitfloat g_iq10MdlCurrTemp; //for nor derating mixed
	ubitfloat lq10DataTemp;
	UINT16 u16FlagTemp;			// send curr ctrl，mix flag and different trait flag


	lq10DataTemp.lData = (INT32)0;
	ubitfData.lData = (INT32)0;
	/*host rectifier calculate average current and send load sharing adjust 
	 command,3s for one time*/
	if (g_u16RunFlag.bit.SHARESYN)
	{
		//clr the time flag
		g_u16RunFlag.bit.SHARESYN = 0;
		
		/*cnt for address atuo-identification,after 9s,atuo-identification is end*/
		g_u16AddIdentifyNum = g_u16AddIdentifyNum + 1;
				
		if(g_u16AddIdentifyNum >= 3)
		{
			g_u16AddIdentifyNum = 3;
			g_u16AddIdentifyFlag = NON_IDENTIFY_ID;

			//refresh the new address
			if (g_u16MdlAddr != g_u16MdlAddrBuff)
			{
				g_u16MdlAddr = g_u16MdlAddrBuff;				
			}

		}

		/*the smallest address is master,master cal average current and send 
		  load sharing adjust command*/
		g_u16SelfDxCtrl.bit.MASTERMDL = 0;

		if (g_u16MdlAddr <= g_u16AddressSmall)
		{
			g_u16SelfDxCtrl.bit.MASTERMDL = 1;

			vMasterLoadSharingCal();

			//#if SHARE_MODE_REAL_CURR
			if (s_u16OldRecInFlag == SHARE_MODE_REAL_CURR)
			{
				//master send the information about volt regulation and ave current
				sEncodeCanTxID(RRP,BROAD_CAST,BROAD_CAST_ADDR,DATA_END);
				sEncodeCanTxIQ10Data(0x02,NORMAL,s_u16CurrCtrlFlag,
			                     g_lq10MdlAvgCurr);
				vCanHdSend6(&g_stCanTxData,6);
			}
			else
			{
				//ls add for new protocol 20120224
				
				u16FlagTemp = (s_u16NewCurrCtrlFlag << 12) | (s_u16SysMixStatus << 8);

				lq10DataTemp.uintdata[0].id = (UINT16)(s_lq10SysAvgPct.lData >> 2);
				lq10DataTemp.uintdata[1].id = s_u16SysRateCurr;
				sEncodeCanTxID(RRP,BROAD_CAST,BROAD_CAST_ADDR,DATA_END);
				sEncodeCanTxData(0x12,s_u16SysAvgVolt, u16FlagTemp, lq10DataTemp);
				vCanHdSend6(&g_stCanTxData,6);
			}
					
		}	
		
		/*clr the data buffer*/	
		g_u16AddressSmall = MAX_NUM;
		for (i = 0; i < MAX_NUM; i++)
		{
			CanComVars._alq10MdlCurrent[i].lData = 0;
			CanComVars._ai16MdlCurrDelta[i] = 0;
			CanComVars._au16MdlOnoff[i] = 0x03;
		}

		s_u16SysMixStatus = 0;  //ls 20120224
	}

	/* all rectifiers send BROAD_CAST infoemation, including ask for address 
	   distribution, ID info. current ,400ms for one time*/
	else if (g_u16RunFlag.bit.SHAREDATA)
	{
		//clr the time flag
		g_u16RunFlag.bit.SHAREDATA = 0;

		/*if non identify id, send module load sharing data*/
		if(g_u16AddIdentifyFlag == NON_IDENTIFY_ID)
		{
			//initial the temp variable
			i = 0;

			/* don't sharing if off state, current serious unbalance, self 
			   diagnosing, fuse broken*/ 

			// for current severily unbalance csm 091202 
			if ( (g_u16MdlStatus.bit.OFFSTAT) || (g_u16CurrSerUnBal>= TIME20S) 
			   || (g_u16ActionReady < NORMAL_RUN_STATE) //|| (g_u16MdlCtrl.bit.FUSEB)
			   || (g_u16SelfDxValue.bit.ATTR == 1) || (g_u16MdlStatusExtend.bit.DCFAULT == 1))//20111130

			{
				// rectifier Status is off
				i = 0x01;							
			}

			//ls add for new protocol 20120224
			//if ((g_u16MdlMixStatus & 0x0500) && (g_u16LimitStatus & POW_LIM))
			if ((g_u16MdlMixStatus & 0x0100) && (g_u16LimitStatus & POW_LIM))  //wjy test problem 20120607
			{
				i = i | 0x08;			// rectifier mask unbalance alarm
			}

			if (g_u16MdlMixStatus & 0x04)
			{
				i = i | 0x04;			// rectifiers have different trait
			}

            //paul 20210610
            if (g_u16CurrLimitStatusFlag)
            {
                i = i | 0x02;           // rectifiers in current limit state
            }
			
			//if DC OFF,g_i16CurrDelta is 0 

			// for current severily unbalance csm 091202 
			if ( (g_u16MdlStatus.bit.OFFSTAT) 
			   || (g_u16ActionReady < NORMAL_RUN_STATE) )
			    				
			{
				g_i16CurrDelta = 0;
			}
			
			// over the valid address ,return
			if (g_u16MdlAddr >= MAX_NUM)
			{
				return;				
			}
			
			//refresh self status、volt regulation value and current

			//#if SHARE_MODE_REAL_CURR
			if (s_u16OldRecInFlag == SHARE_MODE_REAL_CURR)
			{
                //CanComVars._au16MdlOnoff[g_u16MdlAddr] = i & 0x01;  //ls 20120224
                CanComVars._au16MdlOnoff[g_u16MdlAddr] = i & 0x03;  //paul

				CanComVars._alq10MdlCurrent[g_u16MdlAddr].lData = g_lq10MdlCurr.lData;			
				CanComVars._ai16MdlCurrDelta[g_u16MdlAddr] = g_i16CurrDelta;
				g_u16ValueType = ((UINT16)(g_i16CurrDelta & 0x0fff)) | (i << 12);	

				//for nor derating mixed
				g_iq10MdlCurrTemp.lData = _IQmpy(g_lq10MdlCurr.lData, 
			                                 g_i32CurrChgFactor);		
			
				/*module send the information about module status、volt regulation 
			  	value and self current*/
				sEncodeCanTxID(RRP,BROAD_CAST,BROAD_CAST_ADDR,DATA_END);
				sEncodeCanTxIQ10Data(0x01,NORMAL,g_u16ValueType,g_iq10MdlCurrTemp);
				vCanHdSend6(&g_stCanTxData,6);
			}
			else
			{
				CanComVolVars._au16MdlVolt[g_u16MdlAddr] = 4 * (UINT16)(g_lq10MdlVolt.lData >> 10);

				CanComVars._au16MdlOnoff[g_u16MdlAddr] = i;
			
				lq10DataTemp.lData = (INT32)100 * g_lq10MdlCurr.lData;
				CanComVars._alq10MdlCurrPct[g_u16MdlAddr].lData = \
									_IQ10div(lq10DataTemp.lData,g_i32CurrRate);
				CanComVars._ai16MdlCurrDelta[g_u16MdlAddr] = g_i16CurrDelta;
				g_u16ValueType = ((UINT16)(g_i16CurrDelta & 0x0fff)) | (i << 12);

				// change to percent 
				lq10DataTemp.uintdata[0].id = \
						(UINT16)(CanComVars._alq10MdlCurrPct[g_u16MdlAddr].lData >> 2);
				lq10DataTemp.uintdata[1].id = IDC_RATE_Q7;
				sEncodeCanTxID(RRP,BROAD_CAST,BROAD_CAST_ADDR,DATA_END);
				sEncodeCanTxData(0x11,CanComVolVars._au16MdlVolt[g_u16MdlAddr],g_u16ValueType,lq10DataTemp);
				vCanHdSend6(&g_stCanTxData,6);
			}				

		}
		
		/* if ID request,send ID request command*/					
		else if(g_u16AddIdentifyFlag == REQUEST_ID)
		{
			//send request ID command about 3S,then to send self ID
			if(g_u16AddIdentifyNum)
			{
				g_u16AddIdentifyFlag = SEND_ID;
			}
		    
		    //send request ID command
			ubitfData.lData = 0x00;
			sEncodeCanTxID(RRP,BROAD_CAST,BROAD_CAST_ADDR,DATA_END);
			sEncodeCanTxData(0x04,NORMAL,0x5A01,ubitfData);
			vCanHdSend6(&g_stCanTxData,6);
			
			//clr the ID buffer
			for (i = 0; i < MAX_NUM; i++)	
		 	{
		 		 s_afMdlAddID[i].msgaddid[2] = 0;
		 		 s_afMdlAddID[i].msgaddid[1] = 0;
				 s_afMdlAddID[i].msgaddid[0] = 0;
			}
			
			//clr self address to 0
			g_u16MdlAddrBuff = 0;
			//ls add 20120224
			g_u16PowerMixedSystemType=0;			
			g_u16PowerMixedFlag = 0;
			
			s_u16SysMixStatus = 0;
			g_u16MdlMixStatus = 0;
		}
		
		/*if ID send,send self ID*/
		else if (g_u16AddIdentifyFlag == SEND_ID)   
		{
			//send self ID about 6S  (uNodeId1L&0x00ff) |0x5a00; 
            ubitfData.uintdata[0].id = g_u16NodeId0H;
			ubitfData.uintdata[1].id = g_u16NodeId0L;
			
			u16NodeId1LTemp=(g_u16NodeId1L & 0x00ff) | 0x5a00;
			sEncodeCanTxID(RRP,BROAD_CAST,BROAD_CAST_ADDR,DATA_END);
			sEncodeCanTxData(0x03,NORMAL,u16NodeId1LTemp,ubitfData);
			vCanHdSend6(&g_stCanTxData,6);
									
		}	
	}

	//20111130
	else if ((g_u16SelfDxCtrl.bit.TXEN) && (g_u16MdlStatusExtend.bit.PARAFLAG)
			&& ( s_u16FuseNextTime != 1))
	//else if ((g_u16SelfDxSend) && ((!g_u16MasterMdl)
	//		||(g_u16MasterMdl && (g_u16MdlStatusExtend.bit.PARAFLAG))))	
	{
		g_u16SelfDxCtrl.bit.TXEN = 0;

		if (g_u16SelfDxCtrl.bit.MASTERMDL)
		{
			// after power on or auto-addresscal
			if (g_u16SelfDxCtrl.bit.STATE == 1)				
			{	
				g_u16SelfDxCtrl.bit.STATE = 2;
				g_u16SelfDxValue.bit.ATTR = 1;			
				g_u16SelfDxValue.bit.ADDRESS = g_u16MdlAddr;					
			}	
			
			// every 23 hour
			else if(g_u16SelfDxCtrl.bit.STATE == 4)	
			{	
				g_u16SelfDxCtrl.bit.STATE = 3;		
				g_u16SelfDxValue.bit.ATTR = 1;			
				g_u16SelfDxValue.bit.ADDRESS = g_u16MdlAddr;			
			}	

			// start other rectifiers self diagnosis
			else
			{
				g_u16SelfDxValue.bit.ATTR = 2;		
			}
			
			g_u16SelfDxCtrl.bit.INGADDR = g_u16SelfDxValue.bit.ADDRESS;

		}
		else
		{
			g_u16SelfDxValue.bit.ADDRESS = g_u16MdlAddr;
		}

		if ((g_u16FuseBroken > 0) && (g_u16FuseBroken < 3))
		{
			ubitfData.uintdata[0].id = 0x8000|((UINT16)(g_lq10MdlVolt.lData >> 2));	
		}
		else
		{
			ubitfData.uintdata[0].id = (UINT16)(g_lq10MdlVolt.lData >> 2);
		}
		
		ubitfData.uintdata[1].id = (UINT16)(g_lq10SetVolt.lData >> 2);

		/*module send the information about self diagnosis*/	
		sEncodeCanTxID(RRP,BROAD_CAST,BROAD_CAST_ADDR,DATA_END);	
		sEncodeCanTxData(0x05,NORMAL,g_u16SelfDxValue.all,ubitfData);	
		vCanHdSend6(&g_stCanTxData,6);
	}
}
/*******************************************************************************
*Function name:  vRmpProtocol()
*Description: Rmp protocol treatment, commincation with monitor 
*******************************************************************************/
static void vRmpProtocol(void)
{
	UINT16 u16CodeType;
	UINT16  * pu16setadd;
	UINT16 u16TmpData;
	ubitfloat  lq12VoltSampSysbTemp;
	ubitfloat  lq12VoltConSysbTemp;

    //paul-20220228
    UINT16  u16NumberTempH;
    UINT16  u16NumberTempL;
    UINT16  u16NumberTempHTmp;
    UINT16  u16SitNumberTx;

	//define g_u16MsgType & g_u16ErrType
	g_u16MsgType |= 0x40; 
	g_u16ErrType = 0xf0;
	u16CodeType = 0x00;//这个临时变量需要初始化为0，后面有在等号右面的使用
	
    //define can data
	switch (g_u16ValueType)		
	{
		
		case 0x01:	// get rectifier Volt 			
			g_ubitfCanData.fd =  _IQ10toF(g_lq10MdlVolt.lData);			
			break;

		case 0x02:	// get rectifier real Curr 
			//g_ubitfCanData.fd =  _IQ10toF(g_lq10MdlCurr.lData);
		    g_ubitfCanData.fd =  _IQ10toF(g_lq10MdlCurrDisp3.lData);
			break;

		case 0x03:	// get rectifier Curr Limit 
			if (g_lq10MdlLimitDisp.lData >= 1239)
			{
				g_ubitfCanData.fd = 1.21;
			}
			else
			{
				g_ubitfCanData.fd = _IQ10toF(g_lq10MdlLimitDisp.lData);
			}
			break;

        case 0x04:	// get rectifier U1 temperature 
			g_ubitfCanData.fd =  _IQ10toF(g_lq10TempAmbiDisp.lData);
			break;	

		case 0x05:	// get rectifier aca volt 
			g_ubitfCanData.fd = _IQ10toF(g_lq10AcVolt.lData);
			break;

		case 0x06:	// get rectifier volt up 
			g_ubitfCanData.fd =  _IQ10toF(g_lq10MdlVoltUp.lData);
			break;

		case 0x07:	// get rectifier curr for display  
			g_ubitfCanData.fd =  _IQ10toF(g_lq10MdlCurrDisp2.lData);
			break;

		case 0x08:	// get rectifier pfc volt 
			g_ubitfCanData.fd = _IQ10toF(g_lq10PfcVoltDisp.lData);
			break;

		case 0x09:	// get rectifier PFC temperature 
			g_ubitfCanData.fd =  _IQ10toF(g_lq10MdlTempUp.lData);
			//g_ubitfCanData.fd =  _IQ10toF(g_lq10TempPfcDisp.lData);
			break;

		case 0x0b:	// get rectifier M1 temperature 
			g_ubitfCanData.fd =  _IQ10toF(g_lq10TempDcDisp.lData);
			break;
  
		case 0x14:	// get rectifier power limit
			g_ubitfCanData.fd =  _IQ10toF(g_lq10SetPowerWant.lData);
			break;
        
		case 0x16:  //set Estop function
            if(g_ubitfCanData.lData == 0x00005A5A)
			{
				g_u16MdlCtrl.bit.EStopFlag = 1;
			}
			else if(g_ubitfCanData.lData == 0xFFFF0000)
			{
				g_u16MdlCtrl.bit.EStopFlag = 0;
			}
			break;

		case 0x18:	// set rectifier default power limit			
			if ( (g_lq10CanData.lData <= IQ10_POWERLlMlTMAX) 
			   && (g_lq10CanData.lData >= 10) )
			{
				if (labs(g_lq10CanData.lData - g_lq10MdlPowerFt.lData) >= 2)
				{
					g_lq10MdlPowerFt.lData = g_lq10CanData.lData + 1;
					g_u16EpromWr.bit.POWERDFT = 1;
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;
			
		case 0x19:	// set rectifier default current limit
		/*			
			if( (g_lq10CanData.lData <= IQ10_CURRLlMlTMAX) 
			  && (g_lq10CanData.lData >= 10) )
        */
            if( (g_lq10CanData.lData <= g_i32CurrLimtMaxExtra)
			  && (g_lq10CanData.lData >= 10) )			  
			{
				if (labs(g_lq10CanData.lData - g_lq10MdlCurrFt.lData) >= 2)
				{
					g_lq10MdlCurrFt.lData = g_lq10CanData.lData + 1;
					g_u16EpromWr.bit.CURRRDFT = 1;
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x1a:	// set rectifier default AC current limit,1~30A
			if ( (g_lq10CanData.lData < IQ10_IAC_UP) 
			   && (g_lq10CanData.lData >= IQ10_IAC_DN) ) 
			{
				if (labs(g_lq10CanData.lData - g_lq10AcCurrFt.lData) >= 10)
				{
					g_lq10AcCurrFt.lData = g_lq10CanData.lData + 1;
					g_u16EpromWr.bit.ACCURRRDFT = 1;
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x20:	// set rectifier power limit 
			if ((g_lq10CanData.lData <= IQ10_POWERLlMlTMAX) && 
			    (g_lq10CanData.lData >= 10))
		  	{
				g_lq10SetPower.lData = g_lq10CanData.lData + 1;
			}
			else if ((g_lq10CanData.lData > IQ10_POWERLlMlTMAX) && 
			         (g_lq10CanData.lData <= 2048))
			{				
				g_lq10SetPower.lData = IQ10_POWERLlMlTMAX;		//  1.22
			}	
			else if ( (g_lq10CanData.lData >= 0) && (g_lq10CanData.lData < 10))				
			{
				g_lq10SetPower.lData = 10;		//0.01
			}	
			else
			{
				g_u16ErrType = COMM_FAIL;
			}	
			break;

		case 0x21:	// set rectifier volt 
			if (s_u16ForbidConnect)
			{
				g_lq10SetVolt.lData = g_lq10CanData.lData;
			}
			else
			{
				if ( (g_lq10CanData.lData <= (g_lq10MdlVoltUp.lData - VDC_0V5))
				   && (g_lq10CanData.lData >= VDC_41V) )
		  		{
					g_lq10SetVolt.lData = g_lq10CanData.lData + 1;

					if(g_u16MdlStatusExtend.bit.DIAGNOSISTEST ==1)
					{
					    g_u16MdlStatusExtend.bit.DIAGNOSISBEGIN =1;
					}
				}
				else
				{
					g_u16ErrType = COMM_FAIL;
				}
			}
			break;

		case 0x22:	// set rectifier limit ,1.4 for debug
			if ( (g_lq10CanData.lData <= g_i32CurrLimtMax) &&
			     (g_lq10CanData.lData >= 10) )
		  	{
				g_lq10SetLimit.lData = g_lq10CanData.lData + 1;
			}
			else if ( (g_lq10CanData.lData > g_i32CurrLimtMax) &&
			          (g_lq10CanData.lData <= 2048) )
			{				
				g_lq10SetLimit.lData = g_i32CurrLimtMax;		//  1.22
			}	
			else if ( (g_lq10CanData.lData >= 0) && (g_lq10CanData.lData < 10))				
			{
				g_lq10SetLimit.lData = 10;		//0.01
			}	
			else
			{
				g_u16ErrType = COMM_FAIL;
			}	
			break;

		case 0x23:	// set rectifier volt up 
			if (s_u16ForbidConnect)
			{
				g_lq10MdlVoltUp.lData = g_lq10CanData.lData;
			}
			else
			{
				if( (g_lq10CanData.lData <= VDCUP_SET_UP) 
				    && (g_lq10CanData.lData >= VDCUP_SET_DN)
					&& (g_lq10CanData.lData >= g_lq10SetVolt.lData + VDC_0V5)
					&& (g_lq10CanData.lData >= g_lq10MdlVoltFt.lData + VDC_0V5))
			  	{
				//	if (labs(g_lq10CanData.lData - g_lq10MdlVoltUp.lData) >= 10)

                //1024对应1V，0.01V对应10.24，0.24会舍去。
                //导致0.01V步进设置时下行能满足执行条件，上行差1cnt不能满足条件。
					if (labs(g_lq10CanData.lData - g_lq10MdlVoltUp.lData) >= 9)
					{
						g_lq10MdlVoltUp.lData = g_lq10CanData.lData + 1;
						g_u16EpromWr.bit.VOLTUP = 1;
					}
				}
				/*//设置56V以下过压点认为是56V  //如果这么设置会在监控轮寻模块时把过压点调成56V
				else if(g_lq10CanData.lData < VDCUP_SET_DN)
				{
					g_lq10MdlVoltUp.lData = VDCUP_SET_DN;
					g_u16EpromWr.bit.VOLTUP = 1;
				}
                 */
				else
				{
					g_u16ErrType = COMM_FAIL;
				}	
			}
			break;

		case 0x24:	//set rectifier default float volt,for debug,41~58.5V, note	
			if ((g_lq10CanData.lData <= g_lq10MdlVoltUp.lData - VDC_0V5) 
			     && (g_lq10CanData.lData >= VDC_SET_DN))	
	  		{
	  		     //if (labs(g_lq10CanData.lData - g_lq10MdlVoltFt.lData) >= 10)
			    //1024对应1V，0.01V对应10.24，0.24会舍去。
			    //导致0.01V步进设置时下行能满足执行条件，上行差1cnt不能满足条件。
				if (labs(g_lq10CanData.lData - g_lq10MdlVoltFt.lData) >= 9)
				{
					g_lq10MdlVoltFt.lData = g_lq10CanData.lData + 1;
					g_u16EpromWr.bit.VDCDFT = 1;
	  			}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x25:	// set rectifier temp up 
			if ( (g_lq10CanData.lData <= TEMPUP_SET_UP) 
			   && (g_lq10CanData.lData >= TEMPUP_SET_DN) )
		  	{
	  			if (g_lq10CanData.lData != g_lq10MdlTempUp.lData)
	  			{
					u16CodeType \
					    = ucWriteFloatDataThree(TEMP_HIGH_ADDR,g_lq10CanData);

					if (u16CodeType == RECOK)
					{
						g_lq10MdlTempUp.lData = g_lq10CanData.lData;
					}
	  			}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;						 
			}
			break;

		case 0x26:	// set rectifier avg curr adjust volt delta 
			if ( (g_lq10CanData.lData > 0) 
			   && (g_lq10CanData.lData < ((INT32)255<<10)) )
			{
				g_u16VoltAdjDelta = (UINT16)(g_lq10CanData.lData >>10);
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x27: 	// set rectifier pfc out volt ,200 and 500 for debug
			if ( (g_lq10CanData.lData <= ((INT32)500<<10)) 
			    && (g_lq10CanData.lData >= ((INT32)200<<10)) )
			{
				g_lq10SetPfcVolt.lData = g_lq10CanData.lData ;
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x28:	// set rectifier overvolt reon time ,unit:s
			if ((g_lq10CanData.lData <= IQ10TIME300S) && 
			    (g_lq10CanData.lData >= IQ10TIME50S))
		  	{
	  			if (g_lq10CanData.lData != g_lq10ReonTime.lData)
	  			{
					g_lq10ReonTime.lData = g_lq10CanData.lData;
					g_u16EpromWr.bit.REONTIME = 1;
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;
       	case 0x29:	// set rectifier with load softstart time, unit:10ms
			if ((g_lq10CanData.lData <= WALKTIME200S) && 
			    (g_lq10CanData.lData >= WALKTIME8S))
			{				
				if (g_lq10CanData.lData != g_lq10WalkInTime.lData)
	  				{
						g_lq10WalkInTime.lData = g_lq10CanData.lData;
						g_u16EpromWr.bit.WALKTIME = 1;
			  		}	  			
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x2A:	// set rectifier start time one by one ,unit:s
			if ( (g_lq10CanData.lData <= IQ10TIME10S) 
			   && (g_lq10CanData.lData >= 0) )
		  	{
		  		if (g_lq10CanData.lData != g_lq10OpenTime.lData)
	  			{
					g_lq10OpenTime.lData = g_lq10CanData.lData;
					g_u16EpromWr.bit.ORDERTIME = 1;
		  		}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x2F:	// set moudle derating or not in mixed system 
		 	//设置混插时是否需要降额运行，不需要保存的EPROM	  
		  	//byte2-bit0: 1: not derating,  0:derating
			g_u16MdlCtrl.bit.NoDeratingFlag = g_ubitfCanData.uintdata[1].id & 0x01;

			//设置输入低电压限功率的限功率模式，默认为0，模式A，即176V以下线性降额
			//设置输入低电压限功率的限功率模式，1，模式B，即176V～132线性降额，132～90恒定
			//byte2:bit3~bit2:
			//01: mode1;	10: mode2,  //00,11无效		
			if(((g_ubitfCanData.uintdata[1].id & 0x0C) != 0x00) && ((g_ubitfCanData.uintdata[1].id & 0x0C) != 0x0C))
			{
				if((g_ubitfCanData.uintdata[1].id & 0x0C) == 0x04)
				{
					u16TmpData = 0;           //默认为0，模式A，即176V以下线性降额
				}
				else if((g_ubitfCanData.uintdata[1].id & 0x0C) == 0x08)
				{
					u16TmpData = 1;          //1，模式B，即176V～132线性降额，132～90恒定
				}
				
				if(g_u16MdlCtrlExtend.bit.ACLIMMODE != u16TmpData)
				{
					g_u16MdlCtrlExtend.bit.ACLIMMODE = u16TmpData;
					g_u16EpromWr.bit.MDLCTRLEXTEND = 1;
				}
			}			
			//设置如果检测到输入电压为直流时是否告警，默认为1，不告警; 0，告警,关机。
			//byte2:bit5~bit4:
			//01:DC input alarm;  10: no DC input alarm
			if(((g_ubitfCanData.uintdata[1].id & 0x30) != 0x00)&&((g_ubitfCanData.uintdata[1].id & 0x30) != 0x30))
			{
				if((g_ubitfCanData.uintdata[1].id & 0x30) == 0x10)
				{
					u16TmpData = 0;               //告警关机
				}
				else if((g_ubitfCanData.uintdata[1].id & 0x30) == 0x20)
				{
					u16TmpData = 1;              //不告警;
				}
				
				if(g_u16MdlCtrlExtend.bit.DCINPUTALARM != u16TmpData)
				{
					g_u16MdlCtrlExtend.bit.DCINPUTALARM = u16TmpData;
					g_u16EpromWr.bit.MDLCTRLEXTEND = 1;
				}
				
			}	 
		  
			break;

		case 0x30:	// set rectifier on/off 
			if (g_ubitfCanData.uintdata[1].id == 1)			 
			{
				// rectifier off
				g_u16MdlCtrl.bit.OFFCTRL = 1;
			}		
				// rectifier on if no fault 	 
			else if ( !(g_u16MdlStatus.bit.HVSDLOCK || g_u16MdlStatus.bit.PFCOV
			           || g_u16MdlStatus.bit.PFCUV || g_u16MdlStatus.bit.ACOV
			           || g_u16MdlStatus.bit.ACUV || g_u16MdlStatus.bit.FANFAIL
					   || g_u16MdlStatus.bit.AMBIOT || g_u16MdlStatus.bit.RECTOT
					   || g_u16MdlStatus.bit.DCOV || g_u16MdlStatusExtend.bit.HWHVSD
					   || g_u16MdlStatusExtend.bit.PFCOT || g_u16SafetyShortStatus )  )
			{
				// rectifier on if no fault
				g_u16MdlCtrl.bit.OFFCTRL = 0;
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

	case 0x31:	// HVSD reset 
			if ( (g_ubitfCanData.uintdata[1].id == 1) 
			   && (g_u16MdlStatus.bit.HVSDLOCK || g_u16SafetyShortStatus) ) 
			{
				g_u16MdlCtrl.bit.HVSDRESET = 1; //转到HVSDRESET程序处理
               /*
				//HVSD reset 
				g_u16CloseTime = 0;			//by ls 20111130
				g_u16CloseTimeHW=0;  		//by ls 20111030

				g_u16OverVoltStatus = DC_OV_STATE_NORMAL;
				g_u16HWOverVoltStatus = DC_OV_STATE_NORMAL;
				g_u16MdlStatus.bit.HVSDLOCK = 0;
				g_u16MdlStatus.bit.DCOV = 0;
				g_u16MdlStatusExtend.bit.HWHVSD=0;
				g_u16HVSDFistFlag = 0;			//by ls 20111130            	
				g_u16HVSDDebug = 0;			//by ls 20111130
  			
				g_u16SafetyShortStatus = DC_SAFE_SHORT_STATUS_NORMAL;
				g_u16DcRestartFailTimes = 0;
			  //	g_u16HVSDDebug = 1; //调试中防止模块损坏
              */

			}	
			else if  (g_ubitfCanData.uintdata[1].id == 0)   	 
			{
				// rectifier normal;		
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;
		case 0x32:	// set rectifier walk-in 
			if (g_ubitfCanData.uintdata[1].id == 1)			 
			{
				// walk-in enable
				if (!g_u16MdlCtrl.bit.WALKIN)
				{
					g_u16MdlCtrl.bit.WALKIN = 1;
					g_u16EpromWr.bit.MDLCTRL = 1;
				}
			}
			else if (g_ubitfCanData.uintdata[1].id == 0)		   
			{
				// rectifier normal, walk-in disable
				if (g_u16MdlCtrl.bit.WALKIN)
				{
					g_u16MdlCtrl.bit.WALKIN = 0;
					g_u16EpromWr.bit.MDLCTRL = 1;			
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x33:	// set FAN  full-speed or not 
			if (g_ubitfCanData.uintdata[1].id == 1)		 
			{
				// FAN full-speed
				if (!g_u16MdlCtrl.bit.FANFULL)
				{
					g_u16MdlCtrl.bit.FANFULL = 1;
					g_u16EpromWr.bit.MDLCTRL = 1;
				}
			}
			else if (g_ubitfCanData.uintdata[1].id == 0)		 
			{
				// no  full-speed
				if (g_u16MdlCtrl.bit.FANFULL)
				{
					g_u16MdlCtrl.bit.FANFULL = 0;
					g_u16EpromWr.bit.MDLCTRL = 1;
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x34:	// set rectifier green-led for com twinkle or not 
					//1: green-led for com twinkle,0: no twinkle
				g_u16MdlCtrl.bit.IDENTIFY = g_ubitfCanData.uintdata[1].id & 0x01;
			break;
	
		case 0x35:	// set rectifier relay action for over voltage 
				    //1: relay break away 0: relay resume
				g_u16MdlCtrl.bit.SHORTRELAY = g_ubitfCanData.uintdata[1].id & 0x01;
			break;

		case 0x36:	// set rectifier Ecan initialization
			if (g_ubitfCanData.uintdata[1].id == 1)			
			{
				// Ecan initialization
				//InitECana();
				//InitCanaPointer();  //ls 20111128
			    InitCanaPointer();
			    InitCanaIDPointer();
			}
			else if  (g_ubitfCanData.uintdata[1].id == 0)   	 
			{
				// no action;		
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x37:	// set rectifier operation mode 
				// 1:normal mode,0:adjust mode
				g_u16PermitAdjust = g_ubitfCanData.uintdata[1].id & 0x01;
			break;
			
		case 0x39:	// select rectifier action mode for over voltage, no use
				if (g_ubitfCanData.uintdata[1].id == 1)	// HVSD the second time
			{
				if (g_u16MdlCtrl.bit.ONCELOCK == 1)
				{
					g_u16MdlCtrl.bit.ONCELOCK = 0;
					g_u16EpromWr.bit.MDLCTRL = 1;			
				}			
			}
			else if (g_ubitfCanData.uintdata[1].id == 0)	// HVSD the first time
			{
				if (g_u16MdlCtrl.bit.ONCELOCK == 0)
				{
					g_u16MdlCtrl.bit.ONCELOCK = 1;
					g_u16EpromWr.bit.MDLCTRL = 1;			
				}
			}
			else
			{
				u16CodeType = COMM_FAIL;
			}
			break;		


		case 0x3A:	// set weather rectifier permit over voltage 
			if (g_ubitfCanData.uintdata[1].id == 1)			
			{
				// permit 
				if ((IsrVars._u16PermitOverAc & FORBID_ACOVER) == 1)	
				{
					g_u16MdlCtrl.bit.ALLOWACOV = 1;
					g_u16EpromWr.bit.MDLCTRL = 1;
					g_u1660sTimer = 0;	
					IsrVars._u16PermitOverAc = PERMIT_ACOVER;
				}
			}	
			else if (g_ubitfCanData.uintdata[1].id == 0)		 
			{
				if ( (IsrVars._u16PermitOverAc & FORBID_ACOVER) == 0)
				{
				 // forbid 
					g_u16MdlCtrl.bit.ALLOWACOV = 0;
					g_u16EpromWr.bit.MDLCTRL = 1;
					IsrVars._u16PermitOverAc = FORBID_ACOVER;
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;	

		case 0x3C:	// set weather address re-identify 
			if (g_ubitfCanData.uintdata[1].id == 1)			
			{
				// re-identify 
				g_u16AddIdentifyNum = 0;			
				g_u16AddIdentifyFlag = REQUEST_ID;	
			}	
			else if (g_ubitfCanData.uintdata[1].id == 0)		
			{
				// no action;
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;	

			case 0x3E:	// set rectifier output over volt correlative
				// 1:forbid,0:permit
				s_u16ForbidConnect = g_ubitfCanData.uintdata[1].id & 0x01;
			break;


		case 0x40:	// get rectifier warm and status 
			 g_ubitfCanData.fd = fStatusTrans();
			break;

		case 0x50:	// read rectifier delta volt adjust for load sharing
			g_ubitfCanData.uintdata[1].id = (UINT16)g_i16CurrDelta;
			g_ubitfCanData.uintdata[0].id = 0;
			break;

		case 0x52:	// read the first maintain data
			g_ubitfCanData.uintdata[1].id = g_u16MaintainData0H;
			g_ubitfCanData.uintdata[0].id = g_u16MaintainData0L;
			break;

		case 0x53:	// read the second maintain data
			g_ubitfCanData.uintdata[1].id = g_u16MaintainData1H;
			g_ubitfCanData.uintdata[0].id = g_u16MaintainData1L;
			break;

		case 0x54:	// read rectifier SearialNo low
			g_ubitfCanData.uintdata[1].id = g_u16NodeId0H_TO_CONTROLLER;//g_u16NodeId0H;
			g_ubitfCanData.uintdata[0].id = g_u16NodeId0L;
			break;
		case 0x55:	// read rectifier SearialNo high
			g_ubitfCanData.uintdata[1].id = g_u16NodeId1H;
			g_ubitfCanData.uintdata[0].id = g_u16NodeId1L_TO_CONTROLLER;
			break;

		case 0x56:	// read rectifier hw/sw version number
			g_ubitfCanData.uintdata[1].id = g_u16VersionNoHw;
			g_ubitfCanData.uintdata[0].id = g_u16VersionNoSw;
			break;


		case 0x58:	// read the whole time of module running (hour)
			g_ubitfCanData.fd = _IQ10toF(g_lq10RunTime.lData);
			break;

		case 0x59:	// read the repair times of module(times) 
			g_ubitfCanData.fd = _IQ10toF(g_lq10MaintainTimes.lData);
			break;

		case 0x5f:	// read rectifier over voltage protection mode(1 or 4 times)
			if (g_u16MdlCtrl.bit.ONCELOCK)						
			{
				g_ubitfCanData.fd = 0.0;
			}
			else
			{
				g_ubitfCanData.fd = 1.0;
			}	
			break;


		// g_u16MsgType 0x60~0x80: rectifier calibration
		case 0x60:	// set volt samp correct sysa 
			s_u16MdlCalibra = 0;
			s_lq10SetSysTmp.lData = g_lq12CanData.lData;

			g_lq12CanData.lData\
			    = _IQ12mpy(g_lq12VoltSampSysa.lData,g_lq12CanData.lData);
			    						 

			if((g_lq12CanData.lData >= IQ12_VDC_SAM_SYSA_LIMDN ) 
				&& (g_lq12CanData.lData <= IQ12_VDC_SAM_SYSA_LIMUP))
			{
				s_lq10AdjTmp.lData = g_lq12CanData.lData;
				s_u16MdlCalibra = 1;
			}

			break;

		case 0x61:	// set volt samp correct sysb 
			g_lq12CanData.lData \
			    = _IQ12mpy(g_lq12VoltSampSysb.lData,s_lq10SetSysTmp.lData)\
			     +  g_lq12CanData.lData;

			if( (g_lq12CanData.lData >=  IQ12_VDC_SAM_SYSB_LIMDN)
			    && (g_lq12CanData.lData <= IQ12_VDC_SAM_SYSB_LIMUP) && s_u16MdlCalibra)
			{
				u16CodeType \
				    = ucWriteFloatDataThree(VDC_SAM_SYSA_ADDR,s_lq10AdjTmp);

				u16CodeType = u16CodeType \
				              | ucWriteFloatDataThree(VDC_SAM_SYSB_ADDR,
				                                      g_lq12CanData);

				if (u16CodeType == RECOK)
				{
					g_lq12VoltSampSysa.lData = s_lq10AdjTmp.lData;
					g_lq12VoltSampSysb.lData = g_lq12CanData.lData;
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x62:	// set curr samp correct sysa 
			s_u16MdlCalibra = 0;
			s_lq10SetSysTmp.lData = g_lq10CanData.lData;

			g_lq10CanData.lData = _IQ10mpy(g_lq10CurrSampSysa.lData,
			                               g_lq10CanData.lData);
			                               	
			if((g_lq10CanData.lData >= IQ10_IDC_SAM_SYSA_LIMDN ) 
				&& (g_lq10CanData.lData <= IQ10_IDC_SAM_SYSA_LIMUP))
			{
				s_lq10AdjTmp.lData = g_lq10CanData.lData;
				s_u16MdlCalibra = 1;
			}						 
			break;

		case 0x63:	// set curr samp correct sysb	
			g_lq10CanData.lData = _IQ10mpy(g_lq10CurrSampSysb.lData,
			                               s_lq10SetSysTmp.lData) 
			                     + g_lq10CanData.lData;


			if((g_lq10CanData.lData >=  IQ10_IDC_SAM_SYSB_LIMDN) 
			   && (g_lq10CanData.lData <= IQ10_IDC_SAM_SYSB_LIMUP) 
			   && (s_u16MdlCalibra) )
			{
				u16CodeType = ucWriteFloatDataThree(IDC_SAM_SYSA_ADDR,
				                                    s_lq10AdjTmp);

				u16CodeType = u16CodeType  
				              | ucWriteFloatDataThree(IDC_SAM_SYSB_ADDR,
				                                      g_lq10CanData);

				if (u16CodeType == RECOK)
				{
					g_lq10CurrSampSysa.lData = s_lq10AdjTmp.lData;
					g_lq10CurrSampSysb.lData = g_lq10CanData.lData;
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x64:	// set volt ctrl correct sysa 
			s_u16MdlCalibra = 0;
			s_lq10SetSysTmp.lData = g_lq12CanData.lData;

			g_lq12CanData.lData = _IQ12mpy(g_lq12VoltConSysa.lData,
			                               g_lq12CanData.lData);
			                               	

			if((g_lq12CanData.lData >= IQ12_VDC_CON_SYSA_LIMDN ) 
				&& (g_lq12CanData.lData <= IQ12_VDC_CON_SYSA_LIMUP))
			{
				s_lq10AdjTmp.lData = g_lq12CanData.lData;
				s_u16MdlCalibra = 1;
			}
			break;

		case 0x65:	// set volt ctrl correct sysb
			/*g_lq12CanData.lData = _IQ12mpy(g_lq12VoltConSysb.lData,
			                               s_lq10SetSysTmp.lData) 
			                      + g_lq12CanData.lData;*/


		    g_lq12CanData.lData = _IQ12mpy(g_lq12VoltConSysa.lData,
		                                   g_lq12CanData.lData)
		                                      + g_lq12VoltConSysb.lData;


			if( (g_lq12CanData.lData >=  IQ12_VDC_CON_SYSB_LIMDN )  
			   && (g_lq12CanData.lData <= IQ12_VDC_CON_SYSB_LIMUP) 
			   && (s_u16MdlCalibra) )
			{
				u16CodeType = ucWriteFloatDataThree(VDC_CON_SYSA_ADDR,\
				                                    s_lq10AdjTmp);

				u16CodeType = u16CodeType 
				              | ucWriteFloatDataThree(VDC_CON_SYSB_ADDR,\
				                                      g_lq12CanData);

				if (u16CodeType == RECOK)
				{
					g_lq12VoltConSysa.lData = s_lq10AdjTmp.lData;
					g_lq12VoltConSysb.lData = g_lq12CanData.lData;
				}	
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;
			
		case 0x66:	// set curr ctrl correct sysa 
			s_u16MdlCalibra = 0;
			s_lq10SetSysTmp.lData = g_lq10CanData.lData;

			g_lq10CanData.lData = _IQ10mpy(g_lq10CurrConSysa.lData,
			                               g_lq10CanData.lData);
			                               	

			if((g_lq10CanData.lData >= IQ10_IDC_CON_SYSA_LIMDN ) 
				&& (g_lq10CanData.lData <= IQ10_IDC_CON_SYSA_LIMUP))
			{
				s_lq10AdjTmp.lData = g_lq10CanData.lData;
				s_u16MdlCalibra = 1;
			}
			break;

		case 0x67:	// set curr ctrl correct sysb
			g_lq10CanData.lData = _IQ10mpy(g_lq10CurrConSysb.lData,
			                               s_lq10SetSysTmp.lData)
			                      + g_lq10CanData.lData;


			if( (g_lq10CanData.lData >=  IQ10_IDC_CON_SYSB_LIMDN) 
			    && (g_lq10CanData.lData <= IQ10_IDC_CON_SYSB_LIMUP)
			    && (s_u16MdlCalibra) )
			{
				u16CodeType = ucWriteFloatDataThree(IDC_CON_SYSA_ADDR,
				                                    s_lq10AdjTmp);

				u16CodeType = u16CodeType\
				              | ucWriteFloatDataThree(IDC_CON_SYSB_ADDR,
				                                      g_lq10CanData);
				if (u16CodeType == RECOK)
				{
					g_lq10CurrConSysa.lData = s_lq10AdjTmp.lData;
					g_lq10CurrConSysb.lData = g_lq10CanData.lData;
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		
		case 0x68:	// set Vaca samp correct sysa 
			s_u16MdlCalibra = 0;
			s_lq10AdjTmp.lData = g_lq10AcVrmsSampSysa.lData;
			s_lq10SetSysTmp.lData = g_lq10CanData.lData;

			g_lq10CanData.lData = _IQ10mpy(g_lq10AcVrmsSampSysa.lData,
			                               g_lq10CanData.lData);
			                               	
			//if((g_lq10CanData.lData >= 500 ) && (g_lq10CanData.lData <= 1500))

			if((g_lq10CanData.lData >= IQ10_VAC_RMS_SYSA_LIMDN ) 
				&& (g_lq10CanData.lData <= IQ10_VAC_RMS_SYSA_LIMUP))
			{
				s_lq10AdjTmp.lData = g_lq10CanData.lData;
				s_u16MdlCalibra = 1;
			}
			break;

		case 0x69:	// set Vaca samp correct sysb 
			g_lq10CanData.lData = _IQ10mpy(g_lq10AcVrmsSampSysb.lData,
			                               s_lq10SetSysTmp.lData) 
			                      + g_lq10CanData.lData;

			//if((g_lq10CanData.lData >=  ((INT32)-120 << 10) )  
			//   && (g_lq10CanData.lData <= ((INT32)120 << 10)) 
			//   && (s_u16MdlCalibra) )

			if((g_lq10CanData.lData >=  IQ10_VAC_RMS_SYSB_LIMDN)  
			   && (g_lq10CanData.lData <= IQ10_VAC_RMS_SYSB_LIMUP) 
			   && (s_u16MdlCalibra) )
			{
				u16CodeType = ucWriteFloatDataThree(VAC_SAM_SYSA_ADDR,
				                                    s_lq10AdjTmp);

				u16CodeType = u16CodeType
				              | ucWriteFloatDataThree(VAC_SAM_SYSB_ADDR,
				                                      g_lq10CanData);

				if (u16CodeType == RECOK)
				{
					g_lq10AcVrmsSampSysa.lData = s_lq10AdjTmp.lData;
					g_lq10AcVrmsSampSysb.lData = g_lq10CanData.lData;				
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;
	
	
		case 0x6c:	// set Vpfca samp correct sys 
			//s_u16MdlCalibra = 0;
			if(g_u16MdlCtrl.bit.SHORTRELAY)
			{
				s_lq10SetSysTmp.lData = g_lq10CanData.lData;

				s_lq10AdjTmp.lData = _IQ10mpy(g_lq10VpfcSampSys.lData,
			                                  g_lq10CanData.lData);

				//if( (s_lq10AdjTmp.lData >= 800 ) && (s_lq10AdjTmp.lData <= 1250) )

				if( (s_lq10AdjTmp.lData >= IQ10_VPFC_SAM_SYS_LIMDN ) 
					&& (s_lq10AdjTmp.lData <= IQ10_VPFC_SAM_SYS_LIMUP) )
				{
					u16CodeType = ucWriteFloatDataThree(VPFC_SAM_SYS_ADDR,
				                                    	s_lq10AdjTmp);

					if (u16CodeType == RECOK)
					{
						g_lq10VpfcSampSys.lData = s_lq10AdjTmp.lData;					
					}			
				}
				else
				{
					g_u16ErrType = COMM_FAIL;
				}
			}
			else
			{
				s_lq10AdjTmp.lData = _IQ10mpy(g_lq10VpfcConSys.lData, 
				                              g_lq10PfcVoltRegulate.lData);
				s_lq10AdjTmp.lData = _IQ10div(s_lq10AdjTmp.lData,  
				                              g_lq10PfcVoltDisp.lData);	
				
				//if( (s_lq10AdjTmp.lData >= 800 ) && (s_lq10AdjTmp.lData <= 1250) )

				if( (s_lq10AdjTmp.lData >= IQ10_VPFC_CON_SYS_LIMDN )
					&& (s_lq10AdjTmp.lData <= IQ10_VPFC_CON_SYS_LIMUP) )
				{
					u16CodeType = ucWriteFloatDataThree(VPFC_CON_SYS_ADDR,\
				                                    	s_lq10AdjTmp);

					if (u16CodeType == RECOK)
					{
						g_lq10VpfcConSys.lData = s_lq10AdjTmp.lData;					
					}			
				}
				else
				{
					g_u16ErrType = COMM_FAIL;
				}
			}
			break;

		case 0x70:	// set power ctrl correct sysa 
			s_u16MdlCalibra = 0;
			s_lq10SetSysTmp.lData = g_lq10CanData.lData;

			g_lq10CanData.lData = _IQ10mpy(g_lq10PowerConSysa.lData,\
			                               g_lq10CanData.lData);
			                               	
			//if( (g_lq10CanData.lData >= 512 ) && (g_lq10CanData.lData <= 1536) )

			if( (g_lq10CanData.lData >= IQ10_POW_CON_SYSA_LIMDN ) 
				&& (g_lq10CanData.lData <= IQ10_POW_CON_SYSA_LIMUP) )
			{
				s_lq10AdjTmp.lData = g_lq10CanData.lData;
				s_u16MdlCalibra = 1;
			}
			break;

		case 0x71:	// set power ctrl correct sysb
			g_lq10CanData.lData = _IQ10mpy(g_lq10PowerConSysb.lData,
			                               s_lq10SetSysTmp.lData) 
			                      + g_lq10CanData.lData;

			//if( (g_lq10CanData.lData >=  ((INT32)-1000 << 10) )
			//    && (g_lq10CanData.lData <= ((INT32)1000 << 10)) 
			//    && (s_u16MdlCalibra) )

			if( (g_lq10CanData.lData >=  IQ10_POW_CON_SYSB_LIMDN )
			    && (g_lq10CanData.lData <= IQ10_POW_CON_SYSB_LIMUP) 
			    && (s_u16MdlCalibra) )
			{
				u16CodeType = ucWriteFloatDataThree(POW_CON_SYSA_ADDR,
				                                    s_lq10AdjTmp);

				u16CodeType = u16CodeType\
				              | ucWriteFloatDataThree(POW_CON_SYSB_ADDR,
				                                      g_lq10CanData);

				if (u16CodeType == RECOK)
				{
					g_lq10PowerConSysa.lData = s_lq10AdjTmp.lData;
					g_lq10PowerConSysb.lData = g_lq10CanData.lData;
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x7C:	// set Idc current offset 

			if((g_lq10CanData.lData >=  IQ10_IDC_OffSET_LIMDN)  
			   && (g_lq10CanData.lData <= IQ10_IDC_OffSET_LIMUP))
			{
			
				u16CodeType =  ucWriteFloatDataThree(IDC_OFF_SET_ADDR,
				                                      g_lq10CanData);

				if (u16CodeType == RECOK)
				{
					
					CputoClaVar._i16DCCurOffset = (int16)(g_lq10CanData.lData>>10);	
					
					CputoClaVar._i16DCCurOffsetTrue = (int16)(g_lq10CanData.lData>>10);				
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}

			break;


		// receive model real voltage  for first calib when power on 
		// only to b coefficient 
		case 0x7D:	

			if((g_lq10CanData.lData >=  IQ10_VDC_REAL_LIMDN)  
			   && (g_lq10CanData.lData <= IQ10_VDC_REAL_LIMUP))
			{
	
					g_lq10MdlCalibVolt.lData  =  g_lq10CanData.lData;

					//b_samp=b_samp+(Vreal-Vsamp)
					// b_con=b_samp+(53.5-Vreal)

					lq12VoltSampSysbTemp.lData = g_lq12VoltSampSysb.lData;
	  				lq12VoltConSysbTemp.lData = g_lq12VoltConSysb.lData;


					lq12VoltSampSysbTemp.lData = g_lq12VoltSampSysb.lData 
												+(((INT32)(g_lq10MdlCalibVolt.lData - g_lq10MdlVolt.lData))<<2);

					lq12VoltConSysbTemp.lData = g_lq12VoltConSysb.lData 
												+(((INT32)(VDC_53V5 - g_lq10MdlCalibVolt.lData))<<2);
												
					
					g_lq12VoltSampSysb.lData =   lq12VoltSampSysbTemp.lData;
	  				g_lq12VoltConSysb.lData =   lq12VoltConSysbTemp.lData;								

			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}

			break;

        		
		case 0x76:	// set Iaca samp correct sysa 
		
			s_u16MdlCalibra = 0;
			s_lq10AdjTmp.lData = g_lq10AcIrmsSampSysa.lData;
			s_lq10SetSysTmp.lData = g_lq10CanData.lData;

			g_lq10CanData.lData = _IQ10mpy(g_lq10AcIrmsSampSysa.lData,
			                               g_lq10CanData.lData);
			                               	
			//if((g_lq10CanData.lData >= 500 ) && (g_lq10CanData.lData <= 1500))

			if((g_lq10CanData.lData >= IQ10_IAC_RMS_SYSA_LIMDN ) 
				&& (g_lq10CanData.lData <= IQ10_IAC_RMS_SYSA_LIMUP))
			{
				s_lq10AdjTmp.lData = g_lq10CanData.lData;
				s_u16MdlCalibra = 1;
			}
			break;
        
		case 0x77:	// set Iaca samp correct sysb 
			g_lq10CanData.lData = _IQ10mpy(g_lq10AcIrmsSampSysb.lData,
			                               s_lq10SetSysTmp.lData) 
			                      + g_lq10CanData.lData;

			//if((g_lq10CanData.lData >=  ((INT32)-120 << 10) )  
			//   && (g_lq10CanData.lData <= ((INT32)120 << 10)) 
			//   && (s_u16MdlCalibra) )

			if((g_lq10CanData.lData >=  IQ10_IAC_RMS_SYSB_LIMDN)  
			   && (g_lq10CanData.lData <= IQ10_IAC_RMS_SYSB_LIMUP) 
			   && (s_u16MdlCalibra) )
			{
				u16CodeType = ucWriteFloatDataThree(IAC_SAM_SYSA_ADDR,
				                                    s_lq10AdjTmp);

				u16CodeType = u16CodeType
				              | ucWriteFloatDataThree(IAC_SAM_SYSB_ADDR,
				                                      g_lq10CanData);

				if (u16CodeType == RECOK)
				{
					g_lq10AcIrmsSampSysa.lData = s_lq10AdjTmp.lData;
					g_lq10AcIrmsSampSysb.lData = g_lq10CanData.lData;				
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}
			break;

		case 0x85:
			if((g_lq10CanData.lData >=  IQ10_SHORTB_LIMDN)  
			   && (g_lq10CanData.lData <= IQ10_SHORTB_LIMUP))
			{
			
				u16CodeType =  ucWriteFloatDataThree(SHORTB_ADDR,
				                                      g_lq10CanData);

				if (u16CodeType == RECOK)
				{
					
					IsrVars._i16VdcShortCoefB = (int16)(g_lq10CanData.lData>>10);	
						
				}
			}
			else
			{
				g_u16ErrType = COMM_FAIL;
			}

			break;


    	case 0x9E:	// for led test,0:auto,1:all light,2:all off
			
			g_u16LEDFlg = g_ubitfCanData.uintdata[1].id & 0x03;

		break;


		case 0xa0:	// set rectiger NodeID
			u16CodeType = ucWriteFloatDataThree(ENP_LO_SN_ADDR,g_ubitfCanData);
			if (u16CodeType == RECOK)
			{
			//	g_u16NodeId0H = g_ubitfCanData.uintdata[1].id;
				g_u16NodeId0H_TO_CONTROLLER = g_ubitfCanData.uintdata[1].id;
				g_u16NodeId0L = g_ubitfCanData.uintdata[0].id;
			}
			break;

		case 0xa1:	// set rectiger SearialNo
			u16CodeType = ucWriteFloatDataThree(ENP_HI_SN_ADDR,g_ubitfCanData);
			if (u16CodeType == RECOK)
			{
				g_u16NodeId1H = g_ubitfCanData.uintdata[1].id;
				g_u16NodeId1L_TO_CONTROLLER = g_ubitfCanData.uintdata[0].id;
			}
			break;

		case 0xa2:	// set rectiger HW version number 
			u16CodeType = ucWriteFloatDataThree(VERSION_NO_ADDR,g_ubitfCanData);
			if (u16CodeType == RECOK)
			{
				g_u16VersionNoHw = g_ubitfCanData.uintdata[1].id;
			}
			break;

		case 0xa4:	// set rectifer whole run time 
			if((g_lq10CanData.lData > g_lq10RunTime.lData + 1000) && 
			   (g_lq10CanData.lData < g_lq10RunTime.lData + 1100))
			{
				g_u16EpromWr.bit.RUNTIME = 1;
				g_lq10RunTime.lData = g_lq10CanData.lData;
			}
			break;

		case 0xa5:	// set rectifier maintain times 
			if ( ( g_lq10CanData.lData >= 0) 
			   && (g_lq10CanData.lData <= ((INT32)10<<10)) )
			{	
				u16CodeType = ucWriteFloatDataThree(MEND_TIMES_ADDR,
				                                    g_lq10CanData);

				if (u16CodeType == RECOK)
				{
					g_lq10MaintainTimes.lData = g_lq10CanData.lData;
				}
			}					 
			break;

		case 0xa6:	// set rectifier bar code0 
			u16CodeType = ucWriteFloatDataThree(BAR_CODE0_ADDR,g_ubitfCanData);
			if (u16CodeType == RECOK)
			{
				g_u16BarCode0H = g_ubitfCanData.uintdata[1].id;
				g_u16BarCode0L = g_ubitfCanData.uintdata[0].id;
			}
			break;

		case 0xa7:	// set rectifier bar code1
			u16CodeType = ucWriteFloatDataThree(BAR_CODE1_ADDR,g_ubitfCanData);
			if (u16CodeType == RECOK)
			{
				g_u16BarCode1H = g_ubitfCanData.uintdata[1].id;
				g_u16BarCode1L = g_ubitfCanData.uintdata[0].id;
			}
			break;

		case 0xa8:	// set rectifier bar code2
			u16CodeType = ucWriteFloatDataThree(BAR_CODE2_ADDR,g_ubitfCanData);
			if (u16CodeType == RECOK)
			{
				g_u16BarCode2H = g_ubitfCanData.uintdata[1].id;
				g_u16BarCode2L = g_ubitfCanData.uintdata[0].id;
			}
			break;

		case 0xa9:	// set rectifier bar code3
			u16CodeType = ucWriteFloatDataThree(BAR_CODE3_ADDR,g_ubitfCanData);
			if (u16CodeType == RECOK)
			{
				g_u16BarCode3H = g_ubitfCanData.uintdata[1].id;
				g_u16BarCode3L = g_ubitfCanData.uintdata[0].id;
			}
			break;
      //设置模块特征类型数据
	//	case 0xaa://模块特征类型数据直接写死，不再从EPROM里写入、读出
		/*
			u16CodeType = ucWriteFloatDataThree(CHARACT_DATA_ADDR,
			                                    g_ubitfCanData);
			if (u16CodeType == RECOK)
			{
				g_u16CharactData0H= g_ubitfCanData.uintdata[1].id;
				g_u16CharactData0L= g_ubitfCanData.uintdata[0].id;
			}					 
			break;
        */
		case 0xab:	// set module the first maintain time and contain 
			u16CodeType = ucWriteFloatDataThree(MEND_DATA0_ADDR,g_ubitfCanData);
			if (u16CodeType == RECOK)
			{
				g_u16MaintainData0H= g_ubitfCanData.uintdata[1].id;
				g_u16MaintainData0L= g_ubitfCanData.uintdata[0].id;
			}					 
			break;

		case 0xac:	// set module the second maintain time and contain 
			u16CodeType = ucWriteFloatDataThree(MEND_DATA1_ADDR,g_ubitfCanData);
			if (u16CodeType == RECOK)
			{
				g_u16MaintainData1H= g_ubitfCanData.uintdata[1].id;
				g_u16MaintainData1L= g_ubitfCanData.uintdata[0].id;
			}					 
			break;

            //paul-20220228
            case 0xB0:
                //当防盗功能使能的时候，才接收监控的站点信息
                if(g_u16MdlCtrl.bit.AntiTheftFlag)
                {
                    u16NumberTempH = g_ubitfCanData.uintdata[1].id;
                    u16NumberTempHTmp = (u16NumberTempH>>8);
                    u16NumberTempL = g_ubitfCanData.uintdata[0].id;

                    g_u16CustomerNumberRx = u16NumberTempHTmp; //CustomerNumber
                    g_u32SitNumberRx = ((Uint32)(u16NumberTempH&0x00FF)<<16) //SiteNumber
                                    |((Uint32)u16NumberTempL);

                    g_u16ReceiveDataStatus = 1;
                    g_u16ReceiveDataEnFlag = 1;

                    if((g_u16CustomerNumberTx == 0xFF) && (g_u32SitNumberTx == 0x00FFFFFF))
                    {
                        u16CodeType = ucWriteFloatDataThree(MDL_CUSTOMER_NUM_ADDR,g_ubitfCanData);

                        if (u16CodeType == RECOK)
                        {
                            g_u16CustomerNumberTx = g_u16CustomerNumberRx;
                            g_u32SitNumberTx = g_u32SitNumberRx;
                        }
                    }
                    else
                    {
                        ;   //no action
                    }
                }
                else
                {
                    g_u16ErrType = COMM_FAIL;
                }

                if (g_u16MdlCtrl.bit.AntiTheftUnlockFlag)
                {
                    u16NumberTempH = g_ubitfCanData.uintdata[1].id;
                    u16NumberTempHTmp= (u16NumberTempH>>8);
                    u16NumberTempL = g_ubitfCanData.uintdata[0].id;

                    g_u16CustomerNumberRx= u16NumberTempHTmp; //CustomerNumber
                    g_u32SitNumberRx= ((Uint32)(u16NumberTempH&0x00FF)<<16) //SiteNumber
                                    |((Uint32)u16NumberTempL);

                    g_u16ReceiveDataStatus = 1;

                    if((g_u16CustomerNumberRx != g_u16CustomerNumberTx) || (g_u32SitNumberRx != g_u32SitNumberTx))
                    {
                        u16CodeType = ucWriteFloatDataThree(MDL_CUSTOMER_NUM_ADDR,g_ubitfCanData);

                        if (u16CodeType == RECOK)
                        {
                            g_u16CustomerNumberTx=g_u16CustomerNumberRx;
                            g_u32SitNumberTx=g_u32SitNumberRx;
                            g_u16MdlCtrl.bit.AntiTheftUnlockFlag = 0;
                        }
                    }
                    else
                    {
                        ;//no action
                    }
                }
                else
                {
                    //no action
                    ;
                }

                break;

            case 0xB1:
                if(g_u16MdlCtrl.bit.AntiTheftFlag)
                {
                    //监控读取模块站点信息
                    u16SitNumberTx=((UINT16)(g_u32SitNumberTx>>16));
                    g_ubitfCanData.uintdata[1].id = (g_u16CustomerNumberTx<<8)
                                               |(u16SitNumberTx &0x00FF);
                    g_ubitfCanData.uintdata[0].id = ((UINT16)g_u32SitNumberTx);
                }
                else
                {
                     //当模块返回ERRTYPE为0xf2的时候，监控认为模块是没有防盗功能的模块
                     g_u16ErrType = COMM_FAIL;
                }

                break;

            case 0xB2://CAN通信关机使能位；
                if (g_ubitfCanData.uintdata[1].id == 1)
                {
                    if(!g_u16MdlCtrl.bit.AntiTheftCanIntOff)
                    {
                        g_u16MdlCtrl.bit.AntiTheftCanIntOff = 1; //enable CANINT OFF 20210812 Eileen
                        g_u16EpromWr.bit.MDLCTRL = 1;
                    }
                }
                else if(g_ubitfCanData.uintdata[1].id == 0)
                {
                    if((g_u16MdlCtrl.bit.AntiTheftCanIntOff)&&((g_u32SitNumberRx == g_u32SitNumberTx) && (g_u16CustomerNumberRx == g_u16CustomerNumberTx)))
                    {
                        g_u16MdlCtrl.bit.AntiTheftCanIntOff = 0; //disable  CANINT OFF 20210812 Eileen
                        g_u16EpromWr.bit.MDLCTRL = 1;
                    }

                }
                else
                {
                    g_u16ErrType = COMM_FAIL;
                }

                break;

            case 0xB3:  //anti-theft unlock command
                if (g_ubitfCanData.uintdata[1].id == 1)
                {
                    g_u16MdlCtrl.bit.AntiTheftUnlockFlag = 1; //enable unlock 20210806 Eileen

                }
                else if (g_ubitfCanData.uintdata[1].id == 0)
                {
                    g_u16MdlCtrl.bit.AntiTheftUnlockFlag = 0; //disable unlock  20210806 Eileen
                }
                else
                {
                    g_u16ErrType = COMM_FAIL;
                }

                break;

            case 0xB4: //anti-theft enable command
                if (g_ubitfCanData.uintdata[1].id == 1)
                {
                    if(!g_u16MdlCtrl.bit.AntiTheftFlag)
                    {
                        g_u16MdlCtrl.bit.AntiTheftFlag = 1; ////enable antitheft function 20210806 Eileen
                        g_u16EpromWr.bit.MDLCTRL = 1;
                    }

                }
                else if(g_ubitfCanData.uintdata[1].id == 0)
                {

                    if((g_u16MdlCtrl.bit.AntiTheftFlag)&&((g_u32SitNumberRx == g_u32SitNumberTx) && (g_u16CustomerNumberRx == g_u16CustomerNumberTx)))
                    {
                        g_u16MdlCtrl.bit.AntiTheftFlag = 0; ////enable antitheft function 20210806 Eileen
                        g_u16EpromWr.bit.MDLCTRL = 1;
                    }
                }
                else
                {
                    g_u16ErrType = COMM_FAIL;
                }

                break;

            case 0xB5: //get CustNumDisMatch alarm
                g_ubitfCanData.uintdata[1].id = g_u16CustNumDisMatch;
                g_ubitfCanData.uintdata[0].id = 0;

                break;

            case 0xB6: //get CustNumDisMatch OFF TIME
                if ( (g_lq10CanData.lData <= IQ10_AntiThiefDelay_UP)
                       && (g_lq10CanData.lData >= IQ10_AntiThiefDelay_DN) )
                    {
                        if (labs(g_lq10CanData.lData - g_lq10AntiThiefDelayTime.lData) >= 100)
                        {
                            g_lq10AntiThiefDelayTime.lData = g_lq10CanData.lData + 1;

                            g_u16AntiThiefDelaySet = (UINT16)(_IQ10mpy(g_lq10AntiThiefDelayTime.lData,720) ) ;//Time*60*12 base on 5s_base

                            g_u16EpromWr.bit.ANTITHIEFDELAY = 1;
                        }
                    }
                    else
                    {
                        g_u16ErrType = COMM_FAIL;
                    }

                break;


		case 0xD0:	// get rectifier aca current
			g_ubitfCanData.fd =  _IQ10toF(g_lq10AcCur.lData);
			break;

		 case 0xD3:	// get rectifier aca power
			g_ubitfCanData.fd =  _IQ10toF(g_lq10AcPow.lData);
			break;

		case 0xD4:	// get rectifier power	(KWH)
		    if(g_lSCUGetWattAccum.lData < POWER_Q21)
			{
			  //g_lSCUGetWattAccum单位为：W*H,定标为：Q:0
	          //上报监控转换为：KW*H ,g_lSCUGetWattAccum/1000
	          //上面计算的结果Q10定标后转换为浮点数，精度为1/1024=0.0009
	          //即上报误差为1(w*h).
			   g_ubitfCanData.fd = _IQ10toF((g_lSCUGetWattAccum.lData*128)/125);
			}
			else
			{
			   //g_lSCUGetWattAccum单位为：W*H,定标为：Q:0
	           //上报监控转换为：KW*H ,g_lSCUGetWattAccum/1000
	           //上面计算的结果Q4定标后转换为浮点数，精度为1/16=0.0625
	           //即上报误差为60(w*h).
			   g_ubitfCanData.fd = _IQ4toF((g_lSCUGetWattAccum.lData*2)/125);
			} 				
			break;

		case 0xD5:	// get rectifier aca FRE
			g_ubitfCanData.fd =  _IQ10toF(g_lq10AcFre.lData);
			break;	          


		case 0xe0:	// read DSP memory of DCDC

		    if(IsrVars._u1632BitSingleRamEn==1) // 0x10000 以后的数据
		    {
		        s_pu16MemScopeAdd0 = (UINT16 *)g_ubitfCanData.uintdata[0].id;
		        g_ubitfCanData.uintdata[0].id = (UINT16)*(s_pu16MemScopeAdd0 + 0x10000);
		        s_pu16MemScopeAdd1 = (UINT16 *)g_ubitfCanData.uintdata[1].id;
		        g_ubitfCanData.uintdata[1].id = (UINT16)*(s_pu16MemScopeAdd1 + 0x10000);
		    }
		    else
		    {
		        s_pu16MemScopeAdd0 = (UINT16 *)g_ubitfCanData.uintdata[0].id;
		        g_ubitfCanData.uintdata[0].id = (UINT16)*s_pu16MemScopeAdd0;
		        s_pu16MemScopeAdd1 = (UINT16 *)g_ubitfCanData.uintdata[1].id;
		        g_ubitfCanData.uintdata[1].id = (UINT16)*s_pu16MemScopeAdd1;
		    }
		    break;
		    
		case 0xe1:	// write DSP memory of DCDC
			pu16setadd=(UINT16 *)g_ubitfCanData.uintdata[0].id;
			*pu16setadd = g_ubitfCanData.uintdata[1].id;
		    break;

		case 0xe2:	// write EEPROM of DCDC
			s_u16SetEepromAdd = (UINT16)g_ubitfCanData.fd;
		    break;
		    
		case 0xe3:	// write EEPROM of DCDC
			if( (s_u16SetEepromAdd == 4) || (s_u16SetEepromAdd == 8) 
			   || (s_u16SetEepromAdd == 12) || (s_u16SetEepromAdd == 16) )
			{
				ucWriteFloatDataThree (s_u16SetEepromAdd,g_lq12CanData);
			}
			else if(s_u16SetEepromAdd != 220)
			{
				ucWriteFloatDataThree (s_u16SetEepromAdd,g_lq10CanData);
			}
		    break;

		case 0xe8:	// read DSP memory of DCDC continually

		    if(IsrVars._u1632BitConRamEn==1)
		    {
		        s_pu16MemScopeAdd0 = (UINT16 *)g_ubitfCanData.uintdata[0].id;
		        g_ubitfCanData.uintdata[0].id = (UINT16)*(s_pu16MemScopeAdd0 + 0x10000 + s_u16RdMemNum);
		        s_pu16MemScopeAdd1 = (UINT16 *)g_ubitfCanData.uintdata[1].id;
		        g_ubitfCanData.uintdata[1].id = (UINT16)*(s_pu16MemScopeAdd1 + 0x10000 + s_u16RdMemNum);
		        s_u16RdMemNum = s_u16RdMemNum + 1;
		        if (s_u16RdMemNum >= g_u16RdNumSet)
		        {
		            s_u16RdMemNum = 0;
		        }
		    }
		    else
		    {
		        s_pu16MemScopeAdd0 = (UINT16 *)g_ubitfCanData.uintdata[0].id;
		        g_ubitfCanData.uintdata[0].id = (UINT16)*(s_pu16MemScopeAdd0 + s_u16RdMemNum);
		        s_pu16MemScopeAdd1 = (UINT16 *)g_ubitfCanData.uintdata[1].id;
		        g_ubitfCanData.uintdata[1].id = (UINT16)*(s_pu16MemScopeAdd1 + s_u16RdMemNum);
			    s_u16RdMemNum = s_u16RdMemNum + 1;
			    if (s_u16RdMemNum >= g_u16RdNumSet)
			    {
			        s_u16RdMemNum = 0;
			    }
		    }
			break;

		case 0xea: //read float data from three eeprom address

	        s_u16SetEepromAdd = (UINT16)g_ubitfCanData.lData;
	        g_fRdTemp = fReadFloatDataThreePointer(s_u16SetEepromAdd);
			g_ubitfCanData.lData= g_fRdTemp.lData;
			
		    break;

        case 0xf3:  //read 32bit data from eeprom once

            s_u16SetEepromAdd = (UINT16)g_ubitfCanData.lData;
            if (uiI2caReadDataPointer(s_u16SetEepromAdd) == I2C_SUCCESS)//读一次32位数据
            {
                g_ubitfCanData.lData= g_fRdTemp.lData;
            }
            else
            {
                g_u16ErrType = COMM_FAIL;
            }
            break;


		case 0xfa:	/* read dc code */			
			g_ubitfCanData.uintdata[1].id = MDL_SW_BOM_SN;
			g_ubitfCanData.uintdata[0].id = 0x0;
			break;

    	case 0xfc:         /* DSP reset */
            if (g_ubitfCanData.uintdata[1].id == 1)              
            {
			
             mOffDcdcPwm();               
             mOffPfcPwm();
             g_u16MdlStatus.bit.OFFSTAT = 1; 

             DINT;
             IER = 0x0000;
             IFR = 0x0000;

             EALLOW;
                                     
             Cla1Regs.MCTL.bit.HARDRESET = 1;
             WdRegs.WDCR.all = 0; //reset the DSP
             EDIS;
             }
             else if (g_ubitfCanData.uintdata[1].id == 0)               
              {
                                     // no action;
              }
              else
             {
               g_u16ErrType = COMM_FAIL;
              }
             break;   



		case 0xfe:	/* CAN loader set */
			if (g_ubitfCanData.uintdata[1].id == 1)		
			{
               g_u16downloadflag = 1;
               /*
				DINT;

                mOffDcdcPwm();	
            	mOffPfcPwm();
                g_u16MdlStatus.bit.OFFSTAT = 1;

				IER = 0x0000;
				IFR = 0x0000;
				
				
			
					
				DELAY_US(TIME5MS_CPUCLK);
				B1AppEntryPointer();
			*/
			}
			break;   
		
		default:
			g_u16ErrType = COMM_FAIL;
		    //g_u16ErrType = 0xF0;
			break ;
	}
	
	if(g_u16DSTADDR == g_u16MdlAddr) 
   
	{
		sEncodeCanTxID(RMP,PTP_MODE,CSU_ADDR,DATA_END);
		sEncodeCanTxData(g_u16MsgType,g_u16ErrType,g_u16ValueType,g_ubitfCanData);
		sCanWrite5(&g_stCanTxData);
	}
}

/*******************************************************************************
*Name: vMasterLoadSharingCal()
*Function:master  cal average current,and analyse the volt adjust attribute
*******************************************************************************/
static void vMasterLoadSharingCal(void)
{
	UINT16 i,k;
	UINT16 u16MdlNumber;      
	ubitfloat lq10max,lq10min,lq10sum,ubitftemp;
	UINT16 u16MdlVoltSum;
	
	//initial the temp variable
	u16MdlVoltSum = 0;
	lq10max.lData = 0;
	lq10min.lData = ((INT32)250<<10);
	lq10sum.lData = 0;
	u16MdlNumber = 0;
	k = 0;

	s_u16SysMixStatus = s_u16SysMixStatus & 0xFFF3;  //ls 20120224

	s_u16SysRateCurr = IDC_RATE_Q7;	
	
	for (i = 0; i < MAX_NUM; i++)
	{
	    //if rectifier i is on, accumulate its curr
        //if ((CanComVars._au16MdlOnoff[i] & 0x01) == 0)  //REC i is on 20120224
        if ((CanComVars._au16MdlOnoff[i] & 0x03) == 0)  //paul
		{
			//#if SHARE_MODE_REAL_CURR
			if (s_u16OldRecInFlag == SHARE_MODE_REAL_CURR)
			{
				//record the max curr
				if (CanComVars._alq10MdlCurrent[i].lData > lq10max.lData)
				{
					lq10max.lData = CanComVars._alq10MdlCurrent[i].lData;
				}

				//record the min curr
				if (CanComVars._alq10MdlCurrent[i].lData < lq10min.lData)
				{
					lq10min.lData = CanComVars._alq10MdlCurrent[i].lData;
				}

				//accumulate module number
				u16MdlNumber ++;

				//curr sum of all mdls 
				lq10sum.lData = lq10sum.lData + CanComVars._alq10MdlCurrent[i].lData;
			}
			else
			{
				//volt sum of all mdls
				u16MdlVoltSum = u16MdlVoltSum + CanComVolVars._au16MdlVolt[i];
				
				if (CanComVars._alq10MdlCurrPct[i].lData > lq10max.lData)
				{
					lq10max.lData = CanComVars._alq10MdlCurrPct[i].lData;
				}

				//record the min curr
				if (CanComVars._alq10MdlCurrPct[i].lData < lq10min.lData)
				{
					lq10min.lData = CanComVars._alq10MdlCurrPct[i].lData;
				}

							//accumulate module number
				u16MdlNumber ++;

				//curr sum of all mdls change to percent 
				lq10sum.lData = lq10sum.lData + CanComVars._alq10MdlCurrPct[i].lData;
			}
				
			//record the module adjust attribute	
			if (CanComVars._ai16MdlCurrDelta[i] > 1)
			{
				k = k|0x01;
			}
			else if (CanComVars._ai16MdlCurrDelta[i] < (-1))
			{
				k = k|0x02;
			}
			else
			{
				k = k|0x04;                        
			}
		}

		// get SysRateCurr use minimun Mdl Rate current 
		if ((CanComVars._au16MdlRateCurr[i] < s_u16SysRateCurr) 	
			&& CanComVars._au16MdlRateCurr[i])	
		{
			s_u16SysRateCurr = CanComVars._au16MdlRateCurr[i];
		}
		//ls add 20120224
		s_u16SysMixStatus = s_u16SysMixStatus | (CanComVars._au16MdlOnoff[i] & 0x0C);
		g_u16MdlMixStatus |= (s_u16SysMixStatus << 8);
		
	} 			

	// PARAFLAGMASTER for MASTER converter more and on mode:1 ;else PARAFLAG :0
   	if ( u16MdlNumber >= 2 )
	{
		g_u16MdlStatusExtend.bit.PARAFLAGMASTER = 1;
	}
	else
	{
        g_u16MdlStatusExtend.bit.PARAFLAGMASTER = 0;
	}
	
	//cal average curr; if only one rectifier, no adjust.
	if (u16MdlNumber)
	{		
		//#if SHARE_MODE_REAL_CURR
		if (s_u16OldRecInFlag == SHARE_MODE_REAL_CURR)
		{
			g_lq10MdlAvgCurr.lData = lq10sum.lData/(INT32)u16MdlNumber;
		}
		else
		{
			s_u16SysAvgVolt = (u16MdlVoltSum / u16MdlNumber);

			s_lq10SysAvgPct.lData = lq10sum.lData / (INT32)u16MdlNumber;
			ubitftemp.lData = _IQ10mpy(g_i32CurrRate, s_lq10SysAvgPct.lData);
			g_lq10MdlAvgCurr.lData = _IQ10div(ubitftemp.lData,((INT32)100 << 10));
		}			
	}
	else
	{
		s_u16SysAvgVolt = 0;

		g_lq10MdlAvgCurr.lData = 0;
		s_lq10SysAvgPct.lData = 0;		
	}			
	
	/*analyse the volt adjust attribute*/
	s_u16CurrCtrlFlag = NON_ADJUST;
	s_u16NewCurrCtrlFlag = NEW_NON_ADJUST;
	if (k == 0x01)
	{
		//if all module's s_ai16MdlCurrDelta[i] is"+",set the flag to reduce volt
		s_u16CurrCtrlFlag = SYN_REDUCE_VOLT;
		s_u16NewCurrCtrlFlag = NEW_SYN_REDUCE_VOLT;
	}
	else if (k == 0x02)
	{
		//if all module's s_ai16MdlCurrDelta[i] is "-",set the flag to raise volt
		s_u16CurrCtrlFlag = SYN_RAISE_VOLT;
		s_u16NewCurrCtrlFlag = NEW_SYN_RAISE_VOLT;
	}
	else				
	{
		/*if there have "+"  and  "-",set the flag to regulateaccording 
		  to self status,in this condition,the max curr must bigger then 
		  the min curr about 2A*/
	    /*
		if ((lq10max.lData - lq10min.lData) > IDC_4PCT )//100*0.04 ＜＜ 10=4096	
		{
			s_u16CurrCtrlFlag = ATUO_ADJUST;
			s_u16NewCurrCtrlFlag = NEW_ATUO_ADJUST;
		}
		else
		{
			s_u16CurrCtrlFlag = NON_ADJUST;
			s_u16NewCurrCtrlFlag = NEW_NON_ADJUST;
		}
         */


 
                   if (s_u16OldRecInFlag == SHARE_MODE_REAL_CURR )//SHARE_MODE_LOAD_PCT)
                   {
                            /*if there have "+"  and  "-",set the flag to regulateaccording 
                              to self status,in this condition,the max curr must bigger then 
                              the min curr about 2A*/
                            if ((lq10max.lData - lq10min.lData) > IDC_RATE_4PCT) 
                            {
                                     s_u16CurrCtrlFlag = ATUO_ADJUST;
                                     s_u16NewCurrCtrlFlag = NEW_ATUO_ADJUST;
                            }
                            else
                            {
                                     s_u16CurrCtrlFlag = NON_ADJUST;
                                     s_u16NewCurrCtrlFlag = NEW_NON_ADJUST;
                            }
                   }
                   else
                   {
                              /*if there have "+"  and  "-",set the flag to regulateaccording 
                              to self status,in this condition,the max curr must bigger then 
                              the min curr about 2A*/
                            if ((lq10max.lData - lq10min.lData) > IDC_4PCT) //100*0.04 ＜＜ 10=4096	   
                            {
                                     s_u16CurrCtrlFlag = ATUO_ADJUST;
                                     s_u16NewCurrCtrlFlag = NEW_ATUO_ADJUST;
                            }
                            else
                            {
                                     s_u16CurrCtrlFlag = NON_ADJUST;
                                     s_u16NewCurrCtrlFlag = NEW_NON_ADJUST;
                            }        
                   }


	}	
}

/*******************************************************************************
*Name:vCanProcess()                                                         
*Input vars: none                                                            
*Output vars: none                                                        
*******************************************************************************/
//interrupt void sCan_Isr(void)
void vCanProcess(void)
{
    /*if((CanaRegs.CAN_TXRQ_21 & 0x00000008))
    {

        sCanResetTx4();
        //sCanTxISR5();
    }*/

    if(!(CanaRegs.CAN_TXRQ_21 & 0x00000010))
    {
        //sCanResetTx4();
        sCanTxISR5();
    }

    if((CanaRegs.CAN_NDAT_21 & 0x00000001))
    {
        sCanRxISR1();
    }

}        	

/*******************************************************************************
*Function name:  vAvgCurrCal()
*Description: calculation load sharing delta according to rectifier current and 
*           system average current  
*******************************************************************************/
void vAvgCurrCal(void)
{    
	INT32  u32CurrentDeltaTemp;
	longunion lTemp;
	
	if (((s_u16NewCurrCtrlFlag) || (s_u16CurrCtrlFlag)) && (!g_u16MdlStatus.bit.OFFSTAT)  
	    && (g_u16MdlStatusExtend.bit.DCFAULT == 0)// && (g_u16MdlCtrl.bit.FUSEB == 0)
        && (g_u16CurrSerUnBal == 0) && (g_u16ActionReady == NORMAL_RUN_STATE) && (!g_u16CurrLimitStatusFlag) )//paul 2021-0610
	{ 
		//according to the flag,syn reduce volt,1cnt ->2mv
		if ((s_u16CurrCtrlFlag == SYN_REDUCE_VOLT) || (s_u16NewCurrCtrlFlag == NEW_SYN_REDUCE_VOLT))
		{         	
			g_i16CurrDelta = g_i16CurrDelta - 4;
		}
		//according to the flag,syn raise volt,1cnt ->2mv
		else if ((s_u16CurrCtrlFlag == SYN_RAISE_VOLT) || (s_u16NewCurrCtrlFlag == NEW_SYN_RAISE_VOLT))
		{
			g_i16CurrDelta = g_i16CurrDelta + 4;
		}
		//according to the flag,module adjust volt by self,1cnt ->2mv
		else if ((s_u16CurrCtrlFlag == ATUO_ADJUST) || (s_u16NewCurrCtrlFlag == NEW_ATUO_ADJUST))		
		{
			/*according to the difference of avg curr & mdl curr,module adjust 
			  self volt step by step*/
				g_u16RunFlag.bit.LOADSHARE = 0;
				//36/20=1.6*1024=1843.2
				u32CurrentDeltaTemp = g_lq10MdlAvgCurr.lData - g_lq10MdlCurr.lData;
				lTemp.lData = _IQ10mpy(u32CurrentDeltaTemp , (INT32)819);
			 //当lTemp为-1～-1023时，会累加一个-1到g_ i16CurrDelta上
			//g_i16CurrDelta += (INT16)(lTemp.lData>>10);
				g_i16CurrDelta += (INT16)(lTemp.lData/1024);
			
			if ( (g_lq10MdlAvgCurr.lData <= IDC_0A5) 
			    && ((g_i16CurrDelta >= CURR_DELTA_UP) 
			    || (g_i16CurrDelta <= CURR_DELTA_DN)) )
			{
				g_i16CurrDelta = 0;
			}
		}
		
		//the adjust range is -0.7v ~ + 0.7v(-360 ~ +360)
		if(g_i16CurrDelta > CURR_DELTA_UP)
		{
			g_i16CurrDelta =CURR_DELTA_UP;
		}
		else if(g_i16CurrDelta < CURR_DELTA_DN)
		{
			g_i16CurrDelta = CURR_DELTA_DN;
		}
		s_u16CurrCtrlFlag = 0;
		s_u16NewCurrCtrlFlag = 0;
	}
}

/*******************************************************************************
*Function name:  vAddressCal()
*Description: address auto-identify 
*******************************************************************************/
static void  vAddressCal(void)
{
    UINT32 u32IDSelf,u32IDReceive,u32IDInBuffer;
    UINT16 i,u16IdExistFlag;
    UINT16 u16ModuleType;

    //if receive other module`s id
    if(g_u16RunFlag.bit.ADDRCAL)
    {
        g_u16RunFlag.bit.ADDRCAL = 0;

        u16IdExistFlag = 0;

        //get module type from module id info.
        u16ModuleType = (s_afMdlAddID[MAX_NUM + 1].msgaddid[1] >> 4) & 0x000f; //typ0

        if(s_afMdlAddID[MAX_NUM + 1].msgaddid[1]&0x2000) //typ1
        {
            u16ModuleType = u16ModuleType | 0x0010;
        }

        if(s_afMdlAddID[MAX_NUM + 1].msgaddid[2]&0x0010)  //typ2
        {
            u16ModuleType = u16ModuleType | 0x0020;
        }

        if(s_afMdlAddID[MAX_NUM + 1].msgaddid[2]&0x0020)  //typ2=2
        {
            u16ModuleType = u16ModuleType | 0x0040;
        }

        if(s_afMdlAddID[MAX_NUM + 1].msgaddid[2]&0x0040)  //typ2=3
        {
            u16ModuleType = u16ModuleType | 0x0080;
        }

        // 3500E4 TYPE=102， T2=3，T1=0，T0=6
        //chk the system if it is mixed system  // mask by mhp 201004 fro debugging

        // added by lvbl 20110719
        if((u16ModuleType == 0) || (u16ModuleType == 1) ||(u16ModuleType == 2)
           ||(u16ModuleType ==0x20))
        {
            //set the mixed system flag
            //the system has 2900w rectifier
            g_u16PowerMixedSystemType|=MIX_TYPE_2900W;
            g_u16PowerMixedFlag = 1;
        }

        /*else if((u16ModuleType == 3)||(u16ModuleType == 4) || (u16ModuleType == 5)
                 ||(u16ModuleType == 0x13))*/
        else if((u16ModuleType == 90) || (u16ModuleType == 92)||(u16ModuleType == 93))
        {
            //the system has 3200w rectifier
            g_u16PowerMixedSystemType|=MIX_TYPE_3000e3;
            g_u16PowerMixedFlag = 1;
        }

        else if((u16ModuleType == 70)|| (u16ModuleType == 102))
        {
            //the system has 3500w rectifier
            g_u16PowerMixedSystemType|=MIX_TYPE_3500e3;
            g_u16PowerMixedFlag = 1;
        }


        //get module`s id and the receive id
        u32IDReceive = (UINT32)s_afMdlAddID[MAX_NUM + 1].msgaddid[1];

        u32IDReceive = (u32IDReceive << 16)
                       | ((UINT32)s_afMdlAddID[MAX_NUM + 1].msgaddid[0]);

        u32IDSelf = (UINT32)g_u16NodeId0H;
        u32IDSelf = (u32IDSelf << 16) | ((UINT32)g_u16NodeId0L);

        //the receive id is same to module,address overlap,set IDERR warning
        if(u32IDReceive == u32IDSelf)
        {
            g_u16MdlStatus.bit.IDERR = 1;
        }

        /*if the receive id less then module`s id,module`s address add 1,and
          compare the ID with id that in ID buffer,if the id not in the buffer,
          restore the id;*/
        if((u32IDReceive < u32IDSelf) && (u32IDReceive))
        {
            for (i = 0; i < MAX_NUM; i++)
            {
                u32IDInBuffer = ((UINT32)s_afMdlAddID[i].msgaddid[1]);

                u32IDInBuffer = (u32IDInBuffer << 16)
                                | ((UINT32)s_afMdlAddID[i].msgaddid[0]);

                if(u32IDInBuffer)
                {
                    if(u32IDInBuffer == u32IDReceive)
                    {
                        u16IdExistFlag = 1;
                    }
                }
                else
                {
                    break;
                }

                if (u16IdExistFlag)
                {
                    break;
                }
            }

            if(u16IdExistFlag == 0)
            {
                g_u16MdlAddrBuff = g_u16MdlAddrBuff + 1;

                s_afMdlAddID[i].msgaddid[1] \
                     = s_afMdlAddID[MAX_NUM + 1].msgaddid[1];

                s_afMdlAddID[i].msgaddid[0] \
                     = s_afMdlAddID[MAX_NUM + 1].msgaddid[0];
            }
        }

        s_u16OldRecInFlag = SHARE_MODE_LOAD_PCT;     //ls 20120202

    }
}

/*******************************************************************************
*Function name:  vACComInterrupt()
*Description: AC <80V NO COM TO the NGC 
*******************************************************************************/

void vACComInterrupt(void)
{
	if(g_u16CpuStartTimer >= 50)//wait for adc stable
	{
		if(g_u16MdlStatusExtend.bit.DCINPUTSTATUS == 0)//交流输入
		{	
			if(g_lq10AcVolt.lData <= VAC_COM_HALT)		  
		 	{
			    g_u16MdlStatusExtend.bit.ACCOMHALT = 1;  
		        //   g_u16MdlStatusExtend.bit.ACCOMHALT = 0;// 暂时屏蔽掉AC《80V 不通讯
		    }
		    else
		 	{
			    g_u16MdlStatusExtend.bit.ACCOMHALT = 0;
			
			    g_u16ComIntFlag = 0 ;  //ls 20120222
		 	}
		}
		else//直流输入
		{	
			if(g_lq10AcVolt.lData <= VDCIN_COM_HALT)		  
		 	{
			    g_u16MdlStatusExtend.bit.ACCOMHALT = 1;  // 暂时屏蔽掉AC《80V 不通讯
		        //   g_u16MdlStatusExtend.bit.ACCOMHALT = 0;
		    }
		    else
		 	{
			    g_u16MdlStatusExtend.bit.ACCOMHALT = 0;
			
			    g_u16ComIntFlag = 0 ;  //ls 20120222
		 	}
		}
	}

}
//===========================================================================
// No more.
//===========================================================================
