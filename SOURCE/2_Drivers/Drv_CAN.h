/*=============================================================================*
 *         Copyright(c) 2004-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : HD415CZ
 *
 *  FILENAME : gbb_CanDriver.h
 *  PURPOSE  : rectifier communicate with controller and other rectifiers	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *     2008-11-25      A000           zoulx             Created.   Pre-research
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *    detail information refer to gbb_main.h      
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                               		     DESCRIPTION 
 *============================================================================*/

#ifndef Drv_CAN_H
#define Drv_CAN_H


#include "Prj_macro.h"

#if DSP28_CANA
extern void InitCan(void);
extern void InitCanLoad(void);
extern void InitCana(void);

extern void InitCanaID(void);
extern void InitCanGpio(void);
extern void InitCanaGpio(void);
#endif // endif DSP28_ECANA

/*******************************************************************************
*define constants for tx and rx can data														  
*******************************************************************************/
#define CAN_RXBUF_SIZE   	10
#define CAN_TXBUF_SIZE    	15
#define	CAN_RXBUF_EMPTY	  	0
#define	CAN_RXBUF_RDY	  	1

/*******************************************************************************
*define constant in CAN frame
*******************************************************************************/
//for PROTNO
#define   RMP            	0x060
#define   RRP            	0x070

//for PTP
#define   BROAD_CAST      	0x00
#define   PTP_MODE        	0x01

//for CNT
#define   DATA_END       	 0x00
#define   DATA_CNT        	0x01

//for ErrType
#define   NORMAL         	0xf0
#define   ADDR_FAIL      	0xf1
#define   COMM_FAIL      	0xf2
#define   DATA_CHK_FAIL  	0xf3
#define   ADDR_IDENTIFY  	0xf4

//for module address
#define   BROAD_CAST_ADDR  	0xff 
#define   CSU_ADDR          0xf0 

//for message type
#define   REPLY_BY_BYTE   	0x41
#define   REPLY_BY_BIT     	0x42
/*******************************************************************************
*declare variables  for CAN frame
*******************************************************************************/
extern UINT16 g_u16PROTNO;                
extern UINT16 g_u16PTP;      
extern UINT16 g_u16DSTADDR;  
extern UINT16 g_u16SRCADDR;  
extern UINT16 g_u16CNT;      
//extern UINT16 g_u16ErrFlag;  
extern UINT16 g_u16ErrType;  
extern UINT16 g_u16MsgType;  
extern UINT16 g_u16ValueType;
extern ubitfloat g_ubitfCanData;
extern ubitfloat g_lq10CanData;
extern ubitfloat g_lq12CanData;

extern CANFRAME  g_stCanTxData;
extern CANFRAME  g_stCanRxData;


extern void sCanResetTx5(void);
//transmit mail box transmit data
void sCanHdSend5(CANFRAME *pdata);

//receive mail box
void sCanHdRead1(CANFRAME *pdata);

#endif






