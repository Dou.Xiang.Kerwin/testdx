#ifndef GBB_Can_Interface_H
#define GBB_Can_Interface_H



/*******************************************************************************
*declare application for app
*******************************************************************************/
extern void sCanBufferInitial(void);
extern void sCanRxISR1(void);
extern void sCanTxISR5(void);
extern void sCanResetTx4(void);
extern UINT16 sCanRead(CANFRAME *pdata);
extern void sCanWrite5(CANFRAME *pdata);
extern void vCanHdSend6(CANFRAME *pdata, Uint16 mailboxNo);
extern void sCanHdSend5(CANFRAME *pdata);
extern void sDecodeCanRxFrame(CANFRAME stRxFrameTemp);
extern void sEncodeCanTxID(UINT16 u16ProtNoTemp,UINT16 u16PTPTemp,
                           UINT16 u16DstAddrTemp,UINT16 u16CntTemp);
extern void sEncodeCanTxData(UINT16 u16MsgTypeTemp,UINT16 u16ErrTypeTemp,
                             UINT16 u16ValueTypeTemp,ubitfloat ubitfDataTemp);
extern void sEncodeCanTxIQ10Data(UINT16 u16MsgTypeTemp,UINT16 u16ErrTypeTemp,
                                UINT16 u16ValueTypeTemp,ubitfloat ubitfDataTemp);

#endif
