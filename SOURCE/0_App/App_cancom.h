// Running on TMS320LF280xA               
// External clock is 20MHz, PLL * 10/2 , CPU-Clock 100 MHz	      
// Date: from 2005/12/28  ,jurgen
// Version:1.00     Change Date: 
/***************************************************************************
*             Defines
***************************************************************************/
#ifndef App_CanCom_H
#define App_CanCom_H

#ifdef __cplusplus
extern "C" {
#endif


#include "Prj_config.h"




extern UINT16  g_u16CustNumDisMatch;

// variables for nor derating mixed
extern INT32  g_i32CurrChgFactor;
extern INT32  g_i32AvgCurrChgFactor;

// rectifier mix system status, ls 20120224
extern UINT16 g_u16MdlMixStatus;

//Power mixed flag
extern UINT16   g_u16PowerMixedSystemType;

extern UINT16   g_u16PowerMixedFlag;

extern UINT16   g_u16SelfDxCtrlNextTimer;

extern UINT16   g_u16MdlNumber;//��λģ������

//variables for read memery
extern UINT16 g_u16RdNumSet;
/***************************************************************************
*            Variables Definition
***************************************************************************/

extern UINT16 g_u16AddIdentifyFlag; //address auto-identifiication flag
extern UINT16 g_u16AddIdentifyNum;  //address auto-identifiication cnt
extern UINT16 g_u16MdlAddrBuff;     // rectifier address
extern UINT16 g_u16AddressSmall;    //minnum  address on system
extern UINT16 g_u16VoltAdjDelta;    // volt adjust delta
extern INT16  g_i16CurrDelta;       // volt adjust delta cal by current adjust


extern ubitfloat g_lq10MdlAvgCurr;  // system average current

//variables for module`s hardware and software version
extern UINT16 g_u16VersionNoSw;

// variables for nor derating mixed
extern INT32   g_i32CurrChgFactor;
extern INT32   g_i32AvgCurrChgFactor;

// rectifier mix system status, ls 20120224
extern UINT16 g_u16MdlMixStatus;

//Power mixed flag
extern UINT16  g_u16PowerMixedSystemType;
extern UINT16  g_u16PowerMixedFlag;

extern UINT16  g_u16SelfDxCtrlNextTimer;   // delay to next which no answer
extern UINT16  g_u16MdlNumber;//��λģ������

extern ubitinth g_u16SelfDxCtrl;

extern ubitintg g_u16SelfDxValue;

struct CanComStruct
{
//------------offset=0000H-----------------------
//g3 50A 126UNIT������1�����������ݵĻ��棬����������ҳ��ţ�ѡ0x80=128
       UINT16 _au16MdlRateCurr[MAX_NUM + 2];
       ubitfloat _alq10MdlCurrPct[MAX_NUM + 2];
     
	   //for all modules` current restore buffer	   
       ubitfloat _alq10MdlCurrent[MAX_NUM + 2];
       //for all module` load sharing adjust delta restore buffer
       INT16  _ai16MdlCurrDelta[MAX_NUM + 2];
	   UINT16 _au16MdlOnoff[MAX_NUM + 2];	// all rectifier on/off state
//**************************************************** 

};               

extern volatile struct CanComStruct CanComVars;     


struct CanComVolStruct
{


       //for all modules' output voltage restore buffer, ls 20120224
       UINT16 _au16MdlVolt[MAX_NUM + 2];
       UINT16 _au16MdlVoltSelfDiag[MAX_NUM + 2];

//**************************************************** 

};               
 
extern volatile struct CanComVolStruct CanComVolVars;
// CAN communication treatment
extern void vCanDataParsing(void);

//Rrp protocol treatment
extern void vRrpProtocol(void);

//load sharing calcuation
extern void vAvgCurrCal(void);

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif

