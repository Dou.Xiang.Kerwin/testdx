 /*=============================================================================*
 *         Copyright(c) 2007-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : GBB Rectifiers
 *
 *  FILENAME : GBB_macro.h
 *  PURPOSE  : Define the io assignment the macro from the hardware of dsp
 *				     
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *  2008-6-10         OOA            Agnes           Created.   Pre-research 
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *    
 *============================================================================*/

/******************************************************************************/
/*IO definition																  */
/*This part is for the IO port supplied by the DSP 							  */
/******************************************************************************/
#ifndef macro_H
#define  macro_H


#define DSP28_CANA 1
#define FANPWN_ECAP      ECap3Regs.CAP4

#define hiHvsd	        GpioDataRegs.GPADAT.bit.GPIO29
#define hiDCShort       GpioDataRegs.GPADAT.bit.GPIO15
#define UnplugLowOn     GpioDataRegs.GPADAT.bit.GPIO14


/******************************************************************************/
/*macro definition															  */
/*This part defines the macro from hardware of DSP 							  */
/******************************************************************************/
//Stop/start all the TB clocks

#define mTBClockOn()    {CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 1;}
#define mTBClockOff()   {CpuSysRegs.PCLKCR0.bit.TBCLKSYNC = 0;}

#define mSWForceSYN()   {EPwm1Regs.TBCTL.bit.SWFSYNC = TB_ENABLE;}   

//dcdc pwm control
#define mOnDcdcPwm()      {EPwm4Regs.AQCSFRC.all = 0x0;\
                           EPwm5Regs.AQCSFRC.all = 0x0;\
                           EPwm6Regs.AQCSFRC.all = 0x0;\
                           EPwm7Regs.AQCSFRC.all = 0x0;}

#define mOffDcdcPwm()     {EPwm4Regs.AQCSFRC.all = 0x01;\
                           EPwm5Regs.AQCSFRC.all = 0x01;\
                           EPwm6Regs.AQCSFRC.all = 0x01;\
                           EPwm7Regs.AQCSFRC.all = 0x01;}

#define mOnHmosDriv()      {GpioDataRegs.GPBSET.bit.GPIO59 = 1;\
                            GpioDataRegs.GPBSET.bit.GPIO34 = 1;}

#define mOffHmosDriv()      {GpioDataRegs.GPBCLEAR.bit.GPIO59 = 1;\
                            GpioDataRegs.GPBCLEAR.bit.GPIO34 = 1;}

//pfc pwm control
#define mOnPfcPwm()        {EPwm1Regs.AQCSFRC.all = 0x00;\
                           EPwm3Regs.AQCSFRC.all = 0x00;}

#define mOffPfcPwm()       {EPwm1Regs.AQCSFRC.all = 0x01;\
                           EPwm3Regs.AQCSFRC.all = 0x01;}

#define mRedLedOn()		  (GpioDataRegs.GPBSET.bit.GPIO33 = 1)
#define mRedLedOff()	  (GpioDataRegs.GPBCLEAR.bit.GPIO33 = 1)
#define mRedLedTwink()	  (GpioDataRegs.GPBTOGGLE.bit.GPIO33 = 1)

#define mGreenLedOn()     {GpioDataRegs.GPASET.bit.GPIO16 = 1;\
                           GpioDataRegs.GPASET.bit.GPIO17 = 1;}
#define mGreenLedOff()    {GpioDataRegs.GPACLEAR.bit.GPIO16 = 1;\
                           GpioDataRegs.GPACLEAR.bit.GPIO17 = 1;}
#define mGreenLedTwink()  {GpioDataRegs.GPATOGGLE.bit.GPIO16 = 1;\
                           GpioDataRegs.GPATOGGLE.bit.GPIO17 = 1;}

#define mYellowLedOn()	  {GpioDataRegs.GPASET.bit.GPIO11 = 1;\
                           GpioDataRegs.GPASET.bit.GPIO12 = 1;}
#define mYellowLedOff()	  {GpioDataRegs.GPACLEAR.bit.GPIO11 = 1;\
                           GpioDataRegs.GPACLEAR.bit.GPIO12 = 1;}
#define mYellowLedTwink() {GpioDataRegs.GPATOGGLE.bit.GPIO11 = 1;\
                           GpioDataRegs.GPATOGGLE.bit.GPIO12 = 1;}

//over relay control
#define mOverRelayOpen()  (GpioDataRegs.GPASET.bit.GPIO9 = 1)
#define mOverRelayClose() (GpioDataRegs.GPACLEAR.bit.GPIO9 = 1)
#define mOverRelayStatus_disconnect  ( GpioDataRegs.GPADAT.bit.GPIO9 == 1)
#define mOverRelayStatus_connect  ( GpioDataRegs.GPADAT.bit.GPIO9 == 0)

//main relay control	
#define mMainRelayOpen()  (GpioDataRegs.GPACLEAR.bit.GPIO5 = 1)
#define mMainRelayClose() (GpioDataRegs.GPASET.bit.GPIO5 = 1)

//e2prom  wp	
#define mWrEnable()		  (GpioDataRegs.GPACLEAR.bit.GPIO25 = 1)
#define mWrDisable()	  (GpioDataRegs.GPASET.bit.GPIO25 = 1)

//test pin control
//#define mTestSigHigh()	  (GpioDataRegs.GPASET.bit.GPIO23 = 1)	
//#define mTestSigLow()	  (GpioDataRegs.GPACLEAR.bit.GPIO23 = 1)


#define	mGet5msTiming()		CpuTimer0Regs.TCR.bit.TIF
#define	mClr5msTimFlag()	CpuTimer0Regs.TCR.bit.TIF = 1;

#define	mMdlProt_YellowLED()	(g_u16MdlStatus.bit.ACOV\
							 	|| g_u16MdlStatus.bit.ACUV\
							 	|| g_u16MdlStatus.bit.PFCOV\
							 	|| g_u16MdlStatus.bit.PFCUV\
							 	|| g_u16MdlStatus.bit.AMBIOT\
							 	|| g_u16MdlStatus.bit.RECTOT\
							 	|| g_u16MdlStatusExtend.bit.PFCOT\
							 	|| g_u16UnPlugFlag\
							 	|| (g_u16LimitStatus & POW_LIM)\
							 	|| (g_u16CurrUnBal >= TIME20S)\
							 	|| g_u16MdlCtrl.bit.SHORTRELAY\
							 	|| g_u16MdlStatusExtend.bit.PFCFAIL)
							 //	||(g_u32FailFlagPointer == RECPFCFAIL))

#define	mMdlFault_RedLed()		( g_u16MdlStatus.bit.DCOV\
								|| g_u16MdlStatusExtend.bit.HWHVSD\
								|| g_u16RunFlag.bit.HWHVSDDisp\
								|| g_u16MdlStatus.bit.HVSDLOCK\
								|| g_u16SafetyShortStatus\
								|| (g_u16CurrSerUnBal>=TIME20S)\
								|| g_u16MdlStatus.bit.IDERR\
								|| g_u16MdlCtrl.bit.EStopFlag\
								|| g_u16MdlStatusExtend.bit.DCFAULT\
								|| g_u16CustNumDisMatch)//paul-20220301
								//|| g_u16MdlStatusExtend.bit.DCFUSEBROKEN\
                                




//关PFCDC的条件	//PFC fail有另外的操作逻辑							 
#define	mMdlOff_PFCDC()			(g_u16MdlStatus.bit.HVSDLOCK\
								||g_u16MdlStatus.bit.ACOV\
								||g_u16MdlStatus.bit.ACUV\
								||g_u16MdlStatus.bit.FANFAIL\
								||g_u16MdlStatus.bit.RECTOT\
								||g_u16MdlStatus.bit.AMBIOT\
								||g_u16MdlStatus.bit.PFCOV\
								||g_u16MdlStatus.bit.PFCUV\
								||g_u16MdlCtrl.bit.SHORTRELAY\
								||g_u16MdlCtrl.bit.EStopFlag\
								|| g_u16MdlStatusExtend.bit.PFCOT\
								||((g_u16UnPlugFlag)&& (g_u16ActionReady > PFC_HWSOFT_START))\
								|| g_u16MdlStatusExtend.bit.DcInputAlarmStatus)
//关PFCDC后关风扇的条件
#define	mMdlOff_PFCDC_FanOff()		(   g_u16MdlStatus.bit.ACUV\
                                     || g_u16MdlStatus.bit.PFCUV\
                                     || g_u16MdlStatusExtend.bit.PFCFAIL\
	                                 || g_u16MdlStatus.bit.ACOV\
	                                 || g_u16MdlCtrl.bit.SHORTRELAY\
	                                 || g_u16MdlCtrl.bit.EStopFlag\
	                                 || g_u16MdlStatusExtend.bit.DcInputAlarmStatus\
	                                 || g_u16MdlStatus.bit.HVSDLOCK  )

//关PFCDC后不关风扇的条件								 
#define	mMdlOff_PFCDC_FanOn()		(    g_u16MdlStatus.bit.FANFAIL\
								     ||  g_u16MdlStatus.bit.RECTOT\
							         ||  g_u16MdlStatus.bit.AMBIOT\
								     ||  g_u16MdlStatus.bit.PFCOV\
								     ||  g_u16MdlStatusExtend.bit.PFCOT	)



#define	mMdlOff_DC()		((g_u16MdlStatus.bit.DCOV\
                             ||g_u16MdlStatusExtend.bit.HWHVSD\
                             ||g_u16MdlCtrl.bit.OFFCTRL)\
                             ||(g_u16OverVoltStatus == DC_OV_STATE_OV)\
                             ||(g_u16OverVoltStatus == DC_OV_STATE_DELAY_5S)\
                             ||g_u16CustNumDisMatch)//paul-20220301

							 
#define	mScuSetMdl_Off()		(g_u16MdlStatus.bit.HVSDLOCK\
								|| g_u16MdlStatus.bit.PFCOV\
								|| g_u16MdlStatus.bit.PFCUV\
								|| g_u16MdlStatus.bit.ACOV\
								|| g_u16MdlStatus.bit.ACUV\
								|| g_u16MdlStatus.bit.FANFAIL\
								|| g_u16MdlStatus.bit.AMBIOT\
								|| g_u16MdlStatus.bit.RECTOT\
								|| g_u16MdlStatus.bit.DCOV\
								||g_u16MdlStatusExtend.bit.HWHVSD\
								|| g_u16MdlStatusExtend.bit.PFCOT\
                                || g_u16UnPlugFlag\
								||g_u16SafetyShortStatus\
								||g_u16MdlStatusExtend.bit.DcInputAlarmStatus\
                                ||g_u16CustNumDisMatch)//paul-20220301


#define	mDC_TZ_FLG()           (   EPwm4Regs.TZFLG.bit.OST\
                                || EPwm5Regs.TZFLG.bit.OST\
		                        || EPwm6Regs.TZFLG.bit.OST\
		                        || EPwm7Regs.TZFLG.bit.OST  )
	



//PFCFAIL的判定条件

//如果软启动在第二步，AC电压大于80V，但PFC电压小于60V，说明母线采样有问题，报PFCFAIL
#define	mPFCSSFailCond1() (    (g_lq10AcVolt.lData >= VPFC_SSTART_VAC_LIM)\
                            && (g_lq10PfcVolt.lData <= VPFC_DIS_DN)   \
                            && (g_u16ActionReady == PFC_SWSOFT_START_INIT) ) 
                           
//如果软启动在第三步，驱动打开且发最大占空比了，AC采样电压和母线电压给定也正常
//但母线的采样电压却小于（软启动计算的母线电压门槛值）,认为PFC开关电路或采样有问题，报PFCFAIL
#define	mPFCSSFailCond2() (   (g_u16ActionReady == PFC_SWSOFT_START)\
                            &&( EPwm1Regs.AQCSFRC.all == 0x00 )\
                            &&(EPwm1Regs.CMPA.all == PFC_MAX_DUTY)\
							&&(g_lq10SetPfcVolt.lData >= VPFC_DEF_SET)\
							&&(IsrVars._lVpfcProtect.iData.iHD < VPFC_AD_DC_START )\
                            &&(g_lq10AcVolt.lData  >= VPFC_SSTART_VAC_LIM) ) 



#endif
/*inline void vi32MaxMinLimit(INT32 LimitSymbol, INT32 LimitMax, INT32 LimitMin)
{
    LimitSymbol= (LimitSymbol > LimitMax) ? LimitMax : LimitSymbol;
    LimitSymbol= (LimitSymbol < LimitMin) ? LimitMin : LimitSymbol;
}
*/
/*============================================================================*/


