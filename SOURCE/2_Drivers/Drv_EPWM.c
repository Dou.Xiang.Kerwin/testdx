
//#########################################################
//
// FILE:   f28004x_EPwm.c
//
// TITLE:  f28004x ePWM Initialization & Support Functions.
//
//#########################################################
// Running on TMS320LF280xA
// External clock is 20MHz, PLL * 10/2 , CPU-Clock 100 MHz
// Date: from 2005/12/28      , jurgen lv
// Version:1.00     Change Date:
//#########################################################

#include "f28004x_device.h"
#include "App_constant.h"
#include "Prj_macro.h"
#include  "Prj_config.h"

void InitEPwm1(void);
void InitEPwm2(void);
void InitEPwm3(void);
void InitEPwm4(void);
void InitEPwm5(void);
void InitEPwm6(void);
void InitEPwm7(void);
void InitEPwm8(void);
void InitADCTriger(void);
void InitTzAction(void);
void vInitPwmIO(void);


//---------------------------------------------------------------------------
// InitEPwm:
//---------------------------------------------------------------------------
// This function initializes the ePWM(s) to a known state.
//
void InitEPwm(void)
{

    vInitPwmIO();

    EALLOW;
    EPwm1Regs.AQSFRC.bit.RLDCSF = 0x03;  // 立即装载
    EPwm2Regs.AQSFRC.bit.RLDCSF = 0x03;  // 立即装载
    EPwm3Regs.AQSFRC.bit.RLDCSF = 0x03;  // 立即装载
    EPwm4Regs.AQSFRC.bit.RLDCSF = 0x03;  // 立即装载
    EPwm5Regs.AQSFRC.bit.RLDCSF = 0x03;  // 立即装载
    EPwm6Regs.AQSFRC.bit.RLDCSF = 0x03;  // 立即装载
    EPwm7Regs.AQSFRC.bit.RLDCSF = 0x03;  // 立即装载

    //EPWM4作为计算中断频率,采样,不必要设置强制低立即装载模式
    mOffDcdcPwm();
    mOffPfcPwm();

    mTBClockOff();      // Stop all the TB clocks
    //InitTzAction();

    InitEPwm1();        //  EPWM1 for PFC Switch
    InitEPwm2();        //  EPWM2 NO USE
    InitEPwm3();        //  EPWM3 for PFC Switch

/*************************************************************
//--------------------------.--------------------------------//
//               PWM4       .       PWM6
//                          .
//             ............................
//                          .
//               PWM5       .       PWM7
//                          .
//-----------------------------------------------------------//
*************************************************************/
    InitEPwm4();        //  EPWM3 for DCDC Switch
    InitEPwm5();        //  EPWM4 for DCDC Switch
    InitEPwm6();        //  EPWM5 for DCDC Switch
    InitEPwm7();        //  EPWM6 for DCDC Switch  & CLA

    InitEPwm8();        //  EPWM7 for ISR
    InitADCTriger();    //for ADC trigger source

    InitTzAction();  //tyang

    mTBClockOn();       // Start all the TB clocks
    EPwm1Regs.TBCTL.bit.SWFSYNC = 1; //软件同步一次PFC的各PWM开关管,1为master
    EPwm4Regs.TBCTL.bit.SWFSYNC = 1; //软件同步一次DC的各PWM开关管,4为master
    EDIS;
}

//---------------------------------------------------------------------------
// Init  EPwm1:
//---------------------------------------------------------------------------
void InitEPwm1(void)
{
    EPwm1Regs.TBPRD = PFC_PRD_DEF;
    EPwm1Regs.TBPHS.bit.TBPHS = 6665;
    EPwm1Regs.TBCTL.bit.PHSEN = TB_ENABLE;
//  EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE;//TB_CTR_ZERO; //  sync pass through //zero sync
    EPwm1Regs.TBCTL.bit.SYNCOSEL = TB_CTR_ZERO; //  sync pass through //zero sync
    EPwm1Regs.TBCTL.bit.PHSDIR = TB_UP;         // Phase Direction

    EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP; // Count up
    EPwm1Regs.TBCTL.bit.PRDLD = TB_SHADOW;
    EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;      // Timebase clock pre-scale
    EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;   // High speed time pre-scale

    EPwm1Regs.CMPA.bit.CMPA =  1;
    EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_PRD;
    EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;

    //EPwm1Regs.CMPB.bit.CMPB=  PFC_PRD_DEF - 200;
    EPwm1Regs.CMPB.bit.CMPB=  PFC_PRD_DEF - 3166;
    EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_PRD;
    EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;

    //正半周，EPWM1上管，作为整流管，上管占空比最大
    EPwm1Regs.AQCTLA.bit.ZRO =  AQ_SET;
    EPwm1Regs.AQCTLA.bit.CAU =  AQ_CLEAR;
    EPwm1Regs.AQCTLA.bit.CBU =  AQ_CLEAR;

    //Setup Deadband
    EPwm1Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;  //S1=0,S0=0

    EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;   // zero int
    //001: Enable event time-base counter equal to zero. (TBCTR = 0x00)
    EPwm1Regs.ETSEL.bit.INTEN = 0;              // Disable INT
    EPwm1Regs.ETPS.bit.INTPRD = ET_1ST;         // Generate INT on 1st event
    //01: Generate an interrupt on the first event INTCNT = 01 (first event)
    EPwm1Regs.ETCLR.all = 0x000F ;              // clear all ETFLAG

    //PWM1 config for COMP1A sync
    //PWM1A同步配置，同步信号来源于DC事件A->TRIP4->CMPSS4_H->即_VDSVALLEY1信号
    //PWM1A TZ信号来源于DC事件B->TRIP9->CMPSS5.CTRIPH_OR_CTRIPL->即_PFCCURR1 信号

    //20200327 for PFCOVPTZ
    EPwm1Regs.DCTRIPSEL.bit.DCAHCOMPSEL = DC_TRIPIN10; //DCAH = DC_TRIPIN10
    EPwm1Regs.TZDCSEL.bit.DCAEVT1 = TZ_DCAH_HI;
    EPwm1Regs.DCACTL.bit.EVT1SRCSEL = DC_EVT1;
    EPwm1Regs.DCACTL.bit.EVT1FRCSYNCSEL = DC_EVT_ASYNC;
    //PWM1 config for COMP1A sync//EPWM1 DC
    EPwm1Regs.DCTRIPSEL.bit.DCBHCOMPSEL = DC_TRIPIN4;//0x3 //DCAH=DC_TRIPIN4

    //qtest1202
    //EPwm1Regs.DCTRIPSEL.bit.DCBHCOMPSEL = DC_TRIPIN9;//0x8 //DCBH=DC_TRIPIN9
    EPwm1Regs.DCTRIPSEL.bit.DCALCOMPSEL = DC_TRIPIN9;//0x8 //DCAL=DC_TRIPIN9

    EPwm1Regs.TZDCSEL.bit.DCBEVT1 = TZ_DCBH_LOW;

    //qtest1202
    //EPwm1Regs.TZDCSEL.bit.DCBEVT2 = TZ_DCBH_LOW;
    EPwm1Regs.TZDCSEL.bit.DCAEVT2 = TZ_DCAL_HI;//100: DCAL = high, DCAH = don't care

    EPwm1Regs.DCBCTL.bit.EVT1SRCSEL = DC_EVT_FLT;//DC_EVT1;

    //qtest1202
    //EPwm1Regs.DCBCTL.bit.EVT2SRCSEL = DC_EVT2;
    EPwm1Regs.DCACTL.bit.EVT2SRCSEL = DC_EVT2;//0: Source Is DCAEVT2 Signal
                                              //1: Source Is DCEVTFILT Signal
    EPwm1Regs.DCBCTL.bit.EVT1SYNCE = SYNC_ENABLE;

    //qtest1202
    //EPwm1Regs.DCBCTL.bit.EVT2FRCSYNCSEL = DC_EVT_ASYNC;
    EPwm1Regs.DCACTL.bit.EVT2FRCSYNCSEL = DC_EVT_ASYNC;


    //暂时不用blankwindow 滤波
    EPwm1Regs.DCFCTL.bit.PULSESEL = DC_PULSESEL_PRD;//DC_PULSESEL_ZERO;//0x1     1: Time-base counter equal to zero (TBCTR = 0x00)
    EPwm1Regs.DCFCTL.bit.BLANKE = DC_BLANK_ENABLE;//0x1     1: Blanking window is enabled
    //EPwm1Regs.DCFCTL.bit.SRCSEL = DC_SRC_DCBEVT1;//0x2
    //20200226
    EPwm1Regs.DCFCTL.bit.SRCSEL = DC_SRC_DCBEVT1;//0x2         10: Source Is DCBEVT1 Signal
    //EPwm1Regs.DCFCTL.bit.SRCSEL = DC_SRC_DCAEVT2;//0x1
    EPwm1Regs.DCFOFFSET = 0;//偏移量为0
    EPwm1Regs.DCFWINDOW = 0;//250;//250=2.5us


    EPwm1Regs.TZCTL.bit.DCAEVT1 = TZ_NO_CHANGE;//0x3 Do Nothing,trip action is disabled
    EPwm1Regs.TZCTL.bit.DCAEVT2 = TZ_NO_CHANGE;//0x3 Do Nothing,trip action is disabled
    EPwm1Regs.TZCTL.bit.DCBEVT1 = TZ_NO_CHANGE;//0x3 Do Nothing,trip action is disabled
    EPwm1Regs.TZCTL.bit.DCBEVT2 = TZ_NO_CHANGE;//0x3 Do Nothing,trip action is disabled
    EPwm1Regs.TZCTL.bit.TZA = TZ_FORCE_LO;     // EPWMxA will go low



}

//---------------------------------------------------------------------------
// Init  EPwm2:
//---------------------------------------------------------------------------
void InitEPwm2(void)
{
    EPwm2Regs.TBPRD = PFC_PRD_DEF;
    EPwm2Regs.TBPHS.bit.TBPHS = PFC_PRD_DEF/2 ; ;
    EPwm2Regs.TBCTL.bit.PHSEN = TB_ENABLE;
    EPwm2Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE; // not Pass through
    EPwm2Regs.TBCTL.bit.PHSDIR = TB_UP;             // Phase Direction

    EPwm2Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;      // Count up
    EPwm2Regs.TBCTL.bit.PRDLD = TB_SHADOW;
    EPwm2Regs.TBCTL.bit.CLKDIV = TB_DIV1;           // Timebase clock pre-scale
    EPwm2Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;        // High speed time pre-scale

    EPwm2Regs.CMPA.bit.CMPA =  1;
    EPwm2Regs.CMPCTL.bit.LOADAMODE = CC_CTR_PRD;
    EPwm2Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm2Regs.CMPB.bit.CMPB =  PFC_PRD_DEF - 200;
    EPwm2Regs.CMPCTL.bit.LOADBMODE = CC_CTR_PRD;
    EPwm2Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;

    //正半周，EPWM1上管，作为整流管，上管占空比最大
    EPwm2Regs.AQCTLA.bit.ZRO =  AQ_SET;
    EPwm2Regs.AQCTLA.bit.CAU =  AQ_CLEAR;
    EPwm2Regs.AQCTLA.bit.CBU =  AQ_CLEAR;

    //Setup Deadband
    EPwm2Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;//S1=1,S0=1

    EPwm2Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;   // zero int
    EPwm2Regs.ETSEL.bit.INTEN = 0;              // Disable INT
    EPwm2Regs.ETPS.bit.INTPRD = ET_1ST;         // Generate INT on 1st event
    EPwm2Regs.ETCLR.all = 0x000F ;              // clear all ETFLAG
}

//---------------------------------------------------------------------------
// Init  EPwm3:
//---------------------------------------------------------------------------
void InitEPwm3(void)
{
    EPwm3Regs.TBPRD = PFC_PRD_DEF;
    //EPwm3Regs.TBPHS.bit.TBPHS = PFC_PRD_DEF/2 ;
    EPwm3Regs.TBPHS.bit.TBPHS = 6665 ;
    EPwm3Regs.TBCTL.bit.PHSEN = TB_ENABLE;
    EPwm3Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_DISABLE; // not Pass through
    EPwm3Regs.TBCTL.bit.PHSDIR = TB_UP;             // Phase Direction

    EPwm3Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP;      // Count up
    EPwm3Regs.TBCTL.bit.PRDLD = TB_SHADOW;
    EPwm3Regs.TBCTL.bit.CLKDIV = TB_DIV1;           // Timebase clock pre-scale
    EPwm3Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;        // High speed time pre-scale

    EPwm3Regs.CMPA.bit.CMPA =  1;
    EPwm3Regs.CMPCTL.bit.LOADAMODE = CC_CTR_PRD;
    EPwm3Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    //EPwm3Regs.CMPB.bit.CMPB =  PFC_PRD_DEF - 200;
    EPwm3Regs.CMPB.bit.CMPB =  PFC_PRD_DEF - 3166;// max35us
    EPwm3Regs.CMPCTL.bit.LOADBMODE = CC_CTR_PRD;
    EPwm3Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;

    //正半周，EPWM1上管，作为整流管，上管占空比最大
    EPwm3Regs.AQCTLA.bit.ZRO =  AQ_SET;
    EPwm3Regs.AQCTLA.bit.CAU =  AQ_CLEAR;
    EPwm3Regs.AQCTLA.bit.CBU =  AQ_CLEAR;

    //Setup Deadband
    EPwm3Regs.DBCTL.bit.OUT_MODE = DB_DISABLE;//S1=1,S0=1

    EPwm3Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;   // zero int
    EPwm3Regs.ETSEL.bit.INTEN = 0;              // Disable INT
    EPwm3Regs.ETPS.bit.INTPRD = ET_1ST;         // Generate INT on 1st event
    EPwm3Regs.ETCLR.all = 0x000F ;              // clear all ETFLAG

    //PWM3A同步配置，同步信号来源于DC事件A->TRIP5->CMPSS2_H->即_VDSVALLEY2信号
    //PWM3A TZ信号来源于DC事件B->TRIP8->CMPSS3.CTRIPH_OR_CTRIPL->即_PFCCURR2 信号

    //20200327 for PFCOVPTZ
    EPwm3Regs.DCTRIPSEL.bit.DCAHCOMPSEL = DC_TRIPIN10; //DCAH = DC_TRIPIN10
    EPwm3Regs.TZDCSEL.bit.DCAEVT1 = TZ_DCAH_HI;
    EPwm3Regs.DCACTL.bit.EVT1SRCSEL = DC_EVT1;
    EPwm3Regs.DCACTL.bit.EVT1FRCSYNCSEL = DC_EVT_ASYNC;

    //PWM3 config for COMP3A sync
    EPwm3Regs.DCTRIPSEL.bit.DCBHCOMPSEL = DC_TRIPIN5;//0x4 //DCAH=DC_TRIPIN5

    //qtest1202
    //EPwm3Regs.DCTRIPSEL.bit.DCBHCOMPSEL = DC_TRIPIN8;//0x7 //DCBH=DC_TRIPIN8
    EPwm3Regs.DCTRIPSEL.bit.DCALCOMPSEL = DC_TRIPIN8;//0x7 //DCBH=DC_TRIPIN8

    EPwm3Regs.TZDCSEL.bit.DCBEVT1 = TZ_DCBH_LOW;

    //qtest1202
    //EPwm3Regs.TZDCSEL.bit.DCAEVT2 = TZ_DCBH_LOW;
    EPwm3Regs.TZDCSEL.bit.DCAEVT2 = TZ_DCAL_HI;

    EPwm3Regs.DCBCTL.bit.EVT1SRCSEL = DC_EVT_FLT;//DC_EVT1;

    //qtest1202
    //EPwm3Regs.DCBCTL.bit.EVT2SRCSEL = DC_EVT2;
    EPwm3Regs.DCACTL.bit.EVT2SRCSEL = DC_EVT2;

    EPwm3Regs.DCBCTL.bit.EVT1SYNCE = SYNC_ENABLE;

    //qtest1202
    //EPwm3Regs.DCBCTL.bit.EVT2FRCSYNCSEL = DC_EVT_ASYNC;
    EPwm3Regs.DCACTL.bit.EVT2FRCSYNCSEL = DC_EVT_ASYNC;

    //暂时不用blankwindow 滤波
    EPwm3Regs.DCFCTL.bit.PULSESEL = DC_PULSESEL_ZERO;//0x1
    EPwm3Regs.DCFCTL.bit.BLANKE = DC_BLANK_ENABLE;//0x1
    EPwm3Regs.DCFCTL.bit.SRCSEL = DC_SRC_DCBEVT1;//0x2
    EPwm3Regs.DCFOFFSET = 0;
    EPwm3Regs.DCFWINDOW = 0;//240;


    EPwm3Regs.TZCTL.bit.DCAEVT1 = TZ_NO_CHANGE;//0x3 Do Nothing,trip action is disabled
    EPwm3Regs.TZCTL.bit.DCAEVT2 = TZ_NO_CHANGE;//0x3 Do Nothing,trip action is disabled
    EPwm3Regs.TZCTL.bit.DCBEVT1 = TZ_NO_CHANGE;//0x3 Do Nothing,trip action is disabled
    EPwm3Regs.TZCTL.bit.DCBEVT2 = TZ_NO_CHANGE;//0x3 Do Nothing,trip action is disabled
    EPwm3Regs.TZCTL.bit.TZA = TZ_FORCE_LO;     // EPWMxA will go low

}

//---------------------------------------------------------------------------
// Init  EPwm4:
//---------------------------------------------------------------------------
void InitEPwm4(void)
{
    // EPwm4 config config for DCPWM_A
    EPwm4Regs.TBPRD = DC_TIMER_TBPRD;
    EPwm4Regs.TBPHS.bit.TBPHS =1;
    EPwm4Regs.TBCTL.bit.PHSEN = TB_ENABLE;//TB_ENABLE;//lzy for TBPRDHR
    EPwm4Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;  // Pass through
    EPwm4Regs.TBCTL.bit.PHSDIR = TB_UP;     //Phase Direction Down

    EPwm4Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;  // up/down Count
    EPwm4Regs.TBCTL.bit.PRDLD = TB_SHADOW;
    //TBCLK=SYSCLKOUT/(HSPCLKDIV * CLKDIV)=SYSCLKOUT
    EPwm4Regs.TBCTL.bit.CLKDIV = TB_DIV1;               //Timebase clock pre-scale
    EPwm4Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;            //High speed time pre-scale
    EPwm4Regs.ETSEL.bit.INTSEL = ET_CTR_PRD;            // Select INT on Prd event
    EPwm4Regs.ETSEL.bit.INTEN = 0;// Disable INT
    EPwm4Regs.ETPS.bit.INTPRD = ET_1ST;             // Generate INT on 1st event
    EPwm4Regs.ETCLR.all = 0x000F ;      //clear all ETFLAG

    EPwm4Regs.CMPA.bit.CMPA = DC_MIN_CMPA;
    EPwm4Regs.CMPB.bit.CMPB = DC_TIMER_TBPRD - DC_MIN_CMPA;
    EPwm4Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;//CC_CTR_ZERO_PRD;
    EPwm4Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm4Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;//CC_CTR_ZERO_PRD;
    EPwm4Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;

    EPwm4Regs.CMPCTL2.bit.LOADCMODE = CC_CTR_ZERO;
    EPwm4Regs.CMPCTL2.bit.SHDWCMODE = CC_SHADOW;

    EPwm4Regs.AQCTLA.bit.CAU = AQ_CLEAR;
    EPwm4Regs.AQCTLA.bit.CAD = AQ_SET;


    EPwm4Regs.AQCTLB.bit.PRD = AQ_SET;
    EPwm4Regs.AQCTLB.bit.ZRO = AQ_CLEAR;

    //Setup Deadband  20100512 cxm
    EPwm4Regs.DBCTL.bit.OUT_MODE = DBA_ENABLE;//S1=1,S0=0
    EPwm4Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;//S3=0,S2=0
    EPwm4Regs.DBCTL.bit.IN_MODE = DBA_RED_DBB_FED;//S5=1,S4=0
    EPwm4Regs.DBRED.bit.DBRED = DC_DB_TIME;//600NS;


    EPwm4Regs.HRCNFG.all = 0x0;//lzy
    EPwm4Regs.HRCNFG.bit.HRLOAD = HR_CTR_ZERO;//HR_CTR_ZERO_PRD;
    EPwm4Regs.HRCNFG.bit.CTLMODE = HR_CMP;
    EPwm4Regs.HRCNFG.bit.EDGMODE = HR_BEP;//rising and falling edge

    EPwm4Regs.HRCNFG.bit.AUTOCONV = AUTOCONV_AUTO;  // Enable autoconversion
    EPwm4Regs.HRPCTL.bit.HRPE = 0x01; // Turn on high-resolution period control.
    EPwm4Regs.HRPCTL.bit.TBPHSHRLOADE = 1;

    //20200327 for PFCOVPTZ
    EPwm4Regs.DCAHTRIPSEL.bit.TRIPINPUT7 = 1;
    EPwm4Regs.DCAHTRIPSEL.bit.TRIPINPUT10 = 1;
    //EPwm4Regs.DCTRIPSEL.bit.DCAHCOMPSEL = DC_TRIPIN7; //DCAH = DC_TRIPIN7
    EPwm4Regs.DCTRIPSEL.bit.DCAHCOMPSEL = DC_COMBINATION; //DCAH = DC_TRIPIN10 or DC_TRIPIN7

    EPwm4Regs.TZDCSEL.bit.DCAEVT1 = TZ_DCAH_HI;
    EPwm4Regs.DCACTL.bit.EVT1SRCSEL = DC_EVT1;
    EPwm4Regs.DCACTL.bit.EVT1FRCSYNCSEL = DC_EVT_ASYNC;


}

//---------------------------------------------------------------------------
// Init  EPwm5:
//---------------------------------------------------------------------------
void InitEPwm5(void)
{
    // EPWM5 config for DCPWM_B
    EPwm5Regs.TBPRD = DC_TIMER_TBPRD;
    EPwm5Regs.TBPHS.bit.TBPHS = 2;
    EPwm5Regs.TBCTL.bit.PHSEN = TB_ENABLE;//TB_ENABLE;//lzy for TBPRDHR
    EPwm5Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;  // Pass through
    EPwm5Regs.TBCTL.bit.PHSDIR = TB_UP;     //Phase Direction Down

    EPwm5Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;  // up/down Count
    EPwm5Regs.TBCTL.bit.PRDLD = TB_SHADOW;
    //TBCLK=SYSCLKOUT/(HSPCLKDIV * CLKDIV)=SYSCLKOUT
    EPwm5Regs.TBCTL.bit.CLKDIV = TB_DIV1;               //Timebase clock pre-scale
    EPwm5Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;            //High speed time pre-scale
    //用固定的EPWM5的COMPB来触发DC中断
    EPwm5Regs.ETSEL.bit.INTSEL = ET_CTRU_CMPB;//ET_CTRU_CMPB;//ET_CTRD_CMPB;//ET_CTR_PRD;           // Select INT on Prd event

    EPwm5Regs.ETSEL.bit.INTEN = 0;// Disable INT
    EPwm5Regs.ETPS.bit.INTPRD = ET_1ST;             // Generate INT on 1st event
    EPwm5Regs.ETCLR.all = 0x000F ;      //clear all ETFLAG

    EPwm5Regs.CMPA.bit.CMPA = DC_TIMER_TBPRD - DC_MIN_CMPA;
    EPwm5Regs.CMPB.bit.CMPB = 4;//用于触发EPwm5中断

    EPwm5Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;//CC_CTR_ZERO_PRD;
    EPwm5Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;//CC_CTR_ZERO_PRD;
    EPwm5Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm5Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;

    EPwm5Regs.AQCTLA.bit.CAU = AQ_SET    ;//for Piccolo HRPWM
    EPwm5Regs.AQCTLA.bit.CAD = AQ_CLEAR;


    //Setup Deadband  20100512 cxm
    EPwm5Regs.DBCTL.bit.OUT_MODE = DBA_ENABLE;//S1=1,S0=0
    EPwm5Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;//S3=0,S2=0
    EPwm5Regs.DBCTL.bit.IN_MODE = DBA_RED_DBB_FED;//S5=1,S4=0
    EPwm5Regs.DBRED.bit.DBRED = DC_DB_TIME;//600NS;

    EPwm5Regs.HRCNFG.all = 0x0;//lzy
    EPwm5Regs.HRCNFG.bit.HRLOAD = HR_CTR_ZERO;//HR_CTR_ZERO_PRD;
    EPwm5Regs.HRCNFG.bit.CTLMODE = HR_CMP;
    EPwm5Regs.HRCNFG.bit.EDGMODE = HR_BEP;//rising and falling edge

    EPwm5Regs.HRCNFG.bit.AUTOCONV = AUTOCONV_AUTO;  // Enable autoconversion
    EPwm5Regs.HRPCTL.bit.HRPE = 0x01; // Turn on high-resolution period control.
    EPwm5Regs.HRPCTL.bit.TBPHSHRLOADE = 1;

    //20200327 for PFCOVPTZ
    EPwm5Regs.DCAHTRIPSEL.bit.TRIPINPUT7 = 1;
    EPwm5Regs.DCAHTRIPSEL.bit.TRIPINPUT10 = 1;
    //EPwm5Regs.DCTRIPSEL.bit.DCAHCOMPSEL = DC_TRIPIN7; //DCAH = DC_TRIPIN7
    EPwm5Regs.DCTRIPSEL.bit.DCAHCOMPSEL = DC_COMBINATION; //DCAH = DC_TRIPIN10 or DC_TRIPIN7
    EPwm5Regs.TZDCSEL.bit.DCAEVT1 = TZ_DCAH_HI;
    EPwm5Regs.DCACTL.bit.EVT1SRCSEL = DC_EVT1;
    EPwm5Regs.DCACTL.bit.EVT1FRCSYNCSEL = DC_EVT_ASYNC;

}


//---------------------------------------------------------------------------
// Init  EPwm6:
//---------------------------------------------------------------------------
void InitEPwm6(void)
{
    // EPWM6 config for DCPWM_C
    EPwm6Regs.TBPRD = DC_TIMER_TBPRD;
    EPwm6Regs.TBPHS.bit.TBPHS = 2;
    EPwm6Regs.TBCTL.bit.PHSEN = TB_ENABLE;//TB_ENABLE;//lzy for TBPRDHR
    EPwm6Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;  // Pass through
    EPwm6Regs.TBCTL.bit.PHSDIR = TB_UP;     //Phase Direction Down

    EPwm6Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;  // up/down Count
    EPwm6Regs.TBCTL.bit.PRDLD = TB_SHADOW;//use Shadow mode,when CTR=0,load prd
    //TBCLK=SYSCLKOUT/(HSPCLKDIV * CLKDIV)=SYSCLKOUT
    EPwm6Regs.TBCTL.bit.CLKDIV = TB_DIV1;               //Timebase clock pre-scale
    EPwm6Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;            //High speed time pre-scale
/***************************
   FOR DCDC Interrupt
  DC PWM 加载中断定义为过周期中断，
  这样才能保证PRD（PRD只能是过零加载）
  和COMP（为了高精度必须过零过周期加载）同时在过零的时候加载新的值
  在计算中断的末尾设置ETSEL和ETPS 使能DC PWM 加载中断
 以及允许1次过周期出现后中断，保证不会EPWM1挂起过周期变成过零
***************************************************************************/
    EPwm6Regs.ETSEL.bit.INTSEL = ET_CTR_PRD;            // Select INT on Prd event
    EPwm6Regs.ETSEL.bit.INTEN = 0;// Disable INT
    EPwm6Regs.ETPS.bit.INTPRD = ET_1ST;             // Generate INT on 1st event
    EPwm6Regs.ETCLR.all = 0x000F ;      //clear all ETFLAG

    EPwm6Regs.CMPA.bit.CMPA = DC_TIMER_TBPRD - DC_MIN_CMPA;
    EPwm6Regs.CMPB.bit.CMPB = DC_MIN_CMPA;       //xgh
    EPwm6Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;//CC_CTR_ZERO_PRD;
    EPwm6Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;//CC_CTR_ZERO_PRD;
    EPwm6Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm6Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;

    EPwm6Regs.AQCTLA.bit.CBU = AQ_SET;      //  更改上升沿高精度为下降沿高精度  lmh
    EPwm6Regs.AQCTLA.bit.CAD = AQ_CLEAR;   //   更改上升沿高精度为下降沿高精度 lmh

    //Setup Deadband  20100512 cxm
    EPwm6Regs.DBCTL.bit.OUT_MODE = DBA_ENABLE;//S1=1,S0=0
    EPwm6Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;//S3=0,S2=0
    EPwm6Regs.DBCTL.bit.IN_MODE = DBA_RED_DBB_FED;//S5=1,S4=0
    EPwm6Regs.DBRED.bit.DBRED = DC_DB_TIME;//600NS;

    EPwm6Regs.HRCNFG.all = 0x0;//lzy
    EPwm6Regs.HRCNFG.bit.HRLOAD = HR_CTR_ZERO;//HR_CTR_ZERO_PRD;
    EPwm6Regs.HRCNFG.bit.CTLMODE = HR_CMP;
    EPwm6Regs.HRCNFG.bit.EDGMODE = HR_BEP;//rising and falling edge

    EPwm6Regs.HRCNFG.bit.AUTOCONV = AUTOCONV_AUTO;  // Enable autoconversion
    EPwm6Regs.HRPCTL.bit.HRPE = 0x01; // Turn on high-resolution period control.
    EPwm6Regs.HRPCTL.bit.TBPHSHRLOADE = 1;

    //20200327 for PFCOVPTZ
    EPwm6Regs.DCAHTRIPSEL.bit.TRIPINPUT7 = 1;
    EPwm6Regs.DCAHTRIPSEL.bit.TRIPINPUT10 = 1;
    //EPwm6Regs.DCTRIPSEL.bit.DCAHCOMPSEL = DC_TRIPIN7; //DCAH = DC_TRIPIN7
    EPwm6Regs.DCTRIPSEL.bit.DCAHCOMPSEL = DC_COMBINATION; //DCAH = DC_TRIPIN10 or DC_TRIPIN7

    EPwm6Regs.TZDCSEL.bit.DCAEVT1 = TZ_DCAH_HI;
    EPwm6Regs.DCACTL.bit.EVT1SRCSEL = DC_EVT1;
    EPwm6Regs.DCACTL.bit.EVT1FRCSYNCSEL = DC_EVT_ASYNC;


}

//---------------------------------------------------------------------------
// Init  EPwm7:
//---------------------------------------------------------------------------
void InitEPwm7(void)
{
    // EPWM7 config config for DCPWM_D
    SyncSocRegs.SYNCSELECT.bit.EPWM7SYNCIN =1;
    EPwm7Regs.TBPRD = DC_TIMER_TBPRD;
    EPwm7Regs.TBPHS.bit.TBPHS = 2;
    EPwm7Regs.TBCTL.bit.PHSEN = TB_ENABLE;
    EPwm7Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;  // Pass through
    EPwm7Regs.TBCTL.bit.PHSDIR = TB_UP;//Phase Direction
    EPwm7Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;  // up/down Count
    EPwm7Regs.TBCTL.bit.PRDLD = TB_SHADOW;
    //TBPRD is loaded from its shadow register on Counter = 0 event (CTR_zero)
    //TBCLK=SYSCLKOUT/(HSPCLKDIV * CLKDIV)=SYSCLKOUT
    EPwm7Regs.TBCTL.bit.CLKDIV =  TB_DIV1;              //Timebase clock pre-scale
    EPwm7Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;            //High speed time pre-scale
    EPwm7Regs.ETSEL.bit.INTSEL = ET_CTR_PRD;            // Select INT on Zero event
    EPwm7Regs.ETSEL.bit.INTEN = 0;// Disable INT
    EPwm7Regs.ETPS.bit.INTPRD = ET_1ST;             // Generate INT on 1st event
    EPwm7Regs.ETCLR.all = 0x000F ;      //clear all ETFLAG

    EPwm7Regs.CMPA.bit.CMPA = DC_MIN_CMPA;
    EPwm7Regs.CMPB.bit.CMPB = DC_TIMER_TBPRD - DC_MIN_CMPA;
    EPwm7Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;//CC_CTR_ZERO_PRD;
    EPwm7Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;//CC_CTR_ZERO_PRD;
    EPwm7Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
    EPwm7Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;

    EPwm7Regs.AQCTLA.bit.CAU = AQ_CLEAR;    //更改上升沿高精度为下降沿高精度
    EPwm7Regs.AQCTLA.bit.CBD = AQ_SET;      //更改上升沿高精度为下降沿高精度


     //Setup Deadband  20100512 cxm
    EPwm7Regs.DBCTL.bit.OUT_MODE = DBA_ENABLE;//S1=1,S0=0
    EPwm7Regs.DBCTL.bit.POLSEL = DB_ACTV_HI;//S3=0,S2=0
    EPwm7Regs.DBCTL.bit.IN_MODE = DBA_RED_DBB_FED;//S5=1,S4=0
    EPwm7Regs.DBRED.bit.DBRED = DC_DB_TIME;//600NS;

    EPwm7Regs.HRCNFG.all = 0x0;//lzy
    EPwm7Regs.HRCNFG.bit.HRLOAD = HR_CTR_ZERO;//HR_CTR_ZERO_PRD;
    EPwm7Regs.HRCNFG.bit.CTLMODE = HR_CMP;
    EPwm7Regs.HRCNFG.bit.EDGMODE = HR_BEP;//rising and falling edge

    EPwm7Regs.HRCNFG.bit.AUTOCONV = AUTOCONV_AUTO;  // Enable autoconversion
    EPwm7Regs.HRPCTL.bit.HRPE=0x01; // Turn on high-resolution period control.
    EPwm7Regs.HRPCTL.bit.TBPHSHRLOADE = 1;

    //20200327 for PFCOVPTZ
    EPwm7Regs.DCAHTRIPSEL.bit.TRIPINPUT7 = 1;
    EPwm7Regs.DCAHTRIPSEL.bit.TRIPINPUT10 = 1;
    //EPwm7Regs.DCTRIPSEL.bit.DCAHCOMPSEL = DC_TRIPIN7; //DCAH = DC_TRIPIN7
    EPwm7Regs.DCTRIPSEL.bit.DCAHCOMPSEL = DC_COMBINATION; //DCAH = DC_TRIPIN10 or DC_TRIPIN7
    EPwm7Regs.TZDCSEL.bit.DCAEVT1 = TZ_DCAH_HI;
    EPwm7Regs.DCACTL.bit.EVT1SRCSEL = DC_EVT1;
    EPwm7Regs.DCACTL.bit.EVT1FRCSYNCSEL = DC_EVT_ASYNC;

}

//---------------------------------------------------------------------------
// Init  EPwm8: PWM8 作为ISR中断的触发源
//---------------------------------------------------------------------------
void InitEPwm8(void)
{
    // EPWM8 config Interrupt Frequency
    EPwm8Regs.TBPRD = ISR_PRD_140K;
   // EPwm8Regs.TBPRD = 714*2;

    EPwm8Regs.TBPHS.bit.TBPHS = 0;
    EPwm8Regs.TBCTL.bit.PHSEN = TB_DISABLE;
    EPwm8Regs.TBCTL.bit.SYNCOSEL = TB_SYNC_IN;      // Pass through
    EPwm8Regs.TBCTL.bit.PHSDIR = TB_UP;             //Phase Direction

    EPwm8Regs.TBCTL.bit.CTRMODE = TB_COUNT_UPDOWN;      // Count up
    //TBCLK=SYSCLKOUT/(HSPCLKDIV * CLKDIV)=SYSCLKOUT
    EPwm8Regs.TBCTL.bit.CLKDIV = TB_DIV1;           //Timebase clock pre-scale
    EPwm8Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;        //High speed time pre-scale

    // Select INT on ZERO&PRD WHEN Phase Direction DOWN
    EPwm8Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;       //ET_CTR_ZERO;

    EPwm8Regs.ETSEL.bit.INTEN = 0;                  // Disable INT
    EPwm8Regs.ETPS.bit.INTPRD = ET_1ST;             // Generate INT on 1st event
    EPwm8Regs.ETCLR.all = 0x000F ;                  //clear all ETFLAG

 }
//---------------------------------------------------------------------------
// for ADC trigger source
//---------------------------------------------------------------------------
void InitADCTriger(void)
{
    EPwm4Regs.CMPB.bit.CMPB = 1;
    EPwm4Regs.ETSEL.bit.SOCAEN = 1;
    EPwm4Regs.ETSEL.bit.SOCASEL = ET_CTRU_CMPB;//ET_CTR_ZERO;//ET_CTRD_CMPB;//ET_CTRU_CMPB;//ET_CTR_ZERO;//ET_CTR_PRDZERO;////ET_CTR_ZERO; DQA 20121226
    EPwm4Regs.ETPS.bit.SOCAPRD = ET_1ST;

    EPwm4Regs.CMPC= 1;
    EPwm4Regs.ETSEL.bit.SOCBEN = 1;
    EPwm4Regs.ETSEL.bit.SOCBSELCMP=1;//cmpc/cmpd
    EPwm4Regs.ETSEL.bit.SOCBSEL = ET_CTRU_CMPA;//ET_CTR_ZERO;//ET_CTRD_CMPB;//ET_CTRU_CMPB;//ET_CTR_ZERO;//ET_CTR_PRDZERO;////ET_CTR_ZERO; DQA 20121226
    EPwm4Regs.ETPS.bit.SOCBPRD = ET_1ST;



    //EPwm6Regs.CMPC = 1;
    //EPwm6Regs.ETSEL.bit.SOCAEN = 1;
    //EPwm6Regs.ETSEL.bit.SOCASELCMP=1;//cmpc/cmpd
    //EPwm6Regs.ETSEL.bit.SOCASEL = ET_CTRU_CMPA;
    //EPwm6Regs.ETPS.bit.SOCAPRD = ET_1ST;


    EPwm8Regs.ETSEL.bit.SOCAEN = 1;
    EPwm8Regs.ETSEL.bit.SOCASEL = ET_CTR_ZERO;
    EPwm8Regs.ETPS.bit.SOCAPRD = ET_1ST;
}

void InitTzAction(void)
{
    // gqian
    EPwm1Regs.TZSEL.bit.DCAEVT2 = TZ_ENABLE;  //Enable TZ3 as a CBC trip source
    EPwm1Regs.TZCTL.bit.DCAEVT2 = TZ_FORCE_LO;// EPWMxA will go low
    EPwm1Regs.TZEINT.bit.CBC = 0;   //disable interrupt

    EPwm3Regs.TZSEL.bit.DCAEVT2 = TZ_ENABLE;  //Enable TZ3 as a CBC trip source
    EPwm3Regs.TZCTL.bit.DCAEVT2 = TZ_FORCE_LO;// EPWMxA will go low
    EPwm3Regs.TZEINT.bit.CBC = 0;   //disable interrupt

    //20200327 for PFCOVPTZ
    EPwm1Regs.TZSEL.bit.DCAEVT1 = TZ_ENABLE;
    EPwm1Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
    EPwm1Regs.TZCLR.all = 0x0007; //clear all TZFLAG
    EPwm1Regs.TZEINT.bit.OST = 0;   //disable interrupt

    EPwm3Regs.TZSEL.bit.DCAEVT1 = TZ_ENABLE;
    EPwm3Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
    EPwm3Regs.TZCLR.all = 0x0007; //clear all TZFLAG
    EPwm3Regs.TZEINT.bit.OST = 0;   //disable interrupt

    // for DCDC ocp  tyang
    EPwm4Regs.TZSEL.bit.DCAEVT1 = TZ_ENABLE;
    EPwm4Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
    EPwm4Regs.TZCLR.all = 0x0007; //clear all TZFLAG
    EPwm4Regs.TZEINT.bit.OST = 0;   //disable interrupt

    EPwm5Regs.TZSEL.bit.DCAEVT1 = TZ_ENABLE;
    EPwm5Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
    EPwm5Regs.TZEINT.bit.OST = 0;   //disable interrupt

    EPwm6Regs.TZSEL.bit.DCAEVT1 = TZ_ENABLE;
    EPwm6Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
    EPwm6Regs.TZEINT.bit.OST = 0;   //disable interrupt

    EPwm7Regs.TZSEL.bit.DCAEVT1 = TZ_ENABLE;
    EPwm7Regs.TZCTL.bit.TZA = TZ_FORCE_LO;
    EPwm7Regs.TZEINT.bit.OST = 0;   //disable interrupt

}

void vInitPwmIO(void)
{
    // Initialize PFC PWM1,PWM3,DCDC PWM4~PWM7
    // GPIO0 is set to EPWM1A
    GPIO_SetupPinMux(0U, GPIO_MUX_CPU1, EPWM_OUTPUT);
    GPIO_SetupPinOptions(0U, GPIO_OUTPUT, GPIO_PUSHPULL);

    // GPIO4 is set to EPWM3A
    GPIO_SetupPinMux(4U, GPIO_MUX_CPU1, EPWM_OUTPUT);
    GPIO_SetupPinOptions(4U, GPIO_OUTPUT, GPIO_PUSHPULL);

    // GPIO6 is set to EPWM4A
    GPIO_SetupPinMux(6U, GPIO_MUX_CPU1, EPWM_OUTPUT);
    GPIO_SetupPinOptions(6U, GPIO_OUTPUT, GPIO_PUSHPULL);

    // GPIO8 is set to EPWM5A
    GPIO_SetupPinMux(8U, GPIO_MUX_CPU1, EPWM_OUTPUT);
    GPIO_SetupPinOptions(8U, GPIO_OUTPUT, GPIO_PUSHPULL);

    // GPIO10 is set to EPWM6A
    GPIO_SetupPinMux(10U, GPIO_MUX_CPU1, EPWM_OUTPUT);
    GPIO_SetupPinOptions(10U, GPIO_OUTPUT, GPIO_PUSHPULL);

    // GPIO28 is set to EPWM7A
    GPIO_SetupPinMux(28U, GPIO_MUX_CPU1, 0X03);
    GPIO_SetupPinOptions(28U, GPIO_OUTPUT, GPIO_PUSHPULL);
}


//===========================================================================
// End of file.
//===========================================================================
















































