//###########################################################################
//
// FILE:   f28004x_ecap.c
//
// TITLE:  f28004x eCAP Initialization & Support Functions.
//
//###########################################################################
// $TI Release: f28004x Support Library v3.06.00.00 $
// $Release Date: Mon May 27 06:51:04 CDT 2019 $
// $Copyright:
// Copyright (C) 2014-2019 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Texas Instruments Incorporated nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
// LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// $
//###########################################################################

//
// Included Files
//
#include "f28004x_device.h"
#include "App_constant.h"
#include "Prj_config.h"

void InitECapGpio();
void InitECap1Gpio(Uint16 pin);
void InitECap2Gpio(Uint16 pin);
void InitAPwm3Gpio();
//
// InitECapture - Initialize ECAP1 configurations
//
void InitECapture()
{
    InitECapGpio();

    EALLOW;

    SyncSocRegs.SYNCSELECT.bit.ECAP1SYNCIN = 0; //000: EPWM1SYNCOUT selected
    //同步信号输入或软件同步功能，这个信号是同时输入到四个ECAP模块的。不是级联的方式。
    //测量相位用
    //Ecap1 config - ECAP mode
    //qtest0805
    //Ecap1改成抓output8 X-bar的数据
    GpioCtrlRegs.GPAINV.bit.GPIO4 = 0;
    GpioCtrlRegs.GPACTRL.bit.QUALPRD0 = 0;
    GpioCtrlRegs.GPAQSEL1.bit.GPIO4 = 2;   // 6-sample qualification for GPIO4 (EPWM3)
    ECap1Regs.ECCTL0.bit.INPUTSEL = ECAPIN_XBARINPUT8;       //INPUT XBAR-8  =》 PWM3A
    //ECap1Regs.ECCTL0.bit.INPUTSEL = 0x1F; //OUTPUT8-ECAP 对应的是31

    ECap1Regs.ECEINT.all = 0x0000;          // Disable all capture __interrupts
    ECap1Regs.ECCLR.all = 0xFFFF;           // Clear all CAP __interrupt flags
    ECap1Regs.ECCTL1.bit.CAPLDEN = EC_DISABLE;       // Disable CAP1-CAP4 register loads
    ECap1Regs.ECCTL2.bit.TSCTRSTOP = EC_FREEZE;     // Make sure the counter is stopped
    ECap1Regs.CTRPHS = 0;

    ECap1Regs.ECCTL1.bit.PRESCALE = EC_DIV1; // every time trig
    //0h (R/W) = Operate in continuous mode
    //1h (R/W) = Operate in one-Shot mode
    ECap1Regs.ECCTL2.bit.CONT_ONESHT = EC_CONTINUOUS;//=0;Operate in continuous mode
    //0h (R/W) = Operate in continuous mode
    //1h (R/W) = Operate in one-Shot mode
    ECap1Regs.ECCTL2.bit.STOP_WRAP = EC_EVENT1;         // WRAP after Caputure Event1
    //以单发模式捕获事件1之后停止,在连续模式下捕获事件1之后换行。

    //qtest0805
    //要改成抓下降沿，下降沿
    //ECap1Regs.ECCTL1.bit.CAP1POL = EC_FALLING;           // Rising edge 上升沿捕捉
    ECap1Regs.ECCTL1.bit.CAP1POL = EC_RISING;           // Rising edge 上升沿捕捉

    //ECap1Regs.ECCTL1.bit.CAP2POL = EC_FALLING;        // Falling edge
 //   ECap1Regs.ECCTL1.bit.CAP3POL = EC_RISING;         // Rising edge

    ECap1Regs.ECCTL1.bit.CTRRST1 = EC_ABS_MODE;       // Reset counter after latch 复位寄存器(差异模式)
   // ECap1Regs.ECCTL1.bit.CTRRST2 = EC_DELTA_MODE;       // Do not reset counter after
  //  ECap1Regs.ECCTL1.bit.CTRRST3 = EC_DELTA_MODE;     // Reset counter after latch

    ECap1Regs.ECCTL2.bit.SYNCI_EN = EC_SYNCI_ENABLE;   // Enable sync in
   // 0h (R/W) = Disable sync-in option
   // 1h (R/W) = Enable counter (TSCTR) to be loaded from CTRPHS
   // register upon either a SYNCI signal or a S/W force event.
    //ECap1Regs.ECCTL2.bit.SYNCO_SEL = EC_SYNCO_DIS;      // Disable sync out
    ECap1Regs.ECCTL2.bit.SYNCO_SEL = EC_SYNCIN;      // Ensable sync out
   //0h (R/W) = Select sync-in event to be the sync-out signal pass through
    ECap1Regs.ECCTL2.bit.CAP_APWM = EC_CAP_MODE;

    ECap1Regs.ECCTL2.bit.TSCTRSTOP = EC_RUN;            // Start Counter 启动计数器
    ECap1Regs.ECCTL2.bit.REARM = EC_ENABLE;             // arm one-shot
    ECap1Regs.ECCTL1.bit.CAPLDEN = EC_ENABLE;           // Enable CAP1-CAP4 register loads

    //测量周期用
    //Ecap2 config  --  ECAP mode
    //qtest0723
    //ECap2Regs.ECCTL0.bit.INPUTSEL = ECAPIN_XBARINPUT7;      //INPUT XBAR-7  =》 PWM1A
    ECap2Regs.ECCTL0.bit.INPUTSEL = 0x1E; //OUTPUT7-ECAP 对应的是30
    SyncSocRegs.SYNCSELECT.bit.SYNCOUT = 0;

    OutputXbarRegs.OUTPUT7MUX0TO15CFG.bit.MUX14 = 3;  // SYNCOUT->OutputXbar7
    OutputXbarRegs.OUTPUT7MUXENABLE.bit.MUX14 = 1; //1:enable 0:disable

    ECap2Regs.ECEINT.all = 0x0000;          // Disable all capture __interrupts
    ECap2Regs.ECCLR.all = 0xFFFF;           // Clear all CAP __interrupt flags
    ECap2Regs.ECCTL1.bit.CAPLDEN = EC_DISABLE;       // Disable CAP1-CAP4 register loads
    ECap2Regs.ECCTL2.bit.TSCTRSTOP = EC_FREEZE;     // Make sure the counter is stopped
    ECap2Regs.CTRPHS = 0;//YQH test
    ECap2Regs.ECCTL1.bit.PRESCALE = EC_DIV1; // every time trig
    //0h (R/W) = Operate in continuous mode
    //1h (R/W) = Operate in one-Shot mode
    ECap2Regs.ECCTL2.bit.CONT_ONESHT = EC_CONTINUOUS;
    ECap2Regs.ECCTL2.bit.STOP_WRAP = EC_EVENT1;         // WRAP after Caputure Event1

    ECap2Regs.ECCTL1.bit.CAP1POL = EC_RISING;           // Rising edge
    //ECap2Regs.ECCTL1.bit.CAP2POL = EC_FALLING;        // Falling edge
    //ECap2Regs.ECCTL1.bit.CAP3POL = EC_RISING;         // Rising edge

    ECap2Regs.ECCTL1.bit.CTRRST1 = EC_DELTA_MODE;       // Reset counter after latch
    //ECap2Regs.ECCTL1.bit.CTRRST2 = EC_ABS_MODE;       // Do not reset counter after
    //ECap2Regs.ECCTL1.bit.CTRRST3 = EC_DELTA_MODE;     // Reset counter after latch

    ECap2Regs.ECCTL2.bit.SYNCI_EN = EC_SYNCI_DISABLE;   // Disable sync in
    //ECap2Regs.ECCTL2.bit.SYNCI_EN = EC_SYNCI_ENABLE;   // Enable sync in //YQH test
    // 0h (R/W) = Disable sync-in option
    // 1h (R/W) = Enable counter (TSCTR) to be loaded from CTRPHS
    // register upon either a SYNCI signal or a S/W force event.
    ECap2Regs.ECCTL2.bit.SYNCO_SEL = EC_SYNCO_DIS;      // Pass through
    ECap2Regs.ECCTL2.bit.CAP_APWM = EC_CAP_MODE;

    ECap2Regs.ECCTL2.bit.TSCTRSTOP = EC_RUN;            // Start Counter
    ECap2Regs.ECCTL2.bit.REARM = EC_ENABLE;             // arm one-shot
    ECap2Regs.ECCTL1.bit.CAPLDEN = EC_ENABLE;           // Enable CAP1-CAP4 register loads

    //Ecap3 config  --  APWM mode
    ECap3Regs.ECCTL2.bit.CAP_APWM = EC_APWM_MODE;// Enable APWM mode
    ECap3Regs.ECCTL2.bit.APWMPOL = 1;//Output is active low ,comp值越大，占空比越小
    ECap3Regs.CAP1 = FAN_PWM_PRD;
    ECap3Regs.CAP2 = FAN_CMP_MIN;           // Set Compare value
    ECap3Regs.CAP3 = FAN_PWM_PRD;           // shadow mode
    ECap3Regs.CAP4 = FAN_CMP_MIN;           // shadow mode
    ECap3Regs.ECCTL2.bit.TSCTRSTOP = EC_RUN;    // Start Counter

    //Ecap4 config  --  APWM mode
    ECap4Regs.ECCTL2.bit.CAP_APWM = EC_APWM_MODE;// Enable APWM mode
    ECap4Regs.ECCTL2.bit.APWMPOL = 0;
    ECap4Regs.CAP1 = PFC_PRD_DEF;
    //ECap4Regs.CAP2 = FAN_CMP_MIN;           // Set Compare value
    ECap4Regs.CAP3 = PFC_PRD_DEF;           // shadow mode
    //ECap4Regs.CAP4 = FAN_CMP_MIN;           // shadow mode
    ECap4Regs.ECCTL2.bit.TSCTRSTOP = EC_RUN;    // Start Counter

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*
    //Ecap4 config  --  ECAP mode
    ECap4Regs.ECCTL0.bit.INPUTSEL = 124;    //GPIO31-CMPSS5L or CMPSS5H
    ECap4Regs.ECEINT.all = 0x0000;          // Disable all capture __interrupts
    ECap4Regs.ECCLR.all = 0xFFFF;           // Clear all CAP __interrupt flags
    ECap4Regs.ECCTL1.bit.CAPLDEN = EC_DISABLE;      // Disable CAP1-CAP4 register loads
    ECap4Regs.ECCTL2.bit.TSCTRSTOP = EC_FREEZE;     // Make sure the counter is stopped

    ECap4Regs.ECCTL1.bit.CAP1POL = EC_RISING;           // Rising edge
    ECap4Regs.ECCTL1.bit.CAP2POL = EC_FALLING;        // Falling edge
    ECap4Regs.ECCTL1.bit.CAP3POL = EC_RISING;        // Falling edge
    ECap4Regs.ECCTL1.bit.CAP4POL = EC_FALLING;         // Rising edge

    ECap4Regs.ECCTL1.bit.CTRRST1 = EC_DELTA_MODE;       // Reset counter after latch
    ECap4Regs.ECCTL1.bit.CTRRST2 = EC_DELTA_MODE;       // Reset counter after latch
    ECap4Regs.ECCTL1.bit.CTRRST3 = EC_DELTA_MODE;      // Reset counter after latch
    ECap4Regs.ECCTL1.bit.CTRRST4 = EC_DELTA_MODE;

    ECap4Regs.ECCTL1.bit.CAPLDEN = EC_ENABLE;
    ECap4Regs.ECCTL1.bit.PRESCALE = EC_DIV1; // every time trig// Enable CAP1-CAP4 register loads

    //0h (R/W) = Operate in continuous mode
    //1h (R/W) = Operate in one-Shot mode
    ECap4Regs.ECCTL2.bit.CONT_ONESHT = EC_CONTINUOUS;
    ECap4Regs.ECCTL2.bit.STOP_WRAP = EC_EVENT4;
    ECap4Regs.ECCTL2.bit.SYNCI_EN = EC_SYNCI_DISABLE;   // Disable sync in
    ECap4Regs.ECCTL2.bit.SYNCO_SEL = EC_SYNCO_DIS;      // Pass through Disable
    ECap4Regs.ECCTL2.bit.CAP_APWM = EC_CAP_MODE;

    ECap4Regs.ECCTL2.bit.TSCTRSTOP = EC_RUN;            // Start Counter
  //  ECap4Regs.ECCTL2.bit.REARM = EC_ENABLE;             // arm one-shot


    //Ecap5 config  --  ECAP mode
    ECap5Regs.ECCTL0.bit.INPUTSEL = 9;      //INPUT XBAR-9  =》 PWM1A
    ECap5Regs.ECEINT.all = 0x0000;          // Disable all capture __interrupts
    ECap5Regs.ECCLR.all = 0xFFFF;           // Clear all CAP __interrupt flags
    ECap5Regs.ECCTL1.bit.CAPLDEN = EC_DISABLE;       // Disable CAP1-CAP4 register loads
    ECap5Regs.ECCTL2.bit.TSCTRSTOP = EC_FREEZE;     // Make sure the counter is stopped

    ECap5Regs.ECCTL1.bit.PRESCALE = EC_DIV1; // every time trig
    //0h (R/W) = Operate in continuous mode
    //1h (R/W) = Operate in one-Shot mode
    ECap5Regs.ECCTL2.bit.CONT_ONESHT = EC_CONTINUOUS;
    ECap5Regs.ECCTL2.bit.STOP_WRAP = EC_EVENT1;         // WRAP after Caputure Event1

    ECap5Regs.ECCTL1.bit.CAP1POL = EC_RISING;           // Rising edge
    //ECap5Regs.ECCTL1.bit.CAP2POL = EC_FALLING;        // Falling edge
    //ECap5Regs.ECCTL1.bit.CAP3POL = EC_RISING;         // Rising edge

    ECap5Regs.ECCTL1.bit.CTRRST1 = EC_DELTA_MODE;       // Reset counter after latch
    //ECap5Regs.ECCTL1.bit.CTRRST2 = EC_ABS_MODE;       // Do not reset counter after
    //ECap5Regs.ECCTL1.bit.CTRRST3 = EC_DELTA_MODE;     // Reset counter after latch

    ECap5Regs.ECCTL2.bit.SYNCI_EN = EC_SYNCI_DISABLE;   // Disable sync in
    ECap5Regs.ECCTL2.bit.SYNCO_SEL = EC_SYNCO_DIS;      // Pass through
    ECap5Regs.ECCTL2.bit.CAP_APWM = EC_CAP_MODE;

    ECap5Regs.ECCTL2.bit.TSCTRSTOP = EC_RUN;            // Start Counter
    ECap5Regs.ECCTL2.bit.REARM = EC_ENABLE;             // arm one-shot
    ECap5Regs.ECCTL1.bit.CAPLDEN = EC_ENABLE;           // Enable CAP1-CAP4 register loads

*/
    EDIS;
}


//
// InitECapGpio - This function initializes GPIO pins to function as ECAP pins
//                Each GPIO pin can be configured as a GPIO pin or up to 3
//                different peripheral functional pins. By default all pins
//                come up as GPIO inputs after reset.
//                Caution:
//                For each eCAP peripheral
//                Only one GPIO pin should be enabled for ECAP operation.
//                Comment out other unwanted lines.
//

void InitECapGpio()
{
    //qtest0723
   // InitECap1Gpio(0); //GPIO0-PWM1A

    InitECap2Gpio(4); //GPIO4-PWM3A
    InitAPwm3Gpio();
  //  InitECap4Gpio(31); //GPIO31-CMPSS5L or CMPSS5H
  //  InitECap5Gpio(17); //GPIO17-CMPSS3L or CMPSS3H
}

//
// InitECap1Gpio - Initialize ECAP1 GPIOs
//
void InitECap1Gpio(Uint16 pin)
{
    EALLOW;
    InputXbarRegs.INPUT7SELECT = pin;         // Set eCAP1 source to GPIO-pin
    EDIS;
}

//
// InitECap2Gpio - Initialize ECAP2 GPIOs
//
void InitECap2Gpio(Uint16 pin)
{
    EALLOW;
    InputXbarRegs.INPUT8SELECT = pin;         // Set eCAP2 source to GPIO-pin
    EDIS;
}

//
// InitECap3Gpio - Initialize ECAP3 GPIOs
//
void InitECap3Gpio(Uint16 pin)
{
    EALLOW;
    InputXbarRegs.INPUT9SELECT = pin;         // Set eCAP3 source to GPIO-pin
    EDIS;
}

//
// InitECap4Gpio - Initialize ECAP4 GPIOs
//
void InitECap4Gpio(Uint16 pin)
{
    EALLOW;
    InputXbarRegs.INPUT10SELECT = pin;         // Set eCAP4 source to GPIO-pin
    EDIS;
}

//
// InitECap5Gpio - Initialize ECAP5 GPIOs
//
void InitECap5Gpio(Uint16 pin)
{
    EALLOW;
    InputXbarRegs.INPUT11SELECT = pin;         // Set eCAP5 source to GPIO-pin
    EDIS;
}

//
// InitECap6Gpio - Initialize ECAP6 GPIOs
//
void InitECap6Gpio(Uint16 pin)
{
    EALLOW;
    InputXbarRegs.INPUT12SELECT = pin;         // Set eCAP6 source to GPIO-pin
    EDIS;
}

//
// InitAPwm1Gpio - Initialize EPWM1 GPIOs
//
void InitAPwm3Gpio()
{
    EALLOW;
    OutputXbarRegs.OUTPUT1MUX0TO15CFG.bit.MUX4 = 3;//set ecap3 for output 1
    OutputXbarRegs.OUTPUT1MUXENABLE.bit.MUX4 = 1; //1:enable 0:disable

    //GPIO2 is set to OUTPUT XBAR1
    GPIO_SetupPinMux(2U, GPIO_MUX_CPU1, OUTPUTXBAR1);
    GPIO_SetupPinOptions(2U, GPIO_OUTPUT, GPIO_PUSHPULL);
    EDIS;
}


//
// End of file
//
