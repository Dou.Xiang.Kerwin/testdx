/*============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : gbb_start.c
 *  PURPOSE  : relay control pfc, dcdc soft start	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A000           DSP               Created.   Pre-research 
 *===========================================================================*/

#ifndef  App_start_H
#define  App_start_H


extern UINT16 g_u16SequeOnTimer;    //  count for rectifer open sequencely

extern UINT16 g_u16DisableOn;               // for fault record, disable soft start
//control main relay to zvs on
extern void vMainRlyOnOffCtrl(void);

//soft start treatment
extern void vSstartCtrl(void);

#endif

//=============================================================================
// No more.
//=============================================================================

