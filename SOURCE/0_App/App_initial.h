/*=========================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : HD415CZ
 *
 *  FILENAME : gbb_initial.c
 *  PURPOSE  : variables and parameters initialization	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2011-08-26      A000           DSP               Created.   Pre-research 
 *==========================================================================*/
#ifndef App_inital_H
#define App_inital_H

extern void vDataInit(void);
extern void vIsr_init(void);
extern void vInitialInterrupts(void);
extern void vCputoClaVar_init(void);
extern void vCLATaskinit(void);
extern void CLA_configClaMemory(void);

#endif


//===========================================================================
// No more.
//===========================================================================
