/*=============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : HD415CU111_warnctrl.c
 *  PURPOSE  : warn judge and on/off control, panel led, relay, fan control
 *
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A000           DSP               Created.   Pre-research
 *
 *============================================================================*/

#include "f28004x_device.h"		// DSP280x Headerfile Include File
#include  "App_vAdcTreatment.h"
#include  "App_warnctrl.h"
#include  "App_cal.h"
#include  "App_start.h"
#include  "App_vAdcTreatment.h"
#include "Prj_macro.h"
#include <App_cancom.h>
#include <App_isr.h>
#include "CLAShared.h"
#include "GBB_constant.h"
#include <App_constant.h>
#include <App_main.h>
#include "IQmathLib.h"
#include  "App_vEventLog.h"
#include  "Prj_config.h"
/*******************************************************************************
*variables definition:these variables only can use in this file
*******************************************************************************/
/*--- Global variables used to interface to the flash routines */
//FLASH_ST FlashStatus;
//#pragma    DATA_SECTION(g_u32FailFlagPointer,"RepairVar");
//Uint32 g_u32FailFlagPointer;		//PFC OR DC FAIL FLAG from Flash 3f0010-3f0011
//Uint32 g_u32RdFailFlag;				//the FLAG of PFC OR DC FAIL from Flash to RAM

static	UINT16	s_u16MdlOpenBak;		//module on/off control flag
/*******************************************************************************
*global variables definition:these variables can use in all of the files
*******************************************************************************/
 INT16	g_i16FanPwmSet;
 INT16	g_i16FanRunMin;
UINT16	g_u16OverVoltStatus;	//rectifier dcov status
UINT16	g_u16HWOverVoltStatus;
UINT16	g_u16CloseTime;			//count for rectifier the second HVSD
UINT16	g_u16CloseTimeHW;
UINT16	g_u16CurrUnBal;			//count for current unbalance
UINT16	g_u16CurrSerUnBal;		//count for seriously current unbalance
INT16	g_i16FanPwm;			//Fan PWM
UINT16	g_u16FanCtrFlag;			//Fan control flag

INT16   g_i16FanSpecialSet;
UINT16 	g_u16Box0FailTimer;  	//count for can comm with other module interrupt
UINT16 	g_u16CanFailTime;		//count for CAN comm with scu interrupt
UINT16 	g_u1660sTimer;			//count for AC over voltage 60S timing
ubitfloat g_lq10PFCCurr;

//UINT16 	g_u16DcRippSWCtrl;
UINT16 	g_u16HVSDFistFlag ;
//ubitfloat	g_lq10HWHVSDReonTime;
//UINT16 g_u16FanEnable;
ubitfloat g_lq10LowTempWalkInTime;
UINT16 g_u16PFCUVCheckCnt;

UINT16	g_u16SafetyShortStatus;		//Safety Short dc restart status
UINT16	g_u16DcRestartFailTimes;
INT16	g_i16FanFailTimer2;
UINT16  g_u16RippleDisableCont;//闄愭祦鐐规敼鍙樺悗灞忚斀绾�?尝鐜殑璁℃椂鍣�?
static UINT16	g_u16PfcFailRestart;
UINT16  g_u16FanSpeedFlag;
UINT16 g_u16SaveShortCompleteFlag;
UINT16 g_u16OverVoltCounter;
//qtest0601
UINT16 g_u16PhaseSheddingFlag;
//qtest0601end
static  ubitfloat s_lq10SetPowerLastValue;
// CAN ID RESET CHK timer
static UINT16 s_u16CANIDResetTimer;

INT32     g_ai32AcMaxPointVal[3];
//static ubitfloat  s_lVacPeakBeforeDrop;
static ubitfloat  s_lVacPeakAfterDrop;
//static ubitfloat  s_lVacPeakAfterJump;
static ubitfloat  s_lVacPeakBeforeJump;
static INT16    s_i16DspTempPartTimer;
ubitfloat  g_lq10ACPeak_LimPow;
ubitfloat  g_lq10ACDynLimitPower;
ubitfloat  g_lq10MdlVoltHvsd;

//paul-20220228
UINT16  g_u16ReceiveDataEnFlag; //Whether to receive data from SCU
UINT16  g_u16ComIntOffFlag;   //10min not receive data
static UINT16 s_u16CanIntTimerCnt;    //10鍒嗛挓璁℃暟鍣�?
static UINT16 s_u16AntiThiefDelayOff;
UINT16 g_u16CusSitNumDisMatchFlag;   //闃茬洍鍔熻兘锛屽鍔犵敤鎴蜂俊鎭笉鍖归厤鏍囧織浣嶏�?

UINT16 g_u16AntiThiefDelayCnt;
UINT16 g_u16AntiThiefDelaySet;
//UINT16 g_u16AntiThiefDelaySetbyMcu;
ubitfloat g_lq10AntiThiefDelayTime;

/*******************************************************************************
*functions declare: these functions only can use in this file
*******************************************************************************/
//DC Fault Check
//static	void  vDcFaultChk(void);

//fan fault check
static	void  vFanFaultCheck(void);

//current unbalance  check
static	void  vUnshareCheck(void);

//Pfc fault check
static	void  vPfcFaultCheck(UINT16 u16FilterLossPFCOV,UINT16 u16FilterLoss,UINT16 u16FilterBack);

//Vac fault check
static	void  vAcFaultCheck(UINT16 u16FilterLoss,UINT16 u16FilterBack);

//ambi  temperature check
static	void  vAmbiTempCheck(UINT16 u16Filter);

//M1 borad temperature check
static	void  vDcTempCheck(UINT16 u16Filter);

//PFC borad temperature check
//static	void  vPFCTempCheck(UINT16 u16Filter);

//dc volt over handle
static	void  vDcVoltOverHandle(void);

//DC voltage over check
static	void  vDcVoltOverChk(UINT16 u16Filter);

//Dcovp signal check
static	void  vDcHWHVSDCheck(UINT16 u16Filter);

static	void   vDcCurrShortCheck(UINT16 u16Filter);

static	void  vDcHWHVSDRestart(void);

static  void  vHWHVSDDispRstCtrl (void);

//can fail chk
static	void  vCanFailChk(void);

// CAN ID RESET CHK
static	void vCANIDResetChk(void);

//Parallel or not chk
static	void  vParallelChk(void);

//module on/off control
static	void  vOnOffCtrl(void);

//relay control
static	void  vRelayCtrl(void);

//fan speed control
static	void  vFanCtrl(void);

//Input Voltage DC or AC judge
static	void  vVDCInputJudgeWarn(void);

//PFC CUR/VOL PHASE delay complement
static	void  vPFCPhaseComplement(void);

//panel led control
static	void  vLedCtrl(void);

//pfc loop control swith
static	void  vPfcLoopCtrlSwith(void);

//dcdc loop control swith
static	void  vDcdcLoopCtrlSwith(void);

//permit ac volt over handle
static	void  vPermitOverAcHandle(void);

//module seque on time handle
static	void  vSequeOnTimeCal(void);

//mixed system chk and handle
static void   vMixedSystemHandle(void);

//clear Estop flag
static  void  vEStopClear(void);

//PFC softstart fail
static void   vPFCSoftStartFail(void);
void  vModelInputDyn(void);

//Avoid Maitain Handle
//PFC锛孌C鏁呴殰缁翠慨鏍囧織浣嶄俊�?��璧�?��硷紝
//渚濇鍏矰C--鍏砅FC--鍏充腑鏂�?--鍋滄鐪�?��鐙�
//--瑙ｉ�?LASH--鍐�?LASH 缁翠慨淇℃伅--鍔犲瘑FLASH--�?���??�?���?--�?��涓�?
//static void vAvoidMaitainHandle(void);

// HVSD RESET
static void  vHVSDReset(void);
//PFCOCP �?��娴�
static void  vPFCOcpCheck(void);

//module safety short restart handle
static void  vSafetyShortRestartHandle(void);

//棰濆畾�?ヤ綔鏉�?��?涓�?紝绯荤粺閲岋紝�?庢�?鎺у埗绛栫�?
static INT32  vSystemFanCtrl(void);

//棰濆畾�?ヤ綔鏉�?��?涓�?紝绯荤粺閲�20model锛岄鎵囨帶鍒剁瓥鐣�?
static INT32  vSystemFanCtrl20(void);
//棰濆畾�?ヤ綔鏉�?��?涓�?紝鍗曟ā鍧楋紝椋庢墖鎺у埗绛�?��
static INT32  vSingleModelFanCtrl(void);

//�??�潡鍏砅FC,DC鐨勫姩浣溿��?
void vModelTurnOff_PfcDc(void);

void  vUnplugCtrl(void);

//over level chk
UINT16	uiOverLevelChk(INT32 lCompareData, INT32 lHighLever, UINT16 u16Filter,
                       UINT16 *pCounter);

//under level chk
UINT16	uiUnderLevelChk(INT32 lCompareData, INT32 lLowLever, UINT16 u16Filter,
                        UINT16 *pCounter);

//up dowm limit
INT32  lUpdownlimit(INT32 lPareData , INT32 lHighLever, INT32 lLowLever);

static  void    vPFCPhaseShielding(void);

//paul-20220228
//Anti-theft
static  void vCustNumMatchCkeck(void);
static  void vCustNumMatchCnt(void);
/*******************************************************************************
 *Function name: vWarnCtrl()
 *Description :  IO port state sample, filter, warning judge and treatment
 *input:         void
 *global vars:   g_u16RunFlag.bit.WARN: run flag, 10ms once
 *output:        void
 *CALLED BY:     main()
 ******************************************************************************/

void  vWarnCtrl(void)
{
	if (g_u16RunFlag.bit.WARN)
	{
		//clear count flag for next time
		g_u16RunFlag.bit.WARN = 0;

		//clear EStopFlag ,disable estop function
		vEStopClear();

		//fan fault check
		vFanFaultCheck();

        //plug signal check
		vUnplugCtrl();  //for GZ debug

		//current unbalance  chk
		vUnshareCheck();

		//Pfc fault check
		vPfcFaultCheck(TIME10S,TIME3S,TIME10MS);

		// according to G250A v102 csm 091221
		vAcFaultCheck(TIME100MS,TIME1S5);

    	//ambi  temperature protect  check
		vAmbiTempCheck(TIME100MS);

		//M1 borad temperature protect check
		vDcTempCheck(TIME100MS);

		//PFC borad temperature protect check
		//vPFCTempCheck(TIME100MS);

		//Dcovp check
		vDcHWHVSDCheck(TIME100MS);

		vDcCurrShortCheck(TIME100MS);


        //Clear HVSDDisp flag
        vHWHVSDDispRstCtrl ();

		//output voltage HVSD handle
		vDcVoltOverHandle();

		//DC Fault Check
		//vDcFaultChk();		//by ls 20120202 need add after test on moudle

		//can fail chk
		vCanFailChk();

		//Parallel or not chk
        vParallelChk();

        //Input Voltage DC or AC judge
        vVDCInputJudgeWarn();

        //module on/off control
		vOnOffCtrl();

		//relay control
		vRelayCtrl();

		// FAN speed control
		vFanCtrl();

		vPFCPhaseComplement();

		//Led control
		vLedCtrl();

		//pfc loop control swith
		vPfcLoopCtrlSwith();

		//dcdc loop control swith
        vDcdcLoopCtrlSwith();
        vPFCPhaseShielding();

		//permit ac volt over handle
        vPermitOverAcHandle();

		//module seque on time handle
		vSequeOnTimeCal();

		//mixed system chk and handle
		vMixedSystemHandle();

		vPFCSoftStartFail();

       //vAvoidMaitainHandle();

		vHVSDReset();

		vPFCOcpCheck();

		vSafetyShortRestartHandle();

        vModelInputDyn();

        //paul-20220228
        vCustNumMatchCnt();
        vCustNumMatchCkeck();

	}
}

/******************************************************************************
 *Function name: vCustNumMatchCnt()
 *Description :  Anti-theft
 *input:         /
 *global vars: /
 *output:        void
 *CALLED BY:     vCustNumMatchCnt()   20210807 Eileen
 ******************************************************************************/
static  void vCustNumMatchCnt(void)
{
    //浣胯兘闃�?��鍔熻�?&&�?��俊�?傚父鍏佽鍏�?��锛�
    //if((g_u16MdlCtrl.bit.AntiTheftFlag) && (g_u16MdlCtrl.bit.AntiTheftCanIntOff))
    if(g_u16MdlCtrl.bit.AntiTheftFlag)
    {
        //杩炵�?10min娌℃湁鎺ユ敹鍒版暟鎹紱
        if(g_u16ReceiveDataEnFlag == 0)
        {
            s_u16CanIntTimerCnt++;

            if(s_u16CanIntTimerCnt >= TIME10MIN) //10min
            {
                s_u16CanIntTimerCnt = TIME10MIN;
            }
        }
        else
        {
            s_u16CanIntTimerCnt = 0;
        }
    }
    else
    {
        g_u16ReceiveDataEnFlag = 0;
        s_u16CanIntTimerCnt = 0;
    }


    if(g_u16CusSitNumDisMatchFlag)
    {
        if(g_u16RunFlag.bit.ANTITHIEFTIME)
        {
            g_u16RunFlag.bit.ANTITHIEFTIME = 0;
            g_u16AntiThiefDelayCnt ++;
            if(g_u16AntiThiefDelayCnt >= g_u16AntiThiefDelaySet)
            {
                g_u16AntiThiefDelayCnt = g_u16AntiThiefDelaySet;
                s_u16AntiThiefDelayOff = 1;
            }
        }

    }
    else
    {
            g_u16AntiThiefDelayCnt = 0;
            s_u16AntiThiefDelayOff = 0;
    }

}


/******************************************************************************
*Function name: vCustNumMatchCkeck()
*Description :  Anti-theft
*input:         /
*global vars: /
*output:        void
*CALLED BY:     vCustNumMatchCkeck()   20210807 Eileen
******************************************************************************/
//paul-20220228
static  void vCustNumMatchCkeck(void)
{
   if(g_u16MdlCtrl.bit.AntiTheftFlag)
   {
       if((g_u16ActionReady >= NORMAL_RUN_STATE) && (g_u16ReceiveDataStatus == 1))
       {
           if((g_u32SitNumberRx != g_u32SitNumberTx) || (g_u16CustomerNumberRx!= g_u16CustomerNumberTx))
           {
               g_u16CusSitNumDisMatchFlag = 1;
           }
           else
           {
               g_u16CusSitNumDisMatchFlag = 0;
           }
        }
        else
        {
         // no action
         ;
        }
   }
   else
   {
       g_u16CusSitNumDisMatchFlag = 0;
   }


   if((g_u16CustomerNumberTx == 0xFF) && (g_u32SitNumberTx == 0x00FFFFFF))
   {
       g_u16CustNumDisMatch = 0;
   }

   else if((s_u16CanIntTimerCnt == TIME10MIN) || s_u16AntiThiefDelayOff)
   {
       g_u16CustNumDisMatch = 1;
   }
   else
   {
       g_u16CustNumDisMatch = 0;
   }

   g_u16ReceiveDataStatus = 0;

}

/******************************************************************************
 *Function name: vDcFaultChk()
 *Description :  Dc Fault  check
 *input:         /
 *global vars: /
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
/*
static	void  vDcFaultChk(void)
{
	static UINT16 s_u16DcFaultChkCnt;

	if(  (g_u16ActionReady == NORMAL_RUN_STATE)&&(g_lq10MdlVolt.lData <= VDC_58V)
	   &&(g_lq10MdlVolt.lData >= VDC_41V7)&&(g_u16MdlStatus.bit.CALIBRFAIL == 0)
	   &&( g_lq10MdlCurrNoFilter.lData <(g_lq10MdlLimitCurrent.lData - IDC_LOAD_10PCT))
	   &&( g_lq10MdlCurrNoFilter.lData < IDC_10A))
	{
		if(IsrVars._u16ChoiseCon) // || (g_lq10MdlVoltNoFilter.lData < (g_lq10SetVoltTrue.lData - VDC_2V)))
		{

			   	s_u16DcFaultChkCnt ++;

			   	if(s_u16DcFaultChkCnt >=TIME60S)
			   	{
			   		g_u16MdlStatusExtend.bit.DCFAULT = 1;

					s_u16DcFaultChkCnt = TIME60S;
			   	}
			   	else
			   	{
			   		g_u16MdlStatusExtend.bit.DCFAULT = 0;

			   	}

		}
		else
		{
			s_u16DcFaultChkCnt = 0;
			g_u16MdlStatusExtend.bit.DCFAULT = 0;
		}
	}
	else
	{
		s_u16DcFaultChkCnt = 0;
		g_u16MdlStatusExtend.bit.DCFAULT = 0;
	}
}
*/

/*******************************************************************************
 *Function name: vFanFaultCheck()
 *Description :  fan fault judge
 *input:         void
 *global vars:   IsrVars._lFanBad.iData.iHD:fan bad signal sample:1v->267.712418
 *output:        void
 *CALLED BY:     void vWarnCtrl(void)
 ******************************************************************************/
static	void	vFanFaultCheck(void)
{
	static UINT16	s_u16FanFailTimer;
	static UINT16	s_u16FanFault;

	//FAN fault judge start
	s_u16FanFailTimer ++;
	//80ms once
	if(s_u16FanFailTimer % TIME80MS == 0)
	{
		s_u16FanFault = s_u16FanFault << 1;
		if (((IsrVars._lFanBad.iData.iHD >= VFAN_BAD_AD_2V5)
		    || (IsrVars._lFanBad.iData.iHD <= VFAN_BAD_AD_0V5))
		    && (g_u16ActionReady >= PFC_SWSOFT_START))

		{
		    s_u16FanFault  |= FAN_ERR_ONCE;
		}
	}

	// fan fault judge mask when fan stop
	if(g_u16FanEnable == FAN_RUN_DISABLE)
	{
		s_u16FanFault = FAN_OK;
	}
//	s_u16FanFault = FAN_OK;
	//fan fault filter
	if((s_u16FanFault & FAN_FAULT) == FAN_FAULT)
	{	//80m*14=1120ms
		g_u16MdlStatus.bit.FANFAIL = 1;
	}
	else if (s_u16FanFault == FAN_OK)
	{	//80m*16=1.28s
		g_u16MdlStatus.bit.FANFAIL = 0;
	}
	//FAN fault judge end

}

/*******************************************************************************
 *Function name: vPfcFaultCheck()
 *Description :  Pfc over voltage and under voltage check
 *input:         u16Filter: chk times
 *global vars:   g_u16MdlStatus:  module status
 *               g_u16MdlStatus.bit.PFCOV: Pfc over voltage flag
 *				 g_u16MdlStatus.bit.PFCUV: Pfc under voltage flag
 *               g_iq10PfcVoltDisp.lData: Pfc voltage for display
 *output:        void
 *CALLED BY:     void vWarnCtrl(void)
 ******************************************************************************/
static	void  vPfcFaultCheck(UINT16 u16FilterLossPFCOV,UINT16 u16FilterLoss,UINT16 u16FilterBack)
{
	static UINT16 s_u16PfcOVChkCnt, s_u16PfcUnChkCnt;

    //Pfc over voltage judge start
    if(g_u16MdlStatus.bit.PFCOV == 0)
    {
		if((uiOverLevelChk(g_lq10PfcVoltDisp.lData, VPFC_HLOSS, u16FilterLossPFCOV,
        				  &s_u16PfcOVChkCnt))||(IsrVars._u16PfcQuickPFCOVPFault))
        {
			g_u16MdlStatus.bit.PFCOV = 1;
			//IsrVars._i16pfcompare2 = 1;
        }
    }

    else
    {
        if(uiUnderLevelChk(g_lq10PfcVoltDisp.lData,VPFC_HBACK,u16FilterBack,
        				   &s_u16PfcOVChkCnt))
        {
            g_u16MdlStatus.bit.PFCOV = 0;
        }
    }

    //Pfc under voltage judge start

	//if( (IsrVars._u16PfcOpened == 1) && (g_u16ActionReady >= DC_SOFT_START) )
	//澧炲�?(g_u16ActionReady >= DC_SOFT_START)鏄负浜嗗綋PFC MOS鏂矾浠ュ悗涓嶈鎬绘姤PFCUV鑰屼笉鎶FCFAIL
	//鎶�?srVars._u16PfcOpened�?瑰埌杞捣鍔ㄦ瘝绾垮ぇ浜�?380V鏃舵墠缃�?1鍙互淇濊瘉PFC MOS鏂矾鑳芥�?��姤鍑鸿�屼笉鎶FCUV
	//褰撶劧濡傛灉杈撳叆鐢靛帇宄板�煎ぇ浜�?380V锛岄偅PFCUV鍜�?FCFAIL�?��笉浼�?��鍑�
    if(IsrVars._u16PfcOpened == 1)
	{
		if((uiUnderLevelChk(g_lq10PfcVoltDisp.lData, VPFC_LLOSS, u16FilterLoss,
						   &s_u16PfcUnChkCnt)) ||(IsrVars._u16PfcQuickPFCUVPFault))
		{
			g_u16MdlStatus.bit.PFCUV = 1;
		}
	}
	else
	{
		g_u16MdlStatus.bit.PFCUV = 0;
	}
}

/*******************************************************************************
 *Function name: vAcFaultCheck()
 *Description :  Vac over voltage and under voltage check
 *input:         u16Filter: chk times
 *global vars:   g_lq10AcVolt.lData: input voltage Vac
 *               g_u16MdlStatus: module status
 *               g_u16MdlStatus.bit.ACUV: Vac under voltage flag
 * 				 g_u16MdlStatus.bit.ACOV: Vac over voltage flag
 *output:        void
 *CALLED BY:     void vWarnCtrl(void)
 ******************************************************************************/
static	void  vAcFaultCheck(UINT16 u16FilterLoss,UINT16 u16FilterBack)
{
	static UINT16 s_u16AcUnChkCnt, s_u16AcOVChkCnt;

	//Vac under voltage judge start
	if(g_u16ActionReady >= PFC_SWSOFT_START_INIT)
 	{
		if(g_u16MdlStatus.bit.ACUV == 0)
		{
			if(uiUnderLevelChk(g_lq10AcVolt.lData, VAC_LLOSS, u16FilterLoss,
		                       &s_u16AcUnChkCnt))
			{
        		g_u16MdlStatus.bit.ACUV = 1;
			}
		}
		else
		{
 	    	if(uiOverLevelChk(g_lq10AcVolt.lData, VAC_LBACK, u16FilterBack,
 						       &s_u16AcUnChkCnt))
			{
				g_u16MdlStatus.bit.ACUV = 0;
       		}
       	}
	}
    else
	{
		g_u16MdlStatus.bit.ACUV = 0;
	}
	//Vac under  voltage judge end

	//Vac over  voltage judge start
	if(g_u16MdlStatusExtend.bit.DCINPUTSTATUS == 0)//浜ゆ祦杈撳叆杩囧帇鍒ゆ柇
	{
    	if(g_u16MdlStatus.bit.ACOV == 0)
	    {
		    if(uiOverLevelChk(g_lq10AcVolt.lData, VAC_HLOSS, u16FilterLoss,
					  	  &s_u16AcOVChkCnt))
		     {
			     g_u16MdlStatus.bit.ACOV = 1;

			     //Vac over voltage  mask Vac under voltage
		       	g_u16MdlStatus.bit.ACUV = 0;
	     	}
    	}
    	else
    	{
	     	if(uiUnderLevelChk(g_lq10AcVolt.lData, VAC_HBACK, u16FilterBack,
						   &s_u16AcOVChkCnt))
	     	{
		     	g_u16MdlStatus.bit.ACOV = 0;
   	     	}
        }
    }
	else //鐩存祦杈撳叆杩囧帇鍒ゆ柇
	{
    	if(g_u16MdlStatus.bit.ACOV == 0)
	   {
		    if(uiOverLevelChk(g_lq10AcVolt.lData, VHVDC_HLOSS, u16FilterLoss,
					  	  &s_u16AcOVChkCnt))
		     {
			     g_u16MdlStatus.bit.ACOV = 1;

			     //Vac over voltage  mask Vac under voltage
		       	g_u16MdlStatus.bit.ACUV = 0;
	     	}
    	}
    	else
    	{
	     	if(uiUnderLevelChk(g_lq10AcVolt.lData, VHVDC_HBACK, u16FilterBack,
						   &s_u16AcOVChkCnt))
	     	{
		     	g_u16MdlStatus.bit.ACOV = 0;
   	     	}
       }
    }



	if(IsrVars._u16OverAcVolt)
	{
		g_u16MdlStatus.bit.ACOV = 1;
	}
	//Vac over  voltage judge end

	//mask Vac over voltage and pfc over voltage
	if (IsrVars._u16PermitOverAc == PERMIT_ACOVER)
	{
		g_u16MdlStatus.bit.ACOV = 0;
		g_u16MdlStatus.bit.PFCOV = 0;
	}
   //鍦ㄩ�?���?��娴佽�?��ユ帀鐢�?�?60ms鍚庯紝杈撳叆鍒ゅ畾涓轰氦娴侊紝杩囧帇鐐圭敱鐩存祦杩囧帇鐐�?400澶歏鍙樹负浜ゆ祦杩囧帇鐐�?300澶歏锛堟湁鏁堝�硷級锛�
   //鐢变簬ADCTREAMENT �?��30鐐�?护娉㈠�?鏃�?紝浣�?��杈撳叆鏄剧ず鍊间粛澶т�?300澶歏鑰屾姤杩囧帇銆�
   //鍔犲叆涓�?��杩欐�?��屽彲浠ュ�??�屽叏鎺夌數鏃�?紝蹇�熼��鍑鸿繃鍘�?姸鎬併��?
    if(IsrVars._u16InputLowVolCNT >= TIME60MS_ISRTIMER)//濡傛灉杈撳叆鏄繛缁�?60ms浣庡�?30V浠ヤ笅锛屽垯鍒ゆ柇AC涓嶈繃鍘�?
	{
    	g_u16MdlStatus.bit.ACOV = 0;

	}



}

/******************************************************************************
 *Function name: vUnshareCheck()
 *Description :  current unbalance judge
 *input:         void
 *global vars:   g_u16MdlStatus.bit.IUNBALMINOR: load seriously unbalance flag
 *               g_u16MdlStatus.bit.IUNBALMAJOR: load slightly unbalance flag
 *output:        void
 *CALLED BY:     vWarnCtrl(void)
 ******************************************************************************/
static	void     vUnshareCheck(void)
{
	//judge if current unbanlance
	if ( (abs(g_i16CurrDelta) == CURR_DELTA_UP)
	   && (g_lq10MdlAvgCurr.lData >= IDC_RATE_20PCT)  // csm090914
	   && (g_u16ActionReady >= NORMAL_RUN_STATE)
	   && (!g_u16MdlStatus.bit.OFFSTAT)
	   && ((g_u16LimitStatus & POW_LIM) == 0)
	   && ((g_u16MdlMixStatus & 0x0800) == 0)
	   && (!g_u16CurrLimitStatusFlag))  //paul-20220228
	{

		if((g_lq10MdlCurr.lData < IDC_RATE_2PCT)
			&&(g_i16CurrDelta >= CURR_DELTA_UP) )
		{
			g_u16CurrSerUnBal ++;
			g_u16CurrUnBal = 0;

			//wait 3s,then set load seriously unbalance flag
			if (g_u16CurrSerUnBal >= TIME20S)
			{
				g_u16CurrSerUnBal = TIME20S;
				g_u16MdlStatus.bit.IUNBALMAJOR = 1;
			}
		}

		else if ( (g_lq10MdlCurr.lData < IDC_RATE_4PCT)
				&& (g_u16CurrSerUnBal >= TIME20S) )

		{
			g_u16CurrSerUnBal = TIME20S;
			g_u16MdlStatus.bit.IUNBALMAJOR = 1;
		}
		//the deffrence between avg and mdl curr>8A,
		//wait 3s,setslightly unbalance flag
		//IDC_RateCurPer16 csm 090925
		else if (labs(g_lq10MdlAvgCurr.lData - g_lq10MdlCurr.lData)
				>= IDC_RATE_16PCT)
		{
			g_u16CurrSerUnBal = 0;
			g_u16CurrUnBal ++;

			if (g_u16CurrUnBal >= TIME20S)
			{
				g_u16CurrUnBal = TIME20S;
				g_u16MdlStatus.bit.IUNBALMINOR = 1;
			}
		}
		//if setslightly unbalance flag is already set,the deffrence between avg
		//and mdl curr>6A,setslightly unbalance
		else if((labs(g_lq10MdlAvgCurr.lData-g_lq10MdlCurr.lData)
				>= IDC_RATE_12PCT) &&
		        (g_u16CurrUnBal >= TIME20S) )
		{
			g_u16CurrUnBal = TIME20S;
			g_u16MdlStatus.bit.IUNBALMINOR = 1;
		}
		//else no unbalance
		else
 		{
			g_u16CurrSerUnBal = 0;
			g_u16CurrUnBal = 0;
			g_u16MdlStatus.bit.IUNBALMAJOR = 0;
			g_u16MdlStatus.bit.IUNBALMINOR = 0;
		}
	}
	//else no unbalance
	else
	{
		g_u16CurrSerUnBal = 0;
		g_u16CurrUnBal = 0;
		g_u16MdlStatus.bit.IUNBALMAJOR = 0;
		g_u16MdlStatus.bit.IUNBALMINOR = 0;
	}
}

/*******************************************************************************
 *Function name: vOnOffCtrl()
 *Description :  on off control
 *input:         void
 *global vars:   g_u16MdlStatus.bit.OFFSTAT: on/off flag
 *output:        void
 *CALLED BY:     void vWarnCtrl(void)               xgh
 ******************************************************************************/
static	void	vOnOffCtrl(void)
{
	//rectifier fault, protect  on/off control
	//turn off DCDC , if DCOV,SHORT,or OFFCTRL

	if(mMdlOff_DC())
	{
		//set DCDC off control flag
		s_u16MdlOpenBak |=  DCDC_OFF;

		//close dcdc pwm
		mOffDcdcPwm();

		if((g_u16ActionReady == NORMAL_RUN_STATE)
		    ||(g_u16ActionReady == DC_SOFT_START))
		{
			g_u16ActionReady = DCOFF_SOFT_STATE;
		}

		//set dcdc off flag
		g_u16MdlStatus.bit.OFFSTAT = 1;
	}

	else
	{

	if( (s_u16MdlOpenBak & DCDC_OFF)
		&&(g_u16ActionReady >= DC_SOFT_START_INIT)
		&&(g_u16HVSDFistFlag ==0)
		&&(g_u16SafetyShortStatus == DC_SAFE_SHORT_STATUS_NORMAL) )
		{
			//DCDC restart
			g_u16ActionReady = DC_SOFT_START_INIT;

			//clr dcdc off control flag
			s_u16MdlOpenBak &=~ DCDC_OFF;
		}
	}


	//turn off PFC and DCDC if HVSD, PFC AC OV/UV,
	//over temperature, FAN fault and Estop

	if(mMdlOff_PFCDC())

	{
	  vModelTurnOff_PfcDc();
	}

	else
	{

		if (s_u16MdlOpenBak & PFC_OFF)
		{
			//restart pfc and dcdc
			g_u16ActionReady = POWON_STATE;
			s_u16MdlOpenBak &=~ PFC_OFF;
		}
	}



   if(  g_u16downloadflag == 1)//涓�?浇鏍囧織浣嶈〃绀哄紑�??笅杞�?
   {
       	s_u16MdlOpenBak |= PFC_OFF;

	    //close dcdc PWM
		mOffDcdcPwm();

		//close pfc pwm
		mOffPfcPwm();

		//main relay open(default open) & pfc off
 		mMainRelayOpen();

	    g_u16FanEnable = FAN_RUN_DISABLE;

		g_u16ActionReady = PFCOFF_SOFT_STATE;

		g_u16MdlStatus.bit.OFFSTAT = 1;
        IsrVars._u16PwmEnableFlag = 0;

		IsrVars._u16PfcQuickPFCUVPFault = 0;
		IsrVars._u16PfcQuickPFCOVPFault=0;
		IsrVars._u16OverAcVolt = 0;
		IsrVars._u16PfcOpened = 0;
   }

   if((g_u16downloadflag == 1)&&( g_i16FanPwm >= FAN_CMP_MIN))
   //if(g_u16downloadflag == 1)
   {
        g_u16downloadflag =0;

        DINT;

        mOffDcdcPwm();
        mOffPfcPwm();
        g_u16MdlStatus.bit.OFFSTAT = 1;
        IsrVars._u16PwmEnableFlag = 0;

        IER = 0x0000;
        IFR = 0x0000;

        DELAY_US(TIME5MS_CPUCLK);
        B1AppEntryPointer();


   }

}

/******************************************************************************
 *Function name: vRelayCtrl()
 *Description :  main relay and over relay control
 *input:         void
 *global vars:   void
 *output:        void
 *CALLED BY:     void vWarnCtrl(void)
 ******************************************************************************/
static	void  vRelayCtrl(void)

{
	//ac ov/uv,or force relay open
	if((g_u16MdlStatus.bit.ACUV) ||
	   (g_u16MdlStatus.bit.ACOV)||(g_u16MdlCtrl.bit.SHORTRELAY))
	{
	   	//control main relay zvs off
		vMainRlyOnOffCtrl();
	}

	if(g_u16MdlStatus.bit.ACOV)
    {
		//relay control when AC over voltage: main relay open,
		//over relay open or close in turn
		if(g_lq10PfcVolt.lData > VPFC_OVERRELAY_OPEN)
        {
			mOverRelayOpen();//off
        }

		if(g_lq10PfcVolt.lData < VPFC_OVERRELAY_CLOSE)
	    {
			mOverRelayClose();//on
	    }
    }
}


/******************************************************************************
 *Function name: vAmbiTempCheck()
 *Description :  ambi temperature check
 *input:         u16Filter:chk times
 *global vars:   g_iq10TempAmbi: Ambi temperature
 *               g_u16MdlStatus: module status
 *               g_u16MdlStatus.bit.AMBIOT: Ambi temperature over flag
 *output:        void
 *CALLED BY:     vWarnCtrl()
 *******************************************************************************/
static	void  vAmbiTempCheck(UINT16 u16Filter)
{
	static UINT16 s_u16AmbiTempChkCnt;

    if(g_u16MdlStatus.bit.AMBIOT == 0)
    {
        //set over temp flag
        if(uiOverLevelChk(g_lq10TempAmbiDisp.lData, TEMP_AMBI_HLOSS, u16Filter,
           &s_u16AmbiTempChkCnt))
        {
            g_u16MdlStatus.bit.AMBIOT = 1;
        }
    }

    else
    {
        //lclear over temp flag
        if(uiUnderLevelChk(g_lq10TempAmbiDisp.lData,TEMP_AMBI_HBACK,u16Filter,
           &s_u16AmbiTempChkCnt))
        {
            g_u16MdlStatus.bit.AMBIOT = 0;
        }
    }
}

/******************************************************************************
 *Function name: vDcTempCheck()
 *Description :  M1 borad temperature check
 *input:         u16Filter:chk times
 *global vars:   g_iq10TempDc: M1 borad temperature
 *               g_iq10MdlTempUp: M1 borad temperature over point
 *               g_u16MdlStatus: module status
 *               g_u16MdlStatus.bit.RECTOT: M1 borad temperature over flag
 *output:        void
 *CALLED BY:     vWarnCtrl()
 *******************************************************************************/

static	void  vDcTempCheck(UINT16 u16Filter)
{
	static UINT16 s_u16DcTempChkCnt;
    ubitfloat lq10MdlTempUpTrue;

	if(IsrVars._u16SoftStartEndTimer >= DCSTARTEND_5S)
    {   //鏍�?嵁姣嶇嚎鍜孌C鐢靛帇鍦ㄦ�??伐浣滄�?鐨勫彉姣斿喅瀹�?C鏉挎�?��濇姢鐐癸紝濡傛灉澶т簬锛�376/40锛�?�?1024=9625
        //鍥犱负鍙湪NORMAL_RUN璧蜂綔鐢紝鎵�浠ュ�?��囨俯�?��鐐规病�?��
        //褰撴瘝绾夸笌杈撳�?��靛帇鍙樻�?��冨ぇ鏃舵妸DC鏉挎�?��濇姢鐐�?斁浣�?10搴�
        if(g_lq10VpfcVdcRatio.lData > LQ10_VPFC_VDC_RATIO_9P4 )
        {
            lq10MdlTempUpTrue.lData = g_lq10MdlTempUp.lData - TEMP_10C;
        }
        else
        {
            lq10MdlTempUpTrue.lData = g_lq10MdlTempUp.lData;
        }
	}
	else //鍦ㄩ�??ｅ父宸ヤ綔鎬侊紝杩囨�?��逛笉鍙�
	{
	    lq10MdlTempUpTrue.lData = g_lq10MdlTempUp.lData;
	}

    if(g_u16MdlStatus.bit.RECTOT == 0)
    {

        //if(iq10TempDcDisp.lData > g_iq10MdlTempUp.lData),
        //set M1 borad temperature over flag
        if(uiOverLevelChk(g_lq10TempDc.lData, lq10MdlTempUpTrue.lData, u16Filter,
           &s_u16DcTempChkCnt))
        {
            g_u16MdlStatus.bit.RECTOT = 1;
        }
    }

    else
    {
		//clr M1 borad temperature over flag
        if(uiUnderLevelChk(g_lq10TempDc.lData,
        				  (lq10MdlTempUpTrue.lData - TEMP_20C),
        				  u16Filter,&s_u16DcTempChkCnt))
        {
            g_u16MdlStatus.bit.RECTOT = 0;
        }
    }
}

/******************************************************************************
 *Function name: vPFCTempCheck()
 *Description :  PFC borad temperature check
 *input:         u16Filter:chk times
 *global vars:   g_iq10TempPFC: PFC borad temperature
 *               g_iq10MdlTempUp: PFCborad temperature over point
 *               g_u16MdlStatusExtend: module status
 *               g_u16MdlStatusExtend.bit.PFCOT : PFC borad temperature over flag
 *output:        void
 *CALLED BY:     vWarnCtrl()
 *******************************************************************************/

//static	void  vPFCTempCheck(UINT16 u16Filter)
//{
//	static UINT16 s_u16DcTempChkCnt;
//
//    if(g_u16MdlStatusExtend.bit.PFCOT == 0)
//    {
//
//        //set M1 borad temperature over flag
//        if(uiOverLevelChk(g_lq10TempPfc.lData, TEMP_PFC_HLOSS, u16Filter,
//            &s_u16DcTempChkCnt))
//        {
//            g_u16MdlStatusExtend.bit.PFCOT = 1;
//        }
//    }
//
//    else
//    {
//		//clr M1 borad temperature over flag
//        if(uiUnderLevelChk(g_lq10TempPfc.lData,TEMP_PFC_HBACK, u16Filter,
//            &s_u16DcTempChkCnt))
//        {
//            g_u16MdlStatusExtend.bit.PFCOT = 0;
//        }
//    }
//}

/*******************************************************************************
 *Function name: vDcVoltOverHandle()
 *Description :  DC voltage over check and handle
 *input:         void
 *global vars:   g_iq10MdlVolt: DC volt for protect
 *               g_u16MdlStatus: module status
 *               g_u16MdlStatus.bit.HVSDLOCK: DC volt over shut down lock flag
 *               g_u16MdlStatus.bit.DCOV : DC volt over flag
 *               g_u16MdlCtrl.bit.ONCELOCK: 杩囧帇閿佹缁炵獤鈮�??1:one time;
 *															 0:more time
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
static	void  vDcVoltOverHandle(void)
{
	static UINT16	s_u16OpenTime;// = 0;//restart count after the first HVSD(5S)

	//DC volt   over to lock for one time
	if (g_u16MdlCtrl.bit.ONCELOCK)
	{

       	if(g_lq10MdlVolt.lData >= g_lq10MdlVoltUp.lData)
		{
			g_u16MdlStatus.bit.HVSDLOCK = 1;
		}
    }
	//DC volt over to lock for more times
    else
    {
		//DC Voltage over check
		//vDcVoltOverChk(TIME100MS);  TIME3S
		vDcVoltOverChk(TIME4S);
	}

	// rectifier lock judge if over voltage twice
	// first dc over volt(SW), set to 1
	if ((g_u16MdlStatus.bit.DCOV)
		&& ((g_u16OverVoltStatus == DC_OV_STATE_NORMAL)
	    ||(g_u16OverVoltStatus == DC_OV_STATE_DELAY_5MINS)))
	{
	    g_u16OverVoltCounter++;
	    if(g_u16OverVoltCounter >= 100)
	    {
	        g_u16OverVoltCounter = 100;
	    }
		g_u16OverVoltStatus = DC_OV_STATE_OV;

	}
	//close dc then set to 2
	else if(g_u16OverVoltStatus == DC_OV_STATE_OV)    //xgh
	{
		//set module dc off flag
		s_u16MdlOpenBak |= DCDC_OFF;

		//close dc
		mOffDcdcPwm();

		//set module off status flag
		g_u16MdlStatus.bit.OFFSTAT = 1;

		//then set to 2	,and clr restart count
		g_u16OverVoltStatus = DC_OV_STATE_DELAY_5S;
		s_u16OpenTime = 0;
	}
	//count for 5s then set to 3
	else if (g_u16OverVoltStatus == DC_OV_STATE_DELAY_5S)
	{
		s_u16OpenTime++;
		// turn on after 5s if first over volt
		if (s_u16OpenTime >= TIME4S8)
		{
			g_u16OverVoltStatus = DC_OV_STATE_RESTART;
		}
	}
	//restart,then set to 4
	else if( (g_u16OverVoltStatus == DC_OV_STATE_RESTART)
	       &&( g_u16ActionReady >= DC_SOFT_START_INIT)
           &&(g_u16ActionReady <= DCOFF_SOFT_STATE) )
	{
		//restart DCDC because the first dc over voltage
		g_u16ActionReady = DC_SOFT_START_INIT;

		//then set to 4,and clr the count for second for HVSD
		g_u16OverVoltStatus = DC_OV_STATE_DELAY_5MINS;
		g_u16CloseTime = 0;
	}
	//begin to timing for 5min
	//if over volt again in 5min, lock; if not,
	//g_u16OverVoltStatus clear to 0

	else if (g_u16OverVoltStatus == DC_OV_STATE_DELAY_5MINS)
	{
		g_u16CloseTime ++;
		//calculate if over volt lock time is time out
		if (g_u16CloseTime >= (UINT16)(g_lq10ReonTime.lData >> 10) * 100)
		{
			g_u16OverVoltStatus = DC_OV_STATE_NORMAL;
			g_u16OverVoltCounter = 0;
		}
	}

	//if over volt again in 5min, lock
	/*if((g_u16MdlStatus.bit.DCOV)
		&& ((g_u16OverVoltStatus == DC_OV_STATE_RESTART)
		   ||(g_u16OverVoltStatus == DC_OV_STATE_DELAY_5MINS)))*/
	if((g_u16MdlStatus.bit.DCOV) && (g_u16OverVoltCounter == 100))
	{
		g_u16MdlStatus.bit.HVSDLOCK = 1;
		g_u16OverVoltCounter = 0;
	}
}

/******************************************************************************
 *Function name: vDcVoltOverChk()
 *Description :  Dc voltage over check
 *input:         u16Filter:chk times
 *global vars:   g_iq10MdlVolt: dc voltage for protect
 *               g_iq10MdlVoltUp: dc voltage over point
 *               g_u16MdlStatus: module status
 *               g_u16MdlStatus.bit.DCOV: DC voltage over flag
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
static  void  vDcVoltOverChk(UINT16 u16Filter)
{
    static UINT16 s_u16HvsdChkCnt;

    //current < 10%,no dc over
     // csm 090914
    if(g_lq10MdlCurr.lData < IDC_LOAD_10PCT) // csm 090914
    {
        g_u16MdlStatus.bit.DCOV = 0;
    }

    else
    {
        //DC Voltage over check
        if(g_u16MdlStatus.bit.DCOV == 0)
        {

            if(uiOverLevelChk(g_lq10MdlVolt.lData, g_lq10MdlVoltUp.lData,
                              u16Filter, &s_u16HvsdChkCnt))
            {
                g_u16MdlStatus.bit.DCOV = 1;
            }
        }

        else
        {
            if(uiUnderLevelChk(g_lq10MdlVolt.lData,
                               (g_lq10MdlVoltUp.lData - VDC_0V5),
                               u16Filter,&s_u16HvsdChkCnt))
            {
                g_u16MdlStatus.bit.DCOV = 0;
            }
        }
    }
}

/******************************************************************************
 *Function name: vDcOvpCheck()
 *Description :  DCovp check,if the signal active,dcdc shut down and locked
 *input:         u16Filter:chk times
 *global vars:   hiHvsd:DC volt over signal
 *               g_u16MdlStatus: module status
 *               g_u16MdlStatus.bit.HVSDLOCK: DC volt over shut down lock flag
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/

static   void	vDcHWHVSDRestart(void)

{
    static UINT16	s_u16HWOpenTime;//=0 ;

    if (   (g_u16MdlStatusExtend.bit.HWHVSD )
        && (g_u16HWOverVoltStatus == DC_OV_STATE_NORMAL)
        &&(g_u16MdlStatusExtend.bit.SSHORTSD==0)
        &&(g_u16SafetyShortStatus == DC_SAFE_SHORT_STATUS_NORMAL) )
    {
        g_u16HWOverVoltStatus = DC_OV_STATE_OV;

    }
    else if(g_u16HWOverVoltStatus == DC_OV_STATE_OV) // xgh
    {
        //set module dc off flag
        s_u16MdlOpenBak |= DCDC_OFF;

        //close dcdc PWM
        mOffDcdcPwm();

        //set module off status flag
        g_u16MdlStatus.bit.OFFSTAT = 1;

        //then set to 2	,and clr restart count
        g_u16HWOverVoltStatus = DC_OV_STATE_DELAY_5S;
        s_u16HWOpenTime = 0;
    }

    //count for 5s then set to 3
    else if (g_u16HWOverVoltStatus == DC_OV_STATE_DELAY_5S)
    {
        s_u16HWOpenTime++;
        // turn on after 5s if first over volt TIME500MS
        // for 30s
        if (s_u16HWOpenTime >= TIME4S8)
        {

            g_u16HWOverVoltStatus = DC_OV_STATE_RESTART;

            g_u16HVSDFistFlag =0;
        }
    }
    else if (  (g_u16HWOverVoltStatus == DC_OV_STATE_RESTART)
             &&( g_u16ActionReady >= DC_SOFT_START_INIT)
             &&(g_u16ActionReady <= DCOFF_SOFT_STATE) )
    {
        // reset HWHVSDDisp flag
        g_u16RunFlag.bit.HWHVSDDispRst = 1;

        //then set to 4,and clr the count for second for HVSD
        g_u16HWOverVoltStatus = DC_OV_STATE_DELAY_5MINS;
    }

    else if (g_u16HWOverVoltStatus == DC_OV_STATE_DELAY_5MINS)
    {
            g_u16HWOverVoltStatus = DC_OV_STATE_NORMAL;
    }
}


/******************************************************************************
 *Function name: vDcOvpCheck()
 *Description :  DCovp check,if the signal active,dcdc shut down and locked
 *input:         u16Filter:chk times
 *global vars:   hiHvsd:DC volt over signal
 *               g_u16MdlStatus: module status
 *               g_u16MdlStatus.bit.HVSDLOCK: DC volt over shut down lock flag
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/

static   void	vDcHWHVSDCheck (UINT16 u16Filter)
{
    static UINT16 s_u16DcovpCheckCnt;
    static UINT16 s_u16OvpDelayTimer;


    if(g_u16HVSDDebug ==1)
    {
       if(hiHvsd)
       {
          s_u16DcovpCheckCnt ++;

          if(s_u16DcovpCheckCnt >= u16Filter)
          {
             s_u16DcovpCheckCnt = u16Filter;
             g_u16MdlStatus.bit.HVSDLOCK = 1;
          }
       }

       else
       {
           s_u16DcovpCheckCnt = 0;
       }

    }
    else
    {

        if(g_u16MdlStatusExtend.bit.HWHVSD == 0)

        {
            if((hiHvsd)&&(( IsrVars._lVdcUse.iData.iHD - IsrVars._i16VdcSet )<= VDC_AD_0V5))
            {
                s_u16OvpDelayTimer = 0;
                s_u16DcovpCheckCnt ++;
                if(s_u16DcovpCheckCnt >= u16Filter) //10ms basetimer,total=100ms S96843
                {
                    s_u16DcovpCheckCnt = u16Filter;
                }
            }
            else if((hiHvsd)&&(g_lq10MdlVoltHvsd.lData>=(((INT32)60) << 10))) //System high voltage fault detection,S96843 202109
            {
                s_u16DcovpCheckCnt = 0;
                s_u16OvpDelayTimer++;  // 10ms basetimer
                if(s_u16OvpDelayTimer >= 500)  // 5s/10ms=500
                {
                   s_u16OvpDelayTimer = 500;       //5s delay off
                }
            }
            else
            {
                 s_u16DcovpCheckCnt = 0;
                 s_u16OvpDelayTimer = 0;
            }

            if((s_u16DcovpCheckCnt >= u16Filter)||(s_u16OvpDelayTimer >= 500))
            {
                 g_u16MdlStatusExtend.bit.HWHVSD = 1;
                 g_u16RunFlag.bit.HWHVSDDisp = 1;
                 g_u16HVSDFistFlag =1;
            }
         }
         else
         {
             s_u16DcovpCheckCnt = 0;
             s_u16OvpDelayTimer = 0;
             if(hiHvsd == 0)
             {
                 g_u16MdlStatusExtend.bit.HWHVSD = 0;
             }
         }
    }
    vDcHWHVSDRestart();
}


static   void vDcCurrShortCheck(UINT16 u16Filter)
{
	UINT16 u16DcVoltTemp;
	static UINT16 s_u16DcVoltChkCnt;
	
	u16DcVoltTemp = uiOverLevelChk(g_lq10MdlVolt.lData, VDC_SHORT_UP,u16Filter, &s_u16DcVoltChkCnt);
		
	 if((0 == g_u16MdlStatus.bit.SHORT)&&(1 == CputoClaVar._u16ShortFlag))
	 {	
		g_u16MdlStatus.bit.SHORT = 1;
		g_u16SaveShortCompleteFlag = 0;
	 }
	 else if((1 == u16DcVoltTemp)&&(1 == g_u16SaveShortCompleteFlag)&&(0 == CputoClaVar._u16ShortFlag))
	 {   
	     	g_u16MdlStatus.bit.SHORT = 0;
	 }
}

/******************************************************************************
 *Function name: vHWHVSDDispRstCtrl()   //skg20220212
 *Description: control HWHVSDDisp flag reset
 *input: u16Filter:chk times
 *global vars: g_u16RunFlag: module run status
               g_u16RunFlag.bit.HWHVSDDisp: DC volt over shut down Display flag
 *output:      void
 *CALLED BY:   vWarnCtrl()
 *Autor: s96843
 *****************************************************************************/
static   void   vHWHVSDDispRstCtrl (void)
{
    static UINT16 s_u16HWHVSDDispCnt;
    static UINT16 s_u16HWHVSDDispCnt1;

    if(g_u16RunFlag.bit.HWHVSDDispRst == 1)
    {
        if(g_u16ActionReady >= NORMAL_RUN_STATE)
        {
            s_u16HWHVSDDispCnt1 = 0;
            if(g_u16MdlStatusExtend.bit.HWHVSD == 0)
            {
                s_u16HWHVSDDispCnt ++;
                if(s_u16HWHVSDDispCnt >= TIME10S)
                {
                    s_u16HWHVSDDispCnt = TIME10S;
                    g_u16RunFlag.bit.HWHVSDDisp = 0;
                    g_u16RunFlag.bit.HWHVSDDispRst = 0;

                }
            }
            else
            {
                s_u16HWHVSDDispCnt = 0;
            }
        }
        else
        {
            s_u16HWHVSDDispCnt1++;
            if(s_u16HWHVSDDispCnt1 >= TIME150S)
            {
                s_u16HWHVSDDispCnt1 = TIME150S;
                if(g_u16MdlStatusExtend.bit.HWHVSD == 0)
                {
                    g_u16RunFlag.bit.HWHVSDDisp = 0;
                    g_u16RunFlag.bit.HWHVSDDispRst = 0;
                }
            }
        }
    }
    else
    {
        s_u16HWHVSDDispCnt = 0;
        s_u16HWHVSDDispCnt1 = 0;
    }
}

/******************************************************************************
 *Function name: vUnplugCtrl()
 *Description :  check the rectifier is plugged or not
 *input:         void
 *global vars:   g_u16MdlStatus: module status
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
void    vUnplugCtrl(void)//lzy 20200312
{
    static UINT16 s_u16UnPlugCnt, s_u16PlugCnt;

    if(GpioDataRegs.GPADAT.bit.GPIO14 == 1)//Unplug
    {
        s_u16UnPlugCnt ++;

        if(s_u16UnPlugCnt >= 3)
        {
            s_u16UnPlugCnt = 3;

            //g_u16MdlStatusExtend.bit.Unplug = 1;
            g_u16UnPlugFlag=1;

            //disable DCDC PWM
            mOffDcdcPwm();
            //disable pfc pwm
            mOffPfcPwm();

            mOffHmosDriv();
        }

        s_u16PlugCnt = 0;
    }
    else//Plug
    {
        s_u16PlugCnt ++;

        if(s_u16PlugCnt >= 10)
        {
            s_u16PlugCnt = 10;

            //g_u16MdlStatusExtend.bit.Unplug = 0;
            g_u16UnPlugFlag=0;
        }

        s_u16UnPlugCnt = 0;
    }
}



/******************************************************************************
 *Function name: vCanFailChk()
 *Description :  can fail chk,communication interrupt if 40s do not receive any
 *               information from scu.
 *input:         void
 *global vars:   g_u16MdlStatus: module status
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
static	void	vCanFailChk(void)
{
	//comm with scu interrupt cnt add
	g_u16CanFailTime ++;

	//communication interrupt after 40s for non adjust state
	if((g_u16CanFailTime > TIME40S) && g_u16PermitAdjust)
	{
		//set rectifier communication interrupt bit
		g_u16MdlStatus.bit.NOCONTROLLER = 1;

		//mask identify bit
		g_u16MdlCtrl.bit.IDENTIFY = 0;

		//and set rectifier on	flag
		g_u16MdlCtrl.bit.OFFCTRL = 0;

		//set moudle volt/curr/power to default value
		g_lq10SetVolt.lData = g_lq10MdlVoltFt.lData;
		g_lq10SetLimit.lData = g_lq10MdlCurrFt.lData;
		g_lq10SetPower.lData = g_lq10MdlPowerFt.lData;

		//set rectifier relay action to nomal mode
		g_u16MdlCtrl.bit.SHORTRELAY = 0;

		//clr the timer count
		g_u16CanFailTime = 0;

		InitCanaPointer();
		InitCanaIDPointer();
	}

	vCANIDResetChk();
}

/******************************************************************************
 *Function name: vCANIDResetChk()
 *Description :  can ID reset if ID is not 60,70 CanIdProtNo, re-inital receive reg ID
 *               information from scu.
 *input:         void
 *global vars:
 *output:        void
 *CALLED BY:     vCanFailChk
 ******************************************************************************/

static  void vCANIDResetChk(void)
{
    UINT16 u16CanIdProtNo;
    //uint32_t maskReg = 0U;
    union CAN_IF1CMD_REG CAN_IF1CMD_SHADOW;

    s_u16CANIDResetTimer++;

    if(s_u16CANIDResetTimer >= TIME1S)
    {
        s_u16CANIDResetTimer = 0;
        u16CanIdProtNo = (UINT16)((CanaRegs.CAN_IF2ARB.bit.ID>>20) & 0x000001FF);


        if((u16CanIdProtNo!=0x60)&&(u16CanIdProtNo!=0x70))
        {
            InitCanaPointer();
            InitCanaIDPointer();
        }
    }
}


/******************************************************************************
 *Function name: vParallelChk()
 *Description :  Parallel or not chk,if 8s do not receive any information from
 *               other module, it is signal system.not need load share
 *input:         void
 *global vars:   g_iq10MdlAvgCurr: system average current
 *               g_iq10MdlCurr: module current
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
static	void	vParallelChk(void)
{
	//cumulate the timer cnt
	g_u16Box0FailTimer ++;

	//if g_u16Box0FailTimer >8s,it is signal system , not need laod share
	if (g_u16Box0FailTimer >= TIME8S)
	{
 		g_u16Box0FailTimer = TIME8S;

		//the avg curr equal to module`s curr
		g_lq10MdlAvgCurr.lData = g_lq10MdlCurr.lData;

		g_u16MdlStatusExtend.bit.PARAFLAG = 0;
	}
}

/******************************************************************************
 *Function name: vLedCtrl()
 *Description :  Panel Led control
 *input:         void
 *global vars:   g_u16MdlStatus: module status
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
static	void	vLedCtrl(void)
{
	//count for yellow lit delay 10S timing
	static UINT16 s_u1610sTimer;

	//count for  LED twinkle action time 10ms once
	static UINT16 s_u16LedTimer;

   if ( g_u16LEDFlg == 0 )
   {
	       //LED twinkle  frequency, action time:500ms
      	if (++s_u16LedTimer >= TIME500MS)
	     {
		     g_u16RunFlag.bit.LIGHT = 1;
		     s_u16LedTimer = 0;
	     }


           	/*
            	//Watch-dog LED  twinkle control
	           if (g_u16RunFlag.bit.LIGHT)
	            {
	            	mWDLedTwink();
	            }
	         */
   	    //Green Led control
       	if ((g_u16MdlCtrl.bit.IDENTIFY) && (g_u16AddIdentifyNum == 3))
       	{
	    	//Green LED twinke during rectifier identifing
	    	if(g_u16RunFlag.bit.LIGHT)
	     	{
		    	mGreenLedTwink();
		    }
	    }
    	else
    	{
		    //when ac volt is normal, Green led is light
		    mGreenLedOn();
	    	g_u16MdlCtrl.bit.IDENTIFY = 0;
    	}


	      //Yellow Led control
       	//if AC/PFC uv/ov, power cut, over temp,
      	// current unbalance,Break away Relay
           //yellow led is light
	    if(mMdlProt_YellowLED())
    	{
	    	//Delay 10S to light yellow led
	    	s_u1610sTimer ++;

	    	if (s_u1610sTimer >= TIME3S)
	    	{
		    	s_u1610sTimer = TIME3S;
		    	mYellowLedOn();
		    }
	    }

    	else if(g_u16MdlStatus.bit.NOCONTROLLER)
    	{
	    	//Yellow LED  twinke  when rectifier communiaction interrupt
	    	if(g_u16RunFlag.bit.LIGHT)
	    	{
		    	mYellowLedTwink();
	    	}
	    	s_u1610sTimer = 0;
    	}

    	else
    	{
	    	//Yellow LED ligth off
	    	mYellowLedOff();
	    	s_u1610sTimer = 0;
    	}

    	//Red led control
     	//if dcvolt ov, severe current unbalance, ID overlap,Estop,
    	//light the red led

	    if(mMdlFault_RedLed())
    	{
	    	mRedLedOn();
    	}

    	else if(g_u16MdlStatus.bit.FANFAIL)
    	{
	    	//Red LED   twinke when fan fault
	    	if(g_u16RunFlag.bit.LIGHT)
	    	{
	    		mRedLedTwink();
	    	}
	    }

    	else
    	{
		    //Red LED ligth off
	    	mRedLedOff();
	    }
	}
	//宸ヨ娴�?���?��
    else if ( g_u16LEDFlg == 1 )
	{
	    mGreenLedOn();
		mRedLedOn();
		mYellowLedOn();

	}
	else if ( g_u16LEDFlg == 2)
	{
	    mRedLedOff();
		mGreenLedOff();
		mYellowLedOff();
	}


	//after LED control, action time:100*5 = 500ms flag clear
	g_u16RunFlag.bit.LIGHT = 0;

}

/******************************************************************************
 *Function name: vPfcLoopCtrlSwith()
 *Description :  pfc loop control swith
 *input:         void
 *global vars:   void
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
static	void	vPfcLoopCtrlSwith(void)
{
	//pfc softstart end timer cnt,delay 1.5s to enable pfc loop ctrl switch
	/*
	if(IsrVars._u16PfcLoopCount)
	{
		IsrVars._u16PfcLoopCount ++ ;
	}

	//pfc softstart end,delay2.5s to enable fan control
	if(IsrVars._u16PfcLoopCount >= TIME2S5)
	{
		IsrVars._u16PfcLoopCount = TIME2S5;
	}
    */

	//PFC exit quick loop timer control
	IsrVars._i16PfcQuickLoop ++ ;
	if(IsrVars._i16PfcQuickLoop >= TIME150MS)
	{
		/*
		if(IsrVars._u16VpfcPara == 2)
		{
			IsrVars._u16VpfcPara = 3;
		}
		*/
		IsrVars._i16PfcQuickLoop = TIME150MS;
	}

   //浣庢俯PFC鐜矾鍒囨崲锛�-24鍒�-26搴︿箣闂寸湅涓婁竴娆＄殑娓╁�?
	if(g_lq10TempAmbi.lData <= N_TEMP_26C)
	{
	   IsrVars._i16LowTempPFCloopFlag = 1;
	}
	else if(g_lq10TempAmbi.lData >= N_TEMP_24C)
	{
	   IsrVars._i16LowTempPFCloopFlag = 0;
	}



}

/******************************************************************************
 *Function name: vDcdcLoopCtrlSwith()
 *Description :  dcdc loop control swith
 *input:         void
 *global vars:   void
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
static	void	vDcdcLoopCtrlSwith(void)
{
     UINT16  u16RippleStartTimer;
     static UINT16 u16DcVoltPWMchgFlag;

   	//ripple loop ctrl鐩殑锛�?��鐩戞帶璁剧疆鐨勯檺娴佸�煎彂鐢熷彉鍖栨�?鍊欙紝绾规尝鐜笉璧蜂綔�?��2S
     if (g_u16ActionReady == NORMAL_RUN_STATE)
	 {
		//if ( g_lq10SetLimit.lData == s_lq10SetPowerLastValue.lData )
		if ( abs (g_lq10SetLimit.lData - s_lq10SetPowerLastValue.lData) < DCCURR_REGU_STEP)
		{
			if (IsrVars._u16RippleDisable == 1)	//褰撶洃鎺ц缃殑闄愭祦鍊煎彂鐢熷彉鍖栨�?鍊欙紝灞忚斀绾�?尝鐜�?2S 锛屼箣鍚庡厑璁镐娇鑳�?
			{

		    	g_u16RippleDisableCont ++; //g_u16RippleDisableCont = 锛�1锝濼IME2S-1锛�?紝灞忚斀绾�?尝鐜�?

				if (g_u16RippleDisableCont >= TIME2S)
				{
				    g_u16RippleDisableCont = TIME2S; //灞忚�?绾�?尝鐜悗2S锛屽厑璁告�?鍏ョ汗娉㈢幆
				}
			}
			else
			{
				 g_u16RippleDisableCont = 0;	 //鐩戞帶璁剧疆鐨勯檺娴佺偣娌℃湁鍙戠敓鍙樺寲锛屽厑璁镐娇鑳界汗娉㈢�?
			}
		}
		else //濡傛灉鐩戞帶闄愭祦鐐瑰彂鐢熷彉鍖�?.
		{

		    if(g_u16PermitAdjust == NORMAL_RUN_MODE)//鍦ㄦ甯稿伐浣滄ā�?��紝濡傛灉鐩戞帶闄�?��鐐瑰彂鐢熷彉鍖栵紝灏卞睆钄界汗娉㈢幆
			{
				IsrVars._u16RippleDisable = 1;
				g_u16RippleDisableCont = 1;//鐩戞帶璁剧疆鐨勯檺娴佺偣涓嶇瓑浜庡綋鍓嶆ā鍧�?殑闄�?��鐐圭汗娉㈢幆璺睆钄�?
			}
			else //�??�潡澶勫�?��″噯�?��椂锛屼笉灞忚�?绾�?尝鐜�?,濡傛灉璧锋満娌�?�畬鎴愶紝浼�?��鍚庨潰鐨�?��搴忔妸绾规尝鐜睆钄芥帀
			{
			    IsrVars._u16RippleDisable = 0;
				g_u16RippleDisableCont = 0;

			}
		}
	}
	s_lq10SetPowerLastValue.lData = g_lq10SetLimit.lData;


   	if(g_u16ActionReady != NORMAL_RUN_STATE)
	{
 	    IsrVars._u16RippleDisable = 1;
 	    IsrVars._u16SoftStartEndTimer = 0;
 	    g_u16RippleDisableCont = 0;	 //鐩戞帶璁剧疆鐨勯檺娴佺偣娌℃湁鍙戠敓鍙樺寲锛屽嵆鍦ㄥ紑鏈鸿繃绋�?腑涓嶅垽鏂洃鎺ц缃�?檺娴佺偣鐨勫彉鍖�?

	}

 	else
 	{
 		IsrVars._u16SoftStartEndTimer ++;
		//max timer:3000ms
 		if(IsrVars._u16SoftStartEndTimer >= DCSTARTEND_250S)
		{
			IsrVars._u16SoftStartEndTimer = DCSTARTEND_250S;
		}
 	}

	    //闈炴牎鍑嗘�侊紝 DC 鍚姩�?�屾垚鍚�?+WALKIN TIME(?)+ 25S 鍚庣汗娉㈢幆鍏佽鎶曞叆浣跨�?
	   if(g_u16PermitAdjust == NORMAL_RUN_MODE)
	   {
	     u16RippleStartTimer = DCSTARTEND_25S
	                          + (UINT16)(( ((INT32)g_u16MdlCtrl.bit.WALKIN) * g_lq10WalkInTime.lData )>>10);
	   }
	   //鏍�?�噯�?��紝DC 鍚姩�?�屾垚鍚�?+200mS 鍚庣汗娉㈢幆鍏佽鎶曞叆浣跨�?
	   else
	   {
	     u16RippleStartTimer = DCSTARTEND_200MS;
	   }

 	if((IsrVars._u16SoftStartEndTimer >= u16RippleStartTimer)
 	  &&((g_u16RippleDisableCont == TIME2S)||(g_u16RippleDisableCont == 0)))
	{

		//dcdc softstart end ,enable dcdc ripple loop
		if(g_u16DcRippSWCtrl==1)
		{
			IsrVars._u16RippleDisable = 0; //浣胯兘鎾嚿戯�?
		}
		else
		{
		    IsrVars._u16RippleDisable = 1; //灞忚�?绾�?尝鐜�?
		}
	}	// temporary delete, mhp 20100409 for migration
	else
	{
	 IsrVars._u16RippleDisable = 1; //灞忚�?绾�?尝鐜�?
	}


	if(g_lq10TempAmbi.lData <= N_TEMP_27C)
	{
		g_lq10LowTempWalkInTime.lData = ((INT32)6000) << 10;
	}
	else
	{
		g_lq10LowTempWalkInTime.lData = 0;
	}


		// low temp judge for DC voltage Loop parameters change  (wl20140226)
	if(g_lq10TempAmbiDisp.lData<=N_TEMP_10C)
	{
	   IsrVars._i16LowTempLoopflag = 1;
	 }
    else if((g_lq10TempAmbiDisp.lData<=N_TEMP_5C) && (IsrVars._i16LowTempLoopflag == 1))
	{
	  IsrVars._i16LowTempLoopflag = 2;
	}
	else
	{
	  IsrVars._i16LowTempLoopflag = 0;
	}
	//涓轰簡鏀瑰杽鏉傞煶锛�?45A浠ュ�?�?ｉ�DC鐢靛帇鐜�?1.4鍊嶅�??��
    //鍥犱负鎵�閫夊彉閲忛兘鏈夋潈閲嶉槻鎶栧姩婊ゆ尝锛屼笖鍒囨崲鐜矾�?��槸绋冲畾鐨勶紝鎵�浠ヤ笉璁惧洖�?�?
    if((IsrVars._u16LowVolDcLoopChgFlag == 0) && (g_lq10MdlVolt.lData < VDC_50V)
           && (g_lq10MdlCurrDisp.lData < IDC_45A ) )
   {
       IsrVars._u16LowPowerFlag2 = 1;
   }
   else
   {
       IsrVars._u16LowPowerFlag2 = 0;
   }
    //20200417 for Telephone noise
   if((g_lq10MdlVolt.lData < VDC_54V2) && (g_lq10MdlVolt.lData > VDC_52V5))
   {
      //IsrVars._i16NoiseLoopflag = 1;
       if(g_u16MdlStatusExtend.bit.PARAFLAG)
       {
           IsrVars._i16NoiseLoopflag = 0; //20201225  for system Telephone noise
       }
       else
       {
           IsrVars._i16NoiseLoopflag = 1;
       }

   }
   else if((g_lq10MdlVolt.lData > VDC_54V7) || (g_lq10MdlVolt.lData < VDC_52V))
   {
       IsrVars._i16NoiseLoopflag = 0;
   }
   if((IsrVars._i16NoiseLoopflag == 1) && (g_lq10MdlCurrDisp.lData > IDC_50A))
   {
       IsrVars._i16NoiseLoopflag2 = 1;
   }
   else if((IsrVars._i16NoiseLoopflag == 1) && (g_lq10MdlCurrDisp.lData < IDC_47A))
   {
       IsrVars._i16NoiseLoopflag2 = 0;
   }

   //qtest20201210
   if(IsrVars._u16DCDC_Nouse0G == 1)//Low_Bus_DelayFlag
   {
       IsrVars._u16DCDC_Nouse0H ++;//Low_Bus_DelayTimer
    }
   if(IsrVars._u16DCDC_Nouse0H > 2)
    {
       IsrVars._u16DCDC_Nouse0G = 0;//Low_Bus_DelayFlag
       IsrVars._u16DCDC_Nouse0H = 0;//Low_Bus_DelayTimer
    }
   //qtest20201210end
   //qtest20201214
   if(IsrVars._i16PFCFastLoopForce == 1)
   {
       IsrVars._u16DCDC_Nouse0I ++;
    }
   if(IsrVars._u16DCDC_Nouse0I > 60)
    {
       IsrVars._i16PFCFastLoopForce = 0;
       IsrVars._u16DCDC_Nouse0I = 0;
    }
   if(IsrVars._u16DCDC_Dynflag == 1)
   {
       IsrVars._u16DCDC_DynDelayCnt ++;
   }
   if(IsrVars._u16DCDC_DynDelayCnt > 100)
   {
       IsrVars._u16DCDC_DynDelayCnt = 0;
       IsrVars._u16DCDC_Dynflag = 0;
   }
   //qtest20201214end

    //20200323 for hot issue @low Vo
    //if(g_u16ActionReady >= NORMAL_RUN_STATE)
    if(g_u16ActionReady >= DC_SOFT_START_INIT)
    {
      /*  if( g_lq10MdlVolt.lData <= VDC_30V )
        {
             CputoClaVar._u16DCDriveCurveFLG = 1;
             //CputoClaVar._u16InterimVal = 152;//PWM start(38%)
             CputoClaVar._u16InterimVal = 103;//Duty 25.6%
        }
        else if(g_lq10MdlVolt.lData >= VDC_31V)
        {
            CputoClaVar._u16DCDriveCurveFLG = 0;
            CputoClaVar._u16InterimVal = 60;
        }*/
    }
    else
    {
        CputoClaVar._u16DCDriveCurveFLG = 0;
        CputoClaVar._u16InterimVal = 90;//15% duty, Piout=200

    }
    if( g_lq10MdlVolt.lData <= VDC_44V )
    {
       CputoClaVar._u16DCVsamlePointFLG = 1;
       CputoClaVar._u16DCADCCOEF = 20;

       CputoClaVar._u16VDCADCOffSet = 2;
       CputoClaVar._u16IDCADCOffSet = 2;

    }
    else if(g_lq10MdlVolt.lData >= VDC_45V)
    {
       CputoClaVar._u16DCVsamlePointFLG = 0;
       CputoClaVar._u16DCADCCOEF = 34;
       CputoClaVar._u16VDCADCOffSet = 24;
       CputoClaVar._u16IDCADCOffSet = 24;

    }

    /***************************鏍�?嵁杈撳嚭鐢靛帇PWM璋冩暣鍙戞尝鏉�?��?**************************/
    if(g_lq10MdlVolt.lData <= VDC_40V)
    {
        u16DcVoltPWMchgFlag = 1;
    }
    else if((u16DcVoltPWMchgFlag)&&(g_lq10MdlVolt.lData>=VDC_41V))
    {
        u16DcVoltPWMchgFlag = 0;
    }

    if(u16DcVoltPWMchgFlag == 1 )
    {
        CputoClaVar._u16InterimVal = 228 ;//38% duty, Piout=228
    }
    else if(u16DcVoltPWMchgFlag == 0 )
    {
         CputoClaVar._u16InterimVal = 90;//15% duty, Piout=90
    }
}
/******************************************************************************
 *Function name: vPermitOverAcHandle()
 *Description :  permit ac volt over handle
 *input:         void
 *global vars:   IsrVars._uiPermitOverAc: permit ac over flag
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
static	void	vPermitOverAcHandle(void)
{
	g_u1660sTimer ++;

	//AC over voltage,  force on for 60s,then recover AC over protect
    //IsrVars._u16PermitOverAcFlag鐀���?��佽杈撳叆鐢靛帇杩囧帇璧锋満锛岀敤浜庢祴璇昉FC鐜矾锛屾补鏈虹�?
	if ((g_u1660sTimer > TIME60S) && (IsrVars._u16PermitOverAcFlag == 0))
	{
		g_u1660sTimer = TIME60S;

 		IsrVars._u16PermitOverAc = IsrVars._u16PermitOverAc
 									| PERMIT_ACOVER_HANDLED;
	}
}

/******************************************************************************
 *Function name: vSequeOnTimeCal()
 *Description :  module seque on time handle
 *input:         void
 *global vars:   g_u16SequeOnTimer: count for rectifer open sequencely
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
static	void	vSequeOnTimeCal(void)
{
	static UINT16 s_u16SequeOnBaseTimer;

	s_u16SequeOnBaseTimer ++;

	//1s timer for module seque on
	if(s_u16SequeOnBaseTimer % TIME1S == 0)
	{
		g_u16SequeOnTimer ++;

		if (g_u16SequeOnTimer >= SEQUE_ON_TIME_2000S)
		{
			g_u16SequeOnTimer = SEQUE_ON_TIME_2000S;
		}
	}

	if(g_u16AddIdentifyFlag)
	{
		g_u16SequeOnTimer = 0;
	}

}


/******************************************************************************
 *Function name: vEStopClear()
 *Description :  clear Estop flag,disable Estop function
 *input:         void
 *global vars:   g_iq10AcVolt.lData: input voltage Vac
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
static  void  vEStopClear(void)
{
    if(g_u16MdlStatusExtend.bit.DCINPUTSTATUS == 0)//浜ゆ祦杈撳叆
    {
        if((g_u16MdlCtrl.bit.EStopFlag) && (g_lq10AcVolt.lData < VAC_DIS_DN + VAC_5V))
        {
            g_u16MdlCtrl.bit.EStopFlag = 0 ;
        }
    }
    else//鐩存祦杈撳叆
    {
        if((g_u16MdlCtrl.bit.EStopFlag) && (g_lq10AcVolt.lData < VDCIN_DIS_DN + VAC_5V))
        {
            g_u16MdlCtrl.bit.EStopFlag = 0 ;
        }
    }

}

/*******************************************************************************
*Function name:  vMixedSystemHandle()
*Description: mixed system chk and handle
 input:         void
 *global vars:
 *output:        void
 *CALLED BY:     vWarnCtrl()
*******************************************************************************/
static void  vMixedSystemHandle(void)
{
    if(g_u16PowerMixedFlag == 1)// 娣锋�?
    {
        //if need derating
        if(g_u16MdlCtrl.bit.NoDeratingFlag == 0)//闄嶅姛鐜�?��?�?
        {
            //if is 2900w
            if((g_u16PowerMixedSystemType & MIX_TYPE_2900W)==MIX_TYPE_2900W)

            {
                g_i32PowerRate = POW_2900W;
                g_i32CurrRate = CURR_RATE_2900W;
                g_i32CurrDisMax = IDC_UP_2900W;
                g_i32CurrLimtMax= IQ10_CURRLlMlTMAX_2900W;
                g_i32CurrLimtMaxExtra=IQ10_CURRLlMlTMAXEXTRA_2900W;

                g_i32CurrChgFactor = 1024;
                g_i32AvgCurrChgFactor = 1024;
            }

            else if(((g_u16PowerMixedSystemType & MIX_TYPE_3000e3)==MIX_TYPE_3000e3)
                         && (g_u16MdlCtrl.bit.NoDeratingFlag == 0))
            {

                g_i32PowerRate = POW_3000W;
                g_i32CurrRate = CURR_RATE_3000W;
                g_i32CurrDisMax = IDC_UP_3000W;
                g_i32CurrLimtMax= IQ10_CURRLlMlTMAX_3000W;
                g_i32CurrLimtMaxExtra=IQ10_CURRLlMlTMAXEXTRA_3000W;

            }
            else if(((g_u16PowerMixedSystemType & MIX_TYPE_3500e3)==MIX_TYPE_3500e3)
                         && (g_u16MdlCtrl.bit.NoDeratingFlag == 0))
            {
                g_i32PowerRate = POW_3500W;
                g_i32CurrRate = CURR_RATE_3500W;
                g_i32CurrDisMax = IDC_UP_3500W;
                g_i32CurrLimtMax= IQ10_CURRLlMlTMAX_3500W;
                g_i32CurrLimtMaxExtra=IQ10_CURRLlMlTMAXEXTRA_3500W;


            }
            else
            {
                g_i32PowerRate = IQ10POW_RATE;
                g_i32CurrRate = CURR_RATE;
                g_i32CurrDisMax = IDC_UP;
                g_i32CurrLimtMax= IQ10_CURRLlMlTMAX;
                g_i32CurrLimtMaxExtra=IQ10_CURRLlMlTMAXEXTRA;


            }



        }
        //no derating
        else if(g_u16MdlCtrl.bit.NoDeratingFlag == 1) // 涓嶉檷鍔熺巼
        {

            g_i32PowerRate = IQ10POW_RATE;
            g_i32CurrRate = CURR_RATE;
            g_i32CurrDisMax = IDC_UP;
            g_i32CurrLimtMax= IQ10_CURRLlMlTMAX;
            g_i32CurrLimtMaxExtra=IQ10_CURRLlMlTMAXEXTRA;

            if((g_u16PowerMixedSystemType & MIX_TYPE_2900W)==MIX_TYPE_2900W)
            {
                //3000W mixed with 2900w
                g_i32CurrChgFactor = 990;//848;//742;
                g_i32AvgCurrChgFactor = 1059;//1236; //1413;
            }

        }
    }
    else
    {
        g_i32PowerRate = IQ10POW_RATE;
        g_i32CurrRate = CURR_RATE;
        g_i32CurrDisMax = IDC_UP;
        g_i32CurrLimtMax= IQ10_CURRLlMlTMAX;
        g_i32CurrLimtMaxExtra=IQ10_CURRLlMlTMAXEXTRA;
        g_i32CurrChgFactor = 1024;
        g_i32AvgCurrChgFactor = 1024;
    }
}


/*=============================================================================*
 *Function name: vFanCtrl()
 *Description :  Fan speed control
 *input:         temperature and load, etc
 *global vars:   void
 *output:        void
 *CALLED BY:     void vWarnCtrl(void)
 *modify data:   20081209
 *============================================================================*/

void	vFanCtrl(void)
{
	ubitfloat  lTempU1Ctrl,lLoad;
    ubitfloat  lTempM1Ctrl;
    ubitfloat  lMAXM1Ctrl;
    ubitfloat  lVoutCtrl;
    ubitfloat  lTempSensorDelta;
    ubitfloat  lTempSensorPart;
    INT32   lNormalFanSpeedTemp;
	INT32   i32FanPowerPart;
	INT32   i32FanTU1Part;
	INT32   i32FanTM1Part;
    INT32   i32FanVoutPart;
    INT32   i32FanCTLPart;
	INT32   i32FanTM1Part2;
	INT32   i32FanDSPTEMPPart;

	i32FanDSPTEMPPart = 0;

    if(g_lq10TempAmbi.lData <= N_TEMP_27C)
    {
        g_i16FanRunMin	=  FAN_MIN_LOWTEMP; //60%  //浣庢�?���?��鍗犵┖姣斾负60%锛屽惁鍒欒浆涓嶈捣鏉�?
    }
    else if(g_lq10TempAmbi.lData >= N_TEMP_24C)
    {
        g_i16FanRunMin	=  FAN_RUN_MIN;//30%
    }

	if (g_u16FanEnable == FAN_RUN_ENABLE)
	{
    	//to ensure  below temp=28, 45% fan speed
		lTempU1Ctrl.lData = lUpdownlimit(g_lq10TempAmbiCtrl.lData ,TEMP_DIS_UP, TEMP_28C);

        //杈撳�?��熺巼瀵归鎵囩殑鎺у埗锛屽崐杞戒互涓�?畻鍗婅浇
        lLoad.lData = lUpdownlimit(g_lq10MdlPow.lData , IQ10POW_RATE, (IQ10POW_RATE>>1));
        ///////////////////////鏉挎�?��у埗�?庢�?�?���?/////////
		//PFC 鍜� DC 鏉挎�?��栨俯搴﹂�?���?��涓烘帶鍒堕鎵囬�熷害鐨�?��娓�
		lMAXM1Ctrl.lData = g_lq10TempPfcDispSave.lData;

		if(lMAXM1Ctrl.lData < g_lq10TempDcDispSave.lData)
		{
		    lMAXM1Ctrl.lData = g_lq10TempDcDispSave.lData;
		}
        //姝ｅ父�?ヤ綔鐘舵�佹澘娓╂渶楂�65搴�////AC 176V DC48V 婊¤浇 鐜�?55/////////////////
        //65搴﹀紑濮�??�?��,75搴﹀埌鍏ㄩ��?///////////

        lTempM1Ctrl.lData = lUpdownlimit(lMAXM1Ctrl.lData , TEMP_150C, TEMP_65C);

        ///////////////////////杈撳�?��靛帇鎺у埗椋庢墖閫熷�?/////////
        //DC48V锛屽姞閫熷洜瀛愪�?0锛孌C42V,鍔犻�熷洜�?�愬埌�?￠��?//////////

        lVoutCtrl.lData = lUpdownlimit(g_lq10MdlVolt.lData , VDC_48V, VDC_42V);

		//-------------鍒嗗埆璁＄畻鍔熺巼銆佺幆娓┿�佹澘娓┿�佽緭鍑虹數鍘�?��椋庢墖鐨�?��鍒堕�?--------------------////
		//--鍔熺巼�?�归鎵囩殑鎺у埗閲�--//
        i32FanPowerPart = (((lLoad.lData-(IQ10POW_RATE>>1)) * FAN_P_FACTOR)>>16);//
        //--鐜�??�归鎵囩殑鎺у埗閲�--//
        i32FanTU1Part = (((lTempU1Ctrl.lData - TEMP_28C) * FAN_T_FACTOR) >> 16);//
        //--鏉挎�??�归鎵囩殑鎺у埗閲�--//
        i32FanTM1Part = (((lTempM1Ctrl.lData - TEMP_65C) * FAN_M_T_FACTOR ) >> 10);//
        //--杈撳�?��靛帇瀵归鎵囩殑鎺у埗閲�-杞捣鍜屽叧鏈烘椂涓嶄綔�?��--//
        if( g_u16ActionReady == NORMAL_RUN_STATE)
        {
            i32FanVoutPart = (((VDC_48V - lVoutCtrl.lData ) * FAN_Vout_FACTOR ) >> 10);
        }
        else
        {
            i32FanVoutPart = 0 ;
        }
		//绱姞鍚�?��閲忓�??庢�?鐨勬帶鍒�?
		i32FanCTLPart = i32FanPowerPart + i32FanTU1Part + i32FanTM1Part + i32FanVoutPart;

        //-------------瀵瑰姛鐜囥�佺幆娓┿�佹澘娓╁椋庢墖鐨�?��鍒堕噺涓�?,涓�?檺骞�?------------------////

        i32FanCTLPart = lUpdownlimit(i32FanCTLPart , (INT32)FAN_LOW, (INT32)0);

		//-------------鏍�?嵁鍔熺巼銆佺幆娓┿�佹澘娓╄绠楅鎵囪浆閫�?------------------////
       	IsrVars._i16VFanSet  = FAN_LOW - (INT16)i32FanCTLPart; //鏈�浣庤浆閫熶负45%

		//------琛ヤ�?-------璁＄畻棰濆畾�?��澘娓╁椋庢墖鐨�?��鍒堕�?--------------------------------////
          //------------棰濆畾鎬侊紙鐜�?30搴︿笅锛岃緭鍑虹數鍘�?53.5鍦�0.5鍋忓樊鍐咃紝杈撳叆鐢靛帇220V鍋忓�?10V鍐咃�?---//
		//棰濆畾鎬佷笅锛屽洜涓烘渶浣庨鎵�?���?��洿浣�?��鎵�浠ユ澘娓╁�?�?��洿蹇�?
		i32FanTM1Part2 = (((lTempM1Ctrl.lData - TEMP_65C) * FAN_M_T2_FACTOR ) >> 10);
       //鏉挎�?��у埗�?庢�?鍊煎�?��濆畾宸ヤ綔鎬佷笅鐨勪笂涓�??骞�
        i32FanTM1Part2 = lUpdownlimit(i32FanTM1Part2 , (INT32)FAN_EXTRA_LOW, (INT32)0);

		/*------------琛ヤ�?2160104LQC----
		鑰冭檻鍒板父姘斿帇涓�?��鑺�?��﹁灏忎�?140deg锛�
		2000m涓�?灏忎簬150deg鐨勬�?��﹂�?鍊硷�?
		鍦ㄩ�?�氬�?���?��涓�涓狥anSpeed  vs dsp鍐呴儴娓╁�?鎺у埗椋庢墖杞�熺殑鍙犲姞鍑芥暟锛圶B)*/
        /*if (( g_lq10TempSensor.lData >= TEMP_85C )
           && ( g_lq10TempSensor.lData <= TEMP_100C ))
		{

            i32FanDSPTEMPPart = \
		    (((g_lq10TempSensor.lData - TEMP_85C) * FAN_INTERNAL_DSPTEMP_FACTOR ) >> 10);

		}
		else if ( g_lq10TempSensor.lData > TEMP_100C )
		{
		    i32FanDSPTEMPPart = FAN_82_PERCENT;
		}
		else
		{
			i32FanDSPTEMPPart = 0;
		}*/

         //------琛ヤ�?-------璁＄畻棰濆畾�?��鎵囩殑鎺у埗閲�--------------------------//
         //----鍗冲彧瑕佹澘娓╂病鏈夊紓�??崌�?橈紝�?��繚鎸丗AN_EXTRA_LOW------------------////
        if( (g_lq10TempAmbiCtrl.lData < TEMP_30C)
             &&(labs(g_lq10MdlVolt.lData - VDC_53V5) < VDC_0V5 )
             &&(labs(g_lq10AcVolt.lData- VAC_220V) < VAC_10V)  )
		{
         	g_u16FanCtrFlag = 0;
		}
		// 鍥炲樊锛岄潪棰濆畾鎬佸氨閲囩�?srVars._i16VFanSet-----------------------------///
		else if (  (g_lq10TempAmbiCtrl.lData > TEMP_33C)
                 ||(labs(g_lq10MdlVolt.lData - VDC_53V5) > VDC_0V7)
                 ||(labs(g_lq10AcVolt.lData- VAC_220V) > VAC_15V) )
      	{
         	g_u16FanCtrFlag = 1;
        }

		/*鍘熺瓥鐣ュ湪鍥炲樊鍖轰负淇濇寔杞�燂紝杩欐牱浼�?��鐜拌浆閫熶笉闅忚礋杞藉彉鍖栫殑鎯�?��锛�
       	澧炲姞鍙�?噺浣�?负鏍囧織浣嶏紝璁╁洖宸�?��濇寔璇ュ彉閲忓�硷紝鍦ㄥ悗闈㈠啀鏍�?嵁璇ユ爣蹇椾綅纭畾椋庢墖杞�熴��?
        */
		if (g_u16FanCtrFlag== 0)
		{
		      //瑕佽�冭檻绯荤粺椋庢墖鍣煶
			//棰濆畾鎬佷笅杩樻槸鍒嗙郴缁熷拰鍗曟ā鍧楋紝褰�12涓郴缁熸ā鍧�?苟鏈猴紝杩涘叆绯荤粺椋庢墖璋�?��熸潯浠�
			//if ( g_u16MdlNumber == MODL_NUMBER_12 )
		    if ( g_u16MdlNumber == MODL_NUMBER_8 )
	        {
			    lNormalFanSpeedTemp = vSystemFanCtrl();
            }
			/*else if ( g_u16MdlNumber == MODL_NUMBER_20 )
			{

			    lNormalFanSpeedTemp = vSystemFanCtrl20();

			}*/
            else //鍗曟ā鍧�?鎵�?��瀹氭�佺瓥鐣�?��鑰冭檻鏁堢巼锛岄鎵�?��杞揩鐐�?
			{
                lNormalFanSpeedTemp = vSingleModelFanCtrl();
			}


			g_i16FanSpecialSet  =  (INT16)lNormalFanSpeedTemp - (INT16)i32FanTM1Part2\
		                              -(INT16)i32FanDSPTEMPPart;

		    lTempSensorDelta.lData= g_lq10TempSensor.lData - g_lq10TempAmbiDisp.lData;


		   if (lTempSensorDelta.lData>= TEMP_55C )
		   {
		    // add 1c ,fanset add 80cnt  //cons 5min
		    //_IQ10mpy(lq10Temp.lData ,IQ10_DEF_VPFCFACTOR);
		       s_i16DspTempPartTimer++;
		    //5min*60*1000/10ms=
		       if(s_i16DspTempPartTimer>30000)//5min in
		       {
		          s_i16DspTempPartTimer=30000;
		          lTempSensorPart.lData=  lTempSensorDelta.lData-TEMP_55C;
		          lTempSensorPart.lData= _IQ10mpy(lTempSensorPart.lData ,80);
		        }
		        else
		         {
		            lTempSensorPart.lData=0;
		         }
          }
		  else
		  {
		      lTempSensorPart.lData=0;
		      s_i16DspTempPartTimer=0;
		  }

		   g_i16FanSpecialSet = g_i16FanSpecialSet - (INT16) lTempSensorPart.lData;

		}
		else if ( g_u16FanCtrFlag== 1)
		{
		    g_i16FanSpecialSet  = IsrVars._i16VFanSet;
		}


		//-------------濡傛灉棰濆畾�?��ˉ涓佽绠�?緱鍒�?��椋庢墖杞�熷皬锛堝嵆鍙嶉�昏緫锛屾暟閲忓�煎ぇ锛�---------//
		//-------------灏遍噰鐢ㄦ洿灏忚浆閫�?--------------------------------------------------//
        if(g_i16FanSpecialSet  > IsrVars._i16VFanSet)
		{
            IsrVars._i16VFanSet = g_i16FanSpecialSet;
		}

        if((g_u16MdlCtrl.bit.FANFULL == 1)
		    ||(IsrVars._i16VFanSet <= FAN_HIGH)
		    ||(lTempM1Ctrl.lData > TEMP_75C))
		{
			IsrVars._i16VFanSet = FAN_HIGH;	//鍙嶉�昏�?
		}
		//闄愬埗甯告俯鍜屼綆娓╃殑鏈�浣庤浆閫�?
		if(IsrVars._i16VFanSet >= g_i16FanRunMin)
		{
			IsrVars._i16VFanSet = g_i16FanRunMin;
		}
	}
	else
	{
		IsrVars._i16VFanSet = FAN_CMP_MIN ;
	}

	if(g_u16MdlStatus.bit.FANFAIL == 1)	//褰撻鎵囨晠闅滄椂锛岄┍鍔ㄧ�?3S鏈�灏忚捣鏈哄崰绌烘�?���?�?12S鏈�灏忓崰绌烘瘮锛�15S涓�涓懆鏈�?
	{
        g_i16FanFailTimer2 = g_i16FanFailTimer2 + 1;
        if( g_i16FanFailTimer2 > TIME15S)  //0~15S鍛ㄦ湡�?幆
        {
            g_i16FanFailTimer2 = 0;
        }

        if( g_i16FanFailTimer2 < TIME3S )
        {
            //椋庢墖椹卞姩涓嶈兘鏈�灏忓崰绌烘�?��ヤ究FAN_fail娑堥櫎鍚�?��椋庢墖鑳界粰鍑烘纭殑椋庢墖�?ｅ父淇�?�彿
            IsrVars._i16VFanSet	= g_i16FanRunMin;//FAN_CMP_MIN;
        }
        else
        {
            IsrVars._i16VFanSet = FAN_CMP_MIN ;
        }
    }
    else
    {
        g_i16FanFailTimer2 = 0;
    }

	//AC鍏虫�?锛屼絾閮ㄥ垎鏉�?��?浠嶇劧淇濇寔椋庢墖�?ヤ綔锛岃闄�?��椋庢墖杞�熶负灏介噺浣�?��鍚﹀�?85V杈撳叆姣嶇嚎鍙兘涓嶈兘缁存寔杈�?��婧愬伐浣�?
	if(mMdlOff_PFCDC_FanOn())
	{
        //浣庢�?��ㄩ瀹氱數鍘�?��椋庢墖璧锋満銆�
        if(   (g_u16MdlStatus.bit.FANFAIL)
         && (g_i16FanRunMin == FAN_MIN_LOWTEMP)
         && (g_lq10AcVoltUse.lData > VAC_132V ) )
        {
            if( IsrVars._i16VFanSet < FAN_MIN_LOWTEMP )//闄愬埗�?庢�?鏈�楂�?浆閫�?
            {
                IsrVars._i16VFanSet = FAN_MIN_LOWTEMP;
            }
        }
        else
        {
            if( IsrVars._i16VFanSet < FAN_EXTRA_LOW )//闄愬埗�?庢�?鏈�鍜︻��?
            {
                IsrVars._i16VFanSet = FAN_EXTRA_LOW;
            }
        }
	}

    //椋庢墖璋�?��熸寰勮缃�?
	if ((IsrVars._i16VFanSet - g_i16FanPwmSet) > FAN_DELTA)
	{
        if(g_i16FanPwmSet > g_i16FanRunMin)  //鍦≧UNMIN鍜屾渶灏忚浆�?��箣闂�?��鎵囨�?�?��灏�
        {
            g_i16FanPwmSet = g_i16FanPwmSet + FAN_DELTA_SMALL;

        }
        if(g_i16FanPwmSet <= g_i16FanRunMin)  //鍦≧UNMIN鍜屾渶�?樿浆�?��箣闂�?��鎵囨�?�?��澶�
        {
            g_i16FanPwmSet = g_i16FanPwmSet + FAN_DELTA;
        }
	}

	else if ((g_i16FanPwmSet - IsrVars._i16VFanSet) >FAN_DELTA)
	{
		g_i16FanPwmSet = g_i16FanPwmSet - FAN_DELTA;
	}
	else
	{
		g_i16FanPwmSet = IsrVars._i16VFanSet;
	}

	g_i16FanPwm = g_i16FanPwmSet;

	if(g_i16FanPwm <= 1)
    {
       	g_i16FanPwm = 1;
    }
    else if(g_i16FanPwm >= FAN_CMP_MIN)
    {
       	g_i16FanPwm = FAN_CMP_MIN;
    }

	FANPWN_ECAP	= (Uint32)g_i16FanPwm ; //use shadow mode

   //椋庢墖璋�?���?��
	if(g_i16FANADJFLAG == 1)
	{
        if(g_i16FANADJVAL >= FAN_CMP_MIN)
        {
            g_i16FANADJVAL = FAN_CMP_MIN;
        }
        else if(g_i16FANADJVAL <= 1)
        {
            g_i16FANADJVAL = 1;
        }
        FANPWN_ECAP	= (Uint32)g_i16FANADJVAL; //use shadow mode
	}

}


/*=============================================================================*
 *Function name: vPFCPhaseComplement()
 *Description :  PFC CUR/VOL PHASE delay complement
 *input:         g_lq10MdlPow.lData,g_lq10AcVolt.lData etc
 *global vars:   void
 *output:        IsrVars._u16AcVoltPhaseshift
 *CALLED BY:     void vWarnCtrl(void)
 *modify data:   20081209
 *============================================================================*/
static	void  vPFCPhaseComplement(void)
{
   ubitfloat lq10Temp,lq10TempA;
   Uint16 u16PhaseshiftTemp;

  // lq10PFCCurr.lData =  _IQ10mpy(g_lq10MdlPow.lData,g_lq10AcVolt.lData);
   lq10Temp.lData =  _IQ10div(g_lq10MdlPow.lData,g_lq10AcVolt.lData); //蹇界暐浜嗘晥鐜囷紝杈撳嚭鍔熺巼闄や互杈撳叆鐢靛帇寰�?埌杈撳叆鐢垫�?

	if(labs(lq10Temp.lData - g_lq10PFCCurr.lData) < IPFC_0A3	)//4000W/220V*0.02=0.36A锛岄槻鎶栧鐞�
	{
		lq10Temp.lData = (g_lq10PFCCurr.lData * IQ10_ADC_PFCCURR_FILTA
		                  + lq10Temp.lData * IQ10_ADC_PFCCURR_FILTB) >> 10;
	}

  //  g_lq10PFCCurr.lData = lq10Temp.lData + 1024 ;
    g_lq10PFCCurr.lData = lq10Temp.lData;

    if(IsrVars._i16PfcDynFlg <= PFC_DYN_FLG_LOW_LIMIT)//濡傛灉娌℃湁澶勪簬PFC鍔ㄦ�佷笖�?舵�?涓�娈垫椂闂�?
   {
     g_u16PFCphaseADJFLAG =1;//浣胯兘PFC鐢垫祦瓒�??鐩镐綅琛ヤ�?
   }
   else //濡傛灉澶�?��PFC鍔ㄦ��
   {
     g_u16PFCphaseADJFLAG =0;//涓嶄娇鑳絇FC鐢垫祦瓒�??鐩镐綅琛ヤ�?
   }
  //娣欐安缂岄灟绾宠埛�?�柟�?斻剸鎸ョ編閱�?��顑㈣穻杈斿緤閽┿偟鍝閿�?鎾�?
  if((g_u16ActionReady == NORMAL_RUN_STATE)&&(g_u16PFCphaseADJFLAG ==1))
  {

       if(g_lq10PFCCurr.lData >= IPFC_16A)
    	{
        	g_lq10PFCCurr.lData = IPFC_16A;

    	}

        else if(g_lq10PFCCurr.lData < IPFC_0A3)
    	{
	      g_lq10PFCCurr.lData = IPFC_0A3;

     	}
        //杈撳叆鐢�?��锛�2.5A~16A鎷熷悎鏇�?��寰�?埌瓒�??鐩镐綅琛ュ伩鍊�
	   if( (g_lq10PFCCurr.lData > IPFC_2A5)&&(g_lq10PFCCurr.lData <= IPFC_16A))
    	{
           lq10TempA.lData = _IQ10mpy(g_lq10PFCCurr.lData,13);
           lq10TempA.lData = _IQ10div(lq10TempA.lData,10000);
           lq10TempA.lData = (INT32)23-lq10TempA.lData;
           // IsrVars._u16AcVoltPhaseshift = (UINT16)lq10TempA.lData ;
            u16PhaseshiftTemp = (UINT16)lq10TempA.lData ;

    	}
        //杈撳叆鐢�?��锛�0A~2.5A鎷熷悎鏇�?��寰�?埌瓒�??鐩镐綅琛ュ伩鍊�
	    else if((g_lq10PFCCurr.lData <= IPFC_2A5)&&(g_lq10PFCCurr.lData >=0))
    	{
    	   lq10TempA.lData = _IQ10mpy(g_lq10PFCCurr.lData,13);
           lq10TempA.lData = _IQ10div(lq10TempA.lData,1000);
           lq10TempA.lData = (INT32)59-lq10TempA.lData;
           //IsrVars._u16AcVoltPhaseshift = (UINT16)lq10TempA.lData ;
           u16PhaseshiftTemp = (UINT16)lq10TempA.lData ;
    	}
       /*
    	if(IsrVars._u16AcVoltPhaseshift >45)
    	{
    	 IsrVars._u16AcVoltPhaseshift = 45;
        }
    	*/
       //瓒呭墠鐩镐綅琛ュ伩鍊肩殑涓婁笅闄�?��
       if(u16PhaseshiftTemp > PFCPHASESHIFTUPVAL)
	   {
	       u16PhaseshiftTemp = PFCPHASESHIFTUPVAL;
     	}

         /*
     	if(IsrVars._u16AcVoltPhaseshift <1)
       	{
	       IsrVars._u16AcVoltPhaseshift = 0;
	    }
        */

	    else if(u16PhaseshiftTemp < PFCPHASESHIFTCLRVAL)
    	{
	      u16PhaseshiftTemp = (UINT16)0;
	    }

   }

   else //濡傛灉娌℃湁澶勪簬PFC鍔ㄦ�佷笖�?舵�?涓�娈垫椂闂翠笖澶勪簬NORMALRUN鐘舵�侊紝鐩镐綅琛ュ伩涓�?0
   {
     //IsrVars._u16AcVoltPhaseshift = 0;
     u16PhaseshiftTemp = 0;
   }

   IsrVars._u16AcVoltPhaseshift = u16PhaseshiftTemp;
}

/*=============================================================================*
 *Function name: vPFCSoftStartFail()
 *Description :  PFC softstart Fail
 *input:         IsrVars._lVpfcProtect.iData.iHD  etc
 *global vars:   void
 *output:        g_u16MdlStatusExtend.bit.PFCFAIL
 *CALLED BY:     void vWarnCtrl(void)
 *modify data:   20131209
 *============================================================================*/
static void vPFCSoftStartFail(void)
{
	//ls change for PFC START fail 20111129
	//HA415EZ姝ょPFC璧蜂笉鏉ョ殑鐩存帴鍖匬FCUV锛屽苟涓擯FCUV涔熶笂鎶ョ洃鎺�
   /*
	if(   ((g_u16ActionReady == DC_SOFT_START_INIT) || (g_u16ActionReady == PFC_SWSOFT_START_INIT))
	   && (g_lq10AcVolt.lData >= VPFC_SSTART_VAC_LIM) )
   */

	if(    ( ( mPFCSSFailCond1()) || ( mPFCSSFailCond2()) )
	    && (g_u16MdlStatus.bit.CALIBRFAIL == 0)  )

	{
	//	if(IsrVars._lVpfcProtect.iData.iHD < g_u16VpfcAdDcStart)
	    if(IsrVars._lVpfcProtect.iData.iHD < VPFC_AD_DC_START)
		 {
		   g_u16PFCUVCheckCnt ++;
           if(g_u16PFCUVCheckCnt >= TIME60S)
		   {
		        g_u16PFCUVCheckCnt = TIME60S;
		    	g_u16MdlStatusExtend.bit.PFCFAIL = 1;
		    	g_u16PfcFailRestart = g_u16PfcFailRestart + 1; //濡傛灉PFCFail灏濊�?��嶅惎锛屽鏋滆繕鏄捣涓嶆潵鍒欓�??
				if(g_u16PfcFailRestart > 1000)
				{
				 g_u16PfcFailRestart = 1000;
				}
            	 //R48-3000e3 璁捐涓哄綋PFCFAIL鍑虹幇瑕佸皾璇曢噸鏂拌蒋璧峰姩锛屽綋PFCFAIL鏃�?紝璁句负鎶奼_u16ActionReady璋冧负AC鍏虫�?鐘舵�侊�?
	            //鍥犱负鍦�?OFF鍏虫�?閲屾病鏈塒FCFAIL鐨勬�?��讹紝鎵�浠ュ鏋滄病鏈�?叾浠栨晠闅滀細閲嶆柊鍚姩銆�?

	           if( g_u16PfcFailRestart == 1)
	           {
                  //g_u16PfcFailRestart = 0; //濡傛灉PFCFail灏遍噸鍚蒋鍚�?
                   g_u16PFCUVCheckCnt = 0;  //閲嶅惎浠ュ悗灏遍噸鏂拌鏁帮紝鍐嶅60S锛屽氨鍐嶆閲嶆柊鍚姩
		           vModelTurnOff_PfcDc(); //鍏砅FCDC锛屽鏋滄病鏈�?病鏈夊叾浠栨晠闅滃氨浠庣�?0姝ュ紑濮�?��鍚�

	           }

	           if(g_u16PfcFailRestart > 1)
	           {
	              if(  (g_lq10RunTime.lData <  TIME_HOUR_6MONTH)
	                 ||(g_u16RepairEnable ==  RECDISABLE) )
	              {
	                 g_u16PFCUVCheckCnt = 0;  //閲嶅惎浠ュ悗灏遍噸鏂拌鏁帮紝鍐嶅60S锛屽氨鍐嶆閲嶆柊鍚姩
		             vModelTurnOff_PfcDc();//鍏砅FCDC锛屽鏋滄病鏈�?病鏈夊叾浠栨晠闅滃氨浠庣�?0姝ュ紑濮�?��鍚�
	              }
	              else
	              {
	   	            //  g_lFailFlag.lData = RECPFCFAIL;
	              }

	           }

		    }

	     }
	     else//鐩墠姣嶇嚎鐢靛帇澶т�?380灏辩畻鏄垚鍔熼噸鍚簡锛岄�?���?��鍏ョ殑鎯�?��鏆傛椂涓嶇浜嗐��
	     {
	    	g_u16MdlStatusExtend.bit.PFCFAIL = 0;
	    	g_u16PFCUVCheckCnt = 0;
		    g_u16PfcFailRestart = 0;

	     }

	 }
	 else
 	 {
	 	g_u16PFCUVCheckCnt = 0;

	 }
	 //鍙湁褰揚FC閲嶆柊鍚姩锛屽苟鍚姩鎴愬姛PFCFAIL鐨勬爣蹇楁�?浼氭竻闄ゃ��?
	 	 if(  (g_u16ActionReady == DC_SOFT_START_INIT)
	    ||(g_u16ActionReady == DC_SOFT_START)
		||(g_u16ActionReady == NORMAL_RUN_STATE)  )

     {
      	g_u16PFCUVCheckCnt = 0;
    	g_u16MdlStatusExtend.bit.PFCFAIL = 0;//鍙湁妯″潡鑳藉�?ｇ�?�閲嶅惎鎴愬姛鎵嶆妸PFCFAIL娓�0
	 	g_u16PfcFailRestart = 0;


     }
}


//Avoid Maitain Handle
/******************************************************************************
 *Function name: vAvoidMaitainHandle(void)
 *Description :   Avoid Maitain Handle
 *input:         void

 *global vars:   void
 *output:        void
 *            	 void
 *CALLED BY:     Warnctrl()
*******************************************************************************/
//static void vAvoidMaitainHandle(void)
//{  	Uint16 Status;
//  	g_u32RdFailFlag = g_u32FailFlagPointer;
//
//	if(g_u32RdFailFlag == RECPFCFAIL)
//	{
//	  g_u16MdlStatusExtend.bit.PFCFAIL = 1;
//	}
//	if(g_u32RdFailFlag == RECDCBRK)
//	{
//	  g_u16MdlStatusExtend.bit.DCFAULT = 1;
//	}
//	if (g_lFailFlag.lData && (g_u16RepairEnable == RECENABLE) )
//	{
//	    g_u16DisableOn = 1;  //浣胯蒋鍚姩姝�?�?���?0姝�
//		if((g_u32FailFlagPointer) == 0xFFFFFFFF)//褰揊LASH涓病鏈夐槻缁翠慨璁板綍鏃舵墠鍐欏叆
//		{
//			//close dcdc PWM
//		//	mOffDcdcPwm();
//			//close pfc pwm
//		//	mOffPfcPwm();
//
//			vModelTurnOff_PfcDc();
//			PieCtrlRegs.PIECTRL.bit.ENPIE = 0;
//			DisableDogPointer();
//			Status = Example_CsmUnlock();
//	  		if(Status == STATUS_SUCCESS)
//	   		{
//				Example_CallFlashAPI();
//	   		}
//// Resecure
//
//			EALLOW; //CSMSCR register is EALLOW protected.
//			CsmRegs.CSMSCR.all = 0x8000;
//			EDIS;
//
//			EnableDogPointer();
//   			PieCtrlRegs.PIECTRL.bit.ENPIE = 1;
//		}
//	}
//
//}



//#pragma CODE_SECTION(Example_CallFlashAPI,"IsrRamfuncs");
//Uint16 Example_CallFlashAPI(void)
//{
//   Uint16  Status;
//   Uint16  *Flash_ptr;     // Pointer to a location in flash
//   Uint16  Repair_buf[2];
//   Uint32  Length;         // Number of 16-bit values to be programmed
///*------------------------------------------------------------------
//  Program Flash
//------------------------------------------------------------------*/
//
//	Flash_ptr = (Uint16 *)0x3F7F7E;//0x3F0010;
//	Length = 2;
//
//		Repair_buf[0] = ( Uint16)g_lFailFlag.iData.iLD;
//		Repair_buf[1] = ( Uint16)g_lFailFlag.iData.iHD;
//	Status = Flash_Program(Flash_ptr, Repair_buf,Length,&FlashStatus);
//
//    // Verify the values programmed.  The Program step itself does a verify
//    // as it goes.  This verify is a 2nd verification that can be done.
//    Status = Flash_Verify(Flash_ptr,Repair_buf,Length,&FlashStatus);
//
//	return(Status);
//}


/*------------------------------------------------------------------
   Example_CsmUnlock

   Unlock the code security module (CSM)

   Parameters:

   Return Value:

            STATUS_SUCCESS         CSM is unlocked
            STATUS_FAIL_UNLOCK     CSM did not unlock

   Notes:

-----------------------------------------------------------------*/
//Uint16 Example_CsmUnlock()
//{
//    volatile Uint16 temp;
//
//    // Load the key registers with the current password
//    // These are defined in Example_Flash2803x_CsmKeys.asm
//
//    EALLOW;
//    CsmRegs.KEY0 = PRG_key0;
//    CsmRegs.KEY1 = PRG_key1;
//    CsmRegs.KEY2 = PRG_key2;
//    CsmRegs.KEY3 = PRG_key3;
//    CsmRegs.KEY4 = PRG_key4;
//    CsmRegs.KEY5 = PRG_key5;
//    CsmRegs.KEY6 = PRG_key6;
//    CsmRegs.KEY7 = PRG_key7;
//    EDIS;
//
//    // Perform a dummy read of the password locations
//    // if they match the key values, the CSM will unlock
//
//    temp = CsmPwl.PSWD0;
//    temp = CsmPwl.PSWD1;
//    temp = CsmPwl.PSWD2;
//    temp = CsmPwl.PSWD3;
//    temp = CsmPwl.PSWD4;
//    temp = CsmPwl.PSWD5;
//    temp = CsmPwl.PSWD6;
//    temp = CsmPwl.PSWD7;
//
//    // If the CSM unlocked, return succes, otherwise return
//    // failure.
//    if ( (CsmRegs.CSMSCR.all & 0x0001) == 0) return STATUS_SUCCESS;
//    else return STATUS_FAIL_CSM_LOCKED;
//
//}



/*=============================================================================*
 *Function name: void vVDCInputJudgeWarn()
 *Description :  Input Voltage DC or AC judge
 *input:        @ _u16DC_LN_CNT,@_u16DC_NL_CNT etc
 *global vars:   void
 *output:        g_u16MdlStatusExtend.bit.DCINPUTSTATUS
 *CALLED BY:     void vWarnCtrl(void)
 *modify data:   20131209
 *============================================================================*/
static	void vVDCInputJudgeWarn(void)
{

    //濡傛�?3涓伐棰戝懆鏈燂�?60MS鍐呰�?��ョ數鍘�?缁堝皬浜�30Vacrms锛岃涓轰笉鏄洿娴佽緭鍏�
    if(IsrVars._u16InputLowVolCNT >= TIME60MS_ISRTIMER)
    {
        IsrVars._u16InputLowVolCNT = TIME60MS_ISRTIMER;
        IsrVars._u16DC_LN_CNT = 0;
        IsrVars._u16DC_NL_CNT = 0;
    }

    if(IsrVars._u16DC_LN_CNT >= TIME150MS_ISRTIMER)
    {
        g_u16MdlStatusExtend.bit.DCINPUTSTATUS = 1;
        IsrVars._u16DC_LN_CNT = TIME150MS_ISRTIMER;
    }

    else if(IsrVars._u16DC_NL_CNT >= TIME150MS_ISRTIMER)
    {
        g_u16MdlStatusExtend.bit.DCINPUTSTATUS = 1;
        IsrVars._u16DC_NL_CNT = TIME150MS_ISRTIMER;
    }

    else
    {
        g_u16MdlStatusExtend.bit.DCINPUTSTATUS = 0;
    }

    //濡傛灉鍒ゆ柇鏄洿娴佽緭鍏ヤ笖鐩戞帶涓�?彂鐨�?��鐩存祦鍛婅鍏虫�? 鍒欏叧鎺夋ā鍧�?
    if( (g_u16MdlStatusExtend.bit.DCINPUTSTATUS == 1) && (g_u16MdlCtrlExtend.bit.DCINPUTALARM == 0 )  )
    {
        g_u16MdlStatusExtend.bit.DcInputAlarmStatus = 1;
    }
    else
    {
        g_u16MdlStatusExtend.bit.DcInputAlarmStatus = 0;
    }

}

/*=============================================================================*
 *Function name: void vHVSDReset()
 *Description :  HVSD RESET
 *input:
 *global vars:   void
 *output:        void
 *CALLED BY:     void vWarnCtrl(void)
 *modify data:   20131209
 *============================================================================*/
static void vHVSDReset(void)
{

    if(g_u16MdlCtrl.bit.HVSDRESET == 1)
    {
        g_u16CloseTime = 0; 	//by ls 20111130
        g_u16CloseTimeHW=0;  	//by ls 20111130

        g_u16OverVoltStatus = DC_OV_STATE_NORMAL;
        g_u16HWOverVoltStatus = DC_OV_STATE_NORMAL;

        g_u16MdlStatus.bit.HVSDLOCK = 0;
        g_u16MdlStatus.bit.DCOV = 0;
        g_u16MdlStatusExtend.bit.HWHVSD = 0;

        g_u16HVSDFistFlag = 0;			//by ls 20111130

        g_u16HVSDDebug = 0;			//by ls 20111130

        g_u16MdlCtrl.bit.HVSDRESET = 0;
        g_u16SafetyShortStatus = DC_SAFE_SHORT_STATUS_NORMAL;
        g_u16DcRestartFailTimes = 0;

        g_u16OverVoltCounter = 0;
    }
}

/*=============================================================================*
 *Function name: void vPFCOcpCheck()
 *Description :  vPFC OCP Check
 *input:
 *global vars:   void
 *output:        void
 *CALLED BY:     void vWarnCtrl(void)
 *modify data:
 *============================================================================*/
static void vPFCOcpCheck(void)
{
   UINT16 uiTmp;

    //PFC OCP
    uiTmp = EPwm1Regs.TZFLG.bit.CBC;
    if(uiTmp == 1)
    {
        EALLOW;
        EPwm1Regs.TZCLR.bit.CBC = 1;            //clear CBC flag
        EDIS;

        IsrVars._u16PFCOCP ++ ;

        if(IsrVars._u16PFCOCP > 30000)
        {
            IsrVars._u16PFCOCP = 30000;
        }
    }

}

/******************************************************************************
 *Function name: EnableInterrupts()
 *Description :  enables the  CPU interrupts
 *input:         void
 *global vars:   void
 *output:        void
 *CALLED BY:     main()
 ******************************************************************************/

//void EnableInterrupts(void)
//{
//
//	EPwm5Regs.ETSEL.bit.INTEN = 1;	// Enable ePWM6 INT
//   // EPwm3Regs.ETSEL.bit.INTEN = 1;	// Enable ePWM3 INT
//    EPwm7Regs.ETSEL.bit.INTEN = 1;	// Enable ePWM4 INT 璁＄畻涓柇
//
//	IER |= M_INT2;   //鍖呮�?��凾Z涓�?
//	IER |= M_INT3;   //Level INT3 is enabled 鍖呮�?��凟PWM涓�?
//
//	// Enable global Interrupts and higher priority real-time debug events:
//	EINT;   // Enable Global interrupt INTM
//	ERTM;   // Enable Global realtime interrupt DBGM
//
//}




/*******************************************************************************
 *Function name: vSafetyShortRestartHandle()
 *Description :  Handle safety short restart
 *input:         void
 *global vars:
 *
 *author: LiangLiang
 *output:        void
 *CALLED BY:     ()
 ******************************************************************************/
static	void  vSafetyShortRestartHandle(void)
{

	static UINT16	s_u16SafetyShortTimer;	//rectifier dcov status
	if(g_u16MdlStatusExtend.bit.SSHORTSD == 1)		//瀹�?鐭矾鏍囧織浣嶇疆1
	{
		if(g_u16SafetyShortStatus == DC_SAFE_SHORT_STATUS_DELY5MIN )			//鍋囩及浠�?��鍒侯儚蟿璇�?皩�?涘墤顑�?ˉ濞戞惇閰氱洿娣�??
		{
			g_u16DcRestartFailTimes++;
			if(g_u16DcRestartFailTimes >= 5)
			{
				g_u16DcRestartFailTimes = 5;
			}
		}

         g_u16SafetyShortStatus = DC_SAFE_SHORT_STATUS_DCOV;
	}
    //绗竴�?�?��鍏矰C,濡傛灉鍦≒FC鍚姩杩囩▼涓氨鏆傛�?涓嶅�?CDC锛岀瓑鍚姩鍒癉C姝ラ锛孭FC宸茬粡鎵撳紑浜嗗�?鍏矰CDC锛岀劧鍚庝粠DCDCINIT閲嶅�?
	if( (g_u16SafetyShortStatus == DC_SAFE_SHORT_STATUS_DCOV)
      &&( g_u16ActionReady >= DC_SOFT_START_INIT)
      &&(g_u16ActionReady <= DCOFF_SOFT_STATE) )

	{
		//set DCDC off control flag
		s_u16MdlOpenBak |=  DCDC_OFF;
		//close dcdc pwm
		mOffDcdcPwm();
		g_u16ActionReady = DCOFF_SOFT_STATE;
		//set dcdc off flag
		g_u16MdlStatus.bit.OFFSTAT = 1;
		// clear safety short shunt down flag
		g_u16MdlStatusExtend.bit.SSHORTSD = 0;			//娓呭畨瑙�?��璺洓鐤氾拷
		//clear timer
		s_u16SafetyShortTimer = 0;					//娓呰鏃跺櫒
		//next step
		g_u16SafetyShortStatus = DC_SAFE_SHORT_STATUS_DELY5S;

	}
    //绗簩�?�?��寤惰�?5S鎴�5鍒嗛挓�?�鏈�?,濡傛灉鍦≒FC鍚姩杩囩▼涓氨鏆傛�?涓嶅�?CDC锛岀瓑鍚姩鍒癉C姝ラ锛孭FC宸茬粡鎵撳紑浜嗗�?鍏矰CDC锛岀劧鍚庝粠DCDCINIT閲嶅�?
	else if(  (g_u16SafetyShortStatus == DC_SAFE_SHORT_STATUS_DELY5S)
	        &&( g_u16ActionReady >= DC_SOFT_START_INIT)
            &&(g_u16ActionReady <= DCOFF_SOFT_STATE) )
	{
		s_u16SafetyShortTimer++;
		if(g_u16DcRestartFailTimes >= 5)
		{
			if(s_u16SafetyShortTimer > TIME300S)	//5min
			{
				//DCDC restart
				g_u16ActionReady = DC_SOFT_START_INIT;

				//clr dcdc off control flag
				s_u16MdlOpenBak &=~ DCDC_OFF;
				s_u16SafetyShortTimer = 0;
				g_u16SafetyShortStatus = DC_SAFE_SHORT_STATUS_DELY5MIN;
			}
		}
		else
		{
			if(s_u16SafetyShortTimer > TIME4S8)		//5s restart
			{
				//DCDC restart
				g_u16ActionReady = DC_SOFT_START_INIT;

				//clr dcdc off control flag
				s_u16MdlOpenBak &=~ DCDC_OFF;
				s_u16SafetyShortTimer = 0;
				g_u16SafetyShortStatus = DC_SAFE_SHORT_STATUS_DELY5MIN;
			}
		}
	}
	else if(g_u16SafetyShortStatus == DC_SAFE_SHORT_STATUS_DELY5MIN)
	{
		s_u16SafetyShortTimer++;
		if(g_u16MdlCtrl.bit.WALKIN)
		{
			//瀹為�?��堕棿 = 1.125*WALKINTIME
			if(s_u16SafetyShortTimer > ((UINT16)((g_lq10WalkInTime.lData>>10) + (g_lq10WalkInTime.lData>>13))))
			{
				s_u16SafetyShortTimer = 0;
				g_u16DcRestartFailTimes = 0;
				g_u16SafetyShortStatus = DC_SAFE_SHORT_STATUS_NORMAL;
			}
		}
		else if(g_lq10TempAmbi.lData <= N_TEMP_27C)
		{
			//瀹為�?��堕棿 = 1.125*WALKINTIME
			if(s_u16SafetyShortTimer > ((UINT16)((g_lq10LowTempWalkInTime.lData>>10) + (g_lq10LowTempWalkInTime.lData>>13))))
			{
				s_u16SafetyShortTimer = 0;
				g_u16DcRestartFailTimes = 0;
				g_u16SafetyShortStatus = DC_SAFE_SHORT_STATUS_NORMAL;
			}
		}
		else
		{
			if(s_u16SafetyShortTimer > TIME1S)
			{
				s_u16SafetyShortTimer = 0;
				g_u16DcRestartFailTimes = 0;
				g_u16SafetyShortStatus = DC_SAFE_SHORT_STATUS_NORMAL;
			}
		}
	}

}

/******************************************************************************
 *Function name: vSystemFanCtrl()
 *Description :  //棰濆畾�?ヤ綔鏉�?��?涓�?�?
              15A浠ヤ笅缁存寔32%
              15A-35A绾挎�ф彁�?���?38%
              35-45A椋庢墖鍗犵┖姣斾�?38%绾挎�ф彁鍗囦�?46%
              45A-52A缁存寔涓�?46%
              52A-56A鍙婁互涓婇鎵囧崰绌烘瘮浠�46%鎻愬崌涓�?62%
              56A浠ヤ笂缁存寔62%

 *input:         g_lq10MdlCurrDisp,

 *global vars:   void
 *output:        i32FanNormalSpeedTemp
 *
 *CALLED BY:     vFanCtrl()
*******************************************************************************/
static  INT32  vSystemFanCtrl(void)
{
    INT32  i32FanNormalSpeedTemp;

    //20A浠ヤ笅缁存寔32%
    if(g_lq10MdlCurrDisp.lData <= IDC_20A)
    {
        i32FanNormalSpeedTemp = (INT32)FAN_EXTRA_LOW;
    }
    //20A~35椋庢墖鍗犵┖姣斾�?32%鎻愬崌涓�?38%,y = -16x + 3004
    else if(g_lq10MdlCurrDisp.lData <= IDC_35A)
    {
        i32FanNormalSpeedTemp = _IQ10mpy(g_lq10MdlCurrDisp.lData , ((INT32)16)<<10 );
        i32FanNormalSpeedTemp = ((((INT32)3004)<<10) - i32FanNormalSpeedTemp)>>10;
    }
    //35-45A椋庢墖鍗犵┖姣斾�?38%鎻愬崌涓�?46% .   y = -28x + 3460
    else if(g_lq10MdlCurrDisp.lData <= IDC_55A)
    {
            i32FanNormalSpeedTemp = _IQ10mpy(g_lq10MdlCurrDisp.lData , ((INT32)32)<<10 );
            i32FanNormalSpeedTemp = ((((INT32)3600)<<10) - i32FanNormalSpeedTemp)>>10;
     }
        //55A-79A缁存寔涓�?54%
    else if(g_lq10MdlCurrDisp.lData <= IDC_79A)
    {
       i32FanNormalSpeedTemp = FAN_54_PERCENT;
    }

        //79A-82A鍙婁互涓婇鎵囧崰绌烘瘮浠�54%鎻愬崌涓�?70% . y = -213x + 18693
    else if(g_lq10MdlCurrDisp.lData <= IDC_82A)
     {
         i32FanNormalSpeedTemp = _IQ10mpy(g_lq10MdlCurrDisp.lData , 218453);
         i32FanNormalSpeedTemp = (19141973- i32FanNormalSpeedTemp)>>10;
     }
    //82A浠ヤ笂涓�?70%
    else
    {
        i32FanNormalSpeedTemp = FAN_70_PERCENT;
    }

    return(i32FanNormalSpeedTemp);

}
/******************************************************************************
 *Function name: vSystemFanCtrl20()
 *Description :  //棰濆畾�?ヤ綔鏉�?��?涓�?�?
              15A浠ヤ笅缁存寔32%
              15A-35A绾挎�ф彁�?���?38%
              35-45A椋庢墖鍗犵┖姣斾�?38%绾挎�ф彁鍗囦�?44%
              45A-52A缁存寔涓�?44%
              52A-56A鍙婁互涓婇鎵囧崰绌烘瘮浠�44%鎻愬崌涓�?62%
              56A浠ヤ笂缁存寔62%

 *input:         g_lq10MdlCurrDisp,

 *global vars:   void
 *output:        i32FanNormalSpeedTemp
 *
 *CALLED BY:     vFanCtrl()
*******************************************************************************/
static  INT32  vSystemFanCtrl20(void)
{
    INT32  i32FanNormalSpeedTemp;

    //15A浠ヤ笅缁存寔32% 2720
    if(g_lq10MdlCurrDisp.lData <= IDC_15A)
    {
        i32FanNormalSpeedTemp = (INT32)FAN_EXTRA_LOW;
    }
    //15A~35椋庢墖鍗犵┖姣斾�?32%鎻愬崌涓�?38%,y = -12x + 2900
    else if(g_lq10MdlCurrDisp.lData <= IDC_35A)
    {
        i32FanNormalSpeedTemp = _IQ10mpy(g_lq10MdlCurrDisp.lData , ((INT32)12)<<10 );
        i32FanNormalSpeedTemp = ((((INT32)2900)<<10) - i32FanNormalSpeedTemp)>>10;
    }
    //35-45A椋庢墖鍗犵┖姣斾�?38%鎻愬崌涓�?44% .
    //y = -24x + 3320
    else if(g_lq10MdlCurrDisp.lData <= IDC_45A)
    {
        i32FanNormalSpeedTemp = _IQ10mpy(g_lq10MdlCurrDisp.lData , ((INT32)24)<<10 );
        i32FanNormalSpeedTemp = ((((INT32)3320)<<10) - i32FanNormalSpeedTemp)>>10;
    }
    //45A-52A缁存寔涓�?46%
    else if(g_lq10MdlCurrDisp.lData <= IDC_52A)
    {
        i32FanNormalSpeedTemp = FAN_44_PERCENT;
    }
    //52A-56A鍙婁互涓婇鎵囧崰绌烘瘮浠�44%鎻愬崌涓�?62% .
    //y = -180x + 11600
    else if(g_lq10MdlCurrDisp.lData <= IDC_56A)
    {
        i32FanNormalSpeedTemp = _IQ10mpy(g_lq10MdlCurrDisp.lData , ((INT32)180)<<10 );
        i32FanNormalSpeedTemp = ((((INT32)11600)<<10) - i32FanNormalSpeedTemp)>>10;
    }
    //56A浠ヤ笂涓�?62%
    else
    {
        i32FanNormalSpeedTemp = FAN_62_PERCENT;
    }

    return(i32FanNormalSpeedTemp);

}
/******************************************************************************
 *Function name: vSingleModelFanCtrl()
 *Description :  //棰濆畾�?ヤ綔鏉�?��?涓�?紝鍗曟ā鍧楋紝椋庢墖鎺у埗绛�?��
 *              15A浠ヤ笅缁存寔32%锛�
 *              15A-35A绾挎彁閫熷埌38%锛�
 *              35-45A椋庢墖鍗犵┖姣斾�?38%绾挎�ф彁鍗囦�?52%锛�
 *              45A-52A缁存寔涓�?52%
 *              52A-56A鍙婁互涓婇鎵囧崰绌烘瘮浠�52%鎻愬崌涓�?62%
 *              56A浠ヤ笂缁存寔62%
 *input:         g_lq10MdlCurrDisp,
 *global vars:   void
 *output:        i32FanNormalSpeedTemp
 *
 *CALLED BY:     vFanCtrl()
*******************************************************************************/
static  INT32  vSingleModelFanCtrl(void)
{
    INT32  i32FanNormalSpeedTemp;

    /*if((g_lq10TempAmbiCtrl.lData < TEMP_30C)
    &&(g_lq10AcVolt.lData>VAC_210V)
    &&(g_lq10AcVolt.lData<VAC_230V)
    &&(g_lq10MdlVolt.lData > VDC_53V )
    &&(g_lq10MdlVolt.lData < VDC_54V ))
    {
       if((g_lq10MdlCurrDisp.lData > IDC_23A)
        &&(g_lq10MdlCurrDisp.lData < IDC_27A))
       {
           //i32FanNormalSpeedTemp = FAN_30_PERCENT;
           g_u16FanSpeedFlag=1;
       }
       else if((g_lq10MdlCurrDisp.lData < IDC_21A)
       ||(g_lq10MdlCurrDisp.lData> IDC_29A))
       {
           g_u16FanSpeedFlag=0;

       }
    }
    else
    {
        g_u16FanSpeedFlag=0;

    }

    if(g_u16FanSpeedFlag==1)
    {
        i32FanNormalSpeedTemp = FAN_30_PERCENT;

    }
    else
    {*/
        //56A浠ヤ笂缁存寔62%
           //temppowerlimt=  temp*K/Q+B
           //鍏朵腑K鍊间负浜嗘彁楂�?绠楃簿搴︼紝鍦ㄩ厤缃椂鍊�?��,鎵�浠ュ�?��＄畻TMEP*K鍚庡張闄や互浜哘
           if (g_lq10MdlCurrDisp.lData > IDC_82A)
           {
               i32FanNormalSpeedTemp = FAN_70_PERCENT;
           }
           //79A-82A椋庢墖鍗犵┖姣斾�?58%鎻愬崌涓�?70% .   y = -160x + 14320
           else if (g_lq10MdlCurrDisp.lData > IDC_79A)
           {
               i32FanNormalSpeedTemp = _IQ10mpy(g_lq10MdlCurrDisp.lData , ((INT32)160)<<10 );
               i32FanNormalSpeedTemp = ((((INT32)14320)<<10) - i32FanNormalSpeedTemp)>>10;

           }
           //55A-79A缁存寔涓�?58%
           else if (g_lq10MdlCurrDisp.lData > IDC_55A)
           {
               i32FanNormalSpeedTemp = FAN_58_PERCENT;
           }
           //35-45A椋庢墖鍗犵┖姣斾�?38%鎻愬崌涓�?52% .   y = -56x + 4440 //y = -40x + 3888
           else if (g_lq10MdlCurrDisp.lData > IDC_35A)
           {
                i32FanNormalSpeedTemp = _IQ10mpy(g_lq10MdlCurrDisp.lData , ((INT32)40)<<10 );
                i32FanNormalSpeedTemp = ((((INT32)3888)<<10) - i32FanNormalSpeedTemp)>>10;
           }
           //15A~35椋庢墖鍗犵┖姣斾�?32%鎻愬崌涓�?38%.    y = -16x + 3004
           else if (g_lq10MdlCurrDisp.lData > IDC_20A)
           {
                i32FanNormalSpeedTemp = _IQ10mpy(g_lq10MdlCurrDisp.lData , ((INT32)16)<<10 );
                i32FanNormalSpeedTemp = ((((INT32)3004)<<10) - i32FanNormalSpeedTemp)>>10;
           }
           //<15A椋庢墖杞�熷浐瀹氫�?32%
           else
           {
               i32FanNormalSpeedTemp = (INT32)FAN_EXTRA_LOW;
           }

    //}
    return(i32FanNormalSpeedTemp);

}
/******************************************************************************
 *Function name: vModelTurnOff_PfcDc(void)
 *Description :  TurnOffPfcDc
 *input:         void

 *global vars:   void
 *output:        void
 *            	 void
 *CALLED BY:     vOnOffCtrl(),vPFCSoftStartFail,vSstartCtrl and so on.
*******************************************************************************/
//�??�潡鍏砅FC,DC鐨勫姩浣溿��?
void vModelTurnOff_PfcDc(void)
{
 	s_u16MdlOpenBak |= PFC_OFF;

    //close dcdc PWM
    mOffDcdcPwm();

    //close pfc pwm
    mOffPfcPwm();

    //main relay open(default open) & pfc off
    mMainRelayOpen();

    //fanfail, OTP don't close fan
    //only ACOVP(save power), ACUVP, PFCUV(for 3.3v DSP power), SHORTRELAY close fan.
    if( mMdlOff_PFCDC_FanOff() )
    {
        g_u16FanEnable = FAN_RUN_DISABLE;
        DELAY_US(TIME10MS);
    }
    else
    {
        g_u16FanEnable = FAN_RUN_ENABLE;
    }

    if(g_u16ActionReady <= NORMAL_RUN_STATE)
    {
        g_u16ActionReady = PFCOFF_SOFT_STATE;
    }

    g_u16MdlStatus.bit.OFFSTAT = 1;
    IsrVars._u16PwmEnableFlag = 0;
    IsrVars._u16PfcQuickPFCUVPFault = 0;
    IsrVars._u16PfcQuickPFCOVPFault=0;
    IsrVars._u16OverAcVolt = 0;
    IsrVars._u16PfcOpened = 0;

}
/******************************************************************************
 *Function name:  vModelInputDyn(void)
 *Description :     TurnOffPfcDc
 *input:                void
 *global vars:       void
 *output:              void
 *CALLED BY:        void vWarnCtrl(void)     .
*******************************************************************************/
//�??�潡鍏砅FC,DC鐨勫姩浣溿��?
void vModelInputDyn(void)
{
    ubitfloat   lCal_sqare_Temp,lCal_Qtest_Temp;
    ubitfloat   lVacChangeVal1, lVacChangeVal2;

    //-----------------------Vac杈撳叆璺冲彉鍒ゆ�?------------------------------
    g_ai32AcMaxPointVal[2] = g_ai32AcMaxPointVal[1];
    g_ai32AcMaxPointVal[1] = g_ai32AcMaxPointVal[0];
    g_ai32AcMaxPointVal[0] = (INT32)IsrVars._u16AdAcMaxPowLim;//娓呴浂鍦ㄥ悗杈�
    //璧峰悕PowLim锛屽拰闄�??鐜囧凡缁忔瘝绾胯皟鍘�?��閽╋紝�?�為�?��ㄤ簬娴�?�?��ㄦ��?
    lVacChangeVal2.lData = g_ai32AcMaxPointVal[2] - g_ai32AcMaxPointVal[0];
    lVacChangeVal1.lData = g_ai32AcMaxPointVal[1] - g_ai32AcMaxPointVal[0];
    //
    if(( lVacChangeVal1.lData >= VAC_AD_PEAK_50V )
       &&( lVacChangeVal2.lData >= VAC_AD_PEAK_50V ) )
    {
        IsrVars._i16AcDropFlag = 500;//500*10ms=5s涓�鏃﹁繘鍏rop�??�紡,鑷冲�?5s鎵嶅彲閫�鍑�?
      //  s_lVacPeakBeforeDrop.lData = g_ai32AcMaxPointVal[2];
        s_lVacPeakAfterDrop.lData = g_ai32AcMaxPointVal[0];
    }
    else if((lVacChangeVal1.lData <= NEG_VAC_PEAK_50V)
            &&(lVacChangeVal2.lData <= NEG_VAC_PEAK_50V))
    {
        IsrVars._i16AcUpjumpFlag = 500;
        s_lVacPeakBeforeJump.lData = g_ai32AcMaxPointVal[2];
     //   s_lVacPeakAfterJump.lData = g_ai32AcMaxPointVal[0];

    }
    else
    {
        IsrVars._i16AcDropFlag -- ;
        IsrVars._i16AcUpjumpFlag --;//涓�鏍�?殑锛岃繘鍒癑ump�??�紡涔熸�?5s�?��鍑�
    }

    if(IsrVars._i16AcDropFlag <= 1 )
    {
        IsrVars._i16AcDropFlag = 1;
    }
    if(IsrVars._i16AcUpjumpFlag <= 1 )
    {
        IsrVars._i16AcUpjumpFlag = 1;
    }
    //-----------------------------------------------------------------------------
    //鍗曟Vac宄板�兼牎鍑�?(浣跨�?ac 1Khz婊ゆ�?),涓嶄�?30鐐瑰钩鍧囨�?绠�?湁鏁堝�硷紝鐀���?��ㄦ�佹�?1/ACrms2
    //娉ㄦ剰锛欼srVars._u16AdAcMaxPowLim鏄痗nt锛屼笉鐢ㄨ繘琛孉D�?��暟璁＄畻
    //-----------------------------------------------------------------------------
    if(IsrVars._i16AcDropFlag > 1)
       {
            lCal_sqare_Temp.lData= s_lVacPeakAfterDrop.lData;
       }
       else if(IsrVars._i16AcUpjumpFlag > 1)
       {
           lCal_sqare_Temp.lData=s_lVacPeakBeforeJump.lData;
       }
    //lCal_sqare_Temp.lData= (INT32)IsrVars._u16AdAcMaxPowLim;
    lCal_sqare_Temp.lData = _IQ10div(lCal_sqare_Temp.lData<<10, IQ10_DEF_VACFACTOR );
    //宄板�兼牎鍑�?
    lCal_sqare_Temp.lData = _IQ10mpy(lCal_sqare_Temp.lData, g_lq10AcVrmsSampSysa.lData)
                              + g_lq10AcVrmsSampSysb.lData ;
    //濡傛灉杈撳叆鏄氦娴佺數,鐀��嘲鍊奸櫎1.414鍙姌绠楁湁鏁堝��;鑻ヨ�?��ユ槸鐩存祦鐢�?,宄板��=鏈�?晥鍊�?
    if(g_u16MdlStatusExtend.bit.DCINPUTSTATUS == 0)
    {
        lCal_sqare_Temp.lData = _IQ10div(lCal_sqare_Temp.lData,SQRT2);      //1.414
    }
    g_lq10ACPeak_LimPow.lData = lCal_sqare_Temp.lData+VAC_2V;//宄板�兼姌绠楁垚鐨勬湁鏁堝�硷紝鐀��潵鍋氶�?鍔熺巼銆�?

    //g_lq10AcPeakSingle.lData = lCal_sqare_Temp.lData;//G4娑�?強浣嗗弻鐩�?垏鎹�?��杩欓噷鍏堜笉�?��

    IsrVars._u16AdAcMaxPowLim = 0;  //姣忎釜璁＄畻鍛ㄦ�?20ms鎶婁腑鏂噷鐨勫嘲鍊兼�?涓�娆�0锛�
    //-----------------------------------------------------------------------------
    //浣跨�?C宄板�间綔涓鸿烦鍙樻椂鐨�?���?��檺鍔熺巼鍙婃瘝绾跨數鍘�?殑璁惧畾浣跨敤閲�?
    //-----------------------------------------------------------------------------
    if(IsrVars._i16AcDropFlag > 1)//姣嶇嚎璋冨帇瑕佹寜鐓ц烦鍙樼殑楂樺帇娈靛幓璋冿紝闄ら潪�?��鍑鸿烦鍙�?5s銆�
    {
        /*lCal_Temp.lData = _IQ10div(s_lVacPeakBeforeDrop.lData<<10, IQ10_DEF_VACFACTOR );

        g_lq10ACPeak_CalVpfc.lData = _IQ10mpy(lCal_Temp.lData, g_lq10AcVrmsSampSysa.lData)
                                            + g_lq10AcVrmsSampSysb.lData ;*/

        g_lq10ACPeak_CalVpfc.lData =VPFC_435V; // 鏆傛椂鏇存敼锛宑sm0402

    }
    else if(IsrVars._i16AcUpjumpFlag > 1)
    {
        /*lCal_Temp.lData = _IQ10div(s_lVacPeakAfterJump.lData<<10, IQ10_DEF_VACFACTOR );
        g_lq10ACPeak_CalVpfc.lData = _IQ10mpy(lCal_Temp.lData, g_lq10AcVrmsSampSysa.lData)
                                            + g_lq10AcVrmsSampSysb.lData ;*/
        g_lq10ACPeak_CalVpfc.lData=VPFC_435V;  //鏆傛椂鏇存敼锛宑sm0402
    }
    else//閰嶅悎鍓嶈竟鐨勬洿鏀癸紝鍏ㄦ寜鐓ф渶灏忓�肩畻
    {
        g_lq10ACPeak_CalVpfc.lData = g_lq10ACPeak_protect.lData;//璋冨帇鐨�?��鏁�?笉鍙�?绠楋紝杩樻槸鍦ㄥ師浣嶇�?
    }   //g_lq10ACPeak_CalVpfc浣滀负鏈�缁堣皟鍘�?殑渚濇嵁銆�

    //-----------------------------------------------------------------------------
    //闄愬姛鐜囩殑渚濇嵁锛屾柊澧炰竴涓揩�?��檺鍔熺巼锛屽氨鍦ㄨ繖閲岃绠�?
    //浣跨�?C宄板�兼姌绠楃殑鏈�?晥鍊间綔涓鸿烦鍙樻�?鐨勫揩閫熼�?鍔熺巼鍙婃瘝绾跨數鍘�?��璁惧畾浣跨敤閲�
    //-----------------------------------------------------------------------------
    //Dynamic Input
    if((IsrVars._i16AcDropFlag <= 1) && (IsrVars._i16AcUpjumpFlag <= 1))
     //鏂板涓�涓�?鍔熺巼锛岃鎸�?収璺冲彉鐨勪綆鍘�?��闄愬埗锛岄櫎闈為��鍑鸿烦鍙�?5s銆�
    {//涓嶅�?��ㄦ�佺洿鎺ュ彇鏈�澶э紝浠跨湡鍚庤竟鏈夊彇灏忔搷浣滐紝涓嶇敤鎷�?��杩囧姛鐜�?
              g_lq10ACDynLimitPower.lData = IQ10_AC_LIM_POW_MAX;//1024,100%
              IsrVars._i16VacDynFlag=0;
    }
    else
    {   //鍙�?��ㄦ�佽捣浣滅�?
        //90~174Vac:512(50%)->1024(100%); //85~90Vac:409.6(40%)->512(50%)
        //85~90Vac:0.4->0.5 y = 0.02x - 1.3
        //85~90:0.35~0.4    y = 0.01x - 0.5
        if ( g_lq10ACPeak_LimPow.lData <= VAC_85V)
        {
            g_lq10ACDynLimitPower.lData = IQ10_AC_LIM_POW_MIN;//35%  //40%
        }
        else if(g_lq10ACPeak_LimPow.lData <= VAC_90V)
        //�?��172V鍋氳竟鐣岋紝鐣欑偣浣欓噺锛�172V璺冲彉涓�?��涓弧杞藉簲璇ュ媺�?哄彲浠ャ��
        {
             g_lq10ACDynLimitPower.lData = _IQ10mpy(g_lq10ACPeak_LimPow.lData, (((INT32)1)<<10))
                            - (((INT32)50)<<10);
             g_lq10ACDynLimitPower.lData = _IQ10div(g_lq10ACDynLimitPower.lData, (((INT32)100)<<10));
         }
        //90~174Vac:512(50%)->1024(100%) ;//y(鏈畾鏍�?)= 5953*x/1000 -24
        //90~174:40~100   y= 6.549*x -129
        //y=7.14*x -233
        else if (g_lq10ACPeak_LimPow.lData <= VAC_176V)
        {
            g_lq10ACDynLimitPower.lData = _IQ10mpy(g_lq10ACPeak_LimPow.lData, ((INT32)7140));
            g_lq10ACDynLimitPower.lData =
                      _IQ10div(g_lq10ACDynLimitPower.lData, (((INT32)1000)<<10))+ ((INT32)-233);
        }
        else
       {
            g_lq10ACDynLimitPower.lData = IQ10_AC_LIM_POW_MAX;//1024,100%
        }
        IsrVars._i16VacDynFlag=1;

    }

    lCal_Qtest_Temp.lData = _IQ10mpy(g_i32PowerRate, g_lq10ACDynLimitPower.lData)
                    + (((INT32)g_u16PermitAdjust * g_i32PowerRate * (INT32)40)>>10);//IQ10鐨勫姛鐜�?


    lCal_Qtest_Temp.lData = _IQ10mpy(lCal_Qtest_Temp.lData, g_lq10PowerConSysa.lData)
                              + g_lq10PowerConSysb.lData;//鏍�?�噯鍚嶪Q10鐨勫姛鐜�?
    lCal_Qtest_Temp.lData = _IQ10mpy(lCal_Qtest_Temp.lData, IQ10_DEF_POWERFACTOR);//杩欓噷杩樻槸IQ10
    //((INT16)((((DEFAULT_POWER * IQ10_DEF_POWERFACTOR)>>10)*(INT32)1536)>>10))
    IsrVars._i16DCDCPowerSetDyn = (INT16)(lCal_Qtest_Temp.lData>>10);//鎵�浠ヨ繖閲屽彸绉绘槸娌℃湁闂鐨勩��?

    //IsrVars._i16DCDCPowerSetDyn =  POWER_LIM_UPPER;
 }


/******************************************************************************
 *Function name: uiOverLevelChk()
 *Description :  over level chk
 *input:         ulCompareData: Data to be compared
 * 	             ulHighLever: High limit level
 *               uiFiler: Number of times to be confirmed
 *  	         pCounter: pointer of Counter register
 *global vars:   void
 *output:        true:	1 (The data to be compared is over level)
 *            	 false:	0
 *CALLED BY:     vWarnCtrl()
*******************************************************************************/
UINT16	uiOverLevelChk(INT32 lCompareData, INT32 lHighLever, UINT16 u16Filter,
					   UINT16 *pCounter)
{
    if(lCompareData > lHighLever)
	{
		(*pCounter) ++;
	}
    else
	{
		(*pCounter) = 0;
	}

    if((*pCounter) >= u16Filter)
    {
		(*pCounter) = 0;
		return(true);
    }
   	else
	{
    	return(false);
	}
}

/******************************************************************************
 *Function name: uiUnderLevelChk()
 *Description :  under level chk
 *input:         ulCompareData: Data to be compared
 * 	             ulLowLever: Low limit level
 *               uiFiler: Number of times to be confirmed
 *  	         pCounter: pointer of Counter register
 *global vars:   void
 *output:        true:	1 (The data to be compared is under level)
 *            	 false:	0
 *CALLED BY:     vWarnCtrl()
*******************************************************************************/
UINT16	uiUnderLevelChk(INT32 lCompareData, INT32 lLowLever, UINT16 u16Filter,
						UINT16 *pCounter)
{
    if(lCompareData < lLowLever)
	{
		(*pCounter) ++;
	}

    else
	{
		(*pCounter) = 0;
	}

    if((*pCounter) >= u16Filter)
    {
		(*pCounter) = 0;
		return(true);
    }

    else
	{
    	return(false);
	}
}

/******************************************************************************
 *Function name: lUpdownlimit()
 *Description :  parameter up down limit
 *input:         lPareData: Data to up dowm limit
 * 	             lHighLever: up limit
 *               lLowLever: down limit
 *global vars:   void
 *output:        lPareData (The data have be up dowm limit value)
 *CALLED BY:
*******************************************************************************/
INT32 lUpdownlimit(INT32 lPareData , INT32 lHighLever, INT32 lLowLever)
{

  if( lPareData > lHighLever)
  {
      lPareData = lHighLever;
  }
  else if(lPareData < lLowLever)
  {
      lPareData = lLowLever;
  }
  return( lPareData );
}
/******************************************************************************
 *Function name: vPFCPhaseShielding()
 *Description :  pfc phase shielding control
 *input:         void
 *global vars:   void
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
static  void    vPFCPhaseShielding(void)
{

    if (IsrVars._i16PhaseShieldingTest==1)
    {
        IsrVars._u16PfcSinglePhaseMode = 1;//=0, Dual
    }
    else if (IsrVars._i16PhaseShieldingTest==2)
    {
        IsrVars._u16PfcSinglePhaseMode = 0;//=0, Dual
    }
    else
    {
       if(g_u16MdlStatusExtend.bit.PARAFLAG== 0)//PAPAFLAG=0,single
       {
           if( (g_u16ActionReady == NORMAL_RUN_STATE)&&(IsrVars._u16SoftStartEndTimer >= DCSTARTEND_5S)
           &&(IsrVars._i16CphaseOnCnt==3500) && (IsrVars._i16VacDynFlag==0 ) )
           {
               if ( (g_lq10MdlCurrDisp.lData < IDC_12A)&&(labs(g_lq10MdlVolt.lData - VDC_53V5) < VDC_0V5 )
               &&(labs(g_lq10AcVolt.lData- VAC_220V) < VAC_20V) && (g_lq10TempAmbiCtrl.lData > N_TEMP_5C)
               && (g_lq10TempAmbiCtrl.lData < TEMP_45C) )
               {
                   g_u16PhaseSheddingFlag ++;
                }
               if ( (g_lq10MdlCurrDisp.lData > IDC_14A) || (labs(g_lq10MdlVolt.lData - VDC_53V5) > VDC_0V7)
               ||(labs(g_lq10AcVolt.lData- VAC_220V) > VAC_30V) || (g_lq10TempAmbiCtrl.lData > TEMP_50C)
               || (g_lq10TempAmbiCtrl.lData < N_TEMP_10C) )
               {
                   g_u16PhaseSheddingFlag = 0;
                }
           }
           else
           {
               g_u16PhaseSheddingFlag = 0;//=0,Dual;;=1,Single
           }

           if(g_u16PhaseSheddingFlag >= 100)//100*10ms=1s
           {
               IsrVars._u16PfcSinglePhaseMode = 1;
               g_u16PhaseSheddingFlag = 100;
           }
           else
           {
               IsrVars._u16PfcSinglePhaseMode = 0;//=0, Dual
           }
       }
       else //PAPAFLAG=1,system
       {
           if((g_u16ActionReady == NORMAL_RUN_STATE)&&(IsrVars._u16SoftStartEndTimer >= DCSTARTEND_5S)
           &&(IsrVars._i16CphaseOnCnt==3500)&& (IsrVars._i16VacDynFlag==0 ) )
           {
               if ( (g_lq10MdlAvgCurr.lData < IDC_12A)&&(labs(g_lq10MdlVolt.lData - VDC_53V5) < VDC_0V5 )
               &&(labs(g_lq10AcVolt.lData- VAC_220V) < VAC_20V) && (g_lq10TempAmbiCtrl.lData > N_TEMP_5C)
               && (g_lq10TempAmbiCtrl.lData < TEMP_45C) )
               {
                   g_u16PhaseSheddingFlag ++;
               }
               if ( (g_lq10MdlAvgCurr.lData > IDC_14A)  || (labs(g_lq10MdlVolt.lData - VDC_53V5) > VDC_0V7)
               || (labs(g_lq10AcVolt.lData- VAC_220V) > VAC_30V) || (g_lq10TempAmbiCtrl.lData > TEMP_50C)
               || (g_lq10TempAmbiCtrl.lData < N_TEMP_10C) )
               {
                   g_u16PhaseSheddingFlag = 0;
               }
           }
           else
           {
               g_u16PhaseSheddingFlag = 0;//=0,Dual;;=1,Single
           }

           if(g_u16PhaseSheddingFlag >= 100)//100*10ms=1s
           {
               IsrVars._u16PfcSinglePhaseMode = 1;
               g_u16PhaseSheddingFlag = 100;
           }
           else
           {
               IsrVars._u16PfcSinglePhaseMode = 0;//=0, Dual
           }
     }
   }
}

//===========================================================================
// No more.
//===========================================================================
