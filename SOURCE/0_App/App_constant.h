/*=============================================================================*
 *         Copyright(c) 2007-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : GBB Rectifiers
 *
 *  FILENAME : GBB_constant.h
 *  PURPOSE  : Define the constant
 *				     
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *  2008-6-10         OOA            Agnes           Created.   Pre-research 
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *    
 *============================================================================*/

#ifndef App_constant_H
#define App_constant_H

/*============================================================================*
 * constant definition
 *============================================================================*/
//---------------define for pll
#define DCC_COUNTER0_WINDOW_APP 100 //must be same as DCC_COUNTER0_WINDOW in "sysctrl.c"
//
// Defines
//
#define   PLL_RETRIES              100
#define   PLL_LOCK_TIMEOUT        2000

#define CAN_MSG_ID_RX             0x06000003
#define CAN_MSG_ID_60             0x06000003
#define CAN_MSG_ID_70              0x07000003
#define VERTIV_CAN_PROTNO       0x1EF00003  //for 0x60/0x70协议
#define CAN_TX_MSG_OBJ_RST_70 6
#define CAN_TX_MSG_OBJ_NOM_60 5
#define CAN_RX_MSG_OBJ     1
#define CAN_TX_MSG_OBJ_1_70   6
#define CAN_TX_MSG_OBJ_2_60   5
#define CAN_MAX_BIT_DIVISOR     (13)   // The maximum CAN bit timing divisor
#define CAN_MIN_BIT_DIVISOR     (5)    // The minimum CAN bit timing divisor
#define CAN_MAX_PRE_DIVISOR     (1024) // The maximum CAN pre-divisor
#define CAN_MIN_PRE_DIVISOR     (1)    // The minimum CAN pre-divisor
#define CAN_MSG_ID_SHIFT        18U

#define CAN_TX_MSG_OBJ_RST_load  4
#define CAN_TX_MSG_OBJ_1_load   4
#define CAN_TX_MSG_OBJ_2_load   5
#define CAN_MSG_ID_load         0x06000003

// The key value for RAM initialization
#define CAN_RAM_INIT_KEY           (0xAU)

// RAM Initialization Register Mask
#define CAN_RAM_INIT_MASK          (0x003FU)

typedef enum
{
        //! Transmit message object.
        MSG_OBJ_TYPE_TRANSMIT,

        //! Receive message object.
        MSG_OBJ_TYPE_RECEIVE
}msgObjType;


//============================================================================//
//============================================================================//
//=================================常量定义===================================//
//============================================================================//
//============================================================================//
/*______________________________DCDC voltage__________________________________*/
#define VDC_0V2        	((INT32)204)				
#define VDC_0V5        	((INT32)512) 
#define VDC_0V05         ((INT32)51)
#define VDC_0V7        	((INT32)716) 
#define VDC_1V        	(((INT32)1) << 10)		
#define VDC_2V        	(((INT32)2) << 10)
#define VDC_10V  	    (((INT32)10) << 10)
#define VDC_20V  	    (((INT32)20) << 10)
#define VDC_22V         (((INT32)22) << 10)
#define VDC_23V         (((INT32)23) << 10)
#define VDC_30V    		(((INT32)30) << 10)
#define VDC_31V         (((INT32)31) << 10)
#define VDC_32V         (((INT32)32) << 10)
#define VDC_34V         (((INT32)34) << 10)
#define VDC_35V  	    (((INT32)35) << 10)
#define VDC_36V         (((INT32)36) << 10)
#define VDC_41V  	    (((INT32)41) << 10)
#define VDC_40V  	    (((INT32)40) << 10)
#define VDC_41V7  	    ((INT32)42701)  
#define VDC_42V  	    (((INT32)42) << 10)
#define VDC_43V         (((INT32)43) << 10)
#define VDC_44V         (((INT32)44) << 10)
#define VDC_45V 	    (((INT32)45) << 10)
#define VDC_46V         (((INT32)46) << 10)
#define VDC_47V  	    (((INT32)47) << 10)
#define VDC_47V5         ((INT32)48640)//VDC_47V5
#define VDC_47V7         ((INT32)48845) //47.7v
#define VDC_47V25         ((INT32)48384) //47.7v
#define VDC_48V5  	    ((INT32)49664)//VDC_48V5
#define VDC_48V         (((INT32)48) << 10)
#define VDC_50V         (((INT32)50) << 10)
#define VDC_51V  	    (((INT32)51) << 10)
#define VDC_51V5 	    (((INT32)103) << 9)
#define VDC_52V         (((INT32)52) << 10)
#define VDC_52V5         (((INT32)105) << 9)
#define VDC_53V         (((INT32)53) << 10)
#define VDC_53V5  	    (((INT32)107) << 9)
#define VDC_54V  	    (((INT32)54) << 10)
#define VDC_54V2        ((INT32)55501)
#define VDC_54V7        ((INT32)56013)
#define VDC_55V         (((INT32)55) << 10)
#define VDC_55V5        (((INT32)111) << 9)
#define VDC_56V        	(((INT32)56) << 10)
#define VDC_58V        	(((INT32)58) << 10)
#define VDC_58V5       	(((INT32)117) << 9)
#define VDC_59V         (((INT32)59) << 10)
#define VDC_59V7		((INT32)61133)  
#define VDC_60V         (((INT32)60) << 10)
#define VDC_49V         (((INT32)49) << 10)
/*__________AC and HVDC voltage____(INPUT VOLTAGE ) __________________________*/

#define VHVDC_415V      (((INT32)415) << 10)
#define VHVDC_400V      (((INT32)400) << 10)
#define VHVDC_374V      (((INT32)374) << 10)
#define VHVDC_250V      (((INT32)250) << 10)

#define VAC_460V        (((INT32)460) << 10)
#define VAC_438V        (((INT32)438) << 10)
#define VAC_318V        (((INT32)318) << 10)
#define VAC_310V        (((INT32)310) << 10)
#define VAC_307V5       (((INT32)615) << 9)
#define VAC_302V5      	(((INT32)605) << 9)
#define VAC_305V       	(((INT32)305) << 10)
#define VAC_300V        (((INT32)300) << 10)
#define VAC_295V5       (((INT32)591) << 9)
#define VAC_295V       	(((INT32)295) << 10)
#define VAC_290V       	(((INT32)290) << 10)
#define VAC_280V        (((INT32)280) << 10)
#define VAC_278V        (((INT32)278) << 10)
#define VAC_276V        (((INT32)276) << 10)
#define VAC_264V       	(((INT32)264) << 10)
#define VAC_255V       	(((INT32)255) << 10)
#define VAC_245V        (((INT32)245) << 10)
#define VAC_250V        (((INT32)250) << 10)
#define VAC_235V        (((INT32)235) << 10)
#define VAC_230V        (((INT32)230) << 10)
#define VAC_218V        (((INT32)218) << 10)
#define VAC_225V        (((INT32)225) << 10)
#define VAC_220V        (((INT32)220) << 10)
#define VAC_205V        (((INT32)205) << 10)
#define VAC_210V        (((INT32)210) << 10)
#define VAC_200V       	(((INT32)200) << 10)
#define VAC_195V        (((INT32)195) << 10)
#define VAC_190V       	(((INT32)190) << 10) 
#define VAC_176V       	(((INT32)176) << 10) 
#define VAC_154V       	(((INT32)154) << 10)
#define VAC_132V       	(((INT32)132) << 10)
#define VAC_120V       	(((INT32)120) << 10)
#define VAC_115V        (((INT32)115) << 10)
#define VAC_100V       	(((INT32)100) << 10)
#define VAC_95V         (((INT32)95) << 10)
#define VAC_93V         (((INT32)93) << 10)
#define VAC_90V         (((INT32)90) << 10)
#define VAC_85V         (((INT32)85) << 10)
#define VAC_80V         (((INT32)80) << 10)
#define VAC_72V         (((INT32)72) << 10)
#define VAC_55V         (((INT32)55) << 10)
#define VAC_50V         (((INT32)50) << 10)
#define VAC_25V         (((INT32)25) << 10)
#define VAC_15V         (((INT32)15) << 10)
#define VAC_10V         (((INT32)10) << 10)

#define VAC_20V         (((INT32)20) << 10)
#define VAC_30V         (((INT32)30) << 10)

#define VAC_5V          (((INT32)5) << 10)
#define VAC_3V5         (((INT32)7) << 9)  			
#define VAC_2V          (((INT32)2) << 10)
#define VAC_1V5         (((INT32)3) << 9)
#define VAC_0V          ((INT32)0)

#define    VAC_SquareRms_500V     (INT32)(14106785)
/*_______________________________PFC voltage__________________________________*/

#define VPFC_500V     	 (((INT32)500) << 10)
#define VPFC_490V        (((INT32)490) << 10)
#define VPFC_460V        (((INT32)460) << 10)
#define VPFC_450V        (((INT32)450) << 10)
#define VPFC_440V        (((INT32)440) << 10)
#define VPFC_435V        (((INT32)435) << 10)
#define VPFC_427V        (((INT32)427) << 10)
#define VPFC_420V        (((INT32)420) << 10)
#define VPFC_405V        (((INT32)405) << 10) 
#define VPFC_390V        (((INT32)390) << 10)
#define VPFC_385V        (((INT32)385) << 10)
#define VPFC_375V        (((INT32)375) << 10)
#define VPFC_345V        (((INT32)345) << 10)
#define VPFC_300V        (((INT32)300) << 10)
#define VPFC_170V        (((INT32)170) << 10)
#define VPFC_150V        (((INT32)150) << 10)
#define VPFC_80V         (((INT32)80) << 10)
#define VPFC_70V         (((INT32)70) << 10)
#define VPFC_60V         (((INT32)60) << 10)
#define VPFC_15V         (((INT32)15) << 10)
#define VPFC_10V          (((INT32)10) << 10)
#define VPFC_2V        	 (((INT32)2) << 10)
#define VPFC_0V5         (((INT32)1) << 9)				 
#define VPFC_0V3         ((INT32)307)

/*____________________________________________________________________________*/
//
/*_______________________________PFC current__________________________________*/
//
/*____________________________________________________________________________*/

#define IPFC_0A3         ((INT32)307)
#define IPFC_2A5         ((INT32)2560)
#define PFC_4A           ((INT32)4096)
#define PFC_8A           ((INT32)8192)
#define PFC_12A          ((INT32)12288)
#define IPFC_16A         (((INT32)16)<<10)

/*____________________________________________________________________________*/
//
/*_______________________________DC current___________________________________*/
//
/*____________________________________________________________________________*/

#define IDC_0A          ((INT32)0)    // 0<<10
#define IDC_0A1         ((INT32)102)  //0.1<<10
#define IDC_0A3         ((INT32)307)  //0.1<<10 
#define IDC_0A5         ((INT32)512)  //0.5<<10
#define IDC_1A2         ((INT32)1228)  //1.2<<10
#define IDC_0A9         ((INT32)922)  //0.9<<10
#define IDC_3A         ((INT32)3072)  //3<<10
#define IDC_6A         ((INT32)6144)  //4<<10
//qtest0601
#define IDC_7A         ((INT32)7168)  //7<<10
#define IDC_9A         (((INT32)9) << 10)  //15<<10
#define IDC_10A5         ((INT32)10752)  //10.5<<10//IDC_10A5
#define IDC_11A         (((INT32)11) << 10)
//qtest0601
#define IDC_12A         (((INT32)12) << 10)
#define IDC_13A         (((INT32)13) << 10)
#define IDC_14A         (((INT32)14) << 10)
//qtest0601end
#define IDC_15A         (((INT32)15) << 10)  //15<<10
#define IDC_16A         (((INT32)16) << 10)  //16<<10
#define IDC_18A         (((INT32)18) << 10)  //18<<10
#define IDC_20A         (((INT32)20) << 10)  //20<<10
#define IDC_21A         (((INT32)21) << 10)  //26<<10
#define IDC_23A         (((INT32)23) << 10)  //26<<10
#define IDC_26A         (((INT32)26) << 10)  //26<<10
#define IDC_27A         (((INT32)27) << 10)  //26<<10
#define IDC_29A         (((INT32)29) << 10)  //26<<10
#define IDC_32A         (((INT32)32) << 10)  //32<<10
#define IDC_35A         (((INT32)35) << 10)  //35<<10
#define IDC_37A         (((INT32)37) << 10)  //35<<10
#define IDC_40A          (((INT32)40) << 10)  //40<<10
#define IDC_43A          (((INT32)43) << 10)  //43<<10
#define IDC_45A          (((INT32)45) << 10)  //45<<10 
#define IDC_47A          (((INT32)47) << 10)  //47<<10
#define IDC_48A          (((INT32)48) << 10)  //48<<10
#define IDC_52A         (((INT32)52) << 10)  //52<<10 
#define IDC_53A         (((INT32)53) << 10)  //53<<10
#define IDC_55A         (((INT32)55) << 10)  //55<<10
#define IDC_56A         (((INT32)56) << 10)  //56<<10
#define IDC_58A         (((INT32)58) << 10)  //58<<10
#define IDC_59A         (((INT32)59) << 10)  //56<<10
#define IDC_60A         (((INT32)60) << 10)  //60<<10
#define IDC_63A         (((INT32)63) << 10)  //63<<10
#define IDC_65A         (((INT32)65) << 10)  //65<<10
#define IDC_68A         (((INT32)68) << 10)  //68<<10
#define IDC_10A         (((INT32)10) << 10)  //15<<10
#define IDC_50A         (((INT32)50) << 10)  //50<<10
#define IDC_70A         (((INT32)70) << 10)  //70<<10
#define IDC_71A         (((INT32)71) << 10)  //71<<10
#define IDC_72A         (((INT32)72) << 10)  //72<<10
#define IDC_73A         (((INT32)73) << 10)  //73<<10
#define IDC_75A         (((INT32)75) << 10)  //75<<10
#define IDC_76A         (((INT32)76) << 10)  //76<<10
#define IDC_77A         (((INT32)77) << 10)  //77<<10
#define IDC_79A         (((INT32)79) << 10)  //79<<10
#define IDC_80A         (((INT32)80) << 10)  //80<<10
#define IDC_82A         (((INT32)82) << 10)  //82<<10

/*____________________________________________________________________________*/
//
/*_______________________________Power________________________________________*/
//
/*____________________________________________________________________________*/

#define POW_350W        (((INT32)350) << 10)
#define POW_500W        (((INT32)500) << 10)
#define POW_550W        (((INT32)550) << 10)
#define POW_690W        (((INT32)690) << 10)
#define POW_200W        (((INT32)200) << 10)
#define POW_2900W       (((INT32)2900) << 10)
#define POW_3000W       (((INT32)3000) << 10)
#define POW_3200W       (((INT32)3200) << 10)
#define POW_3500W       (((INT32)3500) << 10)

/*____________________________________________________________________________*/
//
/*_______________________________Temperature__________________________________*/
//
/*____________________________________________________________________________*/

#define TEMP_150C           (((INT32)150) << 10)
#define TEMP_125C           (((INT32)125) << 10)
#define TEMP_120C           (((INT32)120) << 10)
#define TEMP_110C           (((INT32)110) << 10)
#define TEMP_100C           (((INT32)100) << 10)
#define TEMP_95C            (((INT32)95) << 10)
#define TEMP_90C            (((INT32)90) << 10)
#define TEMP_85C            (((INT32)85) << 10)
#define TEMP_82C            (((INT32)82) << 10)
#define TEMP_79C            (((INT32)79) << 10)
#define TEMP_77C            (((INT32)77) << 10)
#define TEMP_75C            (((INT32)75) << 10)
#define TEMP_74C            (((INT32)74) << 10)
#define TEMP_70C            (((INT32)70) << 10)
#define TEMP_65C            (((INT32)65) << 10)
#define TEMP_60C            (((INT32)60) << 10)
#define TEMP_56C            (((INT32)56) << 10)
#define TEMP_55C            (((INT32)55) << 10)
#define TEMP_54C            (((INT32)54) << 10)
#define TEMP_53C            (((INT32)53) << 10)
#define TEMP_50C            (((INT32)50) << 10)
#define TEMP_45C            (((INT32)45) << 10)
#define TEMP_40C            (((INT32)40) << 10)
#define TEMP_35C            (((INT32)35) << 10)
#define TEMP_33C            (((INT32)33) << 10)
#define TEMP_30C            (((INT32)30) << 10)
#define TEMP_28C            (((INT32)28) << 10)
#define TEMP_25C            (((INT32)25) << 10)
#define TEMP_20C            (((INT32)20) << 10)
#define TEMP_15C            (((INT32)15) << 10)
#define TEMP_10C            (((INT32)10) << 10)
#define TEMP_6C             (((INT32)6) << 10)
#define TEMP_4C             (((INT32)4) << 10)
#define TEMP_3C             (((INT32)3) << 10)
#define TEMP_2C             (((INT32)2) << 10)
#define TEMP_0C3            ((INT32)307)
#define TEMP_0C7            ((INT32)717)

#define N_TEMP_10C          (((INT32)(-10)) << 10)
#define N_TEMP_5C           (((INT32)(-5)) << 10)
#define N_TEMP_24C          (((INT32)(-24)) << 10)
#define N_TEMP_26C          (((INT32)(-26)) << 10)
#define N_TEMP_27C          (((INT32)(-27)) << 10)
#define N_TEMP_30C          (((INT32)(-30)) << 10)
#define N_TEMP_3C           (((INT32)(-3)) << 10)
#define N_TEMP_40C          (((INT32)(-40)) << 10)

/*_______________________________PFC and DC VOLTAGE RATIO_____________________*/

#define LQ10_VPFC_VDC_RATIO_9P4         ((INT32)9625)				//376/40=9.4  , 9.4*1024=9625
#define LQ10_VPFC_VDC_RATIO_MAX         ((INT32)43827)				//428/10=42.8 , 42.8*1024=43827
#define LQ10_VPFC_VDC_RATIO_MIN         ((INT32)6144)				//6*1024=6144
#define LQ10_VPFC_VDC_RATIO_DEF         ((INT32)7752)				//(405/53.5)=7.57 , 7.57*1024 = 7752

/*____________________________________________________________________________*/
//
/*_______________________________ADC Filter___________________________________*/
//
/*____________________________________________________________________________*/

#define SQRT2       			((INT32)1448) 			// 1.414<<10
#define IQ10_1_59      			((INT32)1628) 			// 1.59<<10 = 1628  //VIN 350V(PEAK)/220V(rms)=1.59
#define IQ10_1_5      			((INT32)1536) 			// 1.5<<10 = 1536
#define IQ10_1_0      			((INT32)1024) 			// 1.0<<10 = 1024   //直流输入
#define IQ10_PERCENT_5  		((INT32)51)				// 0.05<<10 
#define IQ12_ADC_VAC_FILTA  	((INT32)3994)			// 0.975<<12
#define IQ12_ADC_VAC_FILTB  	((INT32)102) 			// 0.025<<12
#define IQ10_ADC_VPFC_FILTA		((INT32)973)  			// 0.95<<10
#define IQ10_ADC_VPFC_FILTB		((INT32)51) 			// 0.05<<10
#define IQ10_ADC_DCCURR_FILTA	((INT32)973)  			// 0.95<<10
#define IQ10_ADC_DCCURR_FILTB	((INT32)51) 			// 0.05<<10
#define IQ10_ADC_PFCCURR_FILTA	((INT32)973)  			// 0.95<<10
#define IQ10_ADC_PFCCURR_FILTB	((INT32)51) 			// 0.05<<10
#define IQ10_ADC_TEMP_FILTA		((INT32)973)  			// 0.95<<10
#define IQ10_ADC_TEMP_FILTB		((INT32)51) 			// 0.05<<10
#define IQ14_ADC_VDC_FILTA  	((INT32)15565)  		// 0.95<<14
#define IQ14_ADC_VDC_FILTB		((INT32)819) 			// 0.05<<14
#define IQ10_IDISP_VADJ    		((INT32)128)   			// 0.125<<10
#define IQ10_VDISP_IADJ    		((INT32)46)				// 0.045<<10
#define IQ10_RATE		        (IQ10_1_0 ) 			// 1.0<<10 = 1024  

/*____________________________________________________________________________*/
//
/*_______________________________TIMER________________________________________*/
//
/*____________________________________________________________________________*/

/*-------------------E2PROM & CANCOM IQ10 TIMER-------------------------------*/

//     Time for 1 hour base                                         
#define  IQ10TIME200000H  	(((INT32)200000) << 10)
//     Time for  1s time base 
#define  IQ10TIME300S  	(((INT32)300) << 10)
#define  IQ10TIME50S   	(((INT32)50) << 10)
#define  IQ10TIME10S   	(((INT32)10) << 10)
     
/*---------------------Time for walkin 100ms base-----------------------------*/ 
                               
#define  WALKTIME8S 	 	(((INT32)800) << 10)
#define  WALKTIME10S 	  	(((INT32)1000) << 10)
#define  WALKTIME120S 	  	(((INT32)12000) << 10)
#define  WALKTIME128S 	 	(((INT32)12800) << 10)
#define  WALKTIME200S  		(((INT32)20000) << 10)

/*--------------------Time for SequeOnTimer 1s base---------------------------*/
 
#define  SEQUE_ON_TIME_2000S     	((UINT32)2000)   
#define  SEQUE_ON_TIME_10S     		((UINT32)10)    //2000  SequeOn_Time

/*--------------------Time for MtimerTreat 5ms base---------------------------*/ 

#define  TIME10MS_BASE     			((UINT16)2)       //2 * 5ms->10ms
#define  TIME20MS_BASE     			((UINT16)4)       //4 * 5ms->20ms
#define  TIME25MS_BASE     			((UINT16)5)       //5 * 5ms->25ms
#define  TIME50MS_BASE     			((UINT16)10)      //10 * 5ms->50ms
#define  TIME115MS_BASE     		((UINT16)23)      //23 * 5ms->115ms
#define  TIME395MS_BASE     		((UINT16)79)      //79 * 5ms->395ms
#define  TIME1S_BASE     			((UINT16)200)     //200 * 5ms->1000ms
#define  TIME5S_BASE                ((UINT16)1000)     //1000 * 5ms->5s
#define  TIME3035MS_BASE     		((UINT16)607)     //607 * 5ms->3035ms
#define  TIME2S_BASE     			((UINT16)400)     //400 * 5ms->2000ms
#define  TIME3300MS_BASE    		((UINT16)660)     //660 * 5ms->3300ms
#define  TIME500MS_BASE    			((UINT16)100)     //100 * 5ms->500ms //ls 20111129

/*--------------------Time for self diagnosis 5ms base-------------------------*/ 

#define  TIME0S5_SELFDX_5MS_BASE    ((UINT16)100)    	//100 * 5ms->0.5s
#define  TIME50MS       ((UINT16)10)        //10 * 5ms->50Ms

#define  TIME3S_SELFDX_5MS_BASE     ((UINT16)600)    	//600 * 5ms->3s
#define  TIME8S_SELFDX_5MS_BASE     ((UINT16)1600)       //1600 * 5ms->8s

/*--------------------Time for self diagnosis 0.5S base-------------------------*/
#define  TIME300S_SELFDX_BASE     	((UINT32)600)    	//600 * 0.5s->5min
#define  TIME30S_SELFDX_BASE     	((UINT32)60)    	//60 * 0.5s->0.5min
#define  TIME320S_SELFDX_BASE     	((UINT32)640)    	//640 * 0.5s->5.3min
#define  TIME270S_SELFDX_BASE     	((UINT32)540)    	//540 * 0.5s->4.5min
#define  TIME23HOUR_SELFDX_BASE     ((UINT32)165600)    //165600 * 0.5s->82800s->1380min->23hour   //
#define  TIME23HOUR20S_SELFDX_BASE  ((UINT32)166240)

#define  TIME15S_SELFDX_BASE        ((UINT16)30)
#define  TIME11S_SELFDX_BASE         ((UINT16)22)
#define  TIME10S_SELFDX_BASE     	((UINT16)20)    	//640 * 0.5s->5.3min
#define  TIME6S_SELFDX_BASE     	((UINT16)12)    	//12 * 0.5s->6s
//500ms base
#define TIME_FUSE_BRO_10S		    ((UINT16)20)			//20 * 0.5s

 
/*-----------------Time for DC start end delay for 10ms base--------------------*/

#define  DCSTARTEND_200MS		((UINT16)20)
#define  DCSTARTEND_500MS		((UINT16)50) 
#define  DCSTARTEND_600MS		((UINT16)60)
#define  DCSTARTEND_250S		((UINT16)25000)
#define  DCSTARTEND_25S	    	((UINT16)2500)
#define  DCSTARTEND_5S	    	((UINT16)500)


/*-----------------Time for CPU CLOCK base--------------------------------------*/
#define  TIME5MS_CPUCLK     	   5000L
                                    
/*-----------------Time for 10ms base-------------------------------------------*/
#define  TIME300S     	((UINT16)30000)    	//30000 * 10ms
#define  TIME150S       ((UINT16)15000)     //15000 * 10ms
#define  TIME60S     	((UINT16)6000)    	//6000 * 10ms
#define  TIME40S     	((UINT16)4000)    	//4000 * 10ms
#define  TIME20S     	((UINT16)2000)    	//2000 * 10ms
#define  TIME15S     	((UINT16)1500)     	//1500 * 10ms
#define  TIME10S        ((UINT16)1000)      //1000 * 10ms
#define  TIME8S         ((UINT16)800)       //800 * 10ms
#define  TIME5S     	((UINT16)500)    	//800 * 10ms
#define  TIME4S8        ((UINT16)480)       //480 * 10ms
#define  TIME4S         ((UINT16)400)       //400 * 10ms
#define  TIME3S     	((UINT16)300)     	//300 * 10ms
#define  TIME2S5     	((UINT16)250)     	//250 * 10ms->2500ms
#define  TIME2S     	((UINT16)200)     	//200 * 10ms
#define  TIME1S5     	((UINT16)150)     	//200 * 10ms
#define  TIME1S     	((UINT16)100)    	//100 * 10ms
#define  TIME500MS      ((UINT16)50)    	//50 * 10ms
#define  TIME250MS      ((UINT16)25)    	//25 * 10ms
#define  TIME150MS      ((UINT16)15)    	//15 * 10ms
#define  TIME100MS      ((UINT16)10)    	//1 * 10ms
#define  TIME80MS       ((UINT16)8)    		//1 * 10ms
#define  TIME30MS       ((UINT16)3)         //1 * 10ms
#define  TIME10MS       ((UINT16)1)   		//1 * 10ms
#define  TIME10MIN      ((UINT16)60000)    //60000*10ms

/*-----------------Time for 20ms base-------------------------------------------*/
#define  TIME3S_20MS_BASE       ((UINT16)150)   		//150 * 20ms = 3s
                                
/*----------------Time for ISR CLOCK 228.5us(70kHZ/16) base--------------------*/

#define  TIME_2S_ISRTIMER    		((UINT16)8753) //228.5us*8753
#define  TIME_6S_ISRTIMER  		((UINT16)26260) //228.5us*26260
#define  TIME150MS_ISRTIMER     	((UINT16)658)	//228.5us*658
#define  TIME60MS_ISRTIMER     	((UINT16)262)	//228.5us*658
#define  TIME20MS_ISRTIMER     	((UINT16)87)	//228.5us*87
#define  TIME5MS_ISRTIMER     	((UINT16)22)	//228.5us*22
#define  TIME1MS_ISRTIMER     	((UINT16)4)		//228.5us*4
#define	 TIME685US_ISRTIMER		((UINT16)3) 	//228.5us*3

                                        
/*----------------Time for relay delay 228.5us(70kHZ/16) base--------------------*/

#define  DeFRLYDELAYACT 	((INT16)66)    //15ms
#define  DEFWAVEPROIDMAX    ((INT16)131)   //30ms/0.22857=131

/*--------------------Time for CAL 115ms base---------------------------*/
#define TIME10S_CAL         ((UINT16)87)
#define TIME1H_CAL    ((INT32)31304)
#define TIME64S_CAL    ((UINT16)559)

/*--------------------Time for power CAL---------------------------*/
#define TIME115MS    ((INT32)115)
#define TIME1H_MIN    ((UINT16)60)
#define TIME1MIN_MS    ((INT32)60000)



#endif
