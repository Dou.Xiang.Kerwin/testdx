/*=============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : HD415CU111_warnctrl.c
 *  PURPOSE  : warn judge and on/off control, panel led, relay, fan control
 *
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A000           DSP               Created.   Pre-research
 *
 *============================================================================*/

#ifndef App_warnctrl_H
#define App_warnctrl_H

//qtest0601
extern UINT16 g_u16PhaseSheddingFlag;
//qtest0601end
extern ubitfloat g_lq10LowTempWalkInTime;

extern ubitfloat g_lq10MdlVoltHvsd;

extern INT16    g_i16FanPwmSet;
extern INT16    g_i16FanRunMin;
extern UINT16 g_u16OverVoltStatus;    // rectifier dcov status
extern UINT16 g_u16HWOverVoltStatus;
extern UINT16 g_u16CloseTime;         // count for rectifier the second HVSD
extern UINT16 g_u16CloseTimeHW;
extern UINT16 g_u16CurrUnBal;         //count for current unbalance
extern UINT16 g_u16CurrSerUnBal;      //count for seriously current unbalance

extern INT16  g_i16FanPwm;            //Fan PWM
extern UINT16   g_u16FanCtrFlag;            //Fan control flag
extern INT16 g_i16FanSpecialSet;
extern UINT16 g_u16Box0FailTimer;     //count for can comm fail timer interrupt
extern UINT16 g_u16CanFailTime;       // count for CAN comm with scu interrupt
extern UINT16 g_u1660sTimer;          // count for AC over voltage 60S timing
extern UINT16   g_u16SafetyShortStatus;     //Safety Short dc restart status
extern UINT16   g_u16DcRestartFailTimes;
extern UINT16 g_u16SaveShortCompleteFlag ;
extern INT16 g_i16FanFailTimer2;
extern ubitfloat g_lq10AntiThiefDelayTime;
extern UINT16 g_u16AntiThiefDelaySet;

extern UINT16  g_u16ReceiveDataEnFlag;

extern UINT16  g_u16CusSitNumDisMatchFlag;
extern UINT16 g_u16AntiThiefDelayCnt;
extern UINT16 g_u16HVSDFistFlag ;     // rectifier HVSD the first time

//paul-20220301
//Anti-theft
extern UINT16  g_u16CustNumDisMatch;
extern UINT16  g_u16ReceiveDataStatus;
extern Uint32  g_u32SitNumberRx;    //scu set Site number
extern UINT16  g_u16CustomerNumberRx;//scu set Customer number
extern Uint32  g_u32SitNumberTx;    //rectifier Site number
extern UINT16  g_u16CustomerNumberTx; //rectifier Customer number




extern void vWarnCtrl(void);
extern void vModelTurnOff_PfcDc(void);
extern INT32 lUpdownlimit(INT32 lPareData , INT32 lHighLever, INT32 lLowLever);

#endif

//===========================================================================
// No more.
//===========================================================================
