 /*=============================================================================*
 *         Copyright(c) 2007-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : GBB Rectifiers
 *
 *  FILENAME : GBB_macro.h
 *  PURPOSE  : Define the io assignment the macro from the hardware of dsp
 *				     
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *  2008-6-10         OOA            Agnes           Created.   Pre-research 
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *    
 *============================================================================*/

/******************************************************************************/
/*IO definition																  */
/*This part is for the IO port supplied by the DSP 							  */
/******************************************************************************/
#ifndef  Prj_config_H
#define  Prj_config_H


//===========================本模块软件版本号的设置==============================//
#define SW_VERSION              (UINT16)    103//V102B007基础上修改而来;


//============================================================================//
//==============================本模块功率的设置==============================//
//============================================================================//
//额定功率3000W  非定标
//#define DEFAULT_POWER           ((INT32)3000)
#define DEFAULT_POWER           ((INT32)4320)   //4350W module
#define DEFAULT_INDUCTANCE      ((INT32)88) //100uh gqian
#define DEFAULT_MOSCOSS         ((INT16)140) //70p
//额定功率 Q10
#define IQ10POW_RATE            ( DEFAULT_POWER << 10)


//===========================本模块系统最大通讯数量==============================//
#define MAX_NUM                 (INT32)60 //30
//===========================本模块效率==========================================//
#define MAX_EFFICIENCY          (INT32)978  //95.5%  Q10

//============================================================================//
//============================================================================//
//==============================硬件采样比的设置==============================//
//============================================================================//
//============================================================================//

//-------------------输入电压（AC或高压直流）---------------------------------//

//(3.63285/803.63285)*(4095/3.184)=5.813941161  Q10=5953
//#define   IQ10_DEF_VACFACTOR      ((INT32)5953)      //
//(4.7/804.7)*(4095/3.184)=7.512  Q10=7692
#define IQ10_DEF_VACFACTOR      ((INT32)7692)

//PFC母线电压硬件采样比，Q10
//---------------------------PFC母线电压-------------------------------------//

//6.2/996.2*（4095/3.184）=8.004348686    Q10=8196
#define IQ10_DEF_VPFCFACTOR     ((INT32)8196)     //

//---------------------------PFC电流-----------------------------------------//

//0.004*8*（5.6/7.1）*(4095/3.184)=32.4608960  Q10=33239
//隔离运放的放大倍数为固定的8倍
#define IQ10_DEF_IPFCFACTOR     ((INT32)33239)     //

//-----------------------输出电压(DC电压)--------------------------------------//

//(2.05/(2.05+21.5+21.5)*(4095/3.184)*8=2.05/45.05*4095/3.184*8=468.19837   Q10=479435
//上面的系数里有个乘以8是因为DC电压在采样后直接左屏?位，相当于硬件采样比放大8倍
#define IQ10_DEF_VDCFACTOR      ((INT32)479435)   //

//-----------------------输出电流原边采样比------------------------------------//

//(0.010mohm*10K/0.56k)*4095/3.184 = 229.66394    Q10 = 235176
#define IQ10_DEF_IDCFACTOR      ((INT32)235176)    //

//-----------------------输出功率原边采样比------------------------------------//

//229.66394 *8.0043*1.2=2206    //1.2 for transformer and component tolerance
//IQ10_DEF_POWERFACTOR = (IQ10_DEF_IDCFACTOR >>10)*(IQ10_DEF_VPFCFACTOR>>10)*1.2   Q10
//PFC母线电压采样比×DC原边电流采样比直接就是Q10的原因：
//是在程序里面，DC功率环的反馈等于PFC母线电压采样值×DC原边电流采样值左移6位（等于右移10位）
#define IQ10_DEF_POWERFACTOR    ((INT32) 2206)  //Q10

//-----------------------输出限流系数采样比------------------------------------//

//输出限流系数采样比（相当于副边电流采样比）
//1A*KIout*Vdc=Power_PFC
//KIout=(2206>>10)<<16/468.19837=KIout=301.5474   Q10=308784.5376
//IQ10_DEF_IDCSYSFACTOR = (IQ10_DEF_POWERFACTOR>>10)<<16/(IQ10_DEF_VDCFACTOR>>10)
#define IQ10_DEF_IDCSYSFACTOR   ((INT32)308785)

//---------------------------环境温度采样比------------------------------------//

//1C -> 0.01*(4095/3.184)=12.86432       Q10: 13173
#define IQ10_DEF_TEMPFACTOR     ((INT32)13173)

//------------------------PFC 偏置电流--------------------------------------//
//#define IPFC_OFFSET_DF_VALUE  (int16)175
//-------------------------DC 偏置电流--------------------------------------//
//#define IDC_OFFSET_DF_VALUE       (int16)130


/*_____________________计算了硬件采样比的功率_____________________________________*/

//DEFAULT_POWER*IQ10_DEF_POWERFACTOR*1.5/1024 = DEFAULT_POWER*2206*1.5/1024
//#define POWER_LIM_UPPER   ((DEFAULT_POWER * 3309)>>10) //150%的限功率点
#define POWER_LIM_UPPER   ((INT16)((((DEFAULT_POWER * IQ10_DEF_POWERFACTOR)>>10)*(INT32)1536)>>10))

//DEFAULT_POWER*IQ10_DEF_POWERFACTOR*0.005/1024 = DEFAULT_POWER*2206*0.005/1024
//#define POW_MAX_DN       (((INT32)DEFAULT_POWER * 11)>>10) //0.5%的限功率点
#define POW_MAX_DN        ((INT16)((((DEFAULT_POWER * IQ10_DEF_POWERFACTOR)>>10)*5)>>10)) //0.5%的限功率点

//DEFAULT_POWER*IQ10_DEF_POWERFACTOR*0.03/1024  = DEFAULT_POWER*2206*0.03/1024
//#define POW_MAX_DELTA       (((INT32)DEFAULT_POWER * 66)>>10)//3%的限功率点
//#define POW_MAX_DELTA       (((INT32)DEFAULT_POWER * 132)>>10)//6%的限功率点
//要参考纹波环的限幅值,6% 要大于VdcRippleUsePos
#define POW_MAX_DELTA            (INT16)(((( DEFAULT_POWER * IQ10_DEF_POWERFACTOR )>>10)* 61)>>10)
//纹波环输出的限幅范围，//5.4%的限功率点
#define VDC_RIPPLE_USE_RANGE     (INT16)(((( DEFAULT_POWER * IQ10_DEF_POWERFACTOR )>>10)* 56)>>10)
#define VDC_RIPPLE_RANGE          50
//限功率调节步径，10% rate power
#define POW_LIM_ADJ_STEP   ((DEFAULT_POWER * IQ10_DEF_POWERFACTOR / 10) >> 10)

// DEFAULT_POWER*2206*0.1/1024
#define POWER_LIM_FLOOR         ((INT16)((DEFAULT_POWER * IQ10_DEF_POWERFACTOR / 10)>>10))


/*_____________________不计算硬件采样比的功率比例__________________________________*/
//DEFAULT_POWER*2.5% Q10  2.5%*1024 =25 Q10
#define POW_LIM_WARN_UP         (DEFAULT_POWER * 25)

//DEFAULT_POWER*3.5% Q10  3.5%*1024=35 Q10
//#define POW_LIM_WARN_BACK     (((INT32)70) << 10)
#define POW_LIM_WARN_BACK       (DEFAULT_POWER * 35)
//Q21*Q10=Q31
#define POWER_Q21    ((INT32)2097151)

/*---------------------电量计量--------------------*/

#define POWER_MAX    ((INT32)2147483647)  //2^31-1
#define POWER_MIN    ((INT32)0)    //
//------------------------------------------------------------------------------
//define constant for dcdc current
//------------------------------------------------------------------------------
/////(DEFAULT_POWER/58)<<7 = 3500/58=68.97 68.97<< 7=7724
//#define IDC_RATE_Q7       ((UINT16)7724)
#define IDC_RATE_Q7         ( ( DEFAULT_POWER<<7 )/58)

#define CURR_RATE           ((DEFAULT_POWER<<10)/58)
#define CURR_RATE_2900W     ((  ((INT32)2900) <<10)/58)
#define CURR_RATE_3000W     ((  ((INT32)3000) <<10)/58)
#define CURR_RATE_3500W     ((  ((INT32)3500) <<10)/58)

//#define CURR_RATE_3200W      (( ((INT32)3200) <<10)/58)
//#define CURR_RATE_3500W      (( ((INT32)3500) <<10)/58)

#define MAX_CURR                (((INT32)CURR_RATE)*1130>>20)
#define MAX_CURR_2900W          (((INT32)CURR_RATE_2900W)*1248>>20)
#define MAX_CURR_3000W          (((INT32)CURR_RATE_3000W)*1248>>20)
#define MAX_CURR_3500W          (((INT32)CURR_RATE_3500W)*1248>>20)
//#define MAX_CURR_3200W            (((INT32)CURR_RATE_3200W)*1248>>20)
//#define MAX_CURR_3500W            (((INT32)CURR_RATE_3500W)*1248>>20)

#define IDC_UP                      (((INT32)MAX_CURR) *1244) //显示电流最大值，给显示电流补偿带来问题
#define IDC_UP_2900W                (((INT32)MAX_CURR_2900W) *1244)
#define IDC_UP_3000W                (((INT32)MAX_CURR_3000W) *1244)
#define IDC_UP_3500W                (((INT32)MAX_CURR_3500W) *1244)
//#define IDC_UP_3200W              (((INT32)MAX_CURR_3200W) *1244)
//#define IDC_UP_3500W              (((INT32)MAX_CURR_3500W) *1244)

//------------输入电流上下限幅值--------------------------------------------//
//因为VAC_LLOSS时模块不可能满功率输出，所以IQ10POW_RATE / VAC_LLOSS 足够表示输入电流显示上限了
#define IPFC_UP     ((IQ10POW_RATE >> 10)/( VAC_LLOSS >> 10))*((INT32)1280 ) //满载按0.8效率。(1/0.8)*1024=1280

//-----------------------20120224 for mix---------------------------------------

#define MIX_TYPE_2900W          0x0001
#define MIX_TYPE_3000e3         0x0002
#define MIX_TYPE_3500e3         0x0004
//#define MIX_TYPE_3200W            0x0002
//#define MIX_TYPE_3500W            0x0004
//#define MIX_TYPE_2000e            0x0008

#define IDC_SYS_MIN         ((INT32)10)

//（最大电流+2A余量）*输出限流系数采样比= 64.5A *　301.5474
#define IDC_SYS_MAX         (((INT32)MAX_CURR+2)*(IQ10_DEF_IDCSYSFACTOR>>10) ) //留两安培余量

//-----------------------------------------------------------------
//define constant for ac  input limt current
// in  the function of vAcCurrLimPowerControl
//------------------------------------------------------------------
//to meet (-1,0) a require （58V*1A*1024*1024）/（3000*1024）=59392/3000
#define ACCRRENT_LIM_DELTA       (((INT32)58*(INT32)1024)/DEFAULT_POWER )


//============================================================================//
//============================================================================//
//==============================阈值==========================================//
//============================================================================//
//============================================================================//

/*_____________________________DC 阈值__________________________________*/

#define VDC_DEF         (VDC_53V5)
#define VDC_POW_LIM     (VDC_58V)
#define VDC_SET_UP      (VDC_58V5)
#define VDC_SET_DN      (VDC_48V)
#define VDCUP_SET_UP    (VDC_59V)
#define VDCUP_SET_DN    (VDC_56V)
#define VDC_DIS_UP      (VDC_60V)
#define VDC_DIS_DN      ((INT32)1)
#define VDC_SHORT_UP    (VDC_10V)
//AD 电压上限 //65*468.19837//DC电压的硬件采样范围一般应该大于65V,
//注意，此数非常接近INT16的上限，如果采样比更大，则范围要小于65V了
#define VDC_AD_MAX              ((IQ10_DEF_VDCFACTOR>>10)*((INT32)65))


#define VDC_AD_42V              ((IQ10_DEF_VDCFACTOR>>10)*((INT32)42))

//20*468.19837=9364 //区分输出是否带有电池
#define VDC_AD_WITH_BATT        ((INT16)( (IQ10_DEF_VDCFACTOR>>10)*((INT32)20) ) )
//0.5*468.19837=234
#define VDC_AD_0V5              ((INT16)( IQ10_DEF_VDCFACTOR>>11 ) )
//5*468.19837=2340  //熔丝自诊断调压值
#define VDC_AD_FUSE_ADJ_5V      ((INT16)((IQ10_DEF_VDCFACTOR>>10)*5))

/*__________________________AC 高压直流 阈值________________________________*/

//10*Q25-->1024*1024*10*2^5=335544320
// 335544320/（80*80）=52429 ，80V以下按80V计算
#define VAC_RMS2_INV_CON        (((INT32)10)<<25)
#define VAC_RMS2_INV_MIN        (VAC_RMS2_INV_CON/(((INT32)80)*((INT32)80)))

#define VAC_DIS_DN              (VAC_50V)
#define VDCIN_DIS_DN            ((VAC_DIS_DN >> 10)*SQRT2)   //50*1.414=72

#define VAC_DIS_CLR             (VAC_0V)
//低压断通讯
#define VAC_COM_HALT            (VAC_80V)
#define VDCIN_COM_HALT          ((VAC_COM_HALT >> 10)*SQRT2)     //80*1.414=113
//AC 欠压用有效值保护
#define VAC_LLOSS               (VAC_80V)
#define VAC_LBACK               (VAC_100V)

#define VDCIN_LLOSS             ( (VAC_LLOSS >> 10 ) * SQRT2 )
#define VDCIN_LBACK             ( (VAC_LBACK >> 10 ) * SQRT2 )

//5.813941161*20   ;xgh IQ10_DEF_VACFACTOR
#define VAC_AD_20V              ((INT16)((IQ10_DEF_VACFACTOR*20)>>10))
#define VAC_AD_PEAK_50V         ((INT16)((((IQ10_DEF_VACFACTOR*SQRT2)>>10) *((INT32)50))>>10)  ) //5.81*1.414*80V=660
#define NEG_VAC_PEAK_50V         ((INT16)((((IQ10_DEF_VACFACTOR*SQRT2)>>10) *((INT32)(-50)))>>10)  )
#define VAC_PEAK_50V            ((INT16)((SQRT2*50)>>10))
//AC 过压用峰值保护,保证无论正弦波还是其他波形保护点都一样。SQRT2=1.414>>10

#define VAC_HLOSS               (VAC_307V5)

#define VAC_HBACK               (VAC_295V5)//(VAC_302V5)

#define VHVDC_HLOSS             (VHVDC_415V) //

#define VHVDC_HBACK             (VHVDC_400V)//

//有效值为310V AC电压的峰值
#define VAC_AD_PEAK_EQU310VRMS   ((INT16)((IQ10_DEF_VACFACTOR*(((VAC_310V>>10)* SQRT2)>>10))>>10))

#define VADC_OUT_VOLT  ((INT32)3184) // 3.184V*1000
#define VADC_INTERNAL_VOLT  ((INT32)3300) // 3.3V*1000
/*____________________________PFC电压 阈值________________________________*/

#define VPFC_HLOSS              (VPFC_450V)
#define VPFC_HBACK              (VPFC_440V)

#define VPFC_LLOSS              (VPFC_300V)

#define VPFC_DIS_UP             (VPFC_500V)
#define VPFC_DIS_DN             (VPFC_60V)

#define VPFC_ADJUST_DN          (VPFC_390V)
#define VPFC_ADJUST_UP          (VPFC_435V)
#define VPFC_ADJUST_UP_Hload    (VPFC_427V)//重载时PFC母线的上限值
#define VPFC_ADJUST_UP_Lload    (VPFC_440V)//轻载时PFC母线的上限值
#define VPFC_OVERRELAY_OPEN     (VPFC_450V)
#define VPFC_OVERRELAY_CLOSE    (VPFC_300V)
#define VPFC_DEF_SET            (VPFC_405V)

/*____________________________温度 阈值________________________________*/

//#define TEMP_AMBI_HLOSS       (TEMP_77C)
//#define TEMP_AMBI_HBACK       (TEMP_74C)

#define TEMP_AMBI_HLOSS         (TEMP_82C)
#define TEMP_AMBI_HBACK         (TEMP_79C)

#define TEMP_PFC_HLOSS          (TEMP_95C)
#define TEMP_PFC_HBACK          (TEMP_75C)

#define TEMP_UP_DEF             (TEMP_95C)//DC 过温保护点

#define TEMPUP_SET_UP           (TEMP_150C)
#define TEMPUP_SET_DN           (TEMP_70C)

#define TEMP_DIS_UP             (TEMP_125C)
#define TEMP_DIS_DN             (N_TEMP_40C)

#define TEMP_BOARD_DIS_UP       (TEMP_125C)
#define TEMP_BOARD_DIS_DN       (TEMP_20C)
#define TEMP_BOARD_HYS          (TEMP_3C)

/*______________________________Vpfc/Vdc设定值比例系数_________________________*/

//#define  VPFC_VDC_K_H_LOAD     ((INT32)7790) // 407V PFC->53.5V DC 407/53.5*1024=7790
//#define  VPFC_VDC_K_L_LOAD     ((INT32)7694)// 402V PFC->53.5VDC 402/53.5*1024=7694
#define    VPFC_VDC_K              ((INT32)7768)//K=7.5862 Q10 7768

//============================================================================//
//============================================================================//
//==========================PFC DC工作频率相关常数============================//
//============================================================================//
//============================================================================//

/*______________________________DC工作频率相关常数_________________________*/

#define DC_TIMER_TBPRD          300//100M/167k=600 三角波up/down 所以600/2=300 DC PWM载波周期值
#define DC_MIN_CMPA             4//用高精度，比0高4个CLK
#define DC_MAX_CMPA             (DC_TIMER_TBPRD - DC_MIN_CMPA)//用高精度，比周期值少4个CLK
//用于中断环路的限幅，DC环路输出PIOUT为PWM周期的2倍，所以
#define DCPWM_TS_MIN            ((INT32)(DC_TIMER_TBPRD * 2)) //250K
#define DC_TS_MAX_40K           ((UINT16)2500) //40K
#define DC_TS_MAX_50K           ((UINT16)2000) //50K
#define DC_TS_MAX_45K           ((UINT16)2222) //45K
#define DC_TS_MAX_85K           ((UINT16)1176) //85K
#define DC_TS_MAX_50K           ((UINT16)2000) //50K
#define DC_TS_MAX_55K          ((UINT16)1818) //55K

#define DC_DB_TIME_400NS        (UINT16)40
#define DC_DB_TIME_600NS        (UINT16)60
#define DC_DB_TIME       (UINT16)60
//强制发波设置
#define DCPWM_TS_TEST           ((INT32)DC_TIMER_TBPRD)
#define DCPWM_DUTY_TEST         ((INT32)((DC_TIMER_TBPRD/2) - 20) )

/*______________________________PFC工作频率相关常数_________________________*/
//--------------------TCM-----------------------------

/*______________________________PFC工作频率相关常数_________________________*/
//--------------------TCM-----------------------------
#define PFC_PRD_10K             10000 // 100MHz/10000=10kHz
#define PFC_PRD_12K             8333 // 100MHz/6666=15kHz
#define PFC_PRD_15K             6666 // 100MHz/6666=15kHz
#define PFC_PRD_20K             5000 // 100MHz/5000=20kHz
#define PFC_PRD_25K             4000 // 100MHz/4000=25kHz
#define PFC_PRD_30K             3333 // 100MHz/3333=30kHz
#define PFC_PRD_70K             1428 // 100MHz/1428=70kHz


#define PFC_PRD_DEF             PFC_PRD_10K //另外一个_PFC_PRD

#define PFC_TON_MIN             12    //200ns  0.2us*60=12CLK
#define PFC_TON_PERMIT          PFC_PRD_DEF -100//4   //200ns  0.2us*60=12CLK

#define PFC_MAX_DUTY            (PFC_PRD_DEF-1200)
#define PFC_MIN_DUTY            1  //没用高精度，所以比周期值少1个CLK
#define PFCPWM_PRD_TEST            PFC_PRD_70K
#define PFCPWM_DUTY_TEST           PFC_PRD_70K/2//50%

//计算中断PWM7
#define ISR_PRD_140K            714 // 100M/140K=714 UP-COUNT MODE

/*______________________________软启动常数_______________________________*/
//428 * 1% = 4.28
#define SSTART_PFCPWM_STEP      ((INT16)PFC_PRD_DEF/100)
//1V Vpfc -->8
#define SSTART_PFCVOL_STEP      ((INT16)IQ10_DEF_VPFCFACTOR>>10)

#define SSTART_DCDCPWM_STEP     ((INT16)3)
#define SSTART_PFCMAXPOW_STEP   ((INT16)10)
//VAC 80V
#define VPFC_SSTART_VAC_LIM     (VAC_80V)
//VPFC 80V
#define VPFC_SSTART_VPFC_LIM    (VPFC_80V)

#define VPFC_AD_30V             ((INT16)((((INT32)30)*IQ10_DEF_VPFCFACTOR)>>10))
#define VPFC_AD_320V            ((INT16)((((INT32)320)*IQ10_DEF_VPFCFACTOR)>>10))
#define VPFC_AD_350V            ((INT16)((((INT32)350)*IQ10_DEF_VPFCFACTOR)>>10))
#define VPFC_AD_380V            ((INT16)((((INT32)380)*IQ10_DEF_VPFCFACTOR)>>10))
#define VPFC_AD_480V            ((INT16)((((INT32)480)*IQ10_DEF_VPFCFACTOR)>>10))

#define VPFC_AD_DC_START        (VPFC_AD_380V)

#define VPFC_AD_Overshoot25V    ((INT16)((((INT32)25)*IQ10_DEF_VPFCFACTOR)>>10))//25v
#define VPFC_AD_Overshoot20V    ((INT16)((((INT32)20)*IQ10_DEF_VPFCFACTOR)>>10))//20v
//480*8.004=3842
#define VPFC_AD_480V            ((INT16)((((INT32)480)*IQ10_DEF_VPFCFACTOR)>>10))
//#define   VPFC_AD_DC_START        ((INT16)2240)  //for debug 20111018
#define SSTART_DCTS_DELTA       (DC_TS_MAX_40K/8)  //750/8=94

#define WALKIN_TIME_K           ((INT32)2258)//((INT32)2520)
#define WALKIN_STEP_CNT         ((INT32)200) //150  //ls 20120202
//#define WALKIN_STEP_CNT2        ((INT32)1200) //150  //ls 20120202
#define WALKIN_STEP_CNT2        ((INT32)2400) //duanqingan
#define WALKIN_STEP_LOWTMP_CNT  ((INT32)5000)



#define PFC_MAXPOW              ((INT16)5800)

#define PFC_DYN_FLG_LOW_LIMIT   6     //PFC动态策略的一个标志
#define ACCURRENT_IVTEFF           ((INT32)1077) //avg95%



/*______________________________短路系数______________________________________*/
// SHORT_COEF_A = (INT16)_IQ10div((MAX_CURR*IQ10_DEF_POWERFACTOR ),IQ10_DEF_VDCFACTOR);
//62.5*2206/467.4319=       （（原边功率1W/边电压1V）×最大电流）（功率，电流环转成功率环）
//#define SHORT_COEF_A            ((INT32)295)
#define SHORT_COEF_A            ((IQ10_DEF_POWERFACTOR*MAX_CURR)/(IQ10_DEF_VDCFACTOR>>10))
#define SHORT_COEF_B_DEF            ((INT16)360)//初始值

#define IDC_RATE_50PCT          ((INT32)CURR_RATE>>1)

#define IDC_RATE_20PCT          (((INT32)CURR_RATE*205)>>10)

#define IDC_RATE_16PCT          (((INT32)CURR_RATE*164)>>10)

#define IDC_RATE_12PCT          (((INT32)CURR_RATE*123)>>10)


#define IDC_RATE_5PCT           (((INT32)CURR_RATE*51)>>10)

#define IDC_RATE_4PCT           (((INT32)CURR_RATE*41)>>10)

#define IDC_RATE_2_5PCT         (((INT32)CURR_RATE*26)>>10)

#define IDC_RATE_2PCT           (((INT32)CURR_RATE*21)>>10)

#define IDC_LOAD_10PCT          (((INT32)CURR_RATE*111)>>10) //用于DCOVP 10%负载电流限定

#define IDC_4PCT                ((INT32)4096)

//--------------------------------------------------------------------
//define constant for shareload to adjust vloatge    start
//-------------------------------------------------------------------
//#define LOAD_REG_DELTA_UP     (((INT16)35))   //75mv 75*468.2/1000=35
#define LOAD_REG_DELTA_UP      ((INT16)((((INT32)IQ10_DEF_VDCFACTOR >>10)*75)/1000))
#define LOAD_REG_DELTA_DN       ( ((INT16)(-1)) * LOAD_REG_DELTA_UP )  //-75mv


#define CURR_SHARE_DELTAK       (((INT32)LOAD_REG_DELTA_UP<<20)/IDC_RATE_50PCT)//可以调整


//---------------------------均流调压的上下限幅，为正负0.4V----------------------//
//#define CURR_DELTA_DN         (INT16)(-200)
//#define CURR_DELTA_UP         (INT16)(200)

#define CURR_DELTA_DN           ((INT16)(((-4)*(IQ10_DEF_VDCFACTOR>>10))/10))
#define CURR_DELTA_UP           ((INT16)(((4)*(IQ10_DEF_VDCFACTOR>>10))/10))

#define VOLT_ADJUST_DEF         ((UINT16)128)

//--------------------------------------------------------------------
//PFC电流超前相位补偿值的上下限幅 //
//--------------------------------------------------------------------
#define PFCPHASESHIFTUPVAL   ((UINT16)60)
#define PFCPHASESHIFTCLRVAL   ((UINT16)1)
#define TONSHIFTUPPERLIM   ((UINT16)200)
#define TONSHIFTLOWERLIM   ((UINT16)40)

//----------------------------------------------------------------
//AC输入频率限幅
//----------------------------------------------------------------
#define FREQUENCY_UP         ((INT32)700)
#define FREQUENCY_DOWN       ((INT32)0)
#define FREQUENCY_55Hz       (((INT32)55) << 10)
//----------------------------------------------------------------
//AC输入频率限幅
//----------------------------------------------------------------
#define FREQUENCY_UP         ((INT32)700)
#define FREQUENCY_DOWN       ((INT32)0)


#define LOAD_12_PERCENT    ((INT32)122) //0.12*1024
#define LOAD_15_PERCENT    ((INT32)153) //0.15*1024
#define LOAD_25_PERCENT    ((INT32)256) //0.25*1024
#define LOAD_35_PERCENT    ((INT32)358) //0.35*1024
#define LOAD_45_PERCENT    ((INT32)461) //0.45*1024
#define LOAD_55_PERCENT    ((INT32)563) //0.55*1024
#define LOAD_65_PERCENT    ((INT32)666) //0.65*1024
#define LOAD_80_PERCENT    ((INT32)819) //0.8*1024
#define LOAD_90_PERCENT    ((INT32)922) //0.9*1024

//============================================================================//
//============================================================================//
//=========================板温曲线拟和系数===================================//
//============================================================================//
//============================================================================//
#define LQ10_ADC_TEMP_3V18      ((INT32)3251) ////3.175V*1024=3251
#define LQ10_ADC_TEMP_2V99      ((INT32)3068) ////2.9959v*1024=3068
#define LQ10_ADC_TEMP_2V52      ((INT32)2584) ////2.523257813*1024=2584
#define LQ10_ADC_TEMP_1V18      ((INT32)1203) ////1.175*1024= 1203
#define LQ10_ADC_TEMP_1V95      ((INT32)1997) ////1.175*1024= 1203

//y = 410.19x^2 - 2369.8x + 3509
#define LQ10_ADC_TEMP_HIGHT_K1      ((INT32)420035)
#define LQ10_ADC_TEMP_HIGHT_K2      ((INT32)2426675)
#define LQ10_ADC_TEMP_HIGHT_K3      ((INT32)3593216)

//  y = 60.346x^2 - 269.66x + 356.69
#define LQ10_ADC_TEMP_K1        ((INT32)61794)
#define LQ10_ADC_TEMP_K2        ((INT32)276132)
#define LQ10_ADC_TEMP_K3        ((INT32)365251)

//  y = 6.4602x^2 + 5.6109x + 4.5663
#define LQ10_ADC_TEMP_LOWT_K1       ((INT32)6615)
#define LQ10_ADC_TEMP_LOWT_K2       ((INT32)5746)
#define LQ10_ADC_TEMP_LOWT_K3       ((INT32)4676)


//============================================================================//
//============================================================================//
//==============================风扇的设置====================================//
//============================================================================//
//============================================================================//
#define FAN_CMP_MIN             ((INT16)3999)
#define FAN_RUN_MIN             ((INT16)2800)     //4000-2800=1200 4000*0.3=1200
#define FAN_PWM_PRD             ((INT16)4000)     //25khz
#define FAN_HIGH                ((INT16)40)       //99%DUTY
#define FAN_LOW                 ((INT16)2200)     //4000-2200=1800 45% DUTY

#define FAN_EXTRA_LOW           ((INT16)2720)     //4000-2720=1280 32% DUTY
#define FAN_MIN_LOWTEMP         ((INT16)1600)     //4000-1600=2400 60% DUTY
#define FAN_DELTA               ((INT16)16)       //风扇调速的步径
#define FAN_DELTA_SMALL         ((INT16)5)        //风扇调速的更小的步径

#define FAN_30_PERCENT          ((INT32)2800)//
#define FAN_44_PERCENT          ((INT32)2240)
#define FAN_46_PERCENT          ((INT32)2160)
#define FAN_52_PERCENT          ((INT32)1920)
#define FAN_54_PERCENT          ((INT32)1840)
#define FAN_58_PERCENT          ((INT32)1680)
#define FAN_62_PERCENT          ((INT32)1520)
#define FAN_70_PERCENT          ((INT32)1200)
#define FAN_82_PERCENT          ((INT32)720)

#define FAN_T_FACTOR            ((INT32)5058)
#define FAN_P_FACTOR            ((INT32)58)     //半载风扇转速为45％，3500W满载为85％，4000W满载为90.6％
#define FAN_M_T_FACTOR          ((INT32)216)    //板温65度,不提供风扇加速,板温75度,提供满转加速 ，[1320(FAN_LOW) - 20(FAN_HIGH)]/(75-65)=130
#define FAN_M_T2_FACTOR         ((INT32)270)    //模块额定态工作，板温65度,不提供风扇加速,板温75度,提供满转加速，[1632(FAN_EXTRA_LOW) - 20(FAN_HIGH)]/(75-65)=162
#define FAN_Vout_FACTOR         ((INT32)361)    //输出电压48V,不提供风扇加速,输出电压42V,提供满转加速。[1320(FAN_LOW) - 20(FAN_HIGH)]/(48-42)=217
#define FAN_INTERNAL_DSPTEMP_FACTOR     ((INT32)240) //DSP内部温度采样调节风扇转速,每5度调节6%

#define FAN_OK                  ((UINT16)0x0000)
#define FAN_ERR_ONCE            ((UINT16)0x0001)

#define FAN_FAULT               ((UINT16)0x3FFF) //11111111111  14*80=1120ms
#define FAN_RUN_ENABLE          ((UINT16)0x0001)
#define FAN_RUN_DISABLE         ((UINT16)0x0000)

#define VFAN_BAD_AD_2V5         ((INT16)3400)
#define VFAN_BAD_AD_0V5         ((INT16)680)

//============================模块数量===================================//
#define MODL_NUMBER_8           ((UINT16)7)   //地址0～7，代表8个模块
#define MODL_NUMBER_12          ((UINT16)11)   //地址0～11，代表12个模块
#define MODL_NUMBER_20          ((UINT16)19)   //地址0～19，代表20个模块

//============================================================================//
//============================================================================//
//==========================Cal文件计算功能的步径=============================//
//============================================================================//
//============================================================================//
//----------------------------DC电沟节步径----------------------------------//
#define DCVOLT_REGU_STEP        ((IQ10_DEF_VDCFACTOR>>10)/10)    //0.1V
//----------------------------DC设定限流值调节步径----------------------------//
#define DCCURR_REGU_STEP        (IQ10_RATE/((INT32)10))   //1024/10 10%
//----------------------------DC限功率调节步径--------------------------------//
#define POW_REGU_STEP           (IQ10_RATE/((INT32)10))//可以调整   10%
//----------------------------PFC电压调节步径---------------------------------//
#define PFCVOLT_REGU_STEP        (VPFC_0V5)
//----------------------------DC真实限流值调节步径----------------------------//
//#define IDC_SYS_REGU_STEP     ((INT32)100)

#define IDC_SYS_REGU_STEP   ((INT32)((_IQ10mpy(IQ10_DEF_IDCSYSFACTOR,g_i32CurrRate)>>10)/10))
//----------------------------DC限流最小值调节步径----CurrentlimFloor---------//
#define IDC_POW_REGU_STEP       ((INT16)10)
#define IDC_POW_LIM_DN          ((INT16)20)
//-----------------------PFC 频率调节步径----------------------------------------//
//#define PFCFREQ_REGU_STEP        1       //428~70K PFC髡?频率步??
//-----------------------PFC 电流环增益调诓骄?---------------------------------//
#define IPFCLOOPGAIN_REGU_STEP   ((UINT16)3)



//============================================================================//
//============================================================================//
//==========================CPU 存储空间的配置================================//
//============================================================================//
//============================================================================//
////xgh    9.29   //堆栈里的还不能清空，要注意
//为了优化内存分配，把CAN数组由原来的.ebss移到固定的0X0~0X400地址
//#define CAN_ARRAY_RAM_START       ((UINT32)0x8D80)
//#define CAN_ARRAY_RAM_END     ((UINT32)0x8F40)
#define CAN_ARRAY_RAM_START     ((UINT32)0xC400)
#define CAN_ARRAY_RAM_END       ((UINT32)0xC5C0)


//#define CAN_VOL_ARRAY_RAM_START       ((UINT32)0x740)
//#define CAN_VOL_ARRAY_RAM_END     ((UINT32)0x7C0)
#define CAN_VOL_ARRAY_RAM_START     ((UINT32)0xC5C0)
#define CAN_VOL_ARRAY_RAM_END       ((UINT32)0xC640)


//0X7E0~0X800 里存放了模块地址等，在CAN通讯下载时不能清空
//所以BOOTMAIN不清空而MAIN中清空
//#define BOOT_ISR_RAM_START        ((UINT32)0x600)
//#define BOOT_ISR_RAM_END      ((UINT32)0x740)
#define BOOT_ISR_RAM_START      ((UINT32)0xC000)
#define BOOT_ISR_RAM_END        ((UINT32)0xC400)


//#define MAIN_ISR_RAM_START        ((UINT32)0xC000)
//#define MAIN_ISR_RAM_END      ((UINT32)0xC640)//把CAN的一部分数组也包括进去了，不过也就是多清零一次

#define MAIN_ISR_RAM_START      ((UINT32)0xC000)
#define MAIN_ISR_RAM_END        ((UINT32)0xC400)//把CAN的一部分数组也包括进去了，不过也就是多清零一次

#define MAIN_ISR_RAM_START1      ((UINT32)0xE000)
#define MAIN_ISR_RAM_END1       ((UINT32)0xE00D)


//#define BOOT_RAM_START        ((UINT32)0x8000)
//#define BOOT_RAM_END      ((UINT32)0xA000)  //ls change RAM 20120319

#define BOOT_RAM_START      ((UINT32)0xE00E)
#define BOOT_RAM_END        ((UINT32)0x14000)  //ls change RAM 20120319

//0x8f40~0x9000 存放固定地址的内存，在MAIN的RAM空间初始化时不要清空
#define MAIN_RAM_START      ((UINT32)0xE200)
#define MAIN_RAM_END        ((UINT32)0x14000)  //ebss  0x8940  + 0x440 =0x8D80

#define CPUTOCLA_RAM_START ((UINT32)0x1500)
#define CPUTOCLA_RAM_END   ((UINT32)0x157F)

//----------------------------- for boot loader-----------------------------//
#define MAX_BLOCK_LENGTH    0x0800
#define ERASE_FLASH_LENGTH  0x7000


// for PFC and DCDC fail record, mhp 20120905
#define TIME_HOUR_6MONTH    ((INT32)4368 << 10)     // 182days * 24
//#define TIME_HOUR_6MONTH  ((INT32)0<< 10)     // 调试防维修用

#define RECDCBRK    ((UINT32)0x157A9)
#define RECPFCFAIL  ((UINT32)0x2566D)
#define RECENABLE   ((INT16)1)
#define RECDISABLE  ((INT16)0)

#ifndef NULL
#define NULL 0
#endif
//============================================================================//
//============================================================================//
//==============================模块类型设置==================================//
//============================================================================//
//============================================================================//

//----------------------------模块条码/征字----------------------------------//
#define CLEAR_TYPE2                 0xFF0F  //把节点类型也清除掉，特征字的TYPE2和节点类型
#define CLEAR_TYPE0_1               0xDF0F  //特征字的TYPE0，TYPE1

//54、55送给监控的（条码、特征字为35），地址识别0x03牟桓模ㄌ趼搿⑻卣髯治?0)

//TYPE = TYPE2×32＋TYPE1×16＋TYPE0
//zlm 20170228
//TYPE : 90 change  to 92
//90= 2*32+1*16+10, TYPE2=2,TYPE1=1,TYPE0=10
//92= 2*32+1*16+12, TYPE2=2,TYPE1=1,TYPE0=12

//#define SET_TYPE0_1_G3_3000W                  0x20A0
//#define SET_TYPE0_1_G3_3000W                  0x20C0
//#define SET_TYPE0_1_G396_3000W                  0x20D0
#define SET_TYPE0_1_G3_4320W                     0x20E0

#define SET_TYPE0_1_G3_4320W_TO_CONTROLLER      0x20E0
#define SET_TYPE2_G3_4320W                      0x0020  //
#define SET_TYPE2_G3_4320W_TO_CONTROLLER        0x0020 //same as G2 3000W

//----------------------模块类型特征数据---------------------------------------//
//#define  MDL_CHARCT_H         0x0096         //单供电,75A
#define  MDL_CHARCT_H           0x0095         //单供电,74.5A
//#define  MDL_CHARCT_L         0x05FF         //
#define  MDL_CHARCT_L           0x07FF         //0x05FF

//----------------------模块软件编码料号---------------------------------------//
//#define  MDL_SW_BOM_SN         0xC3B4//05150100
//#define  MDL_SW_BOM_SN         0xC42C//05150220
#define  MDL_SW_BOM_SN         0xC466//05150278

//-----------------------故障日志相关------------------------------------------//
//eeprom
#define  TIME_0S_1STIMER        ((INT32)0) //0s
#define  TIME_0HOUR5_1STIMER   ((INT32)1800) //1800s =0.5hour

// 32bit 65536*65536=                     4294967296
//#define  TIME_100Year_1S_Timer   ((UINT32)3153600000) //100年= 3600s *24*365*100 =3153 600 000s

#define  RUNTIME_UP_LIMIT   ((INT32)0x7FFFFFFE) // 2147483646/(3600S *24*365) ~=68 year

#define  EEPROM_W_SIZE      (INT16)21


extern void InitPeripherals(void);
extern void EnableInterrupts();

#ifndef TRUE
#define  TRUE               0xFFFF
#else
#error "had define True"
#endif

#ifndef  FALSH
#define  FALSE              0
#else
#error "had define True"
#endif

#endif
/*inline void vi32MaxMinLimit(INT32 LimitSymbol, INT32 LimitMax, INT32 LimitMin)
{
    LimitSymbol= (LimitSymbol > LimitMax) ? LimitMax : LimitSymbol;
    LimitSymbol= (LimitSymbol < LimitMin) ? LimitMin : LimitSymbol;
}
*/
/*============================================================================*/


