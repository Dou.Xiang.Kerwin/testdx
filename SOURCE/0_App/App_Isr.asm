 ;#########################################################
; Date: from 2011/12/23
; Version:1.01     Change Date: 	
;#########################################################
;----------------------------------------------------------------------
;  interrupt code                                              
;----------------------------------------------------------------------
	.include  "App_IsrVariableDefs.asm"
	.include  "Prj_IsrAdc.asm"
    .cdecls C, LIST, "Prj_macro.h"



; fs = 70e3 Hz
;filter=tf([1],[1/6.28/10 1]);c2d(filter,Ts*16,'tustin')	
_Filter10HzK1			.equ    -934
_Filter10HzK2			.equ 	467
_Filter10HzK3			.equ 	467
;fp=1000
_Filter1HzK1			.equ    168
_Filter1HzK2			.equ 	428
_Filter1HzK3			.equ 	428
 
_VpfcDynamicNeg 		.equ 	-200	;ls 20120202
_VpfcDynamicPos 		.equ 	200

_DcdcPWMTsMaxHighLine_45K  .equ 	2222  ;;750;;100M/1250=80k
;_DcdcPWMTsMaxLowLine_103k  .equ 	580  ;;580;;60M/580=103k
;_DcdcPWMTsLIM_95K          .equ 	630  ;;630;;60M/630=95k
_DcdcPWMTsLIM_80K         .equ 	1250  ;;428;;100M/714=140k

;;;Added by XB20140307
_CurrShareDeltaK		.equ	70;;35
_CurrDeltaPos			.equ	100;50 
_CurrDeltaNeg			.equ	-100;;50
;;;Added by XB20140307
_VRippleKQn				.equ 	5;	*64

_PFC_DYN_FLG_LOW_LIMIT  .equ    6

;--------------------CMPA LIMIT---------------------------------;
_PFC_PRD		 .equ	6666;100M/6666=15k
;_CMPAMAX         .equ   (_PFC_PRD- 1000);;;30us
;_CMPAMAX         .equ   900;;;9us
_CMPAMAX         .equ   (_PFC_PRD - 3666);;;30us
;_CMPBMAX         .equ   (_PFC_PRD - 200) ;;保证余量,此处务必大于120
_CMPBMAX         .equ   (_PFC_PRD - 3166) ;;保证余量,此处务必大于120
_CMPSAFE  		 .equ   6000;; 必须留够裕量，不然预制错相会失败
;_CMPAMIN         .equ   30;;12
_CMPAMIN         .equ   50;;12
;;这里需要更改等比例还是减少同样的值？ 如果有XINT直接会被更新的
;_PWM3_TBCTR_INTI .equ   (_CMPAMAX +20);;;900+20;;9.2us多加一点防止CTR不同步问题
_PWM3_TBCTR_INTI .equ   (_CMPAMAX +33);;;900+20;;9.2us多加一点防止CTR不同步问题
;;这里需要更改等比例还是减少同样的值？ 如果有XINT直接会被更新的
_PWM_TBCTR_INTI .equ    (_PFC_PRD- 2000);;;1810;;多加一点防止CTR不同步问题
;--------------------DC电压阈值--------------------------------------;
;;;DC电压硬件采样比为468.19837(CNT/V)
_ShortCircuitVolt		.equ	3744 	;8V*468=3744
_VdcShortCoefB          .equ	173

_VdcdcDynamicNeg		.equ	-60;-80
_VdcdcDynamicPos		.equ	60;80
_VdcdcDynamicNeglowpower 	.equ	-230;-80
_VdcdcDynamicPoslowpower	.equ	230;80

_AvoidHVSDVolt          .equ	3276 	;7V*468=3276
_DC4V                   .equ	1872 	;4V*468=1872 
_DC0V064                .equ   30;      ;0.064V*468=30
_N_DC0V064              .equ   -30;     ;-0.064V*468=-30
_DC0V427                .equ   200;     ;0.427V*468=200
_N_DC0V427              .equ   -200;    ;-0.427V*468=-200
_DC58V5					.equ   27378	;58.5V*468=27378

;;;;定标系数;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
_Q10                    .equ     1024
_N_Q10                  .equ    -1024

	;注意这里使用了3个ADC
	.global	_AdcaResultRegs     ;0B00H
	.global	_AdcbResultRegs     ;0B20H
	.global	_AdccResultRegs     ;0B40H

	.global	_PieCtrlRegs
	.global	_AdcRegs
	.global	_GpioDataRegs
	.global	_EPwm1Regs
	.global	_EPwm2Regs
    .global	_EPwm3Regs
    .global	_EPwm4Regs
    .global	_EPwm5Regs
	.global	_EPwm6Regs
	.global	_EPwm7Regs
	.global	_EPwm8Regs
	.global	_ECap1Regs
	.global	_ECap2Regs

	.global	_ECap4Regs

    .global	_Cmpss2Regs
    .global _Cmpss3Regs
    .global _Cmpss4Regs
    .global _Cmpss6Regs

	.global	_IsrVars    
    .global	_CputoClaVar
    .global _ClatoCpuVar
	.global _g_u16MdlStatus
	.global _g_u16ActionReady
    .global _g_lq10MdlVolt
	.global _g_u16ACMAXPolarity
	.global _g_u16MdlStatusExtend
	.global _g_lq10VpfcSampSys;YQH 20200120


	.def    _Epwm8_Isr
	.sect	"IsrRamfuncs"
	.global _Epwm8_Isr

	;.def    _Epwm1_Isr
	;.sect	"IsrRamfuncs"
	;.global _Epwm1_Isr

	.def    _Epwm3_Isr
	.sect	"IsrRamfuncs"
	.global _Epwm3_Isr



SwitchTable:
	.long 	Switch0	
	.long 	Switch1
	.long 	Switch2
	.long 	Switch3		
	.long 	Switch4	
	.long 	Switch5
	.long 	Switch6
	.long 	Switch7	
	.long 	Switch8	
	.long 	Switch9
	.long 	Switch10
	.long 	Switch11		
	.long 	Switch12	
	.long 	Switch13
	.long 	Switch14
	.long 	Switch15	
	
;******************************************************************************
;* FNAME: _Epwm8_Isr                     FR SIZE:  18          				  *
;*                                                             				  *
;* FUNCTION ENVIRONMENT                                        				  *
;*                                                             				  *
;* FUNCTION PROPERTIES                                         				  *
;*                            0 Parameter,  6 Auto, 10 SOE     				  *
;******************************************************************************
_Epwm8_Isr:
	; entry into Isr ,TBCTR==0x00E
	;automatic context save :
	;ST0,T,AL,AH,PL,PH,AR0,AR1,ST1,DP,IER,DBGSTAT,return address
	;context save	    
	    ASP
        MOVL      *SP++,XAR7
        MOVL      *SP++,XT
		MOV32     *SP++, R0H
		MOV32     *SP++, R1H
		MOV32     *SP++, R2H
		MOV32     *SP++, R3H
        SPM       0

        ;ADDB      SP,#8		        
		NOP 	  *,ARP7        
;----------------------------------------------------------------------
;  | PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;	
;	// Acknowledge this int to receive more int from group 3                                                     
;----------------------------------------------------------------------
		MOVW    DP,#_PieCtrlRegs+_PIEACK
		MOV     @_PieCtrlRegs+_PIEACK,#4
		;CLRC    PAGE0,OVM,SXM,INTM
		CLRC    PAGE0,OVM,SXM
		OR 		IER,#0x0004

;----------------------------Voltage Shift-----------------------------
		MOVW   	DP,#_IsrVars + _PFC1_DP  ;末尾更换过来
		MOV    AH,@_i16AdAcVoltTrue;;;Vac
        MOVW   DP,#_IsrVars + _PFC_DP ;;;第一次换页,末尾要换过来啊

		MOV    AL,@_u16AdAcVoltcount
		ADD    AL,#1
		CMP    AL,#47; 实际变量到了52
		MOVB   AL,#0,GT
		MOV    @_u16AdAcVoltcount,AL
		ADD    AL,#_IsrVars + _i16AdAcVoltDelay0
		MOVZ   AR7,AL
		MOV    *,AH 	;采样值缓存;这里有一条流水线竞争+3

        MOV    AH,@_u16AcVoltPhaseshift;;;60A
		SUB    AL,AH;前移_u16AcVoltPhaseshift 个值。
		CMP    AL,#_IsrVars + _i16AdAcVoltDelay0;;和基地址比较，如果小于则加个模
		BF     VacPhaseShift , GEQ ;
		ADD    AL, #48
VacPhaseShift:
		MOVZ	AR7,AL
		MOV		AH,* 	;这里有一条流水线竞争+3
		MOVL  	XAR7,#_IsrVars + _i16AdAcVoltPhaseShift
		MOV    	*, AH
;------------------------------------------------------------------------------
;**************************Sub-function Modules********************************
;*Description:PFC
;			用于计算TCM中需要使用的一些关键数据，原来执行于CLA中。
;*operation time：--- clk
;******************************************************************************
		;get _f32VpfcVal

		;MOVL    XAR7,#_GpioDataRegs+_GPASET  ;;ty: ISR test
      	;OR      *,#B10_MSK

		;f32VpfcVal
		MOVF32  	R0H,#0.124124 ;this is a Voltage to voltage ratio
		MOVL    	XAR7,#_AdccResultRegs+_PFCVSAMP
		UI16TOF32 	R1H,*XAR7;TOF32;
		MOVW   		DP,#_IsrVars + _PFC5_DP
		MPYF32  	R0H,R0H,R1H
		MOVIZ   	R1H,#0x3f80 ;R1H=1
		MINF32  	R0H,#490.0
		MAXF32  	R0H,#10.0
		MOV32   	@_f32VpfcVal,R0H;True Value PFC
		;1/f32VpfcVal
		DIVF32  	R0H,R1H,R0H
		MOVF32  	R2H,#7500.0  ;maxprd=13.3333kHz  100M/7500 = 13.333khz
		MOV32   	@_f32HVTonPermitMaxPrd,R2H;f32HVTonPermitMaxPrd*duty is  (Ton+2*T)*Kps Up Limit 。
		MOVF32  	R1H,#333.0  ;;300kHz    100M/333 = 300khz
		MOV32   	@_f32TonPermitMinPrd,R1H  ; _f32TonPermitMinPrd*duty is  (Ton+2*T)*Kps Down Limit 。
		MOV32   	@_f32InvVpfcVal,R0H
		;g_f32VacVal = (u16Vref1V6CLA - AdcaResultRegs.ADCRESULT1)* 0.264628;
		MOVL      	XAR7,#_AdccResultRegs+_VACSAMP
		UI16TOF32 	R0H,*XAR7   ;;R0H=VAC_AD
		UI16TOF32 	R2H,@_u16Vref1V6CLA   ;;R2H=OFFSET default is 2048
		MOVL   	  	XAR7,#_IsrVars + _i16AdAcVoltTrue
		SUBF32    	R0H,R2H,R0H   ;R0H=VAC_AD-OFFSET
	;	MOVF32    	R1H,#0.264628 ;R1H=ratio
		MOV32    	R1H,@_f32FilterCoef ;R1H=0.2/0.4 New
		ABSF32	  	R3H,R0H		;R3H=|R0H|=|VAC_AD-OFFSET|
;-----------------------APPLY FILTER TO VAC sample by wangqian---------------------------
		I16TOF32	R2H,*XAR7		
		MPYF32		R0H,R3H,R1H ; R0H=New Value
		SUBF32		R1H,#1.0,R1H ;1-0.2(or 0.4)
		MOVL   	  	XAR7,#_IsrVars + _i16AdAcVoltNoFilter
		MPYF32		R2H,R2H,R1H; R2H=Old Value
		F32TOI16R	R1H, R3H
		ADDF32		R0H,R0H,R2H	; R0H=0.2*New+0.8*Old
		MOV16	  	*,R1H         ;_i16AdAcVoltNF(NoFilter)=R1H=|VAC_AD-OFFSET|
		MOVF32    	R1H,#0.264628 ;R1H=ratio
		MPYF32		R3H,R3H,R1H ; R3H(NoFilter)=|VAC_AD-OFFSET|*ratio
		MOVL   	  	XAR7,#_IsrVars + _i16AdAcVoltTrue
		MOV32     	@_f32VacVal,R3H;@_f32VacVal=VacNoFilter
;----------------------APPLY FILTER TO VAC sample by wangqian----------------------------
		F32TOI16R 	R2H, R0H      ;R2H=|VAC_AD-OFFSET|(Filtered)
		MPYF32		R0H, R0H, R1H ;R0H=R0H(AD_filter)*ratio
		MOV16	  	*,R2H         ;_i16AdAcVoltTrue(Filter)=R2H=VAC_AD-OFFSET
		ABSF32    	R1H, R0H	  ;R1H=Vac_Filter
		MINF32    	R1H, #490.0
		MAXF32    	R1H, #10.0
		MOV32     	@_f32VacAbsUse,R1H   ;_f32VacAbsUse用滤波的值
;;-------------------------------(Vpfc-VacUse) Cal-------------------------------------
    	MOV32      	R2H,@_f32VpfcVal;;;R2=Vpfc
    	SUBF32     	R1H,R2H,R1H  ;;R1=Vpfc-|Vac_Filter|来自于上面
		MOVIZ   	R0H,#0x3f80 ;R0=1
	;;	ABSF32	   	R1H, R1H
    	MINF32     	R1H,#490.0 ;;;设最高490V差值
    	MAXF32     	R1H,#2.0;;;设最低2V差值

;;-------------------------------1/(Vpfc-VacUse) Cal----------------------------------------
		DIVF32  	R3H,R0H,R1H ;R3H=1/R1=1/(Vpfc-VacUse)
		MOV32	   	@_f32DiffVpfcVacUse, R1H;;;R1H=Vpfc-VacUse
		MOV32		R1H, @_f32VacVal ;R1H=VacNoFilter
		SUBF32		R1H, R2H, R1H ;R1H=Vpfc-|Vac_NoFilter|;必须有正负
		NOP
		F32TOI16R  	R2H, R1H
		MOV32		R1H, @_f32VacAbsUse
		MOV16      	@_i16DiffVpfcVacuse, R2H;;;149F
		MOV32   	@_f32INVDiffVpfcVacUse,R3H
;;-------------------------------1/VacUse Cal---------------------------------------
		DIVF32  	R3H,R0H,R1H   ;;line207:R0=1; R3=1/R1(VacNoFilter)
		NOP
;;---------------------------PFCduty=(Vo-Vac)/Vo=1-Vin/Vo   Cal---------------------
		MOV32		R2H, @_f32DiffVpfcVacUse;;;R2=Vpfc-VacUse
		I16TOF32	R1H, @_i16VpfcGap
		MOV32		R0H, @_f32InvVpfcVal;;;R0=1/Vpfc
		SUBF32		R1H, R2H, R1H ;;R1=Vpfc-VacUse-20V
		MPYF32		R2H, R2H, R0H ;R2=(Vpfc-Vac)/Vpfc
		MOV32   	@_f32INVVacUse,R3H  ;1/Vac_NoFiltered
		MPYF32		R3H, R1H, R0H ;R3=(Vpfc-Vac-20V)/Vpfc

		MOV32		R1H, @_f32PFCDuty
		MPYF32		R2H, R2H, #0.4  ;New Value
		MPYF32		R1H, R1H, #0.6  ;Old Value
		MAXF32		R3H, #0.0       ;DutyLimit下限0.0
		ADDF32		R2H, R2H, R1H	;Duty=Duty(n-1)*0.4+Duty(n)*0.6

		MOV32		R0H, @_f32PfcDutyLimit ;DutyLimit(n-1)
		MPYF32		R3H, R3H, #0.4  ;New Value DutyLimit(n)*0.4
		MPYF32		R0H, R0H, #0.6  ;Old Value DutyLimit(n-1)*0.6
		MINF32		R3H, #0.998     ;DutyLimit Upper Limit
		ADDF32		R3H, R3H, R0H	;DutyLimit=DL(n-1)*0.4+DL(n)*0.6

		MAXF32     	R2H, #0.001 ;下限;;1-300*1.414/445
		MINF32     	R2H, #0.998   ;上限
		MOV32		@_f32PFCDuty, R2H;;;(Vo-Vac)/Vo
		MOV32		@_f32PfcDutyLimit, R3H ;pfc limit for HVAC
;----------------------------------------------------------------------------------------------------
;	Phaseshift control: PhaseShift=(Ts/2 - Tph1)* PFCduty=(Tph2-Ts/2)* PFCduty
;----------------------------------------------------------------------------------------------------
		MOVL 		XAR7,#_ECap2Regs + _CAP1
		UI16TOF32 	R0H,*			;R0=Ts

		MOVL    	XAR7, #_IsrVars+_u16VacPolarity
		CMP     	*, #0
		BF      	PHASE_CAPTURE_NEG,EQ

		MOVL 		XAR7,#_ECap1Regs + _CAP1 ;ECAP1=Ts-Tph1
		UI16TOF32	R3H,*                    ;;POS cycle
		BF      	PHASE_ERR_CAL,UNC
PHASE_CAPTURE_NEG:
		MOVL 		XAR7,#_ECap4Regs + _CAP1 ;ECAP4=Ts-Tph1
		UI16TOF32	R3H,*					 ;NEG  cycle

PHASE_ERR_CAL:
		MOVL        XAR7,#_IsrVars + _i16PfcPhaseError		;;ty test
		MPYF32     	R0H, #0.5, R0H  ;R0=Ts/2
		F32TOI16R   R2H, R3H		;R2=(INT16)R3H			;;ty test
		MOV32		R1H, @_f32PFCDuty; R1=Duty
		SUBF32     	R0H, R0H, R3H   ;R0=Ts/2-[(Ts-CAP1)]=Err
		MOV16		*,R2H 			;i16PfcPhaseError=Ts-CAP1  ;;ty test
		MPYF32     	R0H, R0H, R1H  ;;R0 = Err*Duty1
;==============================================================================;
;PERIOD_ERR_CAL:
;		MOVL 		XAR7,#_ECap2Regs + _CAP1
;		UI16TOF32 	R1H,*  ;R1=Ts(n)
;		MOVL		XAR7,#_IsrVars + _u16PfcPwmTsCTR
;		UI16TOF32 	R2H,*  ;R2=Ts(n-1)
;		F32TOUI16	R3H, R1H ;R3=Ts(n)
;		NOP
;		MOV16		*,R3H    ;_u16PfcPwmTsCTR=Ts(n)
;		SUBF32		R3H, R1H, R2H  ;R3=Ts(n)-Ts(n-1)
;		MOV32		R1H, @_f32PFCDuty; R1=Duty
;		MPYF32		R3H, R3H, R1H    ;R3= Ts_Err*Duty
;		MOVL		XAR7,#_IsrVars + _u16TonShiftLimit
;		ADDF32		R0H, R3H, R0H	 ;R0= Ts_Err*Duty+Ps_Err*Duty=Phaseshift
;============================================================================;

;============================================================================;
		MOVL 		XAR7,#_ECap2Regs + _CAP1
		UI16TOF32 	R1H,*
		NOP
		MPYF32     	R1H, #0.0007, R1H;(Ts/2 - Tph1)*Duty1*TS/(TS_70K)=R0H*TS*0.0007;70k/100M =0.0007 ;R0H=(Ts/2-Tph1)*Duty1
		NOP
		MAXF32     	R1H, #0.25;;下限
		MINF32     	R1H, #1.0;;上限
		NOP
		MPYF32      R0H, R0H, R1H;R0=R0*TS*0.0007=R0*R1
;============================================================================;
		MOVL		XAR7,#_IsrVars + _u16TonShiftLimit;His changes are related to the Ac Vol
		UI16TOF32 	R2H,*  ;R2=UpperLimit
		ABSF32		R3H, R0H ;;R3=|R0|=|(Tph2-Ts/2)* PFCduty*TS/(TS_70K)|
		MPYF32		R1H, R2H, #0.5   ;;R1=Lowerlimit=Upper/2
		CMPF32		R3H, R2H;;100cnt=0.1us
		MOVST0 		ZF, NF ; Copy ZF and NF to ST0 Z and N bits
		BF			FastInterleaving, GT;;;=1, HighVolt input;If the limit is exceeded, quickly
;;;;;slow intealeaving Method
		NEGF32		R3H, R1H, UNC;;R3=-R1H
		MINF32		R0H, R1H     ; limit by +u16TonShiftLimit*0.5
		MAXF32		R0H, R3H     ; limit by -u16TonShiftLimit*0.5
FastInterleaving:
		NEGF32		R1H, R2H, UNC ;;R1=-R2;
		MINF32     	R0H, R2H;;上限 ; limit by +u16TonShiftLimit
		MAXF32     	R0H, R1H;;下限 ; limit by -u16TonShiftLimit
		MOV32		@_f32PwmTonshift, R0H

;---------------------------------------------------------------------------------------------------------
;;;	Irv Cal:IrvTrCLA=(TrIpfc-Ioffset+Icompen)>>10;;;IrvTrv=1.6A
;---------------------------------------------------------------------------------------------------------
		I16TOF32	R1H, @_i16IQ10TrIpfcCLA ;; 默认等于512 ;todo init it
		I16TOF32	R0H, @_i16IQ10TrIpfcOffsetCLA;;Pos:0/Neg:256
		NOP
		SUBF32		R1H, R1H, R0H;;;TrIpfc-Ioffset
		MOV32		R0H, @_f32TrIpfcCompenCLA
		ADDF32		R1H, R1H, R0H;;;Irv=TrIpfc-Ioffset+Icompen
		NOP
    	MOVI32		R3H, #0x3A800000;;1/(2^10)=0.00097656=0x3A800000
		MPYF32		R1H, R1H, R3H;;Irv>>10
		I16TOF32   	R0H, @_i16IQ10TrvIpfcCLA;todo cal.c for what?
		MOV32		@_f32IrvTrCLA, R1H;;R1=IrvCLA=(TrIpfc-Ioffset+Icompen)>>10
		MPYF32		R0H, R0H, R3H;;Irv>>10, R3=1/(2^10)3A800000
		MOV32      	R2H, @_f32INVDiffVpfcVacUse;;;R2=1/(Vpfc-VacUse)
		MOV32		@_f32IrvTrvCLA, R0H;;;

;---------------------------------------------------------------------------------------------------------
;;;	Tr Cal:Tr=L*Irv/(Vpfc-Vac)
;---------------------------------------------------------------------------------------------------------
		I16TOF32	R3H, @_i16LpfcCLA ;;;R3=L单位uH  ;todo init it ?
		MOV32		R1H, @_f32IrvTrCLA;;R1=IrvCLA=(TrIpfc-Ioffset+Icompen)>>10
		MPYF32     	R2H, R2H, R3H;;;;R2=L/(Vpfc-Vac)
		MOVL		XAR7,#_IsrVars + _i16PFCPRDMINCLA
    	MPYF32     	R1H, R2H, R1H ;;R1=L*IrvCLA/(Vpfc-Vac), Unit: us
    	I16TOF32	R3H, *XAR7 ;;10000 ; 10kHz ;todo why it is 10Khz？
    	MPYF32		R2H, #100.0, R1H ;;R2=L*Irv>>10/(Vpfc-Vac)*100, Unit:Clk
		I16TOF32	R1H, @_i16LpfcCLA ;R1=单位uH
		MAXF32     	R2H, #8.0 ;下限;;;Tr>8  
		MINF32     	R2H, #708.0 ;上限;;Tr<708
		SUBF32		R0H, R3H, R2H;Prd-Tr
		MOV32      	R2H, @_f32INVVacUse;;R2=1/VacUse
		MOV32		@_f32TrIrv, R0H ;;Prd-Tr后面计算TonHVpermit要用

;------------------------------------------------------------------------------------------------------
;	Trv cal  Trv =L*Irv/Vac
;------------------------------------------------------------------------------------------------------
		MPYF32     	R3H, R2H, R1H   ;R3=L*1/Vac
		MOV32		R1H, @_f32IrvTrvCLA;;R1=IrvCLA=1.6A
    	MPYF32     	R1H, R3H, R1H ;;(Irv>>10)*L*1/Vac, Unit:us
    	NOP
    	MPYF32		R2H, #100.0, R1H ;;(TrvIpfc>>10)*L*1/Vac*100, Unit:Clk
    	NOP
		MAXF32     	R2H, #10.0 	  ;下限>100ns
		MINF32     	R2H, #720.0   ;上限<7.2us
		MOV32		@_f32TrvIrv, R2H ;;Trv =L*Irv/Vac=R2H
    	F32TOI16R  	R1H, R2H
    	MOVF32		R2H, #3000.0    ;;R2H=30A ;unit:30A*100cnt
;;-----------------------HVAC Current Trim---------------------------
	;	MOV32		R0H, @_f32DiffVpfcVacUse; VPFC-VAC
	;	MOVF32		R1H, #500.0    ;;R1H=5A
	;	CMPF32		R0H, #20.0   ;;VPFC-VAC<20V
	;	SWAPF		R1H, R2H, LT  ;;VPFC-VAC, R2H=5A
;;-----------------------------------------------------------------------
    	MPYF32		R1H, R2H, R3H ;;R2H*L/Vin;;L*30A(30*100(clk))/Vin=TonMax  
    	MOV32		R2H, @_f32TrvIrv
		ADDF32	    R1H, R1H, R2H;;L*Imax/Vin+Trv
		MOV32      	R3H, @_f32VpfcVal  ;;R3=Vpfc
		MOV32		@_f32OCPTonPermit, R1H

;------------------------------------------------------------------------------------------------------
;;;calculate Kts=Ts/Ton= Vo/(Vpfc-Vac);;;Ton*Koff=Toff;;Ton+Toff=Ts;;Ts/Ton=Vo/(Vo-Vin)
;------------------------------------------------------------------------------------------------------
		MOV32      	R2H, @_f32INVDiffVpfcVacUse;;R2=1/(Vpfc-VacUse)
		MPYF32     	R2H, R3H, R2H   	;R2=Vo/(Vpfc-Vac)
		MOV32		R1H, @_f32DiffVpfcVacUse;;R1=Vpfc-VacUse
		MINF32     	R2H, #30.0 ;最大值上限幅
		MOV32		@_f32TsTonRatio, R2H ;;Koff=Toff/Ton=Vac/(Vpfc-Vac)  ;for compb calc

;--------------------------------------------------------------------------------------------------------------
;;;calculate Tr_td_compen td_compen=abs((Vpfc-Vac)*td/Lpfc-1.067A)*1024
;--------------------------------------------------------------------------------------------------------------
		MOVF32		R3H, #0.6825 ;;#2.731 ;;td/L=0.2us*1024/150uH; todo check it
		MPYF32		R1H, R1H, R3H ;(Vpfc-VacUse)*td/L
		MOVF32		R2H, #266.24   ;;#1092.6  ;;1.161A*1024
		ABSF32		R1H, R1H      ;;|(Vpfc-VacUse)*td/L|
		SUBF32		R1H, R2H, R1H
		I16TOF32	R3H, @_i16TrCompenCoef;todo why use it ?
		NOP
		MPYF32		R1H, R1H, R3H  ;;Tr=Tr*(2 or 1)
		I16TOF32	R0H, @_i16AdAcVoltPhaseShift ;;R0=Vps
		MAXF32		R1H, #1.0;;;下限幅为1
		MINF32		R1H, #6500.0;;;上限幅为1000
		MOV32		@_f32TrIpfcCompenCLA, R1H
;----------------------------------------------------------------------------------------------------------------
;		calculate the phaseshift Kps=Vps/Vac*1024
;---------------------------------------------------------------------------------------------------------------
		MOV32      	R2H, @_f32INVVacUse;;;1/VacUse,真实量纲
 		MOVF32		R1H, #0.264628;;0.258216 Vac ratio
    	MPYF32     	R0H, R0H, R1H ;;Vps=Vpsad/Kac,将超前ACad电压折算成真实Vac量纲
    	MOV32      	R3H, @_f32HVTonPermitMaxPrd;;;R3=17.14kHz
		MPYF32		R2H, R2H, R0H ;;Vps/Vac
		MOV32		R1H, @_f32PfcDutyLimit;;;R1=(Vo-Vac-20V)/Vo
		MAXF32		R2H, #0.7;;;;下限幅为0.8*1024
		MINF32		R2H, #1.3;;;上限幅为1.2*1024
		MOV32		@_f32PhaseShiftCoef, R2H
;----------------------------------------------------------------------------------------------------------------
;		calculate the HighVolt Ton_Permit;;CMPA=TsMax*Duty
;---------------------------------------------------------------------------------------------------------------
		MPYF32		R3H, R1H, R3H  ;;58us*duty=TupMax=CMPA_Max
		MOV32		R2H, @_f32OCPTonPermit
		MINF32		R3H, R2H;;TsMax*duty<f32OCPTonPermit
		MOV32		@_f32TCMTonPermit, R3H

		;;------CMPA MIN CAL fhx add-2018/12/28----;;
		MOV32		R2H, @_f32TonPermitMinPrd
		MPYF32		R3H, R1H, R2H  ;6.06us*duty = CMPA_MIN 606*0.0467=28.3
		I16TOF32	R0H, @_u16LoadBurstFlagFPU;;Burst=3, normal=2
		MOV32      	@_f32TCMTonMinCMPA,R3H
;--------------------------------------------------------------------------------------------------------------
;		calculate TCM CMPA
;--------------------------------------------------------------------------------------------------------------
		MOV32		R2H, @_f32TrvIrv ;;Trv =L*Irv/Vac Trv_min=6
		MPYF32		R2H, R2H, R0H;;Burst=3*Trv, Normal=2*Trv
		MOV32		R3H, @_f32TCMTonPermit;;R3=TCMTonpermit
		MPYF32     	R2H, R2H, #0.03125 ;Q5还原=1/32
		I16TOF32	R1H, @_u16TCMTonFPU;;单位Clk
		MOV32		R0H, @_f32PhaseShiftCoef;;VacPhaseShift Kps=Vps/Vac
		MPYF32		R1H, R1H, R0H;;(Ton)*Kps
		NOP
		ADDF32		R1H, R1H, R2H;;R1=Ton*Kps+2.Trv
		MOV32		R2H, @_f32PwmTonshift
	;	MAXF32		R1H, #0.0
		MINF32		R1H, R3H;;Ton<TCMTonpermit
		MINF32		R1H, #4000.0;;_CMPAMAX

	;	MOV32 		R3H, @_f32VacAbsUse
	;	CMPF32		R3H, #35.0   ;;35V
	;	MOVST0 		ZF, NF
	;	BF			CMPA_ZERO_LIMIT_END, GT
CMPA_ZERO_LIMIT_END:
        ;;R1H=CMPA 主管Ton;;
		ADDF32		R0H, R1H, R2H ;;R0=从管Ton+Phaseshift
		MOV32		R2H, @_f32TCMTonMinCMPA ;;28.3
		MAXF32		R1H, #90.0 ;>900ns
		MAXF32		R0H, #90.0 ;>900ns
		MAXF32		R1H, R2H;;Ton>TCMTonMinCMPA
		MAXF32		R0H, R2H;;Ton>TCMTonMinCMPA

		MOVL		XAR7, #_IsrVars + _u16CapDischargeLim
		I16TOF32 	R2H, *XAR7
		MOV32		R3H, @_f32TCMTonCMPA  ;;R3H=CMPA(n-1);
		MINF32		R1H, R2H  ;;Ton<3us/50us 47nf cap
		MINF32		R0H, R2H  ;;Ton<3us/50us 47nf cap

		MOVL		XAR7, #_IsrVars + _i16CMPAStep
		I16TOF32	R2H, *XAR7
		NOP
		ADDF32		R3H, R3H, R2H

;		ADDF32		R3H, R3H, #200.0  ;;CMPA(n)=CMPA(n-1)+200clk=2us
		MOVL    	XAR7, #_IsrVars+_i16PfcDutyPermit ;; for PFC softstart
		MINF32		R1H, R3H  ;;主管Ton限幅
		MINF32		R0H, R3H  ;;从管Ton限幅
		I16TOF32 	R3H, *XAR7
		MOV32		@_f32TCMTonCMPA, R1H  ;;CMPA(n)=R1H
		MINF32		R1H, R3H  ;;主管Ton限幅
		MINF32		R0H, R3H  ;;从管Ton限幅
		F32TOUI16R  R2H, R1H  ;;R2H=CMPA(INT16) R1H=CMPA

		ADDF32		R3H, R1H, #100.0; CMPA+50CLK
	;	MOVL		XAR7, #_EPwm7Regs + _DCFOFFSET
	;	MOV16       *,R2H
		MOVL 		XAR7,#_EPwm7Regs + _CMPA
		MOV16       *,R2H

		F32TOUI16R  R2H, R3H
		MOVL		XAR7, #_EPwm7Regs + _DCFWINDOW
		MOV16       *,R2H
		F32TOUI16R  R2H, R0H  ;R0H=Ton+shift; SLAVE PHASE
		ADDF32		R3H, R0H, #100.0; CMPA+50CLK

PHASE_NO_INTERLEAVE:
		MOVL        XAR7,#_EPwm4Regs + _CMPA
		MOV16       *,R2H

		F32TOUI16R  R2H, R3H
		MOVL		XAR7, #_EPwm4Regs + _DCFWINDOW
		MOV16       *,R2H

;--------------------------------------------------------------------------------------------------------------
;		Blank window update
;--------------------------------------------------------------------------------------------------------------
	;	MOVL 		XAR7,#_ECap2Regs + _CAP1 ;_u16PfcPwmTsCTR
	;	UI16TOF32 	R0H,*			;R0=Ts
	;	NOP
	;	MPYF32		R1H, R0H, #0.1  ;Ts/10
	;	NOP
	;	F32TOUI16R	R2H, R1H
	;	MOVL		XAR7, #_EPwm7Regs + _DCFWINDOW
	;	MOV16       *,R2H
	;	MOVL		XAR7, #_EPwm4Regs + _DCFWINDOW
	;	MOV16       *,R2H
;--------------------------------------------------------------------------------------------------------------

;--------------------------------------------------------------------------------------------------------------
;		calculate CMPB/Ts
;--------------------------------------------------------------------------------------------------------------
		MOV32		R0H, @_f32TsTonRatio ;;;Kts=Ts/Ton=Vo/(Vpfc-Vac)
		MPYF32		R0H, R0H, R1H;;;R0=Kts*TCMCMPA=Ts;;;R1H=CMPA
		NOP
		MOV32		@_f32TCMTsByCal, R0H;;;TsCal=Ton*Vo/(Vo-Vin)
		MPYF32		R1H, #0.5, R0H;;0.5*Ts
		NOP
    	F32TOUI16R 	R2H, R1H	  ;;;R2=Ts/2
    	MOVL 		XAR7, #_ECap2Regs + _CAP1
    	MOV16      	@_u16TCMHalfTsByCal, R2H
		MPYF32		R1H, #1.5, R0H	;;R0=Ts, R1=1.5*Ts
		UI16TOF32 	R3H,*			;;R3=TsCAP
		MOV32		R2H, @_f32TrIrv ;;Prd-Tr
		MAXF32		R1H, R3H;;R0>TsCAP
		ADDF32		R2H, #-100.0, R2H;;Prd-Tr-100
		MOV32		R3H,@_f32TrIrv  ;;R3=TBPHS
		MINF32		R1H, R2H;;R1<Tr
    	F32TOUI16R 	R2H, R1H
    	MOVL        XAR7, #_EPwm4Regs + _CMPB
		MOV16       *,R2H
		MOVL        XAR7, #_EPwm7Regs + _CMPB
		MOV16       *,R2H
;***************************TBPHS Update****************************************
		F32TOUI16R 	R2H, R3H
		MOVL        XAR7, #_EPwm4Regs + _TBPHS
		MOV16       *,R2H
		MOVL        XAR7, #_EPwm7Regs + _TBPHS
		MOV16       *,R2H

;***************************PFC MOS HVAC duty Limit****************************


;**************************Sub-function Modules********************************
;*Description: 进行PWM中断分拍，分为2拍，一拍执行PFC功能代码，另外一拍执行DCDC功能代码。
;*opration time：
;******************************************************************************
	;	MOVW    DP,#_IsrVars+_DCDC1_DP
	;	INC   	@_u16SwitchFlag
	;	CMP    	@_u16SwitchFlag,#1
	;	MOVB	@_u16SwitchFlag,#0,GT

		SETC 	SXM
;*********************************140kHz code end******************************

;**************************Sub-function Modules********************************
;*Description: DCDCD和PFC分拍判断
;       		为0则执行PFC及16分拍程序
;				为1则执行DCDC相关代码
;*opration time：--- clk
;******************************************************************************
	;	MOVL    XAR7, #_IsrVars+_u16SwitchFlag
	;	CMP     *, #1
		;BF      DCDC_FuncStart,EQ

;**************************Sub-function Modules********************************
;*Description: VAC采样
;			   1）VAC采用2路AD采样，进行差分相减后得到真实Vac采样值
;			   2）判断Vac极性，用来做AC频率等使用。
;*opration time：clk
;******************************************************************************
PFC_FuncStart:
;**************************Sub-function Modules********************************
;*Description: VAC极性判断
;			  通过外部引脚识别AC输入极性
;*opration time：--- 22clk
;******************************************************************************
		MOVW    DP,#_IsrVars+_PFC1_DP
		MOVL    XAR7,#_GpioDataRegs + _GPBDAT_H;;GPIO56-Pri_L_VacPolarity
        TBIT    *, #8 ;
        MOVB    @_u16VacPolarity, #0 , TC   ;NEG
        MOVB    @_u16VacPolarity, #1 , NTC  ;POS

TON_CAL:

;**************************Sub-function Modules********************************
;*Description: VAC极性判断
; Ton limit
;Tonmin = 2*L/abs(Vac_nu), Vac <10, Vac =10  最小duty限制,
;                                防止无法实现峰值电流ZVS，CLA中计算 =2Trv
;Tonmax = 2*L*(Pomax/2*K)/Vrms/Vrms  ,Vrmslimit 80V   分拍兄苯酉拗?Pvout
;Ton> tonmin
;Ton< tonmax
;Trv= Lpfc*1/abs(Vac_nu)  有除法，在CLA中扑?

;Ton_c = 2*Pvout*Lpfc/Vrms/Vrms + TLC
; Pvout*10*Q25/Vrms/Vrms * Lpfc 放大5倍

;*opration time：--- xxclk
;******************************************************************************
        MOV 	T,#5	;TonTemp/5*100=TonTemp*12 原12，由于溢出把12拆解为4*3
		MPYU 	ACC,T,@_u16VpfcOnePhasePower;;10*2^25/80/80=52429  已经Q16格式啦
		MOV		T,@AL;;;5000*12=60000  =Q16
		MPYU 	ACC,T,@_lVacRmsSqrInv;;10*2^25/80/80=52429  已经Q16格式啦;;Q16*Q15=Q31
	    MOVL    XT,ACC;;;Load 32bit,ACC已经是Q31了,注意，ACC的值一定不能超过32768*65536
    	IMPYL	P, XT, @_u32Lpfc;;Lpfc=150 (Q8)  Lowerbits multiply
		QMPYL	ACC, XT, @_u32Lpfc;;;Q32*Q8      Upperbits multiply
        ASR64 	ACC:P,#7   			;原>>9，由于将12拆解为4*3，故移动了2位。
        ;;10*Q25>>10>>16=10>>1=5,到此为止还应该*2/10才是最终us为单位的Ton
        MOVH    @_u16IpfcTonTemp,P
		MOV		AL,@_u16IpfcTonTemp
		MOV     T,@AL
		MPY     ACC,T,@_i16TonKpower;;;;67B

		MOVL   	XAR7,#_IsrVars + _i16AdVpfcProtect
    	CMP     *, #_PFCVolt460V
		MOVB     @_u16VpfcOverFlag,#1,GEQ
		CMP     *, #_PFCVolt445V
		MOVB     @_u16VpfcOverFlag,#0,LT


	;	MOVL 	XAR7,#_AdccResultRegs+_PFCVSAMP
    ;   CMP     *, #_PFCVolt445V
	;	MOVB     @_u16VpfcOverFlag,#1,GEQ
	;	CMP     *, #_PFCVolt430V
	;	MOVB     @_u16VpfcOverFlag,#0,LT

		;CMP	@_u16VpfcOverFlag,#1
		;BF      NO_VPFC_OVP,NEQ
		;ASR	AL, #1 ;;Vpfc>480V-->Ton>>2
		;MOVB    ACC,#0 ;Pvout<0;;;Vpfc过压
		;MOVL 	@_lVpfcPiOut,ACC  ;如果Vpiout<0,则将其给0

NO_VPFC_OVP:
		MOV		@PH,#_CMPAMAX;;;5000clk=50us
		MIN		AL,@PH	;;;Ton<=5000cnt=50us
		MOV		@PH,#0
		MAX     AL,@PH

		MOVL  	XAR7,#_IsrVars + _u16TCMTonFPU
		MOV    	*,AL


;**************************Sub-function Modules********************************
;*Description: burst模式
;	Ligh Load burst mode
;	重载-->开驱动; 轻载+过压10V(连续10次)-->关驱动;轻载+欠压10V(连续10次)-->开驱动
;*opration time：--- 46clk
;******************************************************************************

		MOVL 	XAR7,#_AdccResultRegs+_PFCVSAMP
		MOV 	AL,*
		SUB     AL,@_i16VpfcSet;;Vpfc-Vset

        CMP     @_u16VpfcOnePhasePower ,#150	;#200	;old is 1000  500
        BF      OUT_BURST,GT
        CMP     @_u16VpfcOnePhasePower ,#100	;#150    ;old is 950   450
		MOVB	@_i16BurstAct10msDly,#0,GT
		BF      END_OF_BURST_JUDGE,GT
		INC		@_i16BurstAct10msDly
		;Pset<Pmin需要延时10ms才能确定进扰动,避免恢复发波Pset没起来就误进扰动了
		CMP		@_i16BurstAct10msDly,#700
		BF		END_OF_BURST_JUDGE,LT
        MOV     @_i16LLoadBurstFlag , #1;;进入过burstmode的标志位
		MOV		@_i16BurstAct10msDly,#700
        BF      END_OF_BURST_JUDGE,UNC

OUT_BURST:
        MOV     @_i16LLoadBurstFlag, #0		;;退出burstMode标志位
        ;=0, Start Drive,recover PWM,电压环开始计算
        MOV     @_i16BurstOffDrvFlag, #0
		MOV		@_i16BurstAct10msDly,#0


END_OF_BURST_JUDGE:
        CMP     @_i16LLoadBurstFlag, #1
        BF      END_OF_BURST_STATE_JUDGE,NEQ
		CMP     AL,#_PFCVolt7V
		BF      NO_OVER_MAX,LT
        INC     @_i16LLoadBurstInTime;;Vpfc > Vset+7.5V
		CMP     @_i16LLoadBurstInTime, #10
		BF      END_OVER_SETTING,LT
		MOV     @_i16BurstOffDrvFlag, #1		;stop drive PWM,电压环停止计算
		MOV 	@_u16VpfcOnePhasePower,#0		;由于Vpfc Loop是16分拍，需要在这里清Pset
		MOV		@_i16FirstPwmDrvFlag,#1			;开驱动第一次要进行预制错相
		MOV     @_i16LLoadBurstInTime, #30
        BF      END_OVER_SETTING,UNC
NO_OVER_MAX:
        MOV     @_i16LLoadBurstInTime, #0
END_OVER_SETTING:
        CMP     AL,#_N_PFCVolt7V     ;8.26*7.5V=60,之前是10V
        BF      NO_UNDER_MIN,GT
        INC     @_i16LLoadBurstOutTime;;;Vpfc < Vset-7V
		CMP     @_i16LLoadBurstOutTime, #10
		BF      END_OF_BURST_STATE_JUDGE,LT
		MOV     @_i16BurstOffDrvFlag, #0		; =0, recover drive signal
		MOV     @_i16LLoadBurstFlag , #0		;退出burst，恢复发波，电压环开始计算
		MOV     @_i16LLoadBurstOutTime, #30
        BF      END_OF_BURST_STATE_JUDGE,UNC
NO_UNDER_MIN:
        MOV     @_i16LLoadBurstOutTime, #0

END_OF_BURST_STATE_JUDGE:

		MOVL  	XAR7,#_IsrVars + _u16LoadBurstFlagFPU;;//1515H
		CMP		@_i16LLoadBurstFlag,#1
		MOVB    *,#73,EQ  ;Q5[2.3]
		CMP		@_i16LLoadBurstFlag,#1
		MOVB    *,#64,NEQ ;Q5

NO_BURST_END:

;**************************Sub-function Modules********************************
;*Description:GPIO34 GanFault ocp
;
;*opration time：--- 46clk
;******************************************************************************
		MOVL 	XAR7,#_EPwm4Regs + _TZFLG
		TBIT 	*, #2 ;;;TZFLG.OST
		MOVB    @_i16PfcFaultFlag, #1 , TC ;;;TC-->High Voltage-->OCP-->Stop AQ
		MOVB    @_i16PfcFaultFlag, #0 , NTC;;;NTC-->Low Voltage-->Normal-->Start
		MOVB	@_i16FirstPwmDrvFlag,#1,TC		;开驱动第一次要进行预制错相

		MOVL 	XAR7,#_EPwm7Regs + _TZFLG
		TBIT 	*, #2 ;;;TZFLG.OST
		MOVB    AL, #1 , TC ;;;TC-->High Voltage-->OCP-->Stop AQ
		MOVB    AL, #0 , NTC;;;NTC-->Low Voltage-->Normal-->Start
		MOVB	AH, #1 , TC		;开驱动第一次要进行预制错相

		OR		AH, @_u16VpfcOverFlag  ;;OVP预制错相，但是不是过流  =1 ov
		OR		@_i16PfcFaultFlag,AL
		OR		@_i16FirstPwmDrvFlag,AH

;;;;;;;;;;;;;;Burst Sum up;;;;;;;;;;;;;;;;;;;
		MOV		AL, @_i16BurstOffDrvFlag
		OR		AL, @_i16PfcFaultFlag
		OR		AL, @_u16VpfcOverFlag  ;OVP disable PWM
		MOV		@_i16BurstSumFlag, AL;;=1, stop PWM; =0, start PWM;

;**************************Sub-function Modules********************************
;*Description:Vac sample Phase Shift
;
;*opration time：--- 22clk
;******************************************************************************
;		MOV    AH,@_i16AdAcVoltTrue;;;Vac
;       MOVW   DP,#_IsrVars + _PFC_DP ;;;第一次换页,末尾要换过来啊

;		MOV    AL,@_u16AdAcVoltcount
;		ADD    AL,#1
;		CMP    AL,#47
;		MOVB   AL,#0,GT
;		MOV    @_u16AdAcVoltcount,AL
;		ADD    AL,#_IsrVars + _i16AdAcVoltDelay0
;		MOVZ   AR7,AL
;		MOV    *,AH 	;这里有一条流水线竞争+3

;       MOV    AH,@_u16AcVoltPhaseshift;;;60A
;		SUB    AL,AH
;		CMP    AL,#_IsrVars + _i16AdAcVoltDelay0
;		BF     VacPhaseShift , GEQ
;		ADD    AL, #48
;VacPhaseShift:
;		MOVZ	AR7,AL
;		MOV		AH,* 	;这里有一条流水线竞争+3
;		MOVL  	XAR7,#_IsrVars + _i16AdAcVoltPhaseShift
;		MOV    	*, AH
;       MOVW   	DP,#_IsrVars + _PFC1_DP  ;末尾更换过来

;**************************Sub-function Modules********************************
;*Description:Vac switch work mode- POS-CMP
;
;*opration time：--- 92clk
;******************************************************************************

;------------------------AC Asymmetry Current Patch----------------------------;
;		MOV		AL, @_u16VacPolarityTmp
;		SUB		AL, @_u16VacPolarity
;		CMP		AL, #0
;		BF		AC_CROSS_18V, NEQ

;		MOV		AL, @_i16AdVacThresholdTmp;;AL=V(N-1)
;		MOV		AH, #125;;30V=30V+3V
;		CMP		@_i16AdAcVoltTrue, #189 ;;VAC>50V?
;		MOV		@_i16AdVacThreshold, AH, GT ;;>50V, Threshold=36V
;		MOV		@_i16AdVacThreshold, AL, LEQ  ;;<=50V, Threshold=Threshold(n-1)
;		BF		AC_CROSS_END, UNC

;AC_CROSS_18V:
;		MOV		@_i16AdVacThreshold, #57 ;;30V=(30-15V)*3.77889
;AC_CROSS_END:
;		MOV		T, @_i16AdVacThreshold
;		MOV		@_i16AdVacThresholdTmp, T  ;;Threshold(n-1)=Threshold(n)
;		MOV		T, @_u16VacPolarity
;		MOV		@_u16VacPolarityTmp, T	;; _u16VacPolarity(n-1)=_u16VacPolarity(n)

;------------------------AC Asymmetry Current Patch End------------------------;


        EALLOW ;;;;;Register Write Begin

		CMP    @_u16VacPolarity,#1;;=1,Pos;;=0,Neg
		BF     NEG_CMP,NEQ

POS_CMP:  ;;;_u16VacPolarity=1
		;ZCD direction change as polarity reversed
		MOVL    XAR7,#_EPwm4Regs + _TZDCSEL  ;todo check it for master and slave
		MOV      *,#0x0102 ;;#0x0102
		MOVL    XAR7,#_EPwm7Regs + _TZDCSEL
		MOV      *,#0x0102 ;;#0x0102

		;用于去校正正负半周的Tr时间来保持相同的负电流
		MOV		AL, @_i16AdIpfcTrue
		MOVL  	XAR7,#_IsrVars + _i16IQ10TrIpfcOffsetCLA
		MOV    	* , AL

		;低频开关30V以下也不导通，以免极性有误，造成交流侧直通
		MOV		AL, @_i16AdAcVoltNoFilter
		CMP		AL, @_i16AdVacThreshold;;;;30V
		MOVB    @_u16VacZeroFlag , #1, LT;;=1,Freeze PFCvoltLoop
        BF      VAC_LOW_AND_PWM_DISABLE_POS,LT
		CMP     @_u16PwmEnableFlag,#1
		BF		VAC_LOW_AND_PWM_DISABLE_POS,NEQ

		MOVL 	XAR7, #_IsrVars + _u16VacTCMEnable ;;todo check it switch14? what todo ?why
		MOV     AL,*
		CMP		AL,#1
		BF		VAC_LOW_AND_PWM_DISABLE_POS,NEQ

OVER_VAC_AD_15V_POS:
		MOV     @_u16VacZeroFlag , #0;;=0, Vac>15V
;		MOV     AL,@_u16PfcSinglePhaseMode;;;步Z;;如果AX!=0，则Z=0; 如果AX=0,騔=1
        INC     @_u16AdVacPosDelayTime

		CMP     @_u16AdVacPosDelayTime, #3;//14 ;f=70KHz so 14~~200us
		BF      END_OF_SETTING_POS,LT;;;延时低于三次直接跳出至POS/NEG CMP结束

		MOV		AL, @_u16DelayTimeCnt;; todo switch14 what and how?
		MOVL	XAR7, #_IsrVars + _i16CMPAStep
		CMP 	AL, @_u16AdVacPosDelayTime
		MOVB	*,#200,LT

		;Vac>30V,且延迟了200us后进入这里打开工频管
;		MOV     @_u16SlaveOffFlag,AL,EQ ;;;只有第3步才会执行此步信息;;66F
        MOVL    XAR7,#_GpioDataRegs+_GPASET  ;;;L,N方向EPWM8A TURN ON  ;todo check it 
        OR      *,#B14_MSK ;GPIO14-EPWM8A-DRV5-下管开
	    MOVL    XAR7,#_GpioDataRegs+_GPACLEAR
        OR      *,#B15_MSK ;GPIO15-EPWM8b-DRV6-上管关

;		CMP		@_u16SlaveOffFlag,#1;;;66F
;		MOVB    @_i16TonKpower,#2,EQ;;67B
;		MOVB    @_i16TonKpower,#1,NEQ

		CMP     @_u16AdVacPosDelayTime , #5;//21 ; f=70KHz so 21~~300us
		BF      END_OF_SETTING_POS,LT;;如果DelayTime<5则直接跳去

		;add for phase shift;;;;DelayTime>=21后每次都会进入到这里来的
        CMP     @_u16AdVacPosDelayTime , #5;//21
        BF      END_OF_SETTING_POS_2,NEQ

		;仅延时第五步进行预置错相（PWM1.TBCTR远离过零点情况下执行）
		MOVL	XAR7,#_EPwm4Regs + _TBCTR; todo why  SAFE？
		CMP		*, #_CMPSAFE; 6000? how？what？
		BF		POS_DANGER_TBCTR,GT
		MOVL	XAR7,#_EPwm7Regs + _TBCTR
		CMP		*, #_CMPSAFE
		BF		CTRINIT_START_POS,LT
POS_DANGER_TBCTR:
		MOV     @_u16AdVacPosDelayTime , #4
		BF      END_OF_SETTING_POS,UNC
CTRINIT_START_POS:

		MOVL  	XAR7,#_IsrVars + _u16TCMHalfTsByCal;;
		MOV		AL,*
		ADD     AL,#_PWM_TBCTR_INTI;;;5000

		MOV		@PH,#_CMPBMAX;;;
		MIN		AL,@PH;AL最大值不能超过CMPBMAX

       	MOVL	XAR7,#_EPwm7Regs + _TBCTR
        MOV     * , AL;;;5000+Ton/2

       	MOVL	XAR7,#_EPwm4Regs + _TBCTR
        MOV     * , #_PWM_TBCTR_INTI;;;5000

END_OF_SETTING_POS_2:
		;add for burst mode
        CMP     @_i16BurstSumFlag, #1;;;=1表示进入burst Mode
		BF      NO_DRIVE_OUT_POS,EQ;;;;如果是BurstMode直接跳过去把AQ拉死
		MOV     @_u16PwmFirstOffCnt,#12 ;ganfault后延时约140us关同步

		;关驱动以后第一次发波前要进行预制错相，回到第4步
		CMP		@_i16FirstPwmDrvFlag,#1
		BF		BURST_RECOVERY_PWM_POS,NEQ
		MOV     @_u16AdVacPosDelayTime , #4
		MOV		@_i16FirstPwmDrvFlag,#0		;清除发波标志位
		BF      PEAK_POL_CHANGE_END2,UNC

BURST_RECOVERY_PWM_POS:
        ;使能PWM驱动
        CMP		@_u16AdVacPosDelayTime, #6
        BF		EMCCAP_DISCHAR_POS, LEQ
        MOV		@_u16CapDischargeLim, #5000
        BF		EMCCAP_DISCHAR_POS_END, UNC
EMCCAP_DISCHAR_POS:
		MOV		@_u16CapDischargeLim, #200
EMCCAP_DISCHAR_POS_END:

		MOVL 	XAR7,#_EPwm4Regs + _AQCTLA
		MOV     *, #0x0221;;;0x0021
		MOVL 	XAR7,#_EPwm4Regs + _AQCTLB
        MOV     *, #0x0221;;;;

;  		CMP		@_u16SlaveOffFlag,#1;;66F
;		BF      SLAVE_NO_DRIVE_POS,EQ

		MOVL 	XAR7,#_EPwm7Regs + _AQCTLA
		MOV     *, #0x0221;0x0021
		MOVL 	XAR7,#_EPwm7Regs + _AQCTLB
        MOV     *, #0x0221;;;;
		CMP		@_u16AdVacPosDelayTime, #5
		BF		END_OF_SETTING_POS,LEQ
		
		MOVL 	XAR7,#_EPwm4Regs+_TBCTL
		OR 		*,#0x0004
		MOVL 	XAR7,#_EPwm7Regs+_TBCTL
		OR 		*,#0x0004;;;打开TB sync function
;SLAVE_NO_DRIVE_POS:
		;CMP     @_u16AdVacPosDelayTime , #100;//21 ; f=70KHz so 21~~300us
        ;MOVB    @_u16AdVacPosDelayTime,#100,GT   ;AC down

END_OF_SETTING_POS:
		BF      PEAK_POL_CHANGE_END2,UNC

;------------------------------------------------------------------------------
;PWM OFF ACTION
;关驱动分为三种操作
;  1)Vac过零点，或者warn.c关PWM，需要关工频管，同步功能，及PWM，计数清零
;  2)burst关PWM，不关工频,计数不清零，但是关PWM,同步功能
;  3)ganfault关PWM,策略为主管马上关，续流管强制拉高一拍且延时到自动过零点才关，计数不清零
;------------------------------------------------------------------------------
VAC_LOW_AND_PWM_DISABLE_POS:
		;;;;;;;EPWM8A,8B/GPIO15,14 turn off,4 EPWM FORCE LOW
        MOV     @_u16AdVacPosDelayTime , #0
        MOV		@_u16CapDischargeLim, #200

        CMP		@_u16VacZeroFlag , #1
        BF		LF_MOS_OFF_POS, NEQ
        ;; if Vac<30V then
        MOVL	XAR7, #_IsrVars + _i16CMPAStep
        MOV		*, #500
        MOV     @_u16DelayTimeCnt , #8

LF_MOS_OFF_POS:
        MOVL    XAR7,#_GpioDataRegs+_GPACLEAR
        OR      *,#B15_MSK
        OR      *,#B14_MSK

NO_DRIVE_OUT_POS:;关驱动时同步功能都不使能

		CMP 	@_i16PfcFaultFlag,#1
		BF      NORMAL_OFF_PWM_POS,UNC ;;NEQ  ;;不要续流管拉高的操作

		DEC     @_u16PwmFirstOffCnt
		MOVL 	XAR7,#_EPwm4Regs + _AQCTLA
		MOV     *, #0x0001;;CTR=0  EPWMB=0
		MOVL 	XAR7,#_EPwm4Regs + _AQCTLB
        MOV     *, 	 #0x0222;;主管CTR=0, CAU, CBU EPWMA=1
		MOVL 	XAR7,#_EPwm7Regs + _AQCTLA
		MOV     *, #0x0001;;CTR=0  EPWMB=0
		MOVL 	XAR7,#_EPwm7Regs + _AQCTLB
        MOV     *, 	 #0x0222;;主管CTR=0, CAU, CBU EPWMA=1

		CMP     @_u16PwmFirstOffCnt,#11
		BF      OFF_SYNC_POS,LT
		MOVL 	XAR7,#_EPwm4Regs + _AQSFRC
        MOV     *,#0xF6;0x24
		MOVL 	XAR7,#_EPwm7Regs + _AQSFRC
        MOV     *,#0xF6;PWMxA-续流管强制拉高一拍
		BF      PEAK_POL_CHANGE_END2,UNC

		;ganfault后延时约140us关同步-new add
OFF_SYNC_POS:
		CMP     @_u16PwmFirstOffCnt,#1
		BF      PEAK_POL_CHANGE_END2,GT

		MOVL 	XAR7,#_EPwm4Regs+_TBCTL
		AND 	*,#0xFFFB ;;;AH=Ton
		MOVL 	XAR7,#_EPwm7Regs+_TBCTL
		AND 	*,#0xFFFB ;;;AH=Ton
		MOV     @_u16PwmFirstOffCnt,#1
		BF      PEAK_POL_CHANGE_END2,UNC

NORMAL_OFF_PWM_POS:
		MOVL 	XAR7,#_EPwm4Regs+_TBCTL
		AND 	*,#0xFFFB ;;;don't load TBCTR from TBPHS
		MOVL 	XAR7,#_EPwm7Regs+_TBCTL
		AND 	*,#0xFFFB ;;;AH=Ton

		MOVL 	XAR7,#_EPwm4Regs + _AQCTLA
		MOV     *, #0x0111;;force EPWMB output low
		MOVL 	XAR7,#_EPwm4Regs + _AQCTLB
        MOV     *, 	 #0x0222;;CAU,CBU EPWMA=high
		MOVL 	XAR7,#_EPwm7Regs + _AQCTLA
		MOV     *, #0x0111;force EPWMB output low;
		MOVL 	XAR7,#_EPwm7Regs + _AQCTLB
        MOV     *, 	 #0x0222;;CAU,CBU EPWMA=high

		MOVL 	XAR7,#_EPwm4Regs + _AQSFRC
        MOV     *,#0xF5 ;CLEAR
		MOVL 	XAR7,#_EPwm7Regs + _AQSFRC
        MOV     *,#0xF5

		BF      PEAK_POL_CHANGE_END2,UNC

;**************************Sub-function Modules********************************
;*Description:switch work mode- NEG-CMP
;
;*opration time：--- 92clk
;******************************************************************************

NEG_CMP:  ;;;_u16VacPolarity=0

		MOVL    XAR7,#_EPwm4Regs + _TZDCSEL
		MOV      *,#0x0101;;#0x0101
		MOVL    XAR7,#_EPwm7Regs + _TZDCSEL
		MOV      *,#0x0101;;#0x0101

		;用于去校正正负半周的Tr时间来保持相同的负电流
		MOV		AL, @_i16AdIpfcTrue2
		MOVL  	XAR7,#_IsrVars + _i16IQ10TrIpfcOffsetCLA
		MOV    	* , AL

		;低频开关15V以下也不导通，以免极性有误，造成交流侧直通
		MOV		AL, @_i16AdAcVoltNoFilter
		CMP		AL, @_i16AdVacThreshold
		MOVB    @_u16VacZeroFlag , #1, LT;;=1,Freeze PFCvoltLoop
        BF      VAC_LOW_AND_PWM_DISABLE_NEG, LT

		CMP     @_u16PwmEnableFlag,#1
		BF		VAC_LOW_AND_PWM_DISABLE_NEG,NEQ

		MOVL 	XAR7, #_IsrVars + _u16VacTCMEnable
		MOV     AL,*
		CMP		AL,#1
		BF		VAC_LOW_AND_PWM_DISABLE_NEG,NEQ

OVER_VAC_AD_15V_NEG:
		MOV     @_u16VacZeroFlag , #0
;		MOV     AL,@_u16PfcSinglePhaseMode;;该步会根据AL的结果来修改Z
        INC     @_u16AdVacNegDelayTime;;;676
		CMP     @_u16AdVacNegDelayTime , #3;;//14;f=70KHz so 14~~200us;;Vac的滤波作用的
		BF      END_OF_SETTING_NEG,LT;;;END_OF_SETTING_NEG啥也不干直接跳出去

		MOV		AL, @_u16DelayTimeCnt
		MOVL	XAR7, #_IsrVars + _i16CMPAStep
		CMP 	AL, @_u16AdVacNegDelayTime
		MOVB	*, #200, LT

;		MOV     @_u16SlaveOffFlag,AL,EQ
      	MOVL    XAR7,#_GpioDataRegs+_GPASET  ;;;N,L方向EPWM5 TURN ON
        OR      *,#B15_MSK
		MOVL    XAR7,#_GpioDataRegs+_GPACLEAR
        OR      *,#B14_MSK

;		CMP		@_u16SlaveOffFlag,#1
;		MOVB    @_i16TonKpower,#2,EQ
;		MOVB    @_i16TonKpower,#1,NEQ

		CMP     @_u16AdVacNegDelayTime , #5;;//21   ; f=70KHz so 21~~300us
		BF      END_OF_SETTING_NEG,LT;;END_OF_SETTING_NEG啥也不干直接跳出去

        CMP     @_u16AdVacNegDelayTime , #5;;//21
        BF      END_OF_SETTING_NEG_2,NEQ;;;END_OF_SETTING_NEG_2此步对AQCTL发波配置

       	;仅延时第五步进行预置错相（PWM1.TBCTR远离过零点情况下执行）
      	MOVL	XAR7,#_EPwm4Regs + _TBCTR
		CMP		*, #_CMPSAFE
		BF		NEG_DANGER_TBCTR,GT
		MOVL	XAR7,#_EPwm7Regs + _TBCTR
		CMP		*, #_CMPSAFE
		BF		CTRINIT_START_NEG,LT
NEG_DANGER_TBCTR:
		MOV     @_u16AdVacNegDelayTime , #4
		BF      END_OF_SETTING_NEG,UNC
CTRINIT_START_NEG:

		MOVL  	XAR7,#_IsrVars + _u16TCMHalfTsByCal;;
		MOV		AL,*
		ADD     AL,#_PWM_TBCTR_INTI;;;1810

		MOV		@PH,#_CMPBMAX;;;3000-200
		MIN		AL,@PH;;;;最大值限幅,AL最大值不能超过CMPBMAX

       	MOVL	XAR7,#_EPwm7Regs + _TBCTR
        MOV     * , AL;;;1810+Ts/2

       	MOVL	XAR7,#_EPwm4Regs + _TBCTR
        MOV     * , #_PWM_TBCTR_INTI;;;1810

END_OF_SETTING_NEG_2:
        CMP     @_i16BurstSumFlag, #1 ;;;=1,stop PWM, into burst
		BF      NO_DRIVE_OUT_NEG,EQ
		MOV     @_u16PwmFirstOffCnt,#12

		;关驱动以后第一次发波前要进行预制错相，回到第4步
		CMP		@_i16FirstPwmDrvFlag,#1
		BF		BURST_RECOVERY_PWM_NEG,NEQ
		MOV     @_u16AdVacNegDelayTime , #4
		MOV		@_i16FirstPwmDrvFlag,#0		;清除发波标志位
		BF      PEAK_POL_CHANGE_END2,UNC

BURST_RECOVERY_PWM_NEG:
		;使能PWM4驱动
        CMP		@_u16AdVacNegDelayTime, #6
        BF		EMCCAP_DISCHAR_NEG, LEQ
        MOV		@_u16CapDischargeLim, #5000
        BF		EMCCAP_DISCHAR_NEG_END, UNC
EMCCAP_DISCHAR_NEG:
		MOV		@_u16CapDischargeLim, #200
EMCCAP_DISCHAR_NEG_END:

		MOVL 	XAR7,#_EPwm4Regs + _AQCTLA
		MOV     *, #0x0112  ;0x0212;
		MOVL 	XAR7,#_EPwm4Regs + _AQCTLB
        MOV     *, #0x0112  ;0x0212;0x0021
;		CMP		@_u16SlaveOffFlag,#1;;;该标志位只有在第3步被刷新一次
;		BF      SLAVE_NO_DRIVE_NEG,EQ
		;使能PWM7驱动
		MOVL 	XAR7,#_EPwm7Regs + _AQCTLA
		MOV     *, #0x0112
		MOVL 	XAR7,#_EPwm7Regs + _AQCTLB
        MOV     *, #0x0112

		CMP		@_u16AdVacNegDelayTime, #5
		BF		END_OF_SETTING_NEG,LEQ

		MOVL 	XAR7,#_EPwm4Regs+_TBCTL
		OR 		*,#0x0004
		MOVL 	XAR7,#_EPwm7Regs+_TBCTL
		OR 		*,#0x0004

;SLAVE_NO_DRIVE_NEG:
		;CMP     @_u16AdVacNegDelayTime , #100;//21 ; f=70KHz so 21~~300us
        ;MOVB    @_u16AdVacNegDelayTime,#100,GT;;;AC down

END_OF_SETTING_NEG:
        BF      PEAK_POL_CHANGE_END2,UNC

;------------------------------------------------------------------------------
;PWM OFF ACTION
;关驱动分为三种操作
;  1)Vac过零点，或者warn.c关PWM，需要关工频管，同步功能，及PWM，计数清零
;  2)burst关PWM，不关工频,计数不清零，但是关PWM,同步功能
;  3)ganfault关PWM,策略为主管马上关，续流管强制拉高一拍且延时到自动过零点才关，计数不清零
;------------------------------------------------------------------------------
VAC_LOW_AND_PWM_DISABLE_NEG:
        MOV     @_u16AdVacNegDelayTime , #0
        MOV		@_u16CapDischargeLim, #200

        CMP		@_u16VacZeroFlag , #1
        BF		LF_MOS_OFF_NEG, NEQ
        ;; if Vac<30V then
        MOVL	XAR7, #_IsrVars + _i16CMPAStep
        MOV		*, #500
        MOV     @_u16DelayTimeCnt , #8

LF_MOS_OFF_NEG:
        MOVL    XAR7,#_GpioDataRegs+_GPACLEAR
		OR      *,#B14_MSK
        OR      *,#B15_MSK

NO_DRIVE_OUT_NEG:;关驱动时同步功能都不使能

		CMP 	@_i16PfcFaultFlag,#1
		BF      NORMAL_OFF_PWM_NEG,UNC;;NEQ   ;;不要续流管拉高的操作

		DEC     @_u16PwmFirstOffCnt
		MOVL 	XAR7,#_EPwm4Regs + _AQCTLA
		MOV     *, #0x0111;;0x0001
		MOVL 	XAR7,#_EPwm4Regs + _AQCTLB
        MOV     *,   #0x0002;;0x0222
		MOVL 	XAR7,#_EPwm7Regs + _AQCTLA
		MOV     *, #0x0111;;0x0001
		MOVL 	XAR7,#_EPwm7Regs + _AQCTLB
        MOV     *,   #0x0002;;0x0222

		CMP     @_u16PwmFirstOffCnt,#11
		BF      OFF_SYNC_NEG,LT

		MOVL 	XAR7,#_EPwm4Regs + _AQSFRC
        MOV     *, #0xED;0x24
		MOVL 	XAR7,#_EPwm7Regs + _AQSFRC
        MOV     *, #0xED;0x24;PWMxB-续流管强制拉高一拍
		BF      PEAK_POL_CHANGE_END2,UNC

OFF_SYNC_NEG:
		CMP     @_u16PwmFirstOffCnt,#1
		BF      PEAK_POL_CHANGE_END2,GT
		MOVL 	XAR7,#_EPwm4Regs+_TBCTL
		AND 	*,#0xFFFB ;;;AH=Ton
		MOVL 	XAR7,#_EPwm7Regs+_TBCTL
		AND 	*,#0xFFFB ;;;AH=Ton
		MOV     @_u16PwmFirstOffCnt,#1
		BF    	PEAK_POL_CHANGE_END2,UNC

NORMAL_OFF_PWM_NEG:
		MOVL 	XAR7,#_EPwm4Regs+_TBCTL
		AND 	*,#0xFFFB ;;;AH=Ton
		MOVL 	XAR7,#_EPwm7Regs+_TBCTL
		AND 	*,#0xFFFB ;;;AH=Ton

		MOVL 	XAR7,#_EPwm4Regs + _AQCTLA
		MOV     *, #0x0111;;0x0001
		MOVL 	XAR7,#_EPwm4Regs + _AQCTLB
        MOV     *,   #0x0222;;0x0222
		MOVL 	XAR7,#_EPwm7Regs + _AQCTLA
		MOV     *, #0x0111;;0x0001
		MOVL 	XAR7,#_EPwm7Regs + _AQCTLB
        MOV     *,   #0x0222;;0x0222

		MOVL 	XAR7,#_EPwm4Regs + _AQSFRC
        MOV     *, #0xF5;0x24
		MOVL 	XAR7,#_EPwm7Regs + _AQSFRC
        MOV     *, #0xF5;0x24

PEAK_POL_CHANGE_END2:
		;计数变量限幅放到最外面
		CMP     @_u16AdVacPosDelayTime , #100;//21 ; f=70KHz so 21~~300us
        MOVB    @_u16AdVacPosDelayTime,#100,GT   ;AC down
		CMP     @_u16AdVacNegDelayTime , #100;//21 ; f=70KHz so 21~~300us
        MOVB    @_u16AdVacNegDelayTime,#100,GT;;;AC down

        EDIS ;;;Register Write End

;**************************Sub-function Modules********************************
;*Description:quick loop judge
;
;*opration time：--- 3clk
;******************************************************************************

;		CMP		@_i16PfcQuickLoop,#5;;<15, fast Loop; =15, slow Loop
;		MOVB    @_i16PFCPRDFLG,#0,GT

;**************************Sub-function Modules********************************
;*Description:Fast protection：PFCOVP/PFCOCP protect
;
;*opration time：--- 31clk
;******************************************************************************

		MOVL    XAR7,#_AdcbResultRegs + _PFCOVP
	;	MOVL   	XAR7,#_IsrVars + _i16AdVpfcProtect
        CMP     *,#_PFCVolt500V
		BF      FAST_OVP_PROTECT , GT

	;	MOVL    XAR7,#_IsrVars + _i16AdAcVoltTrue
	;	CMP     *,#_VAC_AD_460V
	;	BF      FAST_PROTECT , GT

		MOVL	XAR7, #_EPwm4Regs + _TZOSTFLG
		TBIT	*,#2
		BF		FAST_PROTECT,TC  ;;High OST3 shoot_ocp triggered, shutdown

		MOVL	XAR7, #_EPwm7Regs + _TZOSTFLG
		TBIT	*,#2
		BF		FAST_PROTECT,TC  ;;High OST3 shoot_ocp triggered, shutdown


    ;------------GPIO12 PFC FL ocp  防雷OCP----------------------
	;	MOVL 	XAR7,#_GpioDataRegs + _GPADAT;;GPIO12--- PFCOCP-L线总电流
	;	TBIT 	*, #12 ;
	;	MOVB    @_u16PriOcp, #1 , TC   ;ocp
	;	MOVB    @_u16PriOcp, #0 , NTC  ;no ocp
    ;   CMP     @_u16PriOcp, #1
		BF      END_FAST_PROCTET,UNC;;NEQ	;=0 drive normal

FAST_OVP_PROTECT:
        MOVL 	XAR7,#_IsrVars + _u16PfcQuickPFCOVPFault
		MOV     *,#0x01

;-------------------------------------------------------
FAST_PROTECT:

		;off    dcdc
        MOVL 	XAR7,#_EPwm1Regs+_AQCSFRC
		MOV 	*XAR7,#0x01
		MOVL 	XAR7,#_EPwm2Regs+_AQCSFRC
		MOV 	*XAR7,#0x01
		MOVL 	XAR7,#_EPwm3Regs+_AQCSFRC
		MOV 	*XAR7,#0x01
		MOVL 	XAR7,#_EPwm5Regs+_AQCSFRC
		MOV 	*XAR7,#0x01

		MOVL    XAR7,#_EPwm4Regs+_AQCSFRC
        MOV     *,#0x09
        MOVL    XAR7,#_EPwm7Regs+_AQCSFRC
        MOV     *,#0x09

		;main relay control ,GPIO40
    ;   MOVL    XAR7,#_GpioDataRegs+_GPBCLEAR
    ;   OR      *,#B8_MSK

END_FAST_PROCTET:
      ;  BF 			Isr_End,UNC

;**************************Sub-function Modules********************************
;*Description:DCDC 功能函数
;
;*opration time：--- clk
;******************************************************************************
DCDC_FuncStart:
		MOVW    DP,#_IsrVars+_DCDC_DP
				
;dcdc voltage sample		
		;MOVL	XAR7,#_AdcbResultRegs + _DCVOLT
		MOVL	XAR7,#_AdcaResultRegs + _DCVOLT
		MOV 	AL,*
		MOV 	@_i16AdVdcSam0,ACC<<3 ;Q15 ,The ACC register is not modified
;--------------------------------------------------------------
; dcdc short circuit protect	
;--------------------------------------------------------------
		CMP     @_i16AdVdcSam0,#_ShortCircuitVolt		
		BF 		DCDC_NoShort,GT
        MOVL 	XAR7,#_GpioDataRegs + _GPADAT
        TBIT    *,#15  ;GPIO15: _DCSHOT
		BF 		DCDC_NoShort,NTC
         		
DCDC_ShortProtect:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;因为短路时候输出电流震荡，短路时把DC电流的直流偏置设为0;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;在没有短路时把校准的直流偏置赋值;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        MOVL	XAR7, #_IsrVars + _i16ShortIdcOffset
        MOV     AL,*
        MOVL	XAR7, #_IsrVars + _i16ShortVdcOffset
        MOV     AH,*

        MOVL	XAR7,#_CputoClaVar + _i16DCCurOffsetTrue
        MOV     * , AL
	
		MOV     @_i16IdcdcSys,#0
		MOVL	XAR7,#_IsrVars + _u16RippleDisable
		MOV     *,#1
		MOVL	XAR7,#_IsrVars + _i16VdcShortCoefA ;Q16
		MOV 	T,*++

		MAX     AH,@_lVdcUse_H

		MPY     P,T,@AH
		MOVL    ACC,P
		MOV     T,#10
		ASRL    ACC,T
		ADD 	ACC,*    ;此处*为shortB,
        ;因为EPROM中默认校准值为-1，下限又为负值，所以多写一个_VdcShortCoefB，使变量_i16VdcShortCoefB默认值为-1
		ADD     ACC,#_VdcShortCoefB 
;;;限制i16CurrLimFloor短路时的上限；；；
;;;因短路时限流值=_i16CurrLimFloor，所以限制了最大电流;;;;;;
;		CMP     AL,#2500         
;		BF      CURR_LIMIT,LEQ
;		MOV     ACC,#2500
        MOV     AH,#2500
        MIN     AL,@AH

;CURR_LIMIT:
	;	MOV 	@_i16CurrLimFloor,ACC	
		MOV 	@_i16CurrLimFloor,AL
		MOVL    XAR7, #_CputoClaVar + _u16ShortFlag ;短路标志置1
		MOV     *,#1		 
		BF 		DCDC_NO_OVP,UNC  ;don't judge over shoot;;;34clk
DCDC_NoShort:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;因为短路时候输出电流震荡，短路时把DC电流的直流偏置设为0;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;在没有短路时把校准的直流偏置赋值;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
       	MOVL    XAR7,#_CputoClaVar + _u16ShortFlag;短路标志清0
		MOV     *,#0
        MOVL	XAR7,#_CputoClaVar + _i16DCCurOffset
        MOV     AL,*
        MOVL	XAR7,#_CputoClaVar + _i16DCCurOffsetTrue;;;非短路态，把IDC OFFSET恢复
        MOV     *,AL

;**************************Sub-function Modules********************************
;*Description:过冲补丁
;
;*opration time：--- clk
;******************************************************************************
		MOV 	AL,@_i16AdVdcSam0		
		SUB 	AL,@_i16VdcSet
		CMP 	AL,#_DC4V	;;;;;;;大约4V;;;;;限流突卸载HVSD补丁，暂时不改   
		BF 	    ClearOvpFlag,LEQ
		INC     @_i16VdcOVPTimes
		CMP    	@_i16VdcOVPTimes,#2
		BF 		DCDC_NO_OVP,LT
DCDC_OVP:

		MOV 	AL,@_i16VdcSet		
		CMP 	AL,@_lVdcUse_H
		BF 		ClearOvpFlag,LT
		MOV 	@_lVdcUse_H,AL
ClearOvpFlag:
		MOV     @_i16VdcOVPTimes,#0		;;;39clk
			
DCDC_NO_OVP:

;;;;;;;;;;;;;;;安规短路造成HVSD补丁;;;;;;;;;;;;;;;;;;;;;;;;;
		CMP 	@_i16AdVdcSam0,#_AvoidHVSDVolt ;#3276 3276/468=7V
		BF 		DCDC_NO_OVP_2,GT

		INC		@_i16VdcHVSDTimes
		CMP		@_i16VdcHVSDTimes,#3
		BF		DCDC_NO_OVP1,LT
		MOV		@_i16DcdcPWMTsMax,#_DcdcPWMTsLIM_80K;#428;;60M/428=140k

DCDC_NO_OVP1:
		MOVL	XAR7,#_GpioDataRegs + _GPADAT_H
		TBIT	*,#13  ;GPIO29: _DCOVP
		BF 		SAMPLE_SHORT_END,NTC      ;//TC==1-->GPIO13==1-->DCOVP

		;;现在改为置g_u16MdlStatusExtend.bit.SSHORTSD标志位
	    MOVL	XAR7,#_g_u16MdlStatusExtend
		OR 		*,#B11_MSK

	    ;安规短路的硬件HVSD，是否要两次起机
		MOVL 	XAR7,#_EPwm4Regs+_AQCSFRC
		MOV 	*XAR7,#0x01
		MOVL 	XAR7,#_EPwm5Regs+_AQCSFRC
		MOV 	*XAR7,#0x01
		MOVL 	XAR7,#_EPwm6Regs+_AQCSFRC
		MOV 	*XAR7,#0x01
		MOVL 	XAR7,#_EPwm7Regs+_AQCSFRC
		MOV 	*XAR7,#0x01
		BF		SAMPLE_SHORT_END,UNC

DCDC_NO_OVP_2: 
       MOV      @_i16VdcHVSDTimes , #0
       ADD      @_i16DcdcPWMTsMax , #5
       MOV      AL ,  #_DcdcPWMTsMaxHighLine_45K
	   MIN      AL , @_i16DcdcPWMTsMax
	   MOV      @_i16DcdcPWMTsMax , AL

;;;;Added by XB20140508                 
SAMPLE_SHORT_END:

;;;;;;;;;;;;;;;;;;;;;;HVSD ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		MOVW	DP, #_IsrVars+_DCDC2_DP

		MOVL 	XAR7,#_GpioDataRegs + _GPADAT_H   ;
		TBIT	*,#13  ;GPIO29: _DCOVP
		BF 		DC_OVP_RECOVER_JUDGE,NTC
		INC		@_u16DCDC_OvpDelayCnt
		CMP		@_u16DCDC_OvpDelayCnt, #5
		MOVB	@_u16DCDC_OvpLimitFlag, #1, GEQ
		MOVB	@_u16DCDC_OvpDelayCnt, #5, GEQ
		BF 		DC_LIMIT_CHANGE_START,UNC
DC_OVP_RECOVER_JUDGE:
		MOVL 	XAR7, #_IsrVars + _lVdcUse_H
		CMP		*, #_DC58V5
		BF 		DC_LIMIT_CHANGE_START,GEQ
		MOV		@_u16DCDC_OvpDelayCnt, #0
		MOV		@_u16DCDC_OvpLimitFlag, #0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
DC_LIMIT_CHANGE_START:
		MOVL 	XAR7, #_CputoClaVar + _i16IdcPioutPermitCLA1
		MOV		AL, *
		CMP		@_u16DCDC_OvpLimitFlag, #1
		BF 		DC_LIMIT_RAMP_DOWN,EQ
		ADD		AL, #250;;Step_Up
		BF 		DC_LIMIT_FINAL,UNC
DC_LIMIT_RAMP_DOWN:
		CMP    	AL, #250;;Step_Down
		MOVB    AL, #200, LT
		BF	    DC_LIMIT_FINAL,LT
		SUB		AL, #250
DC_LIMIT_FINAL:
		MAX		AL, @_u16DCDC_OvpLimitFloor
		MIN		AL, @_u16DCDC_OvpLimitCeiling
		MOVL 	XAR7, #_CputoClaVar + _i16IdcPioutPermitCLA1
		MOV		*, AL

		MOVW	DP, #_IsrVars+_DCDC_DP
;;;;;;;;;;;;;;;;;;;;;;; HVSD ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;**************************Sub-function Modules********************************
;*Description:环路计算
;
;*opration time：--- clk
;******************************************************************************
;;----------VdcPiOut =3200*3345>>10 << 10 = 10,704,000 *0.5%=53520=0xD110
;-----------idcPiOut =(1430-400)<<16=67,502,080 *0.5% =337,510=21904<<4=5266<<4
;------------------------------------------------------------------------------
; 	DCDC voltage filter,  Q16
;	 y(k)=((y(k-1)*k1)>>16)+Vsam(k)*k2+Vsam(k-1)*k3		
;------------------------------------------------------------------------------
;DCDC电压对消滤波，极点11.15HZ,零点245HZ，Q16定标		
		MOVL 	XT,@_lVdcUse ;XT=[loc32]
		IMPYL 	P,XT,@_lVdcFilterK1  ;P=low 32-bits
		QMPYL 	ACC,XT,@_lVdcFilterK1;ACC=high 32-bits
		ASR64 	ACC:P,#16  ;P=(32bits*32bits)>>16
		
		MOV 	T,@_i16AdVdcSam1 ;T=_iAdVdcSam1,ACC=P
		MPY 	ACC,T,@_i16VdcFilterK3
		MOVAD 	T,@_i16AdVdcSam0 ;T=_iAdVdcSam0,[loc16+1]=[loc16],ACC=P+ACC
		MPY 	P,T,@_i16VdcFilterK2
		ADDL 	ACC,P ;ACC=ACC+P		
		MOVL 	@_lVdcUse,ACC
		
;---------------------------------------------------------------------
;		change limit current into limit power, Q16	
;---------------------------------------------------------------------
;设置限流值最小值为_i16CurrLimFloor	，短路时_i16CurrLimFloor被设成SHORTA*_lVdcUse_H+SHORTB，而没有短路时_i16CurrLimFloor慢慢减成0
;短路时，限流系数i16IdcdcSys被设为0，乘以_lVdcUse_H小于_i16CurrLimFloor，所以限流值就等于_i16CurrLimFloor
;等到退出短路时i16IdcdcSys不为0，乘以_lVdcUse_H大于_i16CurrLimFloor（0），所以功率给定就大于了_i16CurrLimFloor，可以给出较大的功率给定
		MOV 	T,@_i16IdcdcSys
		MPY 	ACC,T,@_lVdcUse_H

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        MOV     PH,@_i16CurrLimFloor
		MOV     PL , #0       ;;短路时的i16CurrLimFloor应该不用太准确或稳定。
        MAXL    ACC,P
		MOVL 	@_lCurrLim,ACC
;---------------------------------------------------------------------------------------
; DCDC voltage loop PI , Q11
;;;;;;;;;;;;;;;;;;;;;;;;Skg——20210923采样电路异常debug;;;;;;;;;;;;;
;		MOVW    DP,#_IsrVars+_PFC2_DP
;        CMP     @_VdcUse_DEBUG,#1
;        BF      ChangeForce_lVdcUse_H,NEQ

; 		MOVW    DP,#_IsrVars+_DCDC_DP
;        MOV     @_lVdcUse_H,#18728
;ChangeForce_lVdcUse_H:
;        MOVW    DP,#_IsrVars+_DCDC_DP
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
		MOV 	AL,@_i16VdcSet				
		SUB 	AL,@_lVdcUse_H

		MOV		@_i16VdcErrUse1,AL
    	CMP 	@_u16ChoiseCon,#0
    	;qtest20201214
		;BF 		DCDC_OverShootEnd2,UNC

		BF 		DCDC_OverShootEnd2,NEQ
		MOVL    XAR7,#_IsrVars + _u16SoftStartEndTimer
		;20200125
		;CMP 	*,#20
		CMP 	*,#300
		BF 		DCDC_OverShootEnd2,LT
;-------------------------------------------------------------------------
; 	DCDC 动态跳变补丁 20201214
;--------------------------------------------------------------------------
		CMP 	@_i16VdcErrUse1,#-90
		BF      Qtest_Dyn1,LT
		CMP 	@_i16VdcErrUse1,#90
		BF      DCDC_OverShootEnd2,LT
Qtest_Dyn1:
		MOVL    XAR7,#_ClatoCpuVar + _u32DCDCPowerFeedbackCLA
        CMP     *,#1000;做动态判断时剔除轻载的情况
		MOVL    XAR7,#_IsrVars+_i16PFCFastLoopForce
		MOVB     *, #1,GT;设置强制PFC快环标志位
		MOVL    XAR7,#_IsrVars+_u16DCDC_Nouse0I
		MOVB     *, #0,GT;清强制快环延迟计时
      	MOVL   XAR7,#_IsrVars + _u16DCDC_Dynflag
      	MOVB    *,#1,GT;
		MOVL    XAR7,#_IsrVars+_u16DCDC_DynDelayCnt
		MOVB     *, #0,GT;清动态延迟计时
		BF 		DCDC_OverShootEnd2,UNC

;-------------------------------------------------------------------------
; 	DCDC 动态跳变补丁 25%~50~75%； 10%~90%
;--------------------------------------------------------------------------
        MOVL    XAR7,#_ClatoCpuVar + _u32DCDCPowerFeedbackCLA;;;只使用功率反馈低位，实际16变量就够了
        CMP     *,#1000;1000;;;;大约为200W输出
        BF      DCDC_OverShootLowpower,LT;duanqingan20140426

		CMP 	@_i16VdcErrUse1,#_N_DC0V064 	       
		BF      DCDC_DynTest1,LT
		CMP 	@_i16VdcErrUse1,#_DC0V064
		BF      DCDC_DynTest3,LT
DCDC_DynTest1:               ;;;;;;;;;<-30或>30进入快环    
		MOV		@_i16VdcLoopK3,#7544  ;18730 
		MOV		@_i16VdcLoopK4,#-6774 ;-18233    
DCDC_DynTest3:  ;;;;;;;;;;;;;;;;;;;;;;(-30,30)之间保持原来的环路
;------------------------------------------------------------------------
; 	欠过超调补丁
;------------------------------------------------------------------------			
		CMP 	@_i16VdcErrUse1,#_VdcdcDynamicNeg 
		BF		DCDC_NOOverShoot2,GT	
		MOV     AL,@_i16VdcErrUse1
		ASR     AL,#5 ;4	
		SUB     @_i16VdcErrUse2,AL		
		BF 		DCDC_OverShootEnd2,UNC
DCDC_NOOverShoot2:
		CMP 	@_i16VdcErrUse1,#_VdcdcDynamicPos ;>100 加载
		BF		DCDC_OverShootEnd2,LT
		MOV     AL,@_i16VdcErrUse1
		ASR     AL,#5	
		SUB     @_i16VdcErrUse2,AL		;;;200	
		BF 		DCDC_OverShootEnd2,UNC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
DCDC_OverShootLowpower:                            ;duanqingan20140426
		CMP 	@_i16VdcErrUse1,#_N_DC0V427;#-200	       
		BF      DCDC_DynTest1A,LT
		CMP 	@_i16VdcErrUse1,#_DC0V427;#200
		BF      DCDC_DynTest3A,LT
DCDC_DynTest1A:               ;;;;;;;;;<-200或>200进入快环    
		MOV		@_i16VdcLoopK3,#7544  ;18730 
		MOV		@_i16VdcLoopK4,#-6774 ;-18233    
DCDC_DynTest3A:  ;;;;;;;;;;;;;;;;;;;;;;(-200,200)之间保持原来的环路
		
		CMP 	@_i16VdcErrUse1,#_VdcdcDynamicNeglowpower;#-230;-180 
		BF		DCDC_NOOverShoot2A,GT	
		MOV     AL,@_i16VdcErrUse1
		ASR     AL,#5;5 ;4	
		SUB     @_i16VdcErrUse2,AL		
		BF 		DCDC_OverShootEnd2,UNC			
DCDC_NOOverShoot2A:
		CMP 	@_i16VdcErrUse1,#_VdcdcDynamicPoslowpower;#230;180 ;>100 加载
		BF		DCDC_OverShootEnd2,LT
		MOV     AL,@_i16VdcErrUse1
		ASR     AL,#5	
		SUB     @_i16VdcErrUse2,AL		;;;200
;------------------------------------------------------------------------
; 	欠过超调补丁
;------------------------------------------------------------------------	
					
DCDC_OverShootEnd2: 
;  single PI voltage loop		
		MOVL 	ACC,@_lVdcPiOut
		MOV 	T,@_i16VdcErrUse2  ;load low 16bits to upper half of XT
		MPY 	P,T,@_i16VdcLoopK4
		MOVAD 	T,@_i16VdcErrUse1  ;
		MPY 	P,T,@_i16VdcLoopK3
		ADDL 	ACC,P  ;ACC=ACC+P
		
;--------------------------------------------------------------------
; DCDC voltage loop output limit
;    	power_min<<12 < _lVdcPiOut < power_max<<12
;-----------------对_lVdcPiOut进行上下限幅------------------------------------
        MPY     P,@_i16PowerLimMin,#_N_Q10 ;;实际上,下限为负_i16PowerLimMin，所以乘负的Q10
        MAXL    ACC,P
		MPY     P,@_i16PowerMaxExtra,#_Q10 
        MINL    ACC,P                      ;;取上限，_i16PowerMaxExtra
		MOVL 	@_lVdcPiOut,ACC	

;--------------------------------------------------------------------
; ripple voltage loop
	
		MOVL	XAR7,#_IsrVars + _lVdcDisUse_H
		MOV		AL,*		
		SUB		AL,@_lVdcUse_H		
		MOVL 	XAR7,#_IsrVars + _i16DcdcVoltRipple0
		
		CMP 	@_u16RippleDisable,#0
		BF 		Ripple_Loop_Limit,EQ
		MOV 	*,#0
		BF 		Ripple_Loop_LimitEnd,UNC
Ripple_Loop_Limit:
		
; ripple voltage loop limit 
		MIN	    AL,@_i16VdcRippleRangePos
		MAX	    AL,@_i16VdcRippleRangeNeg  
		MOV		*,ACC<<#_VRippleKQn 	; by zzh 4-26
Ripple_Loop_LimitEnd:

; ripple voltage controller   Save 6 CLK BY zzh 4-26
;############################################################
		MOVL    ACC,@_lVdcRippleUse
		MOV		T,*++
		MPY 	P,T,@_i16VRippleK2
		ADDL 	ACC,P
		MOV		T,*--
		MPY 	P,T,@_i16VRippleK3
		MOVAD 	T,*
		MOV     T,@_lVdcRippleUse_H
		MPY     P,T,@_i16VRippleK1
		ADDL    ACC,P
		MOVL    @_lVdcRippleUse,ACC

		MOV		AL,@_lVdcRippleUse_H
		MIN	    AL,@_i16VdcRippleUsePos
		MAX	    AL,@_i16VdcRippleUseNeg  

		MOV		@_lVdcRippleUse_H,AL
		MOV 	@_u16ChoiseCon,#0  ;set to 0	

;--如果限流、限功率的分界点是48V，则48V以上@_i16PowerLimMax<@_lCurrLim_H;48V以下反过来
  		MOV 	T,#10
  		MOV 	AL,@_lCurrLim_H
  		;CMP 	AL,@_i16PowerLimMax
  		CMP 	AL,@_i16PowerLimMaxUse
  		BF  	CURR_LIMIT_JUDGE,LT

  		;MOV  	ACC,@_i16PowerLimMax << T
  		MOV  	ACC,@_i16PowerLimMaxUse << T
  		ADD  	ACC,@_lVdcRippleUse_H << T

  		CMPL  	ACC,@_lVdcPiOut
  		MOVB  	@_u16ChoiseCon,#1,LT  ; 限功率
        MINL  	ACC,@_lVdcPiOut
        BF 		PowerLim_END,UNC
CURR_LIMIT_JUDGE:
        MOV  	ACC,@_lCurrLim_H << T
        ADD  	ACC,@_lVdcRippleUse_H << T
        CMPL  	ACC,@_lVdcPiOut
        MOVB 	@_u16ChoiseCon,#2,LT  ; 限流
  		MINL  	ACC,@_lVdcPiOut
PowerLim_END:

;------------------------------------------------------------------------------
; DCDC power set		
		ASRL 	ACC,T
		MOV 	@_i16DCDCPowerSet,AL
		MOVL	XAR7,#_CputoClaVar + _i16DCDCPowerSetCLA
		MOV 	*,AL

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;**************************Sub-function Modules********************************
;*Description:16分拍调用
;
;*opration time：--- clk
;******************************************************************************
        CLRC    SXM
        MOVW    DP,#_IsrVars+_DCDC1_DP
		INC     @_u16IsrTimer
;----------------------------------------------------------------------
;   	16  cleft frequency calculate
;----------------------------------------------------------------------
	 	MOVL	XAR7,#SwitchTable
		MOV 	ACC,@_u16IsrTimer<<#1
		AND 	ACC,#0x000F<<#1
		MOVW    DP,#_IsrVars+_PFC_DP
		ADDL 	@XAR7,ACC ;XAR7 = SwitchTable + index*2

		MOVL 	XAR7,*
		LB		*XAR7
SwitchReturn:     ;  4CLK return
		SETC 	SXM


;**************************Sub-function Modules********************************
;*Description:根据输出电压计算开关管最低工作频率
;
;*opration time：--- clk
;******************************************************************************
        MOVW   DP,#_IsrVars+_DCDC2_DP
		CMP    @_u16VolSamCaliSuccess,#1
		;BF     Isr_End,UNC    ;;;;暂时屏蔽最低频率计算
		BF     Vol_Calibrate,EQ
;;;;;;;;;;;;;;;未校准显示电压;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
        MOVL   XAR7, #_CputoClaVar +_i16DcdcPWMTsMaxCLA
		MOV    AH, @_u16DcdcPWMTsLIM_95K
		MOV    * ,AH  ;;若没校准则DC输出电压，则DC最低频率为95K

		BF     Isr_End,UNC
Vol_Calibrate:
;;;;如果初始化后发现显示电压已经校准了，就按照输出电压计算DC的最低工作频率;;;;;;;;;;;;;;;;;;
        MOVL   XAR7,#_IsrVars + _lVdcUse_H
        MOV 	T,*
        MPY 	ACC, T, @_iq16DcVolCalDcFreqCoefA
        ADD    AH,@_i16DcVolCalDcFreqCoefB
		MIN	    AH,@_u16DcdcPWMTsMaxHighLine_80K
		MAX	    AH,@_u16DcdcPWMTsMaxLowLine_103k
;;;Add by XB20131012
        MOVL    XAR7,#_IsrVars+_i16DcdcPWMTsMax

		MIN		AH,*
;;;Add by XB20131012

		MOVL   XAR7, #_CputoClaVar +_i16DcdcPWMTsMaxCLA
		MOV    * ,AH

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;扫频开始;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;		MOVW    DP,#_IsrVars+ _DCDC2_DP;其实本来就在第二段了～
;		MOV     AL, @_u16OpenLoopCtrl
;		MOVL    XAR7,#_CputoClaVar + _u16OpenLoopCtrlCLA
;        MOV     *,AL;开环标志位送到CLA
;
;		CMP 	@_u16OpenLoopCtrl,#1;;为1 执行扫频，0，不执行扫频
;		BF      NoOpenLoop ,NEQ
;		MOVL 	ACC, @_lOpenPIoutStart
;		CMP 	@_u16OpenLoopLR,#0  ;;扫频方向，为0，降低周期值增加频率
;		BF      OpenLoopDown,EQ
;OpenLoopUp:
;	   	CMPL    ACC, @_lOpenPRDUP;;扫频上限值
;	   	BF      OpenLoop1 ,GEQ
;	   	ADDL 	ACC,@_lOpenLoopStep;;扫频的步进
;		BF		OpenLoopEnd,UNC
;OpenLoop1:
;		MOV		@_u16OpenLoopLR,#0
;OpenLoopDown:
;	    CMPL    ACC, @_lOpenPRDDUN;;扫频下限值
;	   	BF      OpenLoop2 ,LEQ
;		SUBL	ACC,@_lOpenLoopStep
;		BF		OpenLoopEnd,UNC
;OpenLoop2:
;		MOV		@_u16OpenLoopLR,#1
;OpenLoopEnd:
;		MOVL 	@_lOpenPIoutStart, ACC
;        MOVW    DP,#_CputoClaVar
;		MOVL   @_lOpenPIoutStartCLA,ACC;;当前频率值给到Cla
;NoOpenLoop:
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;扫频结束;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Isr_End:

;------------------------------------------FPU Code End-------------------------
        MOVL	XAR7,#_EPwm8Regs+ _ETCLR  ;
        OR      *,#0x0001 

		MOV32     R3H, *--SP, UNCF
		MOV32     R2H, *--SP, UNCF
		MOV32     R1H, *--SP, UNCF
		MOV32     R0H, *--SP, UNCF
        MOVL      XT,*--SP
        MOVL      XAR7,*--SP
        NASP				
        IRET           
;-----------------------------------------------------
; _Epwm8_Isr end
;-----------------------------------------------------
;************************************************************						
;   switch0 : Vpfc quick/slow loop change, 8/18 CLK
;************************************************************
Switch0:   	   
		MOVW    DP,#_IsrVars+_PFC5_DP
	   ;q0426test
;		MOVL     XAR7,#_IsrVars+_i16PFCFastLoopForce
;		CMP      *, #1
;		BF       FORCE_QUICK_LOOP,EQ
	    ;q0426testend
        CMP       @_u16LoopTest, #1;;_u16LoopTest   .set	0234H
        BF        NormalVpfcLoopEnd,EQ;;是1，跳过
        CMP       @_u16LoopTest, #0
        BF        QtestNomalLoop,EQ
        MOVW    DP,#_IsrVars+_PFC1_DP

		MOVL     XAR7,#_IsrVars+_i16PFCFastLoopForce
		CMP      *, #1
		BF       VpfcFastLoop,EQ
;FORCE_QUICK_LOOP:
;        MOVW    DP,#_IsrVars+_PFC1_DP
;        MOV     @_i16PFCPRDFLG,#1
;        MOV 	@_u16VpfcPara,#2
;		MOV 	@_u16VpfcQn,#11
;        BF      NormalVpfcLoopEnd,UNC

QtestNomalLoop:
; Vpfc voltage loop change;;;if err>25V ,then change quick/slow loop factor
        MOVW    DP,#_IsrVars+_PFC1_DP
        CMP     @_i16LowTempPFCloopFlag , #0;低温下的环路
        BF      NomalPFCloopchange,EQ;;Temp>-24deg, =0
		MOV     @_i16PFCPRDFLG,#0
        MOV 	@_u16VpfcPara,#0;;Temp<-26deg
		MOV 	@_u16VpfcQn,#11;;13;;Temp<-26deg
		BF      NormalVpfcLoopEnd,UNC
NomalPFCloopchange: ;不在低温下，直接跳到这里
		MOV 	AL,#0
		MOVL 	XAR7,#_AdccResultRegs+_PFCVSAMP
		MOV 	AH,*
		SUB 	AH,@_i16VpfcSet;;;Vpfc-Vset
		MOVL	P,ACC; USE TO TMP VARRABILE
;;;;;;;;;;;;;;;;;;;;;根据不同输入波形，此值不同;;;;;;;;;;;;;;;;;;;;;;;
     	MOVL    XAR7,#_IsrVars + _i16PfcOvershootVal;;25V
        CMP 	AH, *;;Vpfc-Vset
		BF 		VpfcFastLoop,GT;;Vpfc>Vset+25V
;;;;;;;;;;;;;;;进入这里表示没有过压,但是要判断是否在开机过程中;;;;;;;;;;;;;;;;;;;;
		CMP 	@_u16PfcOpened,#1 ;PfcOpen=1表示PFC已正常工作了
		BF 		VpfcLoopChange,NEQ;;PfcOpen=0表示Pfc在起机过程中
		;CMP 	AH,#_N_PFCVolt25V;;PFC Ok
		CMP   	AH,#_N_PFCVolt25V;;PFC Ok
		BF 		VpfcLoopChange,GT;;Vpfc-Vset>-25-->Vset-Vpfc<25V无欠压退出

VpfcFastLoop:;;Vpfc>Vset+25V or Vpfc<Vset-25V

		MOV 	AL,#1
		CMP 	@_u16VpfcPara,#0
		BF 		VpfcLoopChange,NEQ
		MOV 	@_u16VpfcPara,#1
		BF 		VpfcLoopSlow2Quick,UNC

VpfcLoopChange:
		CMP 	@_u16VpfcPara,#0;;;Stable Slow Loop
		BF 		VpfcLoopSlow,EQ;;=0,跳出switch
		CMP 	@_u16VpfcPara,#2;;;Slow to fast
		BF		VpfcLoopQuick,EQ;;;=2,稳定快环
		CMP 	@_u16VpfcPara,#3;;;Begin Fast switch Slow Loop
		BF		NormalVpfcLoopEnd,NEQ
		CMP 	AL,#1
		BF  	VpfcLoopQuick,EQ
;;;;--------------Fast loop switch to slow loop-------------
VpfcLoopQuick2Slow:;;;when Para=3
;		CMP 	@_i16PfcQuickLoop,#15;;;656H
;		BF      NormalVpfcLoopEnd,LT
		MOV 	@_u16VpfcPara,#0;;SlowLoop Start

		MOVL    XAR7,#_IsrVars+_i16VpfcPiTmp
		MOV		*,#400

        MOVL    XAR7,#_IsrVars + _i16PfcSlowLoopQnSet;;;11
  	    MOV     T,*
        MOV   	@_u16VpfcQn, T
		MOVL 	ACC,@_lVpfcPiOut
        MOVL    XAR7,#_IsrVars + _i16VpfcPioutShiftSet;;1
        MOV     T,*
        LSL     ACC,T
		MOVL 	@_lVpfcPiOut,ACC
VpfcLoopSlow:;;;;when Para=0
		MOV     @_i16PFCPRDFLG,#0;;;=0,with 8Hz;; =1, w/o 8Hz;;;651H
		BF 		NormalVpfcLoopEnd,UNC

;;;;--------------Slow loop switch to Fast loop-------------
VpfcLoopSlow2Quick:;;;when Para=1
		MOV 	@_u16VpfcPara,#2 
        MOVL    XAR7,#_IsrVars + _i16PfcQuickLoopQnSet;;;10,放大8倍
  	    MOV     T,*
        MOV   	@_u16VpfcQn, T
		MOVL 	ACC,@_lVpfcPiOut
        MOVL    XAR7,#_IsrVars + _i16VpfcPioutShiftSet;;;=1
        MOV     T,*
		ASRL	ACC,T;;;
		MOVL 	@_lVpfcPiOut,ACC

VpfcLoopQuick:	;;;when Para=2
;		MOV 	@_i16PfcQuickLoop,#0
		MOV     @_i16PFCPRDFLG,#1;;=1,fastloop+no30Hz;;;651H
;		MOV		@_u16PfcSinglePhaseMode,#0;;=1,singlePhase; =0,dualPhase
		MOVL     XAR7,#_IsrVars+_i16PFCFastLoopForce
		CMP      *, #1
		BF       NormalVpfcLoopEnd,EQ
		;qtest20210126 end
	    MOVL	ACC,P;;;;P=Vpfc-Vset
        CMP 	AH,#_PFCVolt5V  ;+5V*8=40
        BF      NormalVpfcLoopEnd,GT
		CMP     AH,#_N_PFCVolt5V ;-5*8=-40
		BF      NormalVpfcLoopEnd,LT      
        MOV 	@_u16VpfcPara,#3;;;|Vset-Vpfc|<5V开始进入慢环

NormalVpfcLoopEnd:	

		LB SwitchReturn		;end SW0

;************************************************************						
;   switch1 : pfc voltage filter and error cal, 17 CLK
;************************************************************
Switch1: 
		MOVW    DP,#_IsrVars+_PFC1_DP
			; Pfc voltage sample;;;8Hz
		MOVL 	XAR7,#_AdccResultRegs+_PFCVSAMP
	    MOV     T,*                      
		MOV     @_i16AdVpfc0,T
		;30Hz filter
		MOVL    ACC,@_lVpfcUse;;;这是没有右移的y(n-1)<<16
        MPY     P,T,@_i16VpfcFilterK2;;K2*X(n)
        MOVA    T,@_i16AdVpfc1;;T=Vpfc1=x(n-1), ACC=y(n-1)<<16+k2*x(n)
        MPY     P,T,@_i16VpfcFilterK2;;K2*x(n-1);;;K2=374
        MOVA    T,@_lVpfcUse_H;;T=y(n-1), ACC=y(n-1)<<16+k2*[x(n)+x(n-1)]
        MPY     P,T,@_lVpfcFilterK1;;k1*y(n-1);;k1=-748
        MOVAD   T,@_i16AdVpfc0;;T=x(n), Vpfc1=T, ACC=y(n-1)<<16+k2*[x(n)+x(n-1)]+k1*y(n-1)
        MOVL    @_lVpfcUse,ACC;;;Y(n)*Q16=(65536-748)*y(n-1)+374*[x(n)+x(n-1)]

		; err = Vset -Vpfc0  no filter
		MOV 	AL,@_i16VpfcSet;;;648
		CMP     @_i16PFCPRDFLG,#1;;;=1,fastLoop; =0,slowLoop&No30Hz
		BF 		VpfcNoFilter,EQ
		SUB 	AL,@_lVpfcUse_H;;只取H16bit
		BF 		VpfcNoFilterEnd,UNC
VpfcNoFilter:
		SUB 	AL,@_i16AdVpfc0;;;641;;Vset-Vbak
VpfcNoFilterEnd:		 
		MOV 	@_i16VpfcErr0,AL;;AL=Vset-Vbak
	
		LB SwitchReturn	;end SW1

;************************************************************						
;   switch2 :  PFC voltage loop cal, 20 CLK
;************************************************************
Switch2:   

		MOVW    DP,#_IsrVars+_PFC1_DP

		CMP		@_u16VacZeroFlag,#1;;;=1,电压环停止计算
		BF		VoltLoopEnd,EQ
		;qtest
;        CMP		@_u16HighAcVoltFlag,#1;=1,电压环停止计算
;		;BF		VoltLoopEnd,EQ
;		BF		NO_BURST_LOOP_CAL,NEQ
;		MOVB    ACC,#0
;		MOVL 	@_lVpfcPiOut,ACC			;积分清0
;		MOV 	@_u16VpfcOnePhasePower,#0	;pvout清0
;		MOVL    XAR7,#_IsrVars+_i16VpfcPiTmp
;		MOV		*,#400						;;ramp回到初始值 duty=25%
;		BF		VoltLoopEnd,UNC
		;qtest end
		;burst封波停止环路计算并把PI清0
		CMP		@_i16BurstOffDrvFlag,#1
		BF		NO_BURST_LOOP_CAL,NEQ
		MOVB    ACC,#0
		MOVL 	@_lVpfcPiOut,ACC			;积分清0
		MOV 	@_u16VpfcOnePhasePower,#0	;pvout清0
		MOVL    XAR7,#_IsrVars+_i16VpfcPiTmp
		MOV		*,#400						;;ramp回到初始值 duty=25%
		BF		VoltLoopEnd,UNC
NO_BURST_LOOP_CAL:
;;;--------------Ki*x(n)+y(n-1)-----------------
		MOVL 	ACC,@_lVpfcPiOut
		MOV		T,@_i16VpfcErr0
		MPY		P,T,@_i16VpfcLoopKi ;Q11;;Ki*Err0
		ADDL 	ACC,P;;Pvout+Ki*Err0
		MOVL 	@_lVpfcPiOut,ACC;;Piout(n)=Piout(n-1)+Ki*X(n)
;;;-----------------Kp*X(n)---------------------------
		MPY		P,T,@_i16VpfcLoopKp;;Kp*Err0;;28622
		ADDL 	ACC,P;;;ACC=Pvout+Ki*Err0+Kp*Err0
		MOV 	T,@_u16VpfcQn
;------------------------------------------------------------
; 		PFC voltage loop output limit
;------------------------------------------------------------
; PFC voltage loop output limit		
		BF 		PFC_VoltPI_Min,GT  ;ADDL标志位，ACC<0 Piout清零 >0 PI_min
		MOVB 	ACC,#0;;;Pvout<0;;;Vpfc过压
		MOVL 	@_lVpfcPiOut,ACC  ;如果Vpiout<0,则将其给0
		BF 		PFC_VoltPI_END,UNC

PFC_VoltPI_Min:;;;Pvout>0
		SUB 	ACC,@_i16VpfcPiOutMax<<T
		BF 		PFC_VoltPI_Max,LEQ
		MOVB 	ACC,#0;;;Pvout>Pmax;;Vpfc欠压
		ADD 	ACC,@_i16VpfcPiOutMax<<T;如果Vpiout>Pmax,则将其给Pmax
		MOVL 	@_lVpfcPiOut,ACC;;;
		ASRL 	ACC,T;;;;;
		BF 		PFC_VoltPI_END,UNC

PFC_VoltPI_Max:;没有超限，则Vpiout不动
		ADD 	ACC,@_i16VpfcPiOutMax<<T
		ASRL 	ACC,T		;;_u16VpfcQn,慢环位移11，快环10

PFC_VoltPI_END:
		ASR 	AL,#1  ;every loop share half power ACC/2
		CMP		@_u16VpfcPara,#2
		MOVL    XAR7,#_IsrVars+_i16VpfcPiTmp
		MOV		*,AL,NEQ			;;储存慢环PI
		CMP		@_u16VpfcPara,#2
		BF		PFC_DUTY_LIMIT,NEQ  ;;不是快环，不用滤波

RAMP_FILTER_CAL: ;进入欠压快环逐波放开
		CMP		@_i16VpfcErr0,#20
		BF		PFC_DUTY_LIMIT,LT   ;Vset-Vpfc<20cnt   非欠压状态退出
		
	;	MOVL    XAR7,#_IsrVars+_i16VdcLoopFilterK2  ;;debug查看使用 ty
	;	MOV		*,AL

		MOV		AH,@AL
		ASR		AH,#2	;1/8的比例放开Piout; Piout=Piout+AL/8 初始为Max的25%
		;ASR		AH,#3	;1/8的比例放开Piout; Piout=Piout+AL/8 初始为Max的25%
		MOVL    XAR7,#_IsrVars+_i16VpfcPiTmp
		ADD		*,AH						;Piout=Piout+AL/8

		MOV		AH,*	;AH=Piout
		MIN		AH,@AL	;Piout应该小于当前的AL=Pvout输出
		MOV		*,AH	;积分缓存
		MOV		AL,*	;PIout输出

PFC_DUTY_LIMIT:
		MOV		AH,#0
		MAX		AL,@AH
		MIN 	AL,@_i16PfcMaxPower  ;输出限幅
		MOV 	@_u16VpfcOnePhasePower,AL	;给Ton计算的最终值

VoltLoopEnd:
		
		LB SwitchReturn	; end SW2



;************************************************************						
;   switch3 : Save RAM data   //by zzh 4-28
;************************************************************
Switch3:
;     	MOVW    DP,#_IsrVars + _DCDC1_DP;DCDC1_DP	.equ	0x0040 / c000  _IsrVars
;
;		CMP        @_u16PCtrl,#6       ;enable 0xC04D 标志位  .set 004DH
;		BF           CopyRAM_END1 ,NEQ
;
;		MOVL	XAR7,#_g_u16ActionReady
;		CMP		*,#6;PFC_SWSOFT_START_INIT
;		BF		    CopyRAM_END1 ,NEQ
;
 ;       ;MOVL     XAR7, #_IsrVars+ _i16LLoadBurstFlag
 ;       ;MOV 	  AL, *
;		;CMP       AL, #0  ;不在Burst模式就使能，在就读
;        ;MOVB    @_u16RamReadEn,#1,EQ ;_u16RamReadEn .set 	0058H 地址C058H
;        ;CMP      @_u16RamReadEn,#1
;		;BF          CopyRAM_END1 ,NEQ
;
;		MOVW   DP,#_IsrVars + _PFC2_DP;_PFC2_DP	.equ	0x0140 / c000  _IsrVars，这里需要翻页
;		INC		 @_u16RamReadCnt;为免混淆，重新定义一个计数值
;		MOV		 AL,@_u16RamReadCnt;_u16RamReadCnt  .set  0152H 地址C152H
;		CMP		 AL,@_u16RamReadInterval;C155;;697H读数的间隔，要初始化
;		BF          CopyRAM_END1, LT
;		MOV		 @_u16RamReadCnt,#0
;		MOVW   DP,#_IsrVars + _DCDC1_DP;翻回原来的页
;
;		MOVZ 	AR7,@_u16PAddress  	;Address1-0x68E
;		MOV 	AL,*
;		MOV 	AH,#0x8000;
;		ADD		AH,@_u16Pointer
;		MOVZ 	AR7,AH
;		MOV 	*,AL
;
;		MOVZ 	AR7,@_u16PAddress2  	;Address1-0x68F
;		MOV 	AL,*
;		MOV 	AH,#0x80C8;
;		ADD		AH,@_u16Pointer
;		MOVZ 	AR7,AH
;		MOV 	*,AL
;
;		MOVZ 	AR7,@_u16PAddress3 	;Address2-0x690
;		MOV 	AL,*
;		MOV 	AH,#0x8190
;		ADD		AH,@_u16Pointer
;		MOVZ 	AR7,AH
;		MOV 	*,AL

		;MOVZ 	AR7,@_u16PAddress4 	;Address2-0x690
		;MOV 	AL,*
		;MOV 	AH,#0x0258
		;ADD		AH,@_u16Pointer
	    ;MOVZ 	AR7,AH
		;MOV 	*,AL

;		INC 	@_u16Pointer
;		;MOV     @_u16RamReadEn,#0;如果间歇，下次再读
;		CMP 	@_u16Pointer,#200  	;max读数的最值，要看读多少数
;		BF 		CopyRAM_END1,LT
;		MOV 	@_u16Pointer,#0;指针还零
;		MOV     @_u16PCtrl,#8;结束读内存
;		;MOV     @_u16RamReadEn,#0

CopyRAM_END1:

		MOVW    DP,#_IsrVars + _PFC4_DP
		MOVL  	XAR7,#_IsrVars + _i16AdAcVoltTrue;;|Vac|瞬时值,单位V
		MOV		AL,*
		CMP 	AL, @_u16AdAcMaxPowLim
		BF 		AcMax_POW,LT
		MOV 	@_u16AdAcMaxPowLim, AL
AcMax_POW:
		LB SwitchReturn	;end SW3

;************************************************************						
;   switch4 : Vac filter 1 cal, 27CLK
;************************************************************
Switch4: 
;--------------------------------------------------------------
;Caculate Vacrms,Vac^2   u -->t -->y
;first 1Hz filter
;--------------------------------------------------------------
		MOVW    DP,#_IsrVars+_PFC1_DP
		MOV 	T,@_i16AdAcVoltTrue
		MPY 	ACC,T,@_i16AdAcVoltTrue
        MOVL    XT,ACC

		;Input :@_iAdAcSam0 @_lAcSamVoltSquare,
		;Output:@_lAcVoltSquareFilt,@_lAcVoltSquareFilt_LW
        MOVW   DP,#_IsrVars+_PFC2_DP
        ADDL   ACC,@_lAcSamVoltSquare 
        MOVL   @_lAcSamVoltSquare,XT       ;Last squ updata
        SUBL   ACC,@_lAcVoltSquareFilt
        SUBL   ACC,@_lAcVoltSquareFilt
        MOVL   XT,ACC       
        IMPYL  P,XT,@_lVacRmsSquFltK
        QMPYL  ACC,XT,@_lVacRmsSquFltK
        ADDUL  P,@_lAcVoltSquareFilt_LW 
        MOVL   @_lAcVoltSquareFilt_LW,P 
        ADDCL  ACC, @_lAcVoltSquareFilt
        MOVL   @_lAcVoltSquareFilt,ACC
       
		LB SwitchReturn	;end SW4
	
		
;************************************************************						
;   switch5 : Vac filter 1 cal, 23CLK
;************************************************************
Switch5:  

		MOVL 	XAR7,#_IsrVars + _u16MinTimer
		INC 	*
;--------------------------------------------------------------
;Y2 = K1*Y1+K2*X2+K3*X1 ;;;K2=K3  and K1+K2+K3=1 -> K1=1-2K2
;so  Y2 = K2*（ X2+X1-2Y1） +Y1
;--------------------------------------------------------------
;--------------------------------------------------------------
;Caculate Vacrms,Vac^2   u -->t -->y
;second 1Hz filter   							----14clk
;--------------------------------------------------------------
		MOVW    DP,#_IsrVars+_PFC2_DP
		MOVL    XT,@_lAcVoltSquareFilt
		MOVL    ACC,XT
		ADDL    ACC,@_lAcVoltSquareFilt1
		MOVL    @_lAcVoltSquareFilt1,XT
		SUBL    ACC,@_lAcVoltSquareRms
		SUBL    ACC,@_lAcVoltSquareRms
		MOVL    XT,ACC
		
		IMPYL   P,XT,@_lVacRmsSquFltK
		QMPYL   ACC,XT,@_lVacRmsSquFltK
		ADDUL   P,@_lAcVoltSquareRms_LW
		MOVL    @_lAcVoltSquareRms_LW,P 
		ADDCL   ACC,@_lAcVoltSquareRms
		MOVL    @_lAcVoltSquareRms,ACC

		LB SwitchReturn	;end SW5

;************************************************************						
;   switch6 : Ipfc change PI
;************************************************************
Switch6:  
 	    ;Vin Polarity check for frequency cal ----9clk
		MOVW    DP,#_IsrVars + _PFC1_DP
		CMP     @_i16AdAcVoltTrue,#290
		BF 		VIN_POLARITY_CHK ,LT
		MOV     AL,@_u16VacPolarity
		MOVL    XAR7, #_IsrVars+_u16PolarityFlgHz
        MOV     * ,AL
VIN_POLARITY_CHK:

    	MOVW    DP,#_IsrVars + _PFC4_DP
		;VIN Polarity
		MOV 	AL, @_u16PolarityFlgHz
		CMP		AL, @_u16PolarityUse
		MOV     @_u16PolarityUse,AL
		BF 		VIN_POLARITY ,EQ

		;借用G3 mini 这里滤波逻辑有些问题，后面需要优化
		;计算16个周期的点数
		INC     @_u16VinTimer2
		CMP     @_u16VinTimer2,#16
		BF 		VIN_POLARITY ,LT
		MOV     @_u16VinTimer2,#0

		INC     @_u16VinTimer1
		MOV     AL,@_u16VinF
		CMP     @_u16VinTimer1,#2
        BF 		VIN_POLARITY_HZ ,NEQ
		ADD     AL,@_u16VinT
		MOV     @_u16VinFrequency,AL
		MOV     @_u16VinTimer1,#0
		BF 		VIN_POLARITY_HZ_END ,NEQ
VIN_POLARITY_HZ:
        MOV     @_u16VinT,AL
		INC		@_u16VinT
VIN_POLARITY_HZ_END:
		MOV     @_u16VinF,#0
		BF 		VIN_POLARITY_END ,UNC
VIN_POLARITY:
        INC     @_u16VinF
VIN_POLARITY_END:

		LB     SwitchReturn		;end SW6
		
;************************************************************						
;   switch7 : 根据电压和负载切换参数
;************************************************************
;_u16LowVolDcLoopChgFlag  ;;根据输出电压切换DC电压环，纹波环，电流环的标志位
;_u16LowPowerFlag         ;;输出根据输出功率 分段点1 切换DC电压环，纹波环，电流环的标志位
;_u16LowPowerFlag1        ;;输出根据输出功率 分段点2 切换DC电压环，纹波环，电流环的标志位
Switch7:   
	    ;qtest Loop Test
	    MOVW    DP,#_IsrVars+_DCDC_DP

		MOVL    XAR7,#_IsrVars+_u16DCDC_Nouse01;C0A0
		CMP     *,#1
		BF      LoopSWStart,LT

		MOVL    XAR7,#_IsrVars+_u16DCDC_Nouse05;C0A4
		CMP     *,#1		            ;;维持标志位
		BF      LOOPSWITCH_END,EQ;;维持：先C0A4置1，再C0A0置1

		MOVL    XAR7,#_IsrVars+_u16DCDC_Nouse02;C0A1
		MOV     AL,*
		MOVL    XAR7,#_CputoClaVar + _u16DCCurloopCoef
		MOV     *,AL

		MOVL    XAR7,#_IsrVars+_u16DCDC_Nouse03;C0A2
		MOV     AL,*
        MOV     @_i16VdcLoopK3,AL

        MOVL    XAR7,#_IsrVars+_u16DCDC_Nouse04;C0A3
        MOV     AL,*
        MOV     @_i16VdcLoopK4,AL
		BF      LOOPSWITCH_END,UNC
LoopSWStart:
		;qtest Loop Test
		MOVW    DP,#_IsrVars+_DCDC_DP
		MOVL    XAR7,#_g_lq10MdlVolt
		;CMP	    *,#48845;47.7V
		MOVL    ACC,*
		CMPL    ACC,@_u32Vdc47v7Point

		BF	    High_Voltage,GT
		;CMP	    *,#48384;47.25V _u32Vdc47v25Point
		CMPL    ACC,@_u32Vdc47v25Point
		BF	    Low_Voltage,LT
		MOVL    XAR7,#_IsrVars + _u16LowVolDcLoopChgFlag
		CMP     *,#0
		BF	    High_Voltage,EQ

		;MOVL    XAR7,#_g_lq10MdlVolt
		;CMP	    *,#48845;47.7V
		;BF	    High_Voltage,GT
		;CMP	    *,#48384;47.25V
		;BF	    Low_Voltage,LT
		;MOVL    XAR7,#_IsrVars + _u16LowVolDcLoopChgFlag
		;CMP     *,#0
		;BF	    High_Voltage,EQ

Low_Voltage:
   
      MOVL XAR7,#_IsrVars + _u16LowVolDcLoopChgFlag
      MOV     *,#1
      MOVL    XAR7,#_ClatoCpuVar + _u32DCDCPowerFeedbackCLA;;;只使用功率反馈低位，实际16变量就够了
      CMP     *,#1800;;;;大约为550W输出  系数2206/1024=2.154
      BF      High_PowerA,GT
      CMP     *,#1400;1600;;;;大约为500W输出  duanqingan 20140327
      BF      Low_PowerA,LT
      MOVL    XAR7,#_IsrVars + _u16LowPowerFlag
      CMP     *,#1
      BF      Low_PowerA,EQ

High_PowerA:
      MOVL    XAR7,#_IsrVars + _u16LowPowerFlag
      MOV     *,#0
      MOVL    XAR7,#_CputoClaVar + _u16DCCurloopCoef
      MOV     *,#500;600;QGF
      ;MOV     @_i16VdcLoopK3,#10369;
      ;MOV     @_i16VdcLoopK4,#-10003;
      MOV     @_i16VdcLoopK3,#10745;
      MOV     @_i16VdcLoopK4,#-10459;

      MOV     @_i16VRippleK1,#-51
      MOV     @_i16VRippleK2,#20243;
      MOV     @_i16VRippleK3,#-19529;


      BF      LOOPSWITCH_END,UNC

Low_PowerA:
      MOVL XAR7,#_IsrVars + _u16LowPowerFlag
      MOV     *,#1
      ;  MOV    @_i16VRippleK1,#-51
      ;  MOV    @_i16VRippleK2,#12872;6436
      ;  MOV    @_i16VRippleK3,#-12830 ;-6415
      CMP     @_i16LowTempLoopflag,#0
      BF      Low_TempLowVout, NEQ

      MOVL    XAR7,#_ClatoCpuVar + _u32DCDCPowerFeedbackCLA;;;只使用功率反馈低位，实际16变量就够了
      CMP     *,#1300;;;;大约为450w输出
      BF      Low_Power1A,GT
      CMP     *,#800;;;;大约为400w输出  duanqingan 20140327
      BF      Low_Power2A,LT
      MOVL  XAR7,#_IsrVars + _u16LowPowerFlag1
      CMP     *,#1
      BF    Low_Power2A,EQ
      BF    Low_Power1A,UNC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;低温，低压，300W到600W负载范围内切换电压环参数
Low_TempLowVout:                       

      MOVL   XAR7,#_ClatoCpuVar + _u32DCDCPowerFeedbackCLA;;;只使用功率反馈低位，实际16变量就够了
      CMP     *,#780;;; ;大约为300W输出
      BF     Low_TempLVoutMLoad,GT
      MOVL   XAR7,#_ClatoCpuVar + _u32DCDCPowerFeedbackCLA;;;只使用功率反馈低位，实际16变量就够了
      CMP     *,#700;;;
      BF     Low_TempLVoutLLoad,LT
      MOVL   XAR7,#_IsrVars + _u16LowTempLVoutLLoadflag
      CMP     *,#1
      BF     Low_TempLVoutLLoad,EQ
Low_TempLVoutMLoad:

      MOVL   XAR7,#_IsrVars + _u16LowTempLVoutLLoadflag
      MOV    *,#0
      MOVL   XAR7,#_CputoClaVar + _u16DCCurloopCoef
      MOV    *,#700;800;QGF

      MOV    @_i16VdcLoopK3,#10369;  ;在常温低压中间负载的基础上，增益降一半，零点由36挪到50Hz
      MOV    @_i16VdcLoopK4,#-10003;

      MOV    @_i16VRippleK1,#-51
      MOV    @_i16VRippleK2,#26027;
      MOV    @_i16VRippleK3,#-25109;

      BF   LOOPSWITCH_END,UNC

Low_TempLVoutLLoad:
    
       MOVL   XAR7,#_IsrVars + _u16LowTempLVoutLLoadflag
       MOV     *,#1

       MOVL   XAR7,#_CputoClaVar + _u16DCCurloopCoef
       MOV    *,#1000;1000;QGF

       MOV    @_i16VdcLoopK3,#10369;
       MOV    @_i16VdcLoopK4,#-10003;

       MOV    @_i16VRippleK1,#-51
       MOV    @_i16VRippleK2,#26027;
       MOV    @_i16VRippleK3,#-25109;

       BF   LOOPSWITCH_END,UNC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

Low_Power1A:    
      MOVL XAR7,#_IsrVars + _u16LowPowerFlag1
      MOV     *,#0
      MOVL   XAR7,#_CputoClaVar + _u16DCCurloopCoef
      MOV    *,#650;800;QGF

      MOV    @_i16VRippleK1,#-51
      MOV    @_i16VRippleK2,#26027;
      MOV    @_i16VRippleK3,#-25109;

      MOV    @_i16VdcLoopK3,#16590;
      MOV    @_i16VdcLoopK4,#-16005;

      BF   LOOPSWITCH_END,UNC

Low_Power2A:    
      MOVL XAR7,#_IsrVars + _u16LowPowerFlag1
      MOV     *,#1
      MOVL   XAR7,#_CputoClaVar + _u16DCCurloopCoef
      MOV    *,#1400;1200;zhangzhen
      ;MOV    *,#2000;1200;QGF
      MOV    @_i16VdcLoopK3,#8295;
      MOV    @_i16VdcLoopK4,#-8002;

      MOV    @_i16VRippleK1,#-51
      MOV    @_i16VRippleK2,#26027;
      MOV    @_i16VRippleK3,#-25109;

      BF   LOOPSWITCH_END,UNC

High_Voltage:
      MOVL   XAR7,#_IsrVars + _u16LowVolDcLoopChgFlag
      MOV     *,#0
      MOVL   XAR7,#_ClatoCpuVar + _u32DCDCPowerFeedbackCLA;;;只使用功率反馈低位，实际16变量就够了
      CMP     *,#1800;;;;大约为600W输出
      BF      High_PowerB,GT
      CMP     *,#1400;;;;大约为550W输出  duanqingan 20140327
      BF      Low_PowerB,LT
      MOVL  XAR7,#_IsrVars + _u16LowPowerFlag
      CMP     *,#1
      BF    Low_PowerB,EQ

High_PowerB:
      MOVL XAR7,#_IsrVars + _u16LowPowerFlag
      MOV     *,#0
      ; MOV    @_i16VdcLoopK3,#3772;2995;4166;4398;4127;3993;
      ; MOV    @_i16VdcLoopK4,#-3387;-2689;-3877;-4320;-3910;-3650;

      MOV    @_i16VRippleK1,#-51
      MOV    @_i16VRippleK2,#18390;
      MOV    @_i16VRippleK3,#-16962;
      MOVL    XAR7,#_IsrVars + _u16LowPowerFlag2
      CMP     *,#1
      BF      Medium_High_PowerB,EQ;;Flag2=1, BW*1.0;
      MOVL   XAR7,#_CputoClaVar + _u16DCCurloopCoef
      MOV    *,#1500

      MOVL    XAR7,#_IsrVars + _i16NoiseLoopflag
      CMP     *,#1

      ;qtest20201210
      ;BF      Ultar_High_PowerB1,EQ UNC
      BF      Ultar_High_PowerB,NEQ
	  MOVL 	  XAR7,#_AdccResultRegs+_PFCVSAMP
	  CMP     *,#_PFCVolt405V
	  BF      Low_Bus_Judge,LT
	  MOVL    XAR7,#_IsrVars + _u16DCDC_Nouse0G
      CMP     *,#1           ;Low_Bus_DelayFlag
	  MOVL    XAR7,#_IsrVars + _u16DCDC_Nouse0F
	  MOVB     *,#0,NEQ           ;Low_Bus_Flag
	  BF      Noise_Loop_Judge,UNC
Low_Bus_Judge:
      MOVL 	  XAR7,#_AdccResultRegs+_PFCVSAMP
	  CMP     *,#_PFCVolt395V
	  BF      Noise_Loop_Judge,GT
	  MOVL    XAR7,#_ClatoCpuVar + _u32DCDCPowerFeedbackCLA
      CMP     *,#2206;1000W
      MOVL    XAR7,#_IsrVars + _u16DCDC_Nouse0F;Low_Bus_Flag
      MOVB     *,#1,GT
      MOVL    XAR7,#_IsrVars + _u16DCDC_Nouse0G
      MOVB     *,#1,GT                    ;Low_Bus_DelayFlag
Noise_Loop_Judge:
	  MOVL    XAR7,#_IsrVars + _u16DCDC_Nouse0F;Low_Bus_Flag
      CMP     *,#1
      BF      Ultar_High_PowerB1,NEQ
   	  ;qtestend20201210

Ultar_High_PowerB:;;;Flag=0,BW*1.4
      MOVL   XAR7,#_CputoClaVar + _u16DCCurloopCoef
      MOV    *,#950;1000;QGF
      ;MOV    *,#1000;1000;QGF
      ;MOV    @_i16VdcLoopK3,#10035;
      ;MOV    @_i16VdcLoopK4,#-9011;
      ;MOV    @_i16VdcLoopK3,#8675;
      ;MOV    @_i16VdcLoopK4,#-7753;
      MOV    @_i16VdcLoopK3,#7514;
      MOV    @_i16VdcLoopK4,#-6746;


      BF     LOOPSWITCH_END,UNC
Ultar_High_PowerB1:     ;noise test zone
;lym
      MOVL    XAR7,#_IsrVars + _u16DCDC_Dynflag
      CMP     *,#1
      MOVL    XAR7,#_IsrVars + _i16NoiseLoopflag2   ;load > 68A  set 1
      MOVB    *,#1,EQ	;进入动态则保持在这一套参数
      CMP     *,#0
      BF      Ultar_High_PowerB2,EQ
      MOVL   XAR7,#_CputoClaVar + _u16DCCurloopCoef
      MOV    *,#900;
;      MOV    @_i16VdcLoopK3,#30039;21254;19526;
;      MOV    @_i16VdcLoopK4,#-27458;-20230;-18502;
      MOV    @_i16VdcLoopK3,#21456;21254;19526;
      MOV    @_i16VdcLoopK4,#-19613;-20230;-18502;
      BF     LOOPSWITCH_END,UNC
Ultar_High_PowerB2:
      MOV    @_i16VdcLoopK3,#21456;11920;14772;
      MOV    @_i16VdcLoopK4,#-19613;-10896;-12164;-13748;
      BF     LOOPSWITCH_END,UNC

Medium_High_PowerB:
      MOVL   XAR7,#_CputoClaVar + _u16DCCurloopCoef;qgf
      MOV    *,#800;2000;zhangzhen
      ;MOV    *,#1000;2000;qgf
      MOV    @_i16VdcLoopK3,#13207;
      MOV    @_i16VdcLoopK4,#-12403;
      BF   LOOPSWITCH_END,UNC

Low_PowerB:
     MOVL XAR7,#_IsrVars + _u16LowPowerFlag
     MOV     *,#1
     
;    MOV    @_i16VRippleK1,#-51
;    MOV    @_i16VRippleK2,#12872;6436
;    MOV    @_i16VRippleK3,#-12830;-6415

      CMP    @_i16LowTempLoopflag,#0
      BF     Low_TempHVoutLLoad, NEQ

      MOVL   XAR7,#_ClatoCpuVar + _u32DCDCPowerFeedbackCLA;;;只使用功率反馈低位，实际16变量就够了
      CMP     *,#1300;;;;大约为300W输出
      BF      Low_Power1B,GT
      CMP     *,#800;;;;大约为250W输出 duanqingan 20140327
      BF      Low_Power2B,LT
      MOVL  XAR7,#_IsrVars + _u16LowPowerFlag1
      CMP     *,#1
      BF    Low_Power2B,EQ
      BF        Low_Power1B,UNC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;低温，高压，负载小于600W切换电压环参数;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Low_TempHVoutLLoad:

      MOVL   XAR7,#_CputoClaVar + _u16DCCurloopCoef
      MOV    *,#800;900;QGF
      MOV    @_i16VdcLoopK3,#14516;12007;QGF
      MOV    @_i16VdcLoopK4,#-14004;-11275;QGF

      MOV    @_i16VRippleK1,#-51
      ;MOV    @_i16VRippleK2,#18390;
      ;MOV    @_i16VRippleK3,#-16962;
      MOV    @_i16VRippleK2,#27768;
      MOV    @_i16VRippleK3,#-26549;

      BF   LOOPSWITCH_END,UNC
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Low_Power1B:    
      MOVL XAR7,#_IsrVars + _u16LowPowerFlag1
      MOV     *,#0
      MOV    @_i16VRippleK1,#-51
      MOV    @_i16VRippleK2,#18390;
      MOV    @_i16VRippleK3,#-16962;

      MOVL    XAR7,#_IsrVars + _u16LowPowerFlag2;qgf
      CMP     *,#1
      BF      Medium_Low_Power1B,EQ
Ultar_Low_Power1B:
      MOVL   XAR7,#_CputoClaVar + _u16DCCurloopCoef
      MOV    *,#1500;2000 ;qgf
      MOV    @_i16VdcLoopK3,#8601;
      MOV    @_i16VdcLoopK4,#-7723;
      BF     LOOPSWITCH_END,UNC
Medium_Low_Power1B:
      MOVL   XAR7,#_CputoClaVar + _u16DCCurloopCoef
      MOV    *,#700;800;QGF
      MOV    @_i16VdcLoopK3,#13207;
      MOV    @_i16VdcLoopK4,#-12403;

      BF   LOOPSWITCH_END,UNC

Low_Power2B:    
      MOVL XAR7,#_IsrVars + _u16LowPowerFlag1
      MOV     *,#1
      MOV    @_i16VRippleK1,#-51
      MOV    @_i16VRippleK2,#18390;
      MOV    @_i16VRippleK3,#-16962;

      MOVL    XAR7,#_IsrVars + _u16LowPowerFlag2;qgf
      CMP     *,#1
      BF      Medium_Low_Power2B,EQ

Ultar_Low_Power2B:
      MOVL   XAR7,#_CputoClaVar + _u16DCCurloopCoef
      ;MOV    *,#1500;3200 ;zhangzhen20210128
      MOV    *,#1200;3200 ;zhangzhen
      ;MOV    *,#2500;3200 ;qgf
      MOV    @_i16VdcLoopK3,#24995;9998;8601;QGF
      MOV    @_i16VdcLoopK4,#-23898;-9559;-7723;QGF
      BF     LOOPSWITCH_END,UNC
Medium_Low_Power2B:
      MOVL   XAR7,#_CputoClaVar + _u16DCCurloopCoef
      MOV    *,#1000;3200
      MOV    @_i16VdcLoopK3,#22811;
      MOV    @_i16VdcLoopK4,#-22007;
LOOPSWITCH_END:
      LB    SwitchReturn ;end SW7

;************************************************************						
;   switch8 : AC OVP, max 19CLK,过压脱离
;************************************************************
Switch8:  
     	MOVW    DP,#_IsrVars+_PFC1_DP
        ;CMP     @_i16AdAcVoltTrue,#_VAC_AD_275V_RMS
        CMP     @_i16AdAcVoltTrue,#_VAC_AD_305V_RMS
        BF      AcVolt_Normal,LEQ

        MOVW    DP,#_IsrVars+_PFC_DP
	    MOVL   	XAR7,#_IsrVars+_i16Vpfc490VPoint
	    MOV    	AL, *
        CMP     AL,@_i16AdVpfcProtect
        BF      AcVolt_Normal,GT

	    CMP     @_u16PermitOverAc,#0   ;default: AC OVER not permit
        BF      AcVolt_Normal,EQ
     ;  INC     @_u16OverAcVolt   ;for 4.4kv surge, aviod 800us ACOVP
     ;  CMP     @_u16OverAcVolt,#6
     ;  BF      AcVolt_Normal,LT
      	; off dcdc
     	MOVL   	XAR7,#_EPwm4Regs+_AQCSFRC
        MOV    	*,#0x01
        MOVL   	XAR7,#_EPwm5Regs+_AQCSFRC
        MOV    	*,#0x01
        MOVL   	XAR7,#_EPwm6Regs+_AQCSFRC
        MOV    	*,#0x01
        MOVL   	XAR7,#_EPwm7Regs+_AQCSFRC
        MOV    	*,#0x01

		; off pfc
        MOVL   	XAR7,#_EPwm1Regs+_AQCSFRC
        MOV    	*,#0x01
        MOVL   	XAR7,#_EPwm3Regs+_AQCSFRC
        MOV    	*,#0x01

      	;main relay control ,GPIO5
        MOVL    XAR7,#_GpioDataRegs+_GPACLEAR ;GPIO5 MainRelay
        OR      *,#B5_MSK

		;over relay control ,GPIO9
        MOVL    XAR7,#_GpioDataRegs+_GPASET
        OR      *,#B9_MSK

		MOV     @_u16OverAcVolt,#1

AcVolt_Normal:	

		LB SwitchReturn 	; end SW8	

;************************************************************						
;   switch9 : Vpfc UVP, max 20CLK
;************************************************************
Switch9:   
	
;Pfc quick limit power & Vpfc under voltage protect
	    MOVW    DP,#_IsrVars+_PFC1_DP
		CMP 	@_u16PfcOpened,#0
		BF 		PfcLimitNormal,EQ

        MOV     AL, @_i16AdVpfc0
        MOVL    XAR7, #_IsrVars+_i16Vpfc300VPoint
        CMP 	AL, * 								;300V
		BF      PfcLimitNormal,GT					;大于门槛
        INC     @_u16LowPfcVoltCnt					;计数值累加
        CMP     @_u16LowPfcVoltCnt,#2				;计数值与2进行比较
        BF      PfcUvJdgEnd,LT						;<2则不处理
PfcSmall:
		MOV 	@_u16LowPfcVoltCnt,#2				;>2则限制在2
		MOV 	@_u16PfcQuickPFCUVPFault,#1	    ;加工出欠压标志-659

		; off dcdc
     	MOVL   	XAR7,#_EPwm4Regs+_AQCSFRC
        MOV    	*,#0x01
        MOVL   	XAR7,#_EPwm5Regs+_AQCSFRC
        MOV    	*,#0x01
        MOVL   	XAR7,#_EPwm6Regs+_AQCSFRC
        MOV    	*,#0x01
        MOVL   	XAR7,#_EPwm7Regs+_AQCSFRC
        MOV    	*,#0x01
		; off pfc
		MOVL    XAR7,#_EPwm1Regs+_AQCSFRC
        MOV     *,#0x01
        MOVL    XAR7,#_EPwm3Regs+_AQCSFRC
        MOV     *,#0x01

		;main relay control ,GPIO5
        MOVL    XAR7,#_GpioDataRegs+_GPACLEAR ;GPIO5 MainRelay
        OR      *,#B5_MSK

		BF      PfcUvJdgEnd,UNC

PfcLimitNormal:
        MOV 	@_u16LowPfcVoltCnt,#0
PfcUvJdgEnd:


		LB SwitchReturn		;end SW9
;--------------------------------------------------------		

;************************************************************						
;   switch10 : U1 temperature filter, 13 CLK
;************************************************************
Switch10:    

;U1 temperature 10Hz filter
		MOVW    DP,#_IsrVars + _DCDC1_DP
		MOVL 	XAR7,#_AdccResultRegs+_TEMPA
		MOVL    ACC,@_lU1Temp 
        MPY     P,@_u16ADU1Temp,#_Filter10HzK2
        MOVA    T,*
        MPY     P,T,#_Filter10HzK2
        MOVA    T,@_lU1Temp_H                
        MPY     P,T,#_Filter10HzK1
        MOVA    T,*
        MOVL    @_lU1Temp,ACC
        MOV     @_u16ADU1Temp,T

;;;;;;;;;;;判断是否需要退出电流相位补偿;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;进入动态_i16PfcDynFlg＝32767，如果不进入则会每拍减1，;;;;;;;
;;;;;;;;;;所以32760÷(70000/16)hz,大约连续7S处于稳态则启用电流?前相位补??;;;;
	  	MOVL 	XAR7,#_IsrVars + _i16PfcDynFlg;;xgh,用来做PFC进入动态的标志
	  	DEC   *
      	MOV   AL,#_PFC_DYN_FLG_LOW_LIMIT
	  	MAX   AL,* ;;;;;;;设置下限，防止
	  	MOV   *,AL
;PfcDynJudge_end:
;PfcDynJudge_end:


;qtest 20200321
;这里要做两件事，一个是把我们计算的数值和上边的限功率取个小；
;另一件事就是和G4一样通过母线电压再限制个功率，然后取小
;为了和_i16DCDCPowerSetCLA比较，我们先把Warnctrl里的数处理一下
;为了逻辑清晰，先取小，得到新值，再和老值更新和步进
Dyn_Power_Lim:
		MOVW    DP,#_IsrVars + _PFC5_DP

		MOVL    XAR7, #_g_u16ActionReady
		CMP     *,#5;DC_SOFT_START
		BF      AC_Power_NoDyn,LT
		;qadd0130

		MOVL    XAR7, #_IsrVars+_i16VacDynFlag
		CMP 	*,#0
		BF      AC_Power_NoDyn,EQ

		;MOVL	   XAR7,#_CputoClaVar + _i16DCDCPowerSetCLA
		MOVL    XAR7, #_IsrVars+_i16PowerLimMax
		MOV 	   AL,*
		;MOVW    DP,#_IsrVars + _PFC5_DP
		CMP        AL, @_i16DCDCPowerSetDyn
		BF           AC_Power_Lim,GT
		MOV       @_i16DCDCPowerSetNew, AL
		BF           BUS_Power_Lim, UNC
AC_Power_Lim:
	    MOV       AH, @_i16DCDCPowerSetDyn
		MOV       @_i16DCDCPowerSetNew, AH
		BF           AC_Power_Dyn_End,UNC

AC_Power_NoDyn:
		MOVL    XAR7, #_IsrVars+_i16PowerLimMax
		MOV 	   AL,*
		MOV       @_i16DCDCPowerSetNew, AL

AC_Power_Dyn_End:
		;qadd0130
		MOVL    XAR7, #_g_u16ActionReady
		CMP     *,#5;DC_SOFT_START
		BF      NO_BUS_Power_Lim,LT
		;qadd0130
BUS_Power_Lim:
		;q0426test
		;MOVL     XAR7,#_AdcbResultRegs+_PFCOVP
		MOVL     XAR7,#_AdccResultRegs+_PFCVSAMP
        CMP       *,#_PFCVolt310V
        ;CMP       *,#_PFCVolt330V
        ;q0426testend
        BF           VPFC_GT_330V,GT
        ;20%???这里乘了2206的系数？？？待确认
        MOV      @_i16DCDCPowerSetBus, #1290
		BF          Power_Lim_Cmp1,UNC
VPFC_GT_330V:
		;q0426test
		;MOVL     XAR7,#_AdcbResultRegs+_PFCOVP
		MOVL     XAR7,#_AdccResultRegs+_PFCVSAMP
		;q0426testend
        CMP       *,#_PFCVolt350V
		BF           Power_Lim_Cmp1 ,LT

NO_BUS_Power_Lim:;qadd0130
		MOV      @_i16DCDCPowerSetBus, #_POWER_LIM_UPPER

Power_Lim_Cmp1:
		MOV      AL, @_i16DCDCPowerSetBus
		CMP       AL, @_i16DCDCPowerSetNew
		BF          Power_Lim_Cmp1_End, GT
		MOV      @_i16DCDCPowerSetNew, AL
Power_Lim_Cmp1_End:
		MOV      AL, @_i16DCDCPowerSetUse
		CMP       AL, @_i16DCDCPowerSetNew
		BF          Power_Lim_Down, GT
		ADD       AL, #1
		MIN       AL, @_i16DCDCPowerSetNew
		MOV   	@_i16DCDCPowerSetUse, AL
		BF          Power_Lim_End, UNC
Power_Lim_Down:
     	MOV      AL, @_i16DCDCPowerSetNew
		MOV      @_i16DCDCPowerSetUse, AL
Power_Lim_End:

		 MOV      AL,@_i16DCDCPowerSetUse
		 MOVL    XAR7, #_IsrVars+_i16PowerLimMaxUse
		 MOV    *,AL
;qtest  end

        LB SwitchReturn		;end SW10

;************************************************************						
;   switch11 : M1 temperature filter, 13 CLK
;************************************************************
Switch11: 	
	
;M1 temperature 10Hz filter ,Q16
		MOVW    DP,#_IsrVars+_DCDC1_DP
		MOVL 	XAR7,#_AdcaResultRegs+_DCTEMP
		MOVL    ACC,@_lDcTemp 
        MPY     P,@_u16ADDcTemp,#_Filter10HzK2
        MOVA    T,*
        MPY     P,T,#_Filter10HzK2
        MOVA    T,@_lDcTemp_H               
        MPY     P,T,#_Filter10HzK1
        MOVA    T,*
        MOVL    @_lDcTemp,ACC
        MOV     @_u16ADDcTemp,T	

		;暂时没有采样PFC板温
		;PFC temperature 10Hz filter ,Q16
		;MOVW    DP,#_IsrVars+_DCDC2_DP
		;MOVL 	XAR7,#_AdcaResultRegs+_TEMPPFC
		;MOVL    ACC,@_lPFCTemp
        ;MPY     P,@_u16ADPFCTemp,#_Filter10HzK2
        ;MOVA    T,*
        ;MPY     P,T,#_Filter10HzK2
        ;MOVA    T,@_lPFCTemp_H
                
        ;MPY     P,T,#_Filter10HzK1
        ;MOVA    T,*
        ;MOVL    @_lPFCTemp,ACC
        ;MOV     @_u16ADPFCTemp,T

		;qtest0601
        ;这里刚好不用翻页，直接就自_DCDC1_DP
SINGLE_TO_DUAL:
		;qtest0610
		CMP		  @_i16PhaseShieldingTest,#1;;;;;C06F
		BF           FORCE_SINGLE_PHASE, EQ
		CMP		  @_i16PhaseShieldingTest,#2;;;;;C06F
		BF           FORCE_DUAL_PHASE, EQ
		;;qtest0610	end
		MOV     @_i16SingleToDualQuick, #0;;_DCDC1_DP
		;;不需限定单相，若当前双相，清计时也没有问题
		;;_i16SingleToDualQuick置1会配合_u16PfcSinglePhaseMode
 		MOVL	  XAR7,#_IsrVars +_i16SinglePhaseLargeCurrFlag
        MOV      AL,*;;;;_i16SinglePhaseLargeCurrFlag
        MOV       *, #0;;每16分拍清一次
        CMP		  AL,#1;;如果16次中有一次大于30A，就执行
        BF           SWITCH_ACCORDING_TO_ERR, NEQ
        BF           DUAL_QUICK_SWITCH, UNC
SWITCH_ACCORDING_TO_ERR:	;;;;;_i16VpfcErr0
		MOVL      XAR7, #_IsrVars+_i16VpfcErr0
		CMP        *, #_PFCVolt20V;;Vset-Vbak
        BF          SWITCH_JUDGE_END, LT
DUAL_QUICK_SWITCH:
      	MOV     @_i16CphaseOnCnt,#0;;这里每满足一次条件就重新计数
        MOV     @_i16SingleToDualQuick,#1
		MOVL     XAR7,#_IsrVars + _u16PfcSinglePhaseMode;;;C06C
		MOV      * ,#0 ;;dual phase，双相工作
SWITCH_JUDGE_END:
		INC        @_i16CphaseOnCnt
		CMP       @_i16CphaseOnCnt, #3500
		BF		      SINGLE_TO_DUAL_END,LT
		MOV      @_i16CphaseOnCnt,#3500;;;
		;qtest0610
		BF		      SINGLE_TO_DUAL_END,UNC
FORCE_SINGLE_PHASE:
		MOVL     XAR7,#_IsrVars + _u16PfcSinglePhaseMode;;;C06C
		MOV      * ,#1 ;;dual phase，双相工作
		BF		      SINGLE_TO_DUAL_END,UNC
FORCE_DUAL_PHASE:
		MOVL     XAR7,#_IsrVars + _u16PfcSinglePhaseMode;;;C06C
		MOV      * ,#0 ;;dual phase，双相工作
		;;qtest0610	end
SINGLE_TO_DUAL_END:
		;qtest0601end
        LB SwitchReturn		;end SW11

;************************************************************						
;   switch12 : Vpfc filter and Vac max, 13 CLK
;************************************************************
Switch12: 
	;--------------------------------------------------------
		;pfc voltage display 10Hz filter
		;--------------------------------------------------------
		MOVW    DP,#_IsrVars+_PFC_DP
        MOVL 	XAR7,#_AdcbResultRegs+_PFCOVP;;;用作x(n)

        MOVL    ACC,@_lVpfcProtect ;;;y(n-1)<<16
        MPY     P,@_i16AdVpfcProtect,#_Filter10HzK2;;;k2*X(n-1);;;k2=467
        MOVA    T,*;;;;T=x(n), ACC=y(n-1)+k2*X(n-1)
        MPY     P,T,#_Filter10HzK2;;K2*x(n)
        MOVA    T,@_lVpfcProtect_H;;T=y(n-1)不含Q16定标;;ACC=y(n-1)+k2*X(n-1)+K2*x(n)

        MPY     P,T,#_Filter10HzK1;;y(n-1)*K1;;;K1=-934;;
        MOVA    T,*;;;T=Vpfc, ACC=y(n-1)+y(n-1)>>16*K1+k2*X(n-1)+K2*x(n)
        MOVL    @_lVpfcProtect,ACC;;;y(n-1)，含有Q16定标的哟
        MOV     @_i16AdVpfcProtect,T ;;x(n-1)=T

 		;--------------------------------------------------------
		;Vacmax
		;--------------------------------------------------------
		MOVW    DP,#_IsrVars+_PFC1_DP
		MOV 	AL,@_i16AdAcVoltTrue
		CMP		AL,@_i16AdAcMax;;不需要快速比较,CMP足够了
		BF		AcMax_1,LT
		MOV 	@_i16AdAcMax,AL;;瞬时值大才更新

		MOVL    XAR7,#_g_u16ACMAXPolarity;;;更新峰值把当时的极性也更新,不需要每拍都去更新,这是个慢速标志位
		MOV		AL,@_u16VacPolarity
        MOV     *,AL 
AcMax_1:
              
        LB SwitchReturn		;end SW12

;************************************************************						
;   switch13 :FAN voltage filter
;************************************************************
Switch13:  
        MOVW    DP,#_IsrVars+_DCDC1_DP
		MOVL 	XAR7,#_IsrVars + _u16MainRly_Timer
		INC 	*
;---------------------------------------------------------------		
; Fan voltage 10Hz filter ,Q16
		
		MOVL 	XAR7,#_AdcaResultRegs + _FANBAD
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; saving 2 clk by zzh 4-26													
		MOVL    ACC,@_lFanBad 
        MPY     P,@_u16ADFanBad,#_Filter10HzK2
        MOVA    T,*
        MPY     P,T,#_Filter10HzK2
        MOVA    T,@_lFanBad_H
                
        MPY     P,T,#_Filter10HzK1
        MOVA    T,*
        MOVL    @_lFanBad,ACC
        MOV     @_u16ADFanBad,T
    
		LB SwitchReturn		;end SW13
				
;************************************************************						
;   switch14 : Vdc filter, 13 CLK
;************************************************************
Switch14: 		
;--------------------------------------------------------
;for dcdc ripple voltage loop 10Hz filter,Q16
	
		MOVW    DP,#_IsrVars + _DCDC1_DP
;;;;;;;;;;;;;;;;;输出电压显示 10HZ滤波;;;;;;;;;;;;;;;;;;;;;;;;
		MOVL 	XAR7,#_IsrVars + _lVdcUse_H
        MOVL    ACC,@_lVdcDisUse 
        MPY     P,@_i16AdVdcDis,#_Filter10HzK2
        MOVA    T,*
        MPY     P,T,#_Filter10HzK2
        MOVA    T,@_lVdcDisUse_H              
        MPY     P,T,#_Filter10HzK1
        MOVA    T,*
        MOVL    @_lVdcDisUse,ACC
        MOV     @_i16AdVdcDis,T

        MOVW    DP,#_IsrVars + _PFC2_DP
;;;;;;;;;;;;;;;;;判断输入电压是直流还是交流;;;;;;;;;;;;;;;;;;
;;如果输入的瞬时值电压绝对值小于30V,_u16InputLowVolCNT+1
        MOVL    XAR7, #_IsrVars + _i16AdAcVoltTrue
        CMP     *,#_VAC_AD_30V      
		BF      LOW_INPUT_VOL, LT
        MOV     @_u16InputLowVolCNT,#0
		BF      LOW_INPUT_VOL_END,UNC
		 
LOW_INPUT_VOL:
        INC     @_u16InputLowVolCNT
LOW_INPUT_VOL_END:


;;;;;;;如果极性是LN，则_u16DC_LN_CNT+1，_u16DC_NL_CNT清0
;;;;;;;如果极性是NL，则_u16DC_NL_CNT+1，_u16DC_LN_CNT清0
    	MOVL    XAR7, #_IsrVars + _u16VacPolarity    ;
		CMP     *,#1
        BF      DC_IN_NL,EQ
        ADD     @_u16DC_LN_CNT,#1
		MOV     @_u16DC_NL_CNT,#0
		BF      DCIN_JUDGE_END,UNC

DC_IN_NL:
        ADD     @_u16DC_NL_CNT,#1  
		MOV     @_u16DC_LN_CNT,#0

DCIN_JUDGE_END:
     

        LB SwitchReturn		;end SW14
;************************************************************						
;   switch15 : Idc filter, 13 CLK
;************************************************************
Switch15: 			
;--------------------------------------------------------
;dcdc current display 10Hz filter
		MOVW    DP,#_IsrVars + _DCDC1_DP
	;	MOVL 	XAR7,#_IsrVars + _i16AdIdc
        MOVL	XAR7,#_AdcaResultRegs + _DCCURR

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Saving 2 clk by zzh 4-26
        MOVL    ACC,@_lIdcDisUse 
        MPY     P,@_i16AdIdcDis,#_Filter10HzK2
        MOVA    T,*
        MPY     P,T,#_Filter10HzK2
        MOVA    T,@_lIdcDisUse_H
                
        MPY     P,T,#_Filter10HzK1
        MOVA    T,*
        MOVL    @_lIdcDisUse,ACC
        MOV     @_i16AdIdcDis,T


		CMP 	@_u16SoftStartEndTimer,#20 
		BF 		LOADDELTA_COMP_END,LT
		CMP		@_u16LoadDeltaFlag,#1
		BF		NO_LOADDELTA_COMP,NEQ
;;曲率补偿
;;补偿公式为：LOAD=35（半载电流-原边电流采样）
;;VSET=VSETTEM+LOAD
;-------------------------------------------------------K值分段
        ;MOVL    XAR7,#_IsrVars + _u16LowVolDcLoopChgFlag
		;CMP     *,#0
		;MOVB    @_u16CurrShareDeltaK,#70,EQ
        ;MOVB    @_u16CurrShareDeltaK,#140,NEQ
;-------------------------------------------------------
		MOV		AL,@_i16AdIdc25A
		SUB		AL,@_i16AdIdcDis
		MOV 	T,@AL

		MPY	ACC,T,#_CurrShareDeltaK
		MOVH 	@_i16LoadDelta,ACC<<6 ;>>low 16bits of(ACC>>10)
		MOV		AL,@_i16LoadDelta

		MOV     @AH,#_CurrDeltaPos
		MIN	    AL,@AH
		MOV     @AH,#_CurrDeltaNeg
		MAX	    AL,@AH
		
		ADD		AL,@_i16VdcSetTmp			
		
        MOVL 	XAR7,#_IsrVars + _i16VdcSet
        MOV     * , AL
		BF		LOADDELTA_COMP_END,UNC
		
NO_LOADDELTA_COMP:				
		MOV		AL,@_i16VdcSetTmp
        MOVL 	XAR7,#_IsrVars + _i16VdcSet
        MOV     * , AL

LOADDELTA_COMP_END:  

       	LB SwitchReturn		;end SW15
																																																																																																																																																																																																																																																			
;------------------------------------------------------
***************************************************************
;* FNAME: _pmw1_isr
;* FUNCTION ENVIRONMENT
;* FUNCTION PROPERTIES
;* ;zero int
;*主相主管过零中断: 对counter值进行存储并清0操作
;***************************************************************
_Epwm1_Isr:;;;进中断16CLK
	;entry into Isr ,TBCTR==0x00
	;automatic context save :
	;ST0,T,AL,AH,PL,PH,AR0,AR1,ST1,DP,IER,DBGSTAT,return address
	;context save
	ASP
	MOVL    *SP++,XAR7
	SPM		0
	MOVL    *SP++,XT
	NOP  	*,ARP7
	SETC     INTM      ;;屏蔽其他中断
	CLRC     PAGE0,OVM,SXM;;INTM
	;;保存counter值并且清零

	MOVW     DP,#_IsrVars + _PFC2_DP
	MOVL 	   XAR7,#_ECap4Regs + _TSCTR  ;_u16PfcPwmTph1CTR;
	MOV   	   AL,*
	;;EALLOW
	;;MOVL 	  XAR7,#_ECap4Regs + _ECCTL2
	;;OR         *,#0x0800
	;;EDIS
	MOV      * ,#0
	;;ECap4Regs.ECCTL2.bit.CTRFILTRESET = 1;
	MOV		 @_u16PFC_Nouse012, AL ;;C14EH

	MOVL 	  XAR7,#_EPwm1Regs + _TBCTR  ;_u16PfcPwmTph1CTR;
	MOV   	  AL,*
	MOV		 @_u16PFC_Nouse013, AL ;;C14FH
	;;_u16PfcPWM1TsCTR  	.set 	011DH
;----------------------------------------------------------------------
;  | PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
;;// Acknowledge this int to receive more int from group 3
;----------------------------------------------------------------------
	MOVW    DP,#_PieCtrlRegs + _PIEACK
	;#define PIEACK_GROUP3   0x0004=1 0 0
	MOV     @_PieCtrlRegs + _PIEACK,#4 ;
	MOVL	XAR7,#_EPwm1Regs+_ETCLR  ;
	OR      *,#0x0001
	NOP
	CLRC 	INTM         ;;;使能中断
	MOVL	XT,*--SP
	MOVL    XAR7,*--SP
	NASP
	IRET

;***************************************************************
;------------------------------------------------------
***************************************************************
;* FNAME: _pmw1_isr
;* FUNCTION ENVIRONMENT
;* FUNCTION PROPERTIES
;* ;zero int
;*主相主管过零中断: 对counter值进行存储并清0操作
;***************************************************************
_Epwm3_Isr:;;;进中断16CLK
	;entry into Isr ,TBCTR==0x00
	;automatic context save :
	;ST0,T,AL,AH,PL,PH,AR0,AR1,ST1,DP,IER,DBGSTAT,return address
	;context save
	ASP
	MOVL    *SP++,XAR7
	SPM		0
	MOVL    *SP++,XT
	NOP  	*,ARP7
	SETC     INTM      ;;屏蔽其他中断
	CLRC     PAGE0,OVM,SXM;;INTM
	;;保存counter值并且清零

	MOVW     DP,#_IsrVars + _PFC2_DP
	MOVL 	   XAR7,#_ECap1Regs + _TSCTR  ;;C158H
	;MOVL 	   XAR7,#_ECap4Regs + _TSCTR  ;;C158H
	MOV   	   AL,*
	MOVL 	  XAR7,#_EPwm3Regs + _TBCTR  ;_u16PfcPwmTph1CTR;
	MOV   	   AH,*
	SUB  	      AL,AH
	MOV		 @_u16PFC_Nouse017, AL ;;C158H
;----------------------------------------------------------------------
;  | PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
;;// Acknowledge this int to receive more int from group 3
;----------------------------------------------------------------------
	MOVW    DP,#_PieCtrlRegs + _PIEACK
	;#define PIEACK_GROUP3   0x0004=1 0 0
	MOV     @_PieCtrlRegs + _PIEACK,#4 ;
	MOVL	XAR7,#_EPwm3Regs+_ETCLR  ;
	OR      *,#0x0001
	NOP
	CLRC 	INTM         ;;;使能中断
	MOVL	XT,*--SP
	MOVL    XAR7,*--SP
	NASP
	IRET

;***************************************************************

;-----------------------------------------------------
; _Epwm2_Isr end
;-----------------------------------------------------
    .def    _epwm1_tzint_isr
	.sect	"IsrRamfuncs"
	.global  _epwm1_tzint_isr
;******************************************************	
_epwm1_tzint_isr:	
	
	;automatic context save :
	;ST0,T,AL,AH,PL,PH,AR0,AR1,ST1,DP,IER,DBGSTAT,return address
	;context save
	    ASP
        MOVL      *SP++,XAR7
        SPM       0
        MOVL      *SP++,XT	

        MOVL 	XAR7,#_EPwm1Regs+_AQCSFRC
		MOV 	*XAR7,#0x01
		MOVL 	XAR7,#_EPwm3Regs+_AQCSFRC
		MOV 	*XAR7,#0x01
        MOVL 	XAR7,#_EPwm4Regs+_AQCSFRC
		MOV 	*XAR7,#0x01
		MOVL 	XAR7,#_EPwm5Regs+_AQCSFRC
		MOV 	*XAR7,#0x01	
		MOVL 	XAR7,#_EPwm6Regs+_AQCSFRC
		MOV 	*XAR7,#0x01	
		MOVL 	XAR7,#_EPwm7Regs+_AQCSFRC
		MOV 	*XAR7,#0x01

		;set  g_u16MdlStatusExtend.bit.DCOCP =1 
	    MOVL	XAR7,#_g_u16MdlStatusExtend  
		OR 		*,#B15_MSK

;----------------------------------------------------------------------
;  | EPwm1Regs.TZCLR.bit.OST = 1; placed in main
;  | EPwm1Regs.TZCLR.bit.INT = 1; 
; Leave these flags set so we only take this interrupt once                                             
;----------------------------------------------------------------------
		
		MOVW      DP,#_PieCtrlRegs+_PIEIER2  
        AND       @_PieCtrlRegs+_PIEIER2,#0xFFFE 	
;----------------------------------------------------------------------
;  | PieCtrlRegs.PIEACK.all = PIEACK_GROUP2;	
;// Acknowledge this int to receive more int from group 3                                                     
;----------------------------------------------------------------------
       
        MOVW      DP,#_PieCtrlRegs+_PIEACK ; offset = 1
        MOV       @_PieCtrlRegs+_PIEACK,#2    ;
  
        MOVL      XT,*--SP      
        MOVL      XAR7,*--SP
        NASP
        IRET         	
;-----------------------------------------------------
; no more
;-----------------------------------------------------	

							
