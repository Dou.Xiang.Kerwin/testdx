/*=========================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : HD415CZ
 *
 *  FILENAME : gbb_initial.c
 *  PURPOSE  : variables and parameters initialization	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2011-08-26      A000           DSP               Created.   Pre-research 
 *==========================================================================*/


#include "f28004x_device.h"		// DSP280x Headerfile Include File
#include "App_initial.h"
#include "App_vAdcTreatment.h"
#include "App_warnctrl.h"
#include "App_cal.h"
#include "App_vEventLog.h"
#include "App_cancom.h"
#include "App_isr.h"
#include "App_cal.h"
#include "App_main.h"
#include "App_constant.h"

#include "CLAShared.h"
#include "GBB_constant.h"

#include "math.h"
#include "IQmathLib.h"

#include  "Prj_config.h"

//UINT16   s_u16SfoStatus;
//INT16 MEP_ScaleFactor;
void CLA_initCpu1Cla1(void);
void CLA_configClaMemory(void);
/***************************************************************************
 *Function name: vDataInit()  
 *Description :  All global variable initialization
 *input:         void                                
 *global vars:   void
 *output:        void 
 *CALLED BY:    main() :only once after power on
 ****************************************************************************/			
void vDataInit(void)
{
	UINT32	i;
	UINT32 *addr;
	
	WatchDogKickPointer();

   // because cmd change 

    //���CANͨѶ���������  
    for(i= CAN_ARRAY_RAM_START; i< CAN_ARRAY_RAM_END; i++)
	{
		addr = (unsigned long *)i;
		*addr = 0;
	 }
		  //���CAN VOLͨѶ���������
    for(i= CAN_VOL_ARRAY_RAM_START; i< CAN_VOL_ARRAY_RAM_END; i++)
	{
		addr = (unsigned long *)i;
		*addr = 0;
	 }


   // main ram clear zero
	for(i=MAIN_RAM_START; i<MAIN_RAM_END; i++)
	{
		addr = (unsigned long *)i;
		*addr = 0;
	}

    for(i=MAIN_ISR_RAM_START1; i<MAIN_ISR_RAM_END1; i++)
    {
        addr = (UINT32 *)i;
        *addr = 0;
    }


	// main isr  ram clear zero
	for(i=MAIN_ISR_RAM_START; i<MAIN_ISR_RAM_END; i++)
	{
		addr = (unsigned long *)i;
		*addr = 0;
	}

		//lzy for cla
	for(i= CPUTOCLA_RAM_START; i< CPUTOCLA_RAM_END; i++)
	{
		addr = (unsigned long *)i;
		*addr=0;
	}

	
	WatchDogKickPointer();
	
    g_u16MdlStatusExtend.all = 0;
	
	g_lq10MdlVolt.lData = 0;   //ls 20120307
	 
	g_lq10SetVolt.lData = VDC_DEF;
	g_lq10MdlLimit.lData = IQ10_CURRLlMlTMAX;			
	g_lq10MdlLimitDisp.lData = IQ10_CURRLlMlTMAX;

	//g_lq10SetLimit.lData = IQ10_CURRLlMlTMAX;
	//g_lq10SetLimitTrue.lData = IQ10_CURRLlMlTMAX;
	g_lq10SetLimit.lData = IQ10_CURRLlMlTMAXEXTRA;
    g_lq10SetLimitTrue.lData = IQ10_CURRLlMlTMAXEXTRA;
	g_lq10SetPower.lData = IQ10_SET_POW_LlM_DEF;
	g_lq10SetPowerTrue.lData = IQ10_SET_POW_TRUE_LlM_DEF;
	
	//qtest0601
	g_u16PhaseSheddingFlag = 0;
	//qtest0601end

	//for  derating mixed
	g_i32PowerRate = IQ10POW_RATE;
	g_i32CurrRate = CURR_RATE;
	g_i32CurrDisMax = IDC_UP;
	g_i32CurrLimtMax= IQ10_CURRLlMlTMAX;
	g_i32CurrLimtMaxExtra=IQ10_CURRLlMlTMAXEXTRA;

	//for nor derating mixed
	g_i32CurrChgFactor = 1024;
	g_i32AvgCurrChgFactor = 1024;
	
	//g_u16RdNumSet = 0x0700;  //����CLA�����ڴ��ַ��9200~A000�������������ԡ�

	//g_u16RdNumSet = 0x0200;  //ʹ��CLA��0X00~0X400����ڴ����
	//g_u16RdNumSet = 0x1800; //0x0200;  //ʹ��CLA��0x8000,��ʼ1800���ȵ��ڴ�����ڴ���ڴ���� YQH test
	g_u16RdNumSet = 0x2F00; //0x0200;  //ʹ��CLA��0x8000,��ʼ1800���ȵ��ڴ�����ڴ���ڴ���� YQH test

	g_u16PermitAdjust = NORMAL_RUN_MODE;				
	

	g_u16AddressSmall = MAX_NUM;	// defined to 126
	 //set the flag of request rectifier 
	 //ID for address auto-identifiication
	g_u16AddIdentifyFlag = REQUEST_ID;
	g_u16VoltAdjDelta = VOLT_ADJUST_DEF; 
	



	g_u16PfcVoltDebug = 0;

	//g_i16VPFCCALFlag = 0;
	g_lq10SetPfcVolt.lData = VPFC_DEF_SET;
    g_lq10PfcVoltRegulate.lData = VPFC_DEF_SET;
	g_u16PowerMixedSystemType = 0;

	g_u16DcRippSWCtrl=1;

	//g_lq10HWHVSDReonTime.lData = IQ10TIME300S;

	for (i = 0; i < MAX_NUM; i++)
	{
		CanComVars._au16MdlOnoff[i] = 0x03;
	}
	
	g_u16VersionNoSw = SW_VERSION;
    g_i16FANADJFLAG = 0;
    g_i16FANADJVAL = FAN_LOW;
	g_i16FanPwm = FAN_CMP_MIN;
	g_i16FanPwmSet = FAN_CMP_MIN ;
	g_i16FanRunMin	=  FAN_RUN_MIN;

	g_u16PFCphaseADJFLAG =1;
	 g_u16newPFCCORECOEF = 1310;
    g_u16downloadflag = 0;
   // g_u16OverRelayTimer = TIME3S_20MS_BASE;
    g_u16OverRelayTimer = TIME3S_20MS_BASE;//ģ���ϵ�ǰ3S��AC��ֵ����ʾ
	g_i16FanSpecialSet= FAN_EXTRA_LOW;
    //ĸ�ߵ�ѹ��ѹ���Ե�����ֵ�����������л�
	 //g_lq10PfcVoltUpValue.lData = VPFC_ADJUST_UP_Hload;
    //�Ʋ����ڹ�װ����״̬
    g_u16LEDFlg = 0; 
	//��ģ����������д��
	g_u16CharactData0H = MDL_CHARCT_H;  
	g_u16CharactData0L = MDL_CHARCT_L;

	g_i16FanFailTimer2 =0;

    IsrVars._u16PermitOverAc = FORBID_ACOVER;

	g_lq10VpfcVdcRatio.lData = LQ10_VPFC_VDC_RATIO_DEF; //(405/53.5)*1024 = 7752

	g_i16FanFailTimer2 =0;

    IsrVars._u16PermitOverAc = FORBID_ACOVER;
    g_u16RepairEnable = RECENABLE;//RECDISABLE;  //��ά�޳�ʼ��Ϊ��ʹ��
	g_u16AlarmLogEnable = RECENABLE;//20160115 //RECDISABLE //��ʹ�� 

    IsrVars._u16AcChangeFlag = 1; //�������������,Ĭ��ֵ����Ϊ1

    g_lq10TempDc.lData = TEMP_50C;//�ж��˲������ֵ
    g_lq10TempDcDispSave.lData = TEMP_50C;//���ȼ��ٺ��޹���
    g_lq10TempDcDisp.lData = TEMP_50C;//pfc temperature for display

    g_lq20SourceAdjSysa.lData = AC_ADJUST_SYSA;// Դ��������ϵ��
    g_lq10SourceAdjSysb.lData = AC_ADJUST_SYSB;

    g_u16CustNumDisMatch = 0;
    g_u16ReceiveDataStatus = 0;
    g_u16ReceiveDataEnFlag =0;
    g_u16CusSitNumDisMatchFlag = 0;
    g_u16AntiThiefDelayCnt = 0;


}

/***************************************************************************
 *Function name: HrmstepInit()  
 *Description :  All global variable initialization
 *input:         void                                
 *global vars:   void 
 *output:        void 
 *CALLED BY:    main() :only once after power on
 ****************************************************************************/	
/*
void HrmstepInit(void) 
{
     //lzy for SFO variable initialization	
	 s_u16SfoStatus = SFO_INCOMPLETE;
     MEP_ScaleFactor = 0;

	 while  (s_u16SfoStatus == SFO_INCOMPLETE)
    {  // Call until complete
	    s_u16SfoStatus = SFO();
        if (s_u16SfoStatus == SFO_ERROR) 
        {
           // SFO function returns 2 if an error occurs & # of MEP
          // steps/coarse step exceeds maximum of 254.
            error();    
		}
    }
}
*/
/***************************************************************************
 *Function name: HrmstepInit()  
 *Description :  All global variable initialization
 *input:         void                                
 *global vars:   void 
 *output:        void 
 *CALLED BY:    main() :only once after power on
 ****************************************************************************/	
/*
void HrmstepLoop(void) 
{
	s_u16SfoStatus = SFO(); // in background, continuously updates MEP_ScaleFactor
	if (s_u16SfoStatus == SFO_ERROR) 
	{
		error();
	}

}
*/
/***************************************************************************
 *Function name: vIsr_init()  
 *Description :  Isr Vars initialization
 *input:         void                                
 *global vars:   void 
 *output:        void 
 *CALLED BY:    main() :only once after power on
 ************************************************************/	
void vIsr_init()
{
    ubitfloat   lTmp;
    g_i16VpfcCalCnt = 1;
/*****************************  PFC  ************************/
    IsrVars._i16PfcDutyPermit = PFC_TON_MIN;
    IsrVars._i16TonKpower = 1;
    IsrVars._i16AdVacThreshold = 113;//15V*7.51181 =226
    IsrVars._u16PfcPWM1TsCTR = 375;//50V*7.51181 =375
//    IsrVars._i16AdVacThreshold = 226;//30V*7.51181 =226
//    IsrVars._i16AdVacThreshold = 376;//50V*7.51181

    // 15Hz filter
    IsrVars._lVpfcFilterK1.iData.iLD = -1856;
    IsrVars._i16VpfcFilterK2 = 928;


    // 20Hz filter
    //IsrVars._lVpfcFilterK1.iData.iLD = -1856;
    //IsrVars._i16VpfcFilterK2 = 928;

    // 30Hz filter
    //IsrVars._lVpfcFilterK1.iData.iLD = -2764;
    //IsrVars._i16VpfcFilterK2 = 1382;


    // K=260��fz=3Hz
    //IsrVars._i16VpfcLoopKp = 20430;//����1.4��
    //IsrVars._i16VpfcLoopKi = 88;//����1.4��

    //IsrVars._i16VpfcLoopKp = 14319;//����1.4��
    //IsrVars._i16VpfcLoopKi = 62;//����1.4��


    IsrVars._i16VpfcLoopKp = 28683;
    IsrVars._i16VpfcLoopKi = 123;

    //IsrVars._i16VpfcLoopKp = 20430;
    //IsrVars._i16VpfcLoopKi = 88;

    IsrVars._u16VpfcQn = 12;//13
    IsrVars._u16VpfcPara = 0;
    IsrVars._u16LoopTest = 0;
//---------------------------------------------------------- 
    //PFC ��ѹ������޷�max = Q13
    IsrVars._i16VpfcPiOutMax = 8191;//0x1FFF ;
    //1/VacRms^2 ,Q25
    IsrVars._lVacRmsSqrInv.lData = (INT32)26214 ;

//---------------------------------------------------------------
    //IsrVars._i16PfcQn = 2;
//--------------------------------------------------------------
    // 1 Hz filter
    IsrVars._lVacRmsSquFltK.lData = (INT32)3080351;
    IsrVars._lVacAvgFltK.lData = (INT32)3080351;
    IsrVars._lAcCurAdcSquFltK.lData = (INT32)3080351;
    IsrVars._lAcCurRmsSquFltK.lData = (INT32)3080351;

    IsrVars._u16AcVoltPhaseshift = 5;

    //PFC �ж��õ��ĵ�ѹ������
    IsrVars._i16Vpfc460VPoint = 3680 ;  //460V 460��8=3680
    IsrVars._i16Vpfc490VPoint = 3920 ;  //490V  490��8=3920
    IsrVars._i16Vpfc300VPoint = 2400 ;  //300V  300��8=2400
    IsrVars._i16Vpfc70VPoint = 560 ;    //70V   70��8=560

    IsrVars._i16VinHighPeakFlag = 0;
    IsrVars._i16PfcOvershootVal = VPFC_AD_Overshoot25V;// #_PFCVolt25V
    IsrVars._i16PfcSlowLoopQnSet = 12;
    //4���컷
    IsrVars._i16PfcQuickLoopQnSetQtest = 10;
    IsrVars._i16VpfcPioutShiftSetQtest = 2;

    IsrVars._i16PfcQuickLoopQnSet = 11;
    IsrVars._i16VpfcPioutShiftSet = 1;

    IsrVars._i16LowTempPFCloopFlag = 0;
    IsrVars._u16PwmEnableFlag = 0;

    //calculate TLC
    IsrVars._u32Lpfc = DEFAULT_INDUCTANCE;   //100uH
    IsrVars._i16Coss = DEFAULT_MOSCOSS;   //140  ####TK31N60W5
    lTmp.lData = (long)IsrVars._u32Lpfc * IsrVars._i16Coss<<3;//L*8*Coss;;Added By XB
    lTmp.lData = _IQ6sqrt(lTmp.lData)>>3;   //TLC = sqrt(2*Lpfc*Coss)  unit =ns  204ns                                           //      sqrt(2*Lpfc*Coss*2^6)/2^3
    IsrVars._i16TLC =(int16)(lTmp.lData * 60/1000); //8cnt;;/1000��Ϊsqrt(u*p)=1/1000*u

    //qtest
    IsrVars._u16RamReadEn = 0;//��ʼ����ʹ��
    IsrVars._u16RamReadCnt = 0;//������ʼΪ0
    IsrVars._u16RamReadInterval = 64;//������1����16���Ķ�һ��
    //IsrVars._u16RamReadInterval = 4;//������1����16���Ķ�һ��

    //IsrVars._u16PAddress = 0xC121;//_i16AdAcVoltTrue
    //IsrVars._u16PAddress = 0xC23B;//_u16TCMTonMinCMPA;//0xC23B
    //IsrVars._u16PAddress2 = 0x406B;//CMPA
    //IsrVars._u16PAddress3 = 0xC235;//_u16IpfcTonVarTest
    //IsrVars._u16PAddress4 = 0xC23A;//_u16IpfcTonConTest

    //IsrVars._u16PAddress = 0x14B7;//_i16DCPFCPowerDetlaFilter
    //IsrVars._u16PAddress2 = 0x0b41;//_AdccResultRegs+_PFCVSAMP
    //IsrVars._u16PAddress3 = 0xC060;//_i16PFCFastLoopForce
    //IsrVars._u16PAddress4 = 0xC066;//_u16VpfcOnePhasePower

    //IsrVars._u16PAddress = 0xC121;//_i16AdAcVoltTrue
    //IsrVars._u16PAddress2 = 0xC232;//_u16TonHVpermit
    //IsrVars._u16PAddress3 = 0xC233;//_i16PwmTonshift
    //IsrVars._u16PAddress4 = 0x406B;//CMPA

    IsrVars._u16PAddress = 0x0b02;//_DCVOLT
    IsrVars._u16PAddress2 = 0xC0A5;//_i16DCDC_Nouse06Qtest
    //IsrVars._u16PAddress2 = 0xE007;//g_u16ActionReady
    //IsrVars._u16PAddress2 = 0x14B4;//_u16viewflag1  PowerFeedBack
    IsrVars._u16PAddress3 = 0xC07D;//_u16SoftStartEndTimer   Permit

    //IsrVars._u16PAddress3 = 0x1501;//_i16IdcPioutPermitCLA   Permit
    IsrVars._u16PAddress4 = 0xC001;//_i16VdcSet

    //IsrVars._u16PAddress2 = 0x0b01;//
    //IsrVars._u16PAddress = 0x0b41;//_AdccResultRegs+_PFCVSAMP
    //IsrVars._u16PAddress2 = 0xC00A;//_i16VdcErrUse1000AH

    //IsrVars._u16PAddress = 0x5204;//Ecap1
    //IsrVars._u16PAddress = 0x5244;//Ecap2
    //IsrVars._u16PAddress = 0xC159;//_u16PFC_Nouse018
    //IsrVars._u16PAddress =  0x406B;//CMPA
    //IsrVars._u16PAddress2 = 0xC237;//_u16ChangeFlag
    //IsrVars._u16PAddress2 = 0x0b41;//_AdccResultRegs+_PFCVSAMP
    //IsrVars._u16PAddress2 = 0xC14F;//TBCTR
    //IsrVars._u16PAddress3 = 0xC121;//_i16AdAcVoltTrue
    //IsrVars._u16PAddress3 = 0xC158;//_u16PFC_Nouse017
    //IsrVars._u16PAddress4 = 0xC238;//_u16VacAbs
    //IsrVars._u16PAddress3 = 0xC14E;//TSCTR
    //IsrVars._u16PAddress3 = 0xC233;//_i16PwmTonshift
    //IsrVars._u16PAddress4 = 0xC07C;//_u16PFCOCP
    //IsrVars._u16PAddress4 = 0x406B;//CMPA

    IsrVars._u16Vref1V6CLA = VAC_OFFSET_DF_VALUE;

    IsrVars._u16vref1v55 = (Uint16)(g_lq10VacOffsetVolt.lData);

    //for Vpfc fast transfer by PWM
    IsrVars._i16VbusSysa = 1144;
    IsrVars._i16VbusSysb = -2600;

    IsrVars._u16DCDC_Nouse01 = 0;//C0A0
    IsrVars._u16DCDC_Nouse02 = 500;//C0A1
    IsrVars._u16DCDC_Nouse03 = 8295;//C0A2
    IsrVars._u16DCDC_Nouse04 = -8002;//C0A3

    IsrVars._i16PFCPRDMINCLA = PFC_PRD_DEF;


/*****************************  DCDC  ************************/
	/*-------------------------------------------------
	 dcdc para int
	----------------------------------------------------*/
    //DCDC��ѹ�������˲�ϵ��������11.15HZ,���245HZ��Q16����
	IsrVars._lVdcFilterK1.lData = (INT32)65470;//65484;ls change 20111019
	IsrVars._i16VdcFilterK2 = 3014;//3007;
	IsrVars._i16VdcFilterK3 = -2948;//-2956;


	IsrVars._u16DCCurrADtrigValue2 = 5; //DQA 20121226
    IsrVars._u16DCCurrADtrigValue = 0;

//----------------------------------------------------------
	 
	IsrVars._i16VdcLoopK3 = 3772;  // 6407//DQA changed 20121222
	IsrVars._i16VdcLoopK4 = -3387;//-6342//DQA changed 20121222

//----------------------------------------------------------	
	IsrVars._i16CurrLimFloor = POWER_LIM_FLOOR ;
 	IsrVars._i16PowerLimMax = POWER_LIM_UPPER ;
    IsrVars._i16PowerLimMaxUse = POWER_LIM_UPPER ;
	IsrVars._i16PowerMaxExtra = IsrVars._i16PowerLimMax + POW_MAX_DELTA;
	//0.03*2000*4915/1024=
	IsrVars._i16PowerLimMin = 300;

	IsrVars._i16IdcdcLoopQn = 5;   //5

	IsrVars._i16VRippleK1 = -51;//DQA changed 20121222
	IsrVars._i16VRippleK2 = 3738;//12377;//DQA changed 20121222
	IsrVars._i16VRippleK3 = -3356;// -11672;//DQA changed 20121222


//	IsrVars._lDcdcPWMTs.iData.iHD = IsrVars._lDcdcPWMTsShadow.iData.iHD= EPWM1_TIMER_TBPRD*2; 
	IsrVars._lDcdcPWMTsShadow.iData.iHD= DC_TIMER_TBPRD*2;
//	IsrVars._lDcdcDuty.iData.iHD = IsrVars._lDcdcDutyShadow.iData.iHD = 0;
    IsrVars._lDcdcDutyShadow.iData.iHD = 0;

	 
	IsrVars._i16DcdcPWMTsMax = DC_TS_MAX_55K;//DC_TS_MAX_85K;// DC_TS_MAX_95K;//DC_TS_MAX_80K;
	IsrVars._i16DcdcPWMTsMin = DCPWM_TS_MIN; //167; //250k //20111111

	IsrVars._lDcdcPWMDutyK.iData.iHD = 5354;//3195;//11184;//12428   ;xgh 5354
	IsrVars._lDcdcPWMDutyD = (INT32)-30444672; //-30504029;//-30609184;//

	IsrVars._i16VdcShortCoefA = SHORT_COEF_A; //ls 20111128

//	IsrVars._i16VdcShortCoefB = SHORT_COEF_B_DEF;// 0x258; //�ŵ���EPROM�г�ʼ��
	IsrVars._u16IpfcLoopGaincoef = 256;
	IsrVars._u16IpfcLoopGaincoefWant = 256;

	IsrVars._i16PfcDynFlg = 0x7fff;

    IsrVars._u16DcdcPWMTsMaxHighLine_80K = DC_TS_MAX_45K;//60M/750=80k
    IsrVars._u16DcdcPWMTsMaxLowLine_103k = DC_TS_MAX_55K;//60M/580=103k
    IsrVars._u16DcdcPWMTsLIM_95K = DC_TS_MAX_50K;//60M/630=95k

	IsrVars._i16VdcRippleRangePos = VDC_RIPPLE_RANGE;     
    IsrVars._i16VdcRippleRangeNeg = (INT16)(VDC_RIPPLE_RANGE*(-1));  
    IsrVars._i16VdcRippleUsePos = VDC_RIPPLE_USE_RANGE;//350;
    IsrVars._i16VdcRippleUseNeg =(INT16)( VDC_RIPPLE_USE_RANGE*(-1));//-350;



//PFC �ж��õ��ĵ�ѹ������
//	IsrVars._i16Vpfc460VPoint = 3680 ;   //460V 460��8=3680
    IsrVars._i16Vpfc490VPoint = 3920 ;  //490V  490��8=3920
	IsrVars._i16Vpfc300VPoint = 2400 ;  //300V  300��8=2400
//	IsrVars._i16Vpfc70VPoint = 560 ;   //70V   70��8=560


    IsrVars._i16VinHighPeakFlag = 0;

    //IsrVars._i16PfcOvershootVal = VPFC_AD_Overshoot25V;// #_PFCVolt25V
    IsrVars._i16VpfcQnSet = 11;

    IsrVars._i16LowTempPFCloopFlag = 0;

    //У׼��IDC OFFSET ֵ��40��Ϊ��·ʱ��IdcOffset ֵ
    // IsrVars._i16ShortIdcOffset = CputoClaVar._i16DCCurOffset - ((INT16)(IQ10_IDC_OffSET_LIMDN >>10));
     //У׼��IDC OFFSET ֵ��40��Ϊ��·ʱ��IdcOffset ֵ
    IsrVars._i16ShortIdcOffset = CputoClaVar._i16DCCurOffset - 40;

    if( IsrVars._i16ShortIdcOffset <= ((INT16)(IQ10_IDC_OffSET_LIMDN >>10)) )
    {
       IsrVars._i16ShortIdcOffset = ((INT16)(IQ10_IDC_OffSET_LIMDN >>10));
    }

    //�ѵ�ѹOFFSETֵ��Ϊ3.5V,��Ϊ��·ʱ��ѹ����
    IsrVars._i16ShortVdcOffset =(INT16)( (IQ10_DEF_VDCFACTOR*4) >> 10 ) ;//  4v

    //FPU initial
    IsrVars._f32TonMaxPrd = (float) 6666.0; //100M/15K=6666; 100M/6000=16.6KHZ
    //IsrVars._f32TonMaxPrd = (float) 5000.0; //100M/15K=6666; 100M/6000=16.6KHZ

    //IsrVars._f32TonMinPrd = (float) 666.0;//150kHz;��ֵ�����޸ģ������Ƶ�ʿ��Էſ���
    IsrVars._f32TonMinPrd = (float) 333.0;//333.0;  //300k;��ֵ�����޸ģ������Ƶ�ʿ��Էſ���

    IsrVars._i16LpfcPFU = (int16)DEFAULT_INDUCTANCE;
    IsrVars._f32Irv = (float) 1.6;

    //IsrVars._f32ExpLC = sqrt(((float)DEFAULT_MOSCOSS *(float)DEFAULT_INDUCTANCE));
    //IsrVars._f32ExpLC = (float) 273.7;
    IsrVars._f32ExpLC = (float) 376.78;
    //IsrVars._f32TrCompeK = 1.0;

    IsrVars._u32Vdc47v7Point = (UINT32)VDC_47V7;
    IsrVars._u32Vdc47v25Point = (UINT32)VDC_47V25;

    IsrVars._u16DCDC_OvpLimitFloor = 200;
    IsrVars._u16DCDC_OvpLimitCeiling = 2900;
    ////////////////////ɨƵ��ʼ��///////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
/*    IsrVars._u16OpenLoopCtrl = 0;//�ϵ粻ɨƵ
    CputoClaVar._u16OpenLoopCtrlCLA = IsrVars._u16OpenLoopCtrl;

    IsrVars._lOpenPIoutStart.iData.iHD=645;//145k
    CputoClaVar._lOpenPIoutStartCLA.iData.iHD= IsrVars._lOpenPIoutStart.iData.iHD;

    IsrVars._lOpenLoopStep.iData.iLD= 20;//�ϵ粽��Ϊ0

    //IsrVars._lOpenPRDDUN.iData.iHD=20;//ɨƵ�����������е�Ƶ����̬���Բ�ʹ��DCPWM_TS_MIN��240
    IsrVars._lOpenPRDDUN.iData.iHD=645;//155k
    IsrVars._lOpenPRDUP.iData.iHD= 714;//140k;

    IsrVars._u16OpenLoopLR=1; //�����ʼ��
*/
    ////////////////////ɨƵ��ʼ������///////////////////////////////////////
    ////////////////////////////////////////////////////////////////////
}

/******************************************************************************
 *Function name: vInitialInterrupts()
 *Description :  enables the  the PIE  interrupts
 *input:         void
 *global vars:   void
 *output:        void
 *CALLED BY:     main()
 ******************************************************************************/

void vInitialInterrupts(void)
{
  // This is needed to write to EALLOW protected registers
    EALLOW;
    PieVectTable.EPWM8_INT = &Epwm8_Isr;
    //PieVectTable.EPWM1_INT = &Epwm1_Isr;
    //PieVectTable.EPWM3_INT = &Epwm3_Isr;

    // This is needed to disable write to EALLOW protected registers
    EDIS;

  // Enable TINT7 in the PIE: Group 3 interrupt 8��
    PieCtrlRegs.PIEIER3.bit.INTx8 = 1;  //EPWM4 ��Ϊ�����ж�
    //PieCtrlRegs.PIEIER3.bit.INTx1 = 1;  //EPWM1��������//PIE_CTRL_REGS
    //PieCtrlRegs.PIEIER3.bit.INTx3 = 1;  //EPWM1��������//PIE_CTRL_REGS
}


/***************************************************************************
 *Function name: vCputoClaVar_init()  
 *Description :  CputoClaVar Vars initialization
 *input:         void                                
 *global vars:   void 
 *output:        void 
 *CALLED BY:    main() :only once after power on
 ****************************************************************************/	
void vCputoClaVar_init(void)
{
    CputoClaVar._i16IdcPioutPermitCLA = 300;//DC_MIN_CMPA;
    CputoClaVar._i16IdcPioutPermitCLA1 = 2900;
    //CputoClaVar._i16DcdcPWMTsMaxCLA = DC_TS_MAX_103K;//DC_TS_MAX_85K;//DC_TS_MAX_95K;//DC_TS_MAX_80K;
    CputoClaVar._i16DcdcPWMTsMaxCLA = DC_TS_MAX_50K;
    CputoClaVar._i16DcdcPWMTsMinCLA = DCPWM_TS_MIN;
	CputoClaVar._u16DCADCCOEF = 34;//42;//����xiangbo��������
    CputoClaVar._u16DCCurloopCoef = 1000;
	CputoClaVar._i16DCCurOffset = IDC_OFFSET_DF_VALUE;
	CputoClaVar._i16DCCurOffsetTrue = IDC_OFFSET_DF_VALUE;
    //20200323 for hot issue @low Vo
    CputoClaVar._u16DCDriveCurveFLG = 0;
    CputoClaVar._u16InterimVal = 90;

    //CputoClaVar._f32FilterK1 = 0.924427893;
    CputoClaVar._u16DCVsamlePointFLG = 0;
    CputoClaVar._f32FilterK2 = 0.037786053;

    CputoClaVar._u16VDCADCOffSet = 24;//2+22=24 clcl
    CputoClaVar._u16IDCADCOffSet = 24;//2+22=24 clcl
    CputoClaVar._u16Ton_BurstOff = DC_DB_TIME - 4;// ��Ъ�������õĵ�ͨʱ�� = ����ʱ��-4
}



/*****************************************************************************
 *FUNCTION: vCLATaskinit() 
 *PURPOSE:  // Initialize the CLA task
 *INPUT: void
 *global vars: 
 *RETURN: void
 *CALLS: 
 *CALLED BY: main() 
 *Author:
 *Date:
 ****************************************************************************/
void vCLATaskinit(void)
{
    CLA_configClaMemory();
    CLA_initCpu1Cla1();
    //ClatoCpuVar_init// ClatoCpuVar�ڴ�����ĳ�ʼ��
    Cla1ForceTask8();          // software Force CLA task 8.
}

//
// CLA_configClaMemory - Configure CLA memory sections
//
void CLA_configClaMemory(void)
{
    EALLOW;

    MemCopyPointer(&Cla1funcsLoadStart, &Cla1funcsLoadEnd,
                   &Cla1funcsRunStart);
    //
    // Initialize and wait for CLA1ToCPUMsgRAM
    //
    MemCfgRegs.MSGxINIT.bit.INIT_CLA1TOCPU = 1;
    while(MemCfgRegs.MSGxINITDONE.bit.INITDONE_CLA1TOCPU != 1){};

    //
    // Initialize and wait for CPUToCLA1MsgRAM
    //
    MemCfgRegs.MSGxINIT.bit.INIT_CPUTOCLA1 = 1;
    while(MemCfgRegs.MSGxINITDONE.bit.INITDONE_CPUTOCLA1 != 1){};

    //
    // CLA Program will reside in RAMLS5 and  RAMLS6, data in RAMLS7
    //
    MemCfgRegs.LSxCLAPGM.bit.CLAPGM_LS5 = 1U;
    MemCfgRegs.LSxCLAPGM.bit.CLAPGM_LS6 = 1U;
    MemCfgRegs.LSxCLAPGM.bit.CLAPGM_LS7 = 0U;

    //
    // Select CLA as the master of RAMLS5, RAMSL6, RAMLS7
    //
    MemCfgRegs.LSxMSEL.bit.MSEL_LS5 = 1U;
    MemCfgRegs.LSxMSEL.bit.MSEL_LS6 = 1U;
    MemCfgRegs.LSxMSEL.bit.MSEL_LS7 = 1U;


    EDIS;
}

//
// CLA_initCpu1Cla1 - Initialize CLA1 task vectors and end of task interrupts
//
void CLA_initCpu1Cla1(void)
{
    //
    // Compute all CLA task vectors
    // On Type-1 CLAs the MVECT registers accept full 16-bit task addresses as
    // opposed to offsets used on older Type-0 CLAs
    //
    EALLOW;

    Cla1Regs.MVECT1 = (uint16_t)(&Cla1Task1);
    Cla1Regs.MVECT5 = (uint16_t)(&Cla1Task5);
    Cla1Regs.MVECT8 = (uint16_t)(&Cla1Task8);

    //
    // Enable the IACK instruction to start a task on CLA in software
    // for all  8 CLA tasks. Also, globally enable all 8 tasks (or a
    // subset of tasks) by writing to their respective bits in the
    // MIER register
    //
    //asm("   RPT #3 || NOP");
    Cla1Regs.MCTL.bit.IACKE = 1;
    Cla1Regs.MIER.all = (M_INT8 | M_INT5 | M_INT1);   // Enable Task 8,6,1
    //
    // Configure the vectors for the end-of-task interrupt for all
    // 8 tasks
    //
//    PieVectTable.CLA1_1_INT = &cla1Isr1;
//    PieVectTable.CLA1_2_INT = &cla1Isr2;
//    PieVectTable.CLA1_3_INT = &cla1Isr3;
//    PieVectTable.CLA1_4_INT = &cla1Isr4;
//    PieVectTable.CLA1_5_INT = &cla1Isr5;
//    PieVectTable.CLA1_6_INT = &cla1Isr6;
//    PieVectTable.CLA1_7_INT = &cla1Isr7;
//    PieVectTable.CLA1_8_INT = &cla1Isr8;

    //
    // Set the adca.1 as the trigger for task 7
    //
    DmaClaSrcSelRegs.CLA1TASKSRCSEL1.bit.TASK1 = CLA_TRIG_NOPERPH;
    DmaClaSrcSelRegs.CLA1TASKSRCSEL2.bit.TASK5 = CLA_TRIG_EPWM5INT;
   // DmaClaSrcSelRegs.CLA1TASKSRCSEL2.bit.TASK6 = CLA_TRIG_NOPERPH;
    DmaClaSrcSelRegs.CLA1TASKSRCSEL2.bit.TASK8 = CLA_TRIG_NOPERPH;

    //
    // Enable CLA interrupts at the group and subgroup levels
    //
//    PieCtrlRegs.PIEIER11.all = 0xFFFF;
//    IER |= (M_INT11 );
    EDIS;
}

//===========================================================================
// No more.
//===========================================================================
