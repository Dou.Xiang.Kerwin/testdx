;	The second level boot loader and application depend on these
;	constants being exactly where they are.  Their addresses are
;  hard coded in the linkder command files because the DSECT
;	liker directive does not prevent overlaying code on the
;  specified section but instead puts code wherever it wants.

;	THE NUMBER AND LOCATION OF THESE CONSTANTS CANNOT BE CHANGED
;  WITHOUT ALSO CHANGING THE SECOND LEVEL BOOT AND APPLICATION!

	;.global _ApplicationValidPointer,	_Application_Valid
	.global _Application_Valid
	.global	_WatchDogKickPointer,		_Watch_Dog_Kick
	.global	_B1AppEntryPointer	,	_B1_App_Entry
	.global	_B1RstEntryPointer	,	_B1_Rst_Entry
	.global _InitCanaIDPointer    ,   _InitCanaID
	.global _InitCanaPointer   ,  _InitCana
	.global _MemCopyPointer  , _MemCopy
	
	.global _InitSysCtrlPointer  ,  _InitSysCtrl
	.global _InitGpioPointer  ,  _InitGpio
	.global _InitCanPointer   ,  _InitCan
	.global _InitCanLoadPointer   ,  _InitCanLoad
	.global _InitI2CPointer  ,   _InitI2C
	.global _DisableDogPointer  ,  _DisableDog
	.global _InitECapturePointer  ,   _InitECapture

	.global _EnableDogPointer  ,  _EnableDog
	.global _InitCpuTimersPointer  ,  _InitCpuTimers
	.global _InitPieCtrlPointer , _InitPieCtrl
	.global _InitPieVectTablePointer , _InitPieVectTable

	.global _InitRAMPointer   ,_InitRAM
	.global _USER_ISR_Pointer  , _USER_ISR
	
	.global _uiI2caWriteDataPointer, _uiI2caWriteData
	.global _uiI2caWriteData1Pointer, _uiI2caWriteData1
	.global _uiI2caWriteData2Pointer, _uiI2caWriteData2

	.global _uiI2caReadDataPointer , _uiI2caReadData
	.global _uiI2caReadData1Pointer , _uiI2caReadData1
	.global _uiI2caReadData2Pointer , _uiI2caReadData2

	.global _fReadFloatDataThreePointer  ,  _fReadFloatDataThree

	
	.global _Boot_Version;_Hardware_Version
	
	.global _SendBlockRequest;,	_SendBlockRequestPointer
	.global _GetHeader;,	_GetHeaderPointer
	.global _GetDataBlock;,	_GetDataBlockPointer
	.global _StartTimer;,	_StartTimerPointer 			
	.global _TimeIsUp;,	_TimeIsUpPointer 
	.global _ProcessInquiry
	
			.sect	".bootvec"
_Boot_Version   .int   0x0100         ; 0x80002
		.int   0x0100                 ; 0x80003

_B1AppEntryPointer			LB			_B1_App_Entry  ;0x80004
_WatchDogKickPointer		LB			_Watch_Dog_Kick	;0x80006
_ApplicationValidPointer	LB			_Application_Valid ;0x80008
_B1RstEntryPointer			LB		    _B1_Rst_Entry ;0x8000A
_InitCanaIDPointer			LB 			_InitCanaID  ;0x8000C
_InitCanaPointer     		LB 			_InitCana  ;0x8000E
_MemCopyPointer      LB 	_MemCopy	;0x80010
_InitSysCtrlPointer  LB 	_InitSysCtrl ;0x80012
_InitGpioPointer     LB 	_InitGpio   ;0x80014
_InitCanPointer     LB 	_InitCan	;0x80016
_InitCanLoadPointer     LB 	_InitCanLoad	;0x80018
_InitI2CPointer      LB 	_InitI2C	;0x8001A
_DisableDogPointer   LB 	_DisableDog	;0x8001C
_InitECapturePointer LB  _InitECapture	;0x8001E
_EnableDogPointer   LB 	_EnableDog		;0x80020
_InitCpuTimersPointer  LB  	_InitCpuTimers ;0x80022
_InitPieCtrlPointer LB 		_InitPieCtrl	;0x80024
_InitPieVectTablePointer  LB	_InitPieVectTable ;0x80026

_InitRAMPointer    LB   _InitRAM ;0x380028
_USER_ISR_Pointer  LB _USER_ISR		;0x8002A

_uiI2caWriteDataPointer   LB    _uiI2caWriteData		;0x8002C
_uiI2caWriteData1Pointer   LB    _uiI2caWriteData1		;0x8002E
_uiI2caWriteData2Pointer   LB    _uiI2caWriteData2		;0x80030

_uiI2caReadDataPointer    LB    _uiI2caReadData			;0x80032
_uiI2caReadData1Pointer    LB    _uiI2caReadData1			;0x80034
_uiI2caReadData2Pointer    LB    _uiI2caReadData2			;0x80036

_SendBlockRequestPointer	LB			_SendBlockRequest	;0x80038
_GetHeaderPointer			LB			_GetHeader			;0x8003A
_GetDataBlockPointer		LB			_GetDataBlock		;0x8003C
_StartTimerPointer			LB			_StartTimer			;0x8003E
_TimeIsUpPointer			LB			_TimeIsUp			;0x80040
_ProcessInquiryPointer		LB 			_ProcessInquiry		;0x80042
_fReadFloatDataThreePointer  LB  		_fReadFloatDataThree	;0x380044
;********************************************************************
	
	;.ref _c_int00
	.global  _BOOT_MAIN
	.global  _uiFromApplication,_g_u16MdlAddr
__stack:	.usect	".stack",0
	
	.sect	"FlashBoot"
_B1_Rst_Entry:
	;MOV     @SP,#0x0400;
	MOV 	AL,#__stack
	MOV     @SP,AL
	SPM     0
	SETC    OBJMODE
	CLRC    AMODE
	.c28_amode
	SETC    M0M1MAP
	CLRC    PAGE0    ;28x mode
	MOVW    DP,#0x0000
	CLRC    OVM
	ASP
	
	SETC	INTM
	MOV  	AL,#0
	MOVW	DP,#_g_u16MdlAddr ; _uiMdlAddr = 0
	MOV 	@_g_u16MdlAddr,AL
	LB 		ContinueB1
_B1_App_Entry:
	SETC INTM	
	MOV 	AL,#0FFH
	
ContinueB1:
	MOVW		DP,#_uiFromApplication
	MOV 	@_uiFromApplication,AL
		
	LCR	_BOOT_MAIN
	SETC	  INTM				; disable interrupts
justdie:
	LB    	  justdie


;***********************************************************************	
	.global  _Software_Version,_Baseline_Version	
	.global  _main,_exit
	.sect	".appvec"	
_Application_Entry_Point:
	MOV 	AL,#__stack
	MOV     @SP,AL
	
    LCR 	_main  ;LB     _c_int00,0x003F4000

_exit_entry			LCR 	_exit

_Software_Version	.int  0x0100
		 			.int   0x0100
_Baseline_Version	.int  205

;***********************************************************************
	.global _CRC16_CheckWord
	 .sect	"appcrc"
_CRC16_CheckWord:
	.int		0x72BC

;***********************************************************************
																				
	.end
;===========================================================================
; No more.
;===========================================================================