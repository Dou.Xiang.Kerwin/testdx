// TI File $Revision: /main/1 $
// Checkin $Date: March 18, 2009   09:24:46 $
//###########################################################################
//
// FILE:   CLAShared.h
//
// TITLE:  CLA and CPU shared variables and constants
//
//###########################################################################
// $TI Release: 2803x Header Files V1.01 $
// $Release Date: April 30, 2009 $
//###########################################################################


#ifndef App_CLA_SHARED_H
#define App_CLA_SHARED_H

#ifdef __cplusplus
extern "C" {
#endif


//#include "IQmathLib.h"
#include "f28004x_device.h"

// The following are symbols defined in the CLA assembly code
// Including them in the shared header file makes them 
// .global and the main CPU can make use of them. 
 extern Uint32 Cla1Task1;
 extern Uint32 Cla1Task5;
 extern Uint32 Cla1Task8;
 extern Uint32 Cla1Prog_Start;

 // extern Uint32 Cla1T1End;
 // extern Uint32 Cla1T2End;
 // extern Uint32 Cla1T7End;
 // extern Uint32 Cla1T6End;
 // extern Uint32 Cla1T8End;
 // extern Uint32 Cla1Prog_End;

#ifdef __cplusplus
}
#endif /* extern "C" */

//#define		Filter10HzK1	0.9972076//0.98999//0.9993    //16switchchangeto4 xgh
//#define		Filter10HzK2	0.0013886//0.005005//0.00035  //16switchchangeto4






//Variables  for PFC control
//CPU to CLA RAM 0x1500-0x157F 固定的，不能更改
struct CputoClaVarStruct 
{
//------------offset=0000H-----------------------
int16   _i16DCDCPowerSetCLA; 
int16   _i16IdcPioutPermitCLA;
int16   _i16DcdcPWMTsMaxCLA;
int16   _i16DcdcPWMTsMinCLA;   
Uint16  _u16DCADCCOEF;
Uint16  _u16DCCurloopCoef;
int16   _i16DCCurOffset;
int16   _i16DCCurOffsetTrue;
Uint16  _u16ShortFlag;
Uint16  _u16DCDriveCurveFLG;//09
Uint16  _u16InterimVal;//0A
int16     _i16OnePhasePowerToCLA;//150B
//float32    _f32FilterK1;            // .set   000CH
Uint16  _u16DCVsamlePointFLG;
Uint16  _u16DCVsamlePointFLG1;
float32    _f32FilterK2;//e/f
Uint16  _u16VDCADCOffSet;  //10
Uint16  _u16IDCADCOffSet;  //11

longunion _lOpenPIoutStartCLA;//12/13
Uint16 _u16OpenLoopCtrlCLA; //14
Uint16 _u16Ton_BurstOff;
Uint16 _i16IdcPioutPermitCLA1;//  .set    0016H
};               
extern volatile struct CputoClaVarStruct CputoClaVar;

//extern 	volatile Uint16	 sinback[46];
//extern  const    Uint16  sinbackbak[46];

//CLA to CPU RAM 0x1480-0x14FF 固定的，不能更改
struct ClatoCpuVarStruct
{

//------------offset=0000H--------1480---------------

float32  _f32DCDCPowerFeedback;
float32  _f32PowerErr0;
float32  _f32PowerErr1;
float32  _f32PowerErrUse0;
float32  _f32PowerErrUse1;
float32    _f32IdcLoopFilterK1;
float32    _f32IdcLoopFilterK2;
float32    _f32IdcLoopFilterK3;
//------------offset=0010H---------1490--------------
float32  _f32AdIdc;
float32  _f32PowerLoopK3;
float32  _f32PowerLoopK4;
float32  _f32IdcPiout;
float32  _f32DcdcPWMDutyK;
float32  _f32DcdcPWMDutyD;
float32  _f32DcdcPWMPRDShadow;
float32  _f32DcdcPWMCMPShadow;
//------------offset=0020H--------14A0---------------
float32  _u16DcdcPWMChgFlag;
//为了在变化频率的DC发波中断里变频计算控制器而设置的变量//
float32  _f32DcdcPWMTs;  //DC功率环控制器的计算频率
float32  _f32PowerLoopKv;//DC功率环PI控制器增益
float32  _f32PowerLoopfz;//DC功率环PI控制器零点
float32  _f32PowerLoop2PIfzINV;//DC功率环PI控制器INV(2PI*Kz)
float32  _f32IdcLoopKv;//DC功率环SPSZ控制器增益
float32  _f32IdcLoopfp;//DC功率环SPSZ控制器极点
float32  _f32IdcLoopfz;//DC功率环SPSZ控制器零点

//------------offset=0030H-------14B0----------------
float32  _f32IdcSZSPTsfpfzPI;//14B0
float32  _f32IdcSZSPTsfpfzPIINV;//14B2
Uint16   _u16viewflag1;//14B4
Uint16   _u16viewflag2;//14B5
Uint16   _u16viewflag3;//14B6
Uint16   _u16viewflag4;//14B7
//int16    _i16DCPFCPowerDetlaFilter;//14B7

Uint32   _u32DCDCPowerFeedbackCLA;//14B8
int16   _i16QtestCLAtoCPU;//14BA
int16   _i16DCPFCPowerDetla;//14BB

float32     _f32DCPFCPowerDetla;//14BC
float32     _f32DCPFCPowerDetlaFilter;//14BE
//------------offset=0040H-------14C0----------------
//float32      _f32FilterK2;//14C0
//float32      _f32FilterK2;//14C2

};               
extern volatile struct ClatoCpuVarStruct ClatoCpuVar;  


#endif  // end of CLA_SHARED definition
