 /*=============================================================================*
 *         Copyright(c) 2007-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : GBB Rectifiers
 *
 *  FILENAME : GBB_macro.h
 *  PURPOSE  : Define the io assignment the macro from the hardware of dsp
 *				     
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *  2008-6-10         OOA            Agnes           Created.   Pre-research 
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *    
 *============================================================================*/

/******************************************************************************/
/*IO definition																  */
/*This part is for the IO port supplied by the DSP 							  */
/******************************************************************************/
#ifndef  Prj_Adc_H
#define  Prj_Adc_H

/*============================================================================*
 * ADCRESULT definition, please design according to define of Adc.h
 *============================================================================*/

#define ADC_usDELAY     5000L
#define ADC_SHCLK       0x08  //0x08 // sample window
#define ADC_SHCLK_CH0   0x0C  //0x0C // sample window
#define ADC_SHCLK_CH1   0x32
//Other invalid ACQPS  selections: 10h, 11h, 12h, 13h, 14h, 1Dh, 1Eh, 1Fh, 20h, 21h, 2Ah, 2Bh, 2Ch, 2Dh, 2Eh, 37h, 38h, 39h, 3Ah, 3Bh

#define EPWM2_ADCSOCA   0x07    // dcdc Ttriger
#define EPWM3_ADCSOCA   0x09    // dcdc Ttriger，随DC开关频率变化，注意是哪一个桥臂
#define EPWM4_ADCSOCA   0x0B    // PFC 开关 PWM
#define EPWM5_ADCSOCA   0x0D
#define EPWM8_ADCSOCA   0x13
#define EPWM6_ADCSOCA   0x0F    // PFC 开关 PWM
#define EPWM4_ADCSOCB   0x0C


#define ADC_SOCPrity    0x04    //00,01,02,03 HIGH PRIORITY

//SOC0-SOC6 are high priority, SOC7-SOC15 are in round robin mode.
//HD415GU111 ADCIN define
/*
//ADCIN_A
#define AMBTEMP          0x00     //A0
#define PFCOVP           0x01     //A1
#define DCCURR           0x02     //A2
#define DCDCTEMP         0x03     //A3
#define GND_A            0x04     //A4
#define DCVOLT           0x05     //A5
#define FANBAD           0x06     //A6


//ADCIN_B
#define GND_B            0x00     //B0
#define VACSAMP1_L       0x02     //B2
#define VACSAMP2_N       0x03     //B3

//ADCIN_C
#define GND_C            0x0E     //C14
#define PFCVSAMP         0x02     //C2
*/
//ADCIN_A
#define PFCCURR1         0x0E     //A14
#define DCCURR           0x09     //A9
#define FANBAD           0x08     //A8
#define DCDCTEMP         0x0A     //A10
#define DCVOLT           0x06     //A6

//ADCIN_B
//#define DCVOLT           0x03     //B3
#define PFCCURR2         0x0A     //B10
#define VACSAMP2_N       0x06     //B6
#define PFCOVP           0x08     //B8
#define DSPTMP           0x0E     //B14
//ADCIN_C
#define AMBTEMP          0x0F     //C15
#define PFCVSAMP         0x02     //C2
#define VACSAMP1_L       0x06     //C6

//qtest
#define ADCRESULT_AcVoltL           AdcbResultRegs.ADCRESULT2
#define ADCRESULT_AcVoltN           AdcbResultRegs.ADCRESULT2
#define ADCRESULT_PfcVolt           AdccResultRegs.ADCRESULT1


#endif
/*inline void vi32MaxMinLimit(INT32 LimitSymbol, INT32 LimitMax, INT32 LimitMin)
{
    LimitSymbol= (LimitSymbol > LimitMax) ? LimitMax : LimitSymbol;
    LimitSymbol= (LimitSymbol < LimitMin) ? LimitMin : LimitSymbol;
}
*/
/*============================================================================*/


