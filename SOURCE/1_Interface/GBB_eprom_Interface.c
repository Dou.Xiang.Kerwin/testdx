/*=============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : gbb_CanDriver.c
 *  PURPOSE  : rectifier communicate with controller and other rectifiers
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A00           zoulx             Created.   Pre-research 
 *    2019-5-9        A01           fhx               updata for 28004x
 *============================================================================*/

#include "f28004x_device.h"     // DSP280x Headerfile Include File
#include "App_main.h"     // DSP280x Headerfile Include File
#include "GBB_constant.h"
#include "App_constant.h"
#include "GBB_eprom_interface.h"
#include "Prj_macro.h"
#include "GBB_eprom_interface.h"
#include  "GBB_epromdata.h"

/*******************************************************************************
 *Function name: vWriteFloatDataSig
 *Description:   write float in some one address
 *input:         u16Address:eeprom address
 *               fTemp: the data that will be write to the eeprom address
 *global vars:   void
 *output:        void
 *CALLED BY:    vWriteE2PROMSIG()
 ******************************************************************************/

void    vWriteFloatDataSig(unsigned int u16Address, ubitfloat fTemp )
{
    ubitfloat  ubitfData;
    ubitfData.lData = fTemp.lData;

    g_u16WriteNumber ++;
    mWrEnable();            //WP=0,enable write

    if (g_u16WriteNumber == 1)
    {
        uiI2caWriteDataPointer(u16Address, ubitfData);
    }
    else if (g_u16WriteNumber == 2)
    {
        uiI2caWriteDataPointer(u16Address + 256, ubitfData);
    }
    else if (g_u16WriteNumber == 3)
    {
        uiI2caWriteDataPointer(u16Address + 512, ubitfData);
    }
    else
    {
        g_u16WriteNumber = 0;
        mWrDisable();           //WP=1,disable write
    }
}

/*******************************************************************************
 *Function name: ucWriteFloatDataThree
 *Description:   write float in three address and verify
 *input:         u16Address:eeprom address
 *               fTemp: the data that will be write to the eeprom address
 *global vars:   void
 *output:        RECOK,NOVCOM
 *CALLED BY:    vLimitInit();sRmpProtocol();
 ******************************************************************************/

UINT16 ucWriteFloatDataThree(UINT16 u16Address, ubitfloat fTemp)
{
    UINT16 u16CodeMsg,i;
    ubitfloat  ubitfData;

    ubitfData.lData = fTemp.lData;

    mWrEnable();            //WP=0,enable write

    WatchDogKickPointer();
    //DisableDogPointer();//lzy 20200227

    u16CodeMsg = I2C_SUCCESS;

    for (i=0; i<3; i++)
    {
        u16CodeMsg = u16CodeMsg
            | uiI2caWriteDataPointer(u16Address + (i << 8), ubitfData);

        DELAY_US(TIME5MS_CPUCLK);

        if (uiI2caReadDataPointer(u16Address + (i << 8)) == I2C_SUCCESS)
        {
            if (ubitfData.lData != g_fRdTemp.lData)
            {
                u16CodeMsg = I2C_ERROR;
            }
        }
        else
        {
            u16CodeMsg = I2C_ERROR;
        }
    }

    //EnableDogPointer();//lzy 20200227
    //WatchDogKickPointer();
    mWrDisable();           //WP=1,disable write

    if (u16CodeMsg == I2C_SUCCESS)
    {
        return  RECOK;                      // success
    }
    else
    {
        return  NOVCOM;                     // fail
    }
}

/*******************************************************************************
 *Function name: fReadFloatDataThree
 *Description:   read float in three address and verify
 *input:         u16Address:eeprom address
 *global vars:   void
 *output:        ftemp1: the data that will be write to the eeprom address
 *CALLED BY:    vLimitInit()
 ******************************************************************************/
#pragma CODE_SECTION(fReadFloatDataThree,"FlashBoot");
ubitfloat fReadFloatDataThree(unsigned int u16Address)
{
    ubitfloat ubitfTmp,ftemp1,ftemp2,ftemp3;

    g_u16ReadEepromOk = READ_E2PROM_SUCCESS;
    ftemp1.lData = 0x1111;
    ftemp2.lData = 0x2222;
    ftemp3.lData = 0x3333;

    WatchDogKickPointer();

    //DELAY_US(TIME5MS_CPUCLK);             // delay 5ms

    if (uiI2caReadDataPointer(u16Address) == I2C_SUCCESS)
    {
        ftemp1.lData = g_fRdTemp.lData;
    }

    if (uiI2caReadDataPointer(u16Address + 256) == I2C_SUCCESS)
    {
        ftemp2.lData = g_fRdTemp.lData;
    }

    if (uiI2caReadDataPointer(u16Address + 512) == I2C_SUCCESS)
    {
        ftemp3.lData = g_fRdTemp.lData;
    }

    if ((ftemp1.lData == ftemp2.lData) && (ftemp1.lData == ftemp3.lData))
    {
        return ftemp1;
    }
    else if ((ftemp1.lData == ftemp2.lData) && (ftemp1.lData != ftemp3.lData))
    {
        ubitfTmp.lData = ftemp1.lData;

        if (uiI2caWriteDataPointer(u16Address + 512, ubitfTmp) != I2C_SUCCESS)
        {
            g_u16ReadEepromOk = READ_E2PROM_FAIL;
        }

        DELAY_US(TIME5MS_CPUCLK);                   // delay 5ms
        //WatchDogKickPointer();//lzy 20200227
        return ftemp1;
    }
    else if ((ftemp1.lData != ftemp2.lData) && (ftemp2.lData == ftemp3.lData))
    {
        ubitfTmp.lData = ftemp2.lData;

        if (uiI2caWriteDataPointer(u16Address, ubitfTmp) != I2C_SUCCESS)
        {
            g_u16ReadEepromOk = READ_E2PROM_FAIL;
        }

        DELAY_US(TIME5MS_CPUCLK);                   // delay 5ms
        //WatchDogKickPointer();//lzy 20200227
        return ftemp2;
    }
    else if ((ftemp1.lData == ftemp3.lData) && (ftemp1.lData != ftemp2.lData))
    {
        ubitfTmp.lData = ftemp1.lData;

        if (uiI2caWriteDataPointer(u16Address + 256, ubitfTmp) != I2C_SUCCESS)
        {
            g_u16ReadEepromOk = READ_E2PROM_FAIL;
        }

        DELAY_US(TIME5MS_CPUCLK);                   // delay 5ms
        //WatchDogKickPointer();//lzy 20200227
        return ftemp1;
    }
    else
    {
        g_u16ReadEepromOk = READ_E2PROM_FAIL;
        return ftemp1;
    }

}



