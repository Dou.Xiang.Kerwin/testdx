/*===========================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : gbb_AdcTreatment.c
 *  PURPOSE  : analog sample, filter,calculation       
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A000           DSP               Created.   Pre-research
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *    see variables definition for detail information     
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *    see functions declare for detail information 
 *   
 *==========================================================================*/

#include "f28004x_device.h"		// DSP280x Headerfile Include File
#include  "App_vAdcTreatment.h"
#include  "App_warnctrl.h"
#include  "App_cal.h"
#include "Prj_macro.h"
#include <App_cancom.h>
#include <App_isr.h>
#include "GBB_constant.h"
#include <App_constant.h>
#include <App_main.h>
#include "IQmathLib.h"
#include  "App_vEventLog.h"
#include  "Prj_config.h"


/*****************************************************************************
*variables definition:these variables only can use in this file 														  
*****************************************************************************/
static  INT32 s_ai32AdcDataAcMax[31];  // adc data buffer for ac max volt 
static INT32 s_i32AdcSumAcMax;        // sum of the last several adc data of
                                      // ac max volt
static INT32 s_ai32AdcDataAcRms[31];  // adc data buffer for ac rms volt
static INT32 s_i32AdcSumAcRms;        // sum of the last several adc data of
                                      // ac rms volt
//static INT32 s_ai32DataPfcVolt[31];   // adc data buffer for pfc volt
//static INT32 s_i32SumPfcVolt;         //sum of the  pcf volt

static ubitfloat s_lq10AdcAvePfcVolt;   // adc data buffer for pfc volt
static ubitfloat s_lq10AdcSumPfcVolt;         //sum of the  pcf volt 


//static ubitfloat s_lq10AdcSumAcCur;// adc data buffer forAC Cur
//static ubitfloat s_lq10AdcAveAcCur;//sum of the  AC Cur


                             
static INT32 s_ai32AdcDataDcVolt[31]; // adc data buffer for DC volt 
static INT32 s_i32AdcSumDcVolt;		  // sum of the last several adc data of dc volt
                                       
//static INT32 s_ai32AdcDataDcCurr[31]; // adc data buffer for dc curr
//static INT32 s_i32AdcSumDcCurr;	// sum of the last several adc data of dc curr
 
   
static ubitfloat s_lq10AdcAveDcCurr; // adc data buffer for dc curr
static ubitfloat s_lq10AdcSumDcCurr; // sum of the last several adc data of dc curr
                                 
//static INT32 s_ai32AdcDataTempM[31];  // adc data buffer for M1 board temp
static ubitfloat s_lq10AdcSumTempM;	      // sum of the last several adc data of
static ubitfloat s_lq10AdcAveTempM;	      // ave of the last several adc data of
//static INT32 s_ai32AdcDataTempPFC[31];  // adc data buffer for M1 board temp
//static ubitfloat s_lq10AdcSumTempPFC;	      // sum of the last several adc data of
//static ubitfloat s_lq10AdcAveTempPFC;	      // ave of the last several adc data of
                                      // M1 board temp
//static INT32 s_ai32AdcDataTempU[31];  // adc data buffer for U1 board temp 
static ubitfloat s_lq10AdcSumTempU;	 	  // sum of the last several adc data of
static ubitfloat s_lq10AdcAveTempU;	 	  // ave of the last several adc data of
                                      // U1 board temp
//static INT32 s_ai32AcMaxPointVal[3];
static UINT16 s_u16AdcPoint;          //the point for ADC cal
static UINT16 s_u16AdcPoint2;
static UINT16 s_u16AdcAveFlag;

static ubitfloat    s_lTmpFre1;        //for frequency cal
static ubitfloat    s_lTmpFre2;        //for frequency cal
//static ubitfloat    s_lq10AdcdspAvrTemp;//for DSP 内部温度计算
//static ubitfloat    s_lq10AdcdspSumTemp;//for DSP 内部温度计算
//static ubitfloat    s_lq10DSPTemp;
//static int16 s_i16temp; //raw temperature sensor reading
//static int16 s_i16degC; //temperature in deg. C
//static int16 s_i16degK; //temperature in deg. K
//static UINT16 s_u16Peak_Rms_DelayTimer;
//static ubitfloat  s_lVacPeakBeforeDrop;
//static ubitfloat  s_lVacPeakAfterDrop;
//static ubitfloat  s_lVacPeakAfterJump;

/*****************************************************************************
*globle variables definition:these variables  can use in all of the files 														  
*****************************************************************************/
ubitfloat g_lq10AcVolt;         //actual rectifir acvolt
ubitfloat g_lq10AcCur;         //actual rectifir ac cur
ubitfloat g_lq10AcPow;         //actual rectifir ac cur

ubitfloat g_lq10VinwaveSinDegree;
ubitfloat g_lq10ACPeak_protect;  //AC peak value for protect
ubitfloat g_lq10ACPeak_CalVpfc;
UINT16 g_u16OverRelayTimer;      // over relay connect status timer
ubitfloat g_lq10PfcVolt;        //pfc volt for protect
ubitfloat g_lq10PfcVoltDisp;    //pfc volt for display
ubitfloat g_lq10MdlVoltNoFilter;// actual rectifir dcvolt without filter
//ubitfloat g_lq10MdlVolt;		// Display and control rectifir dcvolt
ubitfloat g_lq10MdlCurrNoFilter;// actual rectifir current without filter
ubitfloat g_lq10MdlCurr;    	// actual rectifir current with filter
ubitfloat g_lq10MdlCurrDisp;	// rectifir current for display
ubitfloat g_lq10MdlCurrDisp2;   // rectifir current for monitor display
ubitfloat g_lq10MdlCurrDisp3;   // rectifir current for monitor display
ubitfloat g_lq10MdlPow;		    // rectifier power of cal
ubitfloat g_lq10TempAmbi;		// rectifier intake temperature
ubitfloat g_lq10TempAmbiDisp;	// rectifier intake temperature for display
ubitfloat g_lq10TempAmbiCtrl;  // rectifier intake temperature for control

ubitfloat g_lq10TempDc;		    // rectifier internal temperature
ubitfloat g_lq10TempDcDisp;	    // rectifier internal temperature for display
ubitfloat g_lq10TempPfc;		// rectifier internal pfc temperature
ubitfloat g_lq10TempPfcDisp;	// rectifier internal pfc temperature for display

ubitfloat g_lq10TempDcDispSave;	
ubitfloat g_lq10TempPfcDispSave;


UINT16 g_u16PermitAdjust;       //adjust flag	
ubitfloat  g_lq10AcFre;       //AC frequency

ubitfloat g_lq10TempSensor;
//INT32     g_ai32AcMaxPointVal[3];
//ubitfloat g_lq10ReadVar2;

ubitfloat s_lq10AdcdspAvrTemp;
ubitfloat s_lq10AdcdspSumTemp;
ubitfloat s_lq10DSPTemp;
//ubitfloat g_lq10TempAmbiDispTest;
//ubitfloat g_lq10TempSensorTest;
/*****************************************************************************
*functions declare:these functions only can use in this file                                    
*****************************************************************************/
//AC volt cal and filter
void vAcVoltCal(void);

//AC Cur cal and filter
void vAcCurCal(void);

//AC power cal 
void vAcPowCal(void);

//pfc volt cal  and adjust
void vPfcVoltCal(void);

//pfc volt filter
void vPfcVoltFilter(void);

//dcdc volt cal and adjust 
void vDcdcVoltCal(void);

//dcdc volt filter
void vDcdcVoltFilter(void);

//dcdc curr cal and adjust 
void vDcdcCurrCal(void);

//dcdc curr filter
void vDcdcCurrFilter(void);

//output power cal
void vOutputPowerCal(void);

//Ambi board temperature cal 
void vAmbiTemperatureCal(void);

//Ambi board temperature filter
void vAmbiTemperatureFilter(void);

//Dc board temperature cal
void vDcTemperatureCal(void);

//Dc board temperature filter
void vDcTemperatureFilter(void);

//PFC board temperature cal
//void vPFCTemperatureCal(void);

//PFC board temperature filter
//void vPFCTemperatureFilter(void);

//AC frequency cal
void vAcFrequencyCal(void);

// DSP内部温度采样
//void vDSPTempSensorConvCal(void);
// DSP内部温度采样
void vDSPTempSensorConvCal(void);

int16 ADC_getTemperatureC(UINT16 tempResult, float32 vref);

/*****************************************************************************
 *FUNCTION: vAdcTreatment  
 *PURPOSE:  ADC input sample, filter, calculation--use for display and protect                                                   
 *INPUT:    g_u16RunFlag.bit.ADC ;  20ms once                                  
 *global vars: g_u16RunFlag
 *RETURN:   void
 *CALLS: 
 *CALLED BY: main()
 *Author:
 *Date:   
 *****************************************************************************/

void vAdcTreatment(void)
{
	if (g_u16RunFlag.bit.ADC) 	 
	{
		// 20mS ad samp & filter calculation flag
		g_u16RunFlag.bit.ADC = 0;
		
	    //AC volt cal and filter
		vAcVoltCal();

        //AC cur cal and filter
		vAcCurCal();
        //AC POW cal
		vAcPowCal();

		//pfc volt cal  and filter 
		vPfcVoltCal();
	    vPfcVoltFilter();

		//Dcdc volt cal and filter
		vDcdcVoltCal();
		vDcdcVoltFilter();

		//dcdc current cal and filter
		vDcdcCurrCal();
		vDcdcCurrFilter();

		//cal output power
		vOutputPowerCal();

		//Ambi board temperature cal and filter
		vAmbiTemperatureCal();
		vAmbiTemperatureFilter();

		//dc board temperature cal and filter
		//vDcTemperatureCal();
		//vDcTemperatureFilter();


		//PFC board temperature cal and filter
		//vPFCTemperatureCal();
		//vPFCTemperatureFilter();

		//AC volt Frequency cal and filter
		vAcFrequencyCal();
		//DSP内部温度采样
		vDSPTempSensorConvCal();
				
		// count add 1, max 30 
		s_u16AdcPoint++;
		if( s_u16AdcPoint >= 30)
		{
			s_u16AdcPoint = 0;
		}
        //留几个周期把瞬时采样的10HZ滤波
		//AD 20ms一次，所以间完?0HZ（100ms）滤波
		if(s_u16AdcPoint >= 6)
		{
		    s_u16AdcAveFlag=1;
		}

		//第一次上电等待ADCtreatment变量稳定
		g_u16CpuStartTimer ++;
		if(g_u16CpuStartTimer > 50)
		{
			g_u16CpuStartTimer = 50;
		}
	}
}

/*****************************************************************************
 *FUNCTION: vAcVoltCal  
 *PURPOSE:  ac volt cal and filter                                                    
 *INPUT:    
 *global vars:  
 *RETURN:   
 *CALLS: 
 *CALLED BY: vAdcTreatment()
 *Author:
 *Date:                                
 ****************************************************************************/
void vAcVoltCal(void)
{
  
	ubitfloat	lCalTemp,lTmp,lTmp2;
	//ubitfloat   lVacChangeVal1, lVacChangeVal2;

	//过压继电器吸合后启动计时器（不吸合会被不停复位），20ms跳一次
	g_u16OverRelayTimer ++;
	if((g_u16OverRelayTimer > 1) && (mOverRelayStatus_disconnect))
	{
	    g_u16OverRelayTimer = 0;
	}

	//大约3S的延时20ms*150=3S，这个3S设定是PFC母线电容充电时间
	//vi32MaxMinLimit(g_u16OverRelayTimer,TIME3S_20MS_BASE,g_u16OverRelayTimer);
    if(g_u16OverRelayTimer >=TIME3S_20MS_BASE)
    {
        //大约3S的延时20ms*150=3S，这个3S设定是PFC母线电容充电时间
        g_u16OverRelayTimer = TIME3S_20MS_BASE;
    }

	if(mOverRelayStatus_connect)
	{
        s_ai32AdcDataAcMax[30] = (INT32)IsrVars._i16AdAcMax;
        s_i32AdcSumAcMax += s_ai32AdcDataAcMax[30]
                        - s_ai32AdcDataAcMax[s_u16AdcPoint2];
        s_ai32AdcDataAcMax[s_u16AdcPoint2] = s_ai32AdcDataAcMax[30];
        IsrVars._i16AdAcMax = 0;
        s_u16AdcPoint2 ++;
	}
	else if(g_u16OverRelayTimer == 1)
	{
        s_ai32AdcDataAcMax[30] = (INT32)IsrVars._i16AdAcMax;
        s_i32AdcSumAcMax += s_ai32AdcDataAcMax[30]
                        - s_ai32AdcDataAcMax[s_u16AdcPoint2];
        s_ai32AdcDataAcMax[s_u16AdcPoint2] = s_ai32AdcDataAcMax[30];
        IsrVars._i16AdAcMax = 0;
        s_u16AdcPoint2 ++;
	}

	if(s_u16AdcPoint2 >= 30)
	{
	    s_u16AdcPoint2 = 0;
	}

	//lCal_sqare_Temp.lData= s_ai32AdcDataAcMax[s_u16AdcPoint];//不作30点平均
    
    //      Vac输入跳变判断/*输入跳变判断*/
    //-----------------------------------------------------------------------------
    /*g_ai32AcMaxPointVal[2] = g_ai32AcMaxPointVal[1];
    g_ai32AcMaxPointVal[1] = g_ai32AcMaxPointVal[0];
    g_ai32AcMaxPointVal[0] = (INT32)IsrVars._u16AdAcMaxPowLim;//清零在后边
    //虽然起名PowLim，但暂时没有和限功率挂钩，后续再说
    lVacChangeVal2.lData = g_ai32AcMaxPointVal[2] - g_ai32AcMaxPointVal[0];
    lVacChangeVal1.lData = g_ai32AcMaxPointVal[1] - g_ai32AcMaxPointVal[0];

    
    if(( lVacChangeVal1.lData >= VAC_AD_PEAK_50V )
       &&( lVacChangeVal2.lData >= VAC_AD_PEAK_50V ) )//大约35rms
    {
        IsrVars._i16AcDropFlag = 250;//250*20ms=5s一旦进入Drop模式,至少5s才可退出
        s_lVacPeakBeforeDrop.lData = g_ai32AcMaxPointVal[2];
        s_lVacPeakAfterDrop.lData = g_ai32AcMaxPointVal[0];
    }
    else if((lVacChangeVal1.lData <= NEG_VAC_PEAK_50V)
            &&(lVacChangeVal2.lData <= NEG_VAC_PEAK_50V))
    {
        IsrVars._i16AcUpjumpFlag = 250;
        s_lVacPeakAfterJump.lData = g_ai32AcMaxPointVal[0];
    }
    else
    {
        IsrVars._i16AcDropFlag -- ;
        IsrVars._i16AcUpjumpFlag --;//一样的，进到Jump模式也是5s退出
    }

    if(IsrVars._i16AcDropFlag <= 1 )
    {
        IsrVars._i16AcDropFlag = 1;
    }
    if(IsrVars._i16AcUpjumpFlag <= 1 )
    {
        IsrVars._i16AcUpjumpFlag = 1;
    }*/
    //-----------------------------------------------------------------------------
    //单次Vac峰值校准(使用Vac 1Khz滤波),不作30点平均折算有效值，用于动态时1/ACrms2
    //注意：IsrVars._u16AdAcMaxPowLim是cnt，不用进行AD系数计算
    //-----------------------------------------------------------------------------
    /*lCal_sqare_Temp.lData= (INT32)IsrVars._u16AdAcMaxPowLim;
    lCal_sqare_Temp.lData = _IQ10div(lCal_sqare_Temp.lData<<10, IQ10_DEF_VACFACTOR );
    //峰值校准
    lCal_sqare_Temp.lData = _IQ10mpy(lCal_sqare_Temp.lData, g_lq10AcVrmsSampSysa.lData)
                              + g_lq10AcVrmsSampSysb.lData ;
    //如果输入是交流电,用峰值除1.414可折算有效值;若输入是直流电,峰值=有效值
    if(g_u16MdlStatusExtend.bit.DCINPUTSTATUS == 0)
    {
        lCal_sqare_Temp.lData = _IQ10div(lCal_sqare_Temp.lData,SQRT2);      //1.414
    }*/
    //g_lq10AcPeakSingle.lData = lCal_sqare_Temp.lData;//G4涉及但双相切换，这里先不用
    //-----------------------------------------------------------------------------
    //      从Vac峰值的30次滑动滤波值计算Vac有效值
    //-----------------------------------------------------------------------------
    //IsrVars._u16AdAcMaxPowLim = 0;  //每个计算周期20ms把中断里的峰值清一次0，
    //IsrVars._i16AdAcMax = 0;  //每个计算周期20ms把中断里的峰值清一次0，

    //calculation from AC Max value
    lCalTemp.lData = _IQ10div(s_i32AdcSumAcMax<<10, IQ10_DEF_VACFACTOR * (INT32)30);
    //峰值校准
    lCalTemp.lData = _IQ10mpy(lCalTemp.lData,g_lq10AcVrmsSampSysa.lData)
                 + g_lq10AcVrmsSampSysb.lData ;
    g_lq10ACPeak_protect.lData = lCalTemp.lData; //输入峰值（这是校准后30次滑动的峰值）
    //-----------------------------------------------------------------------------
    //使用AC峰值作为跳变时的快速限功率及母线电压的设定使用量
    //-----------------------------------------------------------------------------
    /*if(IsrVars._i16AcDropFlag > 1)//母线调压要按照跳变的高压段去调，除非退出跳变5s。
    {
        g_lq10ACPeak_CalVpfc.lData = _IQ10mpy(s_lVacPeakBeforeDrop.lData<<10, g_lq10AcVrmsSampSysa.lData)
                                            + g_lq10AcVrmsSampSysb.lData ;
    }
    else if(IsrVars._i16AcUpjumpFlag > 1)
    {
        g_lq10ACPeak_CalVpfc.lData = _IQ10mpy(s_lVacPeakAfterJump.lData<<10, g_lq10AcVrmsSampSysa.lData)
                                            + g_lq10AcVrmsSampSysb.lData ;
    }
    else
    {
        g_lq10ACPeak_CalVpfc.lData = g_lq10ACPeak_protect.lData;
        //g_lq10ACPeak_LimPow.lData = g_lq10ACPeak_protect.lData;暂时不调试
    }   //g_lq10ACPeak_CalVpfc作为最终调压的依据。
    */
    //g_u16VoltGetTest = (Uint16) (g_lq10ACPeak_CalVpfc.lData  >> 10);
    //如果输入是交流电,用峰值除1.414可折算有效值
    //如果输入是直流电,峰值等于有效值
    if(g_u16MdlStatusExtend.bit.DCINPUTSTATUS == 0)
    {
        lCalTemp.lData = _IQ10div(lCalTemp.lData,SQRT2);    //1.414这个是30次滑动滤波的值
        //g_lq10ACRms_LimPow.lData = _IQ10div(g_lq10ACPeak_LimPow.lData,SQRT2);   //1.414
    }
    //else
    //{
    //   g_lq10ACRms_LimPow.lData = g_lq10ACPeak_LimPow.lData;
    //}
    //-----------------------------------------------------------------------------
    //      真有效值计算
    //-----------------------------------------------------------------------------
    //有效值做30点平均计算
    s_ai32AdcDataAcRms[30] = (INT32)IsrVars._lAcVoltSquareRms.lData;
    if( s_ai32AdcDataAcRms[30] > VAC_SquareRms_500V)
    {
        s_ai32AdcDataAcRms[30] =VAC_SquareRms_500V;
    }
    s_i32AdcSumAcRms += (INT32)s_ai32AdcDataAcRms[30]-(INT32)s_ai32AdcDataAcRms[s_u16AdcPoint];
    s_ai32AdcDataAcRms[s_u16AdcPoint] = (INT32)s_ai32AdcDataAcRms[30];
    //Vac显示计算,因为是Q24的累加，所以先除40，再转换为Q6
    lTmp.lData = s_i32AdcSumAcRms/30 ;
    lTmp.lData = lTmp.lData<<6; //IQ6,max -- Q24+6
    lTmp.lData = _IQ6sqrt(lTmp.lData);
    lTmp.lData = _IQ10div(lTmp.lData<<4,IQ10_DEF_VACFACTOR);
    //真有效值校准
    lTmp.lData = _IQ10mpy(lTmp.lData,g_lq10AcVrmsSampSysa.lData) 
                 + g_lq10AcVrmsSampSysb.lData ;
    //过压脱离及吸合后的1S以内都不准，简单起见不用峰值折算此时RMS，直接延时不用
    //辅助继电器接合3S内，要用峰值计算AC电压,有效值此时不准
    if((g_u16OverRelayTimer < TIME3S_20MS_BASE) || (g_u16ActionReady < DC_SOFT_START))
    {
        lTmp.lData = lCalTemp.lData ;
    }
//---------------用峰值与有效值的比例来区分油机等正弦度不好的波形---------------------//
    //根据峰值和真有效值来区分波形正弦度，尤其是油机等需要一些特殊策略。
    if( (g_u16OverRelayTimer >= TIME3S_20MS_BASE)
      && (g_u16ActionReady >= DC_SOFT_START_INIT) )
    {
        lTmp2.lData = lTmp.lData;
        //下限，不能用0做分母
        lTmp2.lData= (lTmp2.lData < VAC_DIS_DN) ? VAC_DIS_DN : lTmp2.lData;
        g_lq10VinwaveSinDegree.lData  = _IQ10div(g_lq10ACPeak_protect.lData , lTmp2.lData);
    }
    else
    {
        g_lq10VinwaveSinDegree.lData  = SQRT2; //正弦交流输入
    }
    if(g_lq10VinwaveSinDegree.lData > IQ10_1_59 )//峰值/有效值>1.59=350/220，认为高峰值波形
    {
        IsrVars._i16VinHighPeakFlag = 1;
    }
    else if(g_lq10VinwaveSinDegree.lData < IQ10_1_5)//做一个回差
    {
        IsrVars._i16VinHighPeakFlag = 0;
    }
//------用峰值与有效值的比例来区分油机等正弦度不好的波形-----END-----------//
    //-----------------------------------------------------------------------------
    //      Vac有效值显示处理 --- 使用Vac真有效值
    //--------------------------------------------------------------
    //Volt*0.95+Tmp*0.025//加权滤波
    if(labs(g_lq10AcVolt.lData - lTmp.lData) < VAC_10V)
    {
        lTmp.lData = (g_lq10AcVolt.lData * IQ12_ADC_VAC_FILTA
                      + lTmp.lData * IQ12_ADC_VAC_FILTB) >> 12;
    }
    g_lq10AcVolt.lData = (INT32)lTmp.lData;

    if(g_u16MdlStatusExtend.bit.DCINPUTSTATUS == 0)//交流输入
    {
        if(g_lq10AcVolt.lData <= VAC_DIS_DN)//50v
        {
            g_lq10AcVolt.lData = VAC_DIS_CLR;
        }
    }
    else
    {
        if(g_lq10AcVolt.lData <= VDCIN_DIS_DN)//72v
        {
            g_lq10AcVolt.lData = VAC_DIS_CLR;
        }
    }
    //10*Q25-->1024*1024*10*2^5=335544320
    // 335544320/（80*80）=52429 ，80V以下按80V计算
    //if((IsrVars._u16AcDropFlag <= 1) && (IsrVars._u16AcUpjumpFlag <= 1))
    //{
        if(g_lq10AcVolt.lData < VAC_80V)
        {
            IsrVars._lVacRmsSqrInv.lData = VAC_RMS2_INV_MIN; //max = <3FFFH
        }
        else
        {
           lTmp.lData = _IQ10mpy(g_lq10AcVolt.lData,g_lq10AcVolt.lData)>>10;
           IsrVars._lVacRmsSqrInv.lData = VAC_RMS2_INV_CON / lTmp.lData;
        }
    //}
    /*else//Dynamic Input
    {
        if(lCal_sqare_Temp.lData < VAC_80V)
        {
            IsrVars._lVacRmsSqrInv.lData = VAC_RMS2_INV_MIN; //max = <3FFFH
        }
        else
        {
            lTmp.lData = _IQ10mpy(lCal_sqare_Temp.lData,lCal_sqare_Temp.lData)>>10;
            IsrVars._lVacRmsSqrInv.lData = VAC_RMS2_INV_CON / lTmp.lData;
        }
    }*/
}

/*****************************************************************************
 *FUNCTION: vAcCurCal  
 *PURPOSE:  ac Cur cal and filter                                                    
 *INPUT:   
 *global vars: 
 *RETURN:   
 *CALLS: 
 *CALLED BY: vAdcTreatment()
 *Author:
 *Date:                                
 ****************************************************************************/
void vAcCurCal(void)
{
  
    //ubitfloat lTmp;
    ubitfloat lq10Temp;
    if(g_u16ActionReady == NORMAL_RUN_STATE)
    {
        lq10Temp.lData = _IQ10div(g_lq10AcPow.lData,g_lq10AcVolt.lData);
    }
    else
    {
        lq10Temp.lData = 0;  //g_lq10AcCur
    }

    if( lq10Temp.lData > IPFC_UP )
    {
        lq10Temp.lData = IPFC_UP;
    }
    else if( lq10Temp.lData <= 0 )
    {
        lq10Temp.lData = 0;
    }

    if((g_u16MdlStatus.bit.OFFSTAT)&&(g_u16PermitAdjust == NORMAL_RUN_MODE))
    {
        lq10Temp.lData = 0;
    }

    g_lq10AcCur.lData= lq10Temp.lData;

}


/*****************************************************************************
 *FUNCTION: 	vAcPowCal 
 *PURPOSE:  ac POW cal and filter                                                    
 *INPUT:   
 *global vars: 
 *RETURN:   
 *CALLS: 
 *CALLED BY: vAdcTreatment()
 *Author:
 *Date:                                
 ****************************************************************************/
void vAcPowCal(void)
{
  //g_lq10AcPow.lData = _IQ10mpy(g_lq10AcVolt.lData,g_lq10AcCur.lData);
    g_lq10AcPow.lData = _IQ10mpy(g_lq10MdlPow.lData,ACCURRENT_IVTEFF);
}


/*****************************************************************************
 *FUNCTION: vPfcVoltCal()  
 *PURPOSE:  pfc volt cal and adjust                                                    
 *INPUT:    void
 *global vars:   IsrVars._lVpfcProtect:pfc volt sample 1v -> 8.493343cnt
 *               iq10PfcVolt:  pfc volt for protect 1V -> 1024cnt              
 *RETURN:       void
 *CALLS:  
 *CALLED BY:    vAdcTreatment()
 *Author:
 *Date: 
 ****************************************************************************/	
void vPfcVoltCal(void)
{
	ubitfloat	lq10Temp;

	//pfc volt sample
	lq10Temp.lData = (INT32)IsrVars._lVpfcProtect.iData.iHD << 10;			
	
	//PFC volt cal 1v -> 1024cnt(Q10)
	//(VSamp/lq10VpfcFactor)*lq10PfcSampSys
	lq10Temp.lData = _IQ10div(lq10Temp.lData ,IQ10_DEF_VPFCFACTOR);

	//pfc  volt adjust
	g_lq10PfcVolt.lData = _IQ10mpy(lq10Temp.lData,g_lq10VpfcSampSys.lData);	   
}

/*****************************************************************************
 *FUNCTION: vPfcVoltFilter()  
 *PURPOSE:  pfc volt filter                                                     
 *INPUT:    void 
 *global vars:   g_lq10PfcVolt: pfc volt for protect,1V -> 1024cnt
 *               g_iq10PfcVoltDisp:  pfc volt for display,1V -> 1024cnt              
 *RETURN:        void
 *CALLS:
 *CALLED BY:     vAdcTreatment()
 *Author:
 *Date:                
 ****************************************************************************/
void vPfcVoltFilter(void)
{
//	ubitfloat	lq10Temp;
/*
	//cal pfc volt sum for filter,1v -> 1024cnt(Q10)
	s_ai32DataPfcVolt[30] = g_lq10PfcVolt.lData;			
	s_i32SumPfcVolt += s_ai32DataPfcVolt[30] - s_ai32DataPfcVolt[s_u16AdcPoint];
    s_ai32DataPfcVolt[s_u16AdcPoint] = s_ai32DataPfcVolt[30];

	//cal pfc volt ave	
	lq10Temp.lData = _IQ10div(s_i32SumPfcVolt,(INT32)30 << 10);

*/
    if(s_u16AdcAveFlag == 0 )
    {
      s_lq10AdcAvePfcVolt.lData = g_lq10PfcVolt.lData ;
	  s_lq10AdcSumPfcVolt.lData = g_lq10PfcVolt.lData<<5;
	}
    else
    {
	  s_lq10AdcSumPfcVolt.lData = s_lq10AdcSumPfcVolt.lData - s_lq10AdcAvePfcVolt.lData
	                            + g_lq10PfcVolt.lData;

     s_lq10AdcAvePfcVolt.lData = s_lq10AdcSumPfcVolt.lData>>5;
    }



    //if difference of fore-and-aft pfc volt in 0.3v,
    //filter with Volt*0.95+Tmp*0.05		
	if (labs(s_lq10AdcAvePfcVolt.lData - g_lq10PfcVoltDisp.lData) < VPFC_0V3)
	{
		s_lq10AdcAvePfcVolt.lData = _IQ10mpy(g_lq10PfcVoltDisp.lData,IQ10_ADC_VPFC_FILTA) 
		                 + _IQ10mpy(s_lq10AdcAvePfcVolt.lData,IQ10_ADC_VPFC_FILTB);
	}
	
	//limit display pfc volt
	if (s_lq10AdcAvePfcVolt.lData > VPFC_DIS_UP)
	{
		g_lq10PfcVoltDisp.lData = VPFC_DIS_UP;
	}
	else if (s_lq10AdcAvePfcVolt.lData < VPFC_DIS_DN)
	{
		g_lq10PfcVoltDisp.lData = VPFC_DIS_DN;
	}
	else
	{
		g_lq10PfcVoltDisp.lData = s_lq10AdcAvePfcVolt.lData;
	}
}

/*****************************************************************************
 *FUNCTION: vDcdcVoltCal()  
 *PURPOSE:  dcdc volt cal and adjust                                                     
 *INPUT:    void                                 
 *global vars:   IsrVars._lVdcDisUse: dcdc volt sample,1v -> 497.04cnt
 *               g_iq10MdlVoltNoFilter:dcdc volt without filter,1V -> 1024cnt              
 *RETURN:   void
 *CALLS: 
 *CALLED BY:     vAdcTreatment()
 *Author:
 *Date:               
 ****************************************************************************/
void vDcdcVoltCal(void)
{
	ubitfloat	lq10Temp;

	//dcdc volt sample
	lq10Temp.lData = (INT32)IsrVars._lVdcDisUse.iData.iHD << 10;

	//dcdc volt cal 1v -> 1024cnt(Q10)
	lq10Temp.lData = _IQ10div(lq10Temp.lData,IQ10_DEF_VDCFACTOR);

	//dcdc  volt adjust
	g_lq10MdlVoltNoFilter.lData = 
						   (_IQ10mpy(lq10Temp.lData,g_lq12VoltSampSysa.lData) 
	                       >> 2) + (g_lq12VoltSampSysb.lData >> 2);
}

/*****************************************************************************
 *FUNCTION: vDcdcVoltFilter()  
 *PURPOSE :  dcdc volt filter for display and control                                                    
 *INPUT:     void                                 
 *global vars:   g_iq10MdlVoltNoFilter: dcdc volt without filter,1V -> 1024cnt 
 *               g_iq10MdlVolt: dcdc volt for display/ control,1V -> 1024cnt             
 *RETURN:    void
 *CALLS:
 *CALLED BY:     vAdcTreatment()
 *Author:
 *Date:                 
 ****************************************************************************/	
void vDcdcVoltFilter(void)
{
	ubitfloat	lq10Temp,lCalTemp;

	//cal dcdc volt sum for filter ,1v -> 1024cnt(Q10)
	s_ai32AdcDataDcVolt[30] = g_lq10MdlVoltNoFilter.lData;
	s_i32AdcSumDcVolt += s_ai32AdcDataDcVolt[30] 
	                     - s_ai32AdcDataDcVolt[s_u16AdcPoint];
	s_ai32AdcDataDcVolt[s_u16AdcPoint] = s_ai32AdcDataDcVolt[30];

	//cal dcdc volt ave	
	lq10Temp.lData = _IQ10div(s_i32AdcSumDcVolt,(INT32)30 << 10);	
	
	//(volt+(1.0-current/(INT32)RatedCurrent/2)*0.045
	//*IsrVars._uiPermitAdjust)*a+b
	//负载调整率原因显示补偿
	lCalTemp.lData = _IQ10div(g_lq10MdlCurr.lData,(g_i32CurrRate >> 1));
	lCalTemp.lData = 1024 - lCalTemp.lData;
	lq10Temp.lData += _IQ10mpy(lCalTemp.lData,IQ10_VDISP_IADJ) 
	                  * (INT32)g_u16PermitAdjust;
	
	//if difference of fore-and-aft Dcdc volt in 0.2v,
	//filter with Volt*0.95+Tmp*0.05		
	if(labs(lq10Temp.lData - g_lq10MdlVolt.lData) < VDC_0V2)
	{
		lq10Temp.lData = (g_lq10MdlVolt.lData * IQ14_ADC_VDC_FILTA 
		                  + lq10Temp.lData * IQ14_ADC_VDC_FILTB) >> 14;
	}
	g_lq10MdlVoltHvsd.lData = lq10Temp.lData;

	//limit display Dcdc volt
	if (lq10Temp.lData > VDC_DIS_UP)
	{
		g_lq10MdlVolt.lData = VDC_DIS_UP;
	}
	else if (lq10Temp.lData < VDC_DIS_DN)
	{
		g_lq10MdlVolt.lData = VDC_DIS_DN;
	}
	else
	{
		g_lq10MdlVolt.lData = lq10Temp.lData;
	}
}

/*****************************************************************************
 *FUNCTION: vDcdcCurrCal()  
 *PURPOSE:  dcdc current cal and adjust                                                     
 *INPUT:         void                                
 *global vars:   IsrVars._lIdcDisUse: dcdc current sample,1A -> 430.713cnt 
 *               g_iq10MdlCurrNoFilter:current without filter,1A -> 1024cnt              
 *RETURN:        void
 *CALLS: 
 *CALLED BY:     vAdcTreatment()
 *Author:
 *Date:               
 ****************************************************************************/
void vDcdcCurrCal(void)
{
    //ubitfloat iq10Temp,lCalTemp;
    ubitfloat   lq10Temp;
    ubitfloat   lq10Temp_compen,lq10Temp_compen2;
    ubitfloat  lq10Temp_PfcMdlVolt;   // PFCVolt/MdlVolt

    //dcdc current sampley/
    lq10Temp.lData = (INT32)IsrVars._lIdcDisUse.iData.iHD << 10;

    //dcdc current cal,1A -> 1024cnt(Q10)
    //原方电流转换为副方电流,(Isample/lq10IdcFactor)*Vpfc_set/MdlVolt
    lq10Temp.lData = _IQ10div(lq10Temp.lData,IQ10_DEF_IDCFACTOR);
    //DC电流硬件采样比//(0.010mohm*10K/0.56k)*4095/3.184=229.66394*Q10=235176
    lq10Temp.lData = _IQ10mpy(lq10Temp.lData,g_lq10PfcVolt.lData);
    lq10Temp.lData = _IQ10div(lq10Temp.lData,g_lq10MdlVoltNoFilter.lData);
    //dcdc curr adjust
    g_lq10MdlCurrNoFilter.lData = _IQ10mpy(lq10Temp.lData,g_lq10CurrSampSysa.lData)
                                    + g_lq10CurrSampSysb.lData;

//g_lq10MdlCurrNoFilter：897a；量纲为真实副边输出电流*1024

   //compensation all voltage range, chenli 20200515
    lq10Temp_PfcMdlVolt.lData = _IQ10div(g_lq10SetPfcVoltTrue.lData, g_lq10MdlVolt.lData);
    //lq10Temp_PfcMdlVolt_test = lq10Temp_PfcMdlVolt.lData;
        if( lq10Temp_PfcMdlVolt.lData > 7987)   // VPfc/Vo>7.8, 7.8 Q10 7987
        {

           if((g_lq10MdlCurr.lData < IDC_40A)&&(lq10Temp_PfcMdlVolt.lData < 9421))   // Vpfc/Vo<9.2, 9.2 Q10 9421
            {
               if((g_lq10MdlCurr.lData < IDC_11A)&&(g_lq10AcVolt.lData>VAC_280V))
               {

                   lq10Temp_compen.lData = lq10Temp_PfcMdlVolt.lData - 7987;
                   lq10Temp_compen.lData = _IQ10mpy(lq10Temp_compen.lData, (INT32)1392);

               }
               else
               {
                  if(lq10Temp_PfcMdlVolt.lData>9753)
                   {
                       lq10Temp_compen.lData = lq10Temp_PfcMdlVolt.lData - (INT32)9753 ;
                       
                   }
                   else
                   {
                   // Vpfc/Vo=9.2 compensation 1.9A, Vpfc/Vo=7.8-8.1 compensation 0A
                   // so 1.9*(Vpfc/Vo-8.1)/1.1=1.73*(Vpfc/Vo-8.1),1.73 Q10 1771
                      lq10Temp_compen.lData = lq10Temp_PfcMdlVolt.lData - 7987;
                      lq10Temp_compen.lData = _IQ10mpy(lq10Temp_compen.lData, (INT32)1393);
                   }
               }

            }
            else if((g_lq10MdlCurr.lData > IDC_40A)&&(lq10Temp_PfcMdlVolt.lData < 9421))
            {
                // Vpfc/Vo=9.2 compensation 1.9A, Vpfc/Vo=7.8 compensation 0A
                // so 1.9*(Vpfc/Vo-7.8)/1.4=1.36*(Vpfc/Vo-7.8),1.36 Q10 1393
                lq10Temp_compen.lData = lq10Temp_PfcMdlVolt.lData - 7987;
                lq10Temp_compen.lData = _IQ10mpy(lq10Temp_compen.lData, (INT32)1393);
            }
        else if((g_lq10MdlCurr.lData < IDC_29A)&&(lq10Temp_PfcMdlVolt.lData > 9421))   // Vpfc/Vo>9.2
        {
            // Vpfc/Vo=10 compensation 3.9A, Vpfc/Vo=9.2 compensation 1.9A
            // so 2*(Vpfc/Vo-9.2)/0.8+1.9=2.5*(Vpfc/Vo-9.2)+1.9, 2.5 Q10 2560, 1.9 Q10 1946
            lq10Temp_compen.lData = lq10Temp_PfcMdlVolt.lData - 9421;
            lq10Temp_compen.lData = _IQ10mpy(lq10Temp_compen.lData, (INT32)2560) + 1946;
        }
        else    // Vpfc/Vo<7.8||(g_lq10MdlCurr.lData>29A&&Vpfc/Vo>9.2)
        {
            // Vpfc/Vo=10 compensation 2.5A, Vpfc/Vo=9.2 compensation 1.9A
            // so 0.6*(Vpfc/Vo-9.2)/0.8+1.9=0.75*(Vpfc/Vo-9.2)+1.9, 0.75 Q10 768, 1.9 Q10 1946
            lq10Temp_compen.lData = lq10Temp_PfcMdlVolt.lData - 9421;
            lq10Temp_compen.lData = _IQ10mpy(lq10Temp_compen.lData, (INT32)768) + 1946;
        }

        // compensation in sections according to the current
        if(g_lq10MdlCurr.lData < IDC_15A)
        {
            if((g_lq10MdlCurr.lData < (INT32)IDC_11A)&&(lq10Temp_PfcMdlVolt.lData < 9421)&&(g_lq10AcVolt.lData>VAC_280V))
            {

              //     lq10Temp_compen2.lData = (INT32)lq10Temp_compenin_test1 - g_lq10MdlCurr.lData;
              //     lq10Temp_compen2.lData = (INT32)lq10Temp_compenin_test2 +_IQ10mpy(lq10Temp_compen2.lData, (INT32)lq10Temp_compenin_test);
                lq10Temp_compen2.lData = 4096;
            }
                //5.17A, compensation 4.7A; 15A, compensation 1.9A; 15-5.17=9.83, Q10 10066
                // 4.7/1.9=2.5, compen2=1+1.5*(15-Io)/9.83, 1.5 Q10 1536
            else if((g_lq10MdlVolt.lData > VDC_47V5)&&(g_lq10MdlVolt.lData < VDC_48V5)&&(g_lq10MdlCurr.lData < IDC_10A5))
            {
                lq10Temp_compen.lData =1024;
                //lq10Temp_compen2.lData = IDC_10A5 - g_lq10MdlCurr.lData;
                 // lq10Temp_compen2.lData = _IQ10div(lq10Temp_compen2.lData, (INT32)10066);
                 lq10Temp_compen2.lData = 1616;
              //  lq10Temp_compen2.lData = lq10Temp_compenin_test1 + _IQ10mpy(lq10Temp_compen2.lData, (INT32)lq10Temp_compenin_test2);
            }

             else
             {
                 lq10Temp_compen2.lData = IDC_15A - g_lq10MdlCurr.lData;
                // lq10Temp_compen2.lData = _IQ10div(lq10Temp_compen2.lData, (INT32)10066);
                lq10Temp_compen2.lData = 1024 + _IQ10mpy(lq10Temp_compen2.lData, (INT32)572);
                if((g_lq10MdlVolt.lData >=VDC_43V)&&(g_lq10MdlVolt.lData< VDC_47V))
                {
                    lq10Temp_compen2.lData = (INT32)2305;
                }
                else
                {
                    lq10Temp_compen2.lData = lq10Temp_compen2.lData;
                }
             }


        }
        else if((g_lq10MdlCurr.lData > IDC_15A)&&(g_lq10MdlCurr.lData < IDC_40A))
        {
            //compensation 1.9A, compen2=1
            if(g_lq10MdlCurr.lData > IDC_29A)
            {
                if(g_lq10MdlVolt.lData > VDC_47V)
                {
                    lq10Temp_compen2.lData = 3072;
                }
                /*else if((g_lq10MdlVolt.lData > VDC_47V)&&(g_lq10MdlVolt.lData < VDC_49V))
                {
                    lq10Temp_compen2.lData = -50000;
                }*/
                else  if((g_lq10MdlVolt.lData>VDC_41V)&&(g_lq10MdlVolt.lData<VDC_43V))
                {
                    lq10Temp_compen2.lData = 1536;
                }
                else
                {
                    lq10Temp_compen2.lData = -128;
                }
            }
            else
            {
            lq10Temp_compen2.lData = 1024;
            }
        }
        else   // Io>40A
        {
            //62.59A,compensation 7A; 40A, compensation 1.9A; 62.59-40=22.59, Q10 23132
            //7/1.9=3.68, compen2=1+2.68*(Io-40)/22.59, 2.68 Q10 2744
            lq10Temp_compen2.lData = g_lq10MdlCurr.lData - IDC_40A;
            lq10Temp_compen2.lData = _IQ10div(lq10Temp_compen2.lData, (INT32)23132);
            if(g_lq10MdlVolt.lData > VDC_47V)
            {
                if(g_lq10MdlCurr.lData < IDC_68A)
                {
                    if(g_lq10MdlCurr.lData < IDC_48A)
                    {
                        lq10Temp_compen2.lData = 1024 - 4096+ _IQ10mpy(lq10Temp_compen2.lData, (INT32)2744);
                    }
                    else
                    {
                        lq10Temp_compen2.lData = 1024 - 11264+ _IQ10mpy(lq10Temp_compen2.lData, (INT32)2744);
                    }
                }
                else if((g_lq10MdlCurr.lData > IDC_68A) && (g_lq10MdlCurr.lData < IDC_80A))
                {
                    lq10Temp_compen2.lData = 1024 - 2048 + _IQ10mpy(lq10Temp_compen2.lData, (INT32)2744);
                }
                else
                {
                    lq10Temp_compen2.lData = 1024 - 0 + _IQ10mpy(lq10Temp_compen2.lData, (INT32)2744);
                }
            }
            else
            {
                if(g_lq10MdlCurr.lData < IDC_80A)
                {
                    if((g_lq10MdlVolt.lData>VDC_41V)&&(g_lq10MdlVolt.lData<VDC_43V))
                    {
                        if(g_lq10MdlCurr.lData < IDC_60A)
                        {
                            lq10Temp_compen2.lData = 1024 + 512 + _IQ10mpy(lq10Temp_compen2.lData, (INT32)2744);
                        }
                        else if(g_lq10MdlCurr.lData > IDC_76A)
                        {
                            lq10Temp_compen2.lData = 1024 - 2048 + _IQ10mpy(lq10Temp_compen2.lData, (INT32)2744);
                        }
                        else
                        {
                            lq10Temp_compen2.lData = 1024 - 512 + _IQ10mpy(lq10Temp_compen2.lData, (INT32)2744);
                        }
                    }
                    else
                    {
                        lq10Temp_compen2.lData = 1024 - 2048 + _IQ10mpy(lq10Temp_compen2.lData, (INT32)2744);
                    }
                }
                else
                {
                    lq10Temp_compen2.lData = 1024 - 1800 + _IQ10mpy(lq10Temp_compen2.lData, (INT32)2744);
                }
            }
        }
        //calculate compensation
        //lq10Temp_compen_test = lq10Temp_compen2.lData;
        //lq10Temp_compen_test1 = lq10Temp_compen.lData;
       if(g_u16CurrCompenEnable == 1)
       {
          lq10Temp_compen.lData -= 2560;  //2.5*1024=2560
       }

        lq10Temp_compen.lData= _IQ10mpy(lq10Temp_compen2.lData, lq10Temp_compen.lData);
        if((lq10Temp_compen.lData > 6656)&&(g_lq10MdlCurr.lData < IDC_15A))   // 6.5*1024=6656
        {
                lq10Temp_compen.lData = 6656;
        }
        if((g_lq10MdlVolt.lData>VDC_41V)&&(g_lq10MdlVolt.lData<VDC_43V)&&(g_lq10MdlCurr.lData < IDC_15A))
        {
            lq10Temp_compen.lData = 2560;
        }
        if((g_lq10MdlCurr.lData>=IDC_29A)&&(g_lq10MdlCurr.lData < IDC_55A)&&(g_lq10MdlVolt.lData< VDC_36V))
         {
            lq10Temp_compen.lData=_IQ10mpy(lq10Temp_PfcMdlVolt.lData, (INT32)1380);
            lq10Temp_compen.lData=lq10Temp_compen.lData-(INT32)2000;
         }
        if((g_lq10MdlCurr.lData>=IDC_55A)&&(g_lq10MdlVolt.lData< VDC_34V))  //2209
        {
            lq10Temp_compen.lData=_IQ10mpy(lq10Temp_PfcMdlVolt.lData, (INT32)2128);
            lq10Temp_compen.lData=lq10Temp_compen.lData-(INT32)2000;
        }
       if((g_lq10MdlCurr.lData>=IDC_29A)&&(g_lq10MdlCurr.lData<IDC_80A)&&(g_lq10MdlVolt.lData>=VDC_43V)&&(g_lq10MdlVolt.lData< VDC_45V))
       {
            lq10Temp_compen.lData=_IQ10mpy(g_lq10MdlCurr.lData,(INT32)125);
            lq10Temp_compen.lData=lq10Temp_compen.lData-2954;
       }
       if((g_lq10MdlCurr.lData>=IDC_68A)&&(g_lq10MdlCurr.lData<IDC_80A)&&(g_lq10MdlVolt.lData>=VDC_45V)&&(g_lq10MdlVolt.lData< VDC_46V))
       {
            lq10Temp_compen.lData=_IQ10mpy(g_lq10MdlCurr.lData,(INT32)109);
            lq10Temp_compen.lData=lq10Temp_compen.lData-2954;
       }
       if((g_lq10MdlCurr.lData>=IDC_58A)&&(g_lq10MdlCurr.lData<IDC_80A)&&(g_lq10MdlVolt.lData>=VDC_41V)&&(g_lq10MdlVolt.lData< VDC_43V))
       {
           if(g_lq10MdlCurr.lData>=IDC_71A)
           {
               lq10Temp_compen.lData=_IQ10mpy(g_lq10MdlCurr.lData,(INT32)197);
           }
           else
           {
               lq10Temp_compen.lData=_IQ10mpy(g_lq10MdlCurr.lData,(INT32)224);
           }
            lq10Temp_compen.lData=lq10Temp_compen.lData-(INT32)5904;
       }
       if((g_lq10MdlCurr.lData>=IDC_32A)&&(g_lq10MdlCurr.lData<IDC_43A)&&(g_lq10MdlVolt.lData>=VDC_47V)&&(g_lq10MdlVolt.lData< VDC_54V))
       {
            lq10Temp_compen.lData=-(INT32)656;
       }
      }
      else  //VPfc/Vo<7.8 mountain
      {
          if((g_lq10MdlCurr.lData > IDC_15A)&&(g_lq10MdlCurr.lData < IDC_29A))
          {
              lq10Temp_compen.lData= -700;
          }
      else if((g_lq10MdlCurr.lData > IDC_29A)&&(g_lq10MdlCurr.lData < IDC_48A))
          {
              lq10Temp_compen.lData= -1350;
          }
      else if((g_lq10MdlCurr.lData > IDC_56A)&&(g_lq10MdlCurr.lData < IDC_65A)&&(g_lq10MdlVolt.lData > VDC_54V7))
      {
          lq10Temp_compen.lData= -1024;
      }
          else
          {
        lq10Temp_compen.lData= 0;
          }
      }

      // limit the upper and lower compensation
       if(lq10Temp_compen.lData > 15360)   // 15A*1024=15360
       {
            if ((g_lq10MdlCurr.lData < IDC_29A) || (g_lq10MdlVolt.lData > VDC_31V))
            {
                if((g_lq10MdlCurr.lData > IDC_29A)&&(g_lq10MdlCurr.lData < IDC_40A))
                {
                    lq10Temp_compen.lData = lq10Temp_compen.lData;
                }
                else
                {
                    lq10Temp_compen.lData = 15360;
                }
            }
            else if ((g_lq10MdlCurr.lData > IDC_29A) && (g_lq10MdlVolt.lData <= VDC_34V))
            {
                if (lq10Temp_compen.lData > 50000)
                {
                    lq10Temp_compen.lData = 50000;    //30*1024=30720
                }

            }
        }

      if(lq10Temp_compen.lData < 0)   // 0A*1024=0
      {
          if((g_lq10MdlCurr.lData > IDC_15A)&&(g_lq10MdlCurr.lData < IDC_48A))
          {
              lq10Temp_compen.lData = lq10Temp_compen.lData;
          }
          else if((g_lq10MdlCurr.lData > IDC_56A)&&(g_lq10MdlCurr.lData < IDC_65A)&&(g_lq10MdlVolt.lData > VDC_54V7))
          {
              lq10Temp_compen.lData = lq10Temp_compen.lData;
          }
          else
          {
              lq10Temp_compen.lData = 0;
          }
      }
      //lq10Temp_compen_test3 = lq10Temp_compen.lData;

      //g_lq10ReadVar2.lData = lq10Temp_compen.lData;   //read compensation
      g_lq10MdlCurrNoFilter.lData -= lq10Temp_compen.lData * (INT32)g_u16PermitAdjust;
    //chenli 20200515

    //u16AdIdc25A=(25-b)/a*Vo/Vpfc*491.42857
    lq10Temp.lData = IDC_RATE_50PCT - (INT32)g_lq10CurrSampSysb.lData;
    lq10Temp.lData = _IQ10div(lq10Temp.lData, g_lq10CurrSampSysa.lData);

    lq10Temp.lData = _IQ10mpy(lq10Temp.lData,g_lq10MdlVolt.lData);
    lq10Temp.lData = _IQ10div(lq10Temp.lData,g_lq10PfcVolt.lData);
    lq10Temp.lData = _IQ10mpy(lq10Temp.lData,IQ10_DEF_IDCFACTOR) ;
    IsrVars._i16AdIdc25A = (INT16)( _IQ10div(lq10Temp.lData,(INT32)900)>>10);
//Added by XB20140307

}
/*****************************************************************************
 *FUNCTION: vDcdcCurrFilter()  
 *PURPOSE:  dcdc current filter for display and control                                                    
 *INPUT:         void                                
 *global vars:   g_iq10MdlCurrNoFilter: currentt without filter,1V -> 1024cnt 
 *               g_iq10MdlCurr: real dcdc current ,1V -> 1024cnt              
 				 g_iq10MdlCurrDisp:dcdc current for display,1V -> 1024cnt
 *RETURN:        void
 *CALLS:  
 *CALLED BY:     vAdcTreatment() 
 *Author:
 *Date:              
 ****************************************************************************/
void vDcdcCurrFilter(void)
{
    // ubitfloat    lq10Temp;
   /*
    //cal Dcdc current sum for filter,1A -> 1024cnt(Q10)
    s_ai32AdcDataDcCurr[30] = g_lq10MdlCurrNoFilter.lData;
    s_i32AdcSumDcCurr += s_ai32AdcDataDcCurr[30]
                         - s_ai32AdcDataDcCurr[s_u16AdcPoint];
    s_ai32AdcDataDcCurr[s_u16AdcPoint] = s_ai32AdcDataDcCurr[30];

    //cal Dcdc curr ave
    lq10Temp.lData = _IQ10div(s_i32AdcSumDcCurr,(INT32)30 << 10);
    */

    if(s_u16AdcAveFlag == 0 )
    {
      s_lq10AdcAveDcCurr.lData = g_lq10MdlCurrNoFilter.lData ;
      s_lq10AdcSumDcCurr.lData = g_lq10MdlCurrNoFilter.lData <<5;
    }
    else
    {
      s_lq10AdcSumDcCurr.lData = s_lq10AdcSumDcCurr.lData - s_lq10AdcAveDcCurr.lData
                                 + g_lq10MdlCurrNoFilter.lData ;
      s_lq10AdcAveDcCurr.lData = s_lq10AdcSumDcCurr.lData>>5;
    }

//    IsrVars._lIdcPiout.lData = s_lq10AdcAveDcCurr.lData;    //mountain

    if(labs(s_lq10AdcAveDcCurr.lData - g_lq10MdlCurr.lData) < IDC_RATE_2PCT)
    {
        s_lq10AdcAveDcCurr.lData = (g_lq10MdlCurr.lData * IQ10_ADC_DCCURR_FILTA
                          + s_lq10AdcAveDcCurr.lData * IQ10_ADC_DCCURR_FILTB) >> 10;
    }
    //IsrVars._lAcSamVoltAvg.lData = s_lq10AdcAveDcCurr.lData;
    //limit display dcdc curr

    if(s_lq10AdcAveDcCurr.lData >= g_i32CurrDisMax)
    {
        g_lq10MdlCurr.lData = g_i32CurrDisMax;
    }

    else if (s_lq10AdcAveDcCurr.lData <= IDC_0A)  //ls change 20111227 for Wangjingyu test problem
    {
        g_lq10MdlCurr.lData = IDC_0A;
    }
    else
    {
        g_lq10MdlCurr.lData = s_lq10AdcAveDcCurr.lData;
    }

 //  IsrVars._lAcSamVoltAvg.iData.iLD=DcCurDispCNT;

    g_lq10MdlCurrDisp.lData = g_lq10MdlCurr.lData;
    g_lq10MdlCurrDisp3.lData = g_lq10MdlCurr.lData;
    //qgf 20200613

   //qgf 20200613
   //if(((g_u16MdlStatusExtend.bit.PARAFLAG) && (g_lq10MdlVolt.lData <= VDC_46V)
   //    && (g_lq10MdlAvgCurr.lData <= IDC_10A))||((g_u16MdlStatusExtend.bit.PARAFLAG)
    //   && (g_lq10MdlVolt.lData <= VDC_50V)&& (g_lq10MdlAvgCurr.lData <= IDC_15A)&& (g_lq10AcVolt.lData >= VAC_280V)))
     if((g_u16MdlStatusExtend.bit.PARAFLAG) && (g_lq10MdlVolt.lData <= VDC_50V)
          && (g_lq10MdlAvgCurr.lData <= IDC_18A))
   {
         g_lq10MdlCurrDisp2.lData = (g_lq10MdlAvgCurr.lData * (INT32)717
                                     + g_lq10MdlCurr.lData * (INT32)307) >> 10;
   }
   else
   {
      g_lq10MdlCurrDisp2.lData = g_lq10MdlCurr.lData;
   }
   if((g_lq10MdlCurrDisp2.lData<IDC_1A2)||(g_lq10MdlCurrDisp3.lData<IDC_1A2)||(g_u16MdlStatus.bit.OFFSTAT))
   {
      	g_lq10MdlCurrDisp2.lData = IDC_0A;
      	g_lq10MdlCurrDisp3.lData = IDC_0A;
   }
}


/*****************************************************************************
 *FUNCTION: vOutputPowerCal()  
 *PURPOSE:  output power cal                                                     
 *INPUT:    void                                
 *global vars:   g_iq10MdlVolt: dcdc volt for protect
 *               g_iq10MdlCurr: dcdc current for protect 
 *               g_iq10MdlPow:  output power for protect,1w -> 1024cnt              
 *RETURN:        void
 *CALLS: 
 *CALLED BY:     vAdcTreatment()
 *Author:
 *Date:                 
 ****************************************************************************/
void vOutputPowerCal(void)
{
	//Cal output power 
	g_lq10MdlPow.lData = _IQ10mpy(g_lq10MdlVolt.lData,g_lq10MdlCurr.lData);
}

/*****************************************************************************
 *FUNCTION: vAmbiTemperatureCal()  
 *PURPOSE:  Ambi temperture cal                                                     
 *INPUT:     void
 *global vars:   IsrVars._lAmbiTemp: Ambi temperture  sample,1C->13.65333cnt
 *               iq10TempAmbi: Ambi temperture for protect,1C -> 1024cnt             
 *output:        void  
 *CALLED BY:     vAdcTreatment()
 *Author:
 *Date:               
 ****************************************************************************/
void vAmbiTemperatureCal(void)
{
	ubitfloat	lq10Temp,lq10TempCompen;

	//U1 temp sample
	lq10Temp.lData = (INT32)IsrVars._lU1Temp.iData.iHD << 10;

	//U1 temperture cal
	lq10Temp.lData =  _IQ10div(lq10Temp.lData,IQ10_DEF_TEMPFACTOR) 
	                        - TEMP_50C;

	//Ambient temperature compensation //wangqian20200429
	lq10TempCompen.lData = (g_lq10MdlPow.lData >> 11)+(g_lq10MdlPow.lData >> 12);
	lq10TempCompen.lData = lUpdownlimit(lq10TempCompen.lData, TEMP_3C, 0);

	g_lq10TempAmbi.lData = lq10Temp.lData - lq10TempCompen.lData;	
}

/*****************************************************************************
 *FUNCTION: vAmbiTemperatureFilter()  
 *PURPOSE:  Ambi temperture filter                                                    
 *INPUT:         void 
 *global vars:   g_iq10TempAmbi: Ambi temperture for protect,1C -> 1024cnt
 *               g_iq10TempAmbiDisp: Ambi temperture for display,1C -> 1024cnt             
 *RETURN:        void
 *CALLS:
 *CALLED BY:     vAdcTreatment()
 *Author:
 *Date:                
 ****************************************************************************/
void vAmbiTemperatureFilter(void)
{
//	ubitfloat	lq10Temp;
    
/*		
	//cal U1 temperature  sum for filter ,1C -> 1024cnt(Q10)
	s_ai32AdcDataTempU[30] = g_lq10TempAmbi.lData;
	s_i32AdcSumTempU += s_ai32AdcDataTempU[30] 
	                    - s_ai32AdcDataTempU[s_u16AdcPoint];
	s_ai32AdcDataTempU[s_u16AdcPoint] = s_ai32AdcDataTempU[30];

	//cal U1 temperture ave/
	lq10Temp.lData = _IQ10div(s_i32AdcSumTempU,(INT32)30 << 10);
*/


    if(s_u16AdcAveFlag == 0 )
    {
      s_lq10AdcAveTempU.lData = g_lq10TempAmbi.lData;
      s_lq10AdcSumTempU.lData = g_lq10TempAmbi.lData<<5;
	}
    else
    {
      s_lq10AdcSumTempU.lData = s_lq10AdcSumTempU.lData - s_lq10AdcAveTempU.lData
	                           + g_lq10TempAmbi.lData;
	  s_lq10AdcAveTempU.lData = s_lq10AdcSumTempU.lData>>5; 
    }
   
   if(labs(s_lq10AdcAveTempU.lData - g_lq10TempAmbiDisp.lData) < TEMP_0C3)
   {
	//filter with Volt*0.95+Tmp*0.05	
	s_lq10AdcAveTempU.lData = (g_lq10TempAmbiDisp.lData * IQ10_ADC_TEMP_FILTA 
	                  + s_lq10AdcAveTempU.lData * IQ10_ADC_TEMP_FILTB) >> 10;
	
	}
	//limit the temp value
	if (s_lq10AdcAveTempU.lData > TEMP_DIS_UP)
	{
		g_lq10TempAmbiDisp.lData = TEMP_DIS_UP;
	}
	else if (s_lq10AdcAveTempU.lData < TEMP_DIS_DN)
	{
		g_lq10TempAmbiDisp.lData = TEMP_DIS_DN;
	}
	else
	{
		g_lq10TempAmbiDisp.lData = s_lq10AdcAveTempU.lData;
	}

   	//for control about ambi temp, such as power lim, fan control... 
	g_lq10TempAmbiCtrl.lData = g_lq10TempAmbiDisp.lData-TEMP_3C;//zxf
	//g_lq10TempAmbiCtrl.lData = g_lq10TempAmbiDisp.lData;

	//g_lq10TempAmbiDisp.lData = TEMP_65C;
	//g_lq10TempAmbiCtrl.lData = TEMP_65C;

}

/*****************************************************************************
 *FUNCTION: vDcTemperatureCal()  
 *PURPOSE:  Dc temperture cal                                                     
 *INPUT:         void
 *global vars:   IsrVars._lDcTemp: Dc temperture  sample,1C->13.65333cnt 
 *               g_iq10TempDc: Dc temperture for protect,1C -> 1024cnt
 *RETURN:
 *CALLS:             
 *CALLED BY:     vAdcTreatment()
 *Author:
 *Date:             
 ****************************************************************************/
void vDcTemperatureCal(void)
{
	ubitfloat	lq10Temp;
 
	//Dc temp sample
	lq10Temp.lData = (INT32)IsrVars._lDcTemp.iData.iHD << 10;
	
	/*	
		X=3.184/4096*ADSAMPLE
		y = 410.19x^2 - 2369.8x + 3509    	3.175>x>2.99V
		y = 60.346x^2 - 269.66x + 356.69      2.99>x>2.52V
		y = 6.4602x^2 + 5.6109x + 4.5663      2.52>X>1.175
	*/
     
	lq10Temp.lData = _IQ10mpy(lq10Temp.lData,(INT32)3260);//3.184<<10 

	lq10Temp.lData =_IQ10div(lq10Temp.lData,((INT32)4096) << 10);

	//dcdc temp
    if(lq10Temp.lData > LQ10_ADC_TEMP_3V18)   //3.175V*1024=3251
	{
	
     g_lq10TempDc.lData = TEMP_120C;
	
	}
     //	y = 410.19x^2 - 2369.8x + 3509    	3.175>x>2.99V
	else if(lq10Temp.lData > LQ10_ADC_TEMP_2V99) //2.9959v*1024=3068

	{
		g_lq10TempDc.lData = _IQ10mpy(lq10Temp.lData,lq10Temp.lData);

		g_lq10TempDc.lData = _IQ10mpy(g_lq10TempDc.lData,LQ10_ADC_TEMP_HIGHT_K1);//20

		g_lq10TempDc.lData = g_lq10TempDc.lData
							- _IQ10mpy(lq10Temp.lData,LQ10_ADC_TEMP_HIGHT_K2)
							+ LQ10_ADC_TEMP_HIGHT_K3;	
		
	}

	//	y = 60.346x^2 - 269.66x + 356.69      2.99>x>2.52V

	else if(lq10Temp.lData > LQ10_ADC_TEMP_2V52)//2.523257813*1024=2584
	{
	
		g_lq10TempDc.lData = _IQ10mpy(lq10Temp.lData,lq10Temp.lData);

		g_lq10TempDc.lData = _IQ10mpy(g_lq10TempDc.lData,LQ10_ADC_TEMP_K1);//20

		g_lq10TempDc.lData = g_lq10TempDc.lData
							- _IQ10mpy(lq10Temp.lData,LQ10_ADC_TEMP_K2)
							+ LQ10_ADC_TEMP_K3;
	}

	//	y = 6.4602x^2 + 5.6109x + 4.5663      2.52>X>1.175
	else if (lq10Temp.lData > LQ10_ADC_TEMP_1V95)//1.95*1024= 1203
	{
		g_lq10TempDc.lData = _IQ10mpy(lq10Temp.lData,lq10Temp.lData);

		g_lq10TempDc.lData = _IQ10mpy(g_lq10TempDc.lData,LQ10_ADC_TEMP_LOWT_K1);//20

		g_lq10TempDc.lData = g_lq10TempDc.lData
							+_IQ10mpy(lq10Temp.lData,LQ10_ADC_TEMP_LOWT_K2)
							+ LQ10_ADC_TEMP_LOWT_K3;
	}

	else
	{
	
	g_lq10TempDc.lData = TEMP_40C;

	}
    

}

/*****************************************************************************
 *FUNCTION: vDcTemperatureFilter()  
 *PURPOSE:  Dc temperture filter                                                    
 *INPUT:    void
 *global vars:   g_iq10TempDc: Dc temperture for protect,1C -> 1024cnt
 *               g_iq10TempDcDisp: Dc temperture for display,1C -> 1024cnt             
 *RETURN:        void
 *CALLS: 
 *CALLED BY:     vAdcTreatment()
 *Author:
 *Date:                
 ****************************************************************************/
void vDcTemperatureFilter(void)
{
//	ubitfloat	lq10Temp;
	/*	
	//cal U1 temperature  sum for filter ,1C -> 1024cnt(Q10)
	s_ai32AdcDataTempM[30] = g_lq10TempDc.lData;
	s_i32AdcSumTempM += s_ai32AdcDataTempM[30] 
	                    - s_ai32AdcDataTempM[s_u16AdcPoint];
	s_ai32AdcDataTempM[s_u16AdcPoint] = s_ai32AdcDataTempM[30];

	//cal U1 temperture ave/
	lq10Temp.lData = _IQ10div(s_i32AdcSumTempM,(INT32)30 << 10);
    */

	if(s_u16AdcAveFlag == 0 )
    {
    // lq10Temp.lData = g_lq10TempDc.lData;
	 s_lq10AdcAveTempM.lData = g_lq10TempDc.lData;
     s_lq10AdcSumTempM.lData = g_lq10TempDc.lData <<5;
	}
    else
    {
      s_lq10AdcSumTempM.lData = s_lq10AdcSumTempM.lData - s_lq10AdcAveTempM.lData 
	                     + g_lq10TempDc.lData;

      s_lq10AdcAveTempM.lData = s_lq10AdcSumTempM.lData >>5;
    }
	
	if(labs(s_lq10AdcAveTempM.lData - g_lq10TempDcDispSave.lData) < TEMP_0C3)
	//filter with Volt*0.95+Tmp*0.05	
	{
    	s_lq10AdcAveTempM.lData = (g_lq10TempDcDispSave.lData * IQ10_ADC_TEMP_FILTA 
	                        + s_lq10AdcAveTempM.lData * IQ10_ADC_TEMP_FILTB) >> 10;
    }
	/*
	//limit the temp value
	if (s_lq10AdcAveTempM.lData > TEMP_DIS_UP)
	{
		g_lq10TempDcDispSave.lData = TEMP_DIS_UP;
	}
	else if (s_lq10AdcAveTempM.lData < TEMP_DIS_DN)
	{
		g_lq10TempDcDispSave.lData = TEMP_DIS_DN;
	}
	else
	{
		g_lq10TempDcDispSave.lData = s_lq10AdcAveTempM.lData;
	}
    */
	g_lq10TempDcDispSave.lData = s_lq10AdcAveTempM.lData;
	
	/*************************************************************
		pfc temp dc temp <20deg, not correct,so lower than 20deg ,
	
		the  pfc temp dc temp deg equal to ambi temp

	******************************************************************/
	// patch dcdc temp

	if (g_lq10TempDcDispSave.lData > TEMP_BOARD_DIS_DN)
	{
		g_lq10TempDcDisp.lData	= g_lq10TempDcDispSave.lData;
	}

	else if (g_lq10TempDcDispSave.lData <(TEMP_BOARD_DIS_DN-TEMP_BOARD_HYS)) 
	{
	
		g_lq10TempDcDisp.lData = g_lq10TempAmbiDisp.lData;
	
	}
	else
	{
		g_lq10TempDcDisp.lData = TEMP_BOARD_DIS_DN;
	}		
}
/*****************************************************************************
 *FUNCTION: vPFCTemperatureCal()  
 *PURPOSE:  PFC temperture cal                                                     
 *INPUT:         void
 *global vars:   IsrVars._lDcTemp: PFC temperture  sample,1C->13.65333cnt 
 *               g_iq10TempDc: PFCtemperture for protect,1C -> 1024cnt
 *RETURN:
 *CALLS:             
 *CALLED BY:     vAdcTreatment()
 *Author:
 *Date:             
 ****************************************************************************/
//void vPFCTemperatureCal(void)
//{
//	ubitfloat	lq10Temp;
//
//	//pfc temp sample
//	lq10Temp.lData = ((INT32)IsrVars._lPFCTemp.iData.iHD) << 10;
///*
//		X=3.184/4096*ADSAMPLE
//		y = 410.19x^2 - 2369.8x + 3509    	3.175>x>2.99V
//		y = 60.346x^2 - 269.66x + 356.69      2.99>x>2.52V
//		y = 6.4602x^2 + 5.6109x + 4.5663      2.52>X>1.175
//	*/
//
//	lq10Temp.lData = _IQ10mpy(lq10Temp.lData,(INT32)3260);//3.184<<10
//
//	lq10Temp.lData =_IQ10div(lq10Temp.lData,((INT32)4096) << 10);
//
//	//PFC temp
//    if(lq10Temp.lData > LQ10_ADC_TEMP_3V18)   //3.175V*1024=3251
//	{
//
//     g_lq10TempPfc.lData = TEMP_120C;
//
//	}
//     //	y = 410.19x^2 - 2369.8x + 3509    	3.175>x>2.99V
//	else if(lq10Temp.lData > LQ10_ADC_TEMP_2V99) //2.9959v*1024=3068
//
//	{
//		g_lq10TempPfc.lData = _IQ10mpy(lq10Temp.lData,lq10Temp.lData);
//
//		g_lq10TempPfc.lData = _IQ10mpy(g_lq10TempPfc.lData,LQ10_ADC_TEMP_HIGHT_K1);//20
//
//		g_lq10TempPfc.lData = g_lq10TempPfc.lData
//							- _IQ10mpy(lq10Temp.lData,LQ10_ADC_TEMP_HIGHT_K2)
//							+ LQ10_ADC_TEMP_HIGHT_K3;
//
//	}
//
//	//	y = 60.346x^2 - 269.66x + 356.69      2.99>x>2.52V
//
//	else if(lq10Temp.lData > LQ10_ADC_TEMP_2V52)//2.523257813*1024=2584
//	{
//
//		g_lq10TempPfc.lData = _IQ10mpy(lq10Temp.lData,lq10Temp.lData);
//
//		g_lq10TempPfc.lData = _IQ10mpy(g_lq10TempPfc.lData,LQ10_ADC_TEMP_K1);//20
//
//		g_lq10TempPfc.lData = g_lq10TempPfc.lData
//							- _IQ10mpy(lq10Temp.lData,LQ10_ADC_TEMP_K2)
//							+ LQ10_ADC_TEMP_K3;
//	}
//
//	//	y = 6.4602x^2 + 5.6109x + 4.5663      2.52>X>1.175
//	else if (lq10Temp.lData > LQ10_ADC_TEMP_1V18)//1.175*1024= 1203
//	{
//		g_lq10TempPfc.lData = _IQ10mpy(lq10Temp.lData,lq10Temp.lData);
//
//		g_lq10TempPfc.lData = _IQ10mpy(g_lq10TempPfc.lData,LQ10_ADC_TEMP_LOWT_K1);//20
//
//		g_lq10TempPfc.lData = g_lq10TempPfc.lData
//							+_IQ10mpy(lq10Temp.lData,LQ10_ADC_TEMP_LOWT_K2)
//							+ LQ10_ADC_TEMP_LOWT_K3;
//	}
//
//	else
//	{
//
//	g_lq10TempPfc.lData = TEMP_20C;
//
//	}
//
//}

/*****************************************************************************
 *FUNCTION: vPFCTemperatureFilter()  
 *PURPOSE: PFC temperture filter                                                    
 *INPUT:    void
 *global vars:   g_iq10TempDc: PFC temperture for protect,1C -> 1024cnt
 *               g_iq10TempDcDisp: PFC temperture for display,1C -> 1024cnt             
 *RETURN:        void
 *CALLS: 
 *CALLED BY:     vAdcTreatment()
 *Author:
 *Date:                
 ****************************************************************************/
//void vPFCTemperatureFilter(void)
//{
//
//    if(s_u16AdcAveFlag == 0 )
//    {
//        s_lq10AdcAveTempPFC.lData = g_lq10TempPfc.lData;
//        s_lq10AdcSumTempPFC.lData = g_lq10TempPfc.lData <<5;
//	}
//    else
//    {
//        s_lq10AdcSumTempPFC.lData = s_lq10AdcSumTempPFC.lData - s_lq10AdcAveTempPFC.lData
//                                   + g_lq10TempPfc.lData;
//
//        s_lq10AdcAveTempPFC.lData = s_lq10AdcSumTempPFC.lData >>5;
//    }
//
//	if(labs(s_lq10AdcAveTempPFC.lData - g_lq10TempPfcDispSave.lData) < TEMP_0C3)
//	{	//filter with Volt*0.95+Tmp*0.05
//    	s_lq10AdcAveTempPFC.lData = (g_lq10TempPfcDispSave.lData * IQ10_ADC_TEMP_FILTA
//	                               + s_lq10AdcAveTempPFC.lData * IQ10_ADC_TEMP_FILTB) >> 10;
//    }
//
//
//	g_lq10TempPfcDispSave.lData = s_lq10AdcAveTempPFC.lData;
//	/*************************************************************
//		pfc temp dc temp <20deg, not correct,so lower than 20deg ,
//
//		the  pfc temp dc temp deg equal to ambi temp
//
//	******************************************************************/
//
//	// patch pfc temp
//	if (g_lq10TempPfcDispSave.lData > TEMP_BOARD_DIS_DN)
//	{
//		g_lq10TempPfcDisp.lData	= g_lq10TempPfcDispSave.lData;
//	}
//
//	else if (g_lq10TempPfcDispSave.lData <(TEMP_BOARD_DIS_DN-TEMP_BOARD_HYS))
//	{
//		g_lq10TempPfcDisp.lData = g_lq10TempAmbiDisp.lData;
//	}
//	else
//	{
//		g_lq10TempPfcDisp.lData = TEMP_BOARD_DIS_DN;
//	}
//}

/*****************************************************************************
 *FUNCTION: vAcFrequencyCal  
 *PURPOSE:  ac volt  frequency cal and filter                                                    
 *INPUT:    
 *global vars:  
 *RETURN:   
 *CALLS: 
 *CALLED BY: vAdcTreatment()
 *Author:
 *Date:                                
 ****************************************************************************/
void vAcFrequencyCal(void)
{
	ubitfloat	lTmp;
	ubitfloat   lTmpFre3;
	

	//当输入电压大于50V的时候开始计算频率
    if(g_lq10AcVolt.lData > VAC_DIS_DN)
	{

	    //取40次加权滤波
		s_lTmpFre1.lData=(INT32)IsrVars._u16VinFrequency<<10;
		s_lTmpFre2.lData= s_lTmpFre1.lData+s_lTmpFre2.lData*9;
	    s_lTmpFre2.lData= s_lTmpFre2.lData/10; 
		lTmpFre3.lData =  s_lTmpFre2.lData/1024;
	    //中断频率=60M/(428*2)	
	    //输入波形的周期= lTmp.lData/中断频率
		//输入波形的频率= 1/输入波形的周期 =60M/(lTmp.lData*428*2)
		//为提高计算精度，计算是放大10倍。 
		lTmp.lData = ((INT32)75000000)/lTmpFre3.lData;
		lTmp.lData = lTmp.lData/((INT32)107);

	}
	else
	{
		s_lTmpFre2.lData = (INT32)IsrVars._u16VinFrequency;
		lTmp.lData = 0 ;
	}

	lTmp.lData = lTmp.lData<<10;
	// 频率校准   
//    lTmp.lData = _IQ10mpy(lTmp.lData,g_lq10AcFrequencySampSysa.lData) 
//		         + g_lq10AcFrequencySampSysb.lData ;

    lTmp.lData = lTmp.lData/1024;

    //输入限幅度
    if( lTmp.lData > FREQUENCY_UP )
	{
	    lTmp.lData = FREQUENCY_UP;
	}
	else if( lTmp.lData <= FREQUENCY_DOWN )
	{
        lTmp.lData = FREQUENCY_DOWN;
	}

	//mdl closed,clr display 
	/*if((g_u16MdlStatus.bit.OFFSTAT)
	 ||(g_lq10AcVolt.lData < VAC_DIS_DN))
	{
		lTmp.lData = 0;
	}*/

    if(g_lq10AcVolt.lData < VAC_DIS_DN)
    {
        lTmp.lData = 0;
    }


	g_lq10AcFre.lData = ((lTmp.lData << 10)/10);


}
/*****************************************************************************
 *FUNCTION: vDSPTempSensorConvCal  
 *PURPOSE:  ac volt  frequency cal and filter                                                    
 *INPUT:    
 *global vars:  
 *RETURN:   
 *CALLS: 
 *CALLED BY: vAdcTreatment()
 *Author:
 *Date:                                
 ****************************************************************************/
void vDSPTempSensorConvCal(void)
{
     INT32 AdcSumOSCTemp;

     INT16 s_i16temp;
     INT16 s_i16degC;
    //Get temp sensor sample result from SOC1
    //s_lq10DSPTemp.lData = (((INT32)AdcaResultRegs.ADCRESULT12)<<10);//$
    s_lq10DSPTemp.lData = (((INT32)AdcbResultRegs.ADCRESULT4)<<10);//$
    //内部温度采样滤波
    if(s_u16AdcAveFlag == 0 )
    {
         s_lq10AdcdspAvrTemp.lData = s_lq10DSPTemp.lData;
         s_lq10AdcdspSumTemp.lData = s_lq10DSPTemp.lData <<5;
    }
    else
    {
         s_lq10AdcdspSumTemp.lData = s_lq10AdcdspSumTemp.lData - s_lq10AdcdspAvrTemp.lData
                             + s_lq10DSPTemp.lData;

         s_lq10AdcdspAvrTemp.lData = s_lq10AdcdspSumTemp.lData >>5;
    }

//    s_i16temp =(INT16)(s_lq10AdcdspAvrTemp.lData/1024);
    s_i16temp =(UINT16)(s_lq10AdcdspAvrTemp.lData/1024);//$
    //Convert the raw temperature sensor measurement into temperature
//    s_i16degC = GetTemperatureC(s_i16temp); //$定义函数GetTemperatureC()
    s_i16degC = ADC_getTemperatureC(s_i16temp,3.3F); //$定义函数GetTemperatureC()
    //内部温度采样基于AD内部基准电压3.3V.需要转换为外部基准电压采样.
    AdcSumOSCTemp =  (INT32)s_i16degC * VADC_OUT_VOLT;
    AdcSumOSCTemp =   AdcSumOSCTemp/ VADC_INTERNAL_VOLT;
    s_i16degC = (INT16) AdcSumOSCTemp;

    //DSP内部温度采样Q10定标
    g_lq10TempSensor.lData = (((INT32) s_i16degC)<<10);

    //limit DSP internal temp
    if (g_lq10TempSensor.lData > TEMP_125C)
    {
        g_lq10TempSensor.lData = TEMP_125C;
    }
    else if (g_lq10TempSensor.lData < N_TEMP_40C)
    {
        g_lq10TempSensor.lData = N_TEMP_40C;
    }
    IsrVars._lIdcPiout.lData=g_lq10TempSensor.lData;//$后台观测
    IsrVars._lAcVoltAvgFilt_LW.iData.iHD=ADC_EXT_REF_TSOFFSET;
    IsrVars._lAcVoltAvgFilt_LW.iData.iLD=ADC_EXT_REF_TSSLOPE;

    /*if(IsrVars._i16DsptmpFlag==1)
    {
        g_lq10TempSensor.lData=g_lq10TempSensorTest.lData;
    }*/
}


int16 ADC_getTemperatureC(UINT16 tempResult, float32 vref) //$
{
    float32 temp;

    //
    // Read temp sensor slope and offset locations from OTP and convert
    //
    temp = (float32)tempResult * (vref / 2.5F);
    return((int16)((((int32)temp - ADC_EXT_REF_TSOFFSET) * 4096) /
                     ADC_EXT_REF_TSSLOPE));
}



//===========================================================================
// No more.
//===========================================================================

