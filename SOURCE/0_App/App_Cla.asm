;// Include variables and constants that will be shared in the
;// C28x C-code and CLA assembly code.  This is accomplished by
;// using .cdecls to include a C-code header file that contains
;// these variables and constants
       .cdecls C, LIST, "CLAShared.h"

;// CLA code must be within its own assembly section and must be
;// even aligned.  Note: since all CLA instructions are 32-bit
;// this alignment naturally occurs and the .align 2 is most likely
;// redundant

       .sect        "Cla1Prog"
_Cla1Prog_Start
       .align       2


_Cla1Task1:
     ; MDEBUGSTOP
	;;;;DC PRD 和CMP的初始化;;;;;;;;
	MMOVF32		MR1,#120.0 ;
    MMOVF32		MR2,#4.0;
	MMOV32      @_ClatoCpuVar._f32DcdcPWMPRDShadow,MR1
    MMOV32      @_ClatoCpuVar._f32DcdcPWMCMPShadow,MR2

    MSTOP
	MNOP
    MNOP
    MNOP
_Cla1T1End:


_Cla1Task5:
 ; MDEBUGSTOP
;;;;;;;;;功率环计算;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;变频计算功率环控制器的系数;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;测试进出中断的时刻;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;    MUI16TOF32   MR1,@_EPwm5Regs.TBCTR
 ;    MUI16TOF32   MR0,@_EPwm5Regs.TBCTR
 ;    MF32TOUI16   MR2, MR1
 ;    MF32TOUI16   MR3, MR0
 ;    MMOV16       @_ClatoCpuVar._u16viewflag1, MR2
 ;    MMOV16       @_ClatoCpuVar._u16viewflag2, MR3
;;;;;;;;;;;测试进出中断的时刻;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;计算 控制器的实时频率;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;只用PWM而不用高精度计算控制器频率会带来1%的误差，
;;;;;;;;;;;即零极点和增益可能有1%的偏差;;;;;;;;;;;;;;;;;;;
    MUI16TOF32   MR0,@_EPwm5Regs.TBPRD;;;;TBPRD为DC开关周期值的一半CLK
	;;;1/30Mhz,因为前面用开关周期一半CLK，所以这里用60Mhz/2
    MMOVF32 MR2, #0.00000002
    MMPYF32 MR0,MR0,MR2;;;;如果直接用#0.000000033333333333只有16位浮点精度
    MMOV32  @_ClatoCpuVar._f32DcdcPWMTs,MR0 ;;;这个_f32DcdcPWMTs是真正的TS不是CLK
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;计算单零单极系数;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;计算PI*T*fp*fz;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MMOVF32  MR2, #3.14159265;;;;如果直接用3.14159265去乘，只有16位浮点精度
    MMPYF32  MR1,MR0,MR2; ;;;;PI*T给MR1
||  MMOV32   MR3,@_ClatoCpuVar._f32IdcLoopfp;;fp给MR2
    MMPYF32  MR1,MR1,MR3  ;PI*T*fp给MR1
||	MMOV32   MR3 ,@_ClatoCpuVar._f32IdcLoopfz;;fz给MR3
	MMPYF32  MR1,MR1,MR3  ; MR1=PI*T*fp*fz   
||  MMOV32   MR2,@_ClatoCpuVar._f32IdcLoopfz
	MMOV32   @_ClatoCpuVar._f32IdcSZSPTsfpfzPI,MR1    
;;;;;;;;;;;;;计算1/（PI*T*fp*fz+fz);;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;浮点数除法，用两次牛顿迭带;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    MADDF32		MR1,MR2,MR1   ;;;MR1=PI*T*fp*fz+fz
    MEINVF32    MR2, MR1 ; MR2 = Ye = Estimate(1/Den)
    MMPYF32     MR3, MR2, MR1 ; MR3 = Ye*Den
    MSUBF32     MR3, #2.0, MR3 ; MR3 = 2.0 - Ye*Den
    MMPYF32     MR2, MR2, MR3 ; MR2 = Ye = Ye*(2.0 - Ye*Den)
    MMPYF32     MR3, MR2, MR1 ; MR3 = Ye*Den
    MSUBF32     MR3, #2.0, MR3 ; MR3 = 2.0 - Ye*Den
    MMPYF32     MR2, MR2, MR3 ; MR2 = Ye = Ye*(2.0 - Ye*Den);MR2=1/(PI*T*fp*fz+fz )
||  MMOV32      MR1,@_ClatoCpuVar._f32IdcSZSPTsfpfzPI
;;;;;;;;;;;;;;;;;;计算SZSP的K1，K2，K3;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MMOV32      MR3,@_ClatoCpuVar._f32IdcLoopfp 
    MADDF32     MR0,MR1,MR3
    MMPYF32     MR0,MR0,MR2
    MMOV32      @_ClatoCpuVar._f32IdcLoopFilterK2,MR0
    MSUBF32     MR0,MR1,MR3
    MMPYF32     MR0,MR0,MR2
    MMOV32      @_ClatoCpuVar._f32IdcLoopFilterK3,MR0
    MSUBF32     MR0,#1.0,MR0
    MMOV32      MR3,@_ClatoCpuVar._f32IdcLoopFilterK2
    MSUBF32     MR0,MR0,MR3
||  MMOV32      MR1,@_ClatoCpuVar._f32DcdcPWMTs
    MMOV32      @_ClatoCpuVar._f32IdcLoopFilterK1,MR0
;;;;;;;;;;;;;;;;;计算SZSP的K1，K2，K3 END;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;计算PI控制器的PI*Ts*fz*kv;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;    
    MMOVF32     MR2, #3.14159265
    MMPYF32     MR1,MR1,MR2   ;PI*Ts给MR1
||  MMOV32      MR3,@_ClatoCpuVar._f32PowerLoopfz
    MMPYF32     MR1,MR1,MR3   ;PI*Ts*fz给MR1
||  MMOV32      MR2,@_ClatoCpuVar._f32PowerLoopKv
    MMPYF32     MR1,MR1,MR2   ;PI*Ts*fz*kv给MR1
||  MMOV32      MR3,@_ClatoCpuVar._f32PowerLoop2PIfzINV;MR3=1/(2*PI*fz)
    MADDF32     MR0,MR1,MR2   ;PI*Ts*fz*kv+kv给MR0
    MMPYF32     MR0,MR0,MR3   ;MR0= (PI*Ts*fz*kv+kv) /(2*PI*fz)
    MSUBF32     MR1,MR1,MR2   ;PI*Ts*fz*kv-kv给MR1
||  MMOV32      @_ClatoCpuVar._f32PowerLoopK3,MR0
    MMPYF32     MR1,MR1,MR3   ;MR1= (PI*Ts*fz*kv-kv) /(2*PI*fz)
    MMOV32      @_ClatoCpuVar._f32PowerLoopK4,MR1
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ; MDEBUGSTOP
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;DC 功率环计算;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MI16TOF32	MR0,@_AdcaResultRegs.ADCRESULT1  ;;;;;;;;;;DC电流采样存放到MR0
	MUI16TOF32	MR1,@_AdccResultRegs.ADCRESULT1  ;;;;;;;;;;PFC电压采样存放到MR1
    MI16TOF32   MR2,@_CputoClaVar._i16DCCurOffsetTrue
    MSUBF32     MR0,MR0,MR2
	MMAXF32     MR0, #0.0 ;;如果_ClatoCpuVar._f32AdIdc小于0，则把0给_ClatoCpuVar._f32AdIdc。下限 

    MMOV32		@_ClatoCpuVar._f32AdIdc,MR0;;;;;;;;;DC电流采样存放_ClatoCpuVar._f32AdIdc
    MMPYF32     MR0, MR0, MR1;;;;;;;;;;;;;;;;;;;;;Vpfc*Idc存放到MR0
    MF32TOI32   MR1,MR0;;;;;;;;;;;;;;;;;;转成整形存在MR1中
    MASR32      MR1, #10  ;;;;;;;;右移10位，这个10不用写成浮点形式
    MMOV32      @_ClatoCpuVar._u32DCDCPowerFeedbackCLA , MR1;;;;;;把功率环反馈给i16DCDCPowerFeedback 做环路切换
    MI32TOF32   MR2,MR1;;;转浮点给MR2
	
    MMOV32      @_ClatoCpuVar._f32DCDCPowerFeedback,MR2;;存到f32DCDCPowerFeedback
    ;q0426test
    ;这里可以用到M0\M1和M3，M2不能修改赋值
;    MF32TOUI16   MR1, MR2
;	MMOV16       @_ClatoCpuVar._i16QtestCLAtoCPU, MR1
;
;	MMOVF32      MR1,#0.464188577;Q10格式的数2206对应的倒数的小数
;	MMPYF32       MR1,MR1,MR2;这里转成和功率对应的数值
;
;	MUI16TOF32   MR3,@_CputoClaVar._i16OnePhasePowerToCLA;定义一个变量，把单相功率传过来
;	MMPYF32		  MR3,MR3,#2.0
;	MSUBF32        MR1,MR1,MR3;M1是新计算的Delta：DCDC-PFC
;	MMAXF32      MR1, #-500.0
;	MMINF32       MR1, #3500.0
;	MF32TOUI16   MR0, MR1
;	MMOV16       @_ClatoCpuVar._i16DCPFCPowerDetla,MR0;;14BB
;	;filter   cla变频，先按照定频去算，不管了
;	MMOV32      MR0,@_ClatoCpuVar._f32DCPFCPowerDetla;X(n-1)
;	MMOV32		 MR3,@_CputoClaVar._f32FilterK2;;;K2给MR3
;	MMPYF32		 MR0,MR3,MR0;x(n-1)*k2
;	MMOV32      @_ClatoCpuVar._f32DCPFCPowerDetla,MR1;X(n)更新
;	MMPYF32		 MR3,MR1,MR3;x(n)*k2
;	MADDF32       MR0,MR3,MR0;x(n)*k2+x(n-1)*k2
;	MMOV32      MR3,@_ClatoCpuVar._f32DCPFCPowerDetlaFilter
;	MMOV32      MR1,@_CputoClaVar._f32FilterK1
;	MMPYF32		  MR3,MR1,MR3;y(n-1)*k1
;	MADDF32       MR0,MR3,MR0;x(n)*k2+x(n-1)*k2+y(n-1)*k1
;	MMOV32      @_ClatoCpuVar._f32DCPFCPowerDetlaFilter,MR0;y(n)
;
;	MF32TOUI16   MR1, MR0
;	MMOV16       @_ClatoCpuVar._i16DCPFCPowerDetlaFilter, MR1
    ;q0426testend
    
    MI16TOF32	MR3,@_CputoClaVar._i16DCDCPowerSetCLA;;;_i16DCDCPowerSetCLA存MR3
    MSUBF32     MR3,MR3,MR2  ;;_i16DCDCPowerSetCLA-_f32DCDCPowerFeedback给MR3
;;;;;;;;;;;这里做一个电流环的增益系数;;;;;;;;;;;;;;;;;;;;;;;;;;;   
    MUI16TOF32  MR1,@_CputoClaVar._u16DCCurloopCoef
    MMOVF32     MR2,#0.001
	MMPYF32     MR3,MR3,MR1
    MMPYF32     MR3,MR3,MR2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;    
    MMOV32      @_ClatoCpuVar._f32PowerErr0,MR3
;    MDEBUGSTOP
;;;;;;;;;功率环单零单极;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MMOVD32		MR1,@_ClatoCpuVar._f32PowerErrUse0;;;_f32PowerErrUse0给MR1和_f32PowerErrUse1
;;;;;;;;;;;;K1,K2,K3要把定标的Q14去掉;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	MMOV32		MR2,@_ClatoCpuVar._f32IdcLoopFilterK1;;;K1给MR2
	MMPYF32		MR3,MR2,MR1   ;;K1*ErrUse1（Y1）=MR3
||	MMOV32		MR1,@_ClatoCpuVar._f32IdcLoopFilterK3;;K3给MR1
	MMOV32		MR2,@_ClatoCpuVar._f32PowerErr1;;;PowerErr1(X1)给MR2
	MMPYF32		MR1,MR2,MR1         ;;;K3*PowerErr1(X1)给MR1
||	MMOV32		MR2,@_ClatoCpuVar._f32IdcLoopFilterK2;;;K2给MR2
	MADDF32		MR3,MR3,MR1                    ;;K1*Y1+K3*X1=MR3
	MMOVD32		MR0,@_ClatoCpuVar._f32PowerErr0;;;PowerErr0(X2)给MR0和X1
	MMPYF32		MR1,MR2,MR0              ;;;;;K2*X2=MR1
	MADDF32		MR3,MR3,MR1              ;;;;;MR3(K1*Y1+K3*X1)+ K2*X2=MR3
	MMOV32		@_ClatoCpuVar._f32PowerErrUse0,MR3;;;;MR3给PowerErrUse0(Y2)
;;;;;;;;;注意，原程序这里有个左移两位等于右移14位，是把K1~K3的定标去掉，;;;;;
;;;;;;;;;因为是浮点计算，可以把系数的定标直接去掉,就不用右移14位了;;;;;;;;;;;

;;;;;;;;;;;;功率环 PI;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	MMOV32		MR0,@_ClatoCpuVar._f32PowerLoopK3;;;;K3给MR0
	MMPYF32		MR1,MR3,MR0        ;;;K3*PowerErrUse0（X2）给MR1
||	MMOV32		MR2,@_ClatoCpuVar._f32PowerLoopK4 ;;;K4给MR2
	MMOV32		MR0,@_ClatoCpuVar._f32PowerErrUse1;;;X1给MR0
	MMPYF32		MR2,MR2,MR0                    ;;;;;K4*X1给MR2
||	MMOV32		MR0,@_ClatoCpuVar._f32IdcPiout ;;;Y1 给MR0  
	MADDF32		MR1,MR2,MR1        ;;;K3*X2+K4*X1=MR1           
	MADDF32		MR1,MR1,MR0        ;;;Y1+K3*X2+K4*X1=MR1,MR1现在为未限定上下的_f32IdcPiout
;;;;;;;;;;原程序中K3，K4是Q21定标，通过Qn移去Q5，然后高位给PWM，低位给PWMHR,相当于右移了16位
;;;;;;;改系数时需要注意，不要带定标，高精度用小数转换，整数给PWM;;;;;;;;;;;;;;;;;;;;;

;/////////////////////扫频程序赋值/////////////////////////////////////////////////////


;	MMOVZ16   MR2,@_CputoClaVar._lOpenPIoutStartCLA.iData.iHD
;	MUI16TOF32 MR3, MR2  ;提出高位的非高精度位
;   MMOVZ16	  MR0,@_CputoClaVar._lOpenPIoutStartCLA.iData.iLD;低位高精度
;    MUI16TOF32 MR2, MR0
;	MMOVI32 MR0, #0x37800000  ;;MR0=1/65536
;	MMPYF32 MR2, MR2, MR0
;    MADDF32 MR3,MR2,MR3;;MR3中是整周期

;    MUI16TOF32 MR2, @_CputoClaVar._u16OpenLoopCtrlCLA
;	MCMPF32  MR2, #1.0 ;;标志位是1才扫频
;    MMOV32 MR1, MR3,EQ

;///////////////////扫频程序赋值/结束/////////////////////////////////////////////////////////////


;;;;;;;;环路计算的上下限，_f32IdcPiout;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
     
    ;MMAXF32     MR1, #100.0 ;;;如果_f32IdcPiout小于100，则把100给_f32IdcPiout。下限
	;;;;;;MMAXF32用立即数参与比较，只有16位浮点精度;;;;
    MMAXF32     MR1, #190.0
    MI16TOF32	MR3,@_CputoClaVar._i16IdcPioutPermitCLA
    MMINF32     MR1,MR3    ;;;如果_f32IdcPiout大于i16IdcPioutPermitCLA，
                           ;;;;;;则把i16IdcPioutPermitCLA给_f32IdcPiout,上限 
    MI16TOF32	MR3,@_CputoClaVar._i16IdcPioutPermitCLA1
    MMINF32     MR1,MR3
    MMOV32		@_ClatoCpuVar._f32IdcPiout,MR1;;;把经过上下限幅的MR1付值给_f32IdcPiout
    MMOV32      MR0,MR1;;;;MR1付值给MR0，留做后面的调频调宽用

    MF32TOUI16 	MR3,MR1;qgf
	MMOV16     	@_ClatoCpuVar._u16viewflag1,MR3  ;qgf 14B4
;;;;;;;;;;;;;;;;;;;;;把_f32IdcPiout转成整形以便读取
 
  ;  MMOVF32     MR2,#65536.0
  ;  MMPYF32     MR2,MR2,MR1
;	MF32TOUI32  MR3,MR2
;	MMOV32      @_ClatoCpuVar._u32viewflag5, MR3
;;;;;;;;;环路计算的上下限结束;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;暂时不改这句，后面有重复的地方;;;;;;;;;;;;;;;;;


;;;;;;;;;发波频率（周期）上下限;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MI16TOF32	MR3,@_CputoClaVar._i16DcdcPWMTsMaxCLA;把_i16DcdcPWMTsMaxCLA（最大周期）给MR3
    MMINF32     MR1,MR3 ;把MR3（_i16DcdcPWMTsMaxCLA）作为上蓿徊较拗芲f32IdcPiout最后给发波
    MI16TOF32	MR3,@_CputoClaVar._i16DcdcPWMTsMinCLA;把_i16DcdcPWMTsMinCLA（最小周期）给MR3
    MMAXF32     MR1,MR3;把MR3（_i16DcdcPWMTsMinCLA）作为下限，进一步限制_f32IdcPiout最后给发波
  
;;;;;;;;;发波频率（周期）上下限结束;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    
;;;;;;;发波计算，由PIOUT 浮点数~下限100(注意不是发波周期~下限240)转化成频率和占空比定点数;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MMOV32		MR2,@_ClatoCpuVar._f32DcdcPWMDutyK
    MMPYF32     MR2, MR2, MR0      ;;;DutyK*TS，这里MR1已经进一步上下限做频率不能用做计算调宽（单调宽）了
||  MMOV32      MR3, @_ClatoCpuVar._f32DcdcPWMDutyD
    MSUBF32     MR2,MR2,MR3        ;;;DutyK*TS-DutyD=DUTY
    MMPYF32     MR2,MR2,MR0        ;;DUTY*TS=导通时间
    MMPYF32     MR3,MR1,#0.5   ;;;MR3为MR1（周期值的一半，即50%占空比，所以不用MR0代表的PIOUT），作为DUTYMAX（最大导通值的上限）
    MMINF32     MR2,MR3      ;;;把MR3（DUTYMAX）作为上限限制导通时间
;;;;;;计算到这里;;MR1为周期值（PWMPRD*2）而MR2纪ㄊ奔?;;;;;;;;


;;;;;;;间歇发波设置;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    ;;MMOVF32     MR0, #0.0     ;;MR0付值为0
	;;MMOVF32     MR3, #20.0
	;MCMPF32     MR2, #36.0    ;;导通时间MR2和36比较
    ;; MCMPF32     MR2, #60.0
    ;;;;;;;间歇发波设置;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
	;20200323 for hot issue @low Vo
	MUI16TOF32 	MR0, @_CputoClaVar._u16InterimVal
	MUI16TOF32  MR3, @_CputoClaVar._u16Ton_BurstOff	;与死区时间关联//DC_DB_TIME-4
	MCMPF32     MR2, MR0
	MMOVF32     MR0, #0.0     ;;MR0付值为0


    MMOV32      MR2, MR3, LT ;;导通时间小于36，则把导通时间MR2改为20
    MMOVF32     MR3, #1.0
    MMOV32      MR0, MR3, LT ;;导通时间小于36，则把MR0改为1
    MMOV32      @_ClatoCpuVar._u16DcdcPWMChgFlag,MR0;袽R0付值给_u16DcdcPWMChgFlag
 ;   MMOV32      @_ClatoCpuVar._f32DcdcDutyShadow,MR2
;	MMOV32      @_ClatoCpuVar._f32DcdcPWMTsShadow,MR1
    MMPYF32     MR1,MR1,#0.5 ;f32DcdcPWMTsShadow*0.5给MR1作为PWM的PRD值

    MMPYF32     MR2,MR2,#0.5 ;f32DcdcDutyShadow*0.5给MR2作为PWM的CMP值
    MMOV32      @_ClatoCpuVar._f32DcdcPWMPRDShadow,MR1
    MMOV32      @_ClatoCpuVar._f32DcdcPWMCMPShadow,MR2
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;环路计算结束;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;@_ClatoCpuVar._f32DcdcPWMPRDShadow=MR1;;;;;;;;;;;;
;;;@_ClatoCpuVar._f32DcdcPWMCMPShadow=MR2;;;;;;;;;;;


;;;;;;;;;;;;;;;;;发波设置;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
 
;;;;;;;;;;;;;;;;;MR1;PRD 浮点 转换成整数和高精度部分;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   MFRACF32 MR3, MR1;;;;;;;把PRD的小数部分取出来放到MR3里
  ; MSUBF32  MR1,MR1,MR3;;;;MR1减去MR3里存的小数部分，整数部分存在MR1里
   MF32TOUI16 MR0, MR1 ;;;;把MR1中的整数部分取出来，转换成16位的整形存到MR0里
   MMOVF32   MR1,#65536.0
   MMPYF32  MR3,MR3,MR1
  ; MMPYF32  MR3,MR3,#65536.0;;;;把MR3里存的小数部分*65536;;;MMPYF32用立即数只有16位浮点数精度
   MF32TOUI16 MR1,MR3       ;;把MR3中的整数部分取出来，转换成16位的整形存到MR1里
   ;;;到此处，MR0中存放了PRD的整数部分，而MR1中存放了PRD的高精度部分;;;

;;;;;prd 设置；；；；

    
	MMOV16      @_EPwm4Regs.TBPRD, MR0
	MMOV16      @_EPwm4Regs.TBPRDHR, MR1
    MMOV16      @_EPwm5Regs.TBPRD, MR0
	MMOV16      @_EPwm5Regs.TBPRDHR, MR1
    MMOV16      @_EPwm6Regs.TBPRD, MR0
	MMOV16      @_EPwm6Regs.TBPRDHR, MR1
    MMOV16      @_EPwm7Regs.TBPRD, MR0
	MMOV16      @_EPwm7Regs.TBPRDHR, MR1


;;;;;;;;;;;;;;;;MR2;;CMP 浮点 转换成整数和高精度部分;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   MFRACF32 MR3, MR2;;;;;;;把CMP的小数部分取出来放到MR3里
   MF32TOUI16 MR0, MR2 ;;;;把MR2中的整数部分取出来，转换成16位的整形存到MR0里

   MMOVF32  MR2,#65536.0
   MMPYF32  MR3,MR3,MR2

   ;MMPYF32  MR3,MR3,#65536.0;;;;把MR3里存的小数部分*65536;;;MMPYF32用立即数只有16位浮点数精度
   MF32TOUI16 MR2,MR3       ;;把MR3中的整数部分取出来，转换成16位的整形存到MR2里
    ;;;到此处，MR0中存放了CMP的整数部分，而MR2中存放了CMP的高精度部分;;;
   
;;;;;cmp 设置；；；；
   MMOV32   MR1,@_ClatoCpuVar._u16DcdcPWMChgFlag
   MCMPF32  MR1, #0.0 ;;;如果_u16DcdcPWMChgFlag=1，则为间歇发波
   MNOP
   MNOP
   MNOP
   ;MBCNDD		DC_DC_PWM_NORMAL,EQ	;;;_u16DcdcPWMChgFlag=0,说明是双极性发波
;;;;;;;;;;;;;;;;;;发最小占空比时两个桥臂都是对称发;;;;;;CMPA=CMPB.bit.CMPB;;;;;;;;;;
 ;      MOVL	XAR7,#_EPwm4Regs + _CMPA; Addr = (ePWM1 _CMPA)
 ;		MOV 	*--,AH
 ;       MOV 	*,AL

;		MOVL	XAR7,#_EPwm7Regs + _CMPA; Addr = (ePWM1 _CMPA)
;		MOV 	*--,AH
;        MOV 	*,AL
     
 ;    	MOVL	XAR7,#_EPwm7Regs + _CMPB.bit.CMPB; Addr = (ePWM1 _CMPA)
  ;      MOV 	*,AH

  	MMOV16      @_EPwm4Regs.CMPA.bit.CMPA, MR0
    MMOV16      @_EPwm4Regs.CMPA.bit.CMPAHR, MR2
    MNOP
     
	MMOV16      @_EPwm7Regs.CMPA.bit.CMPA, MR0
    MMOV16      @_EPwm7Regs.CMPA.bit.CMPAHR, MR2

    MMOV16      @_EPwm7Regs.CMPB.bit.CMPB, MR0
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;      MOVL 	ACC,@_lDcdcPWMTsShadow        
  ;      SUBL 	ACC,@_lDcdcDutyShadow
;		SFR 	ACC,#1
;		MOVL	XAR7,#_EPwm5Regs + _CMPA; Addr = (ePWM2 _CMPA)
;		MOV 	*--,AH
 ;       MOV 	*,AL

;		MOVL	XAR7,#_EPwm6Regs + _CMPA; Addr = (ePWM1 _CMPA)
;		MOV 	*--,AH
  ;      MOV 	*,AL

  ;     	MOVL	XAR7,#_EPwm6Regs + _CMPB.bit.CMPB; Addr = (ePWM1 _CMPA)
  ;      MOV 	*,AH 
    MMOV32      MR1,@_ClatoCpuVar._f32DcdcPWMPRDShadow
    MMOV32      MR2,@_ClatoCpuVar._f32DcdcPWMCMPShadow
	MSUBF32     MR2, MR1, MR2    ;;;计算出另一组桥臂的CMP值
    ;;;;;;;;;;;;;;;;MR2;;CMP 浮点 转换成整数和高精度部分;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MFRACF32   MR3, MR2;;;;;;;把CMP的小数部分取出来放到MR3里
    MF32TOUI16 MR0, MR2 ;;;;把MR2中的整数部分取出来，转换成16位的整形存到MR0里

    MMOVF32   MR2,#65536.0
    MMPYF32   MR3,MR3,MR2

  ;  MMPYF32  MR3,MR3,#65536.0;;;;把MR3里存的小数部分*65536
    MF32TOUI16 MR2,MR3       ;;把MR3中的整数部分取出来，转换成16位的整形存到MR2里
    ;;;到此处，MR0中存放了CMP的整数部分，而MR2中存放了CMP的高精度部分;;;
    MMOV16      @_EPwm5Regs.CMPA.bit.CMPA, MR0
    MMOV16      @_EPwm5Regs.CMPA.bit.CMPAHR, MR2

	MMOV16      @_EPwm6Regs.CMPA.bit.CMPA, MR0
    MMOV16      @_EPwm6Regs.CMPA.bit.CMPAHR, MR2

    MMOV16      @_EPwm6Regs.CMPB.bit.CMPB, MR0

    MBCNDD		DC_DC_PWM_NORMAL_END,UNCF
	MNOP
    MNOP
    MNOP
 
DC_DC_PWM_NORMAL:
   ;;;;;;;;;;有限双极性发波，一个桥臂对称发波，另一个桥臂始终50%互补	；
;;************************************************************；；；
;;-----------------------初始化设置，设置为互补----------------;;
;;     PWM4(对称)                  .                                                                  PWM6（50%互补）
;;     CAU = AQ_CLEAR              .        CBU = AQ_SET（上升沿不需高精度）
;;     CAD = AQ_SET                .        CAD = AQ_CLEAR
;;     CMPA = MIN_CMPA             .        CMPA = _TBPRD - _MIN_CMPA
;;                                 .        CMPB = MIN_CMPA（中断付值此处COMPA、B值对调）
;;...........................................................;;
;;                                 .
;;     PWM5 (对称)                 .                                                               PWM7（50%互补）
;;     CAU = AQ_SET                .        CAU = AQ_CLEAR
;;     CAD = AQ_CLEAR              .        CBD = AQ_SET（上升沿不需高精度）
;;     CMPA = TBPRD - MIN_CMPA     .        CMPA = MIN_CMPA（中断付值此处COMPA、B值对调）
;;                                 .        CMPB = TBPRD - MIN_CMPA
;;-----------------------------------------------------------;;
;;************************************************************;;
   

    MMOV16      @_EPwm4Regs.CMPA.bit.CMPA, MR0
    MMOV16      @_EPwm4Regs.CMPA.bit.CMPAHR, MR2

	MMOV16      @_EPwm6Regs.CMPA.bit.CMPA, MR0
    MMOV16      @_EPwm6Regs.CMPA.bit.CMPAHR, MR2

    MMOV16      @_EPwm7Regs.CMPB.bit.CMPB, MR0


    MMOV32      MR1,@_ClatoCpuVar._f32DcdcPWMPRDShadow
    MMOV32      MR2,@_ClatoCpuVar._f32DcdcPWMCMPShadow
	MSUBF32     MR2, MR1, MR2    ;;;计算出另一组桥臂的CMP值
    ;;;;;;;;;;;;;;;;MR2;;CMP 浮点 转换成整数和高精度部分;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    MFRACF32 MR3, MR2;;;;;;;把CMP的小数部分取出来放到MR3里
    MF32TOUI16 MR0, MR2 ;;;;把MR2中的整数部分取出来，转换成16位的整形存到MR0里

	MMOVF32   MR2,#65536.0
    MMPYF32  MR3,MR3,MR2

   ;MMPYF32  MR3,MR3,#65536.0;;;;把MR3里存的小数部分*65536
    MF32TOUI16 MR2,MR3       ;;把MR3中的整数部分取出来，转换成16位的整形存到MR2里
    ;;;到此处，MR0中存放了CMP的整数部分，而MR2中存放了CMP的高炔糠?;;

    MMOV16      @_EPwm5Regs.CMPA.bit.CMPA, MR0
    MMOV16      @_EPwm5Regs.CMPA.bit.CMPAHR, MR2

	MMOV16      @_EPwm7Regs.CMPA.bit.CMPA, MR0
    MMOV16      @_EPwm7Regs.CMPA.bit.CMPAHR, MR2

    MMOV16      @_EPwm6Regs.CMPB.bit.CMPB, MR0
   


DC_DC_PWM_NORMAL_END:
  ; MDEBUGSTOP
   MUI16TOF32   MR3, @_CputoClaVar._u16DCADCCOEF ;34
   MMOV32       MR0, @_ClatoCpuVar._f32DcdcPWMPRDShadow

   MF32TOUI16 	MR1,MR0;qgf
   MMOV16     	@_ClatoCpuVar._u16viewflag2,MR1;qgf
   MMPYF32      MR3,MR3,MR0
   MMPYF32      MR2,MR3,#0.01 ;0.01*34=0.34
   ;MMOVF32      MR1, #2.0
   MUI16TOF32   MR1, @_CputoClaVar._u16VDCADCOffSet;24/2
   MSUBF32      MR2,MR2,MR1  ;;MR2=0.34*PRD-2作为采样点
  ; MF32TOUI16   MR3,MR2
  ; MMOV16       @_EPwm4Regs.CMPB.bit.CMPB, MR3   ;;更新EPWM3的COMPB，即采样点时刻

   MMPYF32      MR1,MR0,#2.0  ;2*PRD给MR1
   MMOVF32      MR3, #236.0 ;;;程序执行的长度的上限，算上进出中断的时间，应该比这个小。60M/250K=240 - 4 = 236
   MSUBF32      MR1,MR1,MR3 ;;MR1=2*PRD-236作为中断触发点
   MMOVF32      MR3,#4.0
   MSUBF32      MR0,MR0,MR3 ;;;PRD-4.0作为上限
   MMINF32      MR1,MR0     ;2*PRD-236 与 PRD-4.0 取小作为中断触发点
 ;  MF32TOUI16   MR2,MR1
  ; MMOV16         @_EPwm5Regs.CMPB.bit.CMPB, MR2

  
    ;MUI16TOF32  MR0, @_CputoClaVar._u16ShortFlag
	;MCMPF32     MR0, #0.0
    ;MBCNDD		No_Short_No_Change_Sample_Point,EQ
;When_Short_Change_Sample_Point:  ;;短路时为保证环路稳定，需要在LLC高频时也保证采样在中断前完成
    ;MNOP;;;;;加两句NOP，如果没短路，不要把采样点MR2限制到点比中断点小10
    ;MNOP
    MMOVF32      MR3,#10.0
    MSUBF32      MR0,MR1,MR3 ;中断触发点减10作为采样点的上限
    MMINF32      MR2,MR0 ;;限制采样点比中断点小10
;No_Short_No_Change_Sample_Point:
    MMaxF32      MR2, #4.0  ;;采样点时刻不小于上升沿的4CLK

	MF32TOUI16   MR3,MR2  
    ;MMOV16       @_EPwm4Regs.CMPB.bit.CMPB, MR3   ;;更新EPWM4的COMPB，即采样点时刻
    MMOV16       @_EPwm4Regs.CMPC, MR3;;更新EPWM6的CMPC，即输出电压采样点时刻
    MMOV16     	 @_ClatoCpuVar._u16viewflag3,MR3
;;-----------------------------------------------------------输出电流采样时刻
    MMOV32       MR0, @_ClatoCpuVar._f32DcdcPWMPRDShadow
    MMPYF32      MR3,MR0,#34.0
    MMPYF32      MR2,MR3,#0.01 ;0.01*34=0.34
    MUI16TOF32   MR3, @_CputoClaVar._u16IDCADCOffSet;/24/2
    MSUBF32      MR2,MR2,MR3
 ;;------------------------上下限
    MMOVF32      MR3,#10.0
    MSUBF32      MR0,MR1,MR3 ;中断触发点减10作为采样点的上限
    MMINF32      MR2,MR0 ;;限制采样点比中断点小10
    MMaxF32      MR2, #4.0
    MF32TOUI16   MR3,MR2
    MMOV16       @_EPwm4Regs.CMPB.bit.CMPB, MR3 ;;更新EPWM4的CMPB，即输出电流采样点时刻
    MMOV16     	 @_ClatoCpuVar._u16viewflag4,MR3
;;------------------------------------------------------------
    MF32TOUI16   MR0,MR1
    ;MADDF32		 MR1, MR2, #10.0;qgf
    ;MF32TOUI16   MR0,MR1;qgf
    MMOV16       @_EPwm5Regs.CMPB.bit.CMPB, MR0
    ;MMOV16     	 @_ClatoCpuVar._u16viewflag4,MR0
    MMOVXI       MR0, #0x0001
    MMOV16       @_EPwm5Regs.ETCLR, MR0
;;;;;;;;;;;测试进出中断的时刻;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
 ;   MNOP
;	  MUI16TOF32   MR1,@_EPwm5Regs.TBCTR
  ;   MUI16TOF32   MR0,@_EPwm5Regs.TBCTR
  ;   MF32TOUI16   MR2, MR1
 ;;    MF32TOUI16   MR3, MR0
  ;   MMOV16       @_ClatoCpuVar._u16viewflag3, MR2
  ;   MMOV16       @_ClatoCpuVar._u16viewflag4, MR3
;;;;;;;;;;;测试进出中断的时刻;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; 
;	MNOP
	MSTOP
    MNOP
    MNOP
    MNOP

_Cla1T5End:


          
_Cla1Task8:
	MMOVIZ		MR0,#0.0
	;ClatoCpuVar变量初始化清零
ClatoCpuVar_Init:	
	MMOV16		MAR0,MR0,#0x1480
    MUI16TOF32	MR0,  MR0                    
    MADDF32		MR0,  MR0, #1.0               
    MCMPF32		MR0,  #128.0       ;;;只有16位浮点数精度
    MF32TOUI16	MR0,  MR0                     
   ; MNOP        ;;;;;这个地方原来都有这个NOP，可是按照跳转超前比较3句似乎多了这个NOP
    MMOVIZ		MR2,  #0.0
	MMOV16		*MAR0,MR2
	MBCNDD		ClatoCpuVar_Init,LEQ
	MNOP
	MNOP
	MNOP

	;;;;DC PRD 和CMP的初始化;;;;;;;;
	MMOVF32		MR1,#120.0 ;
    MMOVF32		MR2,#4.0;
	MMOV32      @_ClatoCpuVar._f32DcdcPWMPRDShadow,MR1
    MMOV32      @_ClatoCpuVar._f32DcdcPWMCMPShadow,MR2

	;;;;DC 发波调频调宽计算的斜率和截距的初始化;;;;;;;;
    MMOVF32     MR1,#-0.2692307692  ;
    MMOVF32     MR2,#0.0001846154;

	MMOV32		@_ClatoCpuVar._f32DcdcPWMDutyD,MR1
    MMOV32		@_ClatoCpuVar._f32DcdcPWMDutyK,MR2
    ;;;;DC 功率环单零单极计算的K1，K2，K3的初始化;;;;;;;;
    MMOVF32		MR1,#0.977633492 ;
 	MMOVF32		MR2,#0.505591627 ;
   	MMOVF32		MR3,#-0.483225119;
	MMOV32      @_ClatoCpuVar._f32IdcLoopFilterK1,MR1;按250K初始化环路系数，去掉Q14定标
	MMOV32      @_ClatoCpuVar._f32IdcLoopFilterK2,MR2;
	MMOV32      @_ClatoCpuVar._f32IdcLoopFilterK3,MR3; 
    ;;;;DC 功率环PI计算的K3，K4的初始化;;;;;;;;
     MMOVF32		MR1,#0.04520970641 ;;按250K初始化环路系数，去掉Q21定标
    MMOVF32		MR2,#-0.04320970641 ;
	MMOV32      @_ClatoCpuVar._f32PowerLoopK3,MR1;
	MMOV32      @_ClatoCpuVar._f32PowerLoopK4,MR2; 
    ;;;;DC 间歇发波模式的初始化;;;;;;;;
    MMOVF32		MR1,#1.0 
	MMOV32      @_ClatoCpuVar._u16DcdcPWMChgFlag,MR1;初始化为间歇发波

   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
   ;;;;为了在变化频率的DC发波中断里变频计算控制器而设置的变量的初始化;;;;;
   ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

    ;;;DC功率环控制器的计算频率的初始化;;;;
    MMOVF32		MR1,#0.000004 
	MMOV32      @_ClatoCpuVar._f32DcdcPWMTs,MR1;初始化为250K对应的Ts

	;;;DC功率环SPSZ控制器增益,极点，零点,;;;;;;;;
	MMOVF32		MR1,#1.0;3.0;
    MMOVF32		MR2,#900.0 ;
   	MMOVF32		MR3,#1800.0;
    MMOV32      @_ClatoCpuVar._f32IdcLoopKv,MR1   ;DC功率环SPSZ控制器增益
	MMOV32      @_ClatoCpuVar._f32IdcLoopfp,MR2   ;DC功率环SPSZ控制器极点
	MMOV32      @_ClatoCpuVar._f32IdcLoopfz,MR3   ;DC功率环SPSZ控制器零点

    MMOVF32		MR1,#20.3575204;3.0;
    MMOVF32		MR2,#0.049121896;
	MMOV32      @_ClatoCpuVar._f32IdcSZSPTsfpfzPI,MR1
    MMOV32      @_ClatoCpuVar._f32IdcSZSPTsfpfzPIINV,MR2

    ;;;DC功率环PI控制器增益,零点,INV(2PI*Kz);;;;;;;;;
	MMOVF32		MR1,#650.0 ;把SPSZ的增益3乘到PI增益上
    MMOVF32		MR2,#1800.0 ;
   	MMOVF32		MR3,#0.0000884194128;
    MMOV32      @_ClatoCpuVar._f32PowerLoopKv,MR1   ;;DC功率环PI控制器增益
	MMOV32      @_ClatoCpuVar._f32PowerLoopfz,MR2   ;DC功率环PI控制器零点
	MMOV32      @_ClatoCpuVar._f32PowerLoop2PIfzINV,MR3;;DC功率环PI控制器INV(2PI*Kz)

		
    MSTOP
	MNOP
    MNOP
    MNOP
_Cla1T8End:

    
    
_Cla1Prog_End:


    .end
    ;.include "CLAShared.h"



