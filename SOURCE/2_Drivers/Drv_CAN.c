//###########################################################################
//
// FILE:   F2837xS_can.c
//
// TITLE:  F2837xS CAN Support Functions.
//
//###########################################################################
// $TI Release: F2837xS Support Library v3.05.00.00 $
// $Release Date: Thu Oct 18 15:50:26 CDT 2018 $
// $Copyright:
// Copyright (C) 2014-2018 Texas Instruments Incorporated - http://www.ti.com/
//
// Redistribution and use in source and binary forms, with or without 
// modification, are permitted provided that the following conditions 
// are met:
// 
//   Redistributions of source code must retain the above copyright 
//    notice, this list of conditions and the following disclaimer.
// 
//   Redistributions in binary form must reproduce the above copyright
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the   
//   distribution.
// 
//   Neither the name of Texas Instruments Incorporated nor the names of
//   its contributors may be used to endorse or promote products derived
//   from this software without specific prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
// OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 // LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
// DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
// THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
// OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// $
//###########################################################################

//
// Included Files
//
#include "f28004x_device.h"     // Headerfile Include File
#include "Drv_CAN.h"
#include <App_constant.h>
void InitCanaGpio(void);
void InitCana(void);
void InitCanaID(void);
void InitCanaLoad(void);
uint32_t setCANBitRate(uint32_t sourceClock, uint32_t bitRate);
void setupMessageObject(uint32_t objID, uint32_t msgID, msgObjType msgType);
static inline void CANA_initRAM();


/*******************************************************************************
*Function name: InitCan()
*Description:   This function initializes the CAN module  to a known state
*******************************************************************************/
void InitCan(void)
{
    InitCanaGpio();

    InitCana();

    // Setup CAN to be clocked off the SYSCLKOUT
    //
    //ClkCfgRegs.CLKSRCCTL2.bit.CANABCLKSEL = 0;

    InitCanaID();

}

void InitCanaID(void)
{

        // Setup CAN to be clocked off the SYSCLKOUT
        //
        ClkCfgRegs.CLKSRCCTL2.bit.CANABCLKSEL = 0;
        // messages.
        // ����5��Ϊ����
        setupMessageObject(CAN_TX_MSG_OBJ_1_70, CAN_MSG_ID_70, MSG_OBJ_TYPE_TRANSMIT);
        setupMessageObject(CAN_TX_MSG_OBJ_2_60, CAN_MSG_ID_60, MSG_OBJ_TYPE_TRANSMIT);

        //
        // Initialize the message object that will be used for receiving CAN
        // messages.
        // ����1��Ϊ����
    setupMessageObject(CAN_RX_MSG_OBJ, CAN_MSG_ID_RX, MSG_OBJ_TYPE_RECEIVE);



     //
    // Enable the CAN for operation.
    //
    CanaRegs.CAN_CTL.bit.ABO = 1;
    CanaRegs.CAN_ABOTR=128;
    CanaRegs.CAN_CTL.bit.Init = 0;
    CanaRegs.CAN_CTL.bit.CCE = 0;
    //CanaRegs.CAN_CTL.bit.DAR=1;
    //CanaRegs.CAN_CTL.bit.ABO = 1;

}
void InitCanLoad(void)
{
    InitCanaGpio();

    InitCana();

    // Setup CAN to be clocked off the SYSCLKOUT
    //
    ClkCfgRegs.CLKSRCCTL2.bit.CANABCLKSEL = 0;

    //InitCanaID();

        //
      // Initialize the message object that will be used for sending CAN
      // messages.
     // ����5��Ϊ����
    setupMessageObject(CAN_TX_MSG_OBJ_1_load, CAN_MSG_ID_load, MSG_OBJ_TYPE_TRANSMIT);
    setupMessageObject(CAN_TX_MSG_OBJ_2_load, CAN_MSG_ID_load, MSG_OBJ_TYPE_TRANSMIT);

    //
    // Initialize the message object that will be used for receiving CAN
    // messages.
    // ����1��Ϊ����
    setupMessageObject(CAN_RX_MSG_OBJ, CAN_MSG_ID_load, MSG_OBJ_TYPE_RECEIVE);



     //
    // Enable the CAN for operation.CanaRegs.CAN_NDAT_21
    //
    CanaRegs.CAN_CTL.bit.ABO = 1;
    CanaRegs.CAN_ABOTR=128;
    CanaRegs.CAN_CTL.bit.Init = 0;
    CanaRegs.CAN_CTL.bit.CCE = 0;
    //CanaRegs.CAN_CTL.bit.DAR=1;
    //CanaRegs.CAN_CTL.bit.ABO = 1;

}
/*void InitCanaID(void)
{
    ;
}*/
/*void InitCanaID(void)
{
    //
       // Initialize the message object that will be used for sending CAN
       // messages.
       // ����5��Ϊ����
    setupMessageObject(CAN_TX_MSG_OBJ_1, CAN_MSG_ID_70, MSG_OBJ_TYPE_TRANSMIT);
    setupMessageObject(CAN_TX_MSG_OBJ_2, CAN_MSG_ID_60, MSG_OBJ_TYPE_TRANSMIT);

    //
    // Initialize the message object that will be used for receiving CAN
    // messages.
    // ����1��Ϊ����
    setupMessageObject(CAN_RX_MSG_OBJ, CAN_MSG_ID_RX, MSG_OBJ_TYPE_RECEIVE);


}*/
/*******************************************************************************
*Function name: InitCanaGpio()
*Description:   This function initializes GPIO pins to function as eCAN pins
*******************************************************************************/
//  Each GPIO pin can be configured as a GPIO pin or up to 3 different
// peripheral functional pins. By default all pins come up as GPIO
// inputs after reset.
//
// Caution:
// Only one GPIO pin should be enabled for CANTXA/B operation.
// Only one GPIO pin shoudl be enabled for CANRXA/B operation.
// Comment out other unwanted lines.
void InitCanaGpio(void)
{
   Uint16 GpioMux;
   EALLOW;

    // Enable internal pull-up for the selected CAN pins
    // Pull-ups can be enabled or disabled by the user.
    // This will enable the pullups for the specified pins.
    // Comment out other unwanted lines.
    GpioCtrlRegs.GPAPUD.bit.GPIO30 = 0;     // Enable pull-up for GPIO32 (CANRXA)
    GpioCtrlRegs.GPAPUD.bit.GPIO31 = 0;     // Enable pull-up for GPIO33 (CANTXA)

    // Set qualification for selected CAN pins to asynch only
    // Inputs are synchronized to SYSCLKOUT by default.
    // This will select asynch (no qualification) for the selected pins.
    GpioCtrlRegs.GPAQSEL2.bit.GPIO30 = 3;   // Asynch qual for GPIO33 (CANRXA)

    // Configure eCAN-A pins using GPIO regs
    // This specifies which of the possible GPIO pins will be eCAN functional pins.
    // Configure GPIO32 for CANTXA operation
    GpioMux = 01;  //CANA_TX
    GpioCtrlRegs.GPAGMUX2.bit.GPIO31 = GpioMux >> 2;
    GpioCtrlRegs.GPAMUX2.bit.GPIO31 = GpioMux & 0x03;


    // Configure GPIO33 for CANRXA operation
    GpioMux = 01;  //CANA_RX
    GpioCtrlRegs.GPAGMUX2.bit.GPIO30 = GpioMux >> 2;
    GpioCtrlRegs.GPAMUX2.bit.GPIO30 = GpioMux & 0x03;


    GPIO_SetupPinOptions(30, GPIO_INPUT, GPIO_ASYNC);
    GPIO_SetupPinOptions(31, GPIO_OUTPUT, GPIO_PUSHPULL);

    EDIS;
}

//
// InitCAN - Initializes the CAN-A controller after reset.
//
void InitCana(void)
{
	int16_t iMsg;

	//
    // Place CAN controller in init state, regardless of previous state.  This
    // will put controller in idle, and allow the message object RAM to be
    // programmed.
	//
	 //EALLOW;
	CanaRegs.CAN_CTL.bit.Init = 1;
	CanaRegs.CAN_CTL.bit.PMD = 5;

    CANA_initRAM();

     EALLOW;
	 CanaRegs.CAN_CTL.bit.SWR = 1;
	 EDIS;

	 //
    // Delay for 14 cycles
    //
    DELAY_US(20L);


	CanaRegs.CAN_CTL.bit.CCE = 1;
	//
    // Wait for busy bit to clear
	//
    while(CanaRegs.CAN_IF1CMD.bit.Busy)
    {
    }

    //
    // Clear the message value bit in the arbitration register.  This indicates
    // the message is not valid and is a "safe" condition to leave the message
    // object.  The same arb reg is used to program all the message objects.
    //
    /*CanaRegs.CAN_IF1CMD.bit.DIR = 1;
    CanaRegs.CAN_IF1CMD.bit.Arb = 1;
    CanaRegs.CAN_IF1CMD.bit.Control = 1;

    CanaRegs.CAN_IF1ARB.all = 0;

    CanaRegs.CAN_IF1MCTL.all = 0;

    CanaRegs.CAN_IF2CMD.bit.DIR = 1;
    CanaRegs.CAN_IF2CMD.bit.Arb = 1;
    CanaRegs.CAN_IF2CMD.bit.Control = 1;

    CanaRegs.CAN_IF2ARB.all = 0;

    CanaRegs.CAN_IF2MCTL.all = 0;*/

    //
    // Loop through to program all 32 message objects
    //
    for(iMsg = 1; iMsg <= 32; iMsg+=2)
    {
        //
    	// Wait for busy bit to clear
    	//
        while(CanaRegs.CAN_IF1CMD.bit.Busy)
        {
        }

        //
        // Initiate programming the message object
        //
        CanaRegs.CAN_IF1CMD.bit.MSG_NUM = iMsg;

        //
        // Wait for busy bit to clear
        //
        while(CanaRegs.CAN_IF2CMD.bit.Busy)
        {
        }

        //
        // Initiate programming the message object
        //
        CanaRegs.CAN_IF2CMD.bit.MSG_NUM = iMsg + 1;
    }

    iMsg = CanaRegs.CAN_CTL.all;
    CanaRegs.CAN_CTL.bit.Init = 1;
    CanaRegs.CAN_CTL.bit.CCE = 1;
    //CanaRegs.CAN_CTL.bit.IE0 = 1;
    //
    // Now add in the pre-scalar on the bit rate.
    //
    CanaRegs.CAN_BTR.all = 0x000058F1;
    //
    // Restore the saved CAN Control register.
    //
    CanaRegs.CAN_CTL.all = iMsg;

    //EDIS;

}
//
// setupMessageObject - Setup message object as Transmit or Receive
//
/******************************************************************************
 *Function name: void setupMessageObject(uint32_t objID, uint32_t msgID, msgObjType msgType)
 *Description :  setup the message Object ��Ҫ��������CMD,MASK,
 *input:         objID: mailbox number
 *input:         msgID: message ID
 *input:         msgType: receive or sent
 *global vars:

 *output:        void
 *CALLED BY:     void InitCan(void)
 *****************************************************************************/
void setupMessageObject(uint32_t objID, uint32_t msgID, msgObjType msgType)
{
    uint32_t maskReg = 0U;
    //
    // Use Shadow variable for IF1CMD. IF1CMD should be written to in
    // single 32-bit write.
    //
    union CAN_IF1CMD_REG CAN_IF1CMD_SHADOW;

    //
    // Wait for busy bit to clear.
    //
    while(CanaRegs.CAN_IF1CMD.bit.Busy)
    {
    }

    //
    // Clear and Write out the registers to program the message object.
    //
    CAN_IF1CMD_SHADOW.all = 0;
    //CanaRegs.CAN_IF1MSK.all = 0;
    //CanaRegs.CAN_IF1ARB.all = 0;
    //CanaRegs.CAN_IF1MCTL.all = 0;

    //
    // Set the Control, Mask, and Arb bit so that they get transferred to the
    // Message object.
    //
    /*CAN_IF1CMD_SHADOW.bit.Control = 1;
    CAN_IF1CMD_SHADOW.bit.Arb = 1;
    CAN_IF1CMD_SHADOW.bit.Mask = 1;
    CAN_IF1CMD_SHADOW.bit.DIR = 1; //��ʼ��ʱ������IFX��RAMд��Ϣ
	*/
    //
    // Configure the Mask Registers for 29 bit Identifier mask.

   // maskReg = msgID & CAN_IF1MSK_MSK_M;
    maskReg = VERTIV_CAN_PROTNO;

    //
    // If the caller wants to filter on the extended ID bit then set it.
    //
    maskReg |= CAN_MSG_OBJ_USE_EXT_FILTER;

    //
    // The caller wants to filter on the message direction field.
    //
    maskReg |= CAN_MSG_OBJ_USE_DIR_FILTER;

    CanaRegs.CAN_IF1MSK.all = maskReg;

    //
    // Set direction to transmit
    //
    if(msgType == MSG_OBJ_TYPE_TRANSMIT)
    {
       CanaRegs.CAN_IF1ARB.bit.Dir = 1;
    }
    else
    {
        CanaRegs.CAN_IF1ARB.bit.Dir = 0;
    }
    //
    // Set Message ID (this example assumes 29 bit ID mask)
    //

    CanaRegs.CAN_IF1ARB.bit.ID = msgID;
    CanaRegs.CAN_IF1ARB.bit.MsgVal = 1;
    CanaRegs.CAN_IF1ARB.bit.Xtd = 1;

    //
    // Set the data length since this is set for all transfers.  This is
    // also a single transfer and not a FIFO transfer so set EOB bit.
    //
    CanaRegs.CAN_IF1MCTL.bit.DLC = 8;
    CanaRegs.CAN_IF1MCTL.bit.EoB = 1;
    CanaRegs.CAN_IF1MCTL.bit.UMask = 1; //use Mask


	CAN_IF1CMD_SHADOW.bit.Control = 1;
    CAN_IF1CMD_SHADOW.bit.Arb = 1;
    CAN_IF1CMD_SHADOW.bit.Mask = 1;
    CAN_IF1CMD_SHADOW.bit.DIR = 1; 
	
    //
    // Transfer data to message object RAM
    //
    CAN_IF1CMD_SHADOW.bit.MSG_NUM = objID; //ʹ���ĸ�����
    CanaRegs.CAN_IF1CMD.all = CAN_IF1CMD_SHADOW.all;


}


//*****************************************************************************
//
//! Initializes the CAN controller's RAM.
//!
//! \param base is the base address of the CAN controller.
//!
//! Performs the initialization of the RAM used for the CAN message objects.
//!
//! \return None.
//
//*****************************************************************************
static inline void CANA_initRAM()
{
    //
    // Check the arguments.
    //
    CanaRegs.CAN_RAM_INIT.all = CAN_RAM_INIT_CAN_RAM_INIT |
            CAN_RAM_INIT_KEY;
    while(!((CanaRegs.CAN_RAM_INIT.all & CAN_RAM_INIT_MASK) ==
            (CAN_RAM_INIT_RAM_INIT_DONE | CAN_RAM_INIT_KEY2 |
             CAN_RAM_INIT_KEY0)))
    {
            //
            // Wait until RAM Init is complete
            //
    }

}
//mail box 4: transmit
void sCanResetTx5(void)
{
    //struct ECAN_REGS ECanaShadow;
    union CAN_IF1CMD_REG CAN_IF1CMD_SHADOW;

//  // send data in box 4 success,  Reset TA4
    EALLOW;
    //ECanaShadow.CANTA.all = ECanaRegs.CANTA.all;
    //ECanaShadow.CANTA.bit.TA5 = 1;
    //ECanaRegs.CANTA.all = ECanaShadow.CANTA.all;

    //CanaRegs.

    CAN_IF1CMD_SHADOW.all = CanaRegs.CAN_IF1CMD.all ;

    CAN_IF1CMD_SHADOW.bit.TxRqst = 0;

    CanaRegs.CAN_IF1CMD.all=CAN_IF1CMD_SHADOW.all;
    EDIS;
}


/*******************************************************************************
*Description:      Read message from the message 1 through IF2
*******************************************************************************/
void sCanHdRead1(CANFRAME *pdata)
{
    //
    // Use Shadow variable for IF2CMD. IF2CMD should be written to in
    // single 32-bit write.
    //
    union CAN_IF2CMD_REG CAN_IF2CMD_SHADOW;

    //
    // Set the Message Data A, Data B, and control values to be read
    // on request for data from the message object.
    //
    CAN_IF2CMD_SHADOW.all = 0;
    CAN_IF2CMD_SHADOW.bit.Control = 1;
    CAN_IF2CMD_SHADOW.bit.Arb = 1;
    CAN_IF2CMD_SHADOW.bit.DATA_A = 1;
    CAN_IF2CMD_SHADOW.bit.DATA_B = 1;

    //
    // Transfer the message object to the message object IF register.
    //
    CAN_IF2CMD_SHADOW.bit.MSG_NUM = CAN_RX_MSG_OBJ;
    CanaRegs.CAN_IF2CMD.all = CAN_IF2CMD_SHADOW.all;

    //
    // Wait for busy bit to clear.
    //
    while(CanaRegs.CAN_IF2CMD.bit.Busy)
    {
    }

    //
    // See if there is new data available.
    //
    if(CanaRegs.CAN_IF2MCTL.bit.NewDat == 1)
    {
        //
        // Read out the data from the CAN registers.
        //
        pdata->CanId.all = (Uint32)CanaRegs.CAN_IF2ARB.bit.ID;
        pdata->CanData.byte.BYTE0 = CanaRegs.CAN_IF2DATA.byte.Data_0;
        pdata->CanData.byte.BYTE1 = CanaRegs.CAN_IF2DATA.byte.Data_1;
        pdata->CanData.byte.BYTE2 = CanaRegs.CAN_IF2DATA.byte.Data_2;
        pdata->CanData.byte.BYTE3 = CanaRegs.CAN_IF2DATA.byte.Data_3;
        pdata->CanDatb.byte.BYTE4 = CanaRegs.CAN_IF2DATB.byte.Data_4;
        pdata->CanDatb.byte.BYTE5 = CanaRegs.CAN_IF2DATB.byte.Data_5;
        pdata->CanDatb.byte.BYTE6 = CanaRegs.CAN_IF2DATB.byte.Data_6;
        pdata->CanDatb.byte.BYTE7 = CanaRegs.CAN_IF2DATB.byte.Data_7;

        //
        // Populate Shadow Variable
        //
        CAN_IF2CMD_SHADOW.all = CanaRegs.CAN_IF2CMD.all;

        //
        // Clear New Data Flag
        //
        CAN_IF2CMD_SHADOW.bit.TxRqst = 1;

        //
        // Transfer the message object to the message object IF register.
        //
        CAN_IF2CMD_SHADOW.bit.MSG_NUM = CAN_RX_MSG_OBJ;
        CanaRegs.CAN_IF2CMD.all = CAN_IF2CMD_SHADOW.all;
    }
}

/*******************************************************************************
*Description:      transmit message to the message 4 through IF1
*******************************************************************************/
void  vCanHdSend6(CANFRAME *pdata, Uint16 mailboxNo)
{
    // Use Shadow variable for IF2CMD. IF2CMD should be written to in
    // single 32-bit write.
    //

    union CAN_IF1CMD_REG CAN_IF1CMD_SHADOW;

    // Wait for busy bit to clear.
    //
    while(CanaRegs.CAN_IF1CMD.bit.Busy)
    {
    }

    CanaRegs.CAN_IF1DATA.byte.Data_0 = pdata->CanData.byte.BYTE0;
    CanaRegs.CAN_IF1DATA.byte.Data_1 = pdata->CanData.byte.BYTE1;
    CanaRegs.CAN_IF1DATA.byte.Data_2 = pdata->CanData.byte.BYTE2;
    CanaRegs.CAN_IF1DATA.byte.Data_3 = pdata->CanData.byte.BYTE3;
    CanaRegs.CAN_IF1DATB.byte.Data_4 = pdata->CanDatb.byte.BYTE4;
    CanaRegs.CAN_IF1DATB.byte.Data_5 = pdata->CanDatb.byte.BYTE5;
    CanaRegs.CAN_IF1DATB.byte.Data_6 = pdata->CanDatb.byte.BYTE6;
    CanaRegs.CAN_IF1DATB.byte.Data_7 = pdata->CanDatb.byte.BYTE7;


//    /*write MSGID*/
      CanaRegs.CAN_IF1ARB.bit.ID = (pdata->CanId.all) & 0x1FFFFFFF;
      CanaRegs.CAN_IF1ARB.bit.Dir = 1;
//    /*write MSGCTRL*/
//    CAN_IF1MCTL_SHADOW.bit.DLC = 8; //��ʼ���Ѿ�д��

    //
    // Set Direction to write and set DATA-A/DATA-B to be transfered to
    // message object
    //
    CAN_IF1CMD_SHADOW.all = 0;
    CAN_IF1CMD_SHADOW.bit.DIR = 1;
    CAN_IF1CMD_SHADOW.bit.DATA_A = 1;
    CAN_IF1CMD_SHADOW.bit.DATA_B = 1;
    CAN_IF1CMD_SHADOW.bit.Control = 1;
    CAN_IF1CMD_SHADOW.bit.Arb = 1;

    //
    // Set Tx Request Bit
    //
    CAN_IF1CMD_SHADOW.bit.TxRqst = 1;

    //
    // Transfer the message object to the message object specified by
    // objID.
    //
    CAN_IF1CMD_SHADOW.bit.MSG_NUM = mailboxNo;

    CanaRegs.CAN_IF1CMD.all = CAN_IF1CMD_SHADOW.all;

}

/*******************************************************************************
*Description:      transmit message to the message 5 through IF1
*******************************************************************************/
void    sCanHdSend5(CANFRAME *pdata)
{
    // Use Shadow variable for IF2CMD. IF2CMD should be written to in
    // single 32-bit write.
    //

    union CAN_IF1CMD_REG CAN_IF1CMD_SHADOW;

    // Wait for busy bit to clear.
    //
    while(CanaRegs.CAN_IF1CMD.bit.Busy)
    {
    }

    CanaRegs.CAN_IF1DATA.byte.Data_0 = pdata->CanData.byte.BYTE0;
    CanaRegs.CAN_IF1DATA.byte.Data_1 = pdata->CanData.byte.BYTE1;
    CanaRegs.CAN_IF1DATA.byte.Data_2 = pdata->CanData.byte.BYTE2;
    CanaRegs.CAN_IF1DATA.byte.Data_3 = pdata->CanData.byte.BYTE3;
    CanaRegs.CAN_IF1DATB.byte.Data_4 = pdata->CanDatb.byte.BYTE4;
    CanaRegs.CAN_IF1DATB.byte.Data_5 = pdata->CanDatb.byte.BYTE5;
    CanaRegs.CAN_IF1DATB.byte.Data_6 = pdata->CanDatb.byte.BYTE6;
    CanaRegs.CAN_IF1DATB.byte.Data_7 = pdata->CanDatb.byte.BYTE7;

//    /*write MSGID*/
    CanaRegs.CAN_IF1ARB.bit.ID = (pdata->CanId.all) & 0x1FFFFFFF;
    CanaRegs.CAN_IF1ARB.bit.Dir = 1;
//    /*write MSGCTRL*/
//    CAN_IF1MCTL_SHADOW.bit.DLC = 8; //��ʼ���Ѿ�д��

    //
    // Set Direction to write and set DATA-A/DATA-B to be transfered to
    // message object
    //
    CAN_IF1CMD_SHADOW.all = 0;
    CAN_IF1CMD_SHADOW.bit.DIR = 1;
    CAN_IF1CMD_SHADOW.bit.DATA_A = 1;
    CAN_IF1CMD_SHADOW.bit.DATA_B = 1;
    CAN_IF1CMD_SHADOW.bit.Control = 1;
    CAN_IF1CMD_SHADOW.bit.Arb = 1;

    //
    // Set Tx Request Bit
    //
    CAN_IF1CMD_SHADOW.bit.TxRqst = 1;

    //
    // Transfer the message object to the message object specified by
    // objID.
    //
    CAN_IF1CMD_SHADOW.bit.MSG_NUM = 5;
    CanaRegs.CAN_IF1CMD.all = CAN_IF1CMD_SHADOW.all;

}

//
// End of file
//
