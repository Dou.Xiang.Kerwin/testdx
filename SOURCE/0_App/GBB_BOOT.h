///*;***********************************************************************
//FILENAME:	GBB_BOOT.c
//
//TITLE:		GBB protocal bootloader function
//
//AUTHOR:
//
//PROJECT:
//
// PUBLIC:		none
//
//PRIVATE:	none
//
//				Copyright 2006-09-25 by ENPC
//**************************************************************************/

#ifndef GBB_BOOT_H
#define GBB_BOOT_H



#if (DSP28_28031PAG||DSP28_28030PAG||DSP28_28031PN||DSP28_28030PN)
    #define   Start_Of_Application   0x003F0000
    #define   Application_CRC   0x003F5FFF
#else
    #define   Start_Of_Application   0x082000
    #define   Application_CRC   0x08FFFF
	#endif
#endif

