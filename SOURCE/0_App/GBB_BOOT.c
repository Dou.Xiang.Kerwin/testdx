///*;***********************************************************************
//FILENAME:	GBB_BOOT.c
//
//TITLE:		GBB protocal bootloader function
//
//AUTHOR:
//
//PROJECT:
//
// PUBLIC:		none
//
//PRIVATE:	none
//
//				Copyright 2006-09-25 by ENPC
//**************************************************************************/
#include "f28004x_device.h"     // Headerfile Include File
#include "Drv_CPUTIMER.h"
#include "GBB_epromData.h"
#include "Prj_macro.h"
#include "GBB_constant.h"
#include <App_constant.h>
#include <App_main.h>
#include "GBB_BOOT.h"
#include "Drv_ECAP.h"
#include "Drv_I2C.h"
#include "Drv_GPIO.h"
#include "Drv_CAN.h"
#include  "Prj_config.h"
#include "Prj_Adc.h"


Uint16 Application_Valid(void);
void setMessageObject(uint32_t objID, union CANMSGID_REG MsgId, msgObjType msgType);
Uint16 uiWaitPassiveTriggerCommand(void);
/*************************************************************************
 *  FUNCTION NAME: void BOOT_MAIN (void)
 *
 *  DESCRIPTION:    bootloader entry
 *
 *  PARAMETERS: NONE
 *
 *  RETURNS:    NONE
 *
 *  CAVEATS:    NONE
**************************************************************************/
#pragma CODE_SECTION(BOOT_MAIN,"FlashBoot");
void BOOT_MAIN (void)
{
    union CANMSGID_REG MsgId;
    ubitfloat ubitfTemp;
    Uint16  uiPassiveTriggerDownload = 0;

    //Uint16 i=0;

    //Initialize System Control: PLL,enable Dog,InitPeripheralClocks
    InitSysCtrl();

    DisableDog();

    // Initalize GPIO: detail info in the DSP280x_Gpio.c file
    InitGpio();

    // Clear all interrupts and initialize PIE vector table: Disable CPU interrupts
    DINT;

    // Disable CPU interrupts and clear all CPU interrupt flags:
    IER = 0x0000;
    IFR = 0x0000;

    InitECapture();

    InitRAMPointer();

    /* have alreadly initialed in sysctrl.
    //MemCopy(&BootRamfuncsLoadStart, &BootRamfuncsLoadEnd, &BootRamfuncsRunStart);

    //InitFlash();
     */
 /*----------------------------------------------------------------------------
   为了能够开机时先判断是否AC过压，要把必要的PWM和ADC在BOOTMAIN区就初始化掉,
   在MAIN程序里再重新设置初始化,这样就可以不致于每次修改ADC和PWM都要用仿真器烧写.
    ----------------------------------------------------------------------------*/
//  InitEPwm();
 //   InitAdc();
   // ADC_PWM_Boot_Init();
    //add end

    // Initializes the I2C To default State:
    InitI2C();

    ubitfTemp = fReadFloatDataThreePointer (ENP_LO_SN_ADDR);//get Extended ENP Serial Number
    ulENP_Lo_SN = (UINT32)ubitfTemp.lData;
    ulENP_Hi_SN = fReadFloatDataThreePointer (ENP_HI_SN_ADDR);  //address from 120 to 127, all 8-bytes

    //InitCan();
    InitCanLoad();

    // Initialize CPU Timers To default State:
    InitCpuTimers();
    //Application_Entry_Point(); //20100505

    if(uiFromApplication)
    {
        Load_Code_And_Execute();
    }
    else
    {
//      //Setup_CAN
//      ECanaRegs.CANME.all = 0x00000000;
        MsgId.all = 0x1CA80703;

//      ECanaMboxes.MBOX1.MSGID.all = 0xDCA80703;//MBX1 PCUs receive,ProtNo = 0x1CA
//      ECanaMboxes.MBOX4.MSGID.all = 0xDCAF0003;//MBX4 PCU send
//
//      //ECanaMboxes.MBOX4.MSGID.bit.SRCADDR = uiMdlAddr;
//      ECanaLAMRegs.LAM1.all = 0x9FFFFFFF;
//      ECanaLAMRegs.LAM4.all = 0x9FFFFFFF;
//      ECanaRegs.CANME.all = 0x00000012;

        CanaRegs.CAN_IF1MSK.all = 0X9FFFFFFF;
        setMessageObject(CAN_RX_MSG_OBJ, MsgId, MSG_OBJ_TYPE_RECEIVE);


        MsgId.all = 0x1CAF0003;//MBX4 PCU send
        //MsgId.bit.SRCADDR = g_u16MdlAddr;
        setMessageObject(CAN_TX_MSG_OBJ_1_load, MsgId, MSG_OBJ_TYPE_TRANSMIT);


        if( Application_Valid())
        {
            uiPassiveTriggerDownload = uiWaitPassiveTriggerCommand();
            if(uiPassiveTriggerDownload == 0xF0)
            {
                Load_Code_And_Execute();
            }
            else
            {
                Application_Entry_Point();
            }
            //Application_Entry_Point();
        }
        else
        {
            for(;;)
            {
                uiPassiveTriggerDownload = uiWaitPassiveTriggerCommand();
                //mWDLedTwink();//for indication
                //mRedLedTwink();
                //mGreenLedTwink();
                //mYellowLedTwink();
                //mRedLedTwink();
                GpioDataRegs.GPBTOGGLE.bit.GPIO33 = 1;
                //mGreenLedTwink();
                GpioDataRegs.GPATOGGLE.bit.GPIO16 = 1;
                GpioDataRegs.GPATOGGLE.bit.GPIO17 = 1;
                //mYellowLedTwink();
                GpioDataRegs.GPATOGGLE.bit.GPIO11 = 1;
                GpioDataRegs.GPATOGGLE.bit.GPIO12 = 1;

                if(uiPassiveTriggerDownload == 0xF0)
                {
                    Load_Code_And_Execute();
                }
            }
        }
    }

    return;
}




/*************************************************************************
 *
 *  FUNCTION NAME: Watch_Dog_Kick
 *
 *  DESCRIPTION:	kick watch dog
 *
 *  PARAMETERS:		NONE
 *
 *  RETURNS:		NONE
 *
 *  CAVEATS:		NONE
 *
 ************************************************************************/
#pragma CODE_SECTION(Watch_Dog_Kick,"FlashBoot");
void Watch_Dog_Kick(void)
{
    EALLOW;
    WdRegs.WDKEY.bit.WDKEY = 0x0055;
    WdRegs.WDKEY.bit.WDKEY = 0x00AA;
    EDIS;
}

/**********************************************************************
 *  FUNCTION NAME: void StartTimer(Uint16 uiDelayTime)
 *
 *  DESCRIPTION:	start timer
 *
 *  PARAMETERS:	NONE
 *
 *  RETURNS:	none
 *
 *  CAVEATS:	none
***********************************************************************/
#pragma CODE_SECTION(StartTimer,"FlashBoot");
void StartTimer(Uint16 u16DelayTime)
{// each call TimeIsUp(void) takes 20uS
	u16WaitLoopCount = u16DelayTime;
	return;
}
/**********************************************************************
 *  FUNCTION NAME: Uint16 TimeIsUp(void)
 *
 *  DESCRIPTION:	time is up?
 *
 *  PARAMETERS:	NONE
 *
 *  RETURNS:	none
 *
 *  CAVEATS:	NONE
***********************************************************************/
#pragma CODE_SECTION(TimeIsUp,"FlashBoot");
Uint16 TimeIsUp(void)
{
	// each call takes 20uS
	Watch_Dog_Kick();
	if (u16WaitLoopCount == 0)
	{
		return(1);
	}
	DELAY_US(20L);//20100506
	u16WaitLoopCount--;
	return(0);
}

/*************************************************************************
 *
 *  FUNCTION NAME: InitRAM
 *
 *  DESCRIPTION:
 *
 *  PARAMETERS:		NONE
 *
 *  RETURNS:		NONE
 *
 *  CAVEATS:		NONE
 *
 ************************************************************************/
#pragma CODE_SECTION(InitRAM,"FlashBoot");
void InitRAM(void)
{
	//------------8000H-----------------------
	Uint16 *uiPTmp;

   //清空CAN通讯的数组变量
	for(uiPTmp = ((Uint16 *)CAN_ARRAY_RAM_START);uiPTmp<((Uint16 *)CAN_ARRAY_RAM_END);uiPTmp ++)
	{
		*uiPTmp = 0;
	}

	  //清空CAN VOL通讯的数组变量
	for(uiPTmp = ((Uint16 *)CAN_VOL_ARRAY_RAM_START);uiPTmp<((Uint16 *)CAN_VOL_ARRAY_RAM_END);uiPTmp ++)
	{
		*uiPTmp = 0;
	}

	for(uiPTmp = ((Uint16 *)BOOT_RAM_START);uiPTmp<((Uint16 *)BOOT_RAM_END);uiPTmp ++)
	{
		*uiPTmp = 0;
	}

	for(uiPTmp = ((Uint16 *)BOOT_ISR_RAM_START);uiPTmp< ((Uint16 *)BOOT_ISR_RAM_END);uiPTmp++)
	{//don't include variants of bootloader, ie. _uiFromApplication,and uiMdlAddr
		*uiPTmp = 0;
	}

	// clear variable
    for(uiPTmp = (Uint16 *)MAIN_RAM_START; uiPTmp <= (Uint16 *)MAIN_RAM_END; uiPTmp++)
    {
        *uiPTmp = 0;
    }

	for(uiPTmp = (Uint16 *)CPUTOCLA_RAM_START;uiPTmp<(Uint16 *)CPUTOCLA_RAM_END;uiPTmp ++)
	{
		*uiPTmp = 0;
	}

	return;
}
/*************************************************************************
 *
 *  FUNCTION NAME: Application_Valid
 *
 *  DESCRIPTION:	CRC-16 check program space is ok?
 *
 *  PARAMETERS:	NONE
 *
 *  RETURNS:		Uint16
 *
 *  CAVEATS:		NONE
 *
 ************************************************************************/
#pragma CODE_SECTION(Application_Valid,"FlashBoot");//Flash28_API
Uint16 Application_Valid(void)
{
  	Uint16	uiCRC=0, uiNonZeroDetect=0, temp;
  //	unsigned char z, i ;
    	Uint16 z, i ;
  	Uint16  *p,*crc_table;
  	i=0;
  	crc_table = (Uint16 *)0x00B000; //data placed in the RAM space
  	for(i=0;i<=255;i++)
 	{
		 z = i ^ (i >> 1);
		 z = z ^ (z >> 2);
		 z = z ^ (z >> 4);

	     *(crc_table+i) = z & 1 ? 0xC001 : 0;
		 *(crc_table+i) ^= i << 6 ^ i << 7;
	}

	p=(Uint16 *)Start_Of_Application;
	for (;(Uint32)p<=Application_CRC;p++)
	{
		//Watch_Dog_Kick();
		temp = *p;
	   	uiNonZeroDetect |= temp;
	   	uiCRC = (uiCRC >> 8) ^ *(crc_table + ((uiCRC ^ (temp >> 8))& 0x00ff));
	   	uiCRC = (uiCRC >> 8) ^ *(crc_table + ((uiCRC ^  temp      )& 0x00ff));
	}

	if ((uiCRC==0) && (uiNonZeroDetect !=0))
	{
		return(1);
	}
     return(1);
	// return(0);   //校验码不对则不能下载

}
/*******************************************************************************
*Description:      transmit message to the message 4 through IF1
*******************************************************************************/
#pragma CODE_SECTION(vCanMailSend,"FlashBoot");//Flash28_API
void  vCanMailSend(CANFRAME *pdata, Uint16 mailboxNo)
{
    // Use Shadow variable for IF2CMD. IF2CMD should be written to in
    // single 32-bit write.
    //

    union CAN_IF1CMD_REG CAN_IF1CMD_SHADOW;

    // Wait for busy bit to clear.
    //
    while(CanaRegs.CAN_IF1CMD.bit.Busy)
    {
    }

    CanaRegs.CAN_IF1DATA.byte.Data_0 = pdata->CanData.byte.BYTE0;
    CanaRegs.CAN_IF1DATA.byte.Data_1 = pdata->CanData.byte.BYTE1;
    CanaRegs.CAN_IF1DATA.byte.Data_2 = pdata->CanData.byte.BYTE2;
    CanaRegs.CAN_IF1DATA.byte.Data_3 = pdata->CanData.byte.BYTE3;
    CanaRegs.CAN_IF1DATB.byte.Data_4 = pdata->CanDatb.byte.BYTE4;
    CanaRegs.CAN_IF1DATB.byte.Data_5 = pdata->CanDatb.byte.BYTE5;
    CanaRegs.CAN_IF1DATB.byte.Data_6 = pdata->CanDatb.byte.BYTE6;
    CanaRegs.CAN_IF1DATB.byte.Data_7 = pdata->CanDatb.byte.BYTE7;


//    /*write MSGID*/
      CanaRegs.CAN_IF1ARB.bit.ID = (pdata->CanId.all) & 0x1FFFFFFF;
      CanaRegs.CAN_IF1ARB.bit.Dir = 1;
//    /*write MSGCTRL*/
//    CAN_IF1MCTL_SHADOW.bit.DLC = 8; //初始化已经写了

    //
    // Set Direction to write and set DATA-A/DATA-B to be transfered to
    // message object
    //
    CAN_IF1CMD_SHADOW.all = 0;
    CAN_IF1CMD_SHADOW.bit.DIR = 1;
    CAN_IF1CMD_SHADOW.bit.DATA_A = 1;
    CAN_IF1CMD_SHADOW.bit.DATA_B = 1;
    CAN_IF1CMD_SHADOW.bit.Control = 1;
    CAN_IF1CMD_SHADOW.bit.Arb = 1;

    //
    // Set Tx Request Bit
    //
    CAN_IF1CMD_SHADOW.bit.TxRqst = 1;

    //
    // Transfer the message object to the message object specified by
    // objID.
    //
    CAN_IF1CMD_SHADOW.bit.MSG_NUM = mailboxNo;
    CanaRegs.CAN_IF1CMD.all = CAN_IF1CMD_SHADOW.all;

}
/*:-------------------------------------------------------------------------
;   NAME:	    	VOID ProcessInquiry(U16 addr)
;
;   PURPOSE:	   Reply Bootloader version and Serial Number
;
;
;   ARGUMENTS:
;
;   RETURN VALUE:
;
;   GLOBALS:
;
;
;   REMARKS:	   This is a helper function of GetHeader() and GetDataBlock().
;
;-------------------------------------------------------------------------**/
#pragma CODE_SECTION(ProcessInquiry,"FlashBoot");
void ProcessInquiry(void)
{
	while (!TimeIsUp() && (CanaRegs.CAN_TXRQ_21 & 0x0010));//ECanaRegs.CANTRS.bit.TRS5

	if (!(CanaRegs.CAN_TXRQ_21 & 0x0010))
	{

		if (g_sBootCanRxData.CanId.bit.PROTNO == 0x1CB)
		{
		    //reply serial number
		    g_sBootCanTxData.CanData.word.Word_H = 0x00F0;
		    g_sBootCanTxData.CanData.word.Word_L = ulENP_Hi_SN.uintdata[0].id;
		    g_sBootCanTxData.CanDatb.all = ulENP_Lo_SN;

		}
		else if(g_sBootCanRxData.CanId.bit.PROTNO == 0x1CC)
		{
		    //reply Bootloader version
            g_sBootCanTxData.CanDatb.word.Word_H = *((Uint16 *)&Boot_Version);
            g_sBootCanTxData.CanDatb.word.Word_L = *((Uint16 *)&Boot_Version + 1);
            g_sBootCanTxData.CanData.all = 0x00F00000;

		}
		else
			return;


		g_sBootCanTxData.CanId.bit.DSTADDRH = 	(UINT16)(g_sBootCanRxData.CanId.bit.SRCADDR >>5);
		g_sBootCanTxData.CanId.bit.DSTADDRL = 	g_sBootCanRxData.CanId.bit.SRCADDR & ((UINT16)0x001F);
		g_sBootCanTxData.CanId.bit.PROTNO = g_sBootCanRxData.CanId.bit.PROTNO;

		vCanMailSend(&g_sBootCanTxData,5);
	}
}
/*:-------------------------------------------------------------------------
;   NAME:	    	VOID SendBlockRequest(Uint16 Status, Uint32 Addr, Uint16 Length)
;
;   PURPOSE:	   This function sends a First (Addr == 0) or Next Block of
;		      	Application Code request packet containing the specified
;		      	status word, image address, and length.
;
;   ARGUMENTS:	   Status = download status field (see defines.h)
						Addr = image address for packet(load address)
						Length = download block size
;
;   RETURN VALUE:	none
;
;   GLOBALS:	   none
;
;   REMARKS:	   The transmit arbitration field is already set up with
;						the CAN address, message priority, direction, and rtr.
;
;-------------------------------------------------------------------------**/
#pragma CODE_SECTION(SendBlockRequest,"FlashBoot");
void SendBlockRequest(Uint16  uiStatus, Uint32  ulAddr, Uint16  uiLength)
{
    union CAN_IF1CMD_REG CAN_IF1CMD_SHADOW;
  	while (!TimeIsUp() && (CanaRegs.CAN_TXRQ_21 & 0x0008));//ECanaRegs.CANTRS.bit.TRS4

  	if(!(CanaRegs.CAN_TXRQ_21 & 0x0008))
	{
        CAN_IF1CMD_SHADOW.bit.Arb = 1;
        CAN_IF1CMD_SHADOW.bit.DIR = 0;
        CAN_IF1CMD_SHADOW.bit.MSG_NUM = 4;
        CanaRegs.CAN_IF1CMD.all = CAN_IF1CMD_SHADOW.all;
        g_sBootCanTxData.CanId.all = (Uint32)CanaRegs.CAN_IF1ARB.bit.ID;

        if (ulAddr==0)
            g_sBootCanTxData.CanId.bit.PROTNO = 0x1C8;
        else
            g_sBootCanTxData.CanId.bit.PROTNO = 0x1C9;

        g_sBootCanTxData.CanData.word.Word_H = uiStatus;
        g_sBootCanTxData.CanData.word.Word_L = (Uint16)(ulAddr>>16);
        g_sBootCanTxData.CanDatb.word.Word_H = (Uint16)ulAddr;
        g_sBootCanTxData.CanDatb.word.Word_L = uiLength;

        vCanMailSend(&g_sBootCanTxData,4);

	}

   return;
}
#pragma CODE_SECTION(sCanMailReadBoot,"FlashBoot");
/*******************************************************************************
*Description:      Read message from the message 1 through IF2
*******************************************************************************/
void sCanMailReadBoot(CANFRAME *pdata,Uint16 mailboxNo)
{
    //
    // Use Shadow variable for IF2CMD. IF2CMD should be written to in
    // single 32-bit write.
    //
    union CAN_IF2CMD_REG CAN_IF2CMD_SHADOW;

    //
    // Set the Message Data A, Data B, and control values to be read
    // on request for data from the message object.
    //
    CAN_IF2CMD_SHADOW.all = 0;
    CAN_IF2CMD_SHADOW.bit.Control = 1;
    CAN_IF2CMD_SHADOW.bit.Arb = 1;
    CAN_IF2CMD_SHADOW.bit.DATA_A = 1;
    CAN_IF2CMD_SHADOW.bit.DATA_B = 1;

    //
    // Transfer the message object to the message object IF register.
    //
    CAN_IF2CMD_SHADOW.bit.MSG_NUM = mailboxNo;
    CanaRegs.CAN_IF2CMD.all = CAN_IF2CMD_SHADOW.all;

    //
    // Wait for busy bit to clear.
    //
    while(CanaRegs.CAN_IF2CMD.bit.Busy)
    {
    }

    //
    // See if there is new data available.
    //
    if(CanaRegs.CAN_IF2MCTL.bit.NewDat == 1)
    {
        //
        // Read out the data from the CAN registers.
        //
        pdata->CanId.all = (Uint32)CanaRegs.CAN_IF2ARB.bit.ID;
        pdata->CanCtrl.all = (Uint32)CanaRegs.CAN_IF2MCTL.all;
        pdata->CanData.byte.BYTE0 = CanaRegs.CAN_IF2DATA.byte.Data_0;
        pdata->CanData.byte.BYTE1 = CanaRegs.CAN_IF2DATA.byte.Data_1;
        pdata->CanData.byte.BYTE2 = CanaRegs.CAN_IF2DATA.byte.Data_2;
        pdata->CanData.byte.BYTE3 = CanaRegs.CAN_IF2DATA.byte.Data_3;
        pdata->CanDatb.byte.BYTE4 = CanaRegs.CAN_IF2DATB.byte.Data_4;
        pdata->CanDatb.byte.BYTE5 = CanaRegs.CAN_IF2DATB.byte.Data_5;
        pdata->CanDatb.byte.BYTE6 = CanaRegs.CAN_IF2DATB.byte.Data_6;
        pdata->CanDatb.byte.BYTE7 = CanaRegs.CAN_IF2DATB.byte.Data_7;



        //
        // Populate Shadow Variable
        //
        CAN_IF2CMD_SHADOW.all = CanaRegs.CAN_IF2CMD.all;

        //
        // Clear New Data Flag
        //
        CAN_IF2CMD_SHADOW.bit.TxRqst = 1;

        //
        // Transfer the message object to the message object IF register.
        //
        CAN_IF2CMD_SHADOW.bit.MSG_NUM = mailboxNo;
        CanaRegs.CAN_IF2CMD.all = CAN_IF2CMD_SHADOW.all;
    }
}

/*:-------------------------------------------------------------------------
;   NAME:	    Uint16 GetHeader(APPLICATIONHEADER *Header, Uint16 ImageAddr, Uint16 uiHeaderSize)
;
;   PURPOSE:	   This function fetches a header from offset ImageAddr in the
		      	application image.  It returns a flag indicating whether or not
				the attempt failed. Which tells the loader where to put the next
				download block.
;
;   ARGUMENTS:	   Header = pointer to an APPLICATIONHEADER structure to fill
		    	ImageAddr = offset into load image where we get the header;
				HeaderSize = size of header in bytes
;
;   RETURN VALUE: 	!0 = Header successfully loaded
;		    	0 = Attempt to load header failed
;
;   GLOBALS:	    Uint16 CANAddress
;
;   REMARKS:	    none
;
;-------------------------------------------------------------------------**/
#pragma CODE_SECTION(GetHeader,"FlashBoot");
Uint16 GetHeader(APPLICATIONHEADER *Header, Uint16  uiImageAddr, Uint16 uiHeaderSize)
{
	Uint16 uiTry;


	if (uiImageAddr == 0)
		uiTry = FIRSTTRIES; //255
	else
		//uiTry = TRIES;	//5
	    uiTry = 250;  //5
   	for (;uiTry--;)
   	{
		StartTimer(TIME_1000mS);

		SendBlockRequest(BLOCKACKNOWLEDGE,uiImageAddr,uiHeaderSize);

		while (!TimeIsUp())
		{
		    if((CanaRegs.CAN_NDAT_21 & 0x0001))
			{
		        // FILTER 3 HIT is header
		        //receive mailbox 1's info
		        sCanMailReadBoot(&g_sBootCanRxData,1);

				if(((((UINT16)(g_sBootCanRxData.CanId.bit.DSTADDRH << 5))
					| g_sBootCanRxData.CanId.bit.DSTADDRL) == g_u16MdlAddr)
					&& (g_sBootCanRxData.CanId.bit.SRCADDR == 0x0E0))
				{
				   	if ((g_sBootCanRxData.CanId.bit.PROTNO == 0x1D0)
				   		&& (g_sBootCanRxData.CanCtrl.bit.DLC == uiHeaderSize))
				   	{
						if(uiHeaderSize == SIZE_OF_BOOTLOADERHEADER)
						{
							Header->uiLoadAddress = (Uint16 *)g_sBootCanRxData.CanData.word.Word_H;
							Header->uiLength = g_sBootCanRxData.CanData.word.Word_L;
						}
						else
						{
							Header->uiLoadAddress = (Uint16 *)g_sBootCanRxData.CanData.all;
							Header->uiLength = g_sBootCanRxData.CanDatb.word.Word_H;
						}

						return(1);
				    }
				    else
				    {
				    	ProcessInquiry();
				    }
				}

			}
		}
   }
   return(0);
}

/*:-------------------------------------------------------------------------
;   NAME:	    	GetDataBlock(Uint16 *Destination, Uint16 Length, Uint16 ImageAddr)
;
;   PURPOSE:	   This function fetches a data block from the application image
;						and stuffs it into RAM.
;
;   ARGUMENTS:	   Destination = where to put it
						Length = how many bytes to get
;		    			ImageAddr = offset into load image where we get the data block;
;
;   RETURN VALUE: !0 = Data successfully loaded
;		    			 0 = Attempt to load data failed
;
;   GLOBALS:	    none
;
;   REMARKS:	    none
;
;-------------------------------------------------------------------------**/
#pragma CODE_SECTION(GetDataBlock,"FlashBoot");
Uint16 GetDataBlock(Uint16  *uiDestination, Uint16  uiLength, Uint32  ulImageAddr)
{
	Uint16 uiPacketAddr, uiPacket, uiStatus, uiTry;

	uiStatus = BLOCKACKNOWLEDGE;

	for (uiTry = 250; uiTry--; )

	{
		uiPacketAddr = 0;
		uiPacket = 0x1D0;
		StartTimer(TIME_1000mS);

		SendBlockRequest(uiStatus, ulImageAddr, uiLength);

		while (!TimeIsUp())
		{
			if (CanaRegs.CAN_NDAT_21 & 0x0001)//ECanaRegs.CANRMP.bit.RMP1
			{// FILTER 1 HIT is header
			    //receive mailbox 1's info
			    sCanMailReadBoot(&g_sBootCanRxData,1);
				if(((((UINT16)(g_sBootCanRxData.CanId.bit.DSTADDRH<<5))
					|g_sBootCanRxData.CanId.bit.DSTADDRL) == g_u16MdlAddr)
					&&(g_sBootCanRxData.CanId.bit.SRCADDR == 0x0E0))
				{
				    //由于协议号寄存器只有9位，最大只能到0x1FF，故这里超过后做特殊处理。
				    if(uiPacket > 0x1FF)
				    {
				        uiPacket -=0x200;
				    }

			   		if ((g_sBootCanRxData.CanId.bit.PROTNO == uiPacket)
			   			&& (!(g_sBootCanRxData.CanCtrl.bit.DLC & 0x0001))
			   		 	&& ((uiPacketAddr + g_sBootCanRxData.CanCtrl.bit.DLC) <= uiLength))
			   		{
						if (g_sBootCanRxData.CanCtrl.bit.DLC>=2)
						{
							*(uiDestination + uiPacketAddr/2) = g_sBootCanRxData.CanData.word.Word_H;
							uiPacketAddr += 2;
						}
						if (g_sBootCanRxData.CanCtrl.bit.DLC>=4)
						{
							*(uiDestination + uiPacketAddr/2) = g_sBootCanRxData.CanData.word.Word_L;
							uiPacketAddr += 2;
						}
						if (g_sBootCanRxData.CanCtrl.bit.DLC>=6)
						{
							*(uiDestination + uiPacketAddr/2) = g_sBootCanRxData.CanDatb.word.Word_H;
							uiPacketAddr += 2;
						}
						if (g_sBootCanRxData.CanCtrl.bit.DLC==8)
						{
							*(uiDestination + uiPacketAddr/2) = g_sBootCanRxData.CanDatb.word.Word_L;
							uiPacketAddr += 2;
						}
						if (uiPacketAddr>=uiLength)
						{
							//ECanaRegs.CANRMP.all = 0x0002;//Reset MBX1

							return(1);
						}
						uiPacket++;

			    	}
			    	else if((g_sBootCanRxData.CanId.bit.PROTNO == 0x1CB)||(g_sBootCanRxData.CanId.bit.PROTNO == 0x1CC))
			    	{
			    		ProcessInquiry();
			    	}
			    	else
			    	{
		    			uiStatus = MISSINGDATAPACKET;
					}
				}
				//ECanaRegs.CANRMP.all = 0x0002;//Reset MBX1

				StartTimer(TIME_1000mS);
			}
		}
	}
   	return(0);
}
//****************************************************************
//  Interface Function: PRG_unlock()
//
// Unlock the Code Security Module.
//
// Parameters:
//
// Return Value:
//
// Notes:
//-------------------------------------------------------------------

#pragma CODE_SECTION(PRG_unlock,"FlashBoot");

void PRG_unlock(void)
{
    volatile  Uint32 temp;
    volatile Uint32 *CSMPWL = (volatile Uint32 *)0x78028; //CSM Password location

    // Read the 128-bits of the CSM password locations (PWL)

    temp = *CSMPWL++;
    temp = *CSMPWL++;
    temp = *CSMPWL++;
    temp = *CSMPWL++;

    // Write the 128-bit password to the CSMKEY registers
    // If this password matches that stored in the
    // CSLPWL then the CSM will become unsecure. If it does not
    // match, then the zone will remain secure.

    DcsmBank0Z1Regs.Z1_CSMKEY0 = 0xFFFF55AA;//0xFFFFFFFF
    DcsmBank0Z1Regs.Z1_CSMKEY1 = 0x47FFAA55;//0x47FFFFFF
    DcsmBank0Z1Regs.Z1_CSMKEY2 = 0xFFFF5A5A;//0xFFFFFFFF
    DcsmBank0Z1Regs.Z1_CSMKEY3 = 0xFFFFA5A5;//0xFFFFFFFF
}

//****************************************************************
//  Interface Function: InputOVPreCheck()
//
// Input Voltage OV check and pretect.
//
// Parameters:
//
// Return Value:
//
// Notes:
//****************************************************************
#pragma CODE_SECTION(InputOVPreCheck,"FlashBoot");
void InputOVPreCheck(void)
{    	int16	i16AcVoltL, i16AcVoltN, i16AcVoltTemp, i16VpfcVolt;
	//判断AC是否过压，是否需要脱开过压脱离继电器
		i16AcVoltL = (INT16)ADCRESULT_AcVoltL;
		i16AcVoltN = (INT16)ADCRESULT_AcVoltN;
		i16AcVoltTemp = abs(i16AcVoltL - i16AcVoltN);
		i16VpfcVolt = (INT16)ADCRESULT_PfcVolt;

		//480V*8=3840, pfc protect voltage
		if((i16AcVoltTemp > VAC_AD_PEAK_EQU310VRMS) && (i16VpfcVolt > VPFC_AD_480V ))
		{
			mOverRelayOpen();
	 	}


}

//****************************************************************
//  Interface Function: ResetADCPWM(void)
//
// Reset ADC and PWM.
//
// Parameters:
//
// Return Value:
//
// Notes:
//****************************************************************
#pragma CODE_SECTION(ResetADCPWM,"FlashBoot");
void ResetADCPWM(void)
{
      EALLOW;
	  // zhanglimin 说这里复位会造成ADC采样问题,DSP自带的校准系数会被清掉
//	  AdcRegs.ADCCTL1.bit.RESET = 1;//因为BOOT时采了一下样,所以再初始化前先复位一下

	  asm("  RPT #3 || NOP"); //复位后延时
	  asm("  RPT #3 || NOP");

	  mTBClockOff(); //停掉PWM的TB CLOCK

	 EDIS;
}
#pragma CODE_SECTION(setMessageObject,"FlashBoot");
/******************************************************************************
 *Function name: void setMessageObject(uint32_t objID, CANMSGID_REG MsgId, msgObjType msgType)
 *Description :  setup the message Object 主要就是配置CMD,MASK,
 *input:         objID: mailbox number
 *input:         msgID: message ID
 *input:         msgType: receive or sent
 *global vars:

 *output:        void
 *CALLED BY:     void InitCan(void)
 *****************************************************************************/
void setMessageObject(uint32_t objID, union CANMSGID_REG MsgId, msgObjType msgType)
{
    uint32_t maskReg = 0U;
    //
    // Use Shadow variable for IF1CMD. IF1CMD should be written to in
    // single 32-bit write.
    //
    union CAN_IF1CMD_REG CAN_IF1CMD_SHADOW;

    //
    // Wait for busy bit to clear.
    //
    while(CanaRegs.CAN_IF1CMD.bit.Busy)
    {
    }

    //
    // Clear and Write out the registers to program the message object.
    //
    CAN_IF1CMD_SHADOW.all = 0;
    CanaRegs.CAN_IF1MSK.all = 0;
    CanaRegs.CAN_IF1ARB.all = 0;
    CanaRegs.CAN_IF1MCTL.all = 0;

    //
    // Set the Control, Mask, and Arb bit so that they get transferred to the
    // Message object.
    //
    CAN_IF1CMD_SHADOW.bit.Control = 1;
    CAN_IF1CMD_SHADOW.bit.Arb = 1;
    CAN_IF1CMD_SHADOW.bit.Mask = 1;
    CAN_IF1CMD_SHADOW.bit.DIR = 1; //初始化时，都是IFX向RAM写信息

    //
    // Configure the Mask Registers for 29 bit Identifier mask.

   // maskReg = msgID & CAN_IF1MSK_MSK_M;
    maskReg = 0x80000003;

    //
    // If the caller wants to filter on the extended ID bit then set it.
    //
    maskReg |= CAN_MSG_OBJ_USE_EXT_FILTER;

    //
    // The caller wants to filter on the message direction field.
    //
    maskReg |= CAN_MSG_OBJ_USE_DIR_FILTER;

    CanaRegs.CAN_IF1MSK.all = maskReg;

    //
    // Set direction to transmit
    //
    if(msgType == MSG_OBJ_TYPE_TRANSMIT)
    {
        CanaRegs.CAN_IF1ARB.bit.Dir = 1;
        CanaRegs.CAN_IF1MCTL.bit.UMask = 0; //use Mask
    }
    else
    {
        CanaRegs.CAN_IF1ARB.bit.Dir = 0;
        CanaRegs.CAN_IF1MCTL.bit.UMask = 1; //use Mask
    }
    //
    // Set Message ID (this example assumes 29 bit ID mask)
    //

    CanaRegs.CAN_IF1ARB.bit.ID = MsgId.all & 0x1FFFFFFF;
    CanaRegs.CAN_IF1ARB.bit.MsgVal = 1;
    CanaRegs.CAN_IF1ARB.bit.Xtd = 1;

    //
    // Set the data length since this is set for all transfers.  This is
    // also a single transfer and not a FIFO transfer so set EOB bit.
    //
    CanaRegs.CAN_IF1MCTL.bit.DLC = 8;
    CanaRegs.CAN_IF1MCTL.bit.EoB = 1;

    //
    // Transfer data to message object RAM
    //
    CAN_IF1CMD_SHADOW.bit.MSG_NUM = objID; //使用哪个邮箱
    CanaRegs.CAN_IF1CMD.all = CAN_IF1CMD_SHADOW.all;


}

/*************************************************************************
*   NAME:	    	VOID LoadCodeAndExecute(void)
*
*   PURPOSE:	   This function and its helpers actually pull a code image
*		      		from the controller.  When the code image tells this
*		      		function to execute the image, this function jumps to
*		      		the specified location instead of returning.
*
*   ARGUMENTS:	   none
*
*   RETURN VALUE: None.  Return from this function is an unusual event
*						signifying an	error in the loading process or an image
*		      		that doesn't specify that it be executed.
*
*   GLOBALS:	   MyCanAddress
*
*   REMARKS:	   The CAN controller is left in the software reset state
*		      		when this function is done with it but the registers are
*						such that the boot loader 2 can use them.
**************************************************************************/
#pragma CODE_SECTION(Load_Code_And_Execute,"FlashBoot");
void Load_Code_And_Execute(void)
{
	Uint32 ulImageAddr;
	APPLICATIONHEADER Header;
	union CANMSGID_REG MsgId;
	PRG_unlock();

	//ECanaRegs.CANME.all = 0x00000000;

	MsgId.all = 0x1CA80703;
	MsgId.bit.DSTADDRH = g_u16MdlAddr >> 5;
	MsgId.bit.DSTADDRL = g_u16MdlAddr & 0x001F;
	//MBX1 PCUs receive,ProtNo = 0x1CA
	setMessageObject(CAN_RX_MSG_OBJ, MsgId, MSG_OBJ_TYPE_RECEIVE);

	MsgId.all = 0x1CAF0003;//MBX4 PCU send
	MsgId.bit.SRCADDR = g_u16MdlAddr;
    setMessageObject(CAN_TX_MSG_OBJ_1_load, MsgId, MSG_OBJ_TYPE_TRANSMIT);

	MsgId.all = 0xDCBF0003;
	MsgId.bit.SRCADDR = g_u16MdlAddr;
	//MBX5 for reply PCU's info.
    setMessageObject(CAN_TX_MSG_OBJ_2_load, MsgId, MSG_OBJ_TYPE_TRANSMIT);

    //mRedLedOn();
    GpioDataRegs.GPBSET.bit.GPIO33 = 1;
    //mGreenLedOn();
    GpioDataRegs.GPASET.bit.GPIO16 = 1;
    GpioDataRegs.GPASET.bit.GPIO17 = 1;
    //mYellowLedOn();
    GpioDataRegs.GPASET.bit.GPIO11 = 1;
    GpioDataRegs.GPASET.bit.GPIO12 = 1;

	ulImageAddr = (Uint32)0;

	for (;;)
	{
// Request a header from the application image.  If this is the first one, use
// the first block request and use lots of retries.  Otherwise, use the next
// block request and less retries.

		if (!GetHeader(&Header,ulImageAddr,SIZE_OF_BOOTLOADERHEADER))
		{
			goto Reboot;//break;
		}
		ulImageAddr += (Uint32)SIZE_OF_BOOTLOADERHEADER;

// If this header specifies a zero-length data block, wait until transmitter 0
// is finished transmitting and execute from the specified address.

		if (!Header.uiLength)
		{
		   	StartTimer(TIME_1000mS);
		   	while (!TimeIsUp() && (CanaRegs.CAN_TXRQ_21 & 0x0010));//ECanaRegs.CANTRS.bit.TRS5
			//ECanaRegs.CANME.all = 0x00000000;	// turn off the mailboxes
		   	((void (*)(void))Header.uiLoadAddress)();

		}

//	Punt if the data block is the wrong size

		if ((Header.uiLength & 0x0001) ||(Header.uiLength > RAMBLOCKSIZE*2))// length=1: 8 bits data
		{
	    	SendBlockRequest(INVALIDAPPLICATION, ulImageAddr, Header.uiLength);

	    	goto WaitThenReboot;//return;
		}

// Otherwise, request data from the application image and stuff it into memory.

		if (!GetDataBlock(Header.uiLoadAddress,Header.uiLength,ulImageAddr))
		{
			goto Reboot;//break;
		}
		ulImageAddr += (Uint32)Header.uiLength;

        //mRedLedTwink();
        GpioDataRegs.GPBTOGGLE.bit.GPIO33 = 1;
        //mGreenLedTwink();
        GpioDataRegs.GPATOGGLE.bit.GPIO16 = 1;
        GpioDataRegs.GPATOGGLE.bit.GPIO17 = 1;
        //mYellowLedTwink();
        GpioDataRegs.GPATOGGLE.bit.GPIO11 = 1;
        GpioDataRegs.GPATOGGLE.bit.GPIO12 = 1;


 	}

// If we get here, we bombed out on excessive retries.  Shut down the CAN
// controller, return to the calling routine, and let him sort the mess out.
WaitThenReboot:
   	StartTimer(TIME_1000mS);


   	while (!TimeIsUp() && (CanaRegs.CAN_TXRQ_21 & 0x0010));//ECanaRegs.CANTRS.bit.TRS4


	//ECanaRegs.CANME.all = 0x00000000;	// turn off the mailboxes
Reboot:
    EALLOW;
   // WdRegs.WDCR.all = 0x0028;
    WdRegs.WDCR.all = 0;
    return;
}

/*************************************************************************
*
*
**************************************************************************/
#pragma CODE_SECTION(uiWaitPassiveTriggerCommand,"FlashBoot");
Uint16 uiWaitPassiveTriggerCommand(void)
{
    Uint16  uiTriggerStep = 0;
    union CAN_IF1CMD_REG CAN_IF1CMD_SHADOW;
    //union CANMSGID_REG MsgId;
    StartTimer(TIME_1000mS);

    while (!TimeIsUp())
    {

        //if(ECanaRegs.CANRMP.all & 0x0002)//MBX1 for receive
        if (CanaRegs.CAN_NDAT_21 & 0x0001)
        {
            sCanMailReadBoot(&g_sBootCanRxData,1);
            //if(ECanaMboxes.MBOX1.MSGID.bit.PROTNO == 0x1CA)
            if(g_sBootCanRxData.CanId.bit.PROTNO == 0x1CA)
            {
                if((g_sBootCanRxData.CanCtrl.bit.DLC == 0)&&(uiTriggerStep == 0))

                {

                    g_sBootCanTxData.CanId.all= 0x1CAF0003;

                    g_sBootCanTxData.CanData.word.Word_H = 0x00F0;
                    g_sBootCanTxData.CanData.word.Word_L = ulENP_Hi_SN.uintdata[0].id;
                    g_sBootCanTxData.CanDatb.all = ulENP_Lo_SN;

                    //MsgId.bit.SRCADDR = g_u16MdlAddr;
                    //setMessageObject(CAN_TX_MSG_OBJ_1, MsgId, MSG_OBJ_TYPE_TRANSMIT);
                    vCanMailSend(&g_sBootCanTxData,4);
                    uiTriggerStep = 1;
                    StartTimer(TIME_1000mS);


                }
                /*else if((g_sBootCanRxData.CanCtrl.bit.DLC== 8)
                        &&( g_sBootCanTxData.CanDatb.all== ulENP_Lo_SN )
                        &&( g_sBootCanTxData.CanData.word.Word_L == ulENP_Hi_SN.uintdata[0].id)
                        &&( g_sBootCanTxData.CanData.word.Word_H == 0x80F0)
                        &&(uiTriggerStep == 1))*/
                else if(uiTriggerStep == 1)

                {
                    return 0xF0;
                }
            }
            //ECanaRegs.CANRMP.all = 0x0002;
        }

        //InputOVPreCheck();
    }

//  ResetADCPWM();
//
    return  0;
    //return 0xF0;
}


//===========================================================================
// No more.
//===========================================================================
