#include "Drv_GPIO.h"
#include "f28004x_device.h"
#include <Prj_macro.h>
//
// InitGpio - Sets all pins to be muxed to GPIO in input mode with pull-ups
// enabled.Also resets CPU control to CPU1 and disables open drain
// and polarity inversion and sets the qualification to synchronous.
// Also unlocks all GPIOs. Only one CPU should call this function.
//
#pragma CODE_SECTION(InitGpio,"FlashBoot");
void
InitGpio()
{
    volatile Uint32 *gpioBaseAddr;


    Uint16 regOffset;

    //
    // Disable pin locks
    //
    EALLOW;
    GpioCtrlRegs.GPALOCK.all = 0x00000000;
    GpioCtrlRegs.GPBLOCK.all = 0x00000000;
    GpioCtrlRegs.GPHLOCK.all = 0x00000000;

    //
    // Fill all registers with zeros. Writing to each register separately
    // for three GPIO modules would make this function *very* long.
    // Fortunately, we'd be writing them all with zeros anyway,
    // so this saves a lot of space.
    //
    gpioBaseAddr = (Uint32 *)&GpioCtrlRegs;
    for (regOffset = 0; regOffset < sizeof(GpioCtrlRegs)/2; regOffset++)
    {
        //
        // Must avoid enabling pull-ups on all pins. GPyPUD is offset
        // 0x0C in each register group of 0x40 words. Since this is a
        // 32-bit pointer, the addresses must be divided by 2.
        //
        // Also, to avoid changing pin muxing of the emulator pins to regular
        // GPIOs, skip GPBMUX1 (0x46) and GPBGMUX1 (0x60).
        //
        if ((regOffset % (0x40/2) != (0x0C/2)) && (regOffset != (0x46/2)) &&
            (regOffset != (0x60/2)))
        {
            gpioBaseAddr[regOffset] = 0x00000000;
        }
    }

    gpioBaseAddr = (Uint32 *)&GpioDataRegs;
    for (regOffset = 0; regOffset < sizeof(GpioDataRegs)/2; regOffset++)
    {
        gpioBaseAddr[regOffset] = 0x00000000;
    }

    //GPIO5 for main Relay
    GPIO_SetupPinMux(5U, GPIO_MUX_CPU1, GPIO_USED);
    GPIO_SetupPinOptions(5U, GPIO_OUTPUT, GPIO_PUSHPULL);

    //GPIO9 for over voltage Relay
    GPIO_SetupPinMux(9U, GPIO_MUX_CPU1, GPIO_USED);
    GPIO_SetupPinOptions(9U, GPIO_OUTPUT, GPIO_PUSHPULL);

    //GPIO14,15 for LED green
    GPIO_SetupPinMux(16U, GPIO_MUX_CPU1, GPIO_USED);
    GPIO_SetupPinOptions(16U, GPIO_OUTPUT, GPIO_PUSHPULL);
    GPIO_SetupPinMux(17U, GPIO_MUX_CPU1, GPIO_USED);
    GPIO_SetupPinOptions(17U, GPIO_OUTPUT, GPIO_PUSHPULL);

    //GPIO34 for LED red
    GPIO_SetupPinMux(33U, GPIO_MUX_CPU1, GPIO_USED);
    GPIO_SetupPinOptions(33U, GPIO_OUTPUT, GPIO_PUSHPULL);

    //GPIO39,59 for LED yellow
    //qtest0723
    GPIO_SetupPinMux(11U, GPIO_MUX_CPU1, GPIO_USED);
    GPIO_SetupPinOptions(11U, GPIO_OUTPUT, GPIO_PUSHPULL);

    GPIO_SetupPinMux(12U, GPIO_MUX_CPU1, GPIO_USED);
    GPIO_SetupPinOptions(12U, GPIO_OUTPUT, GPIO_PUSHPULL);

    //GPIO25 for I2C /wp
    GPIO_SetupPinMux(25U, GPIO_MUX_CPU1, GPIO_USED);
    GPIO_SetupPinOptions(25U, GPIO_OUTPUT, GPIO_PUSHPULL);

    //GPIO30 for DC short input
    GPIO_SetupPinMux(15U, GPIO_MUX_CPU1, GPIO_USED);
    GPIO_SetupPinOptions(15U, GPIO_INPUT, GPIO_PULLUP);

    //GPIO30 for unplug input
    GPIO_SetupPinMux(14U, GPIO_MUX_CPU1, GPIO_USED);
    GPIO_SetupPinOptions(14U, GPIO_INPUT, GPIO_PULLUP);


    //GPIO29 for DCOVP input
    GPIO_SetupPinMux(29U, GPIO_MUX_CPU1, GPIO_USED);
    GPIO_SetupPinOptions(29U, GPIO_INPUT, GPIO_PULLUP);

    //for lanchpad demo LED TEST
//    GPIO_SetupPinMux(23U, GPIO_MUX_CPU1, GPIO_USED);
//    GPIO_SetupPinOptions(23U, GPIO_OUTPUT, GPIO_PUSHPULL);
//    GPIO_SetupPinMux(34U, GPIO_MUX_CPU1, GPIO_USED);
//    GPIO_SetupPinOptions(34U, GPIO_OUTPUT, GPIO_PUSHPULL);

    //GPIO34 for HMOS1
    GPIO_SetupPinMux(34U, GPIO_MUX_CPU1, GPIO_USED);
    GPIO_SetupPinOptions(34U, GPIO_OUTPUT, GPIO_PUSHPULL);
    //GPIO59 for HMOS2
    GPIO_SetupPinMux(59U, GPIO_MUX_CPU1, GPIO_USED);
    GPIO_SetupPinOptions(59U, GPIO_OUTPUT, GPIO_PUSHPULL);

    mOffHmosDriv();//lzy 20200312

    EDIS;
}
