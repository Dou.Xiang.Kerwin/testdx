

/*=============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : gbb_CanDriver.c
 *  PURPOSE  : rectifier communicate with controller and other rectifiers
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A00           zoulx             Created.   Pre-research 
 *    2019-5-9        A01           fhx               updata for 28004x
 *============================================================================*/

#include "f28004x_device.h"     // DSP280x Headerfile Include File
#include "App_cancom.h"
#include "Drv_CAN.h"
#include "App_constant.h"
#include "App_main.h"
#include "IQmathLib.h"

/*******************************************************************************
*variables dfinition
*******************************************************************************/
//variables for can data read and write

 static CANFRAME s_aCanRxBuf[CAN_RXBUF_SIZE];
 static CANFRAME s_aCanTxBuf[CAN_TXBUF_SIZE];
 static CANFRAME *s_pCanRxIn;
 static CANFRAME *s_pCanRxOut;
 static CANFRAME *s_pCanTxIn;
 static CANFRAME *s_pCanTxOut;
 static UINT16   s_u16CanRxLength;
//UINT16   g_u16csmCNT;
//static UINT16   s_u16CanTxLength;
//UINT16   s_u16CanTxLength;
 static UINT16   s_u16CanTxLength;

//unsigned char ucTXMsgData[4] = {0x1, 0x2, 0x3, 0x4}; // TX Data
//uint32_t messageSize = sizeof(ucTXMsgData);          // Message Size (DLC)

//variables for can data decode and encode
UINT16 g_u16PROTNO;
UINT16 g_u16PTP;
UINT16 g_u16DSTADDR;
UINT16 g_u16SRCADDR;
UINT16 g_u16CNT;
//UINT16 g_u16ErrFlag;
UINT16 g_u16ErrType;
UINT16 g_u16MsgType;
UINT16 g_u16ValueType;
ubitfloat g_ubitfCanData;
ubitfloat g_lq10CanData;
ubitfloat g_lq12CanData;

CANFRAME  g_stCanTxData;
CANFRAME  g_stCanRxData;

/*******************************************************************************
*functions declare
*******************************************************************************/
//receive mail box
//static void sCanResetRx0(void);

//transmit mail box reset




//application for app
static void sCanCopy(CANFRAME *pSource,CANFRAME *pDestination);

/*******************************************************************************
*Description:      initial can receive and transmit buffer
*******************************************************************************/
void sCanBufferInitial(void)
{
    s_pCanRxIn = s_aCanRxBuf;
    s_pCanRxOut = s_aCanRxBuf;
    s_pCanTxIn = s_aCanTxBuf;
    s_pCanTxOut = s_aCanTxBuf;
    s_u16CanTxLength = 0;
    s_u16CanRxLength = 0;
}

/*******************************************************************************
*Description:      These fucntions reset mailbox
*******************************************************************************/
//mail box 0: receive
//static void sCanResetRx0(void)
//{
//    struct ECAN_REGS ECanaShadow;
//
//  // receive data by box 0 success,  Reset RMP0
//    EALLOW;
//    ECanaShadow.CANRMP.all = ECanaRegs.CANRMP.all;
//  ECanaShadow.CANRMP.bit.RMP1 = 1;
//    ECanaRegs.CANRMP.all = ECanaShadow.CANRMP.all;
//    EDIS;
//}

//mail box 4: transmit
/*void sCanResetTx4(void)
{
//    struct ECAN_REGS ECanaShadow;
//
//  // send data in box 4 success,  Reset TA4
//    EALLOW;
//    ECanaShadow.CANTA.all = ECanaRegs.CANTA.all;
//  ECanaShadow.CANTA.bit.TA4 = 1;
//  ECanaRegs.CANTA.all = ECanaShadow.CANTA.all;
//    EDIS;
    ;
}*/





/*******************************************************************************
*Function name:     sCanCopy
*Parameters:        pSource:pointer to the source data
*                   pDestination:pointer to the destination data
*Description:       This fucntion copy the content of psource to pDestination
*******************************************************************************/
static void sCanCopy(CANFRAME *pSource,CANFRAME *pDestination)
{
    pDestination->CanId.all = pSource->CanId.all;
    pDestination->CanData.all = pSource->CanData.all;
    pDestination->CanDatb.all = pSource->CanDatb.all;
}

/*******************************************************************************
*Function name: sCanRxISR
*Parameters:        none
*Description:       This fucntion should be executed in the can receiving
*                   interrupt,it save the received data to the queue
*******************************************************************************/
void sCanRxISR1(void)
{
    //sCanResetRx0();

    if(s_u16CanRxLength == CAN_RXBUF_SIZE)
    {
        return;
    }
    else
    {
        sCanHdRead1(s_pCanRxIn);
        if(s_pCanRxIn == &s_aCanRxBuf[CAN_RXBUF_SIZE - 1])
        {
            s_pCanRxIn = s_aCanRxBuf;
        }
        else
        {
            s_pCanRxIn++;
        }
        s_u16CanRxLength++;
    }
}

/*******************************************************************************
*Function name:     sCanRead
*Parameters:        pdata:the address where to save the data
*Returns:           CAN_RXBUF_EMPTY:there is no data in the receive buffer
*                   CAN_RXBUF_RDY:data is read successdully from buffer
*Description:       This fucntion get the data from receiving buffer and save
*                   to user's buffer.it should be executed in app
*******************************************************************************/
UINT16  sCanRead(CANFRAME *pdata)
{
    if(s_u16CanRxLength == 0)
    {
        return(CAN_RXBUF_EMPTY);
    }

    sCanCopy(s_pCanRxOut,pdata);

    s_u16CanRxLength--;

    if(s_pCanRxOut == &s_aCanRxBuf[CAN_RXBUF_SIZE - 1])
    {
        s_pCanRxOut = s_aCanRxBuf;
    }
    else
    {
        s_pCanRxOut++;
    }

    return(CAN_RXBUF_RDY);
}

/********************************************************************************
*Function name:     sCanTxISR4
*Parameters:        none
*Description:       This fucntion should be executed in the can transmit
*                   interrupt,it changes the status of transmition
*********************************************************************************/
void sCanTxISR5(void)
{
    //if(IsrVars._u16CanTxLength >1)
    sCanResetTx5();

    //s_pCanTxOut=0
    if(s_u16CanTxLength!=0)
    {
        if(s_pCanTxOut == &s_aCanTxBuf[CAN_TXBUF_SIZE - 1])
        {
            s_pCanTxOut = s_aCanTxBuf;
        }
        else
        {
            s_pCanTxOut++;
        }

        //DELAY_US(1400L);

        if(s_u16CanTxLength >1)
        {
            sCanHdSend5(s_pCanTxOut);// sCanHdSend5(s_pCanTxOut);
            //s_u16CanTxLength--;
        }

        s_u16CanTxLength--;
    }
}


/********************************************************************************
*Function name:     sCanWrite5
*Parameters:        pdata:the start address to be sent
*Returns:           cCanTxBusy:the can bus is busy
*                   cCanTxRdy:can bus is ready to transmit this frame
*Description:       This fucntion send the data to the can registers
*********************************************************************************/
void    sCanWrite5(CANFRAME *pdata)
{
    if(s_u16CanTxLength == CAN_TXBUF_SIZE)
    {
        sCanHdSend5(s_pCanTxOut);
        return;
    }

    sCanCopy(pdata,s_pCanTxIn);

    s_u16CanTxLength++;

    if(s_pCanTxIn == &s_aCanTxBuf[CAN_TXBUF_SIZE - 1])
    {
        s_pCanTxIn = s_aCanTxBuf;
    }
    else
    {
        s_pCanTxIn++;
    }

    if(s_u16CanTxLength == 1)
    {
        sCanHdSend5(s_pCanTxOut);
    }

}

/*******************************************************************************
*Function: sDecodeCanRxFrame
*Discription:The function describes how to decode a frame of can data
*******************************************************************************/
void sDecodeCanRxFrame(CANFRAME stRxFrameTemp)
{
    //Decode message ID
    g_u16PROTNO = stRxFrameTemp.CanId.bit.PROTNO;
    g_u16PTP = stRxFrameTemp.CanId.bit.PTP;

    g_u16DSTADDR = ((UINT16)(stRxFrameTemp.CanId.bit.DSTADDRH << 5))
                   | stRxFrameTemp.CanId.bit.DSTADDRL;

    g_u16SRCADDR = stRxFrameTemp.CanId.bit.SRCADDR;
    g_u16CNT = stRxFrameTemp.CanId.bit.CNT;

    //Decode message data0
    //g_u16ErrType = stRxFrameTemp.CanData1 & 0x00ff;
    //g_u16MsgType = (stRxFrameTemp.CanData1 >> 8) & 0x007f;

    g_u16ErrType = stRxFrameTemp.CanData.byte.BYTE1;
    g_u16MsgType = stRxFrameTemp.CanData.byte.BYTE0 & 0x007f;
    //Decode message data1
    //g_u16ValueType = stRxFrameTemp.CanData0;
    g_u16ValueType = stRxFrameTemp.CanData.word.Word_L;

    //Decode message data2~3
    //g_ubitfCanData.uintdata[0].id = stRxFrameTemp.CanData2;
    //g_ubitfCanData.uintdata[1].id = stRxFrameTemp.CanData3;
    g_ubitfCanData.uintdata[0].id = stRxFrameTemp.CanDatb.word.Word_L;
    g_ubitfCanData.uintdata[1].id = stRxFrameTemp.CanDatb.word.Word_H;

    g_lq10CanData.lData = _IQ10(g_ubitfCanData.fd);
    g_lq12CanData.lData = _IQ12(g_ubitfCanData.fd);
}

/*******************************************************************************
*Function:  sEncodeCanTxID(),sEncodeCanTxData()
*Description: These functions describe how to encode a frame of can data
*******************************************************************************/
void sEncodeCanTxID(UINT16 u16ProtNoTemp,UINT16 u16PTPTemp,
                    UINT16 u16DstAddrTemp,UINT16 u16CntTemp)
{
    //encode a frame of can data ID
    g_stCanTxData.CanId.bit.PROTNO = u16ProtNoTemp;
    g_stCanTxData.CanId.bit.PTP = u16PTPTemp;
    g_stCanTxData.CanId.bit.DSTADDRH = u16DstAddrTemp >> 5;
    g_stCanTxData.CanId.bit.DSTADDRL = u16DstAddrTemp & 0x001f;
    g_stCanTxData.CanId.bit.SRCADDR = g_u16MdlAddr;
    g_stCanTxData.CanId.bit.CNT = u16CntTemp;

    g_stCanTxData.CanId.all |= 0xc0000003;
}

void sEncodeCanTxData(UINT16 u16MsgTypeTemp,UINT16 u16ErrTypeTemp,
                      UINT16 u16ValueTypeTemp,ubitfloat ubitfDataTemp)
{
    /*encode a frame of can data0 */
    //in the process of address auto-identifiication
    if(g_u16AddIdentifyFlag)
    {
        u16ErrTypeTemp = ADDR_IDENTIFY;
    }
    //g_stCanTxData.CanData1 = (u16MsgTypeTemp << 8) + u16ErrTypeTemp;
    g_stCanTxData.CanData.word.Word_H = (u16MsgTypeTemp << 8) + u16ErrTypeTemp;

    //if( (u16ErrTypeTemp != NORMAL) && (u16ErrTypeTemp != ADDR_IDENTIFY) )
    if( (u16ErrTypeTemp == ADDR_FAIL) || (u16ErrTypeTemp == COMM_FAIL))     // ls 20120409
    {
        //g_stCanTxData.CanData1 = g_stCanTxData.CanData1 | 0x8000;
        g_stCanTxData.CanData.word.Word_H = g_stCanTxData.CanData.word.Word_H | 0x8000;
    }

    //encode a frame of can data1
    //g_stCanTxData.CanData0 = u16ValueTypeTemp;
    g_stCanTxData.CanData.word.Word_L = u16ValueTypeTemp;

    //encode a frame of can data2~3
    //g_stCanTxData.CanData2 = ubitfDataTemp.uintdata[0].id;
   // g_stCanTxData.CanData3 = ubitfDataTemp.uintdata[1].id;
    g_stCanTxData.CanDatb.word.Word_L = ubitfDataTemp.uintdata[0].id;
    g_stCanTxData.CanDatb.word.Word_H = ubitfDataTemp.uintdata[1].id;
}

void sEncodeCanTxIQ10Data(UINT16 u16MsgTypeTemp,UINT16 u16ErrTypeTemp,
                      UINT16 u16ValueTypeTemp,ubitfloat ubitfDataTemp)
{
    /*encode a frame of can data0 */
    //in the process of address auto-identifiication
    if(g_u16AddIdentifyFlag)
    {
        u16ErrTypeTemp = ADDR_IDENTIFY;
    }
    g_stCanTxData.CanData.word.Word_H = (u16MsgTypeTemp << 8) + u16ErrTypeTemp;

    //if( (u16ErrTypeTemp != NORMAL) && (u16ErrTypeTemp != ADDR_IDENTIFY) )
    if( (u16ErrTypeTemp == ADDR_FAIL) || (u16ErrTypeTemp == COMM_FAIL))     // ls 20120409
    {
        g_stCanTxData.CanData.word.Word_H = g_stCanTxData.CanData.word.Word_H | 0x8000;
    }

    //encode a frame of can data1
    //g_stCanTxData.CanData0 = u16ValueTypeTemp;
    g_stCanTxData.CanData.word.Word_L = u16ValueTypeTemp;

    //encode a frame of can data2~3
    ubitfDataTemp.fd =  _IQ10toF(ubitfDataTemp.lData);
    //g_stCanTxData.CanData2 = ubitfDataTemp.uintdata[0].id;
   // g_stCanTxData.CanData3 = ubitfDataTemp.uintdata[1].id;

    g_stCanTxData.CanDatb.word.Word_L = ubitfDataTemp.uintdata[0].id;
    g_stCanTxData.CanDatb.word.Word_H = ubitfDataTemp.uintdata[1].id;
}


