//###########################################################################
//
// FILE:    f28004x_cmpss.c
//
// TITLE:   f28004x cmpss and DAC Initialization & Support Functions.
//
//###########################################################################
     // VERTIV DSP created
//###########################################################################

//
// Included Files
//
 #include "f28004x_device.h"      // Headerfile Include File

//
// Globals
//
static void InitCMPSS1(void);//CMPSS1 for _DCOCP
static void InitCMPSS2(void);//CMPSS2 for _PFCOVP
static void InitCMPSS3(void);// CMPSS3 for _PFCCURR2 OCP
static void InitCMPSS4(void);//Vds
static void InitCMPSS5(void);//CMPSS5 for _PFCCURR1 OCP
static void InitCMPSS6(void);//Vds
//
// InitCMPSS - Initialize CMPSS1 and configure values
//
void InitCMPSS(void)
{
    InitCMPSS1(); // for DCOCP ~ TZ ~ PWM4~PWM7 OFF control - one short
    InitCMPSS2(); // for PFCOVP ~ TZ ~ PWM1 PWM3 OFF control - one short
    InitCMPSS4(); // for PFC PHASE 1 Vds compare to generate SYNC for PWM1A
    InitCMPSS6(); // for PFC PHASE 2 Vds compare to generate SYNC for PWM3A

    InitCMPSS3(); // for PFCCURR2 OCP ~ TZ ~ PWM3A OFF control - CBC
    InitCMPSS5(); // for PFCCURR1 OCP ~ TZ ~ PWM1A OFF control - CBC
}

static void InitCMPSS1(void)
{
    EALLOW;

    //
    //Enable CMPSS1 for _DCOCP
    //
    Cmpss1Regs.COMPCTL.bit.COMPDACE = 0; //0:disable 1:enable

    //input GPIO define
    AnalogSubsysRegs.CMPHPMXSEL.bit.CMP1HPMXSEL = 2; // select PGA1_IN
    AnalogSubsysRegs.CMPLPMXSEL.bit.CMP1LPMXSEL = 2;

    //
    //NEG signal comes from DAC
    //
    Cmpss1Regs.COMPCTL.bit.COMPHSOURCE = 0;//0:DAC 1:EXT PIN

    //
    //Use VDDA as the reference for DAC = 3.3V
    //
    Cmpss1Regs.COMPDACCTL.bit.SELREF = 0;//0:VDDA 1:VDAC

    //
    // CMPSS output invert
    //
    Cmpss1Regs.COMPCTL.bit.COMPLINV = 0;
    Cmpss1Regs.COMPCTL.bit.COMPHINV = 0;
    //
    //Set DAC to midpoint for arbitrary reference
    //
    //Cmpss1Regs.DACHVALS.bit.DACVAL = 550; //=4096*560/(560+6800) 对应25A
    //Cmpss1Regs.DACLVALS.bit.DACVAL = 550; //对应30A
    Cmpss1Regs.DACHVALS.bit.DACVAL = 730;
    Cmpss1Regs.DACLVALS.bit.DACVAL = 730;
    //
    // Configure Digital Filter
    //
    Cmpss1Regs.CTRIPHFILCLKCTL.bit.CLKPRESCALE = 0x0;
    Cmpss1Regs.CTRIPLFILCLKCTL.bit.CLKPRESCALE = 0x0;

    //
    //Maximum SAMPWIN value provides largest number of samples
    //
    Cmpss1Regs.CTRIPHFILCTL.bit.SAMPWIN = 0x5;
    Cmpss1Regs.CTRIPLFILCTL.bit.SAMPWIN = 0x5;

    //
    //Maximum THRESH value requires static value for entire window
    //THRESH should be GREATER than half of SAMPWIN
    //
    Cmpss1Regs.CTRIPHFILCTL.bit.THRESH = 0x5;
    Cmpss1Regs.CTRIPLFILCTL.bit.THRESH = 0x5;

    //
    //Reset filter logic & start filtering
    //
    Cmpss1Regs.CTRIPHFILCTL.bit.FILINIT = 1;
    Cmpss1Regs.CTRIPLFILCTL.bit.FILINIT = 1;

    // Configure CTRIPOUT path
    // Digital filter output feeds CTRIPH and CTRIPOUTH
    //0 Asynchronous comparator output drives CTRIPH
    //1 Synchronous comparator output drives CTRIPH
    //2 Output of digital filter drives CTRIPH
    //3 Latched output of digital filter drives CTRIPH

    Cmpss1Regs.COMPCTL.bit.CTRIPHSEL = 2; //for PWM X-BAR
    Cmpss1Regs.COMPCTL.bit.CTRIPLSEL = 2; //for PWM X-BAR

    //just for test
//    Cmpss1Regs.COMPCTL.bit.CTRIPOUTHSEL = 2; //for OUTPUT XBAR
//    Cmpss1Regs.COMPCTL.bit.CTRIPOUTLSEL = 2; //for OUTPUT XBAR

    //choose CMPSS3.CTRIPL for TRIP4
    //EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX5 = 0;
    //EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX5 = 1;
    EPwmXbarRegs.TRIP7MUX0TO15CFG.bit.MUX0 = 0;//tyang
    EPwmXbarRegs.TRIP7MUXENABLE.bit.MUX0 = 1;  //tyang


//    OutputXbarRegs.OUTPUT7MUX0TO15CFG.bit.MUX0 = 1;//set CMPSS1H or CMPSS1L  for output 8
//    OutputXbarRegs.OUTPUT7MUXENABLE.bit.MUX0 = 1; //1:enable 0:disable

    //enable DAC
    Cmpss1Regs.COMPCTL.bit.COMPDACE = 1; //0:disable 1:enable
    EDIS;

    //just for test
    //GPIO11 is set to OUTPUT XBAR7
    //qtest0723
    //GPIO_SetupPinMux(11U, GPIO_MUX_CPU1, OUTPUTXBAR7);
    //GPIO_SetupPinOptions(11U, GPIO_OUTPUT, GPIO_PUSHPULL);

}
//20200327 for PFCOVPTZ
static void InitCMPSS2(void)
{
    EALLOW;

    //
    //Enable CMPSS2 for _PFCOVP
    //
    Cmpss2Regs.COMPCTL.bit.COMPDACE = 0; //0:disable 1:enable

    //input GPIO define
    AnalogSubsysRegs.CMPHPMXSEL.bit.CMP2HPMXSEL = 0; // select PGA1_IN
    AnalogSubsysRegs.CMPLPMXSEL.bit.CMP2LPMXSEL = 0;

    //
    //NEG signal comes from DAC
    //
    Cmpss2Regs.COMPCTL.bit.COMPHSOURCE = 0;//0:DAC 1:EXT PIN

    //
    //Use VDDA as the reference for DAC = 3.3V
    //
    Cmpss2Regs.COMPDACCTL.bit.SELREF = 0;//0:VDDA 1:VDAC

    //
    // CMPSS output invert
    //
    Cmpss2Regs.COMPCTL.bit.COMPLINV = 0;
    Cmpss2Regs.COMPCTL.bit.COMPHINV = 0;

    //Set DAC to midpoint for arbitrary reference
    //440=3521；490=3922；405=3241;3361=420；400=3201;480=3706
    Cmpss2Regs.DACHVALS.bit.DACVAL = 3784; //3784---490V
    Cmpss2Regs.DACLVALS.bit.DACVAL = 3784; //3784---490V
    // Configure Digital Filter
    Cmpss2Regs.CTRIPHFILCLKCTL.bit.CLKPRESCALE = 0x0;
    Cmpss2Regs.CTRIPLFILCLKCTL.bit.CLKPRESCALE = 0x0;
    //Maximum SAMPWIN value provides largest number of samples
    Cmpss2Regs.CTRIPHFILCTL.bit.SAMPWIN = 0x0;//0x5;//20200411
    Cmpss2Regs.CTRIPLFILCTL.bit.SAMPWIN = 0x0;//0x5;
    //Maximum THRESH value requires static value for entire window
    //THRESH should be GREATER than half of SAMPWIN
    Cmpss2Regs.CTRIPHFILCTL.bit.THRESH = 0x0;//0x5;//20200411
    Cmpss2Regs.CTRIPLFILCTL.bit.THRESH = 0x0;//0x5;
    //Reset filter logic & start filtering
    Cmpss2Regs.CTRIPHFILCTL.bit.FILINIT = 0;//1;//20200411
    Cmpss2Regs.CTRIPLFILCTL.bit.FILINIT = 0;//1;

    // Configure CTRIPOUT path
    // Digital filter output feeds CTRIPH and CTRIPOUTH
    //0 Asynchronous comparator output drives CTRIPH
    //1 Synchronous comparator output drives CTRIPH
    //2 Output of digital filter drives CTRIPH
    //3 Latched output of digital filter drives CTRIPH

    Cmpss2Regs.COMPCTL.bit.CTRIPHSEL = 0; //for PWM X-BAR
    Cmpss2Regs.COMPCTL.bit.CTRIPLSEL = 0; //for PWM X-BAR

    //just for test
//    Cmpss2Regs.COMPCTL.bit.CTRIPOUTHSEL = 2; //for OUTPUT XBAR
//    Cmpss2Regs.COMPCTL.bit.CTRIPOUTLSEL = 2; //for OUTPUT XBAR

    //choose CMPSS2.CTRIPL for TRIP10
    //EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX5 = 0;
    //EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX5 = 1;
    EPwmXbarRegs.TRIP10MUX0TO15CFG.bit.MUX2 = 0;
    EPwmXbarRegs.TRIP10MUXENABLE.bit.MUX2 = 1;


//    OutputXbarRegs.OUTPUT7MUX0TO15CFG.bit.MUX0 = 1;//set CMPSS1H or CMPSS1L  for output 8
//    OutputXbarRegs.OUTPUT7MUXENABLE.bit.MUX0 = 1; //1:enable 0:disable

    //enable DAC
    Cmpss2Regs.COMPCTL.bit.COMPDACE = 1; //0:disable 1:enable
    EDIS;

    //just for test
    //GPIO11 is set to OUTPUT XBAR7
//    GPIO_SetupPinMux(11U, GPIO_MUX_CPU1, OUTPUTXBAR7);
//    GPIO_SetupPinOptions(11U, GPIO_OUTPUT, GPIO_PUSHPULL);

}

static void InitCMPSS6(void)
{
    EALLOW;

    Cmpss6Regs.COMPCTL.bit.COMPDACE = 0; //0:disable 1:enable

    //input GPIO define
    AnalogSubsysRegs.CMPHPMXSEL.bit.CMP6HPMXSEL = 2;//select PGA2_IN
    AnalogSubsysRegs.CMPLPMXSEL.bit.CMP6LPMXSEL = 2;//select PGA2_IN

    Cmpss6Regs.COMPDACCTL.bit.SWLOADSEL = 0;//0: on sysclk 1:on EPWMSYNCPER

    //
    //NEG signal comes from DAC
    //
    Cmpss6Regs.COMPCTL.bit.COMPHSOURCE = 0;//0:DAC 1:EXT PIN
    Cmpss6Regs.COMPCTL.bit.COMPLSOURCE = 0;//0:DAC 1:EXT PIN

    //
    // CMPSS output invert
    //
    Cmpss6Regs.COMPCTL.bit.COMPLINV = 0;
    Cmpss6Regs.COMPCTL.bit.COMPHINV = 0;

    //
    //Use VDDA as the reference for DAC = 3.3V
    //
    Cmpss6Regs.COMPDACCTL.bit.SELREF = 0;//0:VDDA 1:VDAC

    //
    //Set DAC to midpoint for arbitrary reference
    //
    //Vpfc * 0.006224 = Vcmpss , so 200V*0.006224/3.3*4096 = 1544
    Cmpss6Regs.DACHVALS.bit.DACVAL = 2702; //=3.3v/2
    Cmpss6Regs.DACLVALS.bit.DACVAL = 2702; //=3.3v/2

    //比较器滞回使能
    Cmpss6Regs.COMPHYSCTL.bit.COMPHYS = 0;

    //
    // Configure Digital Filter
    //
    Cmpss6Regs.CTRIPHFILCLKCTL.bit.CLKPRESCALE = 0x0;
    Cmpss6Regs.CTRIPLFILCLKCTL.bit.CLKPRESCALE = 0x0;

    //
    //Maximum SAMPWIN value provides largest number of samples
    //
    Cmpss6Regs.CTRIPHFILCTL.bit.SAMPWIN = 0;
    Cmpss6Regs.CTRIPLFILCTL.bit.SAMPWIN = 0;

    //
    //Maximum THRESH value requires static value for entire window
    //THRESH should be GREATER than half of SAMPWIN
    //
    Cmpss6Regs.CTRIPHFILCTL.bit.THRESH = 0;
    Cmpss6Regs.CTRIPLFILCTL.bit.THRESH = 0;

    //
    //Reset filter logic & start filtering
    //
    Cmpss6Regs.CTRIPHFILCTL.bit.FILINIT = 0;
    Cmpss6Regs.CTRIPLFILCTL.bit.FILINIT = 0;

    // Configure CTRIPOUT path
    // Digital filter output feeds CTRIPH and CTRIPOUTH
    //0 Asynchronous comparator output drives CTRIPH
    //1 Synchronous comparator output drives CTRIPH
    //2 Output of digital filter drives CTRIPH
    //3 Latched output of digital filter drives CTRIPH

    Cmpss6Regs.COMPCTL.bit.CTRIPHSEL = 0; //for PWM X-BAR
   // Cmpss2Regs.COMPCTL.bit.CTRIPLSEL = 2; //for PWM X-BAR

    //just for test
//    Cmpss2Regs.COMPCTL.bit.CTRIPOUTHSEL = 2;
   // Cmpss2Regs.COMPCTL.bit.CTRIPOUTLSEL = 2;

    // configure CTRIPL to PWM XBAR
    //choose CMPSS5.CTRIPL for TRIP5
    EPwmXbarRegs.TRIP5MUX0TO15CFG.bit.MUX10 = 0; //SELECT CMPSS6.CTRIPOUTH
    EPwmXbarRegs.TRIP5MUXENABLE.bit.MUX10 = 1;

    //qtest0805
    //把CMPSS信号送到output 8 X-bar
    OutputXbarRegs.OUTPUT8MUX0TO15CFG.bit.MUX10 = 0;//SELECT CMPSS6.CTRIPOUTH for output 8
    OutputXbarRegs.OUTPUT8MUXENABLE.bit.MUX10 = 1; //1:enable 0:disable

//    OutputXbarRegs.OUTPUT8MUX0TO15CFG.bit.MUX2 = 0;//SELECT CMPSS2.CTRIPOUTH for output 8
//    OutputXbarRegs.OUTPUT8MUXENABLE.bit.MUX2 = 1; //1:enable 0:disable

    Cmpss6Regs.COMPCTL.bit.COMPDACE = 1; //0:disable 1:enable

    EDIS;

    //just for test
//    //GPIO31 is set to OUTPUT XBAR8
//    GPIO_SetupPinMux(31U, GPIO_MUX_CPU1, OUTPUTXBAR8);
//    GPIO_SetupPinOptions(31U, GPIO_OUTPUT, GPIO_PUSHPULL);

}


static void InitCMPSS4(void)
{
    EALLOW;

    Cmpss4Regs.COMPCTL.bit.COMPDACE = 0; //0:disable 1:enable

    //input GPIO define
    AnalogSubsysRegs.CMPHPMXSEL.bit.CMP4HPMXSEL = 2;//select PGA4_IN
    AnalogSubsysRegs.CMPLPMXSEL.bit.CMP4LPMXSEL = 2;//select PGA4_IN

    Cmpss4Regs.COMPDACCTL.bit.SWLOADSEL = 0;//0: on sysclk 1:on EPWMSYNCPER

    //
    //NEG signal comes from DAC
    //
    Cmpss4Regs.COMPCTL.bit.COMPHSOURCE = 0;//0:DAC 1:EXT PIN
    Cmpss4Regs.COMPCTL.bit.COMPLSOURCE = 0;//0:DAC 1:EXT PIN

    //
    // CMPSS output invert
    //
    Cmpss4Regs.COMPCTL.bit.COMPLINV = 0;
    Cmpss4Regs.COMPCTL.bit.COMPHINV = 0;

    //
    //Use VDDA as the reference for DAC = 3.3V
    //
    Cmpss4Regs.COMPDACCTL.bit.SELREF = 0;//0:VDDA 1:VDAC

    //
    //Set DAC to midpoint for arbitrary reference
    //
    //Vpfc * 0.006224 = Vcmpss , so 200V*0.006224/3.3*4096 = 1544
    Cmpss4Regs.DACHVALS.bit.DACVAL = 2702; //=3.3v/2
    Cmpss4Regs.DACLVALS.bit.DACVAL = 2702; //=3.3v/2

    //比较器滞回使能,1倍滞回
    Cmpss4Regs.COMPHYSCTL.bit.COMPHYS = 0;
    //
    // Configure Digital Filter
    //
    Cmpss4Regs.CTRIPHFILCLKCTL.bit.CLKPRESCALE = 0x0;
    Cmpss4Regs.CTRIPLFILCLKCTL.bit.CLKPRESCALE = 0x0;

    //
    //Maximum SAMPWIN value provides largest number of samples
    //
    Cmpss4Regs.CTRIPHFILCTL.bit.SAMPWIN = 0;
    Cmpss4Regs.CTRIPLFILCTL.bit.SAMPWIN = 0;

    //
    //Maximum THRESH value requires static value for entire window
    //THRESH should be GREATER than half of SAMPWIN
    //
    Cmpss4Regs.CTRIPHFILCTL.bit.THRESH = 0;
    Cmpss4Regs.CTRIPLFILCTL.bit.THRESH = 0;

    //
    //Reset filter logic & start filtering
    //
    Cmpss4Regs.CTRIPHFILCTL.bit.FILINIT = 0;
    Cmpss4Regs.CTRIPLFILCTL.bit.FILINIT = 0;

    // Configure CTRIPOUT path
    // Digital filter output feeds CTRIPH and CTRIPOUTH
    //0 Asynchronous comparator output drives CTRIPH
    //1 Synchronous comparator output drives CTRIPH
    //2 Output of digital filter drives CTRIPH
    //3 Latched output of digital filter drives CTRIPH

    Cmpss4Regs.COMPCTL.bit.CTRIPHSEL = 0; //for PWM X-BAR
   // Cmpss4Regs.COMPCTL.bit.CTRIPLSEL = 2; //for PWM X-BAR

    //just for test
//    Cmpss4Regs.COMPCTL.bit.CTRIPOUTHSEL = 2;
   // Cmpss4Regs.COMPCTL.bit.CTRIPOUTLSEL = 2;

    // configure CTRIPL to PWM XBAR
    //choose CMPSS5.CTRIPL for TRIP4
    EPwmXbarRegs.TRIP4MUX0TO15CFG.bit.MUX6 = 0; //SELECT CMPSS4.CTRIPH
    EPwmXbarRegs.TRIP4MUXENABLE.bit.MUX6 = 1;

//    OutputXbarRegs.OUTPUT5MUX0TO15CFG.bit.MUX6 = 0;//SELECT CMPSS4.CTRIPOUTH for output 8
//    OutputXbarRegs.OUTPUT5MUXENABLE.bit.MUX6 = 1; //1:enable 0:disable

    Cmpss4Regs.COMPCTL.bit.COMPDACE = 1; //0:disable 1:enable

    EDIS;

    //just for test
    //GPIO7 is set to OUTPUT XBAR5
//    GPIO_SetupPinMux(7U, GPIO_MUX_CPU1, OUTPUTXBAR5);
//    GPIO_SetupPinOptions(7U, GPIO_OUTPUT, GPIO_PUSHPULL);

}

static void InitCMPSS3(void)
{
    EALLOW;

    //
    //Enable CMPSS3 for _PFCCURR2 OCP
    //
    Cmpss3Regs.COMPCTL.bit.COMPDACE = 0; //0:disable 1:enable

    //input GPIO define
    AnalogSubsysRegs.CMPHPMXSEL.bit.CMP3HPMXSEL = 2; // select PGA3_IN
    AnalogSubsysRegs.CMPLPMXSEL.bit.CMP3LPMXSEL = 2; // select PGA3_IN

    //
    //NEG signal comes from DAC
    //
    Cmpss3Regs.COMPCTL.bit.COMPHSOURCE = 0;//0:DAC 1:EXT PIN

    //
    //Use VDDA as the reference for DAC = 3.3V
    //
    Cmpss3Regs.COMPDACCTL.bit.SELREF = 0;//0:VDDA 1:VDAC

    //
    // CMPSS output invert
    //
    Cmpss3Regs.COMPCTL.bit.COMPLINV = 0;
    Cmpss3Regs.COMPCTL.bit.COMPHINV = 0;
    //
    //Set DAC to midpoint for arbitrary reference
    //
    Cmpss3Regs.DACHVALS.bit.DACVAL = 248; //=50A*0.004*4096/3.3=248
    Cmpss3Regs.DACLVALS.bit.DACVAL = 248; //=60A*0.004*4096/3.3=298

    //
    // Configure Digital Filter
    //
    Cmpss3Regs.CTRIPHFILCLKCTL.bit.CLKPRESCALE = 0x0;
    Cmpss3Regs.CTRIPLFILCLKCTL.bit.CLKPRESCALE = 0x0;

    //
    //Maximum SAMPWIN value provides largest number of samples
    //
    Cmpss3Regs.CTRIPHFILCTL.bit.SAMPWIN = 0x4;
    Cmpss3Regs.CTRIPLFILCTL.bit.SAMPWIN = 0x4;
    //Cmpss3Regs.CTRIPHFILCTL.bit.SAMPWIN = 0x1;
    //Cmpss3Regs.CTRIPLFILCTL.bit.SAMPWIN = 0x1;

    //
    //Maximum THRESH value requires static value for entire window
    //THRESH should be GREATER than half of SAMPWIN
    //
    Cmpss3Regs.CTRIPHFILCTL.bit.THRESH = 0x4;
    Cmpss3Regs.CTRIPLFILCTL.bit.THRESH = 0x4;

    //
    //Reset filter logic & start filtering
    //
    Cmpss3Regs.CTRIPHFILCTL.bit.FILINIT = 1;
    Cmpss3Regs.CTRIPLFILCTL.bit.FILINIT = 1;

    // Configure CTRIPOUT path
    // Digital filter output feeds CTRIPH and CTRIPOUTH
    //0 Asynchronous comparator output drives CTRIPH
    //1 Synchronous comparator output drives CTRIPH
    //2 Output of digital filter drives CTRIPH
    //3 Latched output of digital filter drives CTRIPH

    Cmpss3Regs.COMPCTL.bit.CTRIPHSEL = 2; //for PWM X-BAR
    Cmpss3Regs.COMPCTL.bit.CTRIPLSEL = 2; //for PWM X-BAR

    //just for test
//    Cmpss3Regs.COMPCTL.bit.CTRIPOUTHSEL = 2; //for OUTPUT XBAR
//    Cmpss3Regs.COMPCTL.bit.CTRIPOUTLSEL = 2; //for OUTPUT XBAR

    //choose CMPSS3.CTRIPL for TRIP8
    //EPwmXbarRegs.TRIP8MUX0TO15CFG.bit.MUX4 = 1;//CMPSS3.CTRIPH_OR_CTRIPL
    EPwmXbarRegs.TRIP8MUX0TO15CFG.bit.MUX4 = 0;//CMPSS3.CTRIPH_OR_CTRIPL

    EPwmXbarRegs.TRIP8MUXENABLE.bit.MUX4 = 1;

    //just for test
  //  OutputXbarRegs.OUTPUT7MUX0TO15CFG.bit.MUX4 = 1;//set CMPSS3.CTRIPH_OR_CTRIPL for output 8
  //  OutputXbarRegs.OUTPUT7MUXENABLE.bit.MUX4 = 1; //1:enable 0:disable

    //enable DAC
    Cmpss3Regs.COMPCTL.bit.COMPDACE = 1; //0:disable 1:enable
    EDIS;

    //just for test
    //GPIO11 is set to OUTPUT XBAR7
   // GPIO_SetupPinMux(11U, GPIO_MUX_CPU1, OUTPUTXBAR7);
   // GPIO_SetupPinOptions(17U, GPIO_OUTPUT, GPIO_PUSHPULL);

}

static void InitCMPSS5(void)
{
    EALLOW;

    //
    //Enable CMPSS5 for _PFCCURR1 OCP
    //
    Cmpss5Regs.COMPCTL.bit.COMPDACE = 0; //0:disable 1:enable

    //input GPIO define
    AnalogSubsysRegs.CMPHPMXSEL.bit.CMP5HPMXSEL = 2; // select PGA5_IN
    AnalogSubsysRegs.CMPLPMXSEL.bit.CMP5LPMXSEL = 2;

    //
    //NEG signal comes from DAC
    //
    Cmpss5Regs.COMPCTL.bit.COMPHSOURCE = 0;//0:DAC 1:EXT PIN

    //
    //Use VDDA as the reference for DAC = 3.3V
    //
    Cmpss5Regs.COMPDACCTL.bit.SELREF = 0;//0:VDDA 1:VDAC

    //
    // CMPSS output invert
    //
    Cmpss5Regs.COMPCTL.bit.COMPLINV = 0;
    Cmpss5Regs.COMPCTL.bit.COMPHINV = 0;

    //
    //Set DAC to midpoint for arbitrary reference
    //
    Cmpss5Regs.DACHVALS.bit.DACVAL = 248; // 199; //=50A*0.004*4096/3.3=248
    Cmpss5Regs.DACLVALS.bit.DACVAL = 248; //=60A*0.004*4096/3.3=298

    //
    // Configure Digital Filter
    //
    Cmpss5Regs.CTRIPHFILCLKCTL.bit.CLKPRESCALE = 0x0;
    Cmpss5Regs.CTRIPLFILCLKCTL.bit.CLKPRESCALE = 0x0;

    //
    //Maximum SAMPWIN value provides largest number of samples
    //
    Cmpss5Regs.CTRIPHFILCTL.bit.SAMPWIN = 0x4;
    Cmpss5Regs.CTRIPLFILCTL.bit.SAMPWIN = 0x4;
    //Cmpss5Regs.CTRIPHFILCTL.bit.SAMPWIN = 0x1;
    //Cmpss5Regs.CTRIPLFILCTL.bit.SAMPWIN = 0x1;

    //
    //Maximum THRESH value requires static value for entire window
    //THRESH should be GREATER than half of SAMPWIN
    //
    Cmpss5Regs.CTRIPHFILCTL.bit.THRESH = 0x4;
    Cmpss5Regs.CTRIPLFILCTL.bit.THRESH = 0x4;
    //Cmpss5Regs.CTRIPHFILCTL.bit.THRESH = 0x1;
    //Cmpss5Regs.CTRIPLFILCTL.bit.THRESH = 0x1;

    //
    //Reset filter logic & start filtering
    //
    Cmpss5Regs.CTRIPHFILCTL.bit.FILINIT = 1;
    Cmpss5Regs.CTRIPLFILCTL.bit.FILINIT = 1;

    // Configure CTRIPOUT path
    // Digital filter output feeds CTRIPH and CTRIPOUTH
    //0 Asynchronous comparator output drives CTRIPH
    //1 Synchronous comparator output drives CTRIPH
    //2 Output of digital filter drives CTRIPH
    //3 Latched output of digital filter drives CTRIPH

    Cmpss5Regs.COMPCTL.bit.CTRIPHSEL = 2; //for PWM X-BAR
    Cmpss5Regs.COMPCTL.bit.CTRIPLSEL = 2; //for PWM X-BAR


   // Cmpss5Regs.COMPCTL.bit.CTRIPOUTHSEL = 2; //for OUTPUT XBAR
    //Cmpss5Regs.COMPCTL.bit.CTRIPOUTLSEL = 2; //for OUTPUT XBAR

    //choose CMPSS5.CTRIPL for TRIP9
    //EPwmXbarRegs.TRIP9MUX0TO15CFG.bit.MUX8 = 1;//CMPSS5.CTRIPH_OR_CTRIPL
    EPwmXbarRegs.TRIP9MUX0TO15CFG.bit.MUX8 = 0;//CMPSS5.CTRIPH
    EPwmXbarRegs.TRIP9MUXENABLE.bit.MUX8 = 1;

    //just for test
   // OutputXbarRegs.OUTPUT7MUX0TO15CFG.bit.MUX8 = 1;//set CMPSS5.CTRIPH_OR_CTRIPL  for output 8
    //OutputXbarRegs.OUTPUT7MUXENABLE.bit.MUX8 = 1; //1:enable 0:disable

    //enable DAC
    Cmpss5Regs.COMPCTL.bit.COMPDACE = 1; //0:disable 1:enable
    EDIS;

    //just for test
    //GPIO11 is set to OUTPUT XBAR7
  //  GPIO_SetupPinMux(11U, GPIO_MUX_CPU1, OUTPUTXBAR7);
  //  GPIO_SetupPinOptions(17U, GPIO_OUTPUT, GPIO_PUSHPULL);

}
//
// End of File
//

//

