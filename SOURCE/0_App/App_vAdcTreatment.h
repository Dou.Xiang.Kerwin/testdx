/*===========================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : H6413Z
 *
 *  FILENAME : gbb_AdcTreatment.c
 *  PURPOSE  : analog sample, filter,calculation       
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2009-02-10      A000           DSP               Created.   Pre-research
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *    see variables definition for detail information     
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *    see functions declare for detail information 
 *   
 *==========================================================================*/
#ifndef   App_vAdcTreatment_H
#define   App_vAdcTreatment_H

 #define ADC_EXT_REF_TSSLOPE  (*(int16 *)0x705BF)
#define ADC_EXT_REF_TSOFFSET (*(int16 *)0x705C0)



extern ubitfloat g_lq10ACPeak_CalVpfc;



extern ubitfloat g_lq10VinwaveSinDegree;

extern UINT16 g_u16OverRelayTimer;      // over relay connect status timer

//extern Uint32 g_u32FailFlagPointer;
extern ubitfloat  g_lq10AcFre;          //actual rectifir Acfrequency


extern ubitfloat g_lq10MdlCurrNoFilter; // actual rectifir current without filter
extern ubitfloat g_lq10MdlCurr;         // actual rectifir current with filter
extern ubitfloat g_lq10MdlCurrDisp;     // rectifir current for display
extern ubitfloat g_lq10MdlCurrDisp2;    // rectifir current for monitor display
extern ubitfloat g_lq10MdlCurrDisp3;    // rectifir current for monitor display
extern ubitfloat g_lq10MdlVoltNoFilter; // actual rectifir dcvolt without filter

extern ubitfloat g_lq10PfcVoltDisp;     //pfc volt for display
extern ubitfloat g_lq10TempAmbi;        // rectifier intake temperature
extern ubitfloat g_lq10TempAmbiDisp;//rectifier intake temperature for display
extern ubitfloat g_lq10TempAmbiCtrl;
extern ubitfloat g_lq10TempDc;          //rectifier internal temperature
extern ubitfloat g_lq10TempDcDisp; //rectifier internal temperature for display
extern ubitfloat g_lq10TempPfc;         //rectifier internal temperature
extern ubitfloat g_lq10TempPfcDisp; //rectifier internal temperature for display


extern ubitfloat g_lq10TempDcDispSave;
extern ubitfloat g_lq10TempPfcDispSave;
extern ubitfloat g_lq10TempSensor;

extern ubitfloat g_lq10AcVolt;          // actual rectifir Acvolt
extern ubitfloat g_lq10AcCur;         //actual rectifir ac cur
extern ubitfloat g_lq10AcPow;
extern ubitfloat g_lq10ACPeak_protect;  //AC peak value for protect

extern UINT16 g_u16PermitAdjust;       //adjust flag

extern void vAdcTreatment(void);

#endif



//===========================================================================
// No more.
//===========================================================================

