/*=============================================================================*
 *         Copyright(c) 2004-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : HD415CZ
 *
 *  FILENAME : gbb_CanDriver.h
 *  PURPOSE  : rectifier communicate with controller and other rectifiers	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *     2008-11-25      A000           zoulx             Created.   Pre-research
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *    detail information refer to gbb_main.h      
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                               		     DESCRIPTION 
 *============================================================================*/

#ifndef Drv_ADC_H
#define Drv_ADC_H
 
extern  void InitAdc(void);
#endif
