#ifndef GBB_epromdata_H
#define GBB_epromdata_H

#define MDLCTRL_EPROM_BIT         0x31C6 // 写入EPROM的g_u16MdlCtrl位 0x31C0
#define MDLCTRL_NOT_EPROM_BIT     0xCE39 //不写入EPROM的g_u16MdlCtrl位0xCE3F
struct EPROM_BITS {         // bits  description
    Uint16  RUNTIME:1;      // 0    module run time
    Uint16  MDLCTRL:1;      // 1    module control flag
    Uint16  INITCAN:1;      // 2    initial can flag
    Uint16  VOLTUP:1;       // 3    module max dc voltage
    Uint16  VDCDFT:1;       // 4    module default dc voltage
    Uint16  TEMPUP:1;       // 5    dc max temp flag
    Uint16  REONTIME:1;     // 6    module reon time
    Uint16  WALKTIME:1;     // 7    module walk-in time
    Uint16  ORDERTIME:1;    // 8    module order on time
    Uint16  POWERDFT:1;     // 9    module default power
    Uint16  CURRRDFT:1;     // 10   module default current
    Uint16  ACCURRRDFT:1;   // 11   module default AC current
    Uint16  MDLCTRLEXTEND:1;    // 12   module AC limit power mode
    Uint16  WATTACCUMULATE:1;   // 13   module input power accumulate
    Uint16  WATTACCUMULATETIME:1;// 14  module input power accumulate time
    Uint16  ANTITHIEFDELAY:1;           // 15 //paul-20220228

};

typedef union {
   Uint16               all;
   struct EPROM_BITS   bit;
}ubitintd;

struct EPROMEXTEND_BITS {       // bits  description
    Uint16  RECORDTIMESELF:1;   // 0    module run time by oneself
    Uint16  DCOCP_PFCOVP:1;         // 1    DCOCP cnt
    Uint16  EEPROMROINTER:1;    // 2    eeprom pointer
    Uint16  INTOSC:1;    // 3    eeprom pointer
    Uint16  rsd:12;             // 15:4
};

typedef union {
   Uint16               all;
   struct EPROMEXTEND_BITS   bit;
}ubitintdextend;

extern  ubitfloat g_fRdTemp;
extern  UINT16 g_u16I2CErrorType;

extern ubitintd  g_u16EpromWr;      //eeprom write flag
extern ubitintdextend   g_u16EpromWrExtend;      //eeprom write flag

//write float in some one address
extern void vWriteE2PROMSIG(void);
//write float in three address and verify
extern UINT16 ucWriteFloatDataThree(UINT16 u16Address, ubitfloat fTemp);

//read float in three address and verify
extern ubitfloat fReadFloatDataThreePointer(UINT16 u16Address);
//Write Data to EEPROM
extern UINT16 uiI2caWriteDataPointer(UINT16 u16Address, ubitfloat fTemp);
extern UINT16 uiI2caWriteData1Pointer(UINT16 u16Address, ubitfloat fTemp);
extern UINT16 uiI2caWriteData2Pointer(UINT16 u16Address, ubitfloat fTemp);

//Read Data from EEPROM
extern UINT16 uiI2caReadDataPointer(UINT16 u16Address);
extern UINT16 uiI2caReadData1Pointer(UINT16 u16Address);
extern UINT16 uiI2caReadData2Pointer(UINT16 u16Address);

extern void vLimitInit(void);

#endif
