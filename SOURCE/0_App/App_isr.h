// Running on TMS320LF280xA               
// External clock is 20MHz, PLL * 10/2 , CPU-Clock 100 MHz	      
// Date: from 2005/12/28  ,jurgen
// Version:1.00     Change Date: 
/***************************************************************************
*             Defines
***************************************************************************/
#ifndef App_IsrVars_H
#define App_IsrVars_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************************************************************
*            Variables Definition
***************************************************************************/
struct IsrVarsStruct
{
//------------offset=0000H--_DCDC_DP------------------
int16  	  _i16LowTempLoopflag;
int16	  _i16VdcSet;            
int16	  _i16AdVdcSam0;        
int16     _i16AdVdcSam1;

longunion _lVdcFilterK1; 
int16     _i16VdcFilterK2;     
int16     _i16VdcFilterK3; 

longunion _lVdcUse;     
int16     _i16VdcErrUse1;           
int16 	  _i16VdcErrUse2;

int16     _i16VdcLoopK3;     
int16     _i16VdcLoopK4;          
int16     _i16VdcRippleRangePos;
int16     _i16VdcRippleRangeNeg;

//---------------offset=0010H-------------------------
int16     _i16VdcRippleUsePos;
int16     _i16VdcRippleUseNeg;
longunion _lVdcRippleUse;

int16     _i16IdcdcSys; 
int16     _i16IdcLoopFilterK2;
longunion _lCurrLim;

int16     _i16CurrLimFloor;  //��·ʱ��ɱȽϴ��ֵ������ά�ֶ�·��������������ʱ���Ƶ���0
int16     _i16PowerMaxExtra;    
longunion _lVdcPiOut;

int16     _i16PowerLimMin;
int16     _i16PowerLimMax;  
int16	  _i16DCDCPowerSet;
int16     _i16DCDCPowerFeedback;

//---------------offset=0020H-------------------------
Uint32      _u32Vdc47v25Point;
Uint32      _u32Vdc47v7Point;

int16     _i16AdIdc;
int16 	  _i16IdcPioutPermit;
longunion _lIdcPiout;

int16 	  _i16DcdcPWMTsMin;
int16 	  _i16DcdcPWMTsMax;
int16	  _i16DcdcPWMDutyMax;
Uint16	  _u16AcChangeFlag;

longunion _lDcdcDutyShadow;
longunion _lDcdcPWMTsShadow; 
 
//---------------offset=0030H-------------------------
longunion _lDcdcPWMDutyK;
int32     _lDcdcPWMDutyD;

Uint16	  _u16LowPowerFlag2;
int16 	  _i16VdcOVPTimes;
int16	  _i16VdcHVSDTimes;
int16     _i16LowACvoltageFlag;

int16     _i16LowACLPowerFlag;
int16     _i16VRippleK1;
int16	  _i16VRippleK2;
int16	  _i16VRippleK3;

int16     _i16PowerLimMaxUse;
Uint16    _u16ChoiseCon;
Uint16    _u16RippleDisable;
int16     _i16IdcLoopFilterK3;

//---------------offset=0040H-_DCDC1_DP---------------------
longunion _lIdcDisUse;
int16     _i16AdIdcDis;
int16     _i16IdcdcLoopQn;
longunion _lVdcDisUse;
int16     _i16AdVdcDis;        
Uint16    _u16DcdcILoopOpen;  //no used
int16     _i16DcdcVoltRipple0;
int16     _i16DcdcVoltRipple1;
int16     _i16VdcShortCoefA;
int16     _i16VdcShortCoefB;
Uint16    _u16Pointer;
Uint16    _u16PCtrl;
Uint16    _u16PAddress;
Uint16    _u16PAddress2;

//---------------offset=0050H-------------------------
Uint16    _u16PAddress3;
Uint16    _u16PAddress4;
longunion _lU1Temp;
longunion _lDcTemp;
Uint16    _u16ADU1Temp;
Uint16    _u16ADDcTemp;
//longunion _lDCDC_Nouse1A;
Uint16  _u16RamReadEn;
Uint16  _u16RamReadNoUse;
Uint16    _u16SwitchFlag;
int16     _i16IdcdcSysTrue;
longunion _lFanBad;       //20081211 cxm
Uint16    _u16ADFanBad;    //20081211 cxm
int16     _i16PfcDynFlg;  // ls change for HVSD 20111108 

//---------------offset=0060H-------------------------
//qtest0224
//longunion _lDCDC_Nouse01;
int16    _i16PFCFastLoopForce;
int16    _i16PFCFastLoopCnt;

int16 _i16VpfcOnePhasePower;
int16 _i16DCPFCPowerDetlaIsr;
//longunion _lDCDC_Nouse02;

longunion  _lDCPFCPowerDetlaIsrUse;
//longunion _lDCDC_Nouse03;
//longunion _lDCDC_Nouse04;
int16   _i16DCPFCPowerDetlaIsrUseFinal;
int16   _i16QtestNoUse;
//qtest0224 end
//qtest0601
//longunion _lDCDC_Nouse05;
//longunion _lDCDC_Nouse06;
//longunion _lDCDC_Nouse07;
//longunion _lDCDC_Nouse08;
longunion   _lIdcFilterOut;
int16          _i16AdIdcSamp;
int16          _i16CphaseOnCnt;
Uint16        _u16PhaseSheddingFlagTest;
int16          _i16AdIdc14A;
int16    _i16SingleToDualQuick;
int16    _i16PhaseShieldingTest;
//int16    _i16DCDC_Nouse08;
//qtest0601end

//---------------offset=0070H-------------------------
Uint16    _u16DcOpened;      
Uint16    _u16nouse1111;
Uint16    _u16NormalRunTimer;      
Uint16    _u16LowTempLVoutLLoadflag;  //WL added for Low temp DC voltage loop
int16     _i16AdIdc25A;//xbo
Uint16    _u16LoadDeltaFlag;//xbo
Uint16    _u16MainRly_Timer; //20081203 cxm
Uint16    _u16DcdcOCPPfcOVP;  //20081203 cxm
Uint16    _u16IsrTimer;
int16     _i16VFanSet   ;//20081209 cxm
int16     _i16VdcSetTmp;//xbo
int16     _i16LoadDelta;//xbo
Uint16    _u16PFCOCP; //PFC OCP COUNT
Uint16    _u16SoftStartEndTimer;//count for dc soft start end
longunion _lDCDC_Nouse09;

//---------------offset=0080H-_DCDC2_DP---------------------
int16     _iq16DcVolCalDcFreqCoefA;
int16     _i16DcVolCalDcFreqCoefB;
Uint16    _u16VolSamCaliSuccess;
Uint16    _u16DcdcPWMTsMaxHighLine_80K;
Uint16    _u16DcdcPWMTsMaxLowLine_103k;
Uint16    _u16DcdcPWMTsLIM_95K;
Uint16    _u16PermitOverAcFlag;
Uint16    _u16ADPFCTemp;
longunion _lPFCTemp;
int16     _i16ShortIdcOffset;//
int16     _i16ShortVdcOffset;//
//longunion _lDCDC_Nouse0A;
int16     _i16DsptmpFlag;
int16     _i16AbmtmpFlag;

longunion _lDCDC_Nouse0B;

//---------------offset=0090H-------------------------
longunion _lVdcLoopFilterK1;
int16     _i16VdcLoopFilterK2 ;
int16     _i16VdcLoopFilterK3;
int16     _i16polartiyV;
Uint16    _u16LowVolDcLoopChgFlag;
Uint16    _u16LowPowerFlag1;
Uint16    _u16DCCurrADtrigValue;    //DQA add 20121226
Uint16    _u16DCCurrADtrigValue2;
int16     _i16VacDynFlag;
Uint16   _u1632BitConRamEn;      // .set    009AH
Uint16   _u1632BitSingleRamEn;  //    .set    009BH

//Uint16    _u16DCDC_Nouse0D;
//Uint16    _u16DCDC_Nouse0E;

Uint16    _u16DCDC_Nouse0F;
Uint16    _u16DCDC_Nouse0G;
Uint16    _u16DCDC_Nouse0H;
Uint16    _u16DCDC_Nouse0I;

//---------------offset=00A0H-------------------------
Uint16    _u16DCDC_Nouse01;
Uint16    _u16DCDC_Nouse02;
Uint16    _u16DCDC_Nouse03;
Uint16    _u16DCDC_Nouse04;
Uint16    _u16DCDC_Nouse05;
Uint16    _u16DCDC_Nouse06;

longunion _lOpenPIoutStart;
longunion _lOpenLoopStep;
Uint16  _u16OpenLoopCtrl;
Uint16  _u16OpenLoopLR;
longunion _lOpenPRDDUN;
longunion _lOpenPRDUP;

//Uint16    _u16DCDC_Nouse07;
//Uint16    _u16DCDC_Nouse08;
//Uint16    _u16DCDC_Nouse09;
//Uint16    _u16DCDC_Nouse010;
//Uint16    _u16DCDC_Nouse011;
//Uint16    _u16DCDC_Nouse012;
//Uint16    _u16DCDC_Nouse013;
//Uint16    _u16DCDC_Nouse014;
//Uint16    _u16DCDC_Nouse015;
//Uint16    _u16DCDC_Nouse016;

//---------------offset=00B0H-------------------------
Uint16    _u16DCDC_DynDelayCnt;
Uint16    _u16DCDC_OvpDelayCnt;    //.set    00B1H
Uint16    _u16DCDC_OvpLimitFlag;   //.set    00B2H
Uint16    _u16DCDC_OvpLimitCeiling;  //.set    00B3H
Uint16    _u16DCDC_OvpLimitFloor;    //.set    00B4H
Uint16    _u16DCDC_Nouse16;
Uint16    _u16DCDC_Nouse17;
Uint16    _u16DCDC_Nouse18;
Uint16    _u16DCDC_Nouse19;
Uint16    _u16DCDC_Nouse110;
Uint16    _u16DCDC_Nouse111;
Uint16    _u16DCDC_Nouse112;
Uint16    _u16DCDC_Nouse113;
Uint16    _u16DCDC_Nouse114;
Uint16    _u16DCDC_Nouse115;
Uint16    _u16DCDC_Nouse116;

//------------offset=00C0H-- _PFC_DP---------------------
Uint16    _u16PermitOverAc;
Uint16    _i16AdVpfcProtect;
longunion _lVpfcProtect;
Uint16    _u16MinTimer;
Uint16    _u16PriOcp;
Uint16    _u16OverAcVolt;
Uint16    _u16AdAcVoltcount;
Uint32    _u32AdAcVoltAddress;
int16     _u16AcVoltPhaseshift;
int16     _u16IpfcLoopGaincoef;
Uint16    _u16IpfcLoopGaincoefWant;
int16     _u16Reserved;
Uint16    _u16QuickProtCnt;
Uint16    _u16PfcZCD3Cnt;

//-------------offset=00D0H-----------------------
int16     _i16IpfcErr0;
int16     _i16IpfcErr1;
longunion _lIpfcErrFilterK1;
int16     _i16IpfcErrFilterK2;
int16     _i16IpfcErrFilterK3;
longunion _lIpfcErrUse0;
longunion _lIpfcErrUse1;
int16     _i16IpfcLoopK3;
int16     _i16IpfcLoopK4;
longunion _lIpfcPiOut;
int16     _i16PfcQn;
int16     _i16PfcMinDuty;

//-------------offset=00E0H------------------------
int16     _i16IpfcErr20;
int16     _i16IpfcErr21;
longunion _lIpfcErrFilterK21;
int16     _i16IpfcErrFilterK22;
int16     _i16IpfcErrFilterK23;
longunion _lIpfcErrUse20;
longunion _lIpfcErrUse21;
int16     _i16IpfcLoopK23;
int16     _i16IpfcLoopK24;
longunion _lIpfcPiOut2;
int16     _i16PfcQn2;
int16     _i16PfcMinDuty2;

//--------------offset=00F0H------------------------
longunion _lVacRmsSqrInvUse;
longunion _lVacRmsSqrInvFast;
Uint16    _u16PfcZCD1Cnt;
int16     _i16PfcSWoffFlag;   //off:1   .on:0
int16     _i16VpfcPiTmp;
//qtest0601
int16     _i16DualDrvStratFlag;
//Uint16    _u16PFC_Nouse02;
//qtest0601end
Uint16    _u16PFC_Nouse03;
Uint16    _u16PFC_Nouse04;
Uint16    _u16PFC_Nouse05;
Uint16    _u16PFC_Nouse06;
Uint16    _u16PFC_Nouse07;
Uint16    _u16PFC_Nouse08;
Uint16    _u16PFC_Nouse09;
Uint16    _u16PFC_Nouse010;


//------------offset=00100H-- _PFC1_DP---------------------
Uint16    _u16PfcOpened;
int16     _i16AdVpfc0;
int16     _i16AdVpfc1;
int16     _i16VpfcFilterK2;
longunion _lVpfcFilterK1;
longunion _lVpfcUse;
int16     _i16VpfcSet;
int16     _i16VpfcErr0;
int16     _i16BurstAct10msDly;
int16     _i16VpfcLoopKp;
int16     _i16VpfcLoopKi;
int16     _i16VpfcPiOutMax;
longunion _lVpfcPiOut;

//-------------offset=00110H-----------------------
Uint16    _u16VpfcOverFlag;
int16     _i16PFCPRDFLG;
Uint16    _u16VpfcPara;
Uint16    _u16VpfcQn;
int16     _i16AdAcVoltSamp;
int16     _i16PfcMaxPower;
int16     _i16PfcQuickLoop;
int16     _i16LowTempPFCloopFlag;
int16     _i16AdAcMax;
Uint16    _u16PfcQuickPFCUVPFault;
Uint16    _u16VpfcOnePhasePower;
Uint16    _u16IpfcTon2CMPA;
Uint16    _u16PfcQuickPFCOVPFault;
Uint16    _u16PfcPWM1TsCTR;
//Uint16    _u16NouseA;
Uint16  _u16PwmFirstOffCnt;
Uint16    _u16LowPfcVoltCnt;
//-------------offset=0120H------------------------
Uint16    _u16vref1v55;
int16     _i16AdAcVoltTrue;
int16     _i16AdVacThreshold;
int16     _i16AdIpfcTrue;
int16     _i16AdIpfcTrue2;
Uint16    _u16IpfcTonTemp;
longunion _lVacRmsSqrInv;
Uint32    _u32Lpfc;
int16     _i16Coss;
int16     _i16TLC;
int16     _i16PfcDutyPermit;
Uint16    _u16IpfcTon2CMPB;
Uint16    _u16PfcSinglePhaseMode;
Uint16    _u16SlaveOffFlag;
//--------------offset=0130H------------------------
int16     _i16LLoadBurstOutTime;
int16     _i16LLoadBurstInTime;
int16     _i16BurstOffDrvFlag;
int16     _i16LLoadBurstFlag;
Uint16    _u16VacPolarity;
Uint16    _u16AdVacPosDelayTime;
Uint16    _u16AdVacNegDelayTime;
int16      _i16pfcompare1;
Uint16    _u16CapDischargeLim;
//Uint16    _u16CCMWorkTime;
//Uint16    _u16TCMWorkTime;
int16     _i16PfcFaultFlag;
Uint16    _i16BurstSumFlag;
int16     _i16TonKpower;
Uint16    _u16VacZeroFlag;
Uint16    _u16PwmEnableFlag;
Uint16    _u16CCMWorkFlag;
int16     _i16FirstPwmDrvFlag;

//------------offset=00140H-- _PFC2_DP---------------------
longunion _lAcSamVoltAvg;
longunion _lAcVoltAvgFilt_LW;
longunion _lAcVoltAvgFilt;
longunion _lAcVoltAvgFilt1;
longunion _lAcVoltAvgOut_LW;
longunion _lAcVoltAvgOut;
Uint16    _u16DC_NL_CNT;
Uint16    _u16DC_LN_CNT;
Uint16    _u16PFC_Nouse012;
Uint16    _u16PFC_Nouse013;


//---------------offset=0150H-------------------------
int16     _i16VbusSysa;
int16     _i16VbusSysb;
Uint16    _u16RamReadCnt;
Uint16    _u16PfcOcpCnt;

int16     _i16AcDropFlag;

//Uint16    _u16PFC_Nouse014;
Uint16   _u16RamReadInterval;
Uint16    _i16NoiseLoopflag;
Uint16    _i16NoiseLoopflag2;

Uint16    _u16PFC_Nouse017;
Uint16    _u16PFC_Nouse018;
Uint16    _u16DCDC_Dynflag;
Uint16    _VdcUse_DEBUG;

//Uint16    _u16PFC_Nouse01A;
Uint16    _u16PFC_Nouse01B;
Uint16    _u16PFC_Nouse01C;
Uint16    _u16PFC_Nouse01D;
Uint16    _u16PFC_Nouse01E;



//---------------offset=0160H-------------------------
longunion _lAcSamVoltSquare;
longunion _lAcVoltSquareFilt_LW;
longunion _lAcVoltSquareFilt;
longunion _lAcVoltSquareFilt1;
longunion _lAcVoltSquareRms_LW;
longunion _lAcVoltSquareRms;
longunion _lVacAvgFltK;
longunion _lVacRmsSquFltK;

//---------------offset=0170H-------------------------
int16      _i16AcUpjumpFlag;
Uint16    _u16InputLowVolCNT;  //WL added for Low temp DC voltage loop
int16       _i16Vpfc460VPoint;
int16       _i16Vpfc490VPoint;
int16       _i16Vpfc300VPoint;
int16       _i16Vpfc70VPoint;
Uint16    _u16PFC_Nouse020;
Uint16    _u16PFC_Nouse021;
Uint16    _u16PFC_Nouse022;
Uint16    _u16PFC_Nouse023;
Uint16    _u16PFC_Nouse024;
Uint16    _u16PFC_Nouse025;
Uint16    _u16PFC_Nouse026;
Uint16    _u16PFC_Nouse027;
Uint16    _u16PFC_Nouse028;
Uint16    _u16PFC_Nouse029;
//------------offset=00180H-- _PFC3_DP---------------------
Uint16    _u16PFC_Nouse030;
Uint16    _u16PFC_Nouse031;
Uint16    _u16PFC_Nouse032;
Uint16    _u16PFC_Nouse033;
Uint16    _u16PFC_Nouse034;
Uint16    _u16PFC_Nouse035;
Uint16    _u16PFC_Nouse036;
Uint16    _u16PFC_Nouse037;
Uint16    _u16PFC_Nouse038;
Uint16    _u16PFC_Nouse039;
Uint16    _u16PFC_Nouse03A;
Uint16    _u16PFC_Nouse03B;
Uint16    _u16PFC_Nouse03C;
Uint16    _u16PFC_Nouse03D;
//Uint16    _u16PFC_Nouse03E;
//Uint16    _u16PFC_Nouse03F;
int16      _i16PfcQuickLoopQnSetQtest;
int16      _i16VpfcPioutShiftSetQtest;


//---------------offset=0190H-------------------------
int16       _i16VpfcP04mp;
Uint16      _u16LowPowerFlag;
int16       _i16VinHighPeakFlag;
int16       _i16PfcOvershootVal;
int16       _i16PfcQuickLoopQnSet;
int16       _i16VpfcPioutShiftSet;
int16       _i16PfcSlowLoopQnSet;
Uint16    _i16VpfcQnSet;
//qtest0601
float32   _f32SinglePhaseTonLimit;
int16     _i16SinglePhaseLargeCurrFlag;
//Uint16    _u16PFC_Nouse041;
//Uint16    _u16PFC_Nouse042;
//Uint16    _u16PFC_Nouse043;
//qtest0601end
Uint16    _u16PFC_Nouse044;
Uint16    _u16PFC_Nouse045;
Uint16    _u16PFC_Nouse046;
Uint16    _u16PFC_Nouse047;
Uint16    _u16PFC_Nouse048;
//---------------offset=01A0H-------------------------
int16       _i16AdAcVoltDelay0;
int16       _i16AdAcVoltDelay1;
int16       _i16AdAcVoltDelay2; 
int16       _i16AdAcVoltDelay3; 
int16       _i16AdAcVoltDelay4;
int16       _i16AdAcVoltDelay5;   
int16       _i16AdAcVoltDelay6;    
int16       _i16AdAcVoltDelay7;  
int16       _i16AdAcVoltDelay8;    
int16       _i16AdAcVoltDelay9;  
int16       _i16AdAcVoltDelay10;   
int16       _i16AdAcVoltDelay11;   
int16       _i16AdAcVoltDelay12;     
int16       _i16AdAcVoltDelay13;    
int16       _i16AdAcVoltDelay14;    
int16       _i16AdAcVoltDelay15;

//---------------offset=01B0H-------------------------
int16       _i16AdAcVoltDelay16;
int16       _i16AdAcVoltDelay17;
int16       _i16AdAcVoltDelay18; 
int16       _i16AdAcVoltDelay19; 
int16       _i16AdAcVoltDelay20;  
int16       _i16AdAcVoltDelay21;   
int16       _i16AdAcVoltDelay22;    
int16       _i16AdAcVoltDelay23;  
int16       _i16AdAcVoltDelay24;    
int16       _i16AdAcVoltDelay25;  
int16       _i16AdAcVoltDelay26;   
int16       _i16AdAcVoltDelay27;   
int16       _i16AdAcVoltDelay28;     
int16       _i16AdAcVoltDelay29;    
int16       _i16AdAcVoltDelay30;    
int16       _i16AdAcVoltDelay31; 
//------------offset=001C0H-- _PFC4_DP---------------------
int16       _i16AdAcVoltDelay32;
int16       _i16AdAcVoltDelay33;
int16       _i16AdAcVoltDelay34; 
int16       _i16AdAcVoltDelay35; 
int16       _i16AdAcVoltDelay36;  
int16       _i16AdAcVoltDelay37;   
int16       _i16AdAcVoltDelay38;    
int16       _i16AdAcVoltDelay39;  
int16       _i16AdAcVoltDelay40;    
int16       _i16AdAcVoltDelay41;  
int16       _i16AdAcVoltDelay42;   
int16       _i16AdAcVoltDelay43;   
int16       _i16AdAcVoltDelay44;     
int16       _i16AdAcVoltDelay45;    
int16       _i16AdAcVoltDelay46;    
int16       _i16AdAcVoltDelay47; 

//---------------offset=01D0H-------------------------
int16       _i16AdAcVoltDelay48;
int16       _i16AdAcVoltDelay49;
int16       _i16AdAcVoltDelay50; 
int16       _i16AdAcVoltDelay51; 
int16       _i16AdAcVoltDelay52;  
Uint16      _u16AdAcMaxPowLim;
longunion   _lVacUse;
int16       _i16AdAcVolt1;
Uint16      _u16PolarityFlgHz;
Uint16      _u16PolarityUse;
Uint16      _u16VinTimer1;
Uint16      _u16VinT;
Uint16      _u16VinFrequency;
Uint16      _u16VinF;
Uint16      _u16VinTimer2;

//---------------offset=01E0H-------------------------
longunion _lAcCurSamSquare; 
longunion _lAcCurSquareFilt_LW;//adding 64 var low 16 of low 32  by zzh 4-26
longunion _lAcCurSquareFilt;//after first filter of AC voltage^2
longunion _lAcCurSquareFilt1;
longunion _lAcCurSquareRms_LW;//adding 64 var high 16 of low 32  by zzh 4-26
longunion _lAcCurSquareRms;//after second filter of AC voltage^2
longunion _lAcCurAdcSquFltK;// add by zzh 4-26
longunion _lAcCurRmsSquFltK;// add by zzh 4-26

//**********offset=01F0H*****--------------------------
longunion _lAcCurSamSquare2;
longunion _lAcCurSquareFilt2_LW;//adding 64 var low 16 of low 32  by zzh 4-26
longunion _lAcCurSquareFilt2;//after first filter of AC voltage^2
longunion _lAcCurSquareFilt21;
longunion _lAcCurSquareRms2_LW;//adding 64 var high 16 of low 32  by zzh 4-26
longunion _lAcCurSquareRms2;//after second filter of AC voltage^2
longunion _lAcCurAdcSquFltK2;// add by zzh 4-26
longunion _lAcCurRmsSquFltK2;// add by zzh 4-26

//------------offset=0200H-- _PFC5_DP---------------------
float32  _f32VpfcVal;
float32  _f32InvVpfcVal;
float32  _f32VacAbsUse;
float32  _f32INVVacUse;
float32  _f32DiffVpfcVacUse;
float32  _f32INVDiffVpfcVacUse;
float32  _f32PFCDuty;
float32  _f32PwmTonshift;

//------------offset=0210H-- _PFC5_DP---------------------
float32  _f32TsTonRatio;
float32  _f32PhaseShiftCoef;
float32  _f32OCPTonPermit;
float32  _f32TonMaxPrd;
float32  _f32TonMinPrd;
float32  _f32TCMTonMaxCMPA;
float32  _f32TCMTonMinCMPA;
int16    _i16AdAcVoltPhaseShift;
int16    _i16LpfcPFU;


//------------offset=0220H-- _PFC5_DP---------------------
int16    _u16TCMTonFPU;
Uint16   _u16IpfcTon1CMPACLA;
Uint16   _u16IpfcTon2CMPACLA;
Uint16   _u16TCMHalfTsByCal;
float32  _f32TCMTonCMPA;
float32  _f32TCMTsByCal;
float32  _f32ExpLC;
float32  _f32Trv;
Uint16   _u16TonCompeK;
int16    _i16Trv;
float32  _f32Irv;

//------------offset=0230H-- _PFC5_DP---------------------
//float32  _f32TrCompeK;
float32   _f32TCMTonCMPA3;
Uint16   _u16TonHVpermit;
int16    _i16PwmTonshift;

Uint16   _u16LoopTest;
Uint16   _u16QVacAbsTest;
Uint16   _u16DiffVpfcVac;
Uint16   _u16ChangeFlag;

Uint16   _u16VacAbs;
Uint16  _u16BlankWindowTest;//  0xC236  //YQH 20200225
Uint16  _u16BlankWindowTest3;//  0xC237  //YQH 20200225
int16  _i16DCDCPowerSetDyn;   //0xC23B

int16  _i16DCDCPowerSetNew;
int16  _i16DCDCPowerSetBus;
int16  _i16DCDCPowerSetUse;
int16   _i16Qtest;


//------------offset=0240H-- _PFC5_DP---------------------

float32  _f32HVTonPermitMaxPrd;
float32  _f32IrvTrCLA;
float32  _f32IrvTrvCLA;
float32  _f32TCMTonPermit;
float32  _f32TonPermitMinPrd;
float32  _f32TrIpfcCompenCLA;

float32  _f32TrIrv;
float32  _f32TrvIrv;
//------------offset=0250H-- _PFC5_DP---------------------

float32  _f32VacVal;
int16    _i16IQ10TrIpfcOffsetCLA;
int16    _i16IQ10TrvIpfcCLA;
int16    _i16DiffVpfcVacuse;
int16    _i16DiffVpfcVacuse_H;
float32  _f32PfcDutyLimit;
int16    _i16IQ10TrIpfcCLA;
int16    _i16LpfcCLA;
int16    _i16VpfcGap;
Uint16   _u16TonShiftLimit;
int16    _i16TrCompenCoef;
Uint16   _u16LoadBurstFlagFPU;
float32  _f32FilterCoef;
//------------offset=0260H-- _PFC5_DP---------------------
Uint16   _u16Vref1V6CLA;
float32  _f32DACVALLowerLimitCCM;
Uint16   _u16AdresultDcOcp2;
int16    _i16PfcPhaseError;
Uint16   _u16PfcOcpCounter;
int16    _i16PFCPRDMINCLA;
int16    _i16CMPAStep;
int16     _i16AdAcVoltNoFilter;//65D
Uint16    _u16DelayTimeCnt;
Uint16    _u16VacTCMEnable;
};               

extern volatile struct IsrVarsStruct IsrVars;               

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif

