
#include "Prj_config.h"

#include "f28004x_device.h"
#include "Drv_ADC.h"
#include "Drv_EPWM.h"
#include "Drv_PGA.h"
#include "Drv_CMPSS.h"
//-------------------------------------------------------------------------
// InitPeripherals:
//-------------------------------------------------------------------------
// The following function initializes the peripherals to a default state.
// It calls each of the peripherals default initialization functions.
// This function should be executed at boot time or on a soft reset.
//
void InitPeripherals(void)
{

    //InitCpuTimers();   // For this example, only initialize the Cpu Timers
    InitCpuTimersPointer();
    //
    // Configure CPU-Timer 0, 1, and 2 to interrupt every second:
    // 100MHz CPU Freq, 1 second Period (in uSeconds)
    //
    //ConfigCpuTimer(&CpuTimer0, 100, 5000);
    //CpuTimer0Regs.TCR.all = 0x0000; //disable interrupt


    //BOOT区有为了实现开机输入过压保护的另外的初始化
    InitEPwm();

    InitPGA();

    //BOOT区有为了实现开机输入过压保护的另外的初始化
    InitAdc();

   //Initialize COMP Peripheral To default State:  改为采用IO口做DCOCP
    InitCMPSS();

    // Initialize eCAN Peripheral To default State:
    //InitECanPointer();
    //InitECanaIDPointer();
    //InitCan();
    InitCanPointer();
    // Initializes the I2C To default State:
    InitI2CPointer();


    //InitECapAPWMPointer(); //for FANPWM
    //InitECapture();
    InitECapturePointer();

}

//
// EnableInterrupts - This function enables the PIE module and CPU __interrupts
//
void EnableInterrupts()
{
    //
    // Enables PIE to drive a pulse into the CPU
    PieCtrlRegs.PIEACK.all = 0xFFFF;

    EPwm5Regs.ETSEL.bit.INTEN = 1;  // Enable ePWM6 INT  ~ CLA中断
    EPwm8Regs.ETSEL.bit.INTEN = 1;  // Enable ePWM8 INT  ~ 主计算中断
    //EPwm1Regs.ETSEL.bit.INTEN = 1;  // Enable ePWM1 INT  ~ 周期
    //EPwm3Regs.ETSEL.bit.INTEN = 1;  // Enable ePWM3 INT  ~ 相位

    IER |= M_INT2;   //包括各TZ中断
    IER |= M_INT3;   //Level INT3 is enabled 包括各EPWM中断

    // Enable global Interrupts and higher priority real-time debug events:
    EINT;   // Enable Global interrupt INTM
    ERTM;   // Enable Global realtime interrupt DBGM
}


void
InitPieCtrl(void)
{
    //
    // Disable Interrupts at the CPU level:
    //
    DINT;

    //
    // Disable the PIE
    //
    PieCtrlRegs.PIECTRL.bit.ENPIE = 0;

    //
    // Clear all PIEIER registers:
    //
    PieCtrlRegs.PIEIER1.all = 0;
    PieCtrlRegs.PIEIER2.all = 0;
    PieCtrlRegs.PIEIER3.all = 0;
    PieCtrlRegs.PIEIER4.all = 0;
    PieCtrlRegs.PIEIER5.all = 0;
    PieCtrlRegs.PIEIER6.all = 0;
    PieCtrlRegs.PIEIER7.all = 0;
    PieCtrlRegs.PIEIER8.all = 0;
    PieCtrlRegs.PIEIER9.all = 0;
    PieCtrlRegs.PIEIER10.all = 0;
    PieCtrlRegs.PIEIER11.all = 0;
    PieCtrlRegs.PIEIER12.all = 0;

    //
    // Clear all PIEIFR registers:
    //
    PieCtrlRegs.PIEIFR1.all = 0;
    PieCtrlRegs.PIEIFR2.all = 0;
    PieCtrlRegs.PIEIFR3.all = 0;
    PieCtrlRegs.PIEIFR4.all = 0;
    PieCtrlRegs.PIEIFR5.all = 0;
    PieCtrlRegs.PIEIFR6.all = 0;
    PieCtrlRegs.PIEIFR7.all = 0;
    PieCtrlRegs.PIEIFR8.all = 0;
    PieCtrlRegs.PIEIFR9.all = 0;
    PieCtrlRegs.PIEIFR10.all = 0;
    PieCtrlRegs.PIEIFR11.all = 0;
    PieCtrlRegs.PIEIFR12.all = 0;
}



#pragma CODE_SECTION(InitPieVectTable,"FlashBoot");
void InitPieVectTable(void)
{
    Uint16  i;
    //Uint32  *Source  =  (void  *)  &PieVectTableInit;
    Uint32  *Source  =  (void  *)  &USER_ISR_Pointer;
    Uint32  *Dest  =  (void  *)  &PieVectTable;

    //
    // Do not write over first 3 32-bit locations (these locations are
    // initialized by Boot ROM with boot variables)
    //
    Source  =  Source  +  3;
    Dest  =  Dest  +  3;

    EALLOW;

    for(i  =  0;  i  <  221;  i++)
    {
        *Dest++  =  *Source;
    }

    EDIS;

    //
    // Enable the PIE Vector Table
    //
    PieCtrlRegs.PIECTRL.bit.ENPIE  =  1;
}

#pragma CODE_SECTION(MemCopy,"FlashBoot");
void MemCopy(Uint16 *SourceAddr, Uint16* SourceEndAddr, Uint16* DestAddr)
{
    while(SourceAddr < SourceEndAddr)
    {
       *DestAddr++ = *SourceAddr++;
    }
     return;
}
