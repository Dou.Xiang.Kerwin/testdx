

/*=============================================================================*
 *         Copyright(c) 2008-2012, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : HD415CU111
 *
 *  FILENAME : HD415CU111_vEventLog.c
 *  PURPOSE  : read and write eventlog data from/into eeprom	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *   2015-04-13        A000           zlm        Created.   Pre-research 
 *	
 *============================================================================*/
#include "f28004x_device.h"	    // DSP280x Headerfile Include File
#include  "App_cal.h"
#include  "App_warnctrl.h"
#include  "App_vEventLog.h"
#include  "GBB_epromdata.h"
#include "App_isr.h"
#include "GBB_constant.h"
#include "App_constant.h"
#include "Prj_macro.h"
#include "App_main.h"
#include  "Prj_config.h"
/*******************************************************************************
*variables definition:these variables only can use in this file 														  
*******************************************************************************/
//调试备注
//IsrVars._u16PCtrl
/*

***中断时间读取内存***
1---设置使能
3---结束标志

***中断分排读取内存***
6---设置使能
8---结束标志

***连续读取故障记录EEPROM数据***到地址0x0000--0x01F8
5---设置使能  IsrVars._u16PCtrl == 5
7---结束标志

***连续清除故障记录EEPROM数据***
2---设置使能
4---结束标志


***连续读取校准数据等原EEPROM数据*** 到地址0x0200--0x0380
9---设置使能  IsrVars._u16PCtrl == 9
A---结束标志

*/


/*******************************************************************************
*globle variables definition:these variables  can use in all of the files 														  
**
*****************************************************************************/
ubitfloat g_lq10CurrentRunTime;			//record run time byself
ubitfloat g_lq10BaseRunTime;          	//record run time byself from eeprom
UINT16	  g_u16AddressPointer;			//eeprom address pointer in alarm record zone
static UINT16 	  g_u16WriteAlarmNumber;		//write alarm count

static UINT16    g_u16AlarmType;   			//当前需要记录的故障，g_u16AddressPointer对应的故障

// last mdl alarm 
ubitintb	g_u16MdlStatusLast;
ubitintbextend	g_u16MdlStatusExtendLast;

//for save alarm to a buffer 
static EepromData g_aEepromWriteBuf[EEPROM_W_SIZE];//EEPROM_W_SIZE *(1+1+2)=21*4=84个内存地址
static EepromData *g_pEepromWriteIn;		//pull in pointer
static EepromData *g_pEepromWriteOut;		//push out pointer

static UINT16 	g_u16EepromWriteQueAvail;	// = CAN_RX_Q_SIZE;
static UINT16 	g_u16EepromWriteQueEmpty;	//= TRUE;
static UINT16 	g_u16EepromWriteQueFull;	//= FALSE;

// for read eeprom data to Ram
UINT16 	*g_pReadEepromData;
UINT16 	g_u16ReadEepromStartAddr;


UINT16 	*g_pReadEepromClibData;
UINT16 	g_u16ReadEepromClibStartAddr;

//for clear eeprom  
UINT16 	g_u16ClearEepromStartAddr;
UINT16 	g_u16ClearNumber;
UINT16 	g_u16ClearEepromOk;	
UINT16	g_u16AlarmLogEnable;

static UINT16  s_u16CpuRestflag;
static UINT16  g_u16TimeRecodeState;

/*******************************************************************************
*functions declare:these functions only can use in this file                                    
*******************************************************************************/

//record time and alarm 
void  	vEventLog(void);	//call by main function 

//record time 
void 	vRecordTime(void);

//record event
void 	vRecordEvent(void);

//write alarm and time to eeprom 
void	vWriteEepromEvent(void);//115ms timebase 

// write alarm to eeprom  in some one address
void    vWriteAlarmOnce(unsigned int u16Address, unsigned int u16AlarmTemp,
                        ubitfloat fRunTimeTemp);

//push in stack 
void 	vEepromWriteIn(void);

//pull out the stack 
void 	vEepromWriteOut(void);

//stack or pointer int
void 	vEepromBufferInitial(void);

// read alarm data from eeprom 
void  	vReadEepromAlarmData(UINT16 u16Address);

void  	vReadEepromcClibaData(UINT16 u16Address);

//clear alarm data in eeprom  
void  	vClearEepromAlarmData(void);

/*******************************************************************************
 *Function name: vEventLog()  
 *Description :  alarm log                                                   
 *input:         void                                 
 *global vars:                
 *output:        void 
 *CALLED BY:     main()
 ******************************************************************************/
void  vEventLog(void)
{
    s_u16CpuRestflag = 1;
	vRecordTime();//record time byself  
	
	vRecordEvent();//record event to buffer 

}
/*******************************************************************************
 *Function name: vRecordEvent
 *Description:   the treatment of float write needed in the process of 
 *input:         communication u16Address:eeprom address
 *global vars:   void 
 *output:        void
 *CALLED BY:    vMtimerTreat()
 ******************************************************************************/
void	vRecordEvent(void)
{
		
//	实现功能
// 	发生故障之后触发一次记录故障功能
//	当检测故障消失的时候也触发一次记录故障
//	每次记录完一条记录后就增加一次EEPROM的地址指针
/*
1	OFFSTAT 模块关机  			发生 1，恢复2
2	
3	DCOV软件输出过压   			发生 3，恢复4
4	
5	AMBIOT环境温度过温   		发生 5，恢复6
6	
7	DCDCOT DCDC板温过温   		发生 7，恢复8
8	
9	FANFAIL 风扇故障   			发生 9，恢复10
10	
11	ACUV AC欠压   				发生 11，恢复12
12	
13	ACOV AC过压   				发生 13，恢复14
14	
15	PFCUV PFC欠压   			发生 15，恢复16
16	
17	PFCOV PFC过压   			发生 17，恢复18
18
19 CALIBRFAIL模块未校准			发生 19，恢复20
20	
21	NOCONTROLLER 通讯中断   	发生 21，恢复22
22	
23	IUNBALMAJOR 严重不均流   	发生 23，恢复24
24	
25	IUNBALMINOR轻微不均流   	发生 25，恢复26
26	
27	SHORT 短路   				发生 27，恢复28
28	
29	IDERR 地址重叠   			发生 29，恢复30
30	
31	HVSDLOCK 过压锁死   		发生 31，恢复32
32	
33	PFCOT   					发生 33，恢复34
34	
35	ACCOMHALT   				发生 35，恢复36
36	
37	HWHVSD   					发生 37，恢复38
38	
39	DCFAULT   					发生 39，恢复40
40	
41	DCFUSEBROKEN   				发生 41，恢复42
42	
43	PFCFAIL   					发生 43，恢复44
44	
45	ACOVPEAK   					发生 45，恢复46   
46	
47	ACOVRMS   					发生 47，恢复48	  
48	
49	DCOCP & hardware PFCOVP   	发生 49，恢复50   
50	
51	PFCOCP   					发生 51，恢复52   
52	
53	UNPLUG   					发生 53，恢复54   未使用
54
*/

	//故障信息收集 ，更新时间为主程序的时间
		
	//本次故障与上一次故障比较，是否有不一样的情况，如果有，触发一次故障记录
	if((g_u16MdlStatusLast.all != g_u16MdlStatus.all))
	{
		//定义故障类型
		//保证只记录一次
		//故障发生和恢复的时候均需要记录

	//-------------------------------------------------------------------
	//1	OFFSTAT 模块关机  			发生 1，恢复2
	//2		
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.OFFSTAT != g_u16MdlStatus.bit.OFFSTAT))
		{
			
			if (g_u16MdlStatus.bit.OFFSTAT)
			{
				g_u16AlarmType = (UINT16)1 ;

			}
			else
			{	
				g_u16AlarmType = (UINT16)2 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}

	//-------------------------------------------------------------------
	//3	DCOV软件输出过压   			发生 3，恢复4
	//4	
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.DCOV != g_u16MdlStatus.bit.DCOV))
		{
		
			if (g_u16MdlStatus.bit.DCOV)
			{
				g_u16AlarmType = (UINT16)3 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)4 ;	
			}
			//把数据压入堆栈
			vEepromWriteIn();
		}

	//-------------------------------------------------------------------
	//5	AMBIOT环境温度过温   		发生 5，恢复6
	//6	
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.AMBIOT != g_u16MdlStatus.bit.AMBIOT))
		{
		
			if (g_u16MdlStatus.bit.AMBIOT)
			{
				g_u16AlarmType = (UINT16)5 ;
			}
			else
			{
				g_u16AlarmType = (UINT16)6 ;	
			}
			//把数据压入堆栈
			vEepromWriteIn();
		}

	//-------------------------------------------------------------------
	//7	DCDCOT DCDC板温过温   		发生 7，恢复8
	//8
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.RECTOT != g_u16MdlStatus.bit.RECTOT))
		{
			
			if (g_u16MdlStatus.bit.RECTOT)
			{
				g_u16AlarmType = (UINT16)7 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)8 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}
		
	//-------------------------------------------------------------------
	//9	FANFAIL 风扇故障   			发生 9，恢复10
	//10	
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.FANFAIL != g_u16MdlStatus.bit.FANFAIL))
		{
			
			if (g_u16MdlStatus.bit.FANFAIL)
			{
				g_u16AlarmType = (UINT16)9 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)10 ;	
			}
			//把数据压入堆栈
			vEepromWriteIn();
		}

	//-------------------------------------------------------------------
	//11	ACUV AC欠压   				发生 11，恢复12
	//12	
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.ACUV != g_u16MdlStatus.bit.ACUV))
		{
		
			if (g_u16MdlStatus.bit.ACUV)
			{
				g_u16AlarmType = (UINT16)11 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)12 ;	
			}
			//把数据压入堆栈
			vEepromWriteIn();
		}

	//-------------------------------------------------------------------
	//13	ACOV AC过压   				发生 13，恢复14
	//14
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.ACOV != g_u16MdlStatus.bit.ACOV))
		{
		
			if (g_u16MdlStatus.bit.ACOV)
			{
				g_u16AlarmType = (UINT16)13 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)14 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}
		
	//-------------------------------------------------------------------
	//15	PFCUV PFC欠压   			发生 15，恢复16
	//16
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.PFCUV != g_u16MdlStatus.bit.PFCUV))
		{
			
			if (g_u16MdlStatus.bit.PFCUV)
			{
				g_u16AlarmType = (UINT16)15 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)16 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}
		
	//-------------------------------------------------------------------
	//17	PFCOV PFC过压   			发生 17，恢复18
	//18	
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.PFCOV != g_u16MdlStatus.bit.PFCOV))
		{
			
			if (g_u16MdlStatus.bit.PFCOV)
			{
				g_u16AlarmType = (UINT16)17 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)18 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}

	//-------------------------------------------------------------------
	//19 CALIBRFAIL模块未校准			发生 19，恢复20
	//20		
	//-------------------------------------------------------------------
	if((g_u16MdlStatusLast.bit.CALIBRFAIL != g_u16MdlStatus.bit.CALIBRFAIL))
	{
		
		if (g_u16MdlStatus.bit.CALIBRFAIL)
		{
			g_u16AlarmType = (UINT16)19 ;
		}
		else
		{
		
			g_u16AlarmType = (UINT16)20 ;	
		}

		//把数据压入堆栈
		vEepromWriteIn();
	}

	//-------------------------------------------------------------------
	//21	NOCONTROLLER 通讯中断   	发生 21，恢复22
	//22	
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.NOCONTROLLER != g_u16MdlStatus.bit.NOCONTROLLER))
		{
			
			if (g_u16MdlStatus.bit.NOCONTROLLER)
			{
				g_u16AlarmType = (UINT16)21 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)22 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}

	//-------------------------------------------------------------------
	//23	IUNBALMAJOR 严重不均流   	发生 23，恢复24
	//24	
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.IUNBALMAJOR != g_u16MdlStatus.bit.IUNBALMAJOR))
		{

			if (g_u16MdlStatus.bit.IUNBALMAJOR)
			{
				g_u16AlarmType = (UINT16)23 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)24 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}

	//-------------------------------------------------------------------
	//25	IUNBALMINOR轻微不均流	 	发生 25，恢复26
	//26	
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.IUNBALMINOR != g_u16MdlStatus.bit.IUNBALMINOR))
		{

			if (g_u16MdlStatus.bit.IUNBALMINOR)
			{
				g_u16AlarmType = (UINT16)25 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)26 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}

	//-------------------------------------------------------------------
	//27	SHORT 短路   				发生 27，恢复28
	//28	
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.SHORT != g_u16MdlStatus.bit.SHORT))
		{

			if (g_u16MdlStatus.bit.SHORT)
			{
				g_u16AlarmType = (UINT16)27 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)28 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
			g_u16SaveShortCompleteFlag = 1;
		}

	//-------------------------------------------------------------------
	//29	IDERR 地址重叠   			发生 29，恢复30
	//30	
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.IDERR != g_u16MdlStatus.bit.IDERR))
		{

			if (g_u16MdlStatus.bit.IDERR)
			{
				g_u16AlarmType = (UINT16)29 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)30 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}

	//-------------------------------------------------------------------
	//31	HVSDLOCK 过压锁死   		发生 31，恢复32
	//32
	//-------------------------------------------------------------------
		if((g_u16MdlStatusLast.bit.HVSDLOCK != g_u16MdlStatus.bit.HVSDLOCK))
		{

			if (g_u16MdlStatus.bit.HVSDLOCK)
			{
				g_u16AlarmType = (UINT16)31 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)32 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}

												
	}/* end if((g_u16MdlStatusLast.all != g_u16MdlStatus.all))*/


	//本次故障与上一次故障比较，是否有不一样的情况，如果有，触发一次故障记录
	if((g_u16MdlStatusExtendLast.all != g_u16MdlStatusExtend.all))
	{
		//定义故障类型
		//保证只记录一次
		//故障发生和恢复的时候均需要记录
	
	//-------------------------------------------------------------------
	//33	PFCOT   					发生 33，恢复34
	//34
	//-------------------------------------------------------------------
		if((g_u16MdlStatusExtendLast.bit.PFCOT != g_u16MdlStatusExtend.bit.PFCOT))
		{

			if (g_u16MdlStatusExtend.bit.PFCOT)
			{
				g_u16AlarmType = (UINT16)33 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)34 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}

		
	//-------------------------------------------------------------------
	//35	ACCOMHALT   				发生 35，恢复36
	//36
	//-------------------------------------------------------------------
		if((g_u16MdlStatusExtendLast.bit.ACCOMHALT != g_u16MdlStatusExtend.bit.ACCOMHALT))
		{

			if (g_u16MdlStatusExtend.bit.ACCOMHALT)
			{
				g_u16AlarmType = (UINT16)35 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)36 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}
		
	//-------------------------------------------------------------------
	//37	HWHVSD   					发生 37，恢复38
	//38
	//-------------------------------------------------------------------
		if((g_u16MdlStatusExtendLast.bit.HWHVSD != g_u16MdlStatusExtend.bit.HWHVSD))
		{

			if (g_u16MdlStatusExtend.bit.HWHVSD)
			{
				g_u16AlarmType = (UINT16)37 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)38 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}
				
	//-------------------------------------------------------------------
	//39	DCFAULT   					发生 39，恢复40
	//40
	//-------------------------------------------------------------------
		if((g_u16MdlStatusExtendLast.bit.DCFAULT != g_u16MdlStatusExtend.bit.DCFAULT))
		{

			if (g_u16MdlStatusExtend.bit.DCFAULT)
			{
				g_u16AlarmType = (UINT16)39 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)40 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}
						
	//-------------------------------------------------------------------
	//41	DCFUSEBROKEN   				发生 41，恢复42
	//42
	//-------------------------------------------------------------------
		if((g_u16MdlStatusExtendLast.bit.DCFUSEBROKEN != g_u16MdlStatusExtend.bit.DCFUSEBROKEN))
		{

			if (g_u16MdlStatusExtend.bit.DCFUSEBROKEN)
			{
				g_u16AlarmType = (UINT16)41 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)42 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}
						
	//-------------------------------------------------------------------
	//43	PFCFAIL   					发生 43，恢复44
	//44
	//-------------------------------------------------------------------
		if((g_u16MdlStatusExtendLast.bit.PFCFAIL != g_u16MdlStatusExtend.bit.PFCFAIL))
		{

			if (g_u16MdlStatusExtend.bit.PFCFAIL)
			{
				g_u16AlarmType = (UINT16)43 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)44 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}
						
	//-------------------------------------------------------------------
	//45	ACOVPEAK   					发生 45，恢复46
	//46
	//-------------------------------------------------------------------
	/*	if((g_u16MdlStatusExtendLast.bit.ACOVPEAK != g_u16MdlStatusExtend.bit.ACOVPEAK))
		{

			if (g_u16MdlStatusExtend.bit.ACOVPEAK)
			{
				g_u16AlarmType = (UINT16)45 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)46 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}*/
				
	//-------------------------------------------------------------------
	//47	ACOVRMS   					发生 47，恢复48
	//48
	//-------------------------------------------------------------------
	/*	if((g_u16MdlStatusExtendLast.bit.ACOVRMS != g_u16MdlStatusExtend.bit.ACOVRMS))
		{

			if (g_u16MdlStatusExtend.bit.ACOVRMS)
			{
				g_u16AlarmType = (UINT16)47 ;
			}
			else
			{
			
				g_u16AlarmType = (UINT16)48 ;	
			}

			//把数据压入堆栈
			vEepromWriteIn();
		}
	*/				
	/*dcocp和pfc ocp需要在考虑一下，最好做到g_u16MdlStatusExtend里面来*/   

	//-------------------------------------------------------------------		
	//49	DCOCP & hardware pfcovp  					发生 49，恢复50
	//50	
	//-------------------------------------------------------------------
	
	if((g_u16MdlStatusExtendLast.bit.DCOCP_PFCOVP != g_u16MdlStatusExtend.bit.DCOCP_PFCOVP))
	{

		if (g_u16MdlStatusExtend.bit.DCOCP_PFCOVP)
		{
			g_u16AlarmType = (UINT16)49 ;
		}
		else
		{
		
			g_u16AlarmType = (UINT16)50 ;	
		}

		//把数据压入堆栈
		vEepromWriteIn();
	}

	//-------------------------------------------------------------------
	//51	PFCOCP   					发生 51，恢复52
	//52	
	//-------------------------------------------------------------------

						
	//本模块没有不记录
	//-------------------------------------------------------------------
	//53	UNPLUG   					发生 53，恢复54
	//51	
	//-------------------------------------------------------------------



	}/* end if((g_u16MdlStatusExtendLast.all != g_u16MdlStatusExtend.all))*/

	/* 是否增加栈满的操作？？？20150708
	// 如果堆栈非满，
	if( g_u16EepromWriteQueFull == FALSE )
	{
	}*/

	//记录上一次故障记录情况
	g_u16MdlStatusLast.all 		 = 	g_u16MdlStatus.all;  //last 
	g_u16MdlStatusExtendLast.all =   g_u16MdlStatusExtend.all ;



}

/*******************************************************************************
*Description:      initial can receive and transmit buffer                           
*******************************************************************************/
void vEepromBufferInitial(void)
{
	g_u16EepromWriteQueAvail = EEPROM_W_SIZE;
	g_u16EepromWriteQueEmpty = TRUE;//ffff
 	g_u16EepromWriteQueFull = FALSE;

	g_pEepromWriteOut = g_aEepromWriteBuf;//21*4

	g_pEepromWriteIn = g_aEepromWriteBuf;

	g_pReadEepromData = (UINT16 *)0x0000;//0x9200;
	g_u16ReadEepromStartAddr = ALARM_START_ADDR;


	g_pReadEepromClibData = (UINT16 *)0x0200;//
	g_u16ReadEepromClibStartAddr =  EEPROM_CHK_ADDR;

	g_u16ClearEepromStartAddr = ALARM_START_ADDR;
	g_u16ClearEepromOk = 0;

}

 /*************************************************************************
 *  FUNCTION NAME: vEepromWriteIn
 *
 *  DESCRIPTION:	This routine increments the transmit EepromWrite Q top pointer.
 *					The Q is a circular buffer of 10 entries of structure
 *					type EepromWrite_Q.  This routine is called by the program to
 *             		push a message onto the queue so it can be transmitted.
 *
 *  PARAMETERS:	NONE 进栈处理

 ************************************************************************/   
void vEepromWriteIn(void)
{
	
	UINT16 u16BlockAddr; //block address 
	UINT16 u16WordAddr;  //word address 

	UINT16	u16AddressPointerTemp; 

	
	// 如果堆栈非满，
	if( g_u16EepromWriteQueFull == FALSE )
	{
		
		//把数据写入进栈指针所指位置
		(*g_pEepromWriteIn).uiAddress = g_u16AddressPointer;//8d05
		(*g_pEepromWriteIn).uiAlarm	  =	g_u16AlarmType;	 //8d0c	
		(*g_pEepromWriteIn).ubitfData.lData = g_lq10CurrentRunTime.lData;//8d18	

		
		/*	if at the top of the queue, need to wrap around to the bottom	*
		*	else just increment pointer	*/
		if (g_pEepromWriteIn == &g_aEepromWriteBuf[EEPROM_W_SIZE - 1])
		{
			g_pEepromWriteIn = g_aEepromWriteBuf;
		}
		else
		{
			g_pEepromWriteIn++;
		}
        // 非空
		g_u16EepromWriteQueEmpty = FALSE;	/*	just added something so queue is not empty	*/
		g_u16EepromWriteQueAvail--;			/*	indicate this one now unavailable	*/

		if (g_pEepromWriteIn == g_pEepromWriteOut)
		{
			/*	If after incrementing the top, the pointers are equal, the queue is full	*/
			g_u16EepromWriteQueFull = TRUE;
		}

		// update EEPROM addr 

		//每条记录占用6个EEPROM地址，一共168条故障---4block* (1275-1024+1)/6= 168
		//记录的EEPROM的首地址 1024~1270，记录的首地址最后一条为2038

		/* 	占用EEPROM 地址
//block	 4		5		6		7
		1024	1280	1536	1792  第一条记录
		  ~		  ~		 ~       ~
		1270	1526	1782	2038  块最后一条记录
		1271	1527	1783	2039
		1272	1528	1784	2040
		1273	1529	1785	2041
		1274	1530	1786	2042
		1275	1531	1787	2043
		*/

		u16AddressPointerTemp = g_u16AddressPointer;

		u16BlockAddr =  (UINT16)((u16AddressPointerTemp >> 8) & 0x0007);

		u16WordAddr = (UINT16)(u16AddressPointerTemp & 0x00ff);

		if(u16WordAddr == BLOCK_END_WORD_ADDR )//246
		{
			if (u16BlockAddr == BLOCK_ADDR) //7
			{
				u16AddressPointerTemp =  ALARM_START_ADDR ; 
			}
			else 
			{			
				//如果一个故障记录压入堆栈后，EEPROM的地址增加10
				u16AddressPointerTemp = u16AddressPointerTemp + 10;			
			}
				
		}
		else if(u16WordAddr < BLOCK_END_WORD_ADDR)
		{
			//如果一个故障记录压入堆栈后，EEPROM的地址增加6
			u16AddressPointerTemp = u16AddressPointerTemp + 6;
				
		}
		else  //u16WordAddr > 246
		{
			if (u16BlockAddr == BLOCK_ADDR) //7
			{
				u16AddressPointerTemp =  ALARM_START_ADDR ; 
			}
			else 
			{			
				
				u16AddressPointerTemp = (UINT16)((u16BlockAddr +1 ) << 8);		
			}
		
		}	
		
		//<1024 or >2038 ,g_u16AddressPointer =1024
		if((u16AddressPointerTemp < ALARM_START_ADDR)||(u16AddressPointerTemp > ALARM_END_ADDR))
		{
			g_u16AddressPointer = ALARM_START_ADDR;
		}
		else
		{
			g_u16AddressPointer = u16AddressPointerTemp;
		}
		
	}

}/*	end vEepromWriteIn	*/

	
/*************************************************************************
 *  FUNCTION NAME: vEepromWriteOut
 *
 *  DESCRIPTION:	This routine increments the transmit EEPROM Q bottom pointer.
 *					The Q is a circular buffer of 10 entries of structure
 *					type EepromWrite_Q.  This routine is called by the program and ISR
 *             		to remove a message from the queue when it has been loaded
 *             		into a transmitter.
 *
 *  PARAMETERS:	NONE 出栈处理
 *
 ************************************************************************/
void vEepromWriteOut(void)
{
	/*	if at the top of the queue, need to wrap around to the bottom	*
	*	else just increment pointer	*/
	if (g_pEepromWriteOut == &g_aEepromWriteBuf[EEPROM_W_SIZE - 1])
	{
		g_pEepromWriteOut = g_aEepromWriteBuf;
	}
	else
	{
		g_pEepromWriteOut++;
	}

	g_u16EepromWriteQueFull = FALSE;		/*	just removed an entry so queue not full	*/
	g_u16EepromWriteQueAvail++;			/*	indicate this one now available	*/

	if (g_pEepromWriteOut == g_pEepromWriteIn)
	{
		/*	If after incrementing bottom, the pointers are equal, the queue is empty	*/
		g_u16EepromWriteQueEmpty = TRUE;//ffff
	}
}/*	end vEepromWriteOut	*/
 

/*******************************************************************************
 *Function name: vWriteEepromEvent
 *Description:   the treatment of float write needed in the process of 
 *input:         communication u16Address:eeprom address
 *global vars:   void  把EEPROM故障记录的堆栈中的信息写隕EPROM中
					每隔115ms执行一次
 *output:        void
 *CALLED BY:    vMtimerTreat()
 ******************************************************************************/
void	vWriteEepromEvent(void)
{
		
	//如果堆栈非空，把一条故障记录写隕EPROM中，否则不进行操作
	if(g_u16EepromWriteQueEmpty == FALSE)//0
	{		
	//等待写完EEPROM操作之后再做故障记录的内容  故障日志：地址；类型；时间	
		if(g_u16WriteNumber == 0)
		{
			vWriteAlarmOnce((*g_pEepromWriteOut).uiAddress,
			                (*g_pEepromWriteOut).uiAlarm,(*g_pEepromWriteOut).ubitfData);

			if(g_u16WriteAlarmNumber == 0)
			{
				vEepromWriteOut();
			}
		}
	}
}

/*******************************************************************************
 *Function name: vRecordTime
 *Description:   record runtime 
 *input:         communication u16Address:eeprom address
 *global vars:   void 
 *output:        void
 *CALLED BY:    vMtimerTreat()
 ******************************************************************************/
//g_lq10CurrentRunTime is count time by oneself 
//每次上次读取最后记录的运行时间（自己记录的），与真正模块运行可能存在半个小时的偏差

void vRecordTime(void)
{
	//UINT16 u16CodeType;

	//u16CodeType = 0;
	
	//1分钟   	60s
	//1个小时 	3600s
	//半个小时 	1800S

	if(g_u16RunFlag.bit.RECORDTIME)//1S
	{
	
		g_u16RunFlag.bit.RECORDTIME = 0;
			
		g_lq10CurrentRunTime.lData++;


		if (g_lq10CurrentRunTime.lData >= RUNTIME_UP_LIMIT) // ~=68 year
		{		
			g_lq10CurrentRunTime.lData = TIME_0S_1STIMER;		
		}					
	}

	//每隔半个小时使能向EEPROM保存模块运行时间
	if (g_lq10CurrentRunTime.lData % TIME_0HOUR5_1STIMER == 0)
	{
		g_u16RunFlag.bit.TIMEEEPROM = 1;

	}

	// 当AC切断后启动一次存储使能
    if((g_u16MdlStatus.bit.ACUV)
       && (g_lq10PfcVolt.lData < VPFC_150V)
       && (s_u16CpuRestflag == 1))
	{
	    if(g_u16TimeRecodeState)
		{
			g_u16RunFlag.bit.TIMEEEPROM = 1;

			g_u16TimeRecodeState = 0;
		}
	}
	else
	{
		g_u16TimeRecodeState = 1;
	}

	//每隔半个小时向EEPROM保存模块运行时间
	if(g_u16RunFlag.bit.TIMEEEPROM)
	{	
		g_u16RunFlag.bit.TIMEEEPROM = 0;
		
		// refresh rectifier run time by self 
				
		g_u16EpromWrExtend.bit.RECORDTIMESELF = 1;			

	}

}

/*******************************************************************************
 *Function name: vWriteAlarmOnce
 *Description:   write one alarm to EEPROM in some one address
 *input:         u16Address:eeprom address
 *               fAlarmTemp: the alarm that will be write to the eeprom address
				 fRunTimeTemp:the runtime  that will be write to the eeprom address
				 每个记录8个字节
 *global vars:   void 
 *output:        void
 *CALLED BY:     vRecordEvent()
 *AUTHOR		 zlm 20150529
 ******************************************************************************/
void    vWriteAlarmOnce(unsigned int u16Address, unsigned int u16AlarmTemp, ubitfloat fRunTimeTemp)
{	
	ubitfloat	iq10wralarmtmp;
	ubitfloat	iq10wraddrtmp;

	g_u16WriteAlarmNumber++;

	iq10wralarmtmp.lData = (INT32)u16AlarmTemp;
	iq10wraddrtmp.lData = (INT32)u16Address;

	mWrEnable();					//WP=0,enable write

	if ((g_u16WriteAlarmNumber == 1))
	{
		uiI2caWriteData1Pointer(u16Address, iq10wralarmtmp);//写一次16位数据 ，2Byte
	}
	else if ((g_u16WriteAlarmNumber == 2))
	{
		uiI2caWriteData1Pointer(u16Address + 2, fRunTimeTemp);//写一次16位时间低 , 2Byte			
	}
        else if ((g_u16WriteAlarmNumber == 3))
	{
	        fRunTimeTemp.uintdata[0]= fRunTimeTemp.uintdata[1];
		uiI2caWriteData1Pointer(u16Address + 4, fRunTimeTemp);//写一次16位时间高 , 2Byte			
	}

	
	/*write current eeprom addr to RECORD_EEPROM_ADDR three times
	for power off to continue record alarm	*/

	else if ((g_u16WriteAlarmNumber == 4))			//把当前记录的EEPROM地址存在三个固定地址
	{
		uiI2caWriteDataPointer(RECORD_EEPROM_ADDR, iq10wraddrtmp);
	}
	else if ((g_u16WriteAlarmNumber == 5))
	{
		uiI2caWriteDataPointer(RECORD_EEPROM_ADDR + 256 , iq10wraddrtmp);
	}
	else if ((g_u16WriteAlarmNumber == 6))
	{
		uiI2caWriteDataPointer(RECORD_EEPROM_ADDR + 512 , iq10wraddrtmp);
	}
	else
	{
		g_u16WriteAlarmNumber = 0;
		mWrDisable();				//WP=1,disable write
	}
}

/*******************************************************************************
 *Function name: vReadEepromAlarmData
 *Description:   Read Data from EEPROM 读取后面1K的数据
 *input:         u16Address:eeprom address
 *global vars:   void 
 *output:        
 *CALLED BY:    void vMtimerTreat(void)  50ms
 *              
 ******************************************************************************/
void  vReadEepromAlarmData(UINT16 u16Address)
{
	
	UINT16 u16BlockAddr; //block address 
	UINT16 u16WordAddr;  //word address 

	
	//读取EEPROM地址 1024~2043内容，(每块的最后四个地址空闲) ----地址单元1byte 
	//每条记录占用6个EEPROM地址，2个为故障类型，4个为运行时间
	//一共 168条记录
	//每块 42条记录 一共4块

	//读取每个记录存储得3个变量 ，1个为故障类型，2个为运行时间 ----地址单元2byte 

	//每隔 50ms 读取一个记录

	//0x9200 ~0x93F8  504 data

	//change to 
	//0x0000 ~0x01F8  504 data

		//---------------------read ram start------------------------
		if(IsrVars._u16PCtrl == 5) //0x68d
		{

			//WatchDogKickPointer();
			if (uiI2caReadData1Pointer(u16Address) == I2C_SUCCESS)//读一次16位数据
			{
				*g_pReadEepromData++ = g_fRdTemp.uintdata[0].id;

			}

			if (uiI2caReadDataPointer(u16Address + 2) == I2C_SUCCESS)//读一次32位数据
			{
											
				*g_pReadEepromData++ = g_fRdTemp.uintdata[0].id;
				*g_pReadEepromData++ = g_fRdTemp.uintdata[1].id;

				//g_u16ReadEepromStartAddr += 6;

				u16BlockAddr =  (UINT16)((g_u16ReadEepromStartAddr >> 8) & 0x0007);

				u16WordAddr = (UINT16)(g_u16ReadEepromStartAddr & 0x00ff);

				if(u16WordAddr == BLOCK_END_WORD_ADDR )//246
				{
					if (u16BlockAddr == BLOCK_ADDR) //7
					{
						g_u16ReadEepromStartAddr =  ALARM_START_ADDR ; 
					}
					else 
					{			
						//读取一个故障记录EEPROM内容后，EEPROM的地址增加10
						g_u16ReadEepromStartAddr = g_u16ReadEepromStartAddr + 10;			
					}
						
				}
				else 
				{
					//读取一个故障记录EEPROM内容后，EEPROM的地址增加6
					g_u16ReadEepromStartAddr = g_u16ReadEepromStartAddr + 6;						
				}		

			}

			if(g_pReadEepromData >= (UINT16 *)0x1F8)//0x93F8)//3*168=504--0x1F8   0x0000+0x1F8 = 0x1F8 //0x9200+ 0x1F8= 0x93F8
			{
				
				g_pReadEepromData = (UINT16 *)0x0000;//0x9200;

				g_u16ReadEepromStartAddr = ALARM_START_ADDR;

				IsrVars._u16PCtrl = 7;
			}

		}//_u16PCtrl=5 end 



} // read eeprom function end 


void  vReadEepromcClibaData(UINT16 u16Address)
{	
	//读取EEPROM地址 0~767内容，(前三块地址) ----地址单元1byte 
	
	//EEPROM 地址每隔4个，内存地址两个 

	//每隔 50ms 读取一个记录

		//---------------------read ram start------------------------
		if(IsrVars._u16PCtrl == 9) //0x68d
		{
			if (uiI2caReadDataPointer(u16Address) == I2C_SUCCESS)//读一次32位数据
			{
											
				*g_pReadEepromClibData++ = g_fRdTemp.uintdata[0].id;
				*g_pReadEepromClibData++ = g_fRdTemp.uintdata[1].id;

				g_u16ReadEepromClibStartAddr += 4;

			}

			if(g_pReadEepromClibData >= (UINT16 *)0x380)//)//128*3=384--0x180   0x0200+0x180 =  
			{
				
				g_pReadEepromClibData = (UINT16 *)0x0200;

				g_u16ReadEepromClibStartAddr = EEPROM_CHK_ADDR;

				IsrVars._u16PCtrl = 10;
			}

		}//_u16PCtrl=9 end 

}// read eeprom function end 


/*******************************************************************************
 *Function name: vWriteFloatDataSig
 *Description:   write float in some one address
 *input:         u16Address:eeprom address
 *               fTemp: the data that will be write to the eeprom address
 *global vars:   void 
 *output:        void
 *CALLED BY:    vWriteE2PROMSIG()
 ******************************************************************************/
void	vClearEepromAlarmData(void)
{
	//UINT16 u16CodeMsg;

	ubitfloat  ubitfData;
	
	ubitfData.lData = EPROM_CLEAR_FFFFFFFF;//

	if(IsrVars._u16PCtrl == 2)//enable  //256*4*115=117760ms=117.760s
	{
        if ((g_u16WriteNumber  == 0) && (g_u16WriteAlarmNumber == 0))//等待写完EEPROM操作后
		//等待故障记录写完后 执行清除故障记录操作（为了保证写延时）
		{
			
			g_u16ClearNumber++;
			mWrEnable();			//WP=0,enable write

			if ((g_u16ClearNumber == 1))
			{
				uiI2caWriteDataPointer(g_u16ClearEepromStartAddr, ubitfData);//写32位数据 4Byte		

			}
			else if ((g_u16ClearNumber == 2))
			{
				uiI2caWriteDataPointer(g_u16ClearEepromStartAddr + 256, ubitfData);
			}
			else if ((g_u16ClearNumber == 3))
			{
				uiI2caWriteDataPointer(g_u16ClearEepromStartAddr + 512, ubitfData);
			}
			else if ((g_u16ClearNumber == 4))
			{
				uiI2caWriteDataPointer(g_u16ClearEepromStartAddr + 768, ubitfData);
			}

			else
			{
				g_u16ClearNumber = 0;

				mWrDisable();			//WP=1,disable write

				//写到每块的最后一个地址清除完成
				//if (g_u16ClearEepromStartAddr % 256 == 0)
				if (g_u16ClearEepromStartAddr == ALARM_CLEAR_END_ADDR)//1276
				{			
					
					/*在清除到1276地址时候，把EEPROM的地址指针写到对应的EEPROM地址三次，
					两个函数的执行时间是一样的但是此函数的是5未舜Γ瑅WriteFloatDataSig
					是3次写完，所有下次进入的时候g_u16ClearEepromOk =2*/
					if(g_u16ClearEepromOk==0)
					{
						// write to eeprom three times 
						g_u16EpromWrExtend.bit.EEPROMROINTER = 1;							
						g_u16ClearEepromOk = 1;											
					}

					//if write over ,set _u16PCtrl= 4
					if (g_u16ClearEepromOk==2)
					{														
						g_u16ClearEepromOk = 0;

						g_u16ClearEepromStartAddr = ALARM_START_ADDR;
						//把EEPROM的操作的首地址更新为1024 20170724
						g_u16AddressPointer = ALARM_START_ADDR;	
						
						vEepromBufferInitial();
												
						IsrVars._u16PCtrl = 4;						

					}								
				}

				else//不到每块的最后一个地址
				{
					//每次把一块的地址写完，地址增加4
					g_u16ClearEepromStartAddr += 4;									
				}											
			}
		}//	if ((g_u16WriteNumber  == 0)&&(g_u16WriteAlarmNumber== 0)) end
	
	}//IsrVars._u16PCtrl == 2 end

}//function end

//===========================================================================
// No more.
//===========================================================================


