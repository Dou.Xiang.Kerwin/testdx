/*===========================================================================*
 *         Copyright(c) 2004-2008, Emerson Network Power Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : HD415CZ
 *
 *  FILENAME : gbb_main.c
 *  PURPOSE  : DSP280x  main Functions.
 *				Running on TMS320LF28032                  
 *				External clock is 20MHz, PLL * 6/2 , CPU-Clock 60MHz	      
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2011-08-26      9000           DSP               Created.   Pre-research 
 *	
 *----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *    detail information refer to gbb_main.h      
 *      
 *----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *    void vMtimerTreat(void)  
 *    
 *==========================================================================*/

#include "f28004x_device.h"		// DSP280x Headerfile Include File
#include "App_initial.h"
#include "App_cal.h"
#include "App_vEventLog.h"
#include "App_warnctrl.h"
#include "GBB_epromdata.h"
#include "App_start.h"
#include "App_vAdcTreatment.h"
#include "App_cancom.h"
#include "GBB_Can_Interface.h"
#include "App_isr.h"
#include "GBB_constant.h"
#include "App_constant.h"
#include "Prj_macro.h"
#include "App_main.h"
#include "Prj_config.h"



/*****************************************************************************
*variables definition:these variables only can use in this file 														  
*****************************************************************************/
static UINT16	s_u16BaseTimer; 		// base timer 
static UINT16   s_u16WrE2DelayTimer;    // Write delay timer

static UINT16	s_u16SelfDxBaseTimer;	//20111130 

static UINT16	s_u16FuseBrokenBackTimer; 		// 20121225 

static UINT16   s_u16FUSEFAULBACK;
static Uint16   s_u16GZtestFirst;
// Global variable used by teh SFO library
// Result can be used for all HRPWM channels
// This variable is also copied to HRMSTEP
// register by SFO(0) function.
/*
volatile struct EPWM_REGS *ePWM[PWM_CH] =
 			 {&EPwm1Regs, &EPwm2Regs, &EPwm3Regs, &EPwm4Regs, &EPwm5Regs, &EPwm6Regs};
*/			 
/*****************************************************************************
*globle variables definition:these variables  can use in all of the files 														  
*****************************************************************************/
ubitintc g_u16RunFlag;  		//run flag of all the functions

//20111130
UINT32 g_u32MasterDxTimer;
INT16  g_i16SelfDxDelta;

UINT16 g_u16FuseBroken;
UINT16 g_u16SelfDxCnt;
UINT16 g_u16CpuStartTimer;
UINT16 g_u16IntOscCalibrateStatus;

ubitfloat g_lq10MdlVoltOld;
ubitfloat g_lq10SetVoltOld;
ubitintg g_u16SelfDxValue;
ubitinth g_u16SelfDxCtrl;
ubitinti g_u16SelfDxTimer;
//g3 50A 126UNIT留至少1个做接受数据的缓存，考虑数组整页存放，选0x80=128

//UINT16 value1,value2,value3,value4;
//UINT16   g_u16CanTxLength;
/*****************************************************************************
*functions declare:these functions only can use in this file                                    
*****************************************************************************/
//the main function
void main(void);

//Time base and timing treatment, every 5ms
void vMtimerTreat(void);				

void vSelfDiagnosis(void);  //20111130

void vSelfDiagnosisTimer(void);

//GZ relay test
void vSingleBrdRelayTest(void);
void  vReadUnplug(UINT16 u16Filter);

void vIntOscCalibrate(void);
void vIntOscfromE2(void);
void vIntOscSet(UINT16  u16set);

/*****************************************************************************
 *FUNCTION: main()  
 *PURPOSE:  this function is the main procedure of the system 
 *INTPUT:         void
 *global vars:   void 
 *RETURN:
 *CALLS:
 *CALLED BY:
 *Author:
 *Date:     
 ****************************************************************************/
void main(void)
{
	//Clear all interrupt, initialize PIE vector table:Disable CPU interrupt 
	DINT;

	//ram initialize
	InitRAMPointer();

	//Disable CPU interrupts and clear all CPU interrupt flags
	IER = 0x0000;
	IFR = 0x0000;
   
	//lzy  instead of SFO() if SFO() cannot work
	EALLOW;
	EPwm1Regs.HRMSTEP.all = 113;
	EDIS;
    
	//Initialize PIE control registers:detail info in DSP280x_PieCtrl.c file
	InitPieCtrlPointer();

	//Initialize PIE vector table with pointers to the shell ISR  
	InitPieVectTablePointer();


	/*  
	Initalize the Flash_CPUScaleFactor variable to SCALE_FACTOR
    Initalize the callback function pointer or set it to NULL 
    */
//   	EALLOW;
//   	Flash_CPUScaleFactor = (Uint32)SCALE_FACTOR;
//	EDIS;
//
//   	EALLOW;
//   	Flash_CallbackPtr = NULL;//&MyCallbackFunction;
//   	EDIS;

	// This function Initial and  enables the PIE  interrupts
	vInitialInterrupts();

	MemCopyPointer(&BootRamfuncsLoadStart, &BootRamfuncsLoadEnd, 
				   &BootRamfuncsRunStart);
  	MemCopyPointer(&IsrRamfuncsLoadStart, &IsrRamfuncsLoadEnd, 
  	               &IsrRamfuncsRunStart);

	
	// Initialize the address
	g_u16MdlAddr = 0;
	g_u16MdlAddrBuff = 0;
    
	//for SFO variable initialization
    //HrmstepInit();

	//for cla initial
	vCLATaskinit();
    
	// Initialize other periphereral:detail info in DSP280x_InitPeripherals.c
	//InitPeripheralsPointer();
	InitPeripherals();

	//Initial OSC FROM E2
	vIntOscfromE2();

	//enable Watchdog
	EnableDogPointer();

	//variable initialization
 	vDataInit(); //CPU 各内存变量包括中断变量、CputoCla变量的初始化和清零

    vCputoClaVar_init();//针对不为零的vCputoCla变量初始化

 	//limit variable and rectifier information initialization
 	vLimitInit();

	sCanBufferInitial();
	
	//event log buffer initialization
	vEepromBufferInitial();

	vIsr_init();
 
	EnableInterrupts();

	g_u16CpuStartTimer = 0;
	s_u16GZtestFirst = 0;



	for(;;)
	{
	    IsrVars._u16AcVoltPhaseshift = 20; //todo test
	    //vCanProcess();           //can recieve and transfer data
	    if(g_u16BrdTestFlag == 1)
		{
	        vSingleBrdTst();    // for single board test
		}
	    else if(g_u16BrdTestFlag == 2)
	    {
	        vLEDTst();    // for single board test
	    }
	    else if(g_u16BrdTestFlag == 3)
	    {
	        vSingleBrdRelayTest();
	    }
	    else if(g_u16BrdTestFlag == 4)
	    {
	        vIntOscCalibrate();
	    }
	else
	{
	    //g_lq10PfcVoltDisp.lData = VPFC_405V;
			vSstartCtrl();			// soft start control

			vMtimerTreat();			// main timer treatment

		//    vSelfDiagnosis();		// output (fuse) self diagnosis

			vWarnCtrl();			// IO samp and control

		   vAdcTreatment();		// AD sample and calculation

		   vCircuitCal();			// volt,curr,power calculation

			vAvgCurrCal();			// load sharing calcuation

			if (g_u16AlarmLogEnable == RECENABLE)
			{
				vEventLog();		// alarm log
			}
		}

		vCanDataParsing();		// CAN communication treatment

		vRrpProtocol();			// Rrp protocol treatment

		vCanProcess();		    //can recieve and transfer data

	}
} 	

/*****************************************************************************
 *FUNCTION: void vSingleBrdTst(void) 
 *PURPOSE:  every time in the for loop
 *INPUT: void
 *global vars: 
 *RETURN: void
 *CALLS: 
 *CALLED BY: main() 
 *Author:
 *Date:
 ****************************************************************************/
 //for board test，force pwm drive
void vSingleBrdTst(void)
{

	g_u16MdlStatusExtend.all = 0;//
   	g_u16MdlStatus.all = 0;//

   	if(g_u16ForceDriveFlag == 1)
	{
		DisableDogPointer();
   	 	DINT;
        EALLOW;
		Cla1Regs.MIER.all = 0;
        EDIS;

        if(s_u16GZtestFirst == 0)
        {
            s_u16GZtestFirst =1;

            EALLOW;
            //PFC ocp TZ,clr trip flag
            EPwm1Regs.TZSEL.bit.OSHT3 = TZ_DISABLE;//TZ_DISABLE
            EPwm3Regs.TZSEL.bit.OSHT3 = TZ_DISABLE;//TZ_DISABLE
            EPwm1Regs.TZCLR.bit.OST = 1;//Latched Status Flag for A One-Shot Trip Event
            EPwm3Regs.TZCLR.bit.OST = 1;
            EPwm1Regs.TZCLR.all = 0x00FF;
            EPwm1Regs.TZCLR.bit.INT = 1;//Latched Trip Interrupt Status Flag*/

            //使能PWM驱动
            EPwm1Regs.AQCTLA.all = 0x0112;
            EPwm3Regs.AQCTLA.all = 0x0112;
            EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;
            EPwm3Regs.TBCTL.bit.PHSEN = TB_DISABLE;

            EPwm4Regs.TBPRD = DCPWM_TS_TEST;
            EPwm5Regs.TBPRD = DCPWM_TS_TEST;
            EPwm6Regs.TBPRD = DCPWM_TS_TEST;
            EPwm7Regs.TBPRD = DCPWM_TS_TEST;

            EPwm1Regs.TBPRD = 666;
            EPwm3Regs.TBPRD = 666;
            DELAY_US(TIME5MS_CPUCLK);

            //需要根据PWM的设置配置单板测试的发波

            EPwm6Regs.CMPA.bit.CMPA = DCPWM_DUTY_TEST;
            EPwm6Regs.CMPB.bit.CMPB = DCPWM_TS_TEST - DCPWM_DUTY_TEST;
            EPwm7Regs.CMPA.bit.CMPA = DCPWM_TS_TEST - DCPWM_DUTY_TEST;
            EPwm7Regs.CMPB.bit.CMPB = DCPWM_DUTY_TEST;
            EPwm4Regs.CMPA.bit.CMPA = DCPWM_DUTY_TEST;
            EPwm5Regs.CMPA.bit.CMPA = DCPWM_TS_TEST - DCPWM_DUTY_TEST;

            EPwm1Regs.CMPA.bit.CMPA = 666/2;
            EPwm3Regs.CMPA.bit.CMPA = 666/2;
            EPwm1Regs.CMPB.bit.CMPB = 666/2;
            EPwm3Regs.CMPB.bit.CMPB = 666/2;

            EPwm3Regs.TBPHS.bit.TBPHS = 666/2 ;

            EPwm1Regs.TBCTL.bit.SWFSYNC = 1; //软件同步一次PFC的各PWM开关管,1为master
            EPwm4Regs.TBCTL.bit.SWFSYNC = 1; //软件同步一次DC的各PWM开关管,3为master

            EDIS;

        }


   	 	g_u16ActionReady = POWON_STATE;

		mOnPfcPwm();//
		mOnDcdcPwm();	//
	}
	else
	{
        EnableDogPointer();//
	 	EINT;//
	}

}


/*****************************************************************************
 *FUNCTION: void vLEDTst(void) 
 *PURPOSE:  every time in the for loop
 *INPUT: void
 *global vars: 
 *RETURN: void
 *CALLS: 
 *CALLED BY: main() 
 *Author:
 *Date:
 ****************************************************************************/
 //for board test，TEST LED
void vLEDTst(void) 
{
	g_u16MdlStatusExtend.all = 0;//
   	g_u16MdlStatus.all = 0;//

   	if(g_u16ForceDriveFlag == 2)//
	{           
		DisableDogPointer();//
   	 	DINT;//
        EALLOW;//
        Cla1Regs.MIER.all = 0;//
        EDIS;//
   	 	g_u16ActionReady = POWON_STATE;	//
   	 	mOffDcdcPwm(); //
        mOffPfcPwm(); //
        
        mGreenLedOn();	//
	    mYellowLedOff();//
        mRedLedOff();//
    }

    else if(g_u16ForceDriveFlag == 3)//
	{           
		DisableDogPointer();//
   	 	DINT;//
        EALLOW;//
        Cla1Regs.MIER.all = 0;//
        EDIS;//
   	 	g_u16ActionReady = POWON_STATE;	//
   	 	mOffDcdcPwm(); //
        mOffPfcPwm(); 	//	
   	 
        mRedLedOn();//		
        mGreenLedOff();	//
        mYellowLedOff();//
     
    }

    else if(g_u16ForceDriveFlag == 4)//
    {
        DisableDogPointer();//
        DINT;//
        EALLOW;//
        Cla1Regs.MIER.all = 0;//
        EDIS;//
        g_u16ActionReady = POWON_STATE;	//
        mOffDcdcPwm(); //
        mOffPfcPwm(); //
        
        mYellowLedOn();	//
        mGreenLedOff();//
        mRedLedOff();//
    }
	else
	{
        EnableDogPointer();//
	 	EINT;//
	}
}

/*****************************************************************************
 *FUNCTION: vSingleBrdRelayTest()
 *PURPOSE:
 *INPUT: void
 *global
 *RETURN: void
 *CALLS:
 *CALLED BY: main(viod)
 *Author:
 *Date:
 ****************************************************************************/
void vSingleBrdRelayTest(void)
{
    g_u16MdlStatusExtend.all = 0;//
    g_u16MdlStatus.all = 0;//

    if(g_u16RealyTestFlag == 1)//
    {
        DisableDogPointer();//
        DINT;//
        EALLOW;//
        Cla1Regs.MIER.all = 0;//
        EDIS;//
        g_u16ActionReady = POWON_STATE; //
        mOffDcdcPwm(); //
        mOffPfcPwm(); //

        mMainRelayOpen();

    }
    else if(g_u16RealyTestFlag == 2)//
    {
        DisableDogPointer();//
        DINT;//
        EALLOW;//
        Cla1Regs.MIER.all = 0;//
        EDIS;//
        g_u16ActionReady = POWON_STATE; //
        mOffDcdcPwm(); //
        mOffPfcPwm(); //

        mMainRelayClose();

    }
    else if(g_u16RealyTestFlag == 3)//
      {
          DisableDogPointer();//
          DINT;//
          EALLOW;//
          Cla1Regs.MIER.all = 0;//
          EDIS;//
          g_u16ActionReady = POWON_STATE; //
          mOffDcdcPwm(); //
          mOffPfcPwm(); //

          mOverRelayOpen();

      }
    else if(g_u16RealyTestFlag == 4)//
      {
          DisableDogPointer();//
          DINT;//
          EALLOW;//
          Cla1Regs.MIER.all = 0;//
          EDIS;//
          g_u16ActionReady = POWON_STATE; //
          mOffDcdcPwm(); //
          mOffPfcPwm(); //

          mOverRelayClose();
      }

    else if(g_u16RealyTestFlag == 5)//
    {
        DisableDogPointer();//
        DINT;//
        EALLOW;//
        Cla1Regs.MIER.all = 0;//
        EDIS;//
        g_u16ActionReady = POWON_STATE; //
        mOffDcdcPwm(); //
        mOffPfcPwm(); //

        mOnHmosDriv() ;  //mOffHmosDriv

   }
    else if(g_u16RealyTestFlag == 6)//
    {
        DisableDogPointer();//
        DINT;//
        EALLOW;//
        Cla1Regs.MIER.all = 0;//
        EDIS;//
        g_u16ActionReady = POWON_STATE; //
        mOffDcdcPwm(); //
        mOffPfcPwm(); //

        mOffHmosDriv() ;  //mOffHmosDriv

    }

    else
    {
        mMainRelayOpen();
        mOverRelayClose();
        mOffHmosDriv() ;

        EnableDogPointer();
        EINT;
    }

    vReadUnplug(TIME100MS);
}

/*****************************************************************************
 *FUNCTION: vIntOscCalibrate()
 *PURPOSE:
 *INPUT: void
 *global:g_IntOscCalibrateFlag(bit15-SUCCESS, bit14-INTOSCERR bit8-11:)
 *RETURN: void
 *CALLS:
 *CALLED BY: main(viod)
 *Author:
 *Date:
 ****************************************************************************/
void vIntOscCalibrate(void)
{

    WatchDogKickPointer();
    if((ClkCfgRegs.CLKSRCCTL1.bit.OSCCLKSRCSEL&0x01 == 0x01))
    {
        g_u16IntOscCalibrateFlag = OUTOSC_RETURN;//SUCCESS=0x8000
        return;
    }
    else if(g_u16IntOscCalibrateFlag == 1)//装备检测标志位，如果是0，进行内部晶振校准
    {
        InitSysPll_APP(INT_OSC1,IMULT_20,FMULT_0,PLLCLK_BY_2);//delay 5s according equipment
        s_u16GZtestFirst = 0;

        g_u16IntOscCalibrateFlag = 2;

    }
    else if(g_u16IntOscCalibrateFlag == 3)
    {
        vSingleBrdTst();
     }
     else if(g_u16IntOscCalibrateFlag == 5)//工装计算并判断，如果频率在合理范围内，直接跳转到下一个测试项目，否则上传频率比例系数（Q10），软件判断区域并且写入E2，重新调用PLL函数；
     {
/*--------------------------------------------------------------
        编号   内部晶振  倍频IMULT FMULT     分频         实际时钟                  偏差
                 1   10      19     0         2   95        -5
                 2   10      19     0.25      2   96.25     -3.75
[1.9%,3.15%]     3   10      19     0.5       2   97.5      -2.5
[0.65%,1.9%]     4   10      19     0.75      2   98.75     -1.25
[-0.65%,0.65%]   5   10      20     0         2   100        0
[-1.9%,-0.65%]   6   10      20     0.25      2   101.25     1.25
[-3.15%,-1.9%]   7   10      20     0.5       2   102.5      2.5
                 8   10      20     0.75      2   103.75     3.75
-----------------------------------------------------------------
        [-0.65%,0.65%]  ok，默认编号5
        [-1.9%,-0.65%]  升一级，编号6
        [-3.15%,-1.9%]  升两级，编号7
        [0.65%,1.9%]  降一级，编号4
        [1.9%,3.15%]  降两级，编号3
        [其他]  晶振err
----------------------------------------------------------------*/
         DisableDogPointer();
         if(g_i16FreDeviation >= Q10_PERCENT3D15)//3226         [3.15,-oo]  err
         {
             g_u16IntOscCalibrateStatus = 0;
         }
         else if(g_i16FreDeviation >= Q10_PERCENT1D9)//1945,        [1.9%,3.15%]  降两级，编号3
         {
             g_u16IntOscCalibrateStatus = 3;
         }
         else if(g_i16FreDeviation >= Q10_PERCENT0D65)//666,        [0.65%,1.9%]  降一级，编号4
         {
             g_u16IntOscCalibrateStatus = 4;
         }
         else if(g_i16FreDeviation >= Q10_N_PERCENT0D65)//-666,          [-0.65%,0.65%]  ok，默认编号5
         {
             g_u16IntOscCalibrateStatus = 5;
         }
         else if(g_i16FreDeviation >= Q10_N_PERCENT1D9)//-1945,         [-1.9%,-0.65%]  升一级，编号6
         {
             g_u16IntOscCalibrateStatus = 6;
         }
         else if(g_i16FreDeviation >= Q10_N_PERCENT3D15)//-3226,               [-3.15%,-1.9%]  升两级，编号7
         {
             g_u16IntOscCalibrateStatus = 7;
         }
         else                                           //[-oo, -3.15]  err
         {
             g_u16IntOscCalibrateStatus = 0;
         }

         g_u16ActionReady = POWON_STATE;

         mOffPfcPwm();//
         mOffDcdcPwm();   //
         s_u16GZtestFirst = 0;

         g_u16EpromWrExtend.bit.INTOSC = 1;
         DELAY_US(TIME5MS_CPUCLK);;
         vIntOscSet(g_u16IntOscCalibrateStatus);//延迟5-10s进入装备下一步

     }//校准完毕需要再测试一下吗？
     else if(g_u16ForceDriveFlag == 0)
     {
         EnableDogPointer();//
         EINT;//
     }

}



/*****************************************************************************
 *FUNCTION: vIntOscSet()
 *PURPOSE:
 *INPUT: void
 *global:
 *RETURN: void
 *CALLS:
 *CALLED BY: main(viod)
 *Author:
 *Date:
 ****************************************************************************/
/*--------------------------------------------------------------
                            编号   内部晶振  倍频IMULT FMULT     分频         实际时钟                  偏差
        1   10      19     0         2   95        -5
        2   10      19     0.25      2   96.25     -3.75
        3   10      19     0.5       2   97.5      -2.5
        4   10      19     0.75      2   98.75     -1.25
        5   10      20     0         2   100        0
        6   10      20     0.25      2   101.25     1.25
        7   10      20     0.5       2   102.5      2.5
        8   10      20     0.75      2   103.75     3.75
-----------------------------------------------------------------*/
void vIntOscSet(UINT16  u16set)
{
    switch(u16set)
    {
        case 3:
            InitSysPll_APP(INT_OSC1,IMULT_19,FMULT_0pt5,PLLCLK_BY_2);
            g_u16IntOscCalibrateFlag = INTOSC_SUCCESS;
            break;

        case 4:
            InitSysPll_APP(INT_OSC1,IMULT_19,FMULT_0pt75,PLLCLK_BY_2);
            g_u16IntOscCalibrateFlag = INTOSC_SUCCESS;
            break;

        case 5:
            InitSysPll_APP(INT_OSC1,IMULT_20,FMULT_0,PLLCLK_BY_2);
            g_u16IntOscCalibrateFlag = INTOSC_SUCCESS;
            break;

        case 6:
            InitSysPll_APP(INT_OSC1,IMULT_20,FMULT_0pt25,PLLCLK_BY_2);
            g_u16IntOscCalibrateFlag = INTOSC_SUCCESS;
            break;

        case 7:
            InitSysPll_APP(INT_OSC1,IMULT_20,FMULT_0pt5,PLLCLK_BY_2);
            g_u16IntOscCalibrateFlag = INTOSC_SUCCESS;
            break;

        default:
            InitSysPll_APP(INT_OSC1,IMULT_20,FMULT_0,PLLCLK_BY_2);
            g_u16IntOscCalibrateFlag = INTOSC_ERR;//0X0800
            //g_u16ErrType = 0xF0;
            break;
    }

}


/*****************************************************************************
 *FUNCTION: vMtimerTreat()  
 *PURPOSE: Time base and timing treatment, every 5ms 
 *INPUT: void
 *global vars: g_u16RunFlag:run flag of  all functions
 *RETURN: void
 *CALLS: 
 *CALLED BY: main() 
 *Author:
 *Date:
 ****************************************************************************/
void vMtimerTreat(void)
{	
	// 5mS flag 			
		
	if (mGet5msTiming())
	{
		//clear 5ms flag
		mClr5msTimFlag();

		// avoid watchdog reset, feed dog
		WatchDogKickPointer();
		
		//cumulate the base timer cnt
		s_u16BaseTimer++;
		s_u16WrE2DelayTimer ++;

		//vCanProcess();

		// set each function executive time
		// 3.035S can synchronize adjust volt flag of sharing current 
		if (s_u16BaseTimer % TIME3035MS_BASE == 0)
		{
			g_u16RunFlag.bit.SHARESYN = 1;

		}
		
		// 395mS can data send flag of sharing current 
		else if (s_u16BaseTimer % TIME395MS_BASE == 0)
		{
			g_u16RunFlag.bit.SHAREDATA = 1;

		}

		// 115mS calculation flag of volt/curr/power reference 
		if (s_u16BaseTimer % TIME115MS_BASE == 0)
		{
			g_u16RunFlag.bit.CAL = 1;
			s_u16WrE2DelayTimer = 0;
			vWriteE2PROMSIG();
			if (g_u16AlarmLogEnable == RECENABLE)
			{
				//clear calibrate data
			//	vClearCalibraData();

				// write alarm event to eeprom 
				vWriteEepromEvent();

	            //clear event log eeprom zone
				vClearEepromAlarmData();			
			
			}
		}

		// 10mS io samp & Ctrl Flag,and HVSD 16*5ms timing flag (for 
		//delay 50~150ms)
		if (s_u16BaseTimer % TIME10MS_BASE == 0)
		{
			g_u16RunFlag.bit.WARN = 1;
		}

		
		// 20mS ad samp & filter calculation flag 
		if (s_u16BaseTimer % TIME20MS_BASE == 0)
		{
			g_u16RunFlag.bit.ADC = 1;
		}
       /*
       	if (s_u16BaseTimer % TIME500MS_BASE == 0)
		{
			g_u16RunFlag.bit.FUSEFAULBACK = 1;
		}
       */

		// For eventlog: 50mS read eeprom data  
		if (s_u16BaseTimer % TIME50MS_BASE == 0)
		{
			//g_u16RunFlag.bit.READEEPROM = 1;
			if(s_u16WrE2DelayTimer > 2)
			{
				vReadEepromAlarmData(g_u16ReadEepromStartAddr);
				vReadEepromcClibaData(g_u16ReadEepromClibStartAddr);
			}
		}
		
		// For eventlog: 1s time for record time  
		if (s_u16BaseTimer % TIME1S_BASE == 0)
		{
			g_u16RunFlag.bit.RECORDTIME = 1;
            //GpioDataRegs.GPATOGGLE.bit.GPIO23 = 1;
            //GpioDataRegs.GPBTOGGLE.bit.GPIO34 = 1;
		}

        //paul 20220302
        if (s_u16BaseTimer % TIME5S_BASE == 0)
        {
            g_u16RunFlag.bit.ANTITHIEFTIME = 1;
        }

		// self diagnosis timer 
		//20111130
	//	vSelfDiagnosisTimer();// 5ms time base
	}
}

/**********************************************************/
/* rectifier fuse broken Timer                            */
/**********************************************************/
void vSelfDiagnosisTimer(void) //20111130
{
    UINT16 i;

    s_u16SelfDxBaseTimer ++;

    if (s_u16SelfDxBaseTimer >= TIME0S5_SELFDX_5MS_BASE)                // 0.5S time base
    {
        s_u16SelfDxBaseTimer = 0;

        s_u16FUSEFAULBACK = 1;  //500ms执行一次

        // 5 min or 23 hour delay for self diagnosis
    /*  if (g_u16SelfDxCtrl.bit.MASTERMDL && g_u16MdlStatusExtend.bit.PARAFLAG
            && (g_u16ActionReady >= NORMAL_RUN_STATE))*/
        if (g_u16SelfDxCtrl.bit.MASTERMDL && g_u16MdlStatusExtend.bit.PARAFLAGMASTER
            && (g_u16ActionReady >= NORMAL_RUN_STATE))
        {
            // start self diagnosis 5 min after power on or auto-addresscal
            if (g_u16SelfDxCtrl.bit.STATE == 0)
            {
                g_u32MasterDxTimer ++;

                if (g_u32MasterDxTimer >= TIME300S_SELFDX_BASE) // 5 min
                {
                    g_u16MdlStatusExtend.bit.DIAGNOSISTEST = 1;
                    if(( g_u16MdlStatusExtend.bit.DIAGNOSISBEGIN ==1)&&(g_u32MasterDxTimer <= TIME320S_SELFDX_BASE))
                    {

                        g_u16MdlStatusExtend.bit.DIAGNOSISTEST = 0;
                        g_u16MdlStatusExtend.bit.DIAGNOSISBEGIN =0;
                        g_u16SelfDxCtrl.bit.STATE = 1;
                        g_u16SelfDxValue.bit.ADDRESS = g_u16MdlAddr;
                        g_u16SelfDxCtrl.bit.START = 1;
                        g_u16SelfDxCnt = 0;
                        for (i = 0; i < MAX_NUM; i++)
                         {
                            CanComVolVars._au16MdlVoltSelfDiag[i] = 0;
                         }
                    }
                    else if((g_u32MasterDxTimer > TIME320S_SELFDX_BASE)&&(g_u16SelfDxCtrl.bit.START == 0))
                    {
                        g_u16SelfDxCtrl.bit.STATE = 3;
                        g_u16MdlStatusExtend.bit.DIAGNOSISTEST = 0;
                        g_u16MdlStatusExtend.bit.DIAGNOSISBEGIN =0;
                    }
                }
            }

            // start self diagnosis every 23 hours after first time
            else if (g_u16SelfDxCtrl.bit.STATE == 3)
            {
                g_u32MasterDxTimer ++;

                // 1 hour:7200=60*60*2, 23*7200=165600
                if (g_u32MasterDxTimer >= TIME23HOUR_SELFDX_BASE)
                {
                    g_u16MdlStatusExtend.bit.DIAGNOSISTEST = 1;
                   if((g_u16MdlStatusExtend.bit.DIAGNOSISBEGIN  == 1)&&(g_u32MasterDxTimer <= TIME23HOUR20S_SELFDX_BASE))
                   {
                        g_u16MdlStatusExtend.bit.DIAGNOSISTEST = 0;
                        g_u16MdlStatusExtend.bit.DIAGNOSISBEGIN =0;
                        g_u16SelfDxCtrl.bit.STATE = 4;
                        g_u16SelfDxValue.bit.ADDRESS = g_u16MdlAddr;
                        g_u32MasterDxTimer = TIME30S_SELFDX_BASE;
                        g_u16SelfDxCtrl.bit.START = 1;
                        g_u16SelfDxCnt = 0;

                        for (i = 0; i < MAX_NUM; i++)
                        {
                            CanComVolVars._au16MdlVoltSelfDiag[i] = 0;
                        }
                    }
                    else if((g_u32MasterDxTimer > TIME23HOUR20S_SELFDX_BASE)&&(g_u16SelfDxCtrl.bit.START == 0))
                    {
                        g_u32MasterDxTimer = TIME30S_SELFDX_BASE;
                        g_u16MdlStatusExtend.bit.DIAGNOSISTEST = 0;
                        g_u16MdlStatusExtend.bit.DIAGNOSISBEGIN =0;
                    }
                }
            }

            if (g_u16SelfDxCtrl.bit.START)
            {
                //g_u16SelfDxCtrl.bit.NEXTDELAY ++;
                g_u16SelfDxCtrlNextTimer++;     // 20130704 LQC
            }
        }

        if (g_u16SelfDxCtrl.bit.START && (!g_u16SelfDxCtrl.bit.MASTERMDL))
        {
            g_u16SelfDxTimer.bit.SLAVETIMER ++;

            //after 10s slave mdl disable self diagnosis start
            if (g_u16SelfDxTimer.bit.SLAVETIMER >= TIME15S_SELFDX_BASE)//15s
            {
                g_u16SelfDxCtrl.bit.START = 0;
                g_u16SelfDxTimer.bit.SLAVETIMER = TIME15S_SELFDX_BASE;
            }
        }
    }

    //if (g_i16SelfDxDelta == 1000)//about 1.5V
    if (g_i16SelfDxDelta == VDC_AD_FUSE_ADJ_5V)//about 5V
    {
        g_u16SelfDxTimer.bit.CHKDELAY ++;

        // 1S dealy after volt adjust for self diagnosis
        //if (g_u16SelfDxTimer.bit.CHKDELAY >= 200) //200---->1s
        if (g_u16SelfDxTimer.bit.CHKDELAY >= TIME8S_SELFDX_5MS_BASE) //600---->3s
        {
            g_u16SelfDxTimer.bit.CHKDELAY = TIME8S_SELFDX_5MS_BASE;
            //g_u16SelfDxTimeOut = 1;
        }
    }

    //if (s_u16BaseTimer % 101 == 0)
    if (s_u16BaseTimer % 331 == 0)  //avoid fault alarm, 2013.6.18
    {
        if (g_u16SelfDxCtrl.bit.START)
        {
            g_u16SelfDxCtrl.bit.TXEN = 1;// 500mS self diagnosis data send flag
        }
    }

}

/***********************************************************************************/
/* rectifier fuse broken judge                                                     */
/***********************************************************************************/
void vSelfDiagnosis(void) //20111130
{

    UINT16 i;

    // mdl master don't receive data for  2S, to next mdl
    //if (g_u16SelfDxCtrl.bit.NEXTDELAY > 4)
    if (g_u16SelfDxCtrlNextTimer > TIME11S_SELFDX_BASE)  //change to 6s, 2013.6.18
    {
        //g_u16SelfDxCtrl.bit.NEXTDELAY = 0;
        g_u16SelfDxCtrlNextTimer = 0;
        g_u16SelfDxValue.bit.ADDRESS ++;
    }

    if ((g_u16SelfDxValue.bit.ADDRESS >= MAX_NUM)
        && g_u16SelfDxCtrl.bit.MASTERMDL)
    {
        g_u16SelfDxValue.bit.ADDRESS = MAX_NUM;
        g_u16SelfDxCtrl.bit.START = 0;

        if(g_u16SelfDxCnt == 0)//lzy 20110908
        {
            g_u16SelfDxCnt = 2;
            for (i = 0; i < MAX_NUM; i++)
            {
                // if one fuse broken, diagnosis again
                if (CanComVolVars._au16MdlVoltSelfDiag[i] & 0x8000)
                {
                    g_u32MasterDxTimer = TIME270S_SELFDX_BASE;  // after 30s diagnosis again
                    g_u16SelfDxCtrl.bit.STATE = 0;
                    break;
                }
            }

        }
    }

    if (g_u16SelfDxCtrl.bit.STATE == 2)
    {
        g_u16SelfDxCtrl.bit.STATE = 3;
    }

    if (g_u16SelfDxValue.bit.ATTR == 0x01)
    {
        if ((g_u16MdlStatus.bit.IDERR) || (g_u16MdlStatus.bit.NOCONTROLLER)
            ||(g_u16MdlStatus.bit.OFFSTAT) ||(IsrVars._u16ChoiseCon)
            || (g_lq10MdlCurr.lData > IDC_RATE_2_5PCT))
        {
            g_u16SelfDxValue.bit.ATTR = 0x0;
            g_u16SelfDxCtrl.bit.TXEN = 1;
        }
        else if (!g_i16SelfDxDelta)
        {
            //g_i16SelfDxDelta = 1000;// 1.5V
            g_i16SelfDxDelta = VDC_AD_FUSE_ADJ_5V;// 5V
            g_lq10MdlVoltOld.lData = g_lq10MdlVolt.lData;
            g_lq10SetVoltOld.lData = g_lq10SetVoltTrue.lData;
        }
    }

    // judge if fuse broken after 3S delay of volt adjust for self diagnosis
    if (g_u16SelfDxTimer.bit.CHKDELAY >= TIME8S_SELFDX_5MS_BASE) // 3S
    {
        g_u16SelfDxValue.bit.ATTR = 0x0;
        // can send self diagnosis over data to CAN bus
        g_u16SelfDxCtrl.bit.TXEN = 1;

        //if ((g_lq10SetVoltOld.lData == g_lq10SetVoltTrue.lData)
        if ((labs(g_lq10SetVoltOld.lData - g_lq10SetVoltTrue.lData) < VDC_0V2)
            && ((g_lq10MdlVoltOld.lData - g_lq10MdlVolt.lData) > VDC_1V))
        {
            CanComVolVars._au16MdlVoltSelfDiag[g_u16MdlAddr] |= 0x8000;

            g_u16FuseBroken ++;
            if (g_u16FuseBroken == 3)
            {
                g_u16FuseBroken = 4;
                g_u16MdlStatusExtend.bit.DCFUSEBROKEN = 1;
                g_u16MdlCtrl.bit.FUSEB = 1;
                CanComVolVars._au16MdlVoltSelfDiag[g_u16MdlAddr] &= 0x7fff;
                //uiEepromFlag = uiEepromFlag|0x0002;// save in EEPROM
                g_u16EpromWr.bit.MDLCTRL = 1;
            }

            if (g_u16SelfDxCtrl.bit.MASTERMDL)
            {
                g_u16SelfDxValue.bit.ADDRESS = g_u16SelfDxCtrl.bit.INGADDR + (UINT16)1;
            }
        }
        else
        {
            g_u16FuseBroken = 0;// clear counter to 0 if discontinuous
        }

        g_i16SelfDxDelta = 0;   // volt adjust delta clear after judgement
        g_u16SelfDxTimer.bit.CHKDELAY = 0;
    }


    if(s_u16FUSEFAULBACK == 1)//500MS
    {
        s_u16FUSEFAULBACK = 0;

        if((g_u16MdlCtrl.bit.FUSEB == 1)&&( g_lq10MdlCurr.lData >= IDC_RATE_5PCT)
            && (g_u16ActionReady == NORMAL_RUN_STATE))
        {
            s_u16FuseBrokenBackTimer++;
            if( s_u16FuseBrokenBackTimer >= TIME_FUSE_BRO_10S)//10s
            {
                s_u16FuseBrokenBackTimer = TIME_FUSE_BRO_10S;
                g_u16EpromWr.bit.MDLCTRL =1;
                g_u16MdlCtrl.bit.FUSEB = 0;
                g_u16MdlStatusExtend.bit.DCFUSEBROKEN = 0;
            }
        }
        else
        {
            s_u16FuseBrokenBackTimer = 0;
        }
    }


}

/******************************************************************************
 *Function name: vReadUnplug()
 *Description :  check the rectifier is plugged or not
 *input:         void
 *global vars:   g_u16MdlStatus: module status
 *output:        void
 *CALLED BY:     vWarnCtrl()
 ******************************************************************************/
void  vReadUnplug(UINT16 u16Filter)
{
    static UINT16 s_u16UnplugFilterTime1;

    // check the converter is plugged or not
    if(g_u16UnPlugFlag == 1)
    {
        if(UnplugLowOn==0)

            {
                s_u16UnplugFilterTime1++;
                if(s_u16UnplugFilterTime1 >= u16Filter)
                {
                    g_u16UnPlugFlag = 0;// had plugged in
                    s_u16UnplugFilterTime1 = 0;

                }
            }
    }

    else
    {
        if(UnplugLowOn==1)
        {
            /*s_u16UnplugFilterTime2++;
            if(s_u16UnplugFilterTime2 >= u16Filter)
            {
                g_u16MdlStatusExtend.bit.UNPLUG = 1; // not plugged in
                s_u16UnplugFilterTime2 = 0;
            }*/

            g_u16UnPlugFlag = 1;
        }
    }
}

/**********************************************************/
/* ETOP                                                   */
/**********************************************************/
//lzy for SFO()   待修改
void error (void)
{
	ESTOP0;		    // Stop here and handle error
}

///for APP
// InitPll - This function initializes the PLL registers.
//
// Note: This function uses the DCC to check that the PLLRAWCLK is running at
// the expected rate. If you are using the DCC, you must back up its
// configuration before calling this function and restore it afterward.
//
void
InitSysPll_APP(Uint16 clock_source, Uint16 imult, Uint16 fmult, Uint16 divsel)
{
    Uint32 timeout, retries, temp_syspllmult, pllLockStatus;
    bool status;

    if(((clock_source & 0x3) == ClkCfgRegs.CLKSRCCTL1.bit.OSCCLKSRCSEL)    &&
       (((clock_source & 0x4) >> 2) == ClkCfgRegs.XTALCR.bit.SE)           &&
                     (imult  == ClkCfgRegs.SYSPLLMULT.bit.IMULT)           &&
                     (fmult  == ClkCfgRegs.SYSPLLMULT.bit.FMULT)           &&
                     (divsel == ClkCfgRegs.SYSCLKDIVSEL.bit.PLLSYSCLKDIV))
    {
        //
        // Everything is set as required, so just return
        //
        return;
    }

    if(((clock_source & 0x3) != ClkCfgRegs.CLKSRCCTL1.bit.OSCCLKSRCSEL) ||
       (((clock_source & 0x4) >> 2) != ClkCfgRegs.XTALCR.bit.SE))
    {
        switch (clock_source)
        {
            case INT_OSC1:
                SysIntOsc1Sel_APP();
                break;

            case INT_OSC2:
                SysIntOsc2Sel_APP();
                break;

            case XTAL_OSC:
                SysXtalOscSel_APP();
                break;

            case XTAL_OSC_SE:
                SysXtalOscSESel_APP();
                break;
        }
    }

    EALLOW;

    //
    // First modify the PLL multipliers
    //
    if(imult != ClkCfgRegs.SYSPLLMULT.bit.IMULT ||
       fmult != ClkCfgRegs.SYSPLLMULT.bit.FMULT)
    {
        //
        // Bypass PLL and set dividers to /1
        //
        ClkCfgRegs.SYSPLLCTL1.bit.PLLCLKEN = 0;
        asm(" RPT #100 || NOP");
        ClkCfgRegs.SYSCLKDIVSEL.bit.PLLSYSCLKDIV = 0;

        //
        // Evaluate PLL multipliers
        //
        temp_syspllmult = ((fmult << 8U) | imult);

        //
        // Loop to retry locking the PLL should the DCC module indicate
        // that it was not successful.
        //
        for(retries = 0; (retries < PLL_RETRIES); retries++)
        {
            //
            // Disable PLL
            //
            ClkCfgRegs.SYSPLLCTL1.bit.PLLEN = 0;

            //Delay at least 60 CPU clock
            asm(" RPT #100 || NOP");

            // Program PLL multipliers
            //
            ClkCfgRegs.SYSPLLMULT.all = temp_syspllmult;

            //
            // Enable SYSPLL
            //
            ClkCfgRegs.SYSPLLCTL1.bit.PLLEN = 1;
            asm(" RPT #100 || NOP");

            timeout = PLL_LOCK_TIMEOUT;
            pllLockStatus = ClkCfgRegs.SYSPLLSTS.bit.LOCKS;

            //
            // Wait for the SYSPLL lock
            //
            while((pllLockStatus != 1) && (timeout != 0U))
            {
                pllLockStatus = ClkCfgRegs.SYSPLLSTS.bit.LOCKS;
                timeout--;
            }

            EDIS;

            status = IsPLLValid_APP(clock_source, imult, fmult);

            //
            // Check DCC Status, if no error break the loop
            //
            if(status)
            {
                break;
            }
        }
    }
    else
    {
        status = true;
    }

    if(status)
    {
        EALLOW;
        //
        // Set divider to produce slower output frequency to limit current increase
        //
        if(divsel != PLLCLK_BY_126)
        {
            ClkCfgRegs.SYSCLKDIVSEL.bit.PLLSYSCLKDIV = divsel + 1;
        }
        else
        {
            ClkCfgRegs.SYSCLKDIVSEL.bit.PLLSYSCLKDIV = divsel;
        }

        //
        // Enable PLLSYSCLK is fed from system PLL clock
        //
        ClkCfgRegs.SYSPLLCTL1.bit.PLLCLKEN = 1;

        //
        // Small 100 cycle delay
        //
        asm(" RPT #100 || NOP");

        //
        // Set the divider to user value
        //
        ClkCfgRegs.SYSCLKDIVSEL.bit.PLLSYSCLKDIV = divsel;
        EDIS;
    }
}


//
// SysIntOsc1Sel - This function switches to Internal Oscillator 1 and turns
// off all other clock sources to minimize power consumption
//
void
SysIntOsc1Sel_APP (void)
{
    EALLOW;
    ClkCfgRegs.CLKSRCCTL1.bit.OSCCLKSRCSEL = 2; // Clk Src = INTOSC1
    ClkCfgRegs.XTALCR.bit.OSCOFF=1;             // Turn off XTALOSC
    EDIS;
}

//
// SysIntOsc2Sel - This function switches to Internal oscillator 2 from
// External Oscillator and turns off all other clock sources to minimize
// power consumption
// NOTE: If there is no external clock connection, when switching from
//       INTOSC1 to INTOSC2, EXTOSC and XLCKIN must be turned OFF prior
//       to switching to internal oscillator 1
//
void
SysIntOsc2Sel_APP (void)
{
    EALLOW;
    ClkCfgRegs.CLKSRCCTL1.bit.INTOSC2OFF=0;         // Turn on INTOSC2
    //delay at least 300 cpu time
    asm(" RPT #200 || NOP");
    asm(" RPT #100 || NOP");
    ClkCfgRegs.CLKSRCCTL1.bit.OSCCLKSRCSEL = 0;     // Clk Src = INTOSC2
    ClkCfgRegs.XTALCR.bit.OSCOFF=1;                 // Turn off XTALOSC
    EDIS;
}

//
// PollX1Counter - Clear the X1CNT counter and then wait for it to saturate
// four times.
//
static void
PollX1Counter_APP(void)
{
    Uint16 loopCount = 0;

    //
    // Delay for 1 ms while the XTAL powers up
    //
    // 2000 loops, 5 cycles per loop + 9 cycles overhead = 10009 cycles
    //
    F28x_usDelay(2000);

    //
    // Clear and saturate X1CNT 4 times to guarantee operation
    //
    do
    {
        //
        // Keep clearing the counter until it is no longer saturated
        //
        while(ClkCfgRegs.X1CNT.all > 0x1FF)
        {
            ClkCfgRegs.X1CNT.bit.CLR = 1;
        }

        //
        // Wait for the X1 clock to saturate
        //
        while(ClkCfgRegs.X1CNT.all != 0x3FFU)
        {
            ;
        }

        //
        // Increment the counter
        //
        loopCount++;
    }while(loopCount < 4);
}

//
// SysXtalOscSel - This function switches to External CRYSTAL oscillator and
// turns off all other clock sources to minimize power consumption. This option
// may not be available on all device packages
//
void
SysXtalOscSel_APP (void)
{
    EALLOW;
    ClkCfgRegs.XTALCR.bit.OSCOFF = 0;     // Turn on XTALOSC
    ClkCfgRegs.XTALCR.bit.SE = 0;         // Select crystal mode
    EDIS;

    //
    // Wait for the X1 clock to saturate
    //
    PollX1Counter_APP();

    //
    // Select XTAL as the oscillator source
    //
    EALLOW;
    ClkCfgRegs.CLKSRCCTL1.bit.OSCCLKSRCSEL = 1;
    EDIS;

    //
    // If a missing clock failure was detected, try waiting for the X1 counter
    // to saturate again. Consider modifying this code to add a 10ms timeout.
    //
    while(ClkCfgRegs.MCDCR.bit.MCLKSTS != 0)
    {
        EALLOW;
        ClkCfgRegs.MCDCR.bit.MCLKCLR = 1;
        EDIS;

        //
        // Wait for the X1 clock to saturate
        //
        PollX1Counter_APP();

        //
        // Select XTAL as the oscillator source
        //
        EALLOW;
        ClkCfgRegs.CLKSRCCTL1.bit.OSCCLKSRCSEL = 1;
        EDIS;
    }
}

//
// SysXtalOscSESel - This function switches to external oscillator in
// single-ended mode and turns off all other clock sources to minimize power
// consumption. This option may not be available on all device packages
//
void
SysXtalOscSESel_APP (void)
{
    EALLOW;
    ClkCfgRegs.XTALCR.bit.OSCOFF = 0;     // Turn on XTALOSC
    ClkCfgRegs.XTALCR.bit.SE = 1;         // Select single-ended mode
    EDIS;

    //
    // Wait for the X1 clock to saturate
    //
    PollX1Counter_APP();

    //
    // Select XTALOSC as the oscillator source
    //
    EALLOW;
    ClkCfgRegs.CLKSRCCTL1.bit.OSCCLKSRCSEL = 1;
    EDIS;

    //
    // If missing clock detected, there is something wrong with the oscillator
    // module.
    //
    if(ClkCfgRegs.MCDCR.bit.MCLKSTS != 0)
    {
        ESTOP0;
    }
}


//*****************************************************************************
//
// SysCtl_isPLLValid()
//
//*****************************************************************************
bool
IsPLLValid_APP(Uint16 oscSource, Uint16 imult, Uint16 fmult)
{
    Uint32 dccCounterSeed0, dccCounterSeed1, dccValidSeed0;

    //
    // Setting Counter0 & Valid Seed Value with +/-2% tolerance
    //
    dccCounterSeed0 = DCC_COUNTER0_WINDOW_APP - 2U;
    dccValidSeed0 = 4U;

    //
    // Multiplying Counter-0 window with PLL Integer Multiplier
    //
    dccCounterSeed1 = DCC_COUNTER0_WINDOW_APP * imult;

    //
    // Multiplying Counter-0 window with PLL Fractional Multiplier
    //
    switch(fmult)
    {
        case FMULT_0pt25:
            //
            // FMULT * CNTR0 Window = 0.25 * 100 = 25, gets added to cntr0
            // seed value
            //
            dccCounterSeed1 = dccCounterSeed1 + 25U;
            break;
        case FMULT_0pt5:
            //
            // FMULT * CNTR0 Window = 0.5 * 100 = 50, gets added to cntr0
            // seed value
            //
            dccCounterSeed1 = dccCounterSeed1 + 50U;
            break;
        case FMULT_0pt75:
            //
            // FMULT * CNTR0 Window = 0.75 * 100 = 75, gets added to cntr0
            // seed value
            //
            dccCounterSeed1 = dccCounterSeed1 + 75U;
            break;
        default:
            //
            // No fractional multiplier
            //
            dccCounterSeed1 = dccCounterSeed1;
            break;
    }

    //
    // Enable Peripheral Clock Domain PCLKCR21 for DCC
    //
    EALLOW;
    CpuSysRegs.PCLKCR21.bit.DCC_0 = 1;

    //
    // Clear Error & Done Flag
    //
    Dcc0Regs.DCCSTATUS.bit.ERR = 1;
    Dcc0Regs.DCCSTATUS.bit.DONE = 1;

    //
    // Disable DCC
    //
    Dcc0Regs.DCCGCTRL.bit.DCCENA = 0x5;

    //
    // Disable Error Signal
    //
    Dcc0Regs.DCCGCTRL.bit.ERRENA = 0x5;

    //
    // Disable Done Signal
    //
    Dcc0Regs.DCCGCTRL.bit.DONEENA = 0x5;

    //
    // Configure Clock Source0 to whatever is set as a clock source for PLL
    //
    switch(oscSource)
    {
        case INT_OSC1:
            Dcc0Regs.DCCCLKSRC0.bit.CLKSRC0 = 1; // Clk Src0 = INTOSC1
            break;

        case INT_OSC2:
            Dcc0Regs.DCCCLKSRC0.bit.CLKSRC0 = 2; // Clk Src0 = INTOSC2
            break;

        case XTAL_OSC:
        case XTAL_OSC_SE:
            Dcc0Regs.DCCCLKSRC0.bit.CLKSRC0 = 0; // Clk Src0 = XTAL
            break;
    }

    //
    // Configure Clock Source1 to PLL
    //
    Dcc0Regs.DCCCLKSRC1.bit.KEY = 0xA; // Clk Src1 Key to enable clock source selection for count1
    Dcc0Regs.DCCCLKSRC1.bit.CLKSRC1 = 0; // Clk Src1 = PLL

    //
    // Configure COUNTER-0, COUNTER-1 & Valid Window
    //
    Dcc0Regs.DCCCNTSEED0.bit.COUNTSEED0 = dccCounterSeed0; // Loaded Counter0 Value
    Dcc0Regs.DCCVALIDSEED0.bit.VALIDSEED = dccValidSeed0;  // Loaded Valid Value
    Dcc0Regs.DCCCNTSEED1.bit.COUNTSEED1 = dccCounterSeed1; // Loaded Counter1 Value

    //
    // Enable Single Shot Mode
    //
    Dcc0Regs.DCCGCTRL.bit.SINGLESHOT = 0xA;

    //
    // Enable Error Signal
    //
    Dcc0Regs.DCCGCTRL.bit.ERRENA = 0xA;

    //
    // Enable Done Signal
    //
    Dcc0Regs.DCCGCTRL.bit.DONEENA = 0xA;

    //
    // Enable DCC to start counting
    //
    Dcc0Regs.DCCGCTRL.bit.DCCENA = 0xA;
    EDIS;

    //
    // Wait until Error or Done Flag is generated
    //
    while((Dcc0Regs.DCCSTATUS.all & 3) == 0)
    {
    }

    //
    // Returns true if DCC completes without error
    //
    return((Dcc0Regs.DCCSTATUS.all & 3) == 2);

}
//===========================================================================
// No more.
//===========================================================================

